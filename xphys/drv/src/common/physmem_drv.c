/*
 *
 * Copyright (C) 2001,2002   luciano.orsini@cern.ch (L.Orsini)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 *       
 */

/*************************************************************************
 * 
 * system headers 
 *
 *************************************************************************/

/* retrieve the CONFIG_* macros */
#include <linux/autoconf.h>
#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
#define MODVERSIONS
#endif

#ifdef MODVERSIONS
#include <linux/modversions.h>
#endif

#include <linux/config.h>
#include <linux/module.h>
#include <linux/param.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/wrapper.h>
#include <linux/config.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/ptrace.h>
#include <linux/mman.h>
#include <linux/mm.h>
#include <linux/time.h>
#include <linux/unistd.h>
#include <linux/major.h>
#include <linux/slab.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <asm/segment.h>
#include <asm/pgtable.h>
#include <asm/io.h>

/*************************************************************************
 * 
 * private headers 
 *
 *************************************************************************/


#include "sysdep.h"
#include "bigphysarea.h"
#include "physmem_internal.h"


/*************************************************************************
 * 
 * local definitions 
 *
 *************************************************************************/

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luciano Orsini");

/* Dynamically allocate a list of bigphys devices
 */

static physmem_dev_t* physmem_devs = 0;

/* Each client (file descriptor) who opens the driver is assigned a
   client_id.  next_client_id is the next free one. */
static int next_client_id = 1;

static struct semaphore sem;	/* for thread safety */

/*************************************************************************
 * 
 * prototypes for public functions 
 *
 *************************************************************************/

int init_module (void);
void cleanup_module (void);

/*************************************************************************
 * 
 * prototypes for private functions 
 *
 *************************************************************************/


int physmem_ioctl (struct inode *, struct file *, unsigned int cmd, unsigned long arg);
int physmem_mmap (struct file *, struct vm_area_struct *);
int physmem_open (struct inode *, struct file *);
int physmem_release (struct inode *, struct file *);

/*************************************************************************
 * 
 * file scope variables
 *
 *************************************************************************/


/*int physmem_nmb_pages = PHYSMEM_NMB_PAGES ; */
/* MODULE_PARM (physmem_nmb_pages,"i") ; */


/*************************************************************************
 * 
 * file operations for char device 
 *
 *************************************************************************/

#ifdef OBSOLETE

struct file_operations physmem_fops =
{
  NULL,		      /* lseek  */
  NULL,		      /* read   */
  NULL,		      /* write  */
  NULL,		      /* readdir */
  NULL,		      /* select */
  physmem_ioctl,      /* ioctl  */
  physmem_mmap,	      /* mmap */
  physmem_open,	      /* open */
  physmem_release,    /* release */
  NULL,		      /* fsync */
  NULL,		      /* fasync */
  NULL,		      /* check_media_change */
  NULL		      /* revalidate */
};

#endif

struct file_operations physmem_fops =
{
  ioctl: physmem_ioctl,      /* ioctl  */
  mmap: physmem_mmap,	      /* mmap */
  open: physmem_open,	      /* open */
  release: physmem_release,    /* release */
};



/*************************************************************************
 * 
 * init_module() is called when driver is installed [/sbin/insmod] 
 *
 *************************************************************************/


int init_module (void)
{
	int rt;
	PRINTF(5) ("physmem: init_module() called\n");

	// initialize list and client counter
	physmem_devs = 0;
	next_client_id = 1;
	
  	/* register char device */

	rt = register_chrdev (PHYSMEM_MAJOR, "physmem", &physmem_fops) ;
	if (rt) 
	{
		PRINTF(0) ("physmem: register_chrdev() failed. major_device_num = %d \n",
		       PHYSMEM_MAJOR);
		return -EIO;
	}
	
	// Initialize semaphore
	sema_init(&sem, 1);
	return  0;
}

int config_bigphys(unsigned long physmem_nmb_pages, physmem_dev_t *dev_p)
{
  int physmem_size ;
  void *bpa_addr ;
  
  /*if (physmem_nmb_pages == PHYSMEM_NMB_PAGES) 
  {
    PRINTF(0) ("physmem:  request default nmb pages: %d \n",physmem_nmb_pages) ;
  }
  else 
  {
    PRINTF(0) ("physmem:  request nmb pages according to insmod parameter: %d \n",physmem_nmb_pages) ;
  }
  */
  bpa_addr = bigphysarea_alloc_pages (physmem_nmb_pages, 0, GFP_KERNEL) ;
  
  if (bpa_addr == 0) 
  {
    PRINTF(0) ("physmem:  bigphysarea_alloc_pages() failed for %ld pages. \n",physmem_nmb_pages) ;
    goto error ;
  }

  physmem_size =  physmem_nmb_pages * PAGE_SIZE ;

  dev_p->bpa_nmb_pages = physmem_nmb_pages ;
  dev_p->bpa_size = (int) physmem_size ;
  dev_p->bpa_addr = (int) bpa_addr ;
  dev_p->bpa_bus  = virt_to_bus(bpa_addr) ;
  dev_p->bpa_phys = virt_to_phys(bpa_addr) ;
 

  PRINTF(0) ("physmem: nmb_pages %ld size x%08X pba kern x%08X bus x%08X phys x%08X \n"
	     ,physmem_nmb_pages
	     ,(int) physmem_size
	     ,(int) bpa_addr, dev_p->bpa_bus, dev_p->bpa_phys   ) ;


  /* mark all pages as 'reserved' */

#ifdef K22
  {
    int i ;
    int start = bpa_addr ;
    int end = start + PHYSMEM_SIZE - 1 ;
    for ( i = MAP_NR(start) ; i <= MAP_NR(end) ; i++) {
      mem_map_reserve(i) ;
    }
  }
#else

  {
    struct page *page ;
    int start = (int) bpa_addr ;
    int end = start + physmem_size - 1 ;
    int npages = 0 ;

    for (page = virt_to_page(start) ; page <= virt_to_page(end) ; page++) {
      npages ++ ;
      mem_map_reserve(page) ;
    }
    PRINTF(0) ("physmem-init_module: mem_map_reserve %d pages start x%08X end x%08X \n"
	       ,npages,(int)  virt_to_page(start), (int)virt_to_page(end)) ; 
  }
#endif

  return 0; /* happy */

 error:

  unregister_chrdev (PHYSMEM_MAJOR, "physmem");
  return -EIO;
}
/*************************************************************************
 * 
 * cleanup_module() is called when driver is removed  [/sbin/rmmod] 
 *
 *************************************************************************/



void cleanup_module(void)
{
  physmem_dev_t *dev_p ;
  physmem_dev_t *tmp ;
  void *bpa_addr ;
  int physmem_size ;

  PRINTF(5) ("physmem: cleanup_module() called\n");

  dev_p = physmem_devs;
  while (dev_p != 0)
  {
	bpa_addr = (void*) dev_p->bpa_addr ;
	physmem_size =  dev_p->bpa_size ;
	
	// Only clean up, if memory has been mapped
	if (physmem_size != 0)
	{
		/* free all resources */
		/* mark all pages as no longer 'reserved' */

		#ifdef K22
		  {
		    int i ;
		    int start = bpa_addr ;
		    int end = start + PHYSMEM_SIZE - 1 ;
		    for ( i = MAP_NR(start) ; i <= MAP_NR(end) ; i++) {
		      mem_map_unreserve(i) ;
		    }
		  }
		#else
		  {
		    struct page *page ;
		    int start = (int) bpa_addr ;
		    int end = start + physmem_size - 1 ;
		    int npages = 0 ;

		    for (page = virt_to_page(start) ; page <= virt_to_page(end) ; page++) {
		      npages ++ ;
		      mem_map_unreserve(page) ;
		    }
		    PRINTF(0) ("physmem-cleanup_module: mem_map_unreserve() %d pages start x%08X end x%08X \n"
			       ,npages,(int)  virt_to_page(start), (int)virt_to_page(end)) ; 
		  }
		#endif

		bigphysarea_free_pages (bpa_addr) ;
	}
	
	tmp = dev_p;  
  	dev_p = dev_p->next;
	vfree(tmp);
  }
  
  unregister_chrdev (PHYSMEM_MAJOR, "physmem");
}


/*************************************************************************
 * 
 * _open()  
 *
 *************************************************************************/


int
physmem_open (struct inode *inode_p, struct file *file_p)
{
 

  int minor = MINOR(inode_p->i_rdev);
  int major = MAJOR(inode_p->i_rdev);

  physmem_dev_t *dev_p ;

  if (down_interruptible(&sem))
    return -ERESTARTSYS;
    
    
  PRINTF(5)("physmem_open(%p, %p)\n", (void*)inode_p,  (void*)file_p);
  PRINTF(5)("physmem_open:  major = %d  minor = %d\n", major, minor);

  /* check device present */

  dev_p = vmalloc(sizeof(struct physmem_dev));
  if (dev_p == 0) 
  {
    printk(KERN_WARNING "bighys: couldn't allocate space to store addresses\n");
    up(&sem); 
    return -EIO;
  }
  
   
  memset(dev_p, 0, sizeof (physmem_dev_t) );
  
    
  dev_p->client = next_client_id;  // we keep track of which client_id is assigned to which id
  dev_p->next = physmem_devs;

  physmem_devs = dev_p;
  
  /*
  if (dev_p->bpa_addr == 0)
    return -ENODEV ;
   */
  file_p->private_data = dev_p ;

  next_client_id++;
  up(&sem); 
  return (0); /* happy */
}

/*************************************************************************
 * 
 * _release()  
 *
 *************************************************************************/


int
physmem_release (struct inode *inode_p, struct file *file_p)
{
  physmem_dev_t *dev_p ;
   physmem_dev_t *previous ;
  void *bpa_addr ;
  int physmem_size ;
  int minor = MINOR(inode_p->i_rdev);
  int major = MAJOR(inode_p->i_rdev);
  
  if (down_interruptible(&sem))
    return -ERESTARTSYS;

  PRINTF(5)("physmem_release(%p, %p)\n", (void*)inode_p, (void*)file_p);
  PRINTF(5)("physmem_release:  major = %d  minor = %d\n", major, minor);
 
  dev_p = physmem_devs;
  previous = dev_p;
  while (dev_p != 0)
  {
  	if (dev_p == file_p->private_data)
	{
		bpa_addr = (void*) dev_p->bpa_addr;
		physmem_size = dev_p->bpa_size;
		
		// only clear, if memory has been mapped before
		if (physmem_size != 0)
		{
			  /* free all resources */
			  /* mark all pages as no longer 'reserved' */

			#ifdef K22
			  {
			    int i ;
			    int start = bpa_addr ;
			    int end = start + PHYSMEM_SIZE - 1 ;
			    for ( i = MAP_NR(start) ; i <= MAP_NR(end) ; i++) {
			      mem_map_unreserve(i) ;
			    }
			  }
			#else
			  {
			    struct page *page ;
			    int start = (int) bpa_addr ;
			    int end = start + physmem_size - 1 ;
			    int npages = 0 ;
			    for (page = virt_to_page(start) ; page <= virt_to_page(end) ; page++) {
			      npages ++ ;
			      mem_map_unreserve(page) ;
			    }
			    PRINTF(0) ("physmem-cleanup_module: mem_map_unreserve() %d pages start x%08X end x%08X \n"
				       ,npages,(int)  virt_to_page(start), (int)virt_to_page(end)) ; 
			  }
			#endif
			  bigphysarea_free_pages (bpa_addr) ;
		}
		// Remove device record
		if (dev_p == physmem_devs)
		{	
			physmem_devs = physmem_devs->next; // remove the first element
		} else
		{		
			previous->next = dev_p->next;	// in the middle or at the end		
		}
		
		vfree(dev_p); // free the current element
		
		up(&sem);
 		return 0;
	}
	previous = dev_p;
  	dev_p = dev_p->next;	
  }
  up(&sem);
  
  return 0;
} 

/*************************************************************************
 * 
 * _ioctl()  
 *
 *************************************************************************/


int
physmem_ioctl (struct inode *inode_p, struct file *file_p,
	      unsigned int cmd, unsigned long arg)
{
  /* int minor = MINOR(inode_p->i_rdev); */
  physmem_dev_t *dev_p ;
  
  struct physmem_info info_copy;  /* holder for_info */
  unsigned long physmem_nmb_pages;

 if (down_interruptible(&sem))
    return -ERESTARTSYS;

  /* check device present */

  dev_p = file_p->private_data;
  
  /*if (dev_p->bpa_addr == 0)
    return -ENODEV ;
  */ 


  /* switch according to cmd code */

  switch(cmd) {

  case PHYSMEM_GET_INFO:

    
  	if (dev_p->bpa_addr == 0)
	{
		up(&sem);
    		return -ENODEV;
	}
   
    /* fill all fields with zeroos in board_info for time being */

    info_copy.bpa_nmb_pages = dev_p->bpa_nmb_pages ;
    info_copy.bpa_size = dev_p->bpa_size ;
    info_copy.bpa_kern = dev_p->bpa_addr ;
    info_copy.bpa_bus  = dev_p->bpa_bus ;
    info_copy.bpa_phys = dev_p->bpa_phys ;

    /* fill struct in user address space */ 

    if (!access_ok (VERIFY_WRITE, (void *) arg, sizeof(struct physmem_info))) {
     up(&sem);
      return -EFAULT ;
    }
    __copy_to_user((void *) arg, &info_copy, sizeof(struct physmem_info));

    break;

  case PHYSMEM_CONFIG:
  	physmem_nmb_pages = (unsigned long)arg;
	 up(&sem);
  	return config_bigphys(physmem_nmb_pages, dev_p);
  break;
  

  default:

   PRINTF(0) ("physmem: ioctl cmd %X \n",cmd) ;
	up(&sem);
    return -EINVAL;
  }

  up(&sem);
  return 0;  /* happy */
}



/*************************************************************************
 * 
 * _mmap()  
 *
 * gives user access to board map (and maybe later copy_block space)
 *  
 *
 *************************************************************************/


int
physmem_mmap (struct file *file_p, struct vm_area_struct *vma)
{
  physmem_dev_t *dev_p ;
  int unit ;
  unsigned long offset;
  unsigned long len ;
  unsigned long vma_len ;
  unsigned long vma_start, vma_end, vma_offset ;
  pgprot_t vma_page_prot ;

 if (down_interruptible(&sem))
    return -ERESTARTSYS;


  unit = -1 ; /* not set */

  PRINTF(5) ("physmem_mmap(%p, %p)\n",(void*)file_p, (void*)vma);
  
  /* [note that unlike most driver entry points inode* is not in the 
   *  argument list. have to obtain unit number another way] 
   */
 
  dev_p = (physmem_dev_t *) file_p->private_data;

  /* check if device present */
  if (dev_p->bpa_addr == 0)
  {
    up(&sem);
    return -ENODEV ;
   }

#ifdef K22
  vma_offset =  (unsigned long) vma_get_offset(vma) ;
  vma_start =  (unsigned long) vma_get_start(vma) ;
  vma_end =  (unsigned long) vma_get_end(vma) ;
  vma_page_prot = vma_get_page_prot(vma) ;
#else

  vma_offset = vma->vm_pgoff << PAGE_SHIFT  ;
  vma_start =  vma->vm_start ;
  vma_end =  vma->vm_end ;
  vma_page_prot = vma->vm_page_prot ;
#endif

  vma_len = vma_end - vma_start ;

  PRINTF(0) ("bpa_mmap[%d]:  offset = %lx  start = %lx  end = %lx (%lx)\n",
	     unit,vma_offset,vma_start,vma_end,vma_len);

  /* 
   * just map the whole area to user space 
   */

  len = dev_p->bpa_size ;

  if (vma_len < len ) {
    PRINTF(0) ("physmem_mmap[%d]: FATL vma_len too small; len %08lX vma_len %08lX\n",unit, len, vma_len);
	up(&sem);
    return -EAGAIN;
  }


  offset = (unsigned long) dev_p->bpa_phys  ;

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,4,18) && RED_HAT_LINUX_KERNEL
  if (remap_page_range (vma, vma_start, offset, len, vma_page_prot)) { 
#else
  if (remap_page_range (vma_start, offset, len, vma_page_prot)) { 
#endif
    up(&sem);
    return -EAGAIN;
  }

  PRINTF(0) ("physmem_mmap[%d]: mapped %08lX to %08lX len %08lX \n",unit, offset,vma_start,len);
 
  up(&sem);
  return (0);   /* happy */  
}


/*************************************************************************
 * 
 * auxilary functions  
 *
 *************************************************************************/





