#ifndef _XPhysAllocator_h_
#define _XPhysAllocator_h_

#include "Allocator.h"
#include "xphysException.h"

class XPhysAllocator;

class XPhysBuffer: public Buffer 
{
	friend class XPhysAllocator;
	
	public:
	
	XPhysBuffer(unsigned long usrAddr, unsigned long busAddr, 
			unsigned long kernelAddr, unsigned long size) ;
	
	void * userAddr() ;
	
	void * physAddr() ;
	
	void * kernelAddr() ;
	
	unsigned long size();
	
	protected:
	unsigned long usrAddr_;
        unsigned long physAddr_;
        unsigned long kernelAddr_;
	unsigned long size_;
};


class XPhysAllocator: public virtual Allocator 
{
	public:
	

	/*! Open the BigPhys area and map at most \param maxSize Bytes into user space */
	XPhysAllocator(unsigned long pages) ;
	
	virtual ~XPhysAllocator() ;
	
	//! Allocate \param size Bytes from the BigPhys area using the bget library and return a Buffer structure	
	Buffer * alloc(unsigned long size) ;
	
	//! Release a previously allocate Buffer structure back to the pool managed by the bget library
	void free (Buffer * buffer) ;

        //! Retrieve the user space, virtual memory base address, indicating the start of the BigPhys area
        unsigned long usrBaseAddr ();

	//! Retrieve the physical base address, indicating the start of the BigPhys area
	unsigned long busBaseAddr ();

	//! Retrieve the kernel space base address, indicating the start of the BigPhys area
	unsigned long kernelBaseAddr ();

	//! Retrieve the total size of the BigPhys area
	unsigned long size ();

	/*!
	If any of the following parameter pointer values is zero, the function returns immediately.
	The amount of space currently allocated is stored into the variable pointed to by \param curalloc. 
	The total free space (sum of all free blocks in the pool) is stored into the variable pointed to by \param totfree, 
	and the size of the largest single block in the free space pool is stored into the variable pointed to by \param maxfree.
	The number of allocated blocks is stored into the variable pointed to by \param nallocated.
	*/	
	void statistics (unsigned long* curalloc, unsigned long* totfree, unsigned long* maxfree, unsigned long* nallocated);
	
	protected:
	
	void * virttophys(unsigned long userAddr);
	void * virttokernel(unsigned long userAddr);
	
	unsigned long segmentSize_;
 	unsigned long baseUserAddress_;
	unsigned long baseBusAddress_;
	unsigned long baseKernelAddress_ ;
	
	int fd_;

};
#endif
