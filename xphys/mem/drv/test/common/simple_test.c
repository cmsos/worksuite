// $Id: simple_test.c,v 1.1 2004/09/16 10:05:07 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, S. Markan and L. Orsini                         *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
 
 /*
 *
 * This is a very simple program to test the memphys driver.  It
 * assumes an inode entry pointing to the driver has been made at
 * /dev/memphys0.  The module must be loaded.  It finds out how much
 * memory is available in the pool, allocates all of it, and (one
 * chunk at a time) mmaps it and tests that it can write to and read
 * from the mmapped region.
 *
 * More information in readme.html
 */

#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

#include "memphys.h"

#define PAGE_SIZE 4096

int main() {
  int i, j;
  int fd = open("/dev/memphys0", O_RDWR);
  int result;

  if (fd <= 0) {
    printf("Couldn't open driver\n");
    exit(1);
  }

  // Print info about the pool
  struct pool_info pi;
  ioctl(fd, MEMPHYS_IOC_POOL_INFO, &pi);
  printf("Pool has %d chunks of order %d available.\n", pi.chunks_available, pi.chunk_order);
  printf("My client id is %d\n", ioctl(fd, MEMPHYS_IOC_CLIENT_ID, NULL));

  // Try allocating the whole thing
  struct alloc_info ai;
  ai.num_chunks = pi.chunks_available;
  ai.result = (struct chunk_addrs*) malloc(ai.num_chunks * sizeof(struct chunk_addrs));

  result = ioctl(fd, MEMPHYS_IOC_ALLOC, &ai);
  if (result == 0)
    printf("Successfully allocated the chunks\n");
  else {
    printf("Could not allocate the chunks, ending\n");
    exit(1);
  }

  // Try mmapping it and confirm that we can write there
  printf("Testing memory received from mmap\n");

  for (j = 0; j < pi.chunks_available; j++) 
	{
    		void *adr = mmap(0, PAGE_SIZE * 8, PROT_READ | PROT_WRITE, MAP_SHARED, fd, ai.result[j].index * PAGE_SIZE << pi.chunk_order);
    
		unsigned long max_addr = (PAGE_SIZE << pi.chunk_order) / sizeof (unsigned long);
		printf ("Testing addresses %d to %d in chunk %d\n", 0, max_addr, j);
    		for (i = 0; i < max_addr; i++) 
		{
      			// ((int *) adr)[i] = i;
			//printf ("Write value %d to address %x", i, ((unsigned long*) adr)[i]);
      			((unsigned long *) adr)[i] = i;
    		}
    
    for (i = 0; i < PAGE_SIZE << pi.chunk_order / sizeof(unsigned long); i++) {
      if (((int *) adr)[i] != i)
	exit(1);
    }
  }
  printf("Test succeeded.\n");

  // Print some info about the first chunk
  printf("\nFirst chunk has addresses:\n");
  printf("Index: %X\n", ai.result[0].index);
  printf("Virt: %X\n", ai.result[0].virt_adr);
  printf("Phys: %X\n", ai.result[0].phys_adr);
  printf("Bus: %X\n", ai.result[0].bus_adr);

  return 0;
}
