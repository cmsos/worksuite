// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_Summary_h_
#define _xdata_Summary_h_

#include <string>
#include <map>

#include "prometheus/summary.h"
#include "prometheus/registry.h"

#include "xdata/Collectable.h"

#include "xdata/Serializable.h"


namespace xdata
{
	// e.g. Summary("exposer_transferred_bytes_total","Transferred bytes to metrics services", {{"label", "value"}})

	class Summary: public Collectable
	{
			public:

			using Quantiles = std::vector<prometheus::detail::CKMSQuantiles::Quantile>;

			class Metric
			{
			    public:

				Metric(prometheus::Summary & c);
				
				/// \brief Observe the given amount.
				void Observe(double value);

			    protected:
				prometheus::Summary & metric_;
			};


			Summary(const std::string & name, const std::string & help,  const std::map<std::string, std::string>& labels);

			std::string type() const;

			void setValue (const xdata::Serializable& s);

			int equals(const xdata::Serializable & s) const;

			std::string toString() const;

			void fromString(const std::string& value);

			template <typename... Args>
			Metric operator()(const std::map<std::string, std::string>& labels, Args&&... args);


  			prometheus::Family<prometheus::Summary> &   family_;
  			std::map<size_t, prometheus::Summary *> labels_;
	};


}
/// \brief Create a summary metric.
  ///
  /// \param quantiles A list of 'targeted' Phi-quantiles. A targeted
  /// Phi-quantile is specified in the form of a Phi-quantile and tolerated
  /// error. For example a Quantile{0.5, 0.1} means that the median (= 50th
  /// percentile) should be returned with 10 percent error or a Quantile{0.2,
  /// 0.05} means the 20th percentile with 5 percent tolerated error. Note that
  /// percentiles and quantiles are the same concept, except percentiles are
  /// expressed as percentages. The Phi-quantile must be in the interval [0, 1].
  /// Note that a lower tolerated error for a Phi-quantile results in higher
  /// usage of resources (memory and cpu) to calculate the summary.
  ///
  /// The Phi-quantiles are calculated over a sliding window of time. The
  /// sliding window of time is configured by max_age and age_buckets.
  ///
  /// \param max_age Set the duration of the time window, i.e., how long
  /// observations are kept before they are discarded. The default value is 60
  /// seconds.
  ///
  /// \param age_buckets Set the number of buckets of the time window. It
  /// determines the number of buckets used to exclude observations that are
  /// older than max_age from the summary, e.g., if max_age is 60 seconds and
  /// age_buckets is 5, buckets will be switched every 12 seconds. The value is
  /// a trade-off between resources (memory and cpu for maintaining the bucket)
  /// and how smooth the time window is moved. With only one age bucket it
  /// effectively results in a complete reset of the summary each time max_age
  /// has passed. The default value is 5.
  // xdata::Summary::operator() (const std::map<std::string, std::string>& labels, const Quantiles& quantiles,
  //       std::chrono::milliseconds max_age = std::chrono::seconds{60},
  //        int age_buckets = 5)

template <typename... Args>
xdata::Summary::Metric xdata::Summary::operator() ( const std::map<std::string, std::string>& labels, Args&&... args)
{
	auto lhash = prometheus::detail::hash_labels(labels);
	//std::cout << "get hash for labels: " << lhash << std::endl;
	if ( labels_.find(lhash) != labels_.end() )
	{
			return xdata::Summary::Metric(*(labels_[lhash]));
	}
	else
	{
		labels_[lhash] = &(family_.Add(labels, args...));
		return xdata::Summary::Metric(*(labels_[lhash]));
	}
}
#endif
