// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_Histogram_h_
#define _xdata_Histogram_h_

#include <string>
#include <map>

#include "prometheus/histogram.h"
#include "prometheus/registry.h"

#include "xdata/Collectable.h"

#include "xdata/Serializable.h"


namespace xdata
{
	// e.g. Histogram("exposer_transferred_bytes_total","Transferred bytes to metrics services", {{"label", "value"}})

	class Histogram: public Collectable
	{
			public:

			using BucketBoundaries = std::vector<double>;


			class Metric
			{
			    public:

				Metric(prometheus::Histogram & c);
				

				//Histogram(const BucketBoundaries& buckets);

				/// \brief Observe the given amount.
				///
				/// The given amount selects the 'observed' bucket. The observed bucket is
				/// chosen for which the given amount falls into the half-open interval [b_n,
				/// b_n+1). The counter of the observed bucket is incremented. Also the total
				/// sum of all observations is incremented.
				void Observe(double value);

				/// \brief Observe multiple data points.
				///
				/// Increments counters given a count for each bucket. (i.e. the caller of
				/// this function must have already sorted the values into buckets).
				/// Also increments the total sum of all observations by the given value.
				void ObserveMultiple(const std::vector<double> bucket_increments, const double sum_of_values);

			    protected:
				prometheus::Histogram & metric_;
			};


			Histogram(const std::string & name, const std::string & help,  const std::map<std::string, std::string>& labels);

			std::string type() const;

			void setValue (const xdata::Serializable& s);

			int equals(const xdata::Serializable & s) const;

			std::string toString() const;

			void fromString(const std::string& value);

			template <typename... Args>
			Metric operator()(const std::map<std::string, std::string>& labels, Args&&... args);


  			prometheus::Family<prometheus::Histogram> &   family_;
  			std::map<size_t, prometheus::Histogram *> labels_;
	};


}

/// \brief Create a histogram with manually chosen buckets.
///
/// The BucketBoundaries are a list of monotonically increasing values
/// representing the bucket boundaries. Each consecutive pair of values is
/// interpreted as a half-open interval [b_n, b_n+1) which defines one bucket.
///
/// There is no limitation on how the buckets are divided, i.e, equal size,
/// exponential etc..
///
/// The bucket boundaries cannot be changed once the histogram is created.
// xdata::Histogram::operator() (const std::map<std::string, std::string>& labels,const BucketBoundaries& buckets);
template <typename... Args>
xdata::Histogram::Metric xdata::Histogram::operator() ( const std::map<std::string, std::string>& labels, Args&&... args)
{
	auto lhash = prometheus::detail::hash_labels(labels);
	//std::cout << "get hash for labels: " << lhash << std::endl;
	if ( labels_.find(lhash) != labels_.end() )
	{
			return xdata::Histogram::Metric(*(labels_[lhash]));
	}
	else
	{
		labels_[lhash] = &(family_.Add(labels, args...));
		return xdata::Histogram::Metric(*(labels_[lhash]));
	}
}
#endif









