// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _prometheus_version_h_
#define _prometheus_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_PROMETHEUS_VERSION_MAJOR 1
#define WORKSUITE_PROMETHEUS_VERSION_MINOR 0
#define WORKSUITE_PROMETHEUS_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_PROMETHEUS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_PROMETHEUS_PREVIOUS_VERSIONS

//
// Template macros
//
#define WORKSUITE_PROMETHEUS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_PROMETHEUS_VERSION_MAJOR,WORKSUITE_XMASLAS2_VERSION_MINOR,WORKSUITE_PROMETHEUS_VERSION_PATCH)
#ifndef WORKSUITE_PROMETHEUS_PREVIOUS_VERSIONS
#define WORKSUITE_PROMETHEUS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_PROMETHEUS_VERSION_MAJOR,WORKSUITE_PROMETHEUS_VERSION_MINOR,WORKSUITE_PROMETHEUS_VERSION_PATCH)
#else 
#define WORKSUITE_PROMETHEUS_FULL_VERSION_LIST  WORKSUITE_PROMETHEUS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_PROMETHEUS_VERSION_MAJOR,WORKSUITE_PROMETHEUS_VERSION_MINOR,WORKSUITE_PROMETHEUS_VERSION_PATCH)
#endif 

namespace prometheus
{
	const std::string project = "worksuite";
	const std::string package  =  "prometheus";
	const std::string versions = WORKSUITE_PROMETHEUS_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Dainius Simelevicius";
	const std::string summary = "XDAQ plugin for Prometheus monitoring";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

