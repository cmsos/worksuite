<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
 <xp:Application heartbeat="false" class="executive::Application" id="0" group="profile" service="executive" network="local" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
   <logUrl xsi:type="xsd:string">console</logUrl>
   <logLevel xsi:type="xsd:string">INFO</logLevel>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libi2outils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>
 <xp:Module>/opt/xdaq/lib/libtcpla.so</xp:Module>
 <xp:Module>/opt/xdaq/lib/libptutcp.so</xp:Module>
 <xp:Application heartbeat="false" class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
   <documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
   <aliasName xsi:type="xsd:string">/directory</aliasName>
   <aliasPath xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}</aliasPath>
   <httpHeaderFields xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[3]">
    <item xsi:type="soapenc:Struct" soapenc:position="[0]">
        <name xsi:type="xsd:string">Access-Control-Allow-Origin</name>
        <value xsi:type="xsd:string">*</value>
    </item>
    <item xsi:type="soapenc:Struct" soapenc:position="[1]">
        <name xsi:type="xsd:string">Access-Control-Allow-Methods</name>
        <value xsi:type="xsd:string">POST, GET, OPTIONS</value>
    </item>
    <item xsi:type="soapenc:Struct" soapenc:position="[2]">
        <name xsi:type="xsd:string">Access-Control-Allow-Headers</name>
        <value xsi:type="xsd:string">x-requested-with</value>
    </item>
   </httpHeaderFields>
   <expiresByType xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[5]">
   <item xsi:type="soapenc:Struct" soapenc:position="[0]">
    <name xsi:type="xsd:string">image/png</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[1]">
    <name xsi:type="xsd:string">image/jpg</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[2]">
    <name xsi:type="xsd:string">image/gif</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[3]">
    <name xsi:type="xsd:string">application/x-shockwave-flash</name>
    <value xsi:type="xsd:string">PT120H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[4]">
    <name xsi:type="xsd:string">application/font-woff</name>
    <value xsi:type="xsd:string">PT8600H</value>
   </item>
   </expiresByType>
         </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>
 <xp:Application heartbeat="false" class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local" logpolicy="inherit" />
 <xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
 <!-- HyperDAQ -->
 <xp:Application heartbeat="false" class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local" logpolicy="inherit"/>
 <xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>
 <!-- XMem probe-->
 <xp:Application class="xmem::probe::Application" id="7" network="local" logpolicy="inherit"/>
 <xp:Module>${XDAQ_ROOT}/lib/libxmemprobe.so</xp:Module>

 <!-- xp:Endpoint protocol="http" service="cgi" hostname="kvm-s3562-1-ip151-69.cms" port="1919" network="prometheus" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000"/-->
 <!-- Prometheus Probe -->
 <xp:Application class="prometheus::Application" id="16" network="prometheus" group="prometheus" service="prometheus" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:prometheus::Application" xsi:type="soapenc:Struct">
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>/nfshome0/lorsini/devel/localbus/worksuite/extern/prometheus-cpp/lib/linux/x86_64_centos7/libprometheus-cpp.so</xp:Module>
 <xp:Module>/nfshome0/lorsini/devel/localbus/worksuite/prometheus/lib/linux/x86_64_centos7/libprometheus.so</xp:Module>
<xp:Application class="prometheus::Example" id="18" network="local" group="example" service="example" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:prometheus::Application" xsi:type="soapenc:Struct">
  </properties>
 </xp:Application>

 <xp:Application class="eventing::core::Publisher" id="64" instance="0" network="localnet" bus="localbus" service="eventing-publisher" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:eventing::core::Publisher" xsi:type="soapenc:Struct">
   <eventings xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[1]">
    <item xsi:type="xsd:string" soapenc:position="[0]">utcp://localhost:2440</item>
   </eventings>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libeventingapi.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libeventingcore.so</xp:Module>


 <!-- Sensor DEAMON -->
<xp:Application heartbeat="true" class="xmas::probe::Application" id="5" network="localnet" group="xmas" service="xmasprobe" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:xmas::probe::Application" xsi:type="soapenc:Struct">
   <autoConfigure xsi:type="xsd:boolean">true</autoConfigure>
   <autoConfSearchPath xsi:type="xsd:string">http://kvm-s3562-1-ip151-69.cms:9966/directory/sensor</autoConfSearchPath>
   <maxReportMessageSize xsi:type="xsd:unsignedInt" >0x100000</maxReportMessageSize>
   <elasticsearchClusterUrl xsi:type="xsd:string">http://cmsos-iaas-cdaq.cms:9200</elasticsearchClusterUrl>
   <dynamicMetadata xsi:type="xsd:boolean">true</dynamicMetadata>
   <tag xsi:type="xsd:string">master</tag>
   <autoTag xsi:type="xsd:string"></autoTag>
   <outputBus xsi:type="xsd:string">localbus</outputBus>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libelasticapi.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasprobe.so</xp:Module>
<xp:Module>${XDAQ_ROOT}/lib/libeventingapi.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libjansson.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libelasticapi.so</xp:Module>


</xp:Profile>
