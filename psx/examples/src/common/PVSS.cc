// $Id $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "PVSS.h"
#include "xgi/Utils.h"

XDAQ_INSTANTIATOR_IMPL(PVSS)

PVSS::PVSS(xdaq::ApplicationStub * s): xdaq::Application(s) 
{	
	getApplicationDescriptor()->setAttribute("icon", "/daq/psx/images/PSX.png");
	//
	// Bind SOAP callback, The namespace http://xdaq.web.cern.ch/xdaq/xsd/2006/psx-10.xsd needs to be given
	//
	xoap::bind(this, &PVSS::dpNotify, "dpNotify", PSX_NS_URI );

	xgi::bind(this,&PVSS::Default, "Default");
	xgi::bind(this,&PVSS::dpGet, "dpGet");
	xgi::bind(this,&PVSS::dpSet, "dpSet");
	xgi::bind(this,&PVSS::dpConnect, "dpConnect");
	xgi::bind(this,&PVSS::dpDisconnect, "dpDisconnect");
	xgi::bind(this,&PVSS::dpGetNames, "dpGetNames");
	xgi::bind(this,&PVSS::dpGetFields, "dpGetFields");
}


//
// SOAP Callback  
//
xoap::MessageReference PVSS::dpNotify (xoap::MessageReference msg) 
{
	std::cout << "Received dpNotify: " << std::endl;
	
	msg->writeTo(std::cout);
	
	std::cout << std::endl;
	
	// reply to caller		
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName( "dpNotifyResponse", "psx", PSX_NS_URI);
	// xoap::SOAPBodyElement e = 
	envelope.getBody().addBodyElement ( responseName );
	return reply;		
}

void PVSS::Default(xgi::Input * in, xgi::Output * out ) 
{
	
	this->showForm(out);

}

void PVSS::dpGet(xgi::Input * in, xgi::Output * out ) 
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string dp = cgi["dp"]->getValue();
		lastDP_ = dp;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("dpGet","psx",PSX_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);

			// add data point name: <psx::dp name="name"/>
			xoap::SOAPName dpName =  env.createName("dp","psx",PSX_NS_URI);
			xoap::SOAPElement dpElement = bodyElem.addChildElement(dpName);
			xoap::SOAPName nameAttribute = env.createName("name","","");
			dpElement.addAttribute(nameAttribute, dp);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor * d = 
				getApplicationContext()->getDefaultZone()->getApplicationDescriptor("pvss", 0);

			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, d);

			reply->writeTo(response_);

			reply->writeTo(std::cout);
			std::cout << std::endl;
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void PVSS::dpSet(xgi::Input * in, xgi::Output * out ) 
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string dp = cgi["dp"]->getValue();
		std::string value = cgi["value"]->getValue();
		lastDP_ = dp;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("dpSet","psx",PSX_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);

			// add data point name: <psx::dp name="name">value</psx::dp>
			xoap::SOAPName dpName =  env.createName("dp","psx",PSX_NS_URI);
			xoap::SOAPElement dpElement = bodyElem.addChildElement(dpName);
			xoap::SOAPName nameAttribute = env.createName("name","","");
			dpElement.addAttribute(nameAttribute, dp);
			dpElement.addTextNode(value);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor * d = 
				getApplicationContext()->getDefaultZone()->getApplicationDescriptor("pvss", 0);

			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, d);

			reply->writeTo(response_);

			reply->writeTo(std::cout);
			std::cout << std::endl;
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void PVSS::dpConnect(xgi::Input * in, xgi::Output * out ) 
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string dp = cgi["dp"]->getValue();
		lastDP_ = dp;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("dpConnect","psx",PSX_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
			
			xoap::SOAPName urlAttribute = env.createName("url","","");
			bodyElem.addAttribute(urlAttribute, this->getApplicationContext()->getContextDescriptor()->getURL());
			
			xoap::SOAPName actionAttribute = env.createName("action","","");
			bodyElem.addAttribute(actionAttribute, this->getApplicationDescriptor()->getURN());

			xoap::SOAPName contextAttribute = env.createName("context","","");
			bodyElem.addAttribute(contextAttribute, this->getApplicationDescriptor()->getURN());

			// add data point name: <psx::dp name="name">value</psx::dp>
			xoap::SOAPName dpName =  env.createName("dp","psx",PSX_NS_URI);
			xoap::SOAPElement dpElement = bodyElem.addChildElement(dpName);
			xoap::SOAPName nameAttribute = env.createName("name","","");
			dpElement.addAttribute(nameAttribute, dp);
			
			
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor * d = 
				getApplicationContext()->getDefaultZone()->getApplicationDescriptor("pvss", 0);

			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, d);

			reply->writeTo(response_);

			reply->writeTo(std::cout);
			std::cout << std::endl;
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void PVSS::dpDisconnect(xgi::Input * in, xgi::Output * out ) 
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string transaction = cgi["transaction"]->getValue();

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("dpDisconnect","psx",PSX_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
			xoap::SOAPName idAttribute = env.createName("id","","");
			bodyElem.addAttribute(idAttribute, transaction);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor * d = 
				getApplicationContext()->getDefaultZone()->getApplicationDescriptor("pvss", 0);

			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, d);

			reply->writeTo(response_);

			reply->writeTo(std::cout);
			std::cout << std::endl;
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void PVSS::dpGetNames(xgi::Input * in, xgi::Output * out ) 
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string pattern = cgi["dp"]->getValue();
		lastDP_ = pattern;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("dpGetNames","psx",PSX_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
			bodyElem.addTextNode(pattern);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor * d = 
				getApplicationContext()->getDefaultZone()->getApplicationDescriptor("pvss", 0);

			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, d);

			reply->writeTo(response_);

			reply->writeTo(std::cout);
			std::cout << std::endl;
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }
}

void PVSS::dpGetFields(xgi::Input * in, xgi::Output * out ) 
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string pattern = cgi["dp"]->getValue();
		lastDP_ = pattern;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("dpGetFields","psx",PSX_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
			bodyElem.addTextNode(pattern);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor * d = 
				getApplicationContext()->getDefaultZone()->getApplicationDescriptor("pvss", 0);

			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, d);

			reply->writeTo(response_);

			reply->writeTo(std::cout);
			std::cout << std::endl;
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }
}


void PVSS::showForm(xgi::Output * out)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	
	*out << cgicc::head() << std::endl;
	
	
	*out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/daq/psx/html/psx.css\">";
	
	*out << cgicc::head() << std::endl;
	

	xgi::Utils::getPageHeader
		(out, 
		"PVSS SOAP Exchange", 
		getApplicationDescriptor()->getContextDescriptor()->getURL(),
		getApplicationDescriptor()->getURN(),
		"/daq/psx/images/PSX.png"
		);

	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("PVSS Access") << std::endl;

	// This method can be invoked using Linux 'wget' command
	// e.g http://lxcmd101:1972/urn:xdaq-application:lid=23/setParameter?value=24
	std::string method = toolbox::toString("/%s/",getApplicationDescriptor()->getURN().c_str());

	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Get Datapoint") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"dpGet") << std::endl;
	*out << cgicc::label("DP Name") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","dp").set("value", lastDP_) << std::endl;
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Set Datapoint") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"dpSet") << std::endl;
	*out << cgicc::label("DP Name") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","dp").set("value", lastDP_) << std::endl;
	*out << cgicc::label("Value") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "10").set("name","value") << std::endl;
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Get Datapoint Names") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"dpGetNames") << std::endl;
	*out << cgicc::label("DP Pattern") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","dp").set("value", lastDP_) << std::endl;
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Connect to Datapoint") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"dpConnect") << std::endl;
	*out << cgicc::label("DP Name") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","dp").set("value", lastDP_) << std::endl;
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Disconnect from Datapoint") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"dpDisconnect") << std::endl;
	*out << cgicc::label("transaction id") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "8").set("name","transaction") << std::endl;
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Get Datapoint Fields") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"dpGetFields") << std::endl;
	*out << cgicc::label("DP Name") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","dp") << std::endl;
	*out << cgicc::input().set("type","submit").set("value","Search")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;

	*out << cgicc::hr() << std::endl;

	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
	*out << cgicc::legend("SOAP Response") << cgicc::p() << std::endl;
	*out << cgicc::textarea().set("rows","20").set("cols","100") << std::endl;

	*out << response_;

	*out << cgicc::textarea() << std::endl;
	*out << cgicc::fieldset() << std::endl;
			
	xgi::Utils::getPageFooter(*out);
	*out <<  cgicc::html() << std::endl;
}
