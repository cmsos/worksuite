// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpGetAllSystemsResult.h"
 
psx::sapi::DpGetAllSystemsResult::DpGetAllSystemsResult(psx::sapi::Request *request) : psx::sapi::Result(request)
{

}


std::set<std::string> &  psx::sapi::DpGetAllSystemsResult::getData()
{
	return sysNames_;
}

