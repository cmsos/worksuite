// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpGetAllSystemsRequest.h"
#include "psx/sapi/ApplicationService.h"
     

psx::sapi::DpGetAllSystemsRequest::DpGetAllSystemsRequest (psx::sapi::ApplicationService * as) : psx::sapi::Request("dpGetAllSystems",as)
{
	pattern_ = "*";
}


void psx::sapi::DpGetAllSystemsRequest::setPattern(const std::string& pattern)
{
        pattern_ = pattern;
}

const char* psx::sapi::DpGetAllSystemsRequest::getPattern()
{
        return pattern_.c_str();
}


