
/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.			                               *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, D. Simelevicius				          *
 *                                                                       *
 * For the licensing terms see LICENSE.		                            *
 * For the list of contributors see CREDITS.   			                   *
 *************************************************************************
 */

#include "psx/sapi/ApplicationService.h"
#include "psx/sapi/DpGetHotlink.h"
#include "psx/sapi/DpSetHotlink.h"
#include "psx/sapi/DpGetResult.h"
#include "psx/sapi/DpGetNamesResult.h"
#include "psx/sapi/DpGetAllSystemsResult.h"
#include "psx/sapi/DpSetResult.h"
#include "psx/sapi/DpSetRequest.h"
#include "psx/sapi/DpGetRequest.h"
#include "psx/sapi/DpGetNamesRequest.h"
#include "psx/sapi/DpGetAllSystemsRequest.h"
#include "psx/sapi/VariableFactory.h"
#include "psx/sapi/RequestFactory.h"
#include "psx/sapi/ResponseFactory.h"

#include "psx/sapi/DpConnectResult.h"
#include "psx/sapi/DpConnectRequest.h"
#include "psx/sapi/DpConnectHotlink.h"
#include "psx/sapi/DpDisconnectResult.h"
#include "psx/sapi/DpDisconnectRequest.h"
#include "psx/sapi/DpDisconnectHotlink.h"
#include "psx/sapi/DpGetFieldsResult.h"
#include "psx/sapi/DpGetFieldsRequest.h"

#include "xoap/domutils.h"
#include "xcept/tools.h"

// PVS includes
//
#include <HotLinkWaitForAnswer.hxx>   
#include <StartDpInitSysMsg.hxx>      
#include <DpMsgAnswer.hxx>            
#include <DpMsgHotLink.hxx>           
#include <DpHLGroup.hxx>   
#include <DpContainer.hxx>            
#include <DpVCItem.hxx>               
#include <ErrHdl.hxx>                 
#include <ErrClass.hxx>               
#include <signal.h>
#include <DpIdentificationResultType.hxx>
#include <DpElementType.hxx>

  
PVSSboolean psx::sapi::ApplicationService::doExit = PVSS_FALSE;
int psx::sapi::ApplicationService::our_argc_ = 0; 
char ** psx::sapi::ApplicationService::our_argv_= 0;

// Receive Signals.
// We are interested in SIGINT and SIGTERM. 
void psx::sapi::ApplicationService::signalHandler(int sig)
{
	if ( (sig == SIGINT) || (sig == SIGTERM) )
	{
		psx::sapi::ApplicationService::doExit = PVSS_TRUE;
	}
	else
	{
		Manager::signalHandler(sig);
	}
}

void psx::sapi::ApplicationService::init(const std::string & projectName, const std::string & projectNum,
const std::string & databaseManager, const std::string & eventManager )
{
	our_argv_ = new char * [10]; // There are 8 arguments
	
	// Executable name
	our_argv_[0] = new char [projectName.size()+1];
	strncpy(our_argv_[0],projectName.c_str(),projectName.size()+1);
	
	unsigned int i = 1;
	
	if ( projectName != "" )
	{
		// Project option
		our_argv_[i] = new char [strlen("-PROJ")+1];
		strcpy(our_argv_[i],"-PROJ");
		i++;
		our_argv_[i] = new char [projectName.size()+1];
		strncpy(our_argv_[i],projectName.c_str(),projectName.size()+1);
                i++;
	}	
	
	if ( projectNum != "" )
	{
		our_argv_[i] = new char [strlen("-num")+1];
		strcpy(our_argv_[i],"-num");
		i++;
		our_argv_[i] = new char [projectNum.size()+1];
		strncpy(our_argv_[i],projectNum.c_str(),projectNum.size()+1);
		i++;
	}
	
	if ( databaseManager != "" )
	{
		our_argv_[i] = new char [strlen("-data")+1];
		strcpy(our_argv_[i],"-data");
		i++;
		our_argv_[i] = new char [databaseManager.size()+1];
		strncpy(our_argv_[i],databaseManager.c_str(),databaseManager.size()+1);
		i++;
	}	
	
	if ( eventManager != "" )
	{
		our_argv_[i] = new char [strlen("-event")+1];
		strcpy(our_argv_[i],"-event");
		i++;
		our_argv_[i] = new char [eventManager.size()+1];
		strncpy(our_argv_[i],eventManager.c_str(),eventManager.size()+1);
		i++;
	}	
	
	our_argv_[i] = 0;
	our_argc_ = i;
		
	Resources::init(our_argc_, our_argv_);
}

void* psx_sapi_application_service_svc(void* arg)
{
	signal(SIGINT,  Manager::sigHdl);
	signal(SIGTERM, Manager::sigHdl);

	psx::sapi::ApplicationService* as = (psx::sapi::ApplicationService*) arg;
	
	as->run(); // never exit until signal tells to exist (CTRL-C, kill SIGTERM)
	Manager::exit(0);
	
	return 0;
}

void psx::sapi::ApplicationService::activate()
{
	pthread_t *thr = new pthread_t();
	pthread_create(thr, 0, psx_sapi_application_service_svc, (void*) this);
	pthread_detach(*thr);
	return;
}
     
psx::sapi::ApplicationService::ApplicationService(psx::PeerTransportService * pts)
	: Manager(ManagerIdentifier(API_MAN, Resources::getManNum())), pts_(pts)
{
	pthread_mutexattr_init(&(this->mutexAttr_));
	pthread_mutexattr_settype(&(this->mutexAttr_), PTHREAD_MUTEX_RECURSIVE_NP);
	pthread_mutex_init(&(this->mutex_),&(this->mutexAttr_));
	
	pthread_mutexattr_init(&(this->lockAttr_));
	pthread_mutexattr_settype(&(this->lockAttr_), PTHREAD_MUTEX_RECURSIVE_NP);
	pthread_mutex_init(&(this->lock_),&(this->lockAttr_));
}

psx::sapi::ApplicationService::~ApplicationService()
{
	pthread_mutexattr_destroy(&(this->mutexAttr_));
	pthread_mutex_destroy(&(this->mutex_));
	
	pthread_mutexattr_destroy(&(this->lockAttr_));
	pthread_mutex_destroy(&(this->lock_));
}

xoap::MessageReference psx::sapi::ApplicationService::onRequest(xoap::MessageReference message) 
{
	// Check if event and data manager are connected
	if ( (!this->isEventManagerConnected()) || (!this->isDataManagerConnected()))
	{
		xoap::MessageReference reply = xoap::createMessage();
        	xoap::SOAPPart soap = reply->getSOAPPart();
       	 	xoap::SOAPEnvelope envelope = soap.getEnvelope();
        	xoap::SOAPBody responseBody = envelope.getBody();
		xoap::SOAPFault f = responseBody.addFault();
		f.setFaultCode("Client");
		f.setFaultString("Cannot perform request, no connection to PVSS system");
		return reply;
	}

	// Serialize SOAp request (one client at the time)
	
	//std::set<std::string> testset = this->getDataPointDefinition("MyRawHtChannel1");

	
	// Received message
	/*
        msg->writeTo(std::cout);
        std::cout << std::endl;
	*/
	psx::sapi::Request* request = 0;
	try
	{
		 request = psx::sapi::RequestFactory::createRequest(message, this);
	}
	catch (psx::sapi::exception::Exception & e )
	{
		xoap::MessageReference reply = xoap::createMessage();
        	xoap::SOAPPart soap = reply->getSOAPPart();
       	 	xoap::SOAPEnvelope envelope = soap.getEnvelope();
        	xoap::SOAPBody responseBody = envelope.getBody();
		xoap::SOAPFault f = responseBody.addFault();
		f.setFaultCode("Client");
		f.setFaultString(xcept::stdformat_exception_history(e));
		return reply;
			
	}
	
	//pthread_mutex_lock( &(this->lock_));
	
	time_t timeout = 30; //  seconds timeout
	
	try
	{
		this->submit(request);
	}
	catch (	psx::sapi::exception::Exception & e)
	{
		pthread_mutex_unlock( &(this->lock_));

		xoap::MessageReference reply = xoap::createMessage();
        	xoap::SOAPPart soap = reply->getSOAPPart();
       	 	xoap::SOAPEnvelope envelope = soap.getEnvelope();
        	xoap::SOAPBody responseBody = envelope.getBody();
		xoap::SOAPFault f = responseBody.addFault();
		f.setFaultCode("Server");
		f.setFaultString(xcept::stdformat_exception_history(e));
		return reply;
	}

	//pthread_mutex_unlock( &(this->lock_));
	
	//std::cout << "Going to wait.. " << std::endl;
	psx::sapi::Result * result = this->wait(request, timeout);	
	
	xoap::MessageReference response;
	
	try
	{
		 response = psx::sapi::ResponseFactory::createResponse(result);
	}
	catch (psx::sapi::exception::Exception & e )
	{

		xoap::MessageReference reply = xoap::createMessage();
        	xoap::SOAPPart soap = reply->getSOAPPart();
       	 	xoap::SOAPEnvelope envelope = soap.getEnvelope();
        	xoap::SOAPBody responseBody = envelope.getBody();
		xoap::SOAPFault f = responseBody.addFault();
		f.setFaultCode("Server");
		f.setFaultString(xcept::stdformat_exception_history(e));
		return reply;	
	}

	return response;
}





void psx::sapi::ApplicationService::submit(psx::sapi::Request* request) 
{
	pthread_mutex_lock( &(this->mutex_));

	if (request->type() == "dpGet")
	{
		DpIdentifier dpId;
		DpIdentList dpl;

		psx::sapi::DpGetRequest* rqst = dynamic_cast<psx::sapi::DpGetRequest*>(request);
		std::set<std::string>& s = rqst->getDpNames();
		std::set<std::string>::iterator i;
	
		// build the data point id list
		for (i = s.begin(); i != s.end(); i++)
		{

			if (Manager::getId( (*i).c_str(), dpId) == PVSS_FALSE)
			{
			
				std::string msg = "cannot map data point name: ";
				msg += (*i).c_str();
				msg += " to data point id";
				pthread_mutex_unlock( &(this->mutex_));	
				XCEPT_RAISE(psx::sapi::exception::Exception, msg);		
			}
			else
			{
				dpl.append(dpId);
			}
		}
	
		// Create a callback object and associate it with the request
		psx::sapi::Hotlink* hotlink = new psx::sapi::DpGetHotlink(request);
		// hotlink object is automatically deleted by PVSS
		if ( Manager::dpGet(dpl, hotlink, PVSS_TRUE) == PVSS_FALSE )
		{
			//delete hotlink;
			pthread_mutex_unlock( &(this->mutex_));	
			XCEPT_RAISE(psx::sapi::exception::Exception, "failed to send dpGet ");	
		
		}
	}
	else if (request->type() == "dpGetAsynch")
	{
		std::string msg = "failed to send dpGetAsync";
		msg += ", not implemented";
		pthread_mutex_unlock( &(this->mutex_));	
		XCEPT_RAISE(psx::sapi::exception::Exception, msg);		
	}
	else if (request->type() == "dpSet")
	{
		DpIdentifier dpId;
		DpIdValueList dpvl;
		psx::sapi::DpSetRequest* rqst = dynamic_cast<psx::sapi::DpSetRequest*>(request);
		
		std::map<std::string, std::string, std::less<std::string> > & m = rqst->getDpValues();
		std::map<std::string, std::string, std::less<std::string> >::iterator i;
	
		
		// build the data point id list
		for (i = m.begin(); i != m.end(); i++)
		{
			
			if (Manager::getId( (*i).first.c_str(), dpId) == PVSS_FALSE)
			{
				std::string msg = "cannot map data point name: ";
				msg += (*i).first.c_str();
				msg += " to data point id";
				pthread_mutex_unlock( &(this->mutex_));	
				XCEPT_RAISE(psx::sapi::exception::Exception, msg);			
			}
			else
			{
                                // Now a variable of the correct type
                                // needs to be created. In case we're
                                // trying to set the actual value of
                                // the data point (i.e., the data
                                // point name ends in '._value'), we
                                // determine the variable type
                                // dynamically from the element
                                // type. The element attributes (e.g.,
                                // data point names ending
                                // '._exp_inv', '._userbit1', etc.)
                                // have fixed types, which we
                                // determine from the attribute
                                // variable type.

				Variable * v;
				DpElementType dpt = DPELEMENT_NOELEMENT;
                                VariableType vt = NO_VAR;
                                std::string const dpName = (*i).first;
                                size_t const dotPos = dpName.find_last_of(".");
                                std::string attributeName = "";
                                if (dotPos != std::string::npos)
                                  {
                                    attributeName = dpName.substr(dotPos + 1);
                                  }
				if (attributeName == "_value")
                                  {
                                    // Setting the real value:
                                    // determine the required variable
                                    // type from the element type.
                                    Manager::getElementType(dpId, dpt);
                                    vt = DpElement::getVariableType(dpt);
                                  }
                                else
                                  {
                                    // We're setting an attribute:
                                    // determine the required variable
                                    // type from the attribute type.
                                    Manager::getDpIdentificationPtr()->getAttributeType(dpId, vt);
                                  }

				try {
					 v = psx::sapi::VariableFactory::createVariable(vt, (*i).second );
				}
				catch (psx::sapi::exception::Exception & e)
				{
					std::string msg = "dpSet failed, cannot create variable for data point name: ";
					msg += (*i).first.c_str();
					pthread_mutex_unlock( &(this->mutex_));	
					XCEPT_RETHROW(psx::sapi::exception::Exception, msg, e);	
				}
				
					
				dpvl.appendItem(dpId, *v);
				delete v;
				
			}
		}
	
		// Create a callback object and associate it with the request
		psx::sapi::Hotlink* hotlink = new psx::sapi::DpSetHotlink(request);
		// hotlink object is automatically deleted by PVSS
		if ( Manager::dpSet(dpvl, hotlink, PVSS_TRUE) == PVSS_FALSE )
		{
			//delete hotlink;
			pthread_mutex_unlock( &(this->mutex_));	
			XCEPT_RAISE(psx::sapi::exception::Exception, "failed to send dpSet");		
		
		}
	}
	else if (request->type() == "dpGetNames")
	{
		//		
		psx::sapi::DpGetNamesRequest* rqst = dynamic_cast<psx::sapi::DpGetNamesRequest*>(request);
				
		DpIdentification * idtf = Manager::getDpIdentificationPtr();
		DpIdentList dpl;
		
		// TBD: check idResult for any kind of error conditions
		//
		DpIdentificationResult idResult = idtf->getIdSet( rqst->getPattern() , dpl);
		if ( idResult != DpIdentOK )
		{
			const char * msg = DpIdentification::getErrorMsg(idResult);
			XCEPT_RAISE(psx::sapi::exception::Exception, msg);		
		}
		
		const DpIdentifier * current = dpl.getFirst();
				
		psx::sapi::DpGetNamesResult * result = new psx::sapi::DpGetNamesResult(rqst);

		std::set<std::string>& names = result->getData();
		while(current != 0 )
		{
			char * dpname;
			Manager::getName(*current, dpname);
			names.insert(dpname);
			current = dpl.getNext();
			delete [] dpname;
		}
		//
		
		this->acknowledge(result);
	}
	else if (request->type() == "dpConnect")
	{
		DpIdentifier dpId;
		DpIdentList dpl;

		psx::sapi::DpConnectRequest* rqst = dynamic_cast<psx::sapi::DpConnectRequest*>(request);
		std::set<std::string>& s = rqst->getDpNames();
		std::set<std::string>::iterator i;
	
		for (i = s.begin(); i != s.end(); i++)
		{
			if (Manager::getId( (*i).c_str(), dpId) == PVSS_FALSE)
			{
				std::string msg = "cannot map data point name: ";
				msg += (*i).c_str();
				msg += " to data point id";
				pthread_mutex_unlock( &(this->mutex_));	
				XCEPT_RAISE(psx::sapi::exception::Exception, msg);			
			}
			else
			{
				dpl.append(dpId);
			}
		}
	
		psx::sapi::Hotlink* hotlink = new psx::sapi::DpConnectHotlink(request);
		if ( Manager::dpConnect(dpl, hotlink, PVSS_TRUE) == PVSS_FALSE )
		{
			//delete hotlink;
			pthread_mutex_unlock( &(this->mutex_));	
			XCEPT_RAISE(psx::sapi::exception::Exception, "failed to send dpConnect");				
		}		
	}
	else if (request->type() == "dpDisconnect")
	{
		DpIdentifier dpId;
		DpIdentList dpl;

		psx::sapi::DpDisconnectRequest* rqst = dynamic_cast<psx::sapi::DpDisconnectRequest*>(request);
		std::string transactionId = rqst->getTransactionId();
		
		if ( ! this->hasTransaction(transactionId) )
		{
			pthread_mutex_unlock( &(this->mutex_));	
			std::string msg = "transaction id:";
			msg += transactionId;
			msg += ", not found";
			XCEPT_RAISE(psx::sapi::exception::Exception, msg);	
		}
		else
		{
			psx::sapi::Hotlink * hotlink = this->getTransaction(transactionId);
			psx::sapi::Request * originalRequest = hotlink->getRequest();
			std::set<std::string>& s = dynamic_cast<psx::sapi::DpConnectRequest*>(originalRequest)->getDpNames();
			std::set<std::string>::iterator i;

			for (i = s.begin(); i != s.end(); i++)
			{
				if (Manager::getId( (*i).c_str(), dpId) == PVSS_FALSE)
				{
					std::string msg = "cannot map data point name: ";
					msg += (*i).c_str();
					msg += " to data point id";
					pthread_mutex_unlock( &(this->mutex_));	
					XCEPT_RAISE(psx::sapi::exception::Exception, msg);	
				}
				else
				{
					dpl.append(dpId);
				}
			}
		
			
			if ( Manager::dpDisconnect(dpl, hotlink) == PVSS_FALSE )
			{
				pthread_mutex_unlock( &(this->mutex_));	
				XCEPT_RAISE(psx::sapi::exception::Exception, "failed tp send dpDisconnect");		

			}
			
			// The origincal DpConnectRequest will be deleteed by the DTOR of the hotlink object  !!!!!
			// The transactio identifier is also cleared in the DTOR of the holink object				
			
	
		}

		if (rqst->getAck())
		{
			psx::sapi::DpDisconnectResult * result = new psx::sapi::DpDisconnectResult(rqst);
			this->acknowledge(result);
		}
	}	
	else if (request->type() == "dpGetFields")
        {
                //              
                psx::sapi::DpGetFieldsRequest* rqst = dynamic_cast<psx::sapi::DpGetFieldsRequest*>(request);
                std::string name = rqst->getName();

                psx::sapi::DpGetFieldsResult * result = new psx::sapi::DpGetFieldsResult(rqst);
		std::set<std::string>  fields;
		try
		{
	        	fields = this->getDataPointDefinition(name);
                }
		catch (psx::sapi::exception::Exception & e)
		{
			delete result;
			pthread_mutex_unlock( &(this->mutex_));	
			XCEPT_RETHROW(psx::sapi::exception::Exception, "failed to retrieve data point fields", e);
		}
		
		result->getData() = fields; 
		this->acknowledge(result);        

	 }	
	else if ( request->type() == "dpGetAllSystems")
	{
		//
                psx::sapi::DpGetAllSystemsRequest* rqst = dynamic_cast<psx::sapi::DpGetAllSystemsRequest*>(request);
		std::string pattern = rqst->getPattern();
		psx::sapi::DpGetAllSystemsResult * result = new psx::sapi::DpGetAllSystemsResult(rqst);

        	DpIdentification* dpi = Manager::getDpIdentificationPtr();
        	SystemNumType *sysIds;
        	CharString *sysNames;
		PVSSlong howManyRef;
		DpIdentification::DpIdNamePair * pairArrRef;

        	size_t count;

        	dpi->getAllSystems(sysIds,sysNames,count);

		std::set<std::string>& names = result->getData();
	
		SystemNumType defSys = dpi->getDefaultSystem();
        	for(size_t i = 0; i<count; ++i)
        	{
			dpi->setDefaultSystem(sysIds[i]);
			dpi->getSortedDpIdNames(pattern.c_str(), pairArrRef, howManyRef);

			for(int j = 0; j<howManyRef; ++j)
      			{
      				CharString fullname = (CharString) pairArrRef[j].text.getText();
				for(size_t k = 0; k<count; ++k)
      				{
                 
            				if(fullname.indexOf(sysNames[k].c_str()) > -1 )
            				{
                				names.insert(sysNames[k].c_str());
            					break;
            				}
      				}
				
        		}
		}

        	if(count > 0)
        	{
                	delete[] sysIds;
                	delete[] sysNames;
        	}

		dpi->setDefaultSystem(defSys);

                this->acknowledge(result);

	}
	else
	{
		pthread_mutex_unlock( &(this->mutex_));	
		XCEPT_RAISE(psx::sapi::exception::Exception, "unknown command");		
	}
	
	pthread_mutex_unlock( &(this->mutex_));	
}

void psx::sapi::ApplicationService::acknowledge(psx::sapi::Result* result)
{

	requestCompletionQueue_.push(result);
}


// void psx::sapi::ApplicationService::notify(psx::sapi::Result* result)
void psx::sapi::ApplicationService::notify(psx::sapi::Result* result)
{
	
	psx::sapi::DpConnectResult* r = dynamic_cast<psx::sapi::DpConnectResult*>(result);
	
	// Create SOAP reply
	xoap::MessageReference notify = xoap::createMessage();
	xoap::SOAPPart soap = notify->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	xoap::SOAPName notifyCommand = envelope.createName("dpNotify","psx", psx::sapi::NSURI);
	xoap::SOAPElement responseElement = responseBody.addBodyElement(notifyCommand);

	xoap::SOAPName transactionId = envelope.createName("id","","");
  	responseElement.addAttribute(transactionId, r->getTransactionId() );
	
	std::string context = dynamic_cast<psx::sapi::DpConnectRequest*>(result->getRequest())->getContext();
	xoap::SOAPName contextName = envelope.createName("context","","");
  	responseElement.addAttribute(contextName, context);
	
	std::map<std::string, std::string, std::less<std::string> > & dpValues = r->getData();
	for ( std::map<std::string, std::string, std::less<std::string> >::iterator i =  dpValues.begin(); i !=  dpValues.end(); i++ )
	{
		//std::cout <<  "Retrieved dpName: " << (*i).first << " value: " << (*i).second << std::endl;
		xoap::SOAPName dpTag = envelope.createName("dp","psx", psx::sapi::NSURI);
		xoap::SOAPElement dpElement = responseElement.addChildElement(dpTag);
		xoap::SOAPName dpName = envelope.createName("name");
		dpElement.addAttribute(dpName, (*i).first);
		dpElement.addTextNode((*i).second);

	}
	
	std::string action = dynamic_cast<psx::sapi::DpConnectRequest*>(result->getRequest())->getAction();
	std::string url = dynamic_cast<psx::sapi::DpConnectRequest*>(result->getRequest())->getURL();
	
	notify->getMimeHeaders()->setHeader("SOAPAction", action);
	
	
	//std::cout << std::endl;
	//notify->writeTo(std::cout);
        //std::cout << std::endl;

	try
	{
		xoap::MessageReference reply = pts_->post(url, notify);
		xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
		if ( b.hasFault() )
		{
			xoap::SOAPFault f = b.getFault();
			std::cerr << "failed to notify to URL:" << url << ", " << f.getFaultString() << std::endl;		
		}
	}
	catch (psx::exception::Exception & e)
	{
		std::cerr << "failed to notify to URL:" << url << ", " << xcept::stdformat_exception_history(e) << std::endl;
	}
	
	
	delete result;

}

psx::sapi::Result* psx::sapi::ApplicationService::wait(psx::sapi::Request* request, time_t sec)
{
	try
	{
		psx::sapi::Result* result  = 0;
		do 
		{
			result = requestCompletionQueue_.pop(sec, 0);
		}
		while(result->getRequest()->getUUID() != request->getUUID());
		return result;

	}
	catch (toolbox::exception::Timeout& e)
	{
		// Timeout, return empty result
		//e.what();
		return 0;
	}
}


void psx::sapi::ApplicationService::run()
{
	long sec, usec;

	pthread_mutex_lock( &(this->mutex_));
	// First connect to Data manager.
	// We want Typecontainer and Identification so we can resolve names
	// This call succeeds or returns false
	// last parameter PVSS_TRUE ensures that we go on
	if (!connectToData(
		StartDpInitSysMsg::TYPE_CONTAINER | StartDpInitSysMsg::DP_IDENTIFICATION,
		0,
		PVSS_TRUE
		)
	    )
	{
		// There was an error
		std::cout << "Failed to connect to database manager" << std::endl;
	}
	
	// While we are in STATE_INIT we are initialized by the Data manager
	while (getManagerState() == STATE_INIT)
	{
		// Wait max. 1 second in select to receive next message from data.
		// It won't take that long...
		sec = 1;
		usec = 0;
		dispatch(sec, usec);
	}

	// We are now in STATE_ADJUST and can connect to Event manager
	// This call will succeed or return false
	// Last parameter PVSS_TRUE ensures that we're going on
		
	if (!connectToEvent(PVSS_TRUE))
	{
		// There was an error
		std::cout << "Failed to connect to event manager" << std::endl;
	}

 	pthread_mutex_unlock( &(this->mutex_));
	
	// Now loop until we are finished
	while (1)
	{
		// Exit flag set by signal handler?
		if (doExit)
		{
			return;
		}

		// Wait 100 ms
		sec = 0;
		usec = 10000;
		//pthread_yield();
		//pthread_mutex_lock( &(this->mutex_));
		dispatch(sec, usec);
		//pthread_mutex_unlock( &(this->mutex_));
		//std::cout << "done dispatch" << std::endl;
		//pthread_yield();
	}
}

void psx::sapi::ApplicationService::setTransaction( const std::string& id, psx::sapi::Hotlink* obj)
{
	subscribers_[id] = obj;
}


void psx::sapi::ApplicationService::clearTransaction (const std::string& id)
{
	if (subscribers_.find(id) != subscribers_.end())
	{
		subscribers_.erase(id);
	}
}

bool psx::sapi::ApplicationService::hasTransaction (const std::string& id)
{
	return (subscribers_.find(id) != subscribers_.end() );
}

std::vector<std::string> psx::sapi::ApplicationService::getTransactions()
{
	std::vector<std::string> v;
	std::map<std::string, psx::sapi::Hotlink*>::iterator i;
	
	for (i = subscribers_.begin(); i != subscribers_.end(); ++i)
	{
		v.push_back( (*i).first );
	}
	return v;
}

std::string psx::sapi::ApplicationService::getTransactionUrl(const std::string& id)
	
{
	if (this->hasTransaction(id))
	{
		psx::sapi::Hotlink* h = subscribers_[id];
		psx::sapi::DpConnectRequest* r = dynamic_cast<psx::sapi::DpConnectRequest*>(h->getRequest());
		if (r != 0)
		{
			return r->getURL();
		}
		else
		{
			std::string msg = "Failed to get URL for transaction id ";
			msg += id;
			msg += ", id exists but is not for a datapoint connection";
			XCEPT_RAISE(psx::sapi::exception::Exception, msg);
		}
	}
	else
	{
		std::string msg = "Failed to get URL for non-existing transaction id ";
		msg += id;
		XCEPT_RAISE(psx::sapi::exception::Exception, msg);
	}
}

psx::sapi::Hotlink* psx::sapi::ApplicationService::getTransaction(const std::string& id) 
{
	std::map<std::string, psx::sapi::Hotlink*>::iterator i;
	i = subscribers_.find(id);
	if (i != subscribers_.end())
	{
		return (*i).second;
	}
	else
	{
		XCEPT_RAISE(psx::sapi::exception::Exception, "cannot find transaction id");
	}
}

std::set<std::string> psx::sapi::ApplicationService::getDataPointDefinition( const std::string & name) 
{
	DpIdentification * idtf = Manager::getDpIdentificationPtr();
	
	
	DpIdentList dpl;
	DpIdentificationResult idResult = idtf->getIdSet( name.c_str() , dpl);
	if ( idResult != DpIdentOK )
	{
			const char * msg = DpIdentification::getErrorMsg(idResult);
			XCEPT_RAISE(psx::sapi::exception::Exception, msg);		
	}
	
	const DpIdentifier * identifier = dpl.getFirst();
	DpTypeContainer* container = Manager::getTypeContainerPtr();
	if (container == 0)
	{
		XCEPT_RAISE(psx::sapi::exception::Exception, "failed to get type container");
	}

	if ( identifier == 0 )
	{
		XCEPT_RAISE(psx::sapi::exception::Exception, "failed to get identifier");

	}

	DpTypeId dptid = identifier->getDpType();
	CharString typeName;
	Manager::getTypeName (dptid, typeName);
	DpType* dpType = container->getTypePtr(dptid);
	const DpTypeNode * dptn = dpType->getRootTypeNodePtr();

	std::set<std::string> names;
	// Recursive function
	this->addNodeDefinitionToList(identifier, dpType,dptid,idtf,container,dptn,names,name);
	return names;
}

void psx::sapi::ApplicationService::addNodeDefinitionToList(const DpIdentifier * identifier,
								DpType* dpType, 
								DpTypeId dptid, 
								DpIdentification * idtf, 
							    DpTypeContainer* container, 
							    const DpTypeNode * dptn, 
							    std::set<std::string> & names,  const std::string & fathername)
{	
	DynPtrArrayIndex num = dptn->getSonNumber();
	for (DynPtrArrayIndex i = 0; i < num; i++)
	{
		DpElementId eid = dptn->getSon(i);
		DpTypeNode* child = dpType->getTypeNodePtr(eid);
		//char* ename;
		CharString ename;
		idtf->getElementName(dptid, eid, ename);
			
		std::string newname = fathername;
		newname += ".";
		newname += ename;
		if ( child->isLeaf() )
		{
			names.insert(newname);
		}
		else
		{	
			// recursively
			this->addNodeDefinitionToList(identifier, dpType, dptid, idtf, container, child, names, newname);
		}	
		
	}
}	

psx::PeerTransportService * psx::sapi::ApplicationService::getPeerTransportService()
{
	return pts_;
}

bool psx::sapi::ApplicationService::isDataManagerConnected()
{
	return this->isDataConnOpen();
}

bool psx::sapi::ApplicationService::isEventManagerConnected()
{
	return this->isEvConnOpen();
}

