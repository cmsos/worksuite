// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpGetRequest.h"
#include "psx/sapi/ApplicationService.h"
     

psx::sapi::DpGetRequest::DpGetRequest (psx::sapi::ApplicationService * as) : psx::sapi::Request("dpGet",as)
{

}

std::set<std::string>  & psx::sapi::DpGetRequest::getDpNames()
{
	return dpnames_;
}

void psx::sapi::DpGetRequest::addDpName(std::string & name)
{
	dpnames_.insert(name);
}


