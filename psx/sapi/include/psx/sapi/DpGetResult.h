// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DbGetResult_h_
#define _psx_sapi_DbGetResult_h_

#include <string>
#include <map>

#include "psx/sapi/Result.h"
     
namespace psx {
	namespace sapi {
	
		
		class DpGetResult : public psx::sapi::Result
		{
      			public:
			
			DpGetResult(psx::sapi::Request * request);
		
			
			std::map<std::string, std::string, std::less<std::string> > &  getData();
			
			
			private:
			
			std::map<std::string, std::string, std::less<std::string> > dpValues_;
		};
	}
}

#endif 

