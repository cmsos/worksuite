// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2010, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini and R. Moser			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_version_h_
#define _psx_sapi_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_PSXSAPI_VERSION_MAJOR 5
#define WORKSUITE_PSXSAPI_VERSION_MINOR 0
#define WORKSUITE_PSXSAPI_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_PSXSAPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef	WORKSUITE_PSXSAPI_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_PSXSAPI_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_PSXSAPI_VERSION_MAJOR,WORKSUITE_PSXSAPI_VERSION_MINOR,WORKSUITE_PSXSAPI_VERSION_PATCH)
#ifndef WORKSUITE_PSXSAPI_PREVIOUS_VERSIONS
#define WORKSUITE_PSXSAPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_PSXSAPI_VERSION_MAJOR,WORKSUITE_PSXSAPI_VERSION_MINOR,WORKSUITE_PSXSAPI_VERSION_PATCH)
#else 
#define WORKSUITE_PSXSAPI_FULL_VERSION_LIST  WORKSUITE_PSXSAPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_PSXSAPI_VERSION_MAJOR,WORKSUITE_PSXSAPI_VERSION_MINOR,WORKSUITE_PSXSAPI_VERSION_PATCH)
#endif 
namespace psxsapi
{
	const std::string project = "worksuite";
	const std::string package  =  "psxsapi";
	const std::string versions = WORKSUITE_PSXSAPI_FULL_VERSION_LIST;
	const std::string summary = "Interface to native PVSS API";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/PSX";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
