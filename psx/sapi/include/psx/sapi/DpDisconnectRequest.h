// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DpDisconnectRequest_h_
#define _psx_sapi_DpDisconnectRequest_h_

#include <string>
#include <set>

#include "psx/sapi/Request.h"
     
namespace psx {
	namespace sapi {
	
		class ApplicationService;
		
		class DpDisconnectRequest : public psx::sapi::Request
		{
      			public:
			
			DpDisconnectRequest( psx::sapi::ApplicationService * as);

			void setURL(const std::string & url );
			
			void setContext(const std::string & context);
			
			std::string getURL();
			
			std::string getContext();
			
			void setTransactionId(const std::string & id);
			
			std::string &  getTransactionId();
						
			private:
			
			std::set<std::string> dpnames_;
			std::string context_;
			std::string url_;
			std::string id_; // transaction id
		
		};
	}
}

#endif 

