// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_ApplicationService_h_
#define _psx_mapi_ApplicationService_h_

#include <string>
#include <map>
#include <iostream>
#include <signal.h>
#include <errno.h>

#include <limits.h>
#include "toolbox/SyncQueue.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"

#include "psx/PeerTransportService.h"
#include "psx/ApplicationService.h"
#include "psx/mapi/exception/Exception.h"
#include "psx/mapi/Notification.h"
    
namespace psx {
	namespace mapi {
	
		class Monitor;
	
		const std::string NSURI = "http://xdaq.web.cern.ch/xdaq/xsd/2006/psx-smi-10.xsd";
		
		/*! Class that implements the PVSS API Manager to access data points
		*/
		class ApplicationService: public psx::ApplicationService
		{
      			public:
			
			ApplicationService(psx::PeerTransportService * pts);
			
			virtual ~ApplicationService();
			
			//! Process an incoming SOAP request
			//
			virtual xoap::MessageReference onRequest(xoap::MessageReference msg)
				;
					
			virtual psx::PeerTransportService * getPeerTransportService();

			/*! Initialization reading the commandline parameters or configuration file information
			 *  This must be done only once
			 */
			static void init(const std::string & dns);
			
			//! Retrieves the list of all transaction identifiers
			//
			std::vector<std::string> getTransactions();
			
			/*! Retrieves the URL for which a transaction identifier has been set
			 *  \throws an exception if the \param id does not exist
			*/
			std::string getTransactionUrl(const std::string& id)
				;
				
			std::string getTransactionOwner(const std::string& id)
				;
			
			void setTransaction( const std::string& id, psx::mapi::Monitor* obj);

			void clearTransaction (const std::string& id);

			bool hasTransaction (const std::string& id);

			psx::mapi::Monitor* getTransaction(const std::string& id) ;
			
			void notify(const std::string& object,
					   const std::string& url,	
					   const std::string& action,	
					   const std::string& context,	
					   const std::string& id,	
					   const std::string& state
					   );
			//! Create the dispatching thread that communicated with PVSS and call \function svc() internally
			//
			void activate(); 
			
			//! Called in the svc thread routine when \function activate() is called from outside
			//
			void run();
			
			//! \returns true if the DNS is reachable, false otherwise
			//
			bool isConnected();
			
			private:			
			
			// Used to send SOAP commands to other peers
			psx::PeerTransportService * pts_;
			
			// Map of monitored SMI objects
			std::map<std::string, psx::mapi::Monitor*> subscribers_;
			
			// Holds the results of completed requests
			toolbox::SyncQueue<psx::mapi::Notification*> notificationQueue_;	
			
			// Mutex used to serialize SOAP requests
			pthread_mutex_t lock_;
			pthread_mutexattr_t lockAttr_;		
		};
	}
}

#endif 

