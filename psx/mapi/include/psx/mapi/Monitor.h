// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_Monitor_h_
#define _psx_mapi_Monitor_h_


#include <iostream>
#include <string>

#include <stdlib.h>
#include "smiuirtl.hxx"
#include "smixx_common.hxx"

#include <ctype.h>

#include "psx/mapi/exception/Exception.h"

namespace psx {
	namespace mapi {

		class ConnectRequest;

		class Monitor: public SmiObject
		{    
			public:
    
    			Monitor(psx::mapi::ConnectRequest* request);
			~Monitor();
			
			void connect() ;
			
			//! \returns the object identifier (stringified this pointer)
			//
			std::string getId();
			
			//! \returns the URL to which to send notify messages
			//
			std::string getURL();
			
			//! \returns the owner or empty string if no owner
			//
			std::string getOwner();

			protected:
			
			void smiStateChangeHandler();
    			void smiExecutingHandler();
			
			psx::mapi::ConnectRequest* request_;
			//SmiDomain* domain_;
			std::string id_; // object identifier as string -> used as transaction id in async protocol		
		};
	}
}

#endif
