// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/ManualRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::ManualRequest::ManualRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("manual",as)
{
	owner_ = "";
}

std::string psx::mapi::ManualRequest::formatObjectName()
{
	 std::string format = "";
        if (domain_ != "")
        {
                format = domain_;
        }
        else
        {
                format = name_;
        }

        format += "::";
        format += name_;
        format += "_FWM";
        return format;
}


std::string psx::mapi::ManualRequest::formatCommand()
{
	std::string format = "MANUAL";
	
	
	if (owner_ != "")
	{
		format += "/OWNER=\"";
		format += owner_;
		format += "\"";
	}
	
	return format;
}



