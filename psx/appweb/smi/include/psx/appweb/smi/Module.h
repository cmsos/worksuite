// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_appweb_smi_Module_h_
#define _psx_appweb_smi_Module_h_

#include <string>

#include "appWeb/appWeb.h"

#include "psx/appweb/smi/HandlerService.h"


extern "C" {
    extern int mprPSXSMIInit(void *handle);
};
    
namespace psx {
	namespace appweb {
		namespace smi {
			class Module : public MaModule 
			{
			      public:
				Module(void *handle);
				~Module();
				int parseConfig(char *key, char *value, MaServer *server,
		        			MaHost *host, MaAuth *auth, MaDir* dir, 
						MaLocation *location);
				int start();
				void stop();

			     private:

			     psx::appweb::smi::HandlerService *service_;
			     std::string dns_;
			};
		}	

	}
}
#endif 
