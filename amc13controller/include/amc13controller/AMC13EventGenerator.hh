#ifndef __AMC13EventGenerator
#define __AMC13EventGenerator

#include "amc13controller/EventGenerator.hh"

#define MAX_EVENT_DESCRIPTORS 1024

namespace amc13controller
{
    class AMC13EventGenerator : public EventGenerator {

    public: 
        AMC13EventGenerator( HAL::HardwareDeviceInterface *amc13,                             
                             Logger logger );
        virtual ~AMC13EventGenerator() {};
    protected:
        virtual void checkParams( uint32_t &nevt, uint32_t streamNo );
        virtual void writeDescriptors( uint32_t streamNo );

    private:
        HAL::HardwareDeviceInterface *amc13_P;
    };
}
#endif /* __AMC13EventGenerator */
