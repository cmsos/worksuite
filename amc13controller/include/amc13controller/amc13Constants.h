#ifndef __amc13Constants
#define __amc13Constants

//#define NB_STREAMS 0x04 //defines the total number of available streams on the amc13
#define NB_INPUT_STREAMS 0x0c //12 input streams
#define NB_OUTPUT_STREAMS 0x03 //3 output streams

//JRF TODO, do we need these?
#define AMC13_VENDORID 0xecd6
#define AMC13_DEVICEID 0xFEA1

//#define MOL_VENDORID 0xecd6
//#define MOL_DEVICEID 0x1124

#define AMC13_ADDRESSTABLE_FILE "/opt/xdaq/htdocs/amc13/html/AMC13AddressTable.dat"

//For firmware versions:
//http://ohm.bu.edu/~dzou/firmware.cgi#xg_cms_10g
//https://twiki.cern.ch/twiki/bin/viewauth/CMS/AMC13
#define MIN_T1_FIRMWARE 0x2255
#define MIN_T2_FIRMWARE 0x2e

//#define AMC13_DAQ_FIRMWARE_TYPE 0xfea
//#define AMC13_SLINKEXPRESS_FIRMWARE_TYPE 0xfea // now there is one firmware for slink and slinkexpress operation. Not used anymore.

//#define AMC13_DAQ_FIRMWARE_MIN_VERSION 0x0016            // min firmware version
//#define AMC13_SLINKEXPRESS_FIRMWARE_MIN_VERSION 0x0000 // irrelevant since only fea is in use: min firmware version for SLINKExpress

// Operation modes of the AMC13
#define AMC13_MODE_NORMAL "AMC13_MODE_NORMAL" // Operating the AMC13 as a noraml AMC13 with real leaf cards attached and output SFPs connected to Ferols/Ferol40s
#define AMC13_MODE_EFED "AMC13_MODE_EFED" // Operating the AMC13 as an EFED, generating data on board
#define AMC13_MODE_MONITOR "AMC13_MODE_MONITOR" // Operating the AMC13 in read only mode, no configuration is done, only monitoring, this is to allow DAQ to spy/monitor sub-det AMC13s without upsetting the configurations
#define AMC13_MODE_NO_DAQ "AMC13_MODE_NO_DAQ" // Operating the AMC13 in a mode akin to Pixel where the AMC13 passes only TTC and TTS signals, but does not output any DATA to DAQ. the data are output directly from the leaf cards. This mode is also used to drive Ferol40 sync gen mode.
#define AMC13_MODE_LEAF "AMC13_MODE_LEAF" //Special mode of operation that requires a specific Firmware. In this mode, AMC13s do not output data over SLink, but rather act as leaf sources sending their data to the AMC13 in the hub slot. 

// Data Source for the various operation modes
// JRF TODO, remove these, the modes are all contained above now. 
//#define GENERATOR_SOURCE "GENERATOR_SOURCE"
//#define L10G_SOURCE "L10G_SOURCE"
//#define L10G_CORE_GENERATOR_SOURCE "L10G_CORE_GENERATOR_SOURCE"

// When running with a 'real' input (i.e. not with an internal event generator)
// the input can be connected to the FED or to an event generator in the core.
// the event generator of the core is controlled by the Ferol40.
//#define SLINK_FED_MODE                "SLINK_FED_MODE"             // used to read data from the fed (normal case)
//#define SLINK_CORE_GENERATOR_MODE     "SLINK_CORE_GENERATOR_MODE"  // used to generate data in the core generator of the sender core.

// These options are important for the Ferol40 Emulator mode
// JRF TODO remove these... 
//#define AMC13_AUTO_TRIGGER_MODE "AMC13_AUTO_TRIGGER_MODE"
//#define AMC13_EXTERNAL_TRIGGER_MODE "AMC13_EXTERNAL_TRIGGER_MODE"
//#define AMC13_SOAP_TRIGGER_MODE "AMC13_SOAP_TRIGGER_MODE"

//JRF TODO, this needs to be changed if we want to use a lock file for AMC13. 
#define LOCKFILE "/dev/xpci"
#define LOCK_PROJECT_ID 'f'

#endif /* __amc13Constants */
