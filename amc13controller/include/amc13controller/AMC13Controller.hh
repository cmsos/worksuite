// $Id: AMC13Controller.hh,v 1.64 2009/04/29 10:25:54 cschwick Exp $
#ifndef _amc13_AMC13Controller_h_
#define _amc13_AMC13Controller_h_

#include <vector>

#include "log4cplus/logger.h"

#include "d2s/utils/SOAPFSMHelper.hh"
#include "d2s/utils/Exception.hh"
#include "amc13controller/ApplicationInfoSpaceHandler.hh"
#include "amc13controller/StatusInfoSpaceHandler.hh"
#include "amc13controller/AMC13StreamInfoSpaceHandler.hh"
#include "amc13controller/InputStreamInfoSpaceHandler.hh"
#include "amc13controller/OutputStreamInfoSpaceHandler.hh"
#include "amc13controller/StreamConfigInfoSpaceHandler.hh"

//#include "amc13controller/Frl.hh"
#include "amc13controller/AMC13.hh"
#include "amc13controller/AMC13WebServer.hh"
#include "amc13controller/AMC13Monitor.hh"
//#include "amc13controller/HardwareLocker.hh"
#include "amc13controller/AMC13StateMachine.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "xdata/ActionListener.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"

#include "xoap/MessageReference.h"

#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Table.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/BSem.h"
#include "toolbox/rlist.h"

#include "hal/linux/StopWatch.hh"

// include for RUNControlState Notifier
#include "xdaq2rc/RcmsStateNotifier.h"

namespace amc13controller {

    class AMC13Controller : public xdaq::Application, public
xgi::framework::UIManager, xdata::ActionListener, utils::InfoSpaceUpdater
    {
    public:
    
        XDAQ_INSTANTIATOR();
    
        AMC13Controller(xdaq::ApplicationStub* stub);

        virtual ~AMC13Controller();


        void ConfigureAction(toolbox::Event::Reference e);
        void EnableAction(toolbox::Event::Reference e); 
        void StopAction(toolbox::Event::Reference e);
        void SuspendAction(toolbox::Event::Reference e);
        void ResumeAction(toolbox::Event::Reference e);
        void HaltAction(toolbox::Event::Reference e);
        void FailAction(toolbox::Event::Reference e);

        void dumpHardwareRegisters();

        xoap::MessageReference changeState( xoap::MessageReference msg );

        virtual xoap::MessageReference ParameterSet( xoap::MessageReference msg ) ;

        xoap::MessageReference laserOn( xoap::MessageReference msg ) ;

        xoap::MessageReference laserOff( xoap::MessageReference msg ) ;

        xoap::MessageReference l0DaqOn( xoap::MessageReference msg ) ;

        xoap::MessageReference l0DaqOff( xoap::MessageReference msg ) ;

        xoap::MessageReference readItem( xoap::MessageReference msg ) ;

        xoap::MessageReference writeItem( xoap::MessageReference msg ) ;

        xoap::MessageReference softTrigger( xoap::MessageReference msg ); 

        xoap::MessageReference sfpStatus( xoap::MessageReference msg ) ;

        //JRF removing becuase no longer needed:
        //xoap::MessageReference resetVitesse( xoap::MessageReference msg ) ;

        xoap::MessageReference resetMonitoring( xoap::MessageReference msg );

        xoap::MessageReference dumpHardwareRegisters( xoap::MessageReference msg );

        bool updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo );

    protected:
        void actionPerformed( xdata::Event &e );

    private:

        void notifyRCMS( toolbox::fsm::FiniteStateMachine & fsm, std::string msg );
        void monitoringWebPage( xgi::Input *in, xgi::Output *out );
        void updateLevel( xgi::Input *in, xgi::Output *out );
        void alternateDisplay( xgi::Input *in, xgi::Output *out );
        void debugAMC13( xgi::Input *in, xgi::Output *out);
        void debugFrl( xgi::Input *in, xgi::Output *out);
        void debugSlinkExpress( xgi::Input *in, xgi::Output *out);
        void testPage( xgi::Input *in, xgi::Output *out);
        void debugBridge( xgi::Input *in, xgi::Output *out);
        void jsonUpdate( xgi::Input *in, xgi::Output *out );
        void jsonInfoSpaces( xgi::Input *in, xgi::Output *out );
        void xdaqWebPage( xgi::Input *in, xgi::Output *out );
        void expertDebugging( xgi::Input *in, xgi::Output *out);
        std::string fillDocumentation();

        void parseQuery( std::string name, std::string value );

    private:
        // To be initialized before the constructor runs
        Logger logger_;
        HAL::StopWatch timer_;
        //JRF we change the application infospace so that it is made up of one global application infospace plus a vector of 4 streamConfig Info Spaces.
	ApplicationInfoSpaceHandler appIS_; //This is the global applicaiton infospace for the configuration
	//JRF TODO, decide the best way to do this: probably have a separate class called StreamConfigInfoSpace which creates its own infospace distinct from the application infospace
	// then use a translation class which maps between the application infospace and the streamConfigInfoSpace.:q
	//
        StatusInfoSpaceHandler statusIS_;
        InputStreamInfoSpaceHandler inputIS_;
        OutputStreamInfoSpaceHandler outputIS_;
	StreamConfigInfoSpaceHandler streamConfigIS_;
        AMC13StateMachine fsm_;
        AMC13Monitor monitor_;
        //HardwareLocker hwLocker_;
        // Frl frl_; //< Controls the FRL hardware.
        AMC13 amc13_; //< Controls the AMC13/MOL hardware.
	bool changingState_;
        //AMC13StateMachine *fsmP_;
        AMC13WebServer *webServer_P;
        
        std::string operationMode_;
        std::string dataSource_;
        std::string errorString_;
        uint32_t instance_;

        std::string htmlDocumentation_;
    };

}
#endif /* __AMC13Controller */
