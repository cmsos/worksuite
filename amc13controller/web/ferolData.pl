#!/usr/bin/perl -w
use strict;
use CGI;
use CGI::Carp;
use JSON;

my $cgi = CGI->new;

my $params = $cgi->Vars;
my $res = "{ \n";
foreach my $para (keys %$params) {
    #warn( $para . " : " . $params->{$para} );
    my $out = `wget -O - $params->{$para} 2>/dev/null`;
    if ( length($out) > 0) {
        $res .= "\"$para\" : $out ,";
    } else {
        $res .= "\"$para\" : {} ,";
    }
}

chop $res;
$res .= " }";

print $cgi->header();

print $res;


