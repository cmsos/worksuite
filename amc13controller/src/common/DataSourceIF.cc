#include "amc13controller/DataSourceIF.hh"
#include "d2s/utils/loggerMacros.h"
#include "amc13controller/amc13Constants.h"
amc13controller::DataSourceIF::DataSourceIF(HAL::HardwareDeviceInterface *device_P_in,
                                    utils::InfoSpaceHandler &appIS,
                                    Logger logger)
    : appIS_(appIS)
{
  operationMode_ = appIS.getstring("OperationMode");
  //JRF TODO I'm not sure why Christoph duplicates this info again... can't we use the vectors in the AMC13 object just pass a pointer?
//JRF TODO, change this to a generic loop over the size of the vector so we no longer need the amc13Constants.hh
//
  for (int i = 0; i < NB_INPUT_STREAMS; i++)
  {
    dataSource_.push_back(dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[i].getField("DataSource"))->value_); //appIS.getstring( "DataSource" );
    streams_.push_back(dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[i].getField("enable"))->value_);       //appIS.getbool( "enableStream0" );
  }
  device_P = device_P_in;
  logger_ = logger;
}

amc13controller::DataSourceIF::~DataSourceIF(){}; //JRF this is required so we at least have a symbol when loading the library.
