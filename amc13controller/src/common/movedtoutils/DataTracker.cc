#include "d2s/utils/DataTracker.hh"

utils::DataTracker::DataTracker(InfoSpaceHandler &is,
                                  //InfoSpaceHandler &appIS,
                                  HAL::PCIDevice **hwpp)
    : is_(&is) //,appIS_(appIS)

{
    hwpp_ = hwpp;
    gettimeofday(&startTime_, NULL);
    gettimeofday(&lastStop_, NULL);
    gettimeofday(&now_, NULL);
}

uint32_t
utils::DataTracker::registerRateTracker(std::string name, std::string hwname, utils::DataTracker::UpdateType updateType, double scale)
{

    TrackerItem it = {RATE, name, hwname, updateType, scale, 0, 0, 0};

    itemMap_.insert(std::pair<std::string, TrackerItem>(name, it));

    return 0;
}

uint32_t
utils::DataTracker::updateScale(std::string name, double scale)
{
    std::tr1::unordered_map<std::string, TrackerItem>::iterator it;
    it = itemMap_.find(name);
    if (it != itemMap_.end())
    {
        (*it).second.scale = scale;
        return 0;
    }
    return -1;
}

void utils::DataTracker::reset()
{
    std::tr1::unordered_map<std::string, TrackerItem>::iterator it;
    for (it = itemMap_.begin(); it != itemMap_.end(); it++)
    {
        (*it).second.value = 0;
        (*it).second.tmp1 = 0;
    }
}

//JRF pass the stream index into update, then track the stream that has been passed in.
uint32_t
utils::DataTracker::update(uint32_t streamNo) throw(HAL::HardwareAccessException)
{
    gettimeofday(&now_, NULL);
    uint32_t dt = subtractTime(now_, lastStop_);
    // require at least 100ms integration time. If not come later and try again...
    //std::cout << "update() on stream No " << streamNo << " time now = " << now_.tv_sec << ", time at last stop = " << lastStop_.tv_sec << ", DT = " << dt << std::endl;
    
    if (dt < 100000)
        return -1;
    lastStop_ = now_;
    //std::cout << "update()... about to loop over tracker Items." << std::endl;
    uint32_t hwval;
    uint64_t hwval64;
    uint32_t stream_offset = streamNo * 0x4000;
    std::tr1::unordered_map<std::string, TrackerItem>::iterator it;
    for (it = itemMap_.begin(); it != itemMap_.end(); it++)
    {
	//std::cout << "for loop ..." << std::endl;
        TrackerItem &item = (*it).second;
        //Update the hardware values
        if (item.updateType == utils::DataTracker::HW32)
        {
            (*hwpp_)->read(item.hwname, &hwval, stream_offset);
            item.value = hwval;
        }
        else if (item.updateType == utils::DataTracker::IF32)
        {
            //JRF Note that when we get the value from the info space, we need to grab it from inside the vector of bags.
            item.value = dynamic_cast<xdata::UnsignedInteger32 *>(is_->getvector("InputPorts")[streamNo].getField(item.hwname))->value_; //JRF replace this with an access to the stream vector item //is_getuint32( item.hwname );
        }
        else if (item.updateType == utils::DataTracker::HW64)
        {
            (*hwpp_)->read64(item.hwname, &hwval64, stream_offset);
            //(*hwpp_)->read( item.hwname, &hwval_hi, 0x4 );
	    //std::cout <<"reading tracker item, name = " << item.hwname << ", " << item.name <<", value = " << hwval64 <<std::endl;
            item.value = hwval64; //(((uint64_t)hwval_hi) << 32 ) + hwval;
        }
        else if (item.updateType == utils::DataTracker::IF64)
        {
            item.value = dynamic_cast<xdata::UnsignedInteger32 *>(is_->getvector("InputPorts")[streamNo].getField(item.hwname))->value_; //is_.getuint64( item.hwname );
        }
        else
        {
            std::cout << "unforeseen item type " << std::endl;
        }

        // process the results
        if (item.tracker_type == RATE)
        {
            //avoid unphysical rates: the difference might become negative at overflows or when
            //resets occur in the hardware.
            if ((item.value >= item.tmp1))
            {
                item.result = item.scale * 1000000.0 * (double)(item.value - item.tmp1) / (double)dt;
            }

            item.tmp1 = item.value;
        }
        else
        {
            item.result = 0;
        }
    }

    //JRF TODO, think more about this. It seems that the better way to do this might be to pass the stream ID into the dataTracker Registration in the utils::Monitor class instead. Then you have one tracker for each stream.
    //JRF After setting all the variables we must set the identifers for the stream.
    /*
    uint32_t fedid = dynamic_cast<xdata::UnsignedInteger32*>(appIS.getvector( "InputPorts" )[stream_index].getField("expectedFedId"))->value_;
    (itemMap_.find( "expectedFedId" ))*.second.value = fedid; //    this->setuint32( "expectedFedId", fedid );
    (itemMap_.find( "expectedFedId" ))*.second.value = streamNo; //this->setuint32( "streamNumber", streamNo );
    (itemMap_.find( "expectedFedId" ))*.second.value = appIS.getuint32("slotNumber"); //this->setuint32( "slotNumber", appIS.getuint32("slotNumber") );*/
    return 0;
}

uint32_t
utils::DataTracker::update() throw(HAL::HardwareAccessException)
{
    gettimeofday(&now_, NULL);
    uint32_t dt = subtractTime(now_, lastStop_);
    // require at least 100ms integration time. If not come later and try again...
    if (dt < 100000)
        return -1;

    lastStop_ = now_;

    uint32_t hwval, hwval_hi;
    std::tr1::unordered_map<std::string, TrackerItem>::iterator it;
    for (it = itemMap_.begin(); it != itemMap_.end(); it++)
    {
        TrackerItem &item = (*it).second;
        //Update the hardware values
        if (item.updateType == utils::DataTracker::HW32)
        {
            (*hwpp_)->read(item.hwname, &hwval);
            item.value = hwval;
        }
        else if (item.updateType == utils::DataTracker::IF32)
        {
            item.value = is_->getuint32(item.hwname);
        }
        else if (item.updateType == utils::DataTracker::HW64)
        {
            (*hwpp_)->read(item.hwname, &hwval);
            (*hwpp_)->read(item.hwname, &hwval_hi, 0x4);

            item.value = (((uint64_t)hwval_hi) << 32) + hwval;
        }
        else if (item.updateType == utils::DataTracker::IF64)
        {
            item.value = is_->getuint64(item.hwname);
        }
        else
        {
            std::cout << "unforeseen item type " << std::endl;
        }

        // process the results
        if (item.tracker_type == RATE)
        {
            //avoid unphysical rates: the difference might become negative at overflows or when
            //resets occur in the hardware.
            if ((item.value >= item.tmp1))
            {
                item.result = item.scale * 1000000.0 * (double)(item.value - item.tmp1) / (double)dt;
            }

            item.tmp1 = item.value;
        }
        else
        {
            item.result = 0;
        }
    }

    return 0;
}

double
utils::DataTracker::get(std::string name)
{
    std::tr1::unordered_map<std::string, TrackerItem>::iterator it;
    it = itemMap_.find(name);
    if (it != itemMap_.end())
        return (*it).second.result;
    return -1;
}

//////////////////////////////////////////////////////////////////////

uint32_t
utils::DataTracker::subtractTime(struct timeval &t, struct timeval &sub)
{
    signed long sec, usec, rsec, rusec;
    sec = t.tv_sec - sub.tv_sec;
    usec = t.tv_usec - sub.tv_usec;
    if (usec < 0)
    {
        sec--;
        usec += 1000000;
    }
    if (sec < 0)
    {
        rsec = 0;
        rusec = 0;
    }
    else
    {
        rsec = (uint32_t)sec;
        rusec = (uint32_t)usec;
    }
    return (rsec * 1000000 + rusec);
}
