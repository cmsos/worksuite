#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "d2s/utils/SOAPFSMHelper.hh"

utils::SOAPFSMHelper::SOAPFSMHelper(  )
{
}

xoap::MessageReference 
utils::SOAPFSMHelper::makeSoapReply( std::string command, std::string answerString )
{
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = envelope.createName( command, "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement bodyElement = envelope.getBody().addBodyElement( responseName );
    std::stringstream resultStr;
    bodyElement.addTextNode( answerString );
    return reply;    
}

xoap::MessageReference 
utils::SOAPFSMHelper::makeSoapFaultReply( std::string command, std::string answerString  )
{
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName faultName = envelope.createName( "Fault", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement bodyElement = envelope.getBody().addBodyElement( faultName );
    xoap::SOAPName faultCodeName = envelope.createName( "faultcode", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement faultCodeElement = bodyElement.addChildElement( faultCodeName );
    faultCodeElement.addTextNode( "Server" );

    xoap::SOAPName faultStringName = envelope.createName( "faultstring", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement faultStringElement = bodyElement.addChildElement( faultStringName );
    faultStringElement.addTextNode( answerString );

    return reply;
}

xoap::MessageReference 
utils::SOAPFSMHelper::makeFsmSoapReply( const std::string event, const std::string state )
{
    xoap::MessageReference message = xoap::createMessage();
    xoap::SOAPEnvelope envelope = message->getSOAPPart().getEnvelope();
    xoap::SOAPBody body = envelope.getBody();
    std::string responseString = event + "Response";
    xoap::SOAPName responseName = envelope.createName(responseString, "xdaq", XDAQ_NS_URI);
    xoap::SOAPBodyElement responseElement = body.addBodyElement(responseName);
    xoap::SOAPName stateName = envelope.createName("state", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement stateElement = responseElement.addChildElement(stateName);
    xoap::SOAPName attributeName = envelope.createName("stateName", "xdaq", XDAQ_NS_URI);

    stateElement.addAttribute(attributeName, state);

    return message;
}

