#include "amc13controller/TCPStreamInfoSpaceHandler.hh"

amc13controller::TCPStreamInfoSpaceHandler::TCPStreamInfoSpaceHandler(xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater, utils::InfoSpaceHandler *appIS)
    : utils::StreamInfoSpaceHandler(xdaq, "TCPStream", updater, appIS)
{
    // name hwitem  update format doc
    // JRF TODO, we may want to add the following:
    // TCP_SOURCE_
    // TCP_ARP_REPLY_OK
    // createuint32( "ARP_MAC_CONFLICT", 0, "", HW32, "Should be '0'. A counter which is incremented every time an ARP reply is received which contaisn a source MAC address equal to ours. If this counter is larger than 0, we have a device on the network which has the same MAC address as this AMC13. This is a deasaster!.");
    //     createuint32( "ARP_IP_CONFLICT", 0, "", HW32, "Should be '0'. A counter which is incremented every time an ARP reply is received which contaisn a source IP address equal to ours. If this counter is larger than 0, we have a device on the network which has the same IP address as this AMC13. This is a deasaster!. At configure time the AMC13 sends a probe ARP-request to search for devices with our IP address in order to detect such a conflict.");
    // DestinationMACAddress
    // createuint64( "ARP_MAC_WITH_IP_CONFLICT_LO", 0, "mac", HW64, "Should be '00:00:00:00:00:00'. If an IP conflict has been detected on the network this register contains the MAC address of the device which has the same IP address as this AMC13." );
    //
    //  eth_10gb_TCP_state
    //  createuint32( "TCP_STATE_FEDS", 0, "hex", HW32, "Bitfield 0: connection stream0 reset; 1: sync request stream0 pending; 2: connection stream0 established; bits 16,17,18 the same for stream 1; bit 31: ARP reply received.");
    //
    //
    createuint32("TCP_STAT_ACK_DELAY_MAX", 0);
//JRF TODO, move this back to 64 bits as soon as the flash list is updated.
    createuint64("BIFI_USED", 0, "", HW64, "The number of 64bit words currently used in the TCP/IP stream buffer (called BIFI). This buffer is 524287 words deep.");
    createuint64("BIFI_USED_MAX", 0, "", HW64, "The maximum number of 64bit words used in the TCP/IP stream buffer (called BIFI), since the TCP/IP connection has been established. This buffer is 524287 words deep.");
    //createuint32("BIFI_USED", 0, "", NOUPDATE, "The number of 64bit words currently used in the TCP/IP stream buffer (called BIFI). This buffer is 524287 words deep.");
    //createuint32("BIFI_USED_MAX", 0, "", NOUPDATE, "The maximum number of 64bit words used in the TCP/IP stream buffer (called BIFI), since the TCP/IP connection has been established. This buffer is 524287 words deep.");

    createuint32("TCP_CONNECTION_ESTABLISHED", 0, "", HW32, "A '1' indicates that a TCP/IP connection is established for this stream.");
    createuint32("TCP_STAT_CONNATTEMPT", 0, "", HW32, "Counts the number of connection attempts since the last reset.");
    createuint32("TCP_STAT_CONNREFUSED", 0, "", HW32, "Counts the number of \"connection refused\" packets received.");
    createuint32("TCP_STAT_CONNRST", 0, "", HW32, "The status of the TCP connection: Bit 0: Connection reset by Peer; Bit 1 : Connection closed by Peer; Bit 2 : Connection refused; Bit 3 : Connection terminated because of URG flag received; Bit 4 : Connection terminated because a higher sequence number than expected received (somebody tries to send data to the AMC13? This is not supported!); Bit 5 : Connection terminated because a SYN packet was received on an established connection.");
    createuint32("TCP_STAT_SNDPROBE", 0, "", HW32, "Counts the number of probe packets sent, while the TCP engine is in the PERSIST state.");
    createuint32("TCP_STAT_SNDPACK", 0, "", HW32, "The number of TCP packets sent out of the 10Gb TCP/IP link but excluding the retransmitted packets.");
    //JRF TODO understand WTF I Cannot add this iwthout it crashing
    //createuint32( "TCP_STAT_SNDPACK64", 0, "", HW64, "The number of TCP packets sent out of the 10Gb TCP/IP link but excluding the retransmitted packets." );
    createuint32("TCP_STAT_SNDREXMITPACK", 0, "", HW32, "The number of retransmitted packets since the TCP/IP connection has been established.");
    createuint32("TCP_STAT_RCVDUPACK", 0, "", HW32, "The number of duplicate acknowledge packets received by this stream.");
    createuint32("TCP_STAT_FASTRETRANSMIT", 0, "");
    createuint32("TCP_STAT_RCVACKTOOMUCH", 0, "");
    createuint32("TCP_STAT_SEG_DROP_AFTER_ACK", 0, "");
    createuint32("TCP_STAT_SEG_DROP", 0, "");
    createuint32("TCP_STAT_SEG_DROP_WITH_RST", 0, "");
    createuint32("TCP_STAT_PERSIST_EXITED", 0, "", HW32, "Counts the number of times this stream leaves the PERSIST state. If this counter is smaller by one than the corresponding TCP_STAT_PERSIST counter, this stream is currently in the PERSIST state.");
    createuint32("TCP_STAT_PERSIST", 0, "", HW32, "Counts the number of times this stream enters the PERSIST state. The Persist state is entered when the receiver cannot receive data anymore. The sender is sending probe packets to find out if it can send data again (see counter TCP_STAT_SNDPROBE). The replies to this probe packet are counted in TCP_STAT_RCVDUPACK.");
    createuint32("TCP_STAT_PERSIST_CLOSEDWND", 0, "");
    createuint32("TCP_STAT_PERSIST_ZEROWND", 0, "");
    createuint32("TCP_STAT_DUPACK_MAX", 0, "");
    createuint64("TCP_STAT_SNDBYTE", 0, "", HW64, "The number of bytes sent out of the 10Gb TCP/IP link but excluding the retransmitted data.");
    createuint64("TCP_STAT_SNDREXMITBYTE", 0, "", HW64);
    createuint32("TCP_STAT_CURRENT_WND", 0, "", HW32, "Snapshot of the TCP window size received by the AMC13");
    createuint32("TCP_CWND", 0, "", HW32, "TCP window size register configured from XML");
    createuint32("TCP_STAT_WND_MAX", 0, "", HW32, "The maximum TCP window size received by the AMC13 since the TCP connection was created.");
    createuint32("TCP_STAT_WND_MIN", 0, "", HW32, "The minimum TCP window size received by the AMC13 since the TCP connection was created.");
    createuint32("TCP_STAT_RTT_COUNTER", 0, "", HW32, "Counts the number of RTT timeouts in this stream.");
    createuint32("TCP_STAT_MEASURE_RTT", 0, "", HW32, "The TCP round-trip-time for a packet measured in this moment [clock cycles: 156.25MHz]");
    createuint32("TCP_STAT_MEASURE_RTT_MAX", 0, "", HW32, "Maximal TCP round-trip-time measured for this stream [clock cycles: 156.25MHz].");
    createuint32("TCP_STAT_MEASURE_RTT_MIN", 0, "", HW32, "Minimal TCP round-trip-time measured for this stream [clock cycles: 156.25MHz].");
    createuint64("TCP_STAT_MEASURE_RTT_SUM", 0, "", HW64, "Sum of all TCP round-trip-times measured for this stream [clock cycles: 156.25MHz]. This value is used to calculate the averate RTT.");
    createuint32("TCP_STAT_MEASURE_RTT_COUNT", 0, "", HW32, "Number of RTT measurements for this stream. (Used to calculate the average RTT)");
    createuint32("TCP_STAT_RTT_SHIFTS_MAX", 0, "", HW32, "Maximal value for the RTT shifts in this stream.");
    createuint32("TCP_STAT_SND_NXT_UNALIGNED", 0, "", HW32, "Number of sent packets where SND_NXT was found not being aligned to 64-bit (is not divisible by 8) and alignment was forced. This register should stay at zero. If non-zero, then somewhere is a big problem. E.g. data corruption inside FEROL?");
 
    createuint64("BACK_PRESSURE_BIFI", 0, "", HW64, "Counts clock cycles when the event generator or SLINKXpress is backpressured by the BIFI (a 175 MHz clock is used)");//JRF TODO remove this once it's no longer in the flash list.

    createdouble( "LatchedTimeBackendSeconds", 0, "", PROCESS, "The integrated or accumulated total time of the AMC13 reference clock at the time of latching the backend regisers (TCP / ethernet daq link) in seconds." );
    createuint64( "BACK_PRESSURE", 0, "", NOUPDATE, "deprecated. This should be removed as soon as the flash list is up-to-date.");
    createdouble( "AccBIFIBackpressureSeconds", 0, "", PROCESS, "The integrated or accumulated total time the input buffer was backpressured by the BIFI in seconds." );
    
    //JRF TODO put this in when I have a register:
    //createuint64( "BACK_PRESSURE_PERSIST", 0 );
    //JRF TODO check why these are commented out
    //createuint64( "PACKETS_RECEIVED", 0, "", HW64, "Total number of packets received from the 10Gb DAQ-link. Here all packets are counted (ARP, PING, DHCP, TCP/IP related packets, ...)" );
    //createuint64( "PACKETS_RECEIVED_BAD", 0, "", HW64, "Total number of errorneous packets received. These are packets with a CRC error." );
    //createuint64( "PACKETS_SENT", 0, "", HW64, "Total number of packets which have been sent out of the 10Gb DAQ-link. Here all packets are counted (ARP, PING, DHCP, sent and retransmitted TCP/IP packets, ...)");
    createuint32("GEN_EVENT_NUMBER", 0, "", HW32, "The number of generated events in the amc13 event generator.");

    createuint64("MAC_RX_GOOD_FRAME_COUNTER", 0, "dec", HW64, "Counts the number of packets without error received from DAQ 10Gb link");
    createuint64("MAC_RX_BAD_FRAME_COUNTER", 0, "dec", HW64, "Counts the number of packets with errors received from DAQ 10Gb link");
    createuint64("MAC_RX_PAUSE_FRAME_COUNTER", 0, "dec", HW64, "Counts bit 35 in avalon_st_rxstatus_data");
    createuint64("MAC_RX_PFC_FRAME_COUNTER", 0, "dec", HW64, "Counts bit 39 in avalon_st_rxstatus_data");
    createuint64("MAC_RX_ERROR_STATUS", 0, "hex", HW64, "when any of previous bits is set, store the content of avalon_st_rxstatus_error here");
    createuint64("MAC_TX_FRAME_ERROR_COUNTER", 0, "dec", HW64, "increases when any of bit 0,1,2,3 and 6 is set in avalon_st_rxstatus_error");
    
    
    createuint64("eth_10gb_status0_MAC_core", 0, "hex", HW64, "MAC Core Status 0");
    createuint64("eth_10gb_status1_MAC_core", 0, "hex", HW64, "MAC Core Status 1");
    createuint64("eth_10gb_status2_MAC_core", 0, "hex", HW64, "MAC Core Status 2");
    createuint64("eth_10gb_status3_MAC_core", 0, "hex", HW64, "MAC Core Status 3");

    // calculated values
    createbool("enable", false, "", PROCESS, "Enable the corresponding stream.");
    createdouble("PACK_REXMIT", 0, "percent", PROCESS);
    createdouble("BIFI", 0, "percent", PROCESS, "The BIFI is the buffer for data queuing to leave the AMC13 via the 10Gb TCP/IP link to the DAQ. It is 507904 bytes large. This value is a snapshot of its filling status in percent.");
    createdouble("BIFI_MAX", 0, "percent", PROCESS, "This value shows the maximal fill stand of the BIFI buffer since the TCP connection was established.");
    createdouble("ACK_DELAY_MAX", 0, "us", PROCESS);
    createdouble("ACK_DELAY", 0, "us", PROCESS);
    createdouble("AVG_ACK_DELAY", 0, "us", PROCESS);
    createdouble("RTT", 0, "us", PROCESS, "The TCP round-trip-time for a packet measured in this moment [us].");
    createdouble("RTT_MAX", 0, "us", PROCESS, "Maximal TCP round-trip-time measured for this stream [us].");
    createdouble("RTT_MIN", 0, "us", PROCESS, "Minimal TCP round-trip-time measured for this stream [us].");
    createdouble("AVG_RTT", 0, "us", PROCESS, "The average TCP round-trip-time for packets since the TCP connection has been established.");

    // calculated rates
    createdouble("TcpThroughput", 0, "Gbitbw", TRACKER, "The current effective data rate to the DAQ link (excluding retransmitted data).");
    createdouble("TcpRetransmit", 0, "Gbitbw", TRACKER, "Rate of retransmitted data in Gbits/sec.");
    createdouble("PauseFrameRate", 0, "rate", TRACKER, "The total rate of pause frames received in this moment." );
//
//JRF Remove this...
//
    createdouble("AMC13EventGenRate", 0, "rate", TRACKER, "The number of generated events in the amc13 event generator.");



    
}

//JRF TODO the rate tracker must update for each stream
void amc13controller::TCPStreamInfoSpaceHandler::registerTrackerItems(DataTracker &tracker)
{
    std::cout << "registerTrackerItems()" << std::endl;
    //JRF TODO put these back in once I implement the rate tracker for streams.
    tracker.registerRateTracker("TcpThroughput", "TCP_STAT_SNDBYTE", amc13controller::DataTracker::HW64, 8.0);
    tracker.registerRateTracker("TcpRetransmit", "TCP_STAT_SNDREXMITBYTE", amc13controller::DataTracker::HW64, 8.0);
    tracker.registerRateTracker( "PauseFrameRate", "MAC_RX_PAUSE_FRAME_COUNTER", amc13controller::DataTracker::HW64 );
    tracker.registerRateTracker( "AMC13EventGenRate", "GEN_EVENT_NUMBER", amc13controller::DataTracker::HW64 );
}
