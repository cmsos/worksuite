#include "amc13controller/BridgeAddressTableReader.hh"
#include "hal/PCIHardwareAddress.hh"

/**
 * The implementation of this class has been chosen to increase
 * readability of the code.
 */
amc13controller::BridgeAddressTableReader::BridgeAddressTableReader()
{

    /************************************************************************************************************/
    /*                                   PCI Address Table : Bridge of the FRL                                  */
    /*                                                                                                          */
    /*          item                           AdrSpace BAR      address         mask   r   w    description    */
    /************************************************************************************************************/

    createItem("ConfigStart", HAL::CONFIGURATION, 0, 0x00000000, 0xffffffff, 1, 1, "");
    createItem("VendorId", HAL::CONFIGURATION, 0, 0x00000000, 0xffffffff, 1, 0, "");
    createItem("command", HAL::CONFIGURATION, 0, 0x00000004, 0x000000ff, 1, 1, "");
    createItem("which", HAL::CONFIGURATION, 0, 0x00000004, 0x00008000, 1, 0, "");
    createItem("BAR0", HAL::CONFIGURATION, 0, 0x00000010, 0xffffffff, 1, 1, "");
    createItem("enable", HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 1, 1, "");
    createItem("jtag", HAL::CONFIGURATION, 0, 0x00000040, 0x00000002, 1, 1, "");
    createItem("firmwareControl", HAL::CONFIGURATION, 0, 0x00000040, 0x0007ffff, 1, 1, "control work for loading firmware and jtag control");
    createItem("firmwareIndex", HAL::CONFIGURATION, 0, 0x00000040, 0x00070000, 1, 1, "index of Fw to load in main fpga");
    createItem("FRLReload", HAL::CONFIGURATION, 0, 0x00000040, 0x00000004, 1, 1, "reload fw of FRL main FPGA");
    createItem("HardwareRevision", HAL::CONFIGURATION, 0, 0x00000048, 0x000f0000, 1, 0, "");
    createItem("FirmwareType", HAL::CONFIGURATION, 0, 0x00000048, 0xfff00000, 1, 0, "");
    createItem("FirmwareVersion", HAL::CONFIGURATION, 0, 0x00000048, 0x0000ffff, 1, 0, "");
    createItem("geogrSlot", HAL::CONFIGURATION, 0, 0x00000050, 0x000000ff, 1, 1, "The  geographic slot (0-20)");
    createItem("serialNumber", HAL::CONFIGURATION, 0, 0x0000005c, 0xffffffff, 1, 0, "");

    createItem("softReset", HAL::MEMORY, 0, 0x00000000, 0x00020000, 0, 1, "software reset for the Bridge");

    createItem("wordCnt2_Link0", HAL::MEMORY, 0, 0x0020040c, 0xffffffff, 1, 1, "counts the 8 bytes words on Link0,high bits");
    createItem("wordCnt2_Link1", HAL::MEMORY, 0, 0x0028040c, 0xffffffff, 1, 1, "");
    createItem("wordCnt1_Link0", HAL::MEMORY, 0, 0x00200408, 0xffffffff, 1, 1, "counts the 8 bytes words on Link0,low bits");
    createItem("wordCnt1_Link1", HAL::MEMORY, 0, 0x00280408, 0xffffffff, 1, 1, "counts the 8 bytes words");
    createItem("WChisto_Link0", HAL::MEMORY, 0, 0x00200000, 0xffffffff, 1, 1, "");
    createItem("WChisto_Link1", HAL::MEMORY, 0, 0x00280000, 0xffffffff, 1, 1, "");
    createItem("WCfedID_Link0", HAL::MEMORY, 0, 0x00200404, 0xffffffff, 1, 1, "");
    createItem("WCfedID_Link1", HAL::MEMORY, 0, 0x00280404, 0xffffffff, 1, 1, "");
    createItem("BXhisto_Link0", HAL::MEMORY, 0, 0x00204000, 0xffffffff, 1, 1, "Bunch crossing histogram, start address");
    createItem("BXhisto_Link1", HAL::MEMORY, 0, 0x00284000, 0xffffffff, 1, 1, "Bunch crossing histogram, start address");
    createItem("spyEnable", HAL::MEMORY, 0, 0x00000000, 0x00000400, 0, 1, "");
    createItem("BXhistoEnd_Link0", HAL::MEMORY, 0, 0x00207ffc, 0xffffffff, 1, 1, "Bunch crossing histogram, end address");
    createItem("BXhistoEnd_Link1", HAL::MEMORY, 0, 0x00287ffc, 0xffffffff, 1, 1, "Bunch crossing histogram, end address");

    /******************************** items for the embedded FEDEmulator ***************************************/
    /*          item                           AdrSpace BAR      address         mask  r   w    description    */

    createItem("stream0_descriptor0", HAL::MEMORY, 0, 0x00700000, 0xffffffff, 1, 1, "first descriptor word for Stream 0");
    createItem("stream0_descriptor1", HAL::MEMORY, 0, 0x00700004, 0xffffffff, 1, 1, "second descriptor word for Stream 0");
    createItem("stream0_descriptor2", HAL::MEMORY, 0, 0x00700008, 0xffffffff, 1, 1, "third descriptor word for Stream 0");
    createItem("stream0_descriptor3", HAL::MEMORY, 0, 0x0070000c, 0xffffffff, 1, 1, "fourth descriptor word for Stream 0");
    createItem("stream0_eventLength", HAL::MEMORY, 0, 0x00700000, 0x00ffffff, 1, 1, "");
    createItem("stream0_source", HAL::MEMORY, 0, 0x00700004, 0x00000fff, 1, 1, "");
    createItem("stream0_seed", HAL::MEMORY, 0, 0x00700004, 0x000ff000, 1, 1, "");
    createItem("stream0_bx", HAL::MEMORY, 0, 0x00700008, 0x00000fff, 1, 1, "");
    createItem("stream0_deltaT", HAL::MEMORY, 0, 0x00700008, 0xfffff000, 1, 1, "in 100ns steps");
    createItem("stream0_eventId", HAL::MEMORY, 0, 0x0070000c, 0x00ffffff, 1, 1, "");
    createItem("stream0_errorEvId", HAL::MEMORY, 0, 0x0070000c, 0x80000000, 1, 1, "");
    createItem("stream0_errorCRC", HAL::MEMORY, 0, 0x0070000c, 0x40000000, 1, 1, "");
    createItem("stream0_memoryEnd", HAL::MEMORY, 0, 0x0077fffc, 0xffffffff, 1, 1, "");

    createItem("stream1_descriptor0", HAL::MEMORY, 0, 0x00780000, 0xffffffff, 1, 1, "first descriptor word for Stream 1");
    createItem("stream1_descriptor1", HAL::MEMORY, 0, 0x00780004, 0xffffffff, 1, 1, "second descriptor word for Stream 1");
    createItem("stream1_descriptor2", HAL::MEMORY, 0, 0x00780008, 0xffffffff, 1, 1, "third descriptor word for Stream 1");
    createItem("stream1_descriptor3", HAL::MEMORY, 0, 0x0078000c, 0xffffffff, 1, 1, "fourth descriptor word for Stream 1");
    createItem("stream1_eventLength", HAL::MEMORY, 0, 0x00780000, 0x00ffffff, 1, 1, "");
    createItem("stream1_source", HAL::MEMORY, 0, 0x00780004, 0x00000fff, 1, 1, "");
    createItem("stream1_seed", HAL::MEMORY, 0, 0x00780004, 0x000ff000, 1, 1, "");
    createItem("stream1_bx", HAL::MEMORY, 0, 0x00780008, 0x00000fff, 1, 1, "");
    createItem("stream1_deltaT", HAL::MEMORY, 0, 0x00780008, 0xfffff000, 1, 1, "in 100ns steps");
    createItem("stream1_eventId", HAL::MEMORY, 0, 0x0078000c, 0x00ffffff, 1, 1, "");
    createItem("stream1_errorEvId", HAL::MEMORY, 0, 0x0078000c, 0x80000000, 1, 1, "");
    createItem("stream1_errorCRC", HAL::MEMORY, 0, 0x0078000c, 0x40000000, 1, 1, "");
    createItem("stream1_memoryEnd", HAL::MEMORY, 0, 0x007ffffc, 0xffffffff, 1, 1, "");

    /***********************************************************************************************************/
}
