#include "amc13controller/ApplicationInfoSpaceHandler.hh"
#include "d2s/utils/loggerMacros.h"
#include "amc13controller/amc13Constants.h"

amc13controller::ApplicationInfoSpaceHandler::ApplicationInfoSpaceHandler(xdaq::Application *xdaq)
    : amc13controller::AMC13BaseInfoSpaceHandler(xdaq, xdaq->getApplicationInfoSpace(), "Appl_IS")
// All values below are configuration parameters and should be set before "Configure"
{
    createDocumentationSeparator("Parameters which control the behaviour of the Application. They are not directly related to the hardware.");
    createstring("fedEnableMask", "uninitialized", "", NOUPDATE, "Contains the FED Enable Mask passed from the Function Manager.");
    // Temporary hack to make RCMS and scripts happy
    createstring("stateName", "uninitialized", "", PROCESS, "A temporary (or not so temporary...) hack to make tools like RCMS happy. These want to query the stateName variable in the ApplicationInfospace. They have no means to easily access other Infospaces.");
    // The following parameters influence the behaviour of the entire application.
    createbool("noFirmwareVersionCheck", false, "", NOUPDATE, "This parameter is intended for debugging only. If set to true, the version checking in the FPGA will be switched off. Do not use this option in production or serious test setups!");
    createbool("lightStop", false, "", NOUPDATE, "If this parameter is set to false, a \"Stop\" command will execute a \"Halt\" and a \"Configure\". If set to true, a faster transition is executed: On the FRL the emulator is stopped and a pause of 0.1sec is introduced if the FRL_EMULATOR_MODE was active. Then a softwareReset is executed, the DAQ_mode is reset adn the FifoMonCounters and the event size histogramming are disabled. Finally the expected source FED IDs are set in the hardware. In the AMC13 the event generators are stopped if the AMC13EmulatorMode was active. Then some monitoring counters are reset. TCP/IP connections are not touched.");
    createuint32("slotNumber", 0, "", NOUPDATE, "The slot numbers are counted from the left side of the crate and start with '1'. (Slot 0 is occupied by the MCH, there are 1-12 AMC slots and 13 is reserved for the hub slot.)");
    createuint32("deltaTMonMs", 2000, "", NOUPDATE, "The time interval in ms between successive updates of the monitoring information."); //
    //JRF TODO change the modes to something more sensible like NORMAL, FED_KIT.
    createstring("OperationMode", AMC13_MODE_NORMAL, "", NOUPDATE, "Defines the Mode of operation: <br>\"AMC13_MODE_EFED\" configures the AMC13 as if it were a FED generating the data on the AMC13. <br>\"AMC13_MODE_NO_DAQ\" configures the AMC13 to transmit the clock and trigger on the backplane only. "); //

    xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> > tempInputPortsVector;
    const xdata::Bag<amc13controller::InputStreamConf> bag1;
    for (int i = 0; i < NB_INPUT_STREAMS; i++)
    {
        tempInputPortsVector.push_back(bag1) ;
    }
    createvector("InputPorts", tempInputPortsVector, "", NOUPDATE, "This Vector contains the configuration params for each input stream");


    xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> > tempOutputPortsVector;
    const xdata::Bag<amc13controller::OutputStreamConf> bag2;
    for (int i = 0; i < NB_OUTPUT_STREAMS; i++)
    {
        tempOutputPortsVector.push_back(bag2);
    }
    createvectoroutputs("OutputPorts", tempOutputPortsVector, "", NOUPDATE, "This Vector contains the configuration params for each output stream");
    

    createbool("ignoreDaq", false, "", NOUPDATE, "Ignore the DAQ outputs");
    createbool("monBufBp", false, "", NOUPDATE, "Monitor buffer backpressure enable");
    createbool("localTtc", false, "", NOUPDATE, "Enable local TTC using loop back fibre.");
    createbool("genEnable", false, "", NOUPDATE, "Enable or disable the event generation on the AMC13");
    createuint32("genSize", 0, "", NOUPDATE, "Set the generated event size in 64bit Words");

    createDocumentationSeparator("Parameters related to the data streams. The AMC13 can be operated with up to 2 streams. At the input they either correspond to the SLINK ports, to the SLINKexpress ports or to the  event generators, In the output they are handled as different TCP/IP streams. They always go to the same IP destination address but to different ports.");


    /*JRF TODO Decide if we want to use this to create random size events. 
    createuint32( "Event_Length_Stdev_bytes_FED0", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated" );
    createuint32( "Event_Length_Stdev_bytes_FED1", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated" );
    createuint32( "Event_Length_Max_bytes_FED0", 0x10000, "", NOUPDATE, "Only in use if Event_Length_Stdev_bytes_FED0 is larger than 0! The event generators truncate at the given maximal event size. No events larger than this size will be generated." );
    createuint32( "Event_Length_Max_bytes_FED1", 0x10000, "", NOUPDATE, "Only in use if Event_Length_Stdev_bytes_FED1 is larger than 0! The event generators truncate at the given maximal event size. No events larger than this size will be generated." );
    createuint32( "Event_Delay_ns_FED0", 20, "", NOUPDATE, "This delay is given in nanoseconds. The FRL hardware can only generate delays in steps of 10ns. 20 bits are available for the delay: therefore the maximal possible delay int the Frl is 10485750ns (10.48ms).  The AMC13 hardware can generate delays in steps of 20ns. 16 bits are available for the delay: therefore the maximal possible delay in the AMC13 is 1310700ns (1.31ms)" );
    createuint32( "Event_Delay_ns_FED1", 20, "", NOUPDATE, "This delay is given in nanoseconds. The FRL hardware can only generate delays in steps of 10ns. 20 bits are available for the delay: therefore the maximal possible delay int the Frl is 10485750ns (10.48ms).  The AMC13 hardware can generate delays in steps of 20ns. 16 bits are available for the delay: therefore the maximal possible delay in the AMC13 is 1310700ns (1.31ms)" );
    createuint32( "Event_Delay_Stdev_ns_FED0", 0, "", NOUPDATE, "If set to 0 a fixed delay between events is generated" );
    createuint32( "Event_Delay_Stdev_ns_FED1", 0, "", NOUPDATE, "If set to 0 a fixed delay between events is generated" );
	*/
    createDocumentationSeparator("The following parameters are required by the JobControl to pass parameters for specific run types. These should be deprecated once the framework allows to send the correct vector of bags structure.");
    //createbool(   "enableDeskew", false, "", NOUPDATE, "Perform a deskew calibration of the SLINK cable(s)." );
    //createbool(   "enableDCBalance", false, "", NOUPDATE, "Use a DC-balance encoding on the SLINK." );
    //createbool(   "enableWCHistogram", true, "", NOUPDATE, "If set to true, wordcount histograms are generated in the FRL." );
    //createuint32( "testDurationMs", 500, "", NOUPDATE, "Defines the length of the SLINK test in ms." );
    //createuint32( "WCHistogramResolution", 4, "", NOUPDATE, "" );*/

    createbool("SourceIpOverride", false, "", NOUPDATE, "Override the Source IP with the value in the xml");
    
    //JRF the following sets of 4 values is used until the framework can handle vectors.
    //JRF TODO, these values should be mirrored with the equivalent parameters in the streamConfigInfoSpace so that when samim sets them by soap messages, they get propagated to the hardware.
    //
    createbool("enableStream0", false, "", NOUPDATE, "Enable Stream 0.");
    createbool("enableStream1", false, "", NOUPDATE, "Enable Stream 1.");
    createbool("enableStream2", false, "", NOUPDATE, "Enable Stream 2.");
    createbool("enableStream3", false, "", NOUPDATE, "Enable Stream 3.");

    createuint32("Event_Length_bytes_FED0", 0x0, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration.");
    createuint32("Event_Length_bytes_FED1", 0x0, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration. ");
    createuint32("Event_Length_bytes_FED2", 0x0, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration.");
    createuint32("Event_Length_bytes_FED3", 0x0, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration. ");

    createuint32("Event_Length_Stdev_bytes_FED0", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated");
    createuint32("Event_Length_Stdev_bytes_FED1", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated");
    createuint32("Event_Length_Stdev_bytes_FED2", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated");
    createuint32("Event_Length_Stdev_bytes_FED3", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated");

    createstring( "T1_IP", "0.0.0.0", "", NOUPDATE, "The IP address/hostname of the AMC13 T1. This can be either in dotted notation or a qulified hostname which will be looked up via the DNS." );
    createstring( "T2_IP", "0.0.0.0", "", NOUPDATE, "The IP address/hostname of the AMC13 T2. This can be either in dotted notation or a qulified hostname which will be looked up via the DNS." );


    createDocumentationSeparator("Parameters to tune the 10 Gbps TCP/IP connection to the DAQ");
    //JRF TODO We may need to Move these into the bag so that we can have different settings per stream. For now I'll leave them as global params.

    // Bind setting of default parameters
    xdaq->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

    //T1 and T2 Identities
    createstring( "ConnectionXML", "connection.xml", "", NOUPDATE, "The name of the\
                   connection file, which should be in the directory $(CACTUS_ROOT)/\
                   etc/amc13/");
    createstring( "Connection_Prefix", "", "", NOUPDATE, "The prefix for both T1 and T2 IDs of the T2 chips, "
		    "as given in the connection xml file.");

}

// callback for Parameter Setting
void amc13controller::ApplicationInfoSpaceHandler::actionPerformed(xdata::Event &e)
{
	DEBUG("actionPerformed called with type " << e.type());
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{

		DEBUG("actionPerformed for setDefaultValues");
		// read all values from the infospace so that they are displayed in
		// the local monitoring.
		readInfoSpace();
		// this pushes the changes into the monitoring system by firing the monitoring events:
		pushInfospace();
	}
}

// special method to set the parameters needed by the RCMSStateListener. This is a structure and a bool to monitor.
void amc13controller::ApplicationInfoSpaceHandler::setRCMSStateListenerParameters(xdata::Bag<xdaq2rc::ClassnameAndInstance> *RcmsStateListenerParameters,
		xdata::Boolean *FoundRcmsStateListener)
{
	is_P->lock();
	is_P->fireItemAvailable("rcmsStateListener", RcmsStateListenerParameters);
	is_P->fireItemAvailable("foundRcmsStateListener", FoundRcmsStateListener);
	is_P->unlock();
	cpis_P->lock();
	cpis_P->fireItemAvailable("rcmsStateListener", RcmsStateListenerParameters);
	cpis_P->fireItemAvailable("foundRcmsStateListener", FoundRcmsStateListener);
	cpis_P->unlock();
}

void amc13controller::ApplicationInfoSpaceHandler::mirrorToFlatParams()
{
	std::cout << " We Are Here " << std::endl;
	readInfoSpace();
	std::cout << " We Are Here " << std::endl;
	//JRF TODO, decide if this is needed or not, I guess not
	/*
	   setbool("enableStream0", dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[0].getField("enable"))->value_);
	   setbool("enableStream1", dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[1].getField("enable"))->value_);
	   setbool("enableStream2", dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[2].getField("enable"))->value_);
	   setbool("enableStream3", dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[3].getField("enable"))->value_);

	   setuint32("Event_Length_bytes_FED0", dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[0].getField("Event_Length_bytes"))->value_);
	   setuint32("Event_Length_bytes_FED1", dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[1].getField("Event_Length_bytes"))->value_);
	   setuint32("Event_Length_bytes_FED2", dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[2].getField("Event_Length_bytes"))->value_);
	   setuint32("Event_Length_bytes_FED3", dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[3].getField("Event_Length_bytes"))->value_);

	   setuint32("Event_Length_Stdev_bytes_FED0", dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[0].getField("Event_Length_Stdev_bytes"))->value_);
	   setuint32("Event_Length_Stdev_bytes_FED1", dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[1].getField("Event_Length_Stdev_bytes"))->value_);
	   setuint32("Event_Length_Stdev_bytes_FED2", dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[2].getField("Event_Length_Stdev_bytes"))->value_);
	   setuint32("Event_Length_Stdev_bytes_FED3", dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[3].getField("Event_Length_Stdev_bytes"))->value_);
*/

	   writeInfoSpace();
	   pushInfospace();
	   
}

void amc13controller::ApplicationInfoSpaceHandler::mirrorFromFlatParams()
{
	//JRF NOTE, we mirror the flat params used by the Function Manager to configure small parts of the infospace during state changes:
	//enableStream0,1,2,3
	//Event_Length_bytes_FED0,1,2,3
	//Event_Length_Stdev_bytes_FED0,1,2,3
	/* JRF TODO, decide if this is needed or not. 

	   std::cout << "enableStream0 = " << (getbool("enableStream0") ? "true" : "false") << std::endl;
	   std::cout << "enableStream1 = " << (getbool("enableStream1") ? "true" : "false") << std::endl;
	   std::cout << "enableStream2 = " << (getbool("enableStream2") ? "true" : "false") << std::endl;
	   std::cout << "enableStream3 = " << (getbool("enableStream3") ? "true" : "false") << std::endl;

	   setvectorelementbool("InputPorts", "enable", getbool("enableStream0"), 0, true);
	   setvectorelementbool("InputPorts", "enable", getbool("enableStream1"), 1, true);
	   setvectorelementbool("InputPorts", "enable", getbool("enableStream2"), 2, true);
	   setvectorelementbool("InputPorts", "enable", getbool("enableStream3"), 3, true);

	   std::cout << "stream enable for stream 0 = " << (dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[0].getField("enable"))->value_ ? "true" : "false") << std::endl;
	   std::cout << "stream enable for stream 1 = " << (dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[1].getField("enable"))->value_ ? "true" : "false") << std::endl;
	   std::cout << "stream enable for stream 2 = " << (dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[2].getField("enable"))->value_ ? "true" : "false") << std::endl;
	   std::cout << "stream enable for stream 3 = " << (dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[3].getField("enable"))->value_ ? "true" : "false") << std::endl;

	   setvectorelementuint32("InputPorts", "Event_Length_bytes", getuint32("Event_Length_bytes_FED0"), 0, true);
	   setvectorelementuint32("InputPorts", "Event_Length_bytes", getuint32("Event_Length_bytes_FED1"), 1, true);
	   setvectorelementuint32("InputPorts", "Event_Length_bytes", getuint32("Event_Length_bytes_FED2"), 2, true);
	   setvectorelementuint32("InputPorts", "Event_Length_bytes", getuint32("Event_Length_bytes_FED3"), 3, true);
	   setvectorelementuint32("InputPorts", "Event_Length_Stdev_bytes", getuint32("Event_Length_Stdev_bytes_FED0"), 0, true);
	   setvectorelementuint32("InputPorts", "Event_Length_Stdev_bytes", getuint32("Event_Length_Stdev_bytes_FED1"), 1, true);
	   setvectorelementuint32("InputPorts", "Event_Length_Stdev_bytes", getuint32("Event_Length_Stdev_bytes_FED2"), 2, true);
	   setvectorelementuint32("InputPorts", "Event_Length_Stdev_bytes", getuint32("Event_Length_Stdev_bytes_FED3"), 3, true);
	   */
}
