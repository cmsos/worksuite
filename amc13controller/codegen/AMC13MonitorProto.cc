#include "amc13controller/AMC13Monitor.hh"
#include "d2s/utils/loggerMacros.h"
#include "amc13controller/amc13Constants.h"

amc13controller::AMC13Monitor::AMC13Monitor(Logger &logger,
                                        ApplicationInfoSpaceHandler &appIS,
                                        xdaq::Application *xdaq,
                                        utils::ApplicationStateMachineIF &fsm)
    : utils::Monitor(logger, appIS, xdaq, fsm)
{
    this->addApplInfoSpaceItemSets(&appIS);
}

void amc13controller::AMC13Monitor::addApplInfoSpaceItemSets(amc13controller::ApplicationInfoSpaceHandler *is)
{
}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::StreamConfigInfoSpaceHandler *is)
{
}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::StatusInfoSpaceHandler *is)
{
//Application state items:
newItemSet("appl_state", 1);
addItem("appl_state","testCounter",is);
addItem("appl_state","instance",is);
addItem("appl_state","slotNumber",is);
addItem("appl_state","stateName",is);
addItem("appl_state","subState",is);
addItem("appl_state","failedReason",is);
addItem("appl_state","lockStatus",is);

`!single
}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::InputStreamInfoSpaceHandler *is)
{
    std::stringstream name;
    for (uint32_t i(0); i < NB_INPUT_STREAMS; i++)
    {
`!input 
    }

}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::OutputStreamInfoSpaceHandler *is)
{
    std::stringstream name;
    for (uint32_t i(0); i < NB_OUTPUT_STREAMS; i++)
    {
`!output
    }

    

}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::AMC13InfoSpaceHandler *is)
{
}
