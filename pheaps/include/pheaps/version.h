// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pheaps_version_h_
#define _pheaps_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_PHEAPS_VERSION_MAJOR 1
#define WORKSUITE_PHEAPS_VERSION_MINOR 14
#define WORKSUITE_PHEAPS_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_PHEAPS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_PHEAPS_PREVIOUS_VERSIONS  "1.9.0,1.10.0,1.11.0,1.12.0,1.13.0"


//
// Template macros
//
#define WORKSUITE_PHEAPS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_PHEAPS_VERSION_MAJOR,WORKSUITE_PHEAPS_VERSION_MINOR,WORKSUITE_PHEAPS_VERSION_PATCH)
#ifndef WORKSUITE_PHEAPS_PREVIOUS_VERSIONS
#define WORKSUITE_PHEAPS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_PHEAPS_VERSION_MAJOR,WORKSUITE_PHEAPS_VERSION_MINOR,WORKSUITE_PHEAPS_VERSION_PATCH)
#else 
#define WORKSUITE_PHEAPS_FULL_VERSION_LIST  WORKSUITE_PHEAPS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_PHEAPS_VERSION_MAJOR,WORKSUITE_PHEAPS_VERSION_MINOR,WORKSUITE_PHEAPS_VERSION_PATCH)
#endif 
namespace pheaps
{
	const std::string project = "worksuite";
	const std::string package  =  "pheaps";
	const std::string versions = WORKSUITE_PHEAPS_FULL_VERSION_LIST;
	const std::string summary = "Physical memory pluggable allocator";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Xplore";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
