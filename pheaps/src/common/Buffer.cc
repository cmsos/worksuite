// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pheaps/Buffer.h"

pheaps::Buffer::Buffer(toolbox::mem::Pool * pool, size_t size, void* address, void * physicalAddress, void * kernelAddress): 
	toolbox::mem::Buffer(pool, size, address)
{	
	physicalAddress_ = physicalAddress;
	kernelAddress_ = kernelAddress;	
}

void * pheaps::Buffer::getPhysicalAddress()
{
	return  physicalAddress_;
}

void * pheaps::Buffer::getKernelAddress()
{
	return  kernelAddress_;
}	
