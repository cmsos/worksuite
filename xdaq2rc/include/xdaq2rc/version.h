#ifndef _xdaq2rc_version_h_
#define _xdaq2rc_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_XDAQ2RC_VERSION_MAJOR 1
#define WORKSUITE_XDAQ2RC_VERSION_MINOR 11
#define WORKSUITE_XDAQ2RC_VERSION_PATCH 0
#undef WORKSUITE_XDAQ2RC_PREVIOUS_VERSIONS


#define WORKSUITE_XDAQ2RC_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XDAQ2RC_VERSION_MAJOR,WORKSUITE_XDAQ2RC_VERSION_MINOR,WORKSUITE_XDAQ2RC_VERSION_PATCH)
#ifndef WORKSUITE_XDAQ2RC_PREVIOUS_VERSIONS
#define WORKSUITE_XDAQ2RC_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XDAQ2RC_VERSION_MAJOR,WORKSUITE_XDAQ2RC_VERSION_MINOR,WORKSUITE_XDAQ2RC_VERSION_PATCH)
#else
#define WORKSUITE_XDAQ2RC_FULL_VERSION_LIST  WORKSUITE_XDAQ2RC_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XDAQ2RC_VERSION_MAJOR,WORKSUITE_XDAQ2RC_VERSION_MINOR,WORKSUITE_XDAQ2RC_VERSION_PATCH)
#endif

namespace xdaq2rc
{
	const std::string project = "worksuite";
  const std::string package = "xdaq2rc";
  const std::string versions = WORKSUITE_XDAQ2RC_FULL_VERSION_LIST;
  const std::string version = PACKAGE_VERSION_STRING(WORKSUITE_RUBUILDEREVM_VERSION_MAJOR,WORKSUITE_RUBUILDEREVM_VERSION_MINOR,WORKSUITE_RUBUILDEREVM_VERSION_PATCH);
  const std::string description = "XDAQ to run-control communications library";
  const std::string summary = "XDAQ to run-control communications library";
  const std::string authors = "Steven Murray, Remi Mommsen";
  const std::string link = "http://cms-ru-builder.web.cern.ch/cms-ru-builder";

  config::PackageInfo getPackageInfo();

  void checkPackageDependencies()
    ;

  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
