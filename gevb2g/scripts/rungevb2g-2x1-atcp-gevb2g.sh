#!/usr/bin/tclsh

set tests {
256
512
768
1024
1280
1536
1792
2048
2304
2560
2816
3072
3328
3584
3840
4096
4352
4608
4864
5120
5376
5632
5888
6144
6400
6656
6912
7168
7424
7680
7936
8192
12288
16384
20480
24064
26112
30720
32768
36864
40960
45056
51200
55296
62720
65536
86016
106496
126976
}

set atcpline {
		{ "pt::atcp" "http://dvbufu-b1b05-21-01.cms:40000" 20 }
		{ "pt::atcp" "http://dvbufu-b1b05-22-01.cms:40000" 20 }
		{ "pt::atcp" "http://dvbufu-b1b05-23-01.cms:40000" 20 }
		{ "pt::atcp" "http://dvbufu-b1b05-24-01.cms:40000" 20 }
}

set bootline {
        { "gevb2g::EVM" "http://dvbufu-b1b05-23-01.cms:40000" 43 }
        { "gevb2g::RU" "http://dvbufu-b1b05-21-01.cms:40000" 43 }
        { "gevb2g::RU" "http://dvbufu-b1b05-22-01.cms:40000" 43 }
}

set inputline {
        { "gevb2g::InputEmulator" "http://dvbufu-b1b05-21-01.cms:40000" 42 }
        { "gevb2g::InputEmulator" "http://dvbufu-b1b05-22-01.cms:40000" 42 }
}

set buline {
        { "gevb2g::BU" "http://dvbufu-b1b05-24-01.cms:40000" 43 }
}

foreach item $atcpline {
	 puts $item
	set name [lindex $item 0]
    	set url  [lindex $item 1]
    	set lid  [lindex $item 2]
    	
    	exec ./configure-gevb2g.sh $url $lid
}

foreach item $atcpline {
	 puts $item
	set name [lindex $item 0]
    	set url  [lindex $item 1]
    	set lid  [lindex $item 2]

    	exec ./enable-gevb2g.sh $url $lid 
}

 puts "waiting connections..." 
	
	after 3000

puts "start test"


#set url "http://lab04.lbdaq.cern.ch:1972"
#set lid 42

foreach test $tests {
	set size [lindex $test 0]

	puts [format "testing size %d " $size]

	foreach item $inputline {
#       puts $item
        	set name [lindex $item 0]
        	set url  [lindex $item 1]
        	set lid  [lindex $item 2]

		puts [format "configuring Input on %s " $url]

		exec ./setinputsize-gevb2g.sh $url $lid $size 
		exec ./configure-gevb2g.sh $url $lid
	}

	foreach item $buline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "set/configure %s on %s " $name $url]

		exec ./parameterset2g-gevb2g.sh $url $lid $size $name 
		exec ./configure-gevb2g.sh $url $lid
        }

	foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "configure %s on %s " $name $url]

                exec ./configure-gevb2g.sh $url $lid
        }


        foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "enable %s on %s " $name $url]

                exec ./enable-gevb2g.sh $url $lid
        }

        foreach item $buline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "enable %s on %s " $name $url]

                exec ./enable-gevb2g.sh $url $lid
        }



	puts "before starting input"
	after 5000
	puts "go"



	foreach item $inputline {
	 	set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "starting Input on %s " $url]
		exec ./enable-gevb2g.sh $url $lid

	}

	#test is running here and data is being collected
	after 100000

	 foreach item $inputline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]
        
                puts [format "halting Input on %s " $url]
		exec ./halt-gevb2g.sh $url $lid

        }



	foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "halting %s on %s " $name $url]

                exec ./halt-gevb2g.sh $url $lid
        }

	after 5000

	foreach item $buline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "halting BU on %s " $url]

		exec ./halt-gevb2g.sh $url $lid
        }

	after 5000



}

foreach item $inputline {
        set name [lindex $item 0]
        set url  [lindex $item 1]
        set lid  [lindex $item 2]

        puts [format "halting Input on %s " $url]
        exec ./halt-gevb2g.sh $url $lid

}


puts "test finished"

