// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, P. Roberts										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _es_api_Stream_h_
#define _es_api_Stream_h_

// sys includes
#include <string>
#include <curl/curl.h>

// extern includes
#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLURL.hpp"
#include "jansson.h"


// xdaq includes
#include "xdaq/Object.h"
#include "es/api/exception/Exception.h"
#include "es/api/Curl.h"
#include "toolbox/Properties.h"

//raw test
//#include "es/api/HTTPURLConnection.h"

//#include "xercesc/util/XMLNetAccessor.hpp"
//#include "xercesc/util/BinInputStream.hpp"
//#include "es/api/CurlNetAccessor.hpp"

namespace es
{
	namespace api
	{
		class Stream : public xdaq::Object
		{
			public:

				Stream (xdaq::Application * owner, const std::string & url, toolbox::Properties & properties) ;
				~Stream ();

				json_t * get(const std::string & path, const std::string & query,  const std::string & fragment,json_t * json, long * httpcode) ;
				json_t * put(const std::string & path, const std::string & query,  const std::string & fragment, json_t * json, long * httpcode) ;
				json_t * post(const std::string & path, const std::string & query,  const std::string & fragment, json_t * json, long * httpcode) ;

				json_t * binaryPost(const std::string & path, const std::string & query,  const std::string & fragment, char * buffer, long length, long * httpcode) ;

				json_t * del(const std::string & path, const std::string & query,  const std::string & fragment, long * httpcode) ;

				void head(const std::string & path, long * httpcode) ;

			protected:
				size_t receive(char *buffer, size_t size, size_t nitems);
				size_t send(char *buffer, size_t size, size_t nitems);
				json_t * request(XMLURL & xmlUrl,  es::api::HTTPMethod fHTTPMethod, char * buffer, long length, long * httpcode) ;
				json_t * request(XMLURL & xmlUrl,  es::api::HTTPMethod fHTTPMethod , json_t * json, long * httpcode) ;

				// libcurl ANSI C
				static size_t staticWriteCallback(char *buffer,	size_t size, size_t nitems, void *stream);
				static size_t staticReadCallback(char *buffer,	size_t size, size_t nitems, void *stream);

			private:

				std::string url_;
				CURL *curl_;
				std::string inputstream_;
				//std::string outputstream_;
				char * fPayload_;
				size_t fPayloadLen_ ;
				toolbox::Properties properties_;

				// raw test, we use libcurl in place
				//HTTPURLConnection *connection_;

				//CurlNetAccessor na_;
				//XMLByte* outputBuffer_;
				//size_t outputBufferSize_;
				//json_t * request(XMLURL & xmlUrl,  es::api::HTTPMethod fHTTPMethod, std::list<std::string> & headers, const char * fPayload, size_t fPayloadLen) ;
			public:
				size_t counter_;


		};
	}
}

#endif
