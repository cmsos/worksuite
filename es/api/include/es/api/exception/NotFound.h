// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Forrest, P. Roberts											 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _es_api_exception_NotFound_h_
#define _es_api_exception_NotFound_h_

#include "es/api/exception/Exception.h"

namespace es
{
	namespace api
	{
		namespace exception
		{

			class NotFound : public es::api::exception::Exception
			{
				public:
					NotFound (std::string name, std::string message, std::string module, int line, std::string function)
						: es::api::exception::Exception::Exception(name, message, module, line, function)
					{
					}

					NotFound (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
						: es::api::exception::Exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
