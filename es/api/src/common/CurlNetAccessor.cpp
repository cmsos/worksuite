// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, P. Roberts										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLExceptMsgs.hpp>

#include "es/api/CurlNetAccessor.hpp"
#include "es/api/CurlURLInputStream.hpp"






es::api::CurlNetAccessor::CurlNetAccessor()
{
	initCurl();
}


es::api::CurlNetAccessor::~CurlNetAccessor()
{
	cleanupCurl();
}


//
// Global once-only init and cleanup of curl
//
// The init count used here is not thread protected; we assume
// that creation of the CurlNetAccessor will be serialized by
// the application. If the application is also using curl, then
// care must be taken that curl is initialized only once, by some
// other means, or by overloading these methods.
//
int es::api::CurlNetAccessor::fgCurlInitCount = 0;

void
es::api::CurlNetAccessor::initCurl()
{
	if (fgCurlInitCount++ == 0)
		curl_global_init(	0
						  | CURL_GLOBAL_ALL			// Initialize all curl modules
					//	  | CURL_GLOBAL_WIN32		// Initialize Windows sockets first
					//	  | CURL_GLOBAL_SSL			// Initialize SSL first
						  );
}


void
es::api::CurlNetAccessor::cleanupCurl()
{
	if (fgCurlInitCount > 0 && --fgCurlInitCount == 0)
		curl_global_cleanup();
}


es::api::CurlURLInputStream* es::api::CurlNetAccessor::makeNew(const XMLURL&  urlSource, es::api::HTTPMethod fHTTPMethod, std::list<std::string> & headers, const es::api::NetHTTPInfo* httpInfo)
{
	// Just create a CurlURLInputStream
	// We defer any checking of the url type for curl in CurlURLInputStream
	std::cout << "------>>>>>" << std::endl;
	CurlURLInputStream* retStrm = new CurlURLInputStream(urlSource, fHTTPMethod, headers, httpInfo);
	return retStrm;            
}



