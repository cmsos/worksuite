<?php
    
    require 'vendor/autoload.php';
    include_once ('config/config.php');

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); // required for certain browsers
    header("Content-type: application/csv"); // I always use this
    header("Content-Disposition: attachment; filename=retrieveClassNames.csv");
    header("Content-Transfer-Encoding: binary");
   
    $url_ = 'http://' . $config['host'] . ":" . $config['port'] . "/" . $config['heart_index'] . "/Application/_search";
    
    $response = \Httpful\Request::get($url_)->send();
    $json = json_decode($response, true);

    echo "[";
    $first = TRUE;
    
    foreach ($json['hits']['hits'] as $hits) {
        foreach ($hits['_source'] as $key => $field)
        {
            if($key == "urn:xdaq-application-descriptor:class")
            {
                if (!$first)
                {
                    echo ',';
                }
                echo '"' . $field . '"';
                $first = FALSE;
            }
        }
    }
    
    echo "]";
?>