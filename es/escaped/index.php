<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>
<link href="css/esxbeat.css" rel="stylesheet" />
<link href="css/tables.css" rel="stylesheet" />
<link href="css/xdaq-hyperdaq-app-widgets.css" rel="stylesheet"/>
<link type="text/css" href="css/ESheartbeat.css" rel="stylesheet" />
</head>
<body>

<?php
    
    ini_set('display_errors', 'Off');
    error_reporting(E_ALL | E_STRICT);
    
    include('config/config.php');
    echo '<b><center>' . $config['zone'] .' </center></b>';
    
    echo '<br />';
    
    echo '<div id="wrapper">';
    
    echo '<div id="hb-widget" class="xdaq-hyperdaq-home-widget-container" data-lid="4" data-id="3" data-name="xmas::heartbeat::probe::Application" data-service="heartbeatprobe" data-network="localnet">';
    echo '<a href="esxbeat.php"><img src="images/heartbeatd-icon.png" class="xdaq-hyperdaq-home-widget-image" alt="XDAQ Application Icon"></a>';
    echo '<div class="xdaq-hyperdaq-home-widget-text"><a href="/esxbeat.php">';
    echo '<span style="font-weight: 600;">ES XBEAT</span><br>';
    echo 'Service: heartbeat<br>';
    echo '<br>';
    echo '</a>';
    echo '</div>';
    echo '</div>';
    
    echo '<div id="dqlas-widget" class="xdaq-hyperdaq-home-widget-container" data-lid="4" data-id="3" data-name="xmas::heartbeat::probe::Application" data-service="heartbeatprobe" data-network="localnet">';
    echo '<a href="eslas.php"><img src="images/xmas-las2g-icon.png" class="xdaq-hyperdaq-home-widget-image" alt="XDAQ Application Icon"></a>';
    echo '<div class="xdaq-hyperdaq-home-widget-text"><a href="/eslas.php">';
    echo '<span style="font-weight: 600;">ES LAS</span><br>';
    echo 'Service: las<br>';
    echo '<br>';
    echo '</a>';
    echo '</div>';
    echo '</div>';  

    
    echo '</div>';//wrapper
    
    echo "</br>";
    echo "</br>";
    echo "</br>";
    
    ?>

</body>
</html>