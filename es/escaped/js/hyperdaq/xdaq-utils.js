
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest					 								 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			       			 *
 *************************************************************************/
  
/*
 * XDAQ HyperDAQ Utility jQuery/JS functions
 */
 
/*
 * General Purpose
 */ 
function isNumber(n) 
{
	return !isNaN(parseFloat(n)) && isFinite(n);
}

// returns time in format HH:MM::SS
function xdaqGetCurrentTime()
{
	var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    if (minutes < 10){
        minutes = "0" + minutes;
    }
    if (seconds < 10){
        seconds = "0" + seconds;
    }
    return (hours + ":" + minutes + ":" + seconds);
}

// helper function to retreive a value for a specific paramter supplied in the given url
// e.g. xdaqGetURLParameter("index.html?id=seven&start=true", "id") returns "seven"
function xdaqGetURLParameter(url, paramName)
{
	var searchURL = url.split('?')[1];
	var parameters = searchURL.split('&');

	for (var i = 0; i < parameters.length; i++)
	{
		var param = parameters[i].split('=');
		if (param[0] == paramName)
		{
			return param[1];
		}
	}
}

/*
 * XHTTPRequests
 */ 
 
var xdaqSOAPTEMPLATE_soapMsgTop = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">';
	xdaqSOAPTEMPLATE_soapMsgTop = xdaqSOAPTEMPLATE_soapMsgTop + '<SOAP-ENV:Header>';
	xdaqSOAPTEMPLATE_soapMsgTop = xdaqSOAPTEMPLATE_soapMsgTop + '</SOAP-ENV:Header>';
	xdaqSOAPTEMPLATE_soapMsgTop = xdaqSOAPTEMPLATE_soapMsgTop + '<SOAP-ENV:Body>';
	xdaqSOAPTEMPLATE_soapMsgTop = xdaqSOAPTEMPLATE_soapMsgTop + '<xdaq:';
var xdaqSOAPTEMPLATE_soapMsgCommandFill = ' xmlns:xdaq="urn:xdaq-soap:3.0">';
    xdaqSOAPTEMPLATE_soapMsgCommandFill = xdaqSOAPTEMPLATE_soapMsgCommandFill + '</xdaq:';
var xdaqSOAPTEMPLATE_soapMsgFoot = '>';
	xdaqSOAPTEMPLATE_soapMsgFoot = xdaqSOAPTEMPLATE_soapMsgFoot + '</SOAP-ENV:Body>';
	xdaqSOAPTEMPLATE_soapMsgFoot = xdaqSOAPTEMPLATE_soapMsgFoot + '</SOAP-ENV:Envelope>';
 
function xdaqGetSOAPMessage(command)
{
	return xdaqSOAPTEMPLATE_soapMsgTop+command+xdaqSOAPTEMPLATE_soapMsgCommandFill+command+xdaqSOAPTEMPLATE_soapMsgFoot;
}

// light wrapper for jQuery $.ajax
function xdaqAJAX(options, callback)
{
	// URL
	if (!("url" in options))
	{
		console.error("No URL provided for ajax request");
		console.error(options);
		return;
	}

	// TYPE - override default to "POST"
	if (!("type" in options))
	{
		options["type"] = "POST";
	}

	// processData - override default to false
	if (!("processData" in options))
	{
		options["processData"] = false;
	}

	// ERROR CALLBACK
	// Type: Function( jqXHR jqXHR, String textStatus, String errorThrown )
	if (!("error" in options))
	{
		options["error"] = function (xhr, textStatus, errorThrown) {
			console.error(xhr.status);
			console.error(errorThrown);
		};
	}
	
	// Check for callback implementation
	if (callback == null || callback == undefined)
	{
		callback = function(data, textStatus, xhr) {};
	}

	//return $.ajax(options).done(callback).fail(options["error"]);
	return $.ajax(options).done(callback);
}

// DEPRECATED
function xdaqSendXHTTP(url, content, context, callback) {
	var xmlhttp = xdaqGetXHTTP();
	xmlhttp.open('POST', url, true);

	xmlhttp.onreadystatechange = function() {callback(xmlhttp, context);};
	
	// Send the POST request
	console.log("Using deprecated call, use 'xdaqAJAX' instead");
	xmlhttp.send(content);
}
// DEPRACTED
function xdaqGetXHTTP()
{
	console.log("Using deprecated call, see 'xdaqAJAX' instead");
	if (window.XMLHttpRequest)
	{
		// code for IE7+, Firefox, Chrome, Opera, Safari
		return new XMLHttpRequest();
	}
	else
	{
		// code for IE6, IE5
		console.log("IE<6");
		return new ActiveXObject("Microsoft.XMLHTTP");
	}
}
 
/*
 * sessionStorage
 */ 

/*
 * Throws on error:
 * -1 : error
 * -2 : not supported
 */
function xdaqSaveSessionStorage(tag, val)
{
	if(typeof(Storage)!=="undefined")
	{
		try
		{
			sessionStorage.setItem(window.location.pathname+tag, val);
		}
		catch (err)
		{
			err.status = -1;
			throw err;
		}
	}
	else
	{
		var err = new Error("Browser does not support sessionStorage");
		err.status = -2;
		throw err;
	}
}
/*
 * Throws on error:
 * -1 : error
 * -2 : not supported
 */
function xdaqLoadSessionStorage(tag)
{
	if(typeof(Storage)!=="undefined")
	{
		try
		{
			return sessionStorage.getItem(window.location.pathname+tag);
		}
		catch (err)
		{
			err.status = -1;
			throw err;
		}
	}
	else
	{
		var err = new Error("Browser does not support sessionStorage");
		err.status = -2;
		throw err;
	}
}

/*
 * Parameter Viewers
 */ 
 
var xdaqParamBuilderIndex = 0;

var xdaqParamBuilderTopElementName = "p:properties";
var xdaqParamBuilderTopElementNameWebKit = "properties";
 
function xdaqBuildParamViewers()
{
	$(".xdaq-param-viewer").each(function() {
		//console.log("Building paramviewer : " + $(this).attr("id"));
		
		var table = $(this);
		var tbody = table.children("tbody").first();
		
		var requestURL = table.attr("data-requesturl");
		var soapAction = table.attr("data-soapaction");
						
		var options = {
			headers: {
				"SOAPAction": soapAction
			},
			url: requestURL,
			data: xdaqGetSOAPMessage("ParameterQuery"),
			error: function (xhr, textStatus, errorThrown) {
				console.error("XDAQ Parameter Viewer Builder : Failed to get properties from URL '" + requestURL + "', error:");
				console.error(errorThrown);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			
			var xmlDoc = xhr.responseXML;
			
			var propertiesElement = xmlDoc.getElementsByTagName(xdaqParamBuilderTopElementName);
			if (propertiesElement.length == 0)
			{
				propertiesElement = xmlDoc.getElementsByTagName(xdaqParamBuilderTopElementNameWebKit);
			}
			
			if (propertiesElement.length == 0)
			{
				console.error("Could not find properties in XML document for parameter viewer");
				return;
			}
			
			var x=propertiesElement[0].childNodes;
			var y=propertiesElement[0].firstChild;
						
			xdaqParamBuilderIndex = 0;
			
			for (var i=0; i<x.length; i++)
			{
				if (y.nodeType == 1)
				{
					xdaqParamViewerAddElement(tbody, y);
				}
				y = y.nextSibling;
			}
			
			xdaqBuildTree(table);
			
		});
	});
}

function xdaqParamViewerAddHandlers()
{
	
	$(".xdaqparamviewer-get").click(function(e) {
	//$("#xdaq-main-wrapper").on("click", ".xdaqparamviewer-get", function(e) {
		
		//console.log($(this));
		var tableName = $(this).attr("data-linkedtable");
		//console.log(tableName);
		
		//var table = $("#"+tableName);
		var table = $(document.getElementById(tableName));
		
		//console.log(table);
		
		var tbody = table.children("tbody").first();
	
		var requestURL = table.attr("data-requesturl");
		var soapAction = table.attr("data-soapaction");
		
		console.log(requestURL);
						
		var options = {
			headers: {
				"SOAPAction": soapAction
			},
			url: requestURL,
			data: xdaqGetSOAPMessage("ParameterQuery"),
			error: function (xhr, textStatus, errorThrown) {
				console.error("XDAQ Parameter Viewer : Failed to get properties from URL '" + requestURL + "', error:");
				console.error(errorThrown);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			var xmlDoc = xhr.responseXML;
			
			if (xmlDoc == undefined)
			{
				console.error("Failed to get parameters");
				return;
			}
				
			var propertiesElement = xmlDoc.getElementsByTagName(xdaqParamBuilderTopElementName);
			if (propertiesElement.length == 0)
			{
				propertiesElement = xmlDoc.getElementsByTagName(xdaqParamBuilderTopElementNameWebKit);
			}
			
			if (propertiesElement.length == 0)
			{
				console.error("Could not find properties in XML document for parameter viewer");
				return;
			}
			
			var x=propertiesElement[0].childNodes;
			var y=propertiesElement[0].firstChild;
						
			rowID = 0;
			
			for (i=0;i<x.length;i++)
			{
				if (y.nodeType == 1)
				{
					xdaqParamViewerUpdateElement(tbody, y);
				}
				y=y.nextSibling;
			}
		});
	});
	
	$('#xdaqparamviewer-menu-refresh').click(function(e) {
		var img = $(this);
		img.addClass("xdaqparamviewer-menu-refresh-rotater");
		setTimeout(function(){
			img.removeClass("xdaqparamviewer-menu-refresh-rotater");
			//console.log("done");
		},900);
		e.stopPropagation();
	});
}

function xdaqParamViewerAddElement(tbody, el)
{	
	if (el != undefined && el != null)
	{
		xdaqParamViewerAddParentRow(tbody, el);
		var parentID = xdaqParamBuilderIndex;		
		xdaqParamBuilderIndex = xdaqParamBuilderIndex + 1;
			
		/* nodes children */
		var children = el.childNodes;
		if (children.length != 0)
		{	
			var i = 0; 
			for (;i<children.length;i++)
			{
				var y = children[i];
				
				if (y.nodeType == 1)
				{
					xdaqParamViewerAddElementChild(tbody, y, parentID);
				}
				y=y.nextSibling;
			}
		}	
	}
}

function xdaqParamViewerAddElementChild(tbody, el, parentID)
{	
	if (el != undefined && el != null)
	{
		xdaqParamViewerAddChildRow(tbody, el, parentID);
		var newParentID = xdaqParamBuilderIndex;		
		xdaqParamBuilderIndex = xdaqParamBuilderIndex + 1;
		
			
		/* nodes children */
		var children = el.childNodes;
		if (children.length != 0)
		{	
			var i = 0; 
			for (;i<children.length;i++)
			{
				var y = children[i];
				
				if (y.nodeType == 1)
				{
					xdaqParamViewerAddElementChild(tbody, y, newParentID);
				}
				y=y.nextSibling;
			}
		}	
	}
}

function xdaqParamViewerAddParentRow(tbody, el)
{
	var newRow = '<tr data-treeid="'+xdaqParamBuilderIndex+'">';
	xdaqParamViewerFinishRow(tbody, newRow, el);
}

function xdaqParamViewerAddChildRow(tbody, el, pid)
{
	var newRow = '<tr data-treeid="'+xdaqParamBuilderIndex+'" data-treeparent="'+pid+'">';
	xdaqParamViewerFinishRow(tbody, newRow, el);
}

function xdaqParamViewerFinishRow(tbody, start, el)
{
	var atts = el.getAttribute('xsi:type');
	var val = "";
	
	var largeTable = (tbody.parent().attr("data-tablesize") == "large");
	
	if (el.hasChildNodes() && el.childNodes[0].nodeValue != null && el.childNodes[0].nodeValue.trim() != "")
	{
		if (largeTable)
		{
			val = '<input type="text" style="width: 300px" value="' + el.childNodes[0].nodeValue.trim() + '">';
		}
		else
		{
			val = el.childNodes[0].nodeValue.trim();
		}
	}
	
	var newRow;
	if (largeTable)
	{
		newRow = start + '<td>' + el.nodeName.substring(2) + '</td><td><input type=\"checkbox\"></td><td>' + atts + '</td><td>' + val + '</td></tr>';
	}
	else
	{
		newRow = start + '<td>' + el.nodeName.substring(2) + '</td><td class="xdaq-flash-idle">' + val + '</td></tr>';
	}
	tbody.append(newRow);
}

function xdaqParamViewerUpdateElement(tbody, el)
{	
	if (el != undefined && el != null)
	{				
		rowID = rowID + 1;
		
		/* Update Values */		
		if (el.hasChildNodes() && el.childNodes[0].nodeValue != null && el.childNodes[0].nodeValue.trim() != "")
		{
			var row = tbody.children("tr:nth-child("+rowID+")");
			
			if (tbody.parent().attr("data-tablesize") == "large")
			{
				var selected = row.find("td input:checkbox:checked");
				if (selected.length > 0)
				{
					var val = el.childNodes[0].nodeValue.trim();
					//console.log("try update for row "+rowID+" with value "+val);
					var cur = row.find("td input[type=text]").first();
					//console.log(rowID+" = "+cur.val()+" -> "+val);
					cur.val(val);
				}
			}
			else
			{
				var val = el.childNodes[0].nodeValue.trim();
				var cell = row.children().eq(1);

				if (cell.html() != val)
				{
					var flashClass = "xdaq-flash-active-green";
					cell.addClass(flashClass);
					setTimeout(function(){
						cell.removeClass(flashClass);
						//console.log("done");
					},200);
				}
				
				cell.html(val);
			}
		}

		/* nodes children */
		var children = el.childNodes;
		if (children.length != 0)
		{	
			var i = 0; 
			for (;i<children.length;i++)
			{
				var y = children[i];
				
				if (y.nodeType == 1)
				{
					xdaqParamViewerUpdateElement(tbody, y);
				}
				y=y.nextSibling;
			}
		}	
	}
}

function xdaqAddTextAreaHandlers()
{
	// Set desired tab- defaults to four space softtab
	var tab = "\t";
	
	$("textarea").keydown(function(evt) {
	    var t = evt.target;
	    var ss = t.selectionStart;
	    var se = t.selectionEnd;
	 
	    // Tab key - insert tab expansion
	    if (evt.keyCode == 9) {
	        evt.preventDefault();
	               
	        if (!evt.shiftKey)
	        {
				// Special case of multi line selection
				if (ss != se && t.value.slice(ss,se).indexOf("\n") != -1) {
					// In case selection was not of entire lines (e.g. selection begins in the middle of a line)
					// we ought to tab at the beginning as well as at the start of every following line.
					var start = t.value.slice(0,ss).lastIndexOf("\n") + 1;
					var pre = t.value.slice(0,start);
					var sel = t.value.slice(start,se).replace(/\n/g,"\n"+tab);
					var post = t.value.slice(se,t.value.length);
					t.value = pre.concat(tab).concat(sel).concat(post);
						   
					t.selectionStart = ss + tab.length;
					t.selectionEnd = se + tab.length;
				}
					   
				// "Normal" case (no selection or selection on one line only)
				else {
					t.value = t.value.slice(0,ss).concat(tab).concat(t.value.slice(ss,t.value.length));
					if (ss == se) {
						t.selectionStart = t.selectionEnd = ss + tab.length;
					}
					else {
						t.selectionStart = ss + tab.length;
						t.selectionEnd = se + tab.length;
					}
				}
	        }
	        else
	        {
	        	if (ss != se && t.value.slice(ss,se).indexOf("\n") != -1) {
	        		var start = t.value.slice(0,ss).lastIndexOf("\n");
	        		//if (t.value.slice(start,start + 1) == tab)
	        		var pre = t.value.slice(0,start);
	        		var old = t.value.slice(start,se);
					var sel = t.value.slice(start,se).replace(/\n\t/g,"\n");
					var post = t.value.slice(se,t.value.length);
					t.value = pre.concat(sel).concat(post);
						   
					if (old != sel)
					{
						t.selectionStart = ss - tab.length;
						t.selectionEnd = se - tab.length;
					}
				}
					   
				// "Normal" case (no selection or selection on one line only)
				else {
					if (t.value.slice(ss - 1,ss) == "\t")
					{
						var pre = t.value.slice(0,ss - 1);
						var post = t.value.slice(ss,t.value.length);
						t.value = pre.concat(post);
						
						t.selectionStart = ss - 1;
						t.selectionEnd = se - 1;
					}	
				}
	        }
	    }
	});
}