/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/05/15 13:03:20 $
 *
 *
 **/
#include "jal/jtagController/version.h"
#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(jaljtagController)

void jaljtagController::checkPackageDependencies()
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
}

std::set<std::string, std::less<std::string> > jaljtagController::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	 
	return dependencies;
}	
	
