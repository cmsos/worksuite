#include "jal/jtagController/CMSDAQTurboJTAGController.h"
#include "hal/HardwareDeviceInterface.hh"
#include "hal/HardwareAccessException.hh"

#include <iostream>
#include <iomanip>

#include <stdint.h>

using namespace std;
using namespace HAL;

jal::CMSDAQTurboJTAGController::CMSDAQTurboJTAGController(HardwareDeviceInterface& device, 
					   string const& jtag_reg_prefix,
					   bool simulatePulsing,
					   double SCKFrequency)
  : _device(device), 
    _jtag_reg_prefix(jtag_reg_prefix), 
    _simulatepulsing(simulatePulsing), 
    _sck_frequency(SCKFrequency),
    _desiredTCKfrequency(-1.), 
    _debug_flag(0), 
    _initialized(false),
    _timer() {
}

jal::CMSDAQTurboJTAGController::~CMSDAQTurboJTAGController() {

  if (_initialized) {
    _device.write(_jtag_reg_prefix+"ENABLE", 0x0);
  }
}

/** lock the chain. Useful if multiple processes are using the chain concurrently */
void jal::CMSDAQTurboJTAGController::lock() {
  //TBD
};


void jal::CMSDAQTurboJTAGController::unlock() {
  //TBD 
};


void jal::CMSDAQTurboJTAGController::shift(uint32_t num_bits, 
				      vector<uint8_t> const& data_out, 
				      vector<uint8_t> &  data_in,
				      bool doRead, 
				      bool autoTMS) {

  if (!_initialized) init();

  uint32_t num_bytes = (num_bits+7) / 8;


  if (_debug_flag >= 4) {
    std::cout << "shift() called. " << std::dec << num_bits << " bits, TMS_high=" << autoTMS << "; ";
    if (data_out.size() != 0) {
      std::cout << "write = ";
      for (int i=num_bytes-1; i>=0; i--) { std::cout << std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_out[i]; }
      std::cout << "; ";
    }
  }

  // grow the vector for response data if necessary
  if (doRead && data_in.capacity() < num_bytes) data_in.resize(num_bytes); 

  uint32_t mask32 = 0;
  uint32_t tms32 = 0;
  uint32_t tdi32 = 0;

  uint32_t bitcount32 = 0;

  for (uint32_t i=0;i<num_bits; i++) {

    uint32_t byte_index = i/8;
    uint8_t bit_index = i%8;
    uint8_t bit_mask = 1 << bit_index;

    mask32 |= ( 1 << bitcount32 );

    if (byte_index < num_bytes &&  (data_out[byte_index] & bit_mask) == bit_mask  )
      tdi32 |= ( 1 << bitcount32 );

    if (autoTMS && i == num_bits-1)
      tms32 |= ( 1 << bitcount32 );
    

    if ( bitcount32 == 31 || i == num_bits-1 ) { 
      // now we have accumulated 32 bits or we are at the last bit
      if (i<=31 || i == num_bits-1) {
	// in first and last longword we need to write the mask32 and tms32
	_device.unmaskedWrite(_jtag_reg_prefix+"MASK32", mask32);
	_device.unmaskedWrite(_jtag_reg_prefix+"TMS32", tms32);	
      }

      _device.unmaskedWrite(_jtag_reg_prefix+"TDI32", tdi32);

      mask32 = tms32 = tdi32 = 0;

      // Always read TDO - this will delay until the JTAG access is finished
      uint32_t tdo32;
      _device.unmaskedRead(_jtag_reg_prefix+"TDO32", &tdo32);
      if (doRead) {

	for (uint32_t j = 0; j < bitcount32+1; j++) {
	  
	  uint32_t rdbyte_index = (i-bitcount32+j)/8;
	  uint8_t rdbit_index = (i-bitcount32+j)%8;
	  uint8_t rdbit_mask = 1 << rdbit_index;

	  if (rdbit_index == 0) data_in[rdbyte_index]=0;

	  if ( tdo32 & (1<<j) )
	    data_in[rdbyte_index] |= rdbit_mask;
	}
      }
    }
    bitcount32 = (bitcount32 + 1) % 32;
  }

  if (doRead &&_debug_flag >= 4) {
    std::cout << "read = ";
    for (int i=(num_bits+7)/8-1; i>=0; i--) {std::cout << std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_in[i]; }
    std::cout << "; ";
  }
    
  if (_debug_flag >= 4) std::cout << std::endl;
}



void jal::CMSDAQTurboJTAGController::sequenceTMS(uint32_t num_bits, uint32_t tms) {

  if (!_initialized) init();

  if (_debug_flag >= 4) {
    std::cout << "sequenceTMS(" << std::dec << tms << ", num=" << num_bits << ") called: ";
    for (int i=num_bits-1;i>=0; i--) std::cout << ( (tms & (1 << i)) ? "1" : "0" );
    std::cout << std::endl << std::endl;
  }

  if (num_bits>32) 
    XCEPT_RAISE(jal::OutOfRangeException, "sequenceTMS(): num_bits out of range");

  if (num_bits == 0) {
    if (_debug_flag >= 4) std::cout << "numbits is zero. returning." << std::endl;
    return;
  }

  uint32_t mask32 = 0;
  for (uint32_t i=0;i<num_bits;i++) mask32 |= (1<<i);
  _device.unmaskedWrite(_jtag_reg_prefix+"MASK32", mask32);

  uint32_t tms32 = (uint32_t) tms;
  _device.unmaskedWrite(_jtag_reg_prefix+"TMS32", tms32);

  uint32_t tdi32 = 0;
  _device.unmaskedWrite(_jtag_reg_prefix+"TDI32", tdi32);
  // Always read TDO - this will delay until the JTAG access is finished
  uint32_t tdo32;
  _device.unmaskedRead(_jtag_reg_prefix+"TDO32", &tdo32);
}


void jal::CMSDAQTurboJTAGController::pulseTCK(uint32_t num_tcks, bool tmshigh, jal::PulseStage stage) {



  if (!_initialized) init();

if (_debug_flag >= 4) std::cout << "pulseTCK(" << std::dec << num_tcks << ") called." << std::endl << std::endl;

  if (_desiredTCKfrequency == -1.)
    XCEPT_RAISE(jal::OutOfRangeException, "error: pulseTCK() was called before a desired frequency was specified.");

  if (stage == jal::PULSESTAGE_PRE || stage == jal::PULSESTAGE_ALL) {
    //
    // do at least 32 clocks before sleeping
    // (at least two clocks needed by EPC4, EPC8, EPC16)
    //
    // assumption is that some extra clocks do not hurt ..
    //  
    uint32_t mask32 = 0xffffffff;
    _device.unmaskedWrite(_jtag_reg_prefix+"MASK32", mask32);

    uint32_t tms32 = tmshigh ? 1 :0;
    _device.unmaskedWrite(_jtag_reg_prefix+"TMS32", tms32);

    uint32_t tdi32 = 0;
    _device.unmaskedWrite(_jtag_reg_prefix+"TDI32", tdi32);
    // Always read TDO - this will delay until the JTAG access is finished
    uint32_t tdo32;
    _device.unmaskedRead(_jtag_reg_prefix+"TDO32", &tdo32);
  }
  
  if (num_tcks>32) num_tcks-=32;

  // return if there was only one clock to do
  if (num_tcks==0) return;

  if (_simulatepulsing) {

    if (stage == jal::PULSESTAGE_PAUSE || stage == jal::PULSESTAGE_ALL) {
      double t_sleep = num_tcks / _desiredTCKfrequency;    
      _timer.sleepMicros( (uint32_t) (t_sleep * 1.e6) );
    }

  } 
  else {

    if (stage == jal::PULSESTAGE_PRE || stage == jal::PULSESTAGE_ALL) {
      // scale the number of clocks if both the system frequency and the desired frequency are known
      if ( _sck_frequency == -1.) 
	XCEPT_RAISE(jal::OutOfRangeException, "error: controller is in pulsing mode, but system frequency was not set.");
      else
	num_tcks = (uint32_t) ( (double) num_tcks * _sck_frequency / _desiredTCKfrequency );
     
      for (uint32_t i=0; i<(num_tcks+1)/32; i++) {
	uint32_t mask32 = 0xffffffff;
	_device.unmaskedWrite(_jtag_reg_prefix+"MASK32", mask32);

	uint32_t tms32 = tmshigh ? 0xffffffff :0x00000000;
	_device.unmaskedWrite(_jtag_reg_prefix+"TMS32", tms32);

	uint32_t tdi32 = 0;
	_device.unmaskedWrite(_jtag_reg_prefix+"TDI32", tdi32);
	uint32_t tdo32;
	_device.unmaskedRead(_jtag_reg_prefix+"TDO32", &tdo32);
      }
    }


  }

}


void jal::CMSDAQTurboJTAGController::init() { 

  try {
    _device.write(_jtag_reg_prefix+"ENABLE", 0x1);
    _initialized = true;
  }
  catch (HAL::HardwareAccessException &e) {
    XCEPT_RETHROW(jal::HardwareException, "error during JTAG read", e);
  }
}

