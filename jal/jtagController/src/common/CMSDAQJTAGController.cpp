#include "jal/jtagController/CMSDAQJTAGController.h"
#include "hal/PCIDevice.hh"
#include "hal/HardwareAccessException.hh"

#include <iostream>
#include <iomanip>
#include <stdint.h>

using namespace std;
using namespace HAL;

#define JTAG_TDO_BIT 7

#define JTAG_TDO (1 << JTAG_TDO_BIT)
#define JTAG_TDI (1 << 6)
#define JTAG_TMS (1 << 1)
#define JTAG_CLK (1 << 0)


jal::CMSDAQJTAGController::CMSDAQJTAGController(PCIDevice& device, 
					   string const& jtag_reg_prefix,
					   bool simulatePulsing,
					   double SCKFrequency)   
  : _device(device), 
    _jtag_reg_prefix(jtag_reg_prefix), 
    _simulatepulsing(simulatePulsing), 
    _sck_frequency(SCKFrequency),
    _jtdo_addr(0), 
    _jtag_addr(0), 
    _use_fast_access(false),
    _desiredTCKfrequency(-1.), 
    _debug_flag(0), 
    _initialized(false),
    _timer() {


  try {
    GeneralHardwareAddress const& ga_jtag = _device.getAddressTableInterface().getGeneralHardwareAddress(_jtag_reg_prefix+"TDI_TMS_CLK");
    GeneralHardwareAddress const& ga_jtdo = _device.getAddressTableInterface().getGeneralHardwareAddress(_jtag_reg_prefix+"TDO");

    if (ga_jtag.isConfigSpace() && ga_jtdo.isConfigSpace()) {
      _jtag_addr = ga_jtag.getAddress();
      _jtdo_addr = ga_jtdo.getAddress();
      _use_fast_access = true;
    }
  }
  catch (HAL::HardwareAccessException &e) {
    XCEPT_RETHROW(jal::HardwareException, "error in constructor", e);
  }
  

}

jal::CMSDAQJTAGController::~CMSDAQJTAGController() {

  if (_initialized) {
    _device.write(_jtag_reg_prefix+"ENABLE", 0x0);
  }
}

/** lock the chain. Useful if multiple processes are using the chain concurrently */
void jal::CMSDAQJTAGController::lock() {
  //TBD
};


void jal::CMSDAQJTAGController::unlock() {
  //TBD 
};


void jal::CMSDAQJTAGController::shift(uint32_t num_bits, 
				      vector<uint8_t> const& data_out, 
				      vector<uint8_t> &  data_in,
				      bool doRead, 
				      bool autoTMS) {

  if (!_initialized) init();

  uint32_t num_bytes = (num_bits+7) / 8;


  if (_debug_flag >= 4) {
    std::cout << "shift() called. " << std::dec << num_bits << " bits, TMS_high=" << autoTMS << "; ";
    if (data_out.size() != 0) {
      std::cout << "write = ";
      for (int i=num_bytes-1; i>=0; i--) { std::cout << std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_out[i]; }
      std::cout << "; ";
    }
  }

  // grow the vector for response data if necessary
  if (doRead && data_in.capacity() < num_bytes) data_in.resize(num_bytes); 

  for (uint32_t i=0;i<num_bits; i++) {

    uint32_t byte_index = i/8;
    uint8_t bit_index = i%8;
    uint8_t bit_mask = 1 << bit_index;

    uint8_t value = 0x0;
    if (byte_index < num_bytes &&  (data_out[byte_index] & bit_mask) == bit_mask  )
      value |= JTAG_TDI;
    
    if (autoTMS && i == num_bits-1)
      value |= JTAG_TMS;
    
    writeJTAG(value);
    writeJTAG(value | JTAG_CLK);

    if (doRead) {

      if (bit_index == 0) data_in[byte_index]=0;

      if (readJTAG()) // remark: this does not care whether bit 0 or bit 7 is set ...
	data_in[byte_index] |= bit_mask;
    }

  }

  if (doRead &&_debug_flag >= 4) {
    std::cout << "read = ";
    for (int i=(num_bits+7)/8-1; i>=0; i--) {std::cout << std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_in[i]; }
    std::cout << "; ";
  }
    
  if (_debug_flag >= 4) std::cout << std::endl;
}



void jal::CMSDAQJTAGController::sequenceTMS(uint32_t num_bits, uint32_t tms) {

  if (!_initialized) init();

  if (_debug_flag >= 4) {
    std::cout << "sequenceTMS(" << std::dec << tms << ", num=" << num_bits << ") called: ";
    for (int i=num_bits-1;i>=0; i--) std::cout <<  ((tms & (1 << i)) ? "1" : "0" );
    std::cout << std::endl << std::endl;
  }

  if (num_bits>16) 
    XCEPT_RAISE(jal::OutOfRangeException, "sequenceTMS(): num_bits out of range");

  if (num_bits == 0) {
    if (_debug_flag >= 4) std::cout << "numbits is zero. returning." << std::endl;
    return;
  }

  for (uint32_t i=0;i<num_bits; i++) {
    uint8_t value = 0x0;
    uint32_t bit_mask = (1<<i);
    value |= (   (tms & bit_mask)  ==  bit_mask  ) ? JTAG_TMS : 0;
    
    writeJTAG(value);
    writeJTAG(value | JTAG_CLK);
  }
}


void jal::CMSDAQJTAGController::pulseTCK(uint32_t num_tcks, bool tmshigh, jal::PulseStage stage) {

  if (!_initialized) init();

  if (_debug_flag >= 4) std::cout << "pulseTCK(" << std::dec << num_tcks << ") called." << std::endl << std::endl;

  if (_desiredTCKfrequency == -1.)
    XCEPT_RAISE(jal::OutOfRangeException, "error: pulseTCK() was called before a desired frequency was specified.");

  uint8_t value = tmshigh ? JTAG_TMS : 0; 
  //
  // do at least 2 clock cycles before sleeping
  //
  for (int i=0;i<2 && num_tcks>0;i++) {

    if (stage == jal::PULSESTAGE_PRE || stage == jal::PULSESTAGE_ALL) {
      writeJTAG(value);
      writeJTAG(value | JTAG_CLK);
    }  
    num_tcks--;
  }

  // return if there was only one clock to do
  if (num_tcks==0) return;

  if (_simulatepulsing) {
 
    if (stage == jal::PULSESTAGE_PAUSE || stage == jal::PULSESTAGE_ALL) {
      double t_sleep = num_tcks / _desiredTCKfrequency;
      _timer.sleepMicros( (uint32_t) (t_sleep * 1.e6) );
    }

  } 
  else {
    
    if (stage == jal::PULSESTAGE_PRE || stage == jal::PULSESTAGE_ALL) {
      // scale the number of clocks if both the system frequency and the desired frequency are known
      if ( _sck_frequency == -1.) 
	XCEPT_RAISE(jal::OutOfRangeException, "error: controller is in pulsing mode, but system frequency was not set.");
      else
	num_tcks = (uint32_t) ( (double) num_tcks * _sck_frequency / _desiredTCKfrequency );
     
      for (uint32_t i=0; i<num_tcks; i++) {
	writeJTAG(value);
	writeJTAG(value | JTAG_CLK);
      }
    }

  }
}


void jal::CMSDAQJTAGController::init() { 

  try {
    _device.write(_jtag_reg_prefix+"ENABLE", 0x1);
    _initialized = true;
  }
  catch (HAL::HardwareAccessException &e) {
    XCEPT_RETHROW(jal::HardwareException, "error during JTAG read", e);
  }
}


uint8_t jal::CMSDAQJTAGController::readJTAG() { 

  uint32_t d;


  try {
    
    // configRead() requires HAL ver 03-11 or above
    // if this is not available, just comment out the next five lines

    if (_use_fast_access) {
      _device.configRead(_jtdo_addr, &d);
      d = (d&JTAG_TDO) >> JTAG_TDO_BIT; 
    }
    else
      _device.read(_jtag_reg_prefix+"TDO", &d); 

  } 
  catch (HAL::HardwareAccessException &e) {
    XCEPT_RETHROW(jal::HardwareException, "error during JTAG read", e);
  }

  return (uint8_t) d;
}

void jal::CMSDAQJTAGController::writeJTAG(uint8_t data) { 

  uint32_t d = (uint32_t) data;

  try {

    // configWrite() requires HAL ver 03-11 or above
    // if this is not available, just comment out the next three lines
    
    if (_use_fast_access)
      _device.configWrite(_jtag_addr, d);
    else
      _device.write(_jtag_reg_prefix+"TDI_TMS_CLK", d);

  } 
  catch (HAL::HardwareAccessException &e) {
    XCEPT_RETHROW(jal::HardwareException, "error during JTAG write", e);
  }
}


