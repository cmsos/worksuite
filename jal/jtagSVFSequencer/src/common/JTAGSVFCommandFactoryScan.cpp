#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryScan.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandScan.h"
#include "jal/jtagSVFSequencer/JTAGSVFData.h"
#include "jal/jtagChain/JTAGScanData.h"

#include <stdio.h>
#include <sstream>

//
// The Scan command syntax is:
// ===========================
//
// XXX length [TDI (tdi)] [TDO (tdo)] [MASK (mask)] [SMASK (smask)];
//  
//
// attention: the above order of parameters is the standard.
//            xilinx also gives the parameters in different order (smask after TDI).
//            the method below works independent of the order of parameters

using namespace std;

jal::JTAGSVFCommand* jal::JTAGSVFCommandFactoryScan::create(vector<string> const& args) {

  if (args.size() < 2) 
    XCEPT_RAISE(jal::SVFSyntaxException, "Scan command needs at least one parameter.");
  
  // read length
  uint32_t length = 0;

  std::istringstream iss(args[1]);  
  bool success = static_cast<bool> ( iss >> std::dec >> length ); 

  if (!success) 
    XCEPT_RAISE (jal::SVFSyntaxException, "wrong data format for length field.");

  jal::JTAGSVFData svfdata;
  svfdata.setBitcount(length);

  vector<string>::const_iterator it = args.begin()+2;
  bool tdi_specified = false;

  while ( it != args.end() ) {

    if ( it+1 == args.end() )
      XCEPT_RAISE(jal::SVFSyntaxException, "parameter must be followed by pattern.");

    if ( (*it) == "TDI" ) 
      { svfdata.setData( *(++it) ); tdi_specified = true; }
    else if ( (*it) == "TDO"  ) 
      svfdata.setResponse( *(++it) ); 
    else if ( (*it) == "MASK" ) 
      svfdata.setMask ( *(++it) );
    else if ( (*it) == "SMASK") 
      svfdata.setSMask ( *(++it) ); 
    else
      XCEPT_RAISE(jal::SVFSyntaxException, "Unknown parameter on Scan command.");

    it++;
  }

  if ( tdi_specified == false  && length!=0 && 
       ( _first || (length != _previous_length) ) )
      XCEPT_RAISE(jal::SVFSyntaxException, "Missing TDI parameter after changing length of scan pattern.");



  _first = false;
  _previous_length = length;

  return (jal::JTAGSVFCommand *) new jal::JTAGSVFCommandScan(svfdata, _my_cmdtype);
}

