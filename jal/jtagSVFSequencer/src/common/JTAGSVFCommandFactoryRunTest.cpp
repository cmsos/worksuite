#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryRunTest.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandRunTest.h"

#include "jal/jtagChain/JTAGState.h"


#include <stdio.h>
#include <stdint.h>
#include <sstream>

using namespace std;

// Syntax of RUNTEST command is:
//
// RUNTEST [run_state] run_count run_clk [min_time SEC [MAXIMUM max_time SEC]] [ENDSTATE end_state];
//
// or
//
// RUNTEST [run_state] min_time SEC [MAXIMUM max_time SEC] [ENDSTATE end_state];

//
// Attention: Violation of maximum time in runstate is not checked 
//            (this may happen if the JTAG Chain is in already in 
//            runstate before the runtest command)
//            
//


jal::JTAGSVFCommand* jal::JTAGSVFCommandFactoryRunTest::create(vector<string> const& args){

  uint32_t i=1;

  jal::JTAGState runstate(jal::JTAGState::UNDEF);
  //
  // parse run_state
  //
  if ( i<args.size() ) {
    
    // try to convert first parameter
    jal::JTAGState state(args[i]);
    if (state != jal::JTAGState::UNDEF) {
      if ( ! state.isStable() ) 
	XCEPT_RAISE(jal::SVFSyntaxException, "runstate must be stable.");
      runstate = state;
      i++;
    }      
  }

  uint32_t nclk = 0;
  bool sysclock = false;
  //
  // parse nclk
  //
  if ( i+1<args.size() ) {
    if (args[i+1] == "SCK" || args[i+1] == "TCK") {

      std::istringstream iss ( args[i] );
      bool success = static_cast<bool> ( iss >> std::dec >> nclk );
      if ( ! success ) 
	XCEPT_RAISE(jal::SVFSyntaxException, "TCK or SCK parameter must be preceeded by and integer.");
      
      if (nclk == 0)
	XCEPT_RAISE(jal::SVFSyntaxException, "run_count must be greater than or equal to 1.");

      sysclock = (args[i+1] == "SCK");
      i += 2;
    }
  }

  double mintime = -1.;
  double maxtime = -1.;
  //
  // parse mintime and maxtime
  //
  if ( i+1<args.size() && args[i+1] == "SEC") {
    
    std::istringstream iss ( args[i] );
    bool success = static_cast<bool> ( iss >> mintime );
    if ( ! success ) 
      XCEPT_RAISE(jal::SVFSyntaxException, "mintime parameter must be a double.");
      
    i+=2;

    //
    // parse maxtime only if there was a mintime parameter
    //

    if ( i+2 < args.size() && args[i] == "MAXTIME" && args[i+2] == "SEC") {
	
      std::istringstream iss1 ( args[i+1] );
      bool success1 = static_cast<bool> ( iss1 >> maxtime );
      if ( ! success1 ) 
	XCEPT_RAISE(jal::SVFSyntaxException, "maxtime parameter must be a double.");
      
      i+=3;
      
    }

  }
  
  if (nclk == 0 && mintime == -.1)
    XCEPT_RAISE(jal::SVFSyntaxException, "either run_count or min_time must be specified.");
      

  jal::JTAGState endstate(jal::JTAGState::UNDEF);
  //
  // parse endstate
  //
  if ( i+1 < args.size() && args[i] == "ENDSTATE") {
    
    // try to convert first parameter
    jal::JTAGState state(args[i+1]);
    if (state == jal::JTAGState::UNDEF) 
      XCEPT_RAISE(jal::SVFSyntaxException, "ENDSTATE must be followed by a JTAG state.");

    if ( ! state.isStable() ) 
      XCEPT_RAISE(jal::SVFSyntaxException, "endstate must be stable.");
      
    endstate = state;
    i+=2;
  }  

  if (i<args.size()) 
    XCEPT_RAISE(jal::SVFSyntaxException, "too many arguments.");
    

  return (jal::JTAGSVFCommand*) new jal::JTAGSVFCommandRunTest(nclk, runstate, endstate, mintime, maxtime, sysclock);
}


