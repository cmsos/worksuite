#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2022, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# Project level Makefile
#
##

BUILD_HOME:=$(shell pwd)

ifndef PACKAGES
PACKAGES= \
	extern/dim \
	extern/smi \
	extern/yui \
	extern/caen/a2818 \
	extern/caen/a3818 \
	extern/caen/caenvmelib \
	extern/caen/caencomm \
	extern/caen/caenbridgeupgrade \
	extern/caen/caenupgrader \
	xdaq2rc \
	interface/evb \
	interface/shared \
	interface/bril/shared \
	evb \
	jobcontrol \
	xpci/drv \
	xpci \
	hal/generic \
	hal/utilities \
	hal/busAdapter/dummy \
	hal/busAdapter/caen \
	hal/busAdapter/pci \
	hal/busAdapter/axi \
	hal/PyHAL \
	pheaps/drv/cmem_rcc \
	pheaps \
	jal/jtagChain \
	jal/jtagController \
	jal/jtagSVFSequencer \
	ttc/utils \
	ttc/monitoring \
	ttc/ltc \
	ttc/ttcci \
	tts/ttsbase \
	tts/atts \
	tts/cpcibase \
	tts/fmmtd \
	tts/ipcutils \
	tts/fmm \
	tts/fmmcontroller \
	tts/fmmdbi \
	tts/fmmtester \
	d2s/utils \
	d2s/fedemulator \
	d2s/gtpe \
	d2s/gtpecontroller \
	d2s/firmwareloader \
	fedstreamer \
	fedkit \
	ferol \
	ferol40 \
	psx/sapi \
	psx/mapi \
	psx \
	psx/watchdog \
	sentinel \
	sentinel/utils \
	sentinel/probe \
	sentinel/sentineld \
	sentinel/tester \
	sentinel/bridge2g \
	sentinel/spotlight2g \
	sentinel/spotlightocci \
	sentinel/xmasforward \
	tstore/utils \
	tstore/client \
	tstore \
	tstore/api \
	xmas/admin \
	xmas/heartbeat \
	xmas/heartbeat/probe \
	xmas/heartbeat/heartbeatd \
	xmas/utils \
	xmas/sensord \
	xmas/probe \
	xmas/tester \
	xmas/bridge2g \
	xmas/slash2g \
	xmas/store2g \
	xmas/smarthub \
	ibvla \
	pt/ibv \
	gevb2g \
	elastic/api \
	elastic/timestream \
	amc13controller \
	dipbridge \
	oms \
	pt/vpi
endif
export PACKAGES

BUILD_SUPPORT=build
export BUILD_SUPPORT

PROJECT_NAME=worksuite
export PROJECT_NAME

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules

Project=$(PROJECT_NAME)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Packages=$(PACKAGES)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
