#ifndef __StreamConfigInfoSpaceHandler
#define __StreamConfigInfoSpaceHandler

#include "ferol40/StreamInfoSpaceHandler.hh"
#include "d2s/utils/DataTracker.hh"

namespace ferol40
{
    class StreamConfigInfoSpaceHandler:public ferol40::StreamInfoSpaceHandler {
    public:
        StreamConfigInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS );

        void registerTrackerItems( utils::DataTracker &tracker );
  
    private:
    };
}

#endif /* __StreamConfigInfoSpaceHandler */
