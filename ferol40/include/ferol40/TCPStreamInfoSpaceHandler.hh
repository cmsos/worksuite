#ifndef __TCPStreamInfoSpaceHandler
#define __TCPStreamInfoSpaceHandler

#include "ferol40/StreamInfoSpaceHandler.hh"
#include "d2s/utils/DataTracker.hh"

namespace ferol40
{
    class TCPStreamInfoSpaceHandler:public ferol40::StreamInfoSpaceHandler {
    public:
        TCPStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater, utils::InfoSpaceHandler *appIS );

        void registerTrackerItems( utils::DataTracker &tracker );
  
    private:
    };
}

#endif /* __TCPStreamInfoSpaceHandler */
