#ifndef __SlinkStream
#define __SlinkStream

#include "xdaq/Application.h"
#include "hal/PCIDevice.hh"
#include "log4cplus/logger.h"
#include "ferol40/ApplicationInfoSpaceHandler.hh"
#include "ferol40/FrlInfoSpaceHandler.hh"
#include "hal/PCIDevice.hh"
#include "d2s/utils/Exception.hh"

namespace ferol40 
{

    class SlinkStream {

    public:

        SlinkStream( Logger logger,
                     ApplicationInfoSpaceHandler &appIS,
                     FrlInfoSpaceHandler &frlIS,
                     HAL::PCIDevice *frlDevice,
                     HAL::PCIDevice *bridgeDevice,
                     uint32_t linkNumber );
    
        ~SlinkStream();

        void testSlink( uint32_t mseconds ) ;

        void deskewLink() ;

        void setDCBalance( bool dcBalanceFlag ) ;

        void setupFedEventGen( uint32_t meansize, 
                               uint32_t maxsize, 
                               uint32_t stdevsize, 
                               uint32_t meandelay, 
                               uint32_t stdevdelay, 
                               uint32_t n, 
                               uint32_t seed ) ;

        void setExpectedSourceId() ;

        void enable() ;

        uint32_t pendingTriggers() ;

        bool isActive();
        
        uint32_t getCmcFwVersion() { return cmcFwVersion_; }

    private:

        Logger logger_;
        ferol40::ApplicationInfoSpaceHandler &appIS_;
        ferol40::FrlInfoSpaceHandler &frlIS_;
        HAL::PCIDevice *frlDevice_P;
        HAL::PCIDevice *bridgeDevice_P;
        uint32_t linkNumber_;
        uint32_t expectedFedsrcid_;
        std::string expectedFedsrcIdItem_;
        uint32_t cmcFwVersion_;
        std::string enableItem_;
        std::string cmcFwItem_;
        std::string pendingTriggerItem_;
        bool DCBalanceActive_;
    };
}
#endif /* __SlinkStream */
