#ifndef __Ferol40StreamInfoSpaceHandler
#define __Ferol40StreamInfoSpaceHandler

#include "d2s/utils/StreamInfoSpaceHandler.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"

namespace ferol40
{
    class StreamInfoSpaceHandler : public utils::StreamInfoSpaceHandler 
    {

    public:
        

        StreamInfoSpaceHandler(  xdaq::Application *xdaq,
                                 std::string name,
                                 utils::InfoSpaceUpdater *updater, 
                                 utils::InfoSpaceHandler *appIS,
                                 bool noAutoPush = false );


        virtual void update();

    };
}

#endif /* __Ferol40StreamInfoSpaceHandler */
