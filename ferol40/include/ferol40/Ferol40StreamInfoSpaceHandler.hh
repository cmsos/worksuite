#ifndef __Ferol40StreamInfoSpaceHandler
#define __Ferol40StreamInfoSpaceHandler

#include "ferol40/StreamInfoSpaceHandler.hh"

namespace ferol40
{
    class Ferol40StreamInfoSpaceHandler : public ferol40::StreamInfoSpaceHandler 
    {

    public:
        
        enum HwInput { FEROL406G, FEROL4010G, FRL };

        Ferol40StreamInfoSpaceHandler(  xdaq::Application *xdaq,
                                      std::string name,
                                      utils::InfoSpaceHandler *appIS,
                                      bool noAutoPush = false );

        void createstring( std::string name, std::string frlHwName, std::string ferol406GHwName,
                           std::string ferol4010GHwName,
                           UpdateType frlUpdateType, UpdateType ferol40UpdateType, 
                           std::string format, std::string doc);

        void createuint32( std::string name, std::string frlHwName, std::string ferol406GHwName,
                           std::string ferol4010GHwName,
                           UpdateType frlUpdateType, UpdateType ferol40UpdateType, 
                           std::string format, std::string doc);

        void createuint64( std::string name, std::string frlHwName, std::string ferol406GHwName,
                           std::string ferol4010GHwName,
                           UpdateType frlUpdateType, UpdateType ferol40UpdateType, 
                           std::string format, std::string doc);

        void createdouble( std::string name, std::string frlHwName, std::string ferol406GHwName,
                           std::string ferol4010GHwName,
                           UpdateType frlUpdateType, UpdateType ferol40UpdateType, 
                           std::string format, std::string doc);

        void createbool  ( std::string name, std::string frlHwName, std::string ferol406GHwName,
                           std::string ferol4010GHwName,
                           UpdateType frlUpdateType, UpdateType ferol40UpdateType, 
                           std::string format, std::string doc);


        /**
         *
         *     @short This defines if the items are filled from SLINKexpress or 
         *            SLINK
         *            
         *            This routine is actually filling the infospace. 
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void setInputSource( HwInput inputSource );
        

    private:
        HwInput inputSource_;
        std::tr1::unordered_map< std::string, ISItem& > frlItemMap_;
        std::tr1::unordered_map< std::string, ISItem& > ferol406GItemMap_;
        std::tr1::unordered_map< std::string, ISItem& > ferol4010GItemMap_;
    };
}

#endif /* __Ferol40StreamInfoSpaceHandler */
