#ifndef __HardwareDebugItem
#define __HardwareDebugItem

#include <string>
#include <stdint.h>

namespace ferol40
{
    class utils::HardwareDebugItem {
    public:
        utils::HardwareDebugItem( std::string item,
                           std::string description,
                           std::string adrStr,
                           std::string valStr,
                           uint32_t address,
                           uint32_t value );

        std::string item;
        std::string description;
        std::string adrStr;
        std::string valStr;
        uint32_t address;
        uint32_t value;
    };
};
#endif /* __HardwareDebugItem */
