#ifndef __InfoSpaceHandler
#define __InfoSpaceHandler

#include <string>
#include <tr1/unordered_map>

#include "log4cplus/logger.h"

#include "xdaq/Application.h"
#include "xdata/InfoSpace.h"
#include "xdata/SimpleType.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Vector.h"


#include "d2s/utils/InfoSpaceUpdater.hh"

namespace ferol40 {
	 //JRF TODO move this struct into a sensible file. We will need to add another another form of bag for internal use which uses only standard variable types.
	 /**
         *  This struct is used only for the vector of streams containing the configuration parameters from the xml
         **/
        class StreamConf
        {
                public:
                        void registerFields (xdata::Bag<StreamConf>* bag)
                        {
                                bag->addField("enable", &enable);
				bag->addField("DataSource", &DataSource);
				bag->addField("N_Descriptors", &N_Descriptors);
				bag->addField("DestinationIP",&DestinationIP);
				bag->addField("SourceIP",&SourceIP);
				bag->addField("expectedFedId",&expectedFedId);
				bag->addField("TCP_DESTINATION_PORT",&TCP_DESTINATION_PORT);
				bag->addField("TCP_CONNECTION_ESTABLISHED",&TCP_CONNECTION_ESTABLISHED);//JRF TODO move to monitoring bag if we think we need persisitent vector in the infospace and cannot survive with the dynamically updated StreamInfoSpaces
                                bag->addField("TCP_SOURCE_PORT",&TCP_SOURCE_PORT);
				bag->addField("TCP_SOURCE_MAC_ADDRESS",&TCP_SOURCE_MAC_ADDRESS);//JRF TODO move to monitoring bag
				bag->addField("TCP_ARP_REPLY_OK",&TCP_ARP_REPLY_OK);//JRF TODO Move to monitoring Bag
				bag->addField("ENA_PAUSE_FRAME", &ENA_PAUSE_FRAME);
				//JRF NOTE: I have removed these becuase Petr said they are generic so we have only one of each for the whole FEROL40
				//bag->addField("TCP_CONFIGURATION",&TCP_CONFIGURATION);
                                //bag->addField("TCP_OPTIONS_MSS_SCALE",&TCP_OPTIONS_MSS_SCALE);
                                bag->addField("TCP_CWND",&TCP_CWND);
                                //bag->addField("TCP_TIMER_RTT",&TCP_TIMER_RTT);
				//bag->addField("TCP_TIMER_RTT_SYN",&TCP_TIMER_RTT_SYN);
				//bag->addField("TCP_REXMTTHRESH",&TCP_REXMTTHRESH);
                                //bag->addField("TCP_REXMTCWND_SHIFT",&TCP_REXMTCWND_SHIFT);
                                bag->addField("Event_Length_bytes",&Event_Length_bytes);
                                bag->addField("Event_Length_Stdev_bytes",&Event_Length_Stdev_bytes);
				bag->addField("Event_Length_Max_bytes",&Event_Length_Max_bytes);
                                bag->addField("Maximal_fragment_size",&Maximal_fragment_size);
                                bag->addField("Event_Delay_ns",&Event_Delay_ns);
                                bag->addField("Event_Delay_Stdev_ns",&Event_Delay_Stdev_ns);
				bag->addField("nb_frag_before_BP",&nb_frag_before_BP);
				bag->addField("Seed",&Seed);
                        }
                        xdata::Boolean enable;
			xdata::String DataSource;
			xdata::UnsignedInteger32 N_Descriptors;
			xdata::String DestinationIP;
			xdata::String SourceIP;
			xdata::UnsignedInteger32  expectedFedId; 
			xdata::UnsignedInteger32 TCP_DESTINATION_PORT;
			xdata::UnsignedInteger32 TCP_CONNECTION_ESTABLISHED;
                        xdata::UnsignedInteger32 TCP_SOURCE_PORT;
			xdata::UnsignedInteger64 TCP_SOURCE_MAC_ADDRESS;
			xdata::UnsignedInteger32 TCP_ARP_REPLY_OK;
			xdata::Boolean ENA_PAUSE_FRAME;
			//xdata::UnsignedInteger32 TCP_CONFIGURATION;
			//xdata::UnsignedInteger32 TCP_OPTIONS_MSS_SCALE;
			xdata::UnsignedInteger32 TCP_CWND;
			//xdata::UnsignedInteger32 TCP_TIMER_RTT;
			//xdata::UnsignedInteger32 TCP_TIMER_RTT_SYN;
			//xdata::UnsignedInteger32 TCP_REXMTTHRESH;
			//xdata::UnsignedInteger32 TCP_REXMTCWND_SHIFT;
			xdata::UnsignedInteger32 Event_Length_bytes;
			xdata::UnsignedInteger32 Event_Length_Stdev_bytes;
			xdata::UnsignedInteger32 Event_Length_Max_bytes;
			xdata::UnsignedInteger32 Maximal_fragment_size;
			xdata::UnsignedInteger32 Event_Delay_ns;
			xdata::UnsignedInteger32 Event_Delay_Stdev_ns;
			xdata::UnsignedInteger32 nb_frag_before_BP;
			xdata::UnsignedInteger32 Seed;

        };
    /************************************************************************
     *
     *
     *     @short A utility class to handle an Infospace in a threadsafe way.
     *            
     *            The  purpose of  this class  is to  ease the  handling of
     *            xdata variables  which are members of  an infospace. This
     *            class keeps copies of the  values of the variables in the
     *            infospace   which  can   be  read   or  written   by  the
     *            programmer.  The infospace  is  read or  written only  on
     *            request.
     *            
     *            The class also handles the  creation of xdata types to be
     *            filled into the infospace. 
     *
     *       @see 
     *    @author $Author: schwick $
     *   @version $Revision: 1.6 $
     *      @date $Date: 2001/03/06 17:07:51 $
     *
     *      Modif 
     *
     **//////////////////////////////////////////////////////////////////////
    class utils::InfoSpaceHandler {
    public:
        
        enum ItemType { UINT32, UINT64, BOOL, STRING, DOUBLE, STREAMS }; //JRF note that STREAM is a vector of bags and requires a definition class stream
        enum UpdateType { HW32, HW64, SLE, PROCESS, TRACKER, NOUPDATE };
        static const char * const UpdateTypeStrings[];

        /**
         * This struct is used during the update of the utils::InfoSpaceHandler
         **/
        class ISItem
        {
        public:
            ISItem( std::string name, std::string hwname, ItemType type, std::string format,
                    UpdateType update, std::string documentation)
                : name(name),
                  hwname(hwname),
                  type(type),
                  format(format),
                  update(update),
                  documentation(documentation),
                  suffix("")
            {};
            virtual ~ISItem() {};  //!< Make pointers/references to objects of this class polymorphic.
            virtual std::string gethwname() const { return hwname + suffix; };
            std::string name;      //!< The name of the item in the infospace.
            std::string hwname;    //!< The name of the item in the addresstable (for HW types only)
            ItemType type;
            std::string format;
            UpdateType update;
            std::string documentation;
            std::string suffix;
        };

//JRF TODO we need to add a createVector method to do this for us... 	xdata::Vector<xdata::Bag<StreamConf> > streams_;

        /**
         *
         *     @short Constructor for the existing Applicationinfospace.
         *            
         *            This  Constructor should  be chosen  for  the application 
         *            Infospace. Internally  another infospace with the
         *            name 'name'  will be created.  That one can then  be used 
         *            in  the  XMAS  monitoring.  All items  created  with  the 
         *            createxyz functions will be copied into both infospaces.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        utils::InfoSpaceHandler( xdaq::Application *xdaq,
                          xdata::InfoSpace  *is,
			  std::string name,
                          utils::InfoSpaceUpdater *updater = NULL,
                          bool noAutoPush = true );


        /**
         *
         *     @short Constructor which creates a new infospace.
         *            
         *            This Constructor  creates a  new infospace with  the name 
         *            given as a parameter. 
         *
         *     @param name: name of the infospace to be created.
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        utils::InfoSpaceHandler( xdaq::Application *xdaq,
                          std::string name,
                          utils::InfoSpaceUpdater *updater = NULL,
                          bool noAutoPush = false );
        
        std::string name();
        std::string isName();
        
        // returns a copy of the related ISItem
        bool exists( std::string name );
        bool isNoAutoPush();

        virtual void createstring( std::string name, std::string value, std::string format = "", UpdateType = PROCESS, std::string doc="" );
        virtual void createuint32( std::string name, uint32_t value, std::string format = "", UpdateType = HW32, std::string doc="" );
        virtual void createuint64( std::string name, uint64_t value, std::string format = "", UpdateType = HW64, std::string doc="" );
        virtual void createdouble( std::string name, double value, std::string format = "", UpdateType = PROCESS, std::string doc="" );
        virtual void createbool( std::string name, bool value, std::string format = "", UpdateType = HW32, std::string doc="" );
        virtual void createvector( std::string name, xdata::Vector<xdata::Bag<StreamConf> >  value, std::string format = "", UpdateType = HW64, std::string doc="" );
	void createDocumentationSeparator( std::string header = "" );

        /**
         * This function  sets a local  copy of an  infospace variable 
         * only. The Infospace variable itself is not touched. 
         * The set functions check for existence of the variable. 
         * They help to identify typos during programming.
         * If the variable 'push' is set to true, however, the new value of 
         * the variable is also pushed into the infospace.
         **/
        virtual void setstring( std::string name, std::string value, bool push=false );
        virtual void setuint32( std::string name, uint32_t value, bool push=false );
        virtual void setuint64( std::string name, uint64_t value, bool push=false );
        virtual void setuint64( std::string name, uint32_t low, uint32_t high, bool push=false );
        virtual void setbool( std::string name, bool value, bool push=false );
        virtual void setdouble( std::string name, double value, bool push=false );
	virtual void setvector( std::string name, xdata::Vector<xdata::Bag<StreamConf> >  value, bool push=false );
	virtual void setvectorelementuint32( std::string vectorName, std::string elementName, uint32_t value, uint32_t stream, bool push=false );	
	virtual void setvectorelementuint64( std::string vectorName, std::string elementName, uint64_t value, uint32_t stream, bool push=false ); 
        virtual void setvectorelementbool( std::string vectorName, std::string elementName, bool value, uint32_t stream, bool push=false );

	/**
         * This function gets a local copy of an infospace variable. 
         * The Infospace variable itself is not touched. 
         * The get functions check for existence of the variable. 
         * They help to identify typos during programming.
         **/
        std::string getstring( std::string name );
        uint32_t getuint32( std::string name );
        uint64_t getuint64( std::string name );
        bool getbool( std::string name );
        double getdouble( std::string name );
	xdata::Vector<xdata::Bag<StreamConf> > & getvector( std::string name );


        std::string getFormatted( std::string name, std::string format = "" );
        std::string getItemDoc( std::string item );

        /**
         *
         *     @short Writes all data from local variables into the Infospace Variables.
         *            
         *            The locking of the infospace is handled by this function.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void writeInfoSpace();



        /**
         *
         *     @short Copy infospace variables in their local counterparts.
         *            
         *            The copies are  then used by access functions  to be read 
         *            and  written.  The infospace  variables  are not  updated 
         *            until the function "writeInfoSpace()" is called.
         *
         *     @param 
         *    @return 
         *       @see writeInfoSpace
         *
         *      Modif 
         *
         **/
        void readInfoSpace();


        /**
         *
         *     @short Update the infospace in case a update handler is registered.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        virtual void update();


        /**
         * 
         *    @short Calls fireItemGroupChanged on the infospace.
         *
         *           Pushes the infospace into the XMAS monitoring system.
         *
         **/
        void pushInfospace() throw( xdata::exception::Exception );
        std::list< ISItem > getItems();



        /**
         *
         *     @short Create an html table with documentation for all variables.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        std::string getDocumentation() const;


        /**
         *
         *     @short Registers an updater for the Infospace.
         *            
         *            This function  is interesting for Infospaces  where it is 
         *            not  a   priori  clear  which  object   will  update  the 
         *            variables. Examples in the  Ferol40: The infospaces for the
         *            input links can be  updated either by the Frl application
         *            or  by the  Ferol40 application  depending on  the  mode of 
         *            operation. 
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void registerUpdater( utils::InfoSpaceUpdater *updater );

    protected:
        /**
         *
         *     @short Called  by  all "createxyz"  functions  to register  some 
         *            documentation for the new variable.
         *            
         *            Internally the  InfospaceHandler is maintaining  a string 
         *            containing  the   html  for  the   documentation  of  the 
         *            Infospace.  This string  is built  up when  the variables 
         *            are registered  with the infospace,  so that it  does not 
         *            need to be constructed every time it is needed.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void addParameterDocumentation( std::string name, std::string value, std::string format, UpdateType updateType, std::string doc );

    private:

        std::string documentationStringHeader() const;
        std::string documentationStringTrailer() const;
        std::string getTableHeader() const;

    protected:
      
        xdaq::Application *xdaq_P;
        xdata::InfoSpace *is_P;
        xdata::InfoSpace *cpis_P;
        std::string name_;
        toolbox::BSem itemlock_;
        bool noAutoPush_;

        Logger logger_;

        utils::InfoSpaceUpdater *updater_P;

        std::tr1::unordered_map< std::string, std::string> formatMap_;     //!< associates an IF member with its format string
        std::tr1::unordered_map< std::string, ISItem& > itemMap_;          //!< associates an IF member with its ISItem

        std::tr1::unordered_map< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >  uint32Map_; //!< associates a uint32 member with a pair containing the copy of the value and the xdata type of the infospace.
        std::tr1::unordered_map< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >  uint64Map_; //!< associates a uint64 member with a pair containing the copy of the value and the xdata type of the infospace.
        std::tr1::unordered_map< std::string, std::pair< bool, xdata::Boolean* > >  boolMap_;                 //!< associates a bool   member with a pair containing the copy of the value and the xdata type of the infospace.
        std::tr1::unordered_map< std::string, std::pair< double, xdata::Double* > >  doubleMap_;              //!< associates a double member with a pair containing the copy of the value and the xdata type of the infospace.
        std::tr1::unordered_map< std::string, std::pair< std::string, xdata::String* > >  stringMap_;         //!< associates a string member with a pair containing the copy of the value and the xdata type of the infospace.
	std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<StreamConf> >, xdata::Vector<xdata::Bag<StreamConf> >* > >  vectorMap_;         //!< associates a vector member with a pair containing the copy of the value and the xdata type of the infospace.


        std::string parameterDocumentation_; // An HTML table with documentation for the parameters.

    };
}

#endif /* __InfoSpaceHandler */
