#ifndef __WebStaticContentTab
#define __WebStaticContentTab

#include "d2s/utils/WebTabIF.hh"
#include <string>

namespace ferol40
{
    class utils::WebStaticContentTab : public utils::WebTabIF {
    public:
        utils::WebStaticContentTab( std::string name,
                             std::string contents 
                             );
        void print( std::ostream *out );
        void jsonUpdate( std::ostream *out );
        
    private:
        std::string name_;
        std::string contents_;
        
    };
}

#endif /* __WebStaticContentTab */
