#ifndef __InputStreamInfoSpaceHandler
#define __InputStreamInfoSpaceHandler

#include "ferol40/StreamInfoSpaceHandler.hh"
#include "d2s/utils/DataTracker.hh"

namespace ferol40
{
    class InputStreamInfoSpaceHandler:public ferol40::StreamInfoSpaceHandler {
    	public:
        	InputStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS );
    		void registerTrackerItems( utils::DataTracker &tracker );
    };
}

#endif /* __InputStreamInfoSpaceHandler */
