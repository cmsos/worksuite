

template.xml
  A xdaq xml configuration file template with some keywords which have to 
  be replaced by values needed for running the test (done in startTest.pl)


ferolControl.pl
  Used to start xdaq processes and control them.
  
  Usage : ./ferolControl.pl --config {configurationFile} {command} 
          command: {Start|Kill|xdaq soap command}
 
  Input to this script are configuration files of the type *.pltxt. There are
  two examples in the svn tree. The configuration files consist of a perl hash
  which has a generic component to set default values, and then specific sections
  for each Ferol in the configuration. There you can overwrite the default values.
  
  This scripts generates xml configuration files. For this it reads the template
  file template.xml and substitutes a lot of strings inside, with values from the
  configuration file. Then the configurationfiles need to be copied in a 
  location which is accessible by all hosts running the xdaq executives.
  This is done via scp. SSH keys need to be setup correctly.
  The executives are configured to write log files. The directory is the same
  as the one of the configuration files. Therefore the permissions of that 
  directory have to be set to allow the processes to write files there.
  For example a radical solution on afs:  fs setacl system:anyuser rlidwka.
  Since this will be spottet by the CERN Network Police and changed automatically
  by the CERN spyware, this is not an options. Therefore log files are written
  to /tmp of the local computers. 

  The command name conventions are: Starting with uppercase letter for all 
  commands which are direct soap commands of the applications. The script just
  forwards these commnands.
  Commands starting with a lowercase letter are processed by the script (e.g.
  if they have additional paramters)
  commands: 
    Start   : starts executives described in configuration
    Kill    : kills all xdaqs on machines of configuration
    {Cmd}   : ssh to hosts of configuration and execute SOAP command with 
              "sendSimpleCmdToApp" and parameters specified in configuration.
              State-transition commands for the ferol are: 
                  Configure
                  Enable
                  Stop
                  Halt
                  Pause
                  Resume
                  SetupEVG  {Takes values of Appl Infospace to re-generate events}
                  Fail (for testing)
		  laserOn   Should initialize the serdes and switch on the laser
                            of the 10G link.
                  laserOff  Should shutdown the serdes and switch of its laser of
                            the 10G link.

    read {item} : read from all applications an item of the Addresstable.
    write {item} {value} : write to all applications an item of the Addresstable.
    set {Parameter name, value} : sets an infospace parameter to a given value.

read/writeFerol.pl
  Two other debugging programs are available in order to read and write 
  arbitrary registers to the Ferol: readFerol.pl and writeFerol.pl
  example usage would be:
  
  readFerol.pl cmd64-cctrl-01 2002 0 CONTROL
  writeFerol.pl cmd64-cctrl-01 2002 0 enableStream0 1
  
  paramGet.pl
  paramSet.pl
  Get and Set Appl. Infospace parameter of a single application instance. 
  paramGet.pl used without specifiying the paramter reads all parameters and
  can be used to find paramters and its type 

  Running the command without arguments shows the usage. These commands are used
  by the ferolControl.pl

Monitoring
  The monitoring is done via a central webserver getting the Infospace Data from
  all applications via Ajax/Json.
  The webserver has been setup on cmd64-cctrl-01.
  The cgi-scripts and configuration data are ~FEROL/public_html/ferol
  Some filenames in the scripts have to be adpated if the server location changes. 
  The items.json file contains the infospace items in the table. Items are only
  distinguished by there name, not by their infospace.
  The ferols.json file contains the applications to be monitored.
  The files are not tolerating any syntax error (e.g. the ',' have to be all right).
  The ferols.json need to be adjusted every time the number of applcations changes.
  
  The link to the monitoring page is 
  
  http://cmd64-cctrl-01.cern.ch/~FEROL/ferol/monitor.html

  To get a list of all infospace items one connects to one of the applications with 
  a URL like
  http://cmd64-cctrl-01.cern.ch:2002/urn:xdaq-application:lid=109/infospaces


=========================================
Extra notes for specific setups

GTPE in lab of dominique:
Partition 0 : triggers the efeds.
  The trigger goes to input 0 of the trigger distributor in the 
  compactPCI crate with the efeds. The trigger distributor contains
  firmware which distributes the triggers to the other Lemos.
  Two of these are used to feed the trigger into the emulators via
  2 small Lemos.
Partition 1 : triggers to the ferol crate.
  The trigger goes to input 0 of the trigger distributor in the
  ferol crate. The trigger distributor contains a firmware where
  input 1 receives a reset signal from the gtpe (the PCI card in
  the GTPE PC.

Running in the lab with gtpe and efeds

ferol : pccmdlab40-10  or cmd64-cctrl-01
efed  : pccmdlab40-12
ru    : htcp03
gtpe  : pccmdlab40-11

The fmm needs to be setup to mask all inputs except for 0/1 where
the efeds are connected:

start the programm FMMConsole ('make tests' in daq/tts/fmm/ to compile it)
fal
fsm ffffc
then the output should be on ready (or reflecting the input of I 0/1)


for efed use program of Dominique (~/hwutils/GIII_fed/src/linux/x86)
. ldpath
./GIIIDebug
77
66 u event1.txt
7



for gtpe use stand alone control program of Hannes ~/proDir/daq/d2s/gtpe/bin/linux/x86_64_slc5
./GTPeConsole.exe
gat stop triggers
grc reset counters
gsc clear all partition groups
gsp 0 1 1
gsm 1 (clocked mode)
gss 0 (ignore slink of gtpe)
gsf 1
gaT
