#include "ferol40/DataSourceFactory.hh"
#include "ferol40/FrlDataSource.hh"
#include "ferol40/Ferol40DataSource.hh"
#include "ferol40/FedkitDataSource.hh"
#include <string>

ferol40::DataSourceIF *
ferol40::DataSourceFactory::createDataSource(HAL::HardwareDeviceInterface *device_P,
                                             utils::InfoSpaceHandler &appIS,
                                             Logger logger)
{
    std::string operationMode = appIS.getstring("OperationMode");
    if (operationMode == FEROL40_MODE)
    {
        return new ferol40::Ferol40DataSource(device_P, appIS, logger);
    }
    /*else if ( operationMode == FEDKIT_MODE )
        return new ferol40::FedkitDataSource( device_P,  appIS, logger );
    */
    else
        return NULL;
}
