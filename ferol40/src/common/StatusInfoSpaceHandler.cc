#include "ferol40/StatusInfoSpaceHandler.hh"
#include "ferol40/loggerMacros.h"

ferol40::StatusInfoSpaceHandler::StatusInfoSpaceHandler( xdaq::Application* xdaq, 
                                                       utils::InfoSpaceUpdater *updater )
    : utils::InfoSpaceHandler( xdaq, "Status_IS", updater )
{
    // these values are set by the application and do not come "from outside"
    createuint32( "instance", 0, "", NOUPDATE );
    createuint32( "slotNumber", 0, "", NOUPDATE );
    createstring( "stateName", "uninitialized" );
    createstring( "subState", "" );
    createstring( "failedReason", "");
    createstring( "lockStatus", "unknown" );


//JRF TODO, change certain montorables to NOUPDATE, but you have to remember to set them manually in the Ferol40.cc or Ferol40Controller.cc as for instance and slotNumber etc.
    createbool(   "QSPF_present", false, "", NOUPDATE, "QSFP present." );
    createbool(   "QSFP_int", false, "", HW32, "QSFP" ); //JRF TODO, if we want to monitor this we will have to add StatusIS to the monitor loop
    createbool(   "QSFP_low_power_r_bit", false, "", NOUPDATE, "QSFP" );
    createbool(   "DDR3_0_Ready_bit", false, "", NOUPDATE, "DDR3 ready" );
    createbool(   "DDR3_0_Calibration_ok_bit", false, "", NOUPDATE, "DDR3 Calibration OK" );
    createuint32( "FirmwareVersion", 0, "hex", NOUPDATE, "The utils::version number of the firmware. Each compilation of the firmware increments this number. The software checks for a minimal value required to run the software." );
    createuint32( "FirmwareType", 0, "hex", NOUPDATE, "The Firmware Type determines the functionality of the card. E.g. there is a dedicated firmware for the Fedkit." );
    createuint32( "HardwareRevision", 0, "hex", NOUPDATE, "The HwRevision refers to the utils::version of the Hardware (PCB, FPGAs, ...)" );
    
    

}
