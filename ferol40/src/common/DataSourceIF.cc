#include "ferol40/DataSourceIF.hh"
#include "ferol40/loggerMacros.h"
#include "ferol40/ferol40Constants.h"
ferol40::DataSourceIF::DataSourceIF(HAL::HardwareDeviceInterface *device_P_in,
                                    utils::InfoSpaceHandler &appIS,
                                    Logger logger)
    : appIS_(appIS)
{
  operationMode_ = appIS.getstring("OperationMode");
  //JRF TODO I'm not sure why Christoph duplicates this info again... can't we use the vectors in the Ferol40 object just pass a pointer?
  for (int i = 0; i < NB_STREAMS; i++)
  {
    dataSource_.push_back(dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[i].getField("DataSource"))->value_); //appIS.getstring( "DataSource" );
    streams_.push_back(dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[i].getField("enable"))->value_);       //appIS.getbool( "enableStream0" );
  }
  device_P = device_P_in;
  logger_ = logger;
}

ferol40::DataSourceIF::~DataSourceIF(){}; //JRF this is required so we at least have a symbol when loading the library.
