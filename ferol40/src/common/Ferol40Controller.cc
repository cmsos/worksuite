#include <iomanip>
#include <stdio.h>
#include <sstream>
#include <unistd.h>
#include <iostream>
#include <bitset>
#include <list>

#include "ferol40/Ferol40Controller.hh"
#include "ferol40/Ferol40StateMachine.hh"
#include "ferol40/Ferol40WebServer.hh"
#include "ferol40/Ferol40Monitor.hh"
#include "ferol40/loggerMacros.h"
#include "ferol40/HardwareDebugger.hh"
#include "d2s/utils/WebStaticContentTab.hh"
#include "ferol40/version.h"

#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"

#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/Event.h"
#include "toolbox/Runtime.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xgi/framework/Method.h"
#include "xdata/InfoSpaceFactory.h"

#include "xcept/tools.h"

XDAQ_INSTANTIATOR_IMPL(ferol40::Ferol40Controller);

ferol40::Ferol40Controller::Ferol40Controller(xdaq::ApplicationStub *stub)  : xdaq::Application(stub),
                                                                                                               xgi::framework::UIManager(this),
                                                                                                               utils::InfoSpaceUpdater(),

                                                                                                               logger_(this->getApplicationLogger()),
                                                                                                               timer_(10),
                                                                                                               appIS_(this),
                                                                                                               statusIS_(this, this),
                                                                                                               inputIS_(this, &appIS_),
                                                                                                               streamConfigIS_(this, &appIS_),
                                                                                                               fsm_(this, &statusIS_, &appIS_),
                                                                                                               monitor_(this->getApplicationLogger(), appIS_, this, fsm_),
                                                                                                               hwLocker_(logger_, appIS_, statusIS_),
                                                                                                               //   frl_( this, appIS_, monitor_, hwLocker_ ),
                                                                                                               ferol40_(this, appIS_, statusIS_, inputIS_,  monitor_, hwLocker_),
                                                                                                               changingState_(false)
{
    timer_.start();
    htmlDocumentation_ = this->fillDocumentation();
    operationMode_ = "n.a.";
    dataSource_ = "n.a.";
    monitor_.addInfoSpace(&statusIS_);

    // JRF Watch out for API change in XDAQ here...
    //#ifndef x86_64_slc6 //JRF TODO, change this to ifdef centos...
    //    const std::string icon1("icon"), icon1Address("/ferol40/images/Ferol40Controller.png"),icon2("icon16x16"),icon2Address("/ferol40/images/Ferol40Controller_16x16.png");
    //    const xdaq::ApplicationDescriptor * tmpAppDesc = getApplicationDescriptor();
    //    tmpAppDesc->setAttribute(icon1,icon1Address);//"icon","/ferol40/images/Ferol40Controller.png");
    //    tmpAppDesc->setAttribute(icon2,icon2Address);//"icon16x16", "/ferol40/images/Ferol40Controller_16x16.png");
    //#else
    stub->getDescriptor()->setAttribute("icon", "/ferol40/images/Ferol40Controller.png");
    stub->getDescriptor()->setAttribute("icon16x16", "/ferol40/images/Ferol40Controller_16x16.png");
    //#endif

    instance_ = getApplicationDescriptor()->getInstance();
    statusIS_.setuint32("instance", instance_, true);
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

    // The soap interface exposed to the outside world:
    // The state transitions called by RCMS
    // The changeState callback in this class is just forwarding the call to the
    // Ferol40StateMachine object. It is not possible to delegate the callback directly
    // to a non-xdaq Application.
    xoap::bind(this, &Ferol40Controller::changeState, "Configure", XDAQ_NS_URI);
    xoap::bind(this, &Ferol40Controller::changeState, "Enable", XDAQ_NS_URI);
    xoap::bind(this, &Ferol40Controller::changeState, "Pause", XDAQ_NS_URI);
    xoap::bind(this, &Ferol40Controller::changeState, "Resume", XDAQ_NS_URI);
    xoap::bind(this, &Ferol40Controller::changeState, "Halt", XDAQ_NS_URI);
    xoap::bind(this, &Ferol40Controller::changeState, "Stop", XDAQ_NS_URI);

    // to change conifguration parameters on the fly
    xoap::bind(this, &Ferol40Controller::ParameterSet, "ParameterSet", XDAQ_NS_URI);

    // for testing
    xoap::bind(this, &Ferol40Controller::changeState, "Fail", XDAQ_NS_URI);

    // to switch on and off the laser
    xoap::bind(this, &Ferol40Controller::laserOn, "laserOn", XDAQ_NS_URI);
    xoap::bind(this, &Ferol40Controller::laserOff, "laserOff", XDAQ_NS_URI);

    // for debugging the fedkit: toggle the ldown flag on the sender side
    xoap::bind(this, &Ferol40Controller::l0DaqOn, "daqOn", XDAQ_NS_URI);
    xoap::bind(this, &Ferol40Controller::l0DaqOff, "daqOff", XDAQ_NS_URI);

    // for debugging we want to be able to read and write any item to the hardware
    xoap::bind(this, &Ferol40Controller::readItem, "ReadItem", XDAQ_NS_URI);
    xoap::bind(this, &Ferol40Controller::writeItem, "WriteItem", XDAQ_NS_URI);

    // to issue a software trigger to the FRL event generator if in FRL_SOAP_TRIGER_MODE
    xoap::bind(this, &Ferol40Controller::softTrigger, "softTrigger", XDAQ_NS_URI);

    // Read out the SFP status information via its I2C interface
    xoap::bind(this, &Ferol40Controller::sfpStatus, "sfpStatus", XDAQ_NS_URI);

    //JRF removing this becuase vitesse doesn't exist any more. Reset the Vitesse chip.
    //    xoap::bind( this, &Ferol40Controller::resetVitesse, "resetVitesse", XDAQ_NS_URI );

    // Reset some monitoring items.
    xoap::bind(this, &Ferol40Controller::resetMonitoring, "resetMonitoring", XDAQ_NS_URI);

    // Dump the Hardware Registers.
    xoap::bind(this, &Ferol40Controller::dumpHardwareRegisters, "dumpHardwareRegisters", XDAQ_NS_URI);

    // bind the web pages
    xgi::framework::deferredbind(this, this, &Ferol40Controller::monitoringWebPage, "Default");
    xgi::framework::deferredbind(this, this, &Ferol40Controller::debugFerol40, "debugFerol40");
    xgi::framework::deferredbind(this, this, &Ferol40Controller::debugFrl, "debugFrl");
    xgi::framework::deferredbind(this, this, &Ferol40Controller::debugBridge, "debugBridge");
    xgi::framework::deferredbind(this, this, &Ferol40Controller::debugSlinkExpress, "debugSlinkExpress");
    xgi::framework::deferredbind(this, this, &Ferol40Controller::expertDebugging, "expertDebugPage");
    xgi::framework::deferredbind(this, this, &Ferol40Controller::testPage, "test");
    xgi::bind(this, &Ferol40Controller::jsonUpdate, "update");
    xgi::framework::deferredbind(this, this, &Ferol40Controller::jsonInfoSpaces, "infospaces");
    xgi::framework::deferredbind(this, this, &Ferol40Controller::xdaqWebPage, "basic");

    timer_.stop();
    uint32_t time = timer_.read();
    INFO("Ferol40Controller constructor needed " << time << "us");
}

void ferol40::Ferol40Controller::expertDebugging(xgi::Input *in, xgi::Output *out)
{
    //webServer_P->printHeader(out);
    webServer_P->expertDebugging(in, out, &ferol40_, this);
    //webServer_P->printFooter(out);
}

void ferol40::Ferol40Controller::testPage(xgi::Input *in, xgi::Output *out)
{
    //webServer_P->printHeader(out);
    *out << "Hello world";
    *out << "<script> $('#debug').append( 'debug'); </script>";
    //webServer_P->printFooter(out);
}

void ferol40::Ferol40Controller::debugSlinkExpress(xgi::Input *in, xgi::Output *out)
{
    //JRF TODO we should adapt this to FEROL40.  we should here check on the mode of operation or if an SFP is plugged in and connected to a core.
    ferol40::HardwareDebugger hwdebug(logger_, &ferol40_);
    if ( appIS_.getvector( "InputPorts" )[0].getField("enable") ) 
        {
            std::list< utils::HardwareDebugItem > slinkExpressRegisters = hwdebug.getSlinkExpressRegisters( 0 );
            webServer_P->printDebugTable( "Slink Express Registers link 0", slinkExpressRegisters, in, out );
        }

    if ( appIS_.getvector( "InputPorts" )[1].getField("enable") )
        {
            std::list< utils::HardwareDebugItem > slinkExpressRegisters = hwdebug.getSlinkExpressRegisters( 1 );
            webServer_P->printDebugTable( "Slink Express Registers link 1", slinkExpressRegisters, in, out );
        }

    if ( appIS_.getvector( "InputPorts" )[2].getField("enable") ) 
        {
            std::list< utils::HardwareDebugItem > slinkExpressRegisters = hwdebug.getSlinkExpressRegisters( 2 );
            webServer_P->printDebugTable( "Slink Express Registers link 2", slinkExpressRegisters, in, out );
        }

    if ( appIS_.getvector( "InputPorts" )[3].getField("enable") ) 
        {
            std::list< utils::HardwareDebugItem > slinkExpressRegisters = hwdebug.getSlinkExpressRegisters( 3 );
            webServer_P->printDebugTable( "Slink Express Registers link 3", slinkExpressRegisters, in, out );
        }
}

void ferol40::Ferol40Controller::debugFerol40(xgi::Input *in, xgi::Output *out)
{
    ferol40::HardwareDebugger hwdebug(logger_, &ferol40_);
    std::list<utils::HardwareDebugItem> ferol40Registers = hwdebug.getFerol40Registers();
    webServer_P->printDebugTable("Ferol40 Registers", ferol40Registers, in, out);
}

void ferol40::Ferol40Controller::debugFrl(xgi::Input *in, xgi::Output *out)
{
    ferol40::HardwareDebugger hwdebug(logger_, &ferol40_);
    std::list<utils::HardwareDebugItem> frlRegisters = hwdebug.getFrlRegisters();
    webServer_P->printDebugTable("Frl Registers", frlRegisters, in, out);
}

void ferol40::Ferol40Controller::debugBridge(xgi::Input *in, xgi::Output *out)
{
    ferol40::HardwareDebugger hwdebug(logger_, &ferol40_);
    std::list<utils::HardwareDebugItem> bridgeRegisters = hwdebug.getBridgeRegisters();
    webServer_P->printDebugTable("Bridge Registers", bridgeRegisters, in, out);
}

void ferol40::Ferol40Controller::monitoringWebPage(xgi::Input *in, xgi::Output *out)
{
    webServer_P->monitoringWebPage(in, out);
}

void ferol40::Ferol40Controller::dumpHardwareRegisters()
{
    HardwareDebugger hwdebug(logger_, &ferol40_);
    hwdebug.dumpHardwareRegisters("_manual");
}

void ferol40::Ferol40Controller::jsonUpdate(xgi::Input *in, xgi::Output *out)
{
    uint32_t tmp = appIS_.getuint32("testCounter") + 1;
    appIS_.setuint32("testCounter", tmp);
    webServer_P->jsonUpdate(in, out);
}

void ferol40::Ferol40Controller::jsonInfoSpaces(xgi::Input *in, xgi::Output *out)
{
    webServer_P->jsonInfoSpaces(in, out);
}

void ferol40::Ferol40Controller::xdaqWebPage(xgi::Input *in, xgi::Output *out)
{
    this->Default(in, out);
}

// This function just forwards the state change callback to the Statemachine.
// Unfortunately in xdaq it is not possible to let the callback work on another
// object than "this". One would have to re-implement the bind function so that
// it takes another object as argument on which the callback function will be
// called.
xoap::MessageReference
ferol40::Ferol40Controller::changeState(xoap::MessageReference msg) 
{
    changingState_ = true; //JRF make we use this bool to make sure we don't start configure before we have mirrored and read and pushed infospace. Otherwise the streams_ vector will get the wrong values.
    //std::cout << "In Ferol40Controller::changeState()" << std::endl;
    xoap::MessageReference reply = fsm_.changeState(msg);
    //std::cout << "fsm_.changeState() called, params should be updated..." << std::endl;
    //DEBUG("changeState() called");
    // update local copies of parameters

    appIS_.readInfoSpace();
    //std::cout << "readInfoSpace() called" << std::endl;
    // Mirror the flat parameters:

    //std::cout << "SOAP MESSAGE: "  << std::endl;
    //msg->writeTo (std::cout);
    //std::cout << "SOAP MESSAGE: "  << std::endl;
    appIS_.mirrorFromFlatParams();
    //std::cout << "mirrorFromFlatParams() called" << std::endl;
    // this pushes the changes to the monitorable copy of the application infospace:
    appIS_.pushInfospace();
    //std::cout << "pushInfospace() called" << std::endl;
    changingState_ = false;
    return reply;

    //    return fsm_.changeState( msg );
}

xoap::MessageReference
ferol40::Ferol40Controller::ParameterSet(xoap::MessageReference msg) 
{
    //JRF First we take the Application Infospace and push the values into the flat params
    appIS_.mirrorToFlatParams();
    
    //JRF now we update the flat params from the ParameterSet Soap message.
    xoap::MessageReference reply = xdaq::Application::ParameterSet(msg);
    //DEBUG("ParameterSet called");

    // update local copies of parameters
    appIS_.readInfoSpace();
    //DEBUG("appIS_.readInfoSpace called"); 
    //mirror values from the flat params back to application infospace 
    appIS_.mirrorFromFlatParams();
    //DEBUG("appIS_.mirrorFromFlatParams called");

    appIS_.pushInfospace();
    //DEBUG("appIS_.pushInfospace called");
    return reply;
}

ferol40::Ferol40Controller::~Ferol40Controller()
{
}

void ferol40::Ferol40Controller::actionPerformed(xdata::Event &e)
{

    if (e.type() == "urn:xdaq-event:setDefaultValues")
    {
        DEBUG("setting default values: here we initiize StreamInfoSpaces");
        operationMode_ = appIS_.getstring("OperationMode");
        dataSource_ = dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[0].getField("DataSource"))->value_; //appIS_.getstring( "DataSource" );
                                                                                                                       //JRF TODO, this entire bit of code can now be removed since we no longer need 3 possible input sources...
                                                                                                                       //if ( operationMode_ == FRL_MODE )
                                                                                                                       //  {
                                                                                                                       // DEBUG( "setting input source to FRL" );
        //inputIS_.setInputSource( Ferol40StreamInfoSpaceHandler::FRL );
        //inputIS_.registerUpdater( &frl_ );
        //  }
        //else
        //    {
        if (dataSource_ == GENERATOR_SOURCE) //||
                                             //(dataSource_ == L6G_SOURCE) ||
                                             //(dataSource_ == L6G_CORE_GENERATOR_SOURCE) ||
                                             //(dataSource_ == L6G_LOOPBACK_GENERATOR_SOURCE) )
        {
            //DEBUG( "setting input source to FEROL406G" );
            //JRF TODO, This is no longe rneeded since in there is only one input stream source now. no need for the fancy switching on input stream type.
            //inputIS_.setInputSource( Ferol40StreamInfoSpaceHandler::FEROL406G );
        }
        else if ((dataSource_ == L10G_SOURCE) ||
                 (dataSource_ == L10G_CORE_GENERATOR_SOURCE))
        {
            //DEBUG( "setting input source to FEROL4010G" );
            //inputIS_.setInputSource( Ferol40StreamInfoSpaceHandler::FEROL4010G );
        }
        else
        {
            ERROR("FATAL Software/configuration bug: no reasonable data Source set: " << dataSource_);
            exit(-1);
        }
        inputIS_.registerUpdater(&ferol40_);
        streamConfigIS_.registerUpdater(&ferol40_);
        //  }
    }
    // setup the utils::Monitoring.
    // First we build infospaces. Then we setup the itemSets for the
    // use in the Hyperdaq web page (i.e. the "flashlists" of the hyperdaq
    // page)

    monitor_.addInfoSpace(&inputIS_);
    monitor_.addInfoSpace(&streamConfigIS_);

    webServer_P = new ferol40::Ferol40WebServer(monitor_,
                                                this->getApplicationDescriptor()->getURN(),
                                                this->getApplicationDescriptor()->getContextDescriptor()->getURL(),
                                                logger_);

    const std::tr1::unordered_map<std::string, utils::InfoSpaceHandler *> ismap = monitor_.getInfoSpaceMap();
    std::tr1::unordered_map<std::string, utils::InfoSpaceHandler *>::const_iterator it;
    for (it = ismap.begin(); it != ismap.end(); it++)
    {
        std::string name = (*it).first;
        utils::InfoSpaceHandler *info = (*it).second;

        utils::WebStaticContentTab *docTab = new utils::WebStaticContentTab(name + " Doc.",
                                                              info->getDocumentation());
        webServer_P->registerTab(docTab);
    }

    //JRF reinstate this page once it's up to date
    utils::WebStaticContentTab *docTab = new utils::WebStaticContentTab("Documentation", htmlDocumentation_);
    webServer_P->registerTab(docTab);

    // the Infospaces are registered with the utils::Monitor, and the Itemsets are already
    // built. Therefore we can start the monitoring.
    //JRF TODO uncomment once infospace items are all correct.
    monitor_.startMonitoring();
    //DEBUG("called monitor_.startMonitoring() ");
}

///////////////////////////////// Callbacks for state transitions ///////////////////////////////////////////////

// In case the error was caused by a StateMachine error (e.g. a wrong state transistion)
// we get a toolbox::fsm::FailedEvent. In case we have a spontaneous error due to for
// example synch lost draining in the middle of a run, we get a simple toolbox Event.
// The latter does not contain information on the originating exception. Therefore there
// is no usefull stuff we could do here (not event printing stuff out).
void ferol40::Ferol40Controller::FailAction(toolbox::Event::Reference e)
{
    ERROR("Entered FailAction");
}

void ferol40::Ferol40Controller::ConfigureAction(toolbox::Event::Reference e) 
{
    timer_.start();
    //JRF I have moved these into the stateChange() handler. So they are done before we get here.
    //appIS_.readInfoSpace();
    //appIS_.pushInfospace();
    //JRF before we continue we wait for the changeState() method to complete to ensure the flat infospace params are copied into the correct places
    while (changingState_)
        usleep(100);
    statusIS_.setuint32("slotNumber", appIS_.getuint32("slotNumber"), true);

    //JRF TODO here we require at least one stream to be active... this should be adapted to a loop over the streams.
    //JRF TODO, this shiould be a loop for generalisation
    if  (       (!dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[0].getField("enable"))->value_) 
            &&  (!dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[1].getField("enable"))->value_) 
            &&  (!dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[2].getField("enable"))->value_) 
            &&  (!dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[3].getField("enable"))->value_)
        )
    {
        statusIS_.setstring("subState", "inactive", true);
        return;
    }
    else
    {
        statusIS_.setstring("subState", "active", true);
    }
    // try and lock the hardware

    hwLocker_.lock();

    if (!hwLocker_.lockedByUs())
    {
        std::string lockstate = hwLocker_.updateLockStatus();
        std::stringstream msg;
        msg << "Cannot get Hardware Lock for Ferol40Controller in slot "
            << appIS_.getuint32("slotNumber")
            << ". The lock status is \"" << lockstate << "\"";
        FATAL(msg.str());
        XCEPT_DECLARE(utils::exception::HardwareNotAvailable, e, msg.str());
        notifyQualified("error", e);
        fsm_.gotoFailed(e);
    }

    try
    {
        // if the firmware in the Ferol40 needs to be changed, the ferol40 Configuration Space also needs to tbe
        // saved and re-loaded. For this the PCIDevice of the Ferol40 must exist. Therfore the instantiateHardware
        // is called on the ferol40 before the configuration of the Ferol40.
        // The entire configuration of the Ferol40 must not be done before the FRL configuration since the TCP
        // connection on the DAQ link breaks during the re-writing of the configuration space (WHY?). If then
        // the configuration is repeated everything is fine but currently the ptFerol40 does not survive this
        // re-connection procedure. (Remember that the Ferol40 just sends a "reset" to abort this connection. This
        // might get lost if there is heavy load on the network. Then the receiver would need some additional
        // intelligence to handle an incoming TCP-Sync request on the same port.)

	ferol40_.instantiateHardware();

        ferol40_.configure();

    }
    catch (utils::exception::Exception &e)
    {
        INFO("going to failed since caught exception during ferol40 configure");
        fsm_.gotoFailed(e);
    }
    timer_.stop();
    uint32_t time = timer_.read();
    INFO("ConfigureAction needed " << time << " mu seconds.");
}

void ferol40::Ferol40Controller::EnableAction(toolbox::Event::Reference e) 
{
    timer_.start();
    try
    {
        ferol40_.enable();
        //frl_.enable();
    }
    catch (utils::exception::Exception &e)
    {
        fsm_.gotoFailed(e);
    }

    timer_.stop();
    uint32_t time = timer_.read();
    INFO("EnableAction needed " << time << " mu seconds.");
}

void ferol40::Ferol40Controller::SuspendAction(toolbox::Event::Reference e) 
{
    try
    {
        // The intelligence is in the Frl and Ferol40 objects: they know
        // if they have to do something.
        ferol40_.suspendEvents();
        // frl_.suspendEvents();
    }
    catch (utils::exception::Exception &e)
    {
        fsm_.gotoFailed(e);
    }
}

void ferol40::Ferol40Controller::ResumeAction(toolbox::Event::Reference e) 
{
    try
    {
        // The intelligence is in the Frl and Ferol40 objects: they know
        // if they have to do something.
        ferol40_.resumeEvents();
        //frl_.resumeEvents();
    }
    catch (utils::exception::Exception &e)
    {
        fsm_.gotoFailed(e);
    }
}

void ferol40::Ferol40Controller::StopAction(toolbox::Event::Reference e) 
{
    timer_.start();
    if (appIS_.getbool("lightStop"))
    {
        try
        {
            // frl_.stop();
            ferol40_.stop();
        }
        catch (utils::exception::Exception &e)
        {
            fsm_.gotoFailed(e);
        }
    }
    else
    {
        this->HaltAction(e);
        this->ConfigureAction(e);
    }
    timer_.stop();
    uint32_t time = timer_.read();
    INFO("StopAction needed " << time << " mu seconds.");
}

void ferol40::Ferol40Controller::HaltAction(toolbox::Event::Reference e) 
{
    timer_.start();
    try
    {
        ferol40_.halt();
    }
    catch (utils::exception::Exception &e)
    {
        fsm_.gotoFailed(e);
    }

    hwLocker_.unlock();
    timer_.stop();
    uint32_t time = timer_.read();
    INFO("HaltAction needed " << time << " mu seconds.");
}

xoap::MessageReference ferol40::Ferol40Controller::laserOn(xoap::MessageReference msg) 
{
    try
    {
        ferol40_.controlSerdes(true);
    }
    catch (utils::exception::Ferol40Exception &e)
    {
        ERROR("Problem switching on the laser: " << e.what());
        return utils::SOAPFSMHelper::makeSoapFaultReply("laserOnResponse", e.what());
    }

    DEBUG("Laser has been switched on, serdes is up.");
    return utils::SOAPFSMHelper::makeSoapReply("laserOnResponse", "ok");
}

xoap::MessageReference ferol40::Ferol40Controller::laserOff(xoap::MessageReference msg) 
{
    try
    {
        ferol40_.controlSerdes(false);
    }
    catch (utils::exception::Ferol40Exception &e)
    {
        ERROR("Problem switching off the laser: " << e.what());
        return utils::SOAPFSMHelper::makeSoapFaultReply("laserOffResponse", e.what());
    }

    DEBUG("Laser has been switched OFF, serdes is down.");
    return utils::SOAPFSMHelper::makeSoapReply("laserOffResponse", "ok");
}

xoap::MessageReference ferol40::Ferol40Controller::l0DaqOn(xoap::MessageReference msg) 
{
    int ret = ferol40_.daqOn(0);
    if (ret == 0)
    {
        DEBUG("LDOWN on sender site should be ON.");
        return utils::SOAPFSMHelper::makeSoapReply("l0DaqOnResponse", "ok");
    }
    else
    {
        return utils::SOAPFSMHelper::makeSoapReply("l0DaqOnResponse", "error");
    }
}

xoap::MessageReference ferol40::Ferol40Controller::l0DaqOff(xoap::MessageReference msg) 
{

    int ret = ferol40_.daqOff(0);
    if (ret == 0)
    {
        DEBUG("LDOWN on sender site should be off.");
        return utils::SOAPFSMHelper::makeSoapReply("l0DaqOffResponse", "ok");
    }
    else
    {
        return utils::SOAPFSMHelper::makeSoapReply("l0DaqOnResponse", "error");
    }
}

xoap::MessageReference ferol40::Ferol40Controller::softTrigger(xoap::MessageReference msg) 
{

    //frl_.softTrigger();
    // Always answer with ok. This is a bit lazy...
    return utils::SOAPFSMHelper::makeSoapReply("softTriggerResponse", "ok");
}

xoap::MessageReference ferol40::Ferol40Controller::sfpStatus(xoap::MessageReference msg) 
{
    try
    {
        DEBUG("trying 1");
        ferol40_.readSFP(1);
        // Always answer with ok. This is a bit lazy...
        DEBUG("trying 1 ok");
    }
    catch (utils::exception::Ferol40Exception &e)
    {
        ERROR("Problem when reading the SFP status: " << e.what());
    }
    try
    {
        DEBUG("trying 0");
        ferol40_.readSFP(0);
        DEBUG("trying 0 ok");
        // Always answer with ok. This is a bit lazy...
    }
    catch (utils::exception::Ferol40Exception &e)
    {
        ERROR("Problem when reading the SFP status: " << e.what());
    }
    return utils::SOAPFSMHelper::makeSoapReply("sfpStatusResponse", "ok");
}

//////////////////////////////////////// Debugging only /////////////////////////
xoap::MessageReference ferol40::Ferol40Controller::readItem(xoap::MessageReference msg) 
{

    ferol40::HardwareDebugger hwdebug(logger_, &ferol40_);

    return hwdebug.readItem(msg);
}

xoap::MessageReference ferol40::Ferol40Controller::writeItem(xoap::MessageReference msg) 
{

    ferol40::HardwareDebugger hwdebug(logger_, &ferol40_);

    return hwdebug.writeItem(msg);
}

xoap::MessageReference ferol40::Ferol40Controller::resetMonitoring(xoap::MessageReference msg) 
{
    try
    {	
	//TODO need to implement this method. Check what it used to do in frl_.resetMonitoring()
        //ferol40_.resetMonitoring();
    }
    catch (utils::exception::Ferol40Exception &e)
    {
        return utils::SOAPFSMHelper::makeSoapReply("resetMonitoring", e.what());
    }
    return utils::SOAPFSMHelper::makeSoapReply("resetMonitoringResponse", "ok");
}

xoap::MessageReference ferol40::Ferol40Controller::dumpHardwareRegisters(xoap::MessageReference msg) 
{
    try
    {
        this->dumpHardwareRegisters();
    }
    catch (utils::exception::Ferol40Exception &e)
    {
        return utils::SOAPFSMHelper::makeSoapReply("dumpHardwareRegisters", e.what());
    }
    return utils::SOAPFSMHelper::makeSoapReply("dumpHardwareRegistersResponse", "ok");
}

std::string
ferol40::Ferol40Controller::fillDocumentation()
{
    std::stringstream tmpss;
    tmpss << WORKSUITE_FEROL40_VERSION_MAJOR << "." << WORKSUITE_FEROL40_VERSION_MINOR << "."<< WORKSUITE_FEROL40_VERSION_PATCH;
    return std::string(
        "<center> <H1>Ferol40 Documentation for Insiders. Ferol40Controller utils::version: ") +  tmpss.str() + std::string( "</H1></center>\n"
        "<H2>Documentation of activities in the software during the state transitions</H2>\n"
        "\n"
        "<H3>Configure</H3>\n"
        "  <ol>\n"
        "    <li>\n"
        "      Read and push the Application Infospace. This guarantees that the values set in the \n"
        "      Application Infospace are copied to the local variables in the Infospacehandlers. This\n"
        "      is valid for values set in the xdaq configuration file and for values set via the \n"
        "      function manager in the soap \"Configure\" function call.\n"
        "    </li>\n"
        "    <li>\n"
        "      Copy the slot number from the Application Infospace (a configuration parameter) into\n"
        "      the StatusInfospace.\n"
        "    </li>\n"
        "    <li>\n"
        "      Set the subState to \"active\" if at least one input stream is enabled.\n"
        "    </li>\n"
        "    <li>\n"
        "      Try and lock the hardware via the HardwareLocker class instance.\n"
        "    </li>\n"
        "    <li>\n"
        "      Instantiate the Ferol40 Hardware.\n"
        "      <br/>\n"
        "      If this needs to be done the, the Ferol40 hardware device needs to exist already since\n"
        "      the PCI configuration of the Ferol40 needs to be saved and written back to the PCI \n"
        "      config space during this operation. \n"
        "    </li>\n"
        "    <ol>\n"
        "      <li>\n"
        "        Call ferol40.createFerol40Device\n"
        "      </li>\n"
        "       <ol>\n"
        "         <li>\n"
        "           Check that the hardware is locked by this application.\n"
        "         </li>\n"
        "         <li>\n"
        "           Scan the crate for the Ferol40 hardware corresponding to this application. The hardware \n"
        "           which corresponds to the slot number given in the configuration needs to be found. This\n"
        "           is possible since the slot number is encoded with some custom pins in the CompactPCI \n"
        "           backplane and this number is mapped into the PCI configuration space. (In case of the \n"
        "           FEDKit, where no CompactPCI crate is present, the slot number is interpreted as PCI \n"
        "           unit number.).\n"
        "         </li>\n"
        "         <li>\n"
        "           Once the device is found the firmware utils::version is checked against the values hardcoded \n"
        "           in the file include/ferol40/ferol40Constants.h except for the configuration parameter \n"
        "           \"noFirmwareVersionCheck\" is set to true. This check indeed checks the hardware revision, \n"
        "           the firmware type and the firmware utils::version agains the operational mode of the application.\n"
        "         </li>\n"
        "         <li>\n"
        "           The SlinkExpressCore object is instantiated. (No action on the hardware.)\n"
        "         </li>\n"
        "         <li>\n"
        "           At this point the global flag \"ferol40.hardwareCreated_\" is set. This flag is used by various \n"
        "           threads and routines to determine if the the pointer to HAL device is valid. \n"
        "         </li>\n"
        "         <li>\n"
        "           Serial number and hardware MAC addresse are read.\n"
        "         </li>\n"
        "       </ol>\n"
        "       <li>\n"
        "         Reset the TCP/IP connections of both TCP/IP streams. (In case there is something left over\n"
        "         from a previous run which did not finish properly.)\n"
        "       </li>\n"
        "    </ol>\n"
        "     <ol>\n"
        "       <ol>\n"
        "         <li>\n"
        "           Verify that the hardware is locked by this application.\n"
        "         </li>\n"
        "         <li>\n"
        "           Scan the PCI bus for the hardware in the slot corresponding to the configutation parameter.\n"
        "           The procedure is the same as in the case for the Ferol40. The geographical slot number encoded\n"
        "           in the custom backplane is mapped into the PCI Configuration Space of the Bridge FPGA.\n"
        "         </li>\n"
        "         <li>\n"
        "           Once the hardware has been set issue a softReset of the bridge FPGA. Then wait 1000us (the soft\n"
        "           reset takes 82us according to Dominique.)\n"
        "         </li>\n"
        "         <li>\n"
        "           If the firmware had been changed in the previous step, issue another softReset to the bridge.\n"
        "         </li>\n"
        "         <li>\n"
        "           For every enabled input stream create a SlinkStream object.\n"
        "         </li>\n"
        "       </ol>\n"
        "       <li>\n"
        "         Reset the bit DAQ_mode of the SLINK. This brings the SLINK into \"command mode\".\n"
        "       </li>\n"
        "       <li>\n"
        "         Issue a softwareReset.\n"
        "       </li>\n"
        "       <li>\n"
        "         Reset the bits \"enableFifoMonCounter\" and \"enableHistograms\".\n"
        "       </li>\n"
        "       <li>\n"
        "         Set the maximal fragment size. (Beyond this size fragments are truncated in the Ferol40.)\n"
        "       </li>\n"
        "       <li>\n"
        "         Set the expected source IDs for both streams.\n"
        "       </li>\n"
        "       <li>\n"
        "         The following is done for both Slink inputs. The SlinkStream objects execute the commands \n"
        "         (but only in case stream is enabled): \n"
        "         Set the DC balance option of the SLINK if the configuration parameter is set, and deskew the links \n"
        "         if the configuration demands it. (NB: The deskew procedure requires the DCBalance to be switched on!) \n"
        "         Then test the SLINK with the test procedure. Then enable the Slink. In normal operation in CMS we \n"
        "         DO NOT USE Deskew OR DCBalance on these links!\n"
        "       </li>\n"
        "     </ol>\n"
        "    <li>\n"
        "      Configure the ferol40 object.\n"
        "    </li>\n"
        "     <ol>\n"
        "       <li>\n"
        "         On any enabled input stream call daqOff. This is done in case the previous run ended \n"
        "         ungracefully.\n"
        "       </li>\n"
        "       <li>\n"
        "         Do a SoftwareReset.\n"
        "       </li>\n"
        "       <li>\n"
        "         Do the Reset_Counters.\n"
        "       </li>\n"
        "       <li>\n"
        "         Setup the input source. Call createDataSource on the DataSourceFactory. \n"
        "       </li>\n"
        "       <li>\n"
        "         Set the dataSource in the Ferol40 register. This implies setting a code in a register of the Ferol40.\n"
        "         Only in case the data source is one of the optical inputs if the FEROL40 the following operations\n"
        "         are executed before this:\n"
        "         <ol>\n"
        "           <li>Check that an SFP is plugged into the corresponding SFP case of the link.</li>\n"
        "           <li>Check that the SERDES of the link is up and running.</li>\n"
        "           <li>Issue a Re-sync on the link: this resets the sequence numbers of the packets of this link\n"
        "             and clears all internal buffer memories.</li>\n"
        "         </ol>\n"
        "       </li>\n"
        "       <li>\n"
        "         Call suspendEvents in case the previous run with event generators did not end gracefully.\n"
        "       </li>\n"
        "       <li>\n"
        "         Set the L5gb_FEDIS_SlX0/1 registers (the event ids of the streams participating in the run.\n"
        "       </li>\n"
        "       <li>\n"
        "         Now set up the 10Gb TCP/IP link to the DAQ.\n"
        "       </li>\n"
        "       <ol>\n"
        "         <li>\n"
        "           Set the bit TCP_SOCKET_BUFFER_DDR if this is requested by the configuration.\n"
        "         </li>\n"
        "         <li>\n"
        "           Check that the XAUI link is up: Read SERDES_STATUS and check the value.\n"
        "         </li>\n"
        "         <li>\n"
        "           Check the status of the Vitesse chip via the MDIO bus: \n"
        "         </li>\n"
        "         <li>\n"
        "           Write the NETMASK. If the netmask is different from 0 the IP_GATEWAY has to be set by the\n"
        "           user, too, since the ferol40 assumes the user wants to route packets. (This is not used in\n"
        "           the CMS network.)\n"
        "         </li>\n"
        "         <li>\n"
        "           Check the configuration parameter \"SourceIP\". If it is set to \"auto\" it is assumed \n"
        "           that the source ip has been set in the hardware by a startup script on the controller\n"
        "           PC. This IP is checked to be different from '0'. If there is a \"normal\" IP string in\n"
        "           this configuration parameter it is used as the source address and written into the hardware.\n"
        "           IP numbers and hostnames are accepted.\n"
        "         </li>\n"
        "         <li>\n"
        "           Check that the IP addresse and the MAC address are unique on the network. This is done by\n"
        "           sending ARP-probe packets. \n"
        "         </li>\n"
        "         <li>\n"
        "           Set a long list of TCP/IP specific parameters. \n"
        "         </li>\n"
        "         <li>\n"
        "           Do the arp-request. The number of trials and the time between the trials are configuration\n"
        "           parameters.\n"
        "         </li>\n"
        "         <li>\n"
        "           Open the TCP/IP connection to the RU.\n"
        "         </li>\n"
        "         <li>\n"
        "           Set the ENA_PAUSE_FRAME register according to the configuration.\n"
        "         </li>\n"
        "       </ol>\n"
        "    </ol>\n"
        "  </ol>\n"
        "<H3>Enable</H3>\n"
        "  <ol>\n"
        "    <li>\n"
        "      Enable the ferol40 object: This resets the counters of the TCP monitoring and, with a different\n"
        "      reset command, the monitoring counters for the SLINK express links. If the datasource\n"
        "      is the SLINKExpress the daqOn command is sent to the links sender cores which participate in the \n"
        "      run. <br/>\n"
        "      Having the monitoring counters reset only in enable does allow for some post mortem analysis after the \n"
        "      run has stopped. On the other hand it makes it more difficult to understand if already in Configure \n"
        "      something is flowing/counting. But you cannot have everything! :-)\n"
        "    </li>\n"
        "    <li>\n"
        "      Enable the ferol40 object. The resetFifoMonCounter is executed followed by enableFifoMonCounter.\n"
        "      The resetHistograms is executed followed by enableHistrograms.\n"
         "    </li>\n"
        "  </ol>\n");
    //return std::string(doc);
}

bool ferol40::Ferol40Controller::updateInfoSpace(utils::InfoSpaceHandler *is, uint32_t streamNo)
{
    return true; //JRF todo, decide if we have cases when we don't want to push to the flashlist.
}
