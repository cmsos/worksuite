#include "ferol40/Ferol40StreamInfoSpaceHandler.hh"
#include "d2s/utils/Exception.hh"
#include "ferol40/loggerMacros.h"

ferol40::Ferol40StreamInfoSpaceHandler::Ferol40StreamInfoSpaceHandler( xdaq::Application *xdaq,
                                                                 std::string name,
                                                                 utils::InfoSpaceHandler *appIS,
                                                                 bool noAutoPush )
    : Ferol40::StreamInfoSpaceHandler( xdaq, name, NULL, appIS, noAutoPush )
{
    //    createuint32( "expectedFedId", "", "", "", NOUPDATE, NOUPDATE, "", "The fedid expected by the configuration for this input stream. This value is used as a key to identify the flashlist entries.");
}

void
ferol40::Ferol40StreamInfoSpaceHandler::createstring( std::string name,
                                                  std::string frlHwName,
                                                  std::string ferol406GHwName,
                                                  std::string ferol4010GHwName,
                                                  UpdateType frlUpdateType,
                                                  UpdateType ferol40UpdateType,
                                                  std::string format,
                                                  std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   STRING, format, frlUpdateType,   doc);
    ISItem *itferol40 = new ISItem( name, ferol406GHwName, STRING, format, ferol40UpdateType, doc);
    ISItem *it10Gferol40 = new ISItem( name, ferol4010GHwName, STRING, format, ferol40UpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol406GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol40 ) );
    ferol4010GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol40 ) );

    // to do all the stuff in the base classes
    // ferol40::StreamInfoSpaceHandler::createstring( name, "not initialised", format, frlUpdateType, doc );
}


void
ferol40::Ferol40StreamInfoSpaceHandler::createuint32( std::string name,
                                                  std::string frlHwName,
                                                  std::string ferol406GHwName,
                                                  std::string ferol4010GHwName,
                                                  UpdateType frlUpdateType,
                                                  UpdateType ferol40UpdateType,
                                                  std::string format,
                                                  std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   UINT32, format, frlUpdateType,   doc);
    ISItem *itferol40 = new ISItem( name, ferol406GHwName, UINT32, format, ferol40UpdateType, doc);
    ISItem *it10Gferol40 = new ISItem( name, ferol4010GHwName, UINT32, format, ferol40UpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol406GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol40 ) );
    ferol4010GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol40 ) );

    // to do all the stuff in the base classes
    // ferol40::StreamInfoSpaceHandler::createuint32( name, 0, format, frlUpdateType, doc );
}


void
ferol40::Ferol40StreamInfoSpaceHandler::createuint64( std::string name,
                                                  std::string frlHwName,
                                                  std::string ferol406GHwName,
                                                  std::string ferol4010GHwName,
                                                  UpdateType frlUpdateType,
                                                  UpdateType ferol40UpdateType,
                                                  std::string format,
                                                  std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   UINT64, format, frlUpdateType,   doc);
    ISItem *itferol40 = new ISItem( name, ferol406GHwName, UINT64, format, ferol40UpdateType, doc);
    ISItem *it10Gferol40 = new ISItem( name, ferol4010GHwName, UINT64, format, ferol40UpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol406GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol40 ) );
    ferol4010GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol40 ) );

    // to do all the stuff in the base classes
    //StreamInfoSpaceHandler::createuint64( name, 0, format, frlUpdateType, doc );

}


void
ferol40::Ferol40StreamInfoSpaceHandler::createdouble( std::string name,
                                                  std::string frlHwName,
                                                  std::string ferol406GHwName,
                                                  std::string ferol4010GHwName,
                                                  UpdateType frlUpdateType,
                                                  UpdateType ferol40UpdateType,
                                                  std::string format,
                                                  std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   DOUBLE, format, frlUpdateType,   doc);
    ISItem *itferol40 = new ISItem( name, ferol406GHwName, DOUBLE, format, ferol40UpdateType, doc);
    ISItem *it10Gferol40 = new ISItem( name, ferol4010GHwName, DOUBLE, format, ferol40UpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol406GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol40 ) );
    ferol4010GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol40 ) );

    // to do all the stuff in the base classes
    //StreamInfoSpaceHandler::createdouble( name, 0., format, frlUpdateType, doc );

}


void
ferol40::Ferol40StreamInfoSpaceHandler::createbool( std::string name,
                                                std::string frlHwName,
                                                std::string ferol406GHwName,
                                                std::string ferol4010GHwName,
                                                UpdateType frlUpdateType,
                                                UpdateType ferol40UpdateType,
                                                std::string format,
                                                std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   BOOL, format, frlUpdateType,   doc);
    ISItem *itferol40 = new ISItem( name, ferol406GHwName, BOOL, format, ferol40UpdateType, doc);
    ISItem *it10Gferol40 = new ISItem( name, ferol4010GHwName, BOOL, format, ferol40UpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol406GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol40 ) );
    ferol4010GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol40 ) );

    // to do all the stuff in the base classes
    //StreamInfoSpaceHandler::createbool( name, false, format, frlUpdateType, doc );

}

void
ferol40::Ferol40StreamInfoSpaceHandler::setInputSource( HwInput inputSource )
{

    INFO("SETTING INPUT SOURCE in Ferol40StreamInfoSpaceHandler");

    inputSource_ = inputSource;
    std::tr1::unordered_map< std::string, ISItem&>::iterator it;
    std::tr1::unordered_map< std::string, ISItem&> cmap;
    if ( inputSource_ == FEROL406G )
        {
            it = ferol406GItemMap_.begin();
            cmap = ferol406GItemMap_;
        }
    else if ( inputSource_ == FEROL4010G )
        {
            it = ferol4010GItemMap_.begin();
            cmap = ferol4010GItemMap_;
        }
    else if ( inputSource_ == FRL )
        {
            it = frlItemMap_.begin();
            cmap = frlItemMap_;
        }
    else
        {
            std::cout << "Heavy software bug in Ferol40StreamInfoSpaceHandler::setInputSource...";
        }


    itemlock_.take();

    //itemMap_.clear();
    for ( ; it != cmap.end(); it++ ) 
        {
            ISItem &isitem = (*it).second;
            std::string name = isitem.name;
            //DEBUG( "loop " << name);
            if ( itemMap_.find( name ) != itemMap_.end() ) 
                {
                    std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
                    XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
                    xdaq_P->notifyQualified( "fatal", top );
                    ERROR( err );                    
                }

            itemMap_.insert( std::make_pair< std::string, ISItem&> ( name, (*it).second ) );
            //DEBUG("insterted in tiem map");
            is_P->lock();
            if ( cpis_P )
                cpis_P->lock();
            if ( isitem.type == STRING )
                {
                    xdata::String *ptr = new xdata::String( "not initialized" );
                    stringMap_.insert( std::make_pair< std::string, std::pair< std::string, xdata::String* > >
                                       ( name, std::make_pair< std::string, xdata::String* > ( "not initialized", ptr )));
                    std::vector<std::string> vals;
                    vals.push_back( "uninitialised" );
                    vals.push_back( "uninitialised" );
                    stringStreamValues_[name] = vals;

                    is_P->fireItemAvailable( name, ptr );
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );
                }
            else if( isitem.type == UINT32 )
                {
                    //DEBUG("uin32");
                    xdata::UnsignedInteger32 *ptr = new xdata::UnsignedInteger32( 0 );
                    uint32Map_.insert( std::make_pair< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >
                                       ( name, std::make_pair< uint32_t, xdata::UnsignedInteger32* > ( 0, ptr )));
                    std::vector<uint32_t> vals;
                    vals.push_back( 0 );
                    vals.push_back( 0 );
                    uint32StreamValues_[name] = vals;
                    //DEBUG("inserted in map");

                    is_P->fireItemAvailable( name, ptr );
                    //DEBUG( "filred tiem ava");
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );

                    //DEBUG( "IS fired ");
                }
            else if ( isitem.type == UINT64 )
                {
                    xdata::UnsignedInteger64 *ptr = new xdata::UnsignedInteger64( 0 );
                    uint64Map_.insert( std::make_pair< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >
                                       ( name, std::make_pair< uint64_t, xdata::UnsignedInteger64* > ( 0, ptr )));
                    std::vector<uint64_t> vals;
                    vals.push_back( 0 );
                    vals.push_back( 0 );
                    uint64StreamValues_[name] = vals;
                    is_P->fireItemAvailable( name, ptr );
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );
               }
            else if ( isitem.type == DOUBLE )
                {
                    xdata::Double *ptr = new xdata::Double( 0.0 );
                    doubleMap_.insert( std::make_pair< std::string, std::pair< double, xdata::Double* > >
                                       ( name, std::make_pair< double, xdata::Double* > ( 0.0, ptr )));
                    std::vector<double> vals;
                    vals.push_back( 0. );
                    vals.push_back( 0. );
                    doubleStreamValues_[name] = vals;
                    is_P->fireItemAvailable( name, ptr );
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );
               }
            else if ( isitem.type == BOOL )
                {
                    xdata::Boolean *ptr = new xdata::Boolean( false );
                    boolMap_.insert( std::make_pair< std::string, std::pair< bool, xdata::Boolean* > >
                                     ( name, std::make_pair< bool, xdata::Boolean* > ( false, ptr )));
                    std::vector<bool> vals;
                    vals.push_back( false );
                    vals.push_back( false );
                    boolStreamValues_[name] = vals;
                    is_P->fireItemAvailable( name, ptr );
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );
                }
            else
                {
                    ERROR("Unknown type for item " << isitem.name << "  => " << isitem.type );
                }


            if ( cpis_P )
                cpis_P->unlock();
            is_P->unlock();
            
            //DEBUG("add docs");
            addParameterDocumentation( name, "string", getFormatted( name, 0, isitem.format ), isitem.update, isitem.documentation );
            //DEBUG("done");
            
        }
    itemlock_.give();
    DEBUG("setting done");
}

