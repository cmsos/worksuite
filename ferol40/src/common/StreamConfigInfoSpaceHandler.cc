#include "ferol40/StreamConfigInfoSpaceHandler.hh"
#include "ferol40/ferol40Constants.h"

ferol40::StreamConfigInfoSpaceHandler::StreamConfigInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS )
    : ferol40::StreamInfoSpaceHandler( xdaq, "StreamConfig", NULL,  appIS  )
{
    // name hwitem  update format doc
    // JRF TODO, this infospace should take the values from the applicationInfospace each time it's updated. Note that it need not get the values from the hardware.
    // JRF NOTE That not all of these parameters come directly from the Application XML parameters. Some of them are set by the application and some can be set using soap messages.
    //
	createDocumentationSeparator("Parameters related to the configuration of the data streams. The Ferol40 can be operated with up to 4 streams. In the input they can be either optical SLink or Event Generator sources. In the output they are handled as different TCP/IP streams. They always go to the same IP destination address but to different ports.");
	createstring( "DataSource", L10G_SOURCE, "", PROCESS, "Defines the Source of the data.  \"10G_SOURCE\" for receiving data via the 10Gbps optical input, and \"10G_CORE_GENERATOR_SOURCE\" for receiveing data via the optical 10Gbps input but this time data is generated in the data-generator of the sender core and finally \"GENERATOR_SOURCE\" for receiving data from the internal event generator in the FEROL40.");
	createbool(   "enable", false, "", PROCESS, "Enable the corresponding stream." );
	createstring( "DestinationIP", "0.0.0.0", "", PROCESS, "The destination IP address of the DAQ link. This can be either in dotted notation or a qulified hostname which will be looked up via the DNS." );
	createstring( "SourceIP", "0.0.0.0", "", PROCESS, "The IP address of the DAQ link in the Ferol40. This can be either in dotted notation or a qulified hostname which will be looked up via the DNS." );
   	createuint32( "TCP_SOURCE_PORT", 10, "", PROCESS, "The source port for the Stream of the Ferol40." );
	createuint32( "TCP_DESTINATION_PORT", 2000, "", PROCESS, "The destination port for the Stream of the Ferol40." );
   	createbool( "ENA_PAUSE_FRAME", true, "", PROCESS, "Specifies if the FEROL40 should consider TCP/IP pause-frames received via the DAQ link or wether to ignore them. " );  
	createuint32( "TCP_CWND", 800000, "", PROCESS, "" );
	createuint32( "nb_frag_before_BP", 0, "", PROCESS, "The maximal number of fragments in the time window defined by 'Window_trg_stop' before backpressure is generated in Stream. A value of 0 means that the 'artificial' backpressure is never activated." );
        createuint32( "Maximal_fragment_size", 16382, "", PROCESS, "Incoming fragments greater than this (value+2), measured in 64bit words, are truncated. The traler of these fragments is marked with 0xAA. No CRC will be generated for these fragments. The default corresponds to 128kB");

	

	createDocumentationSeparator("Parameters to control the event generators. These parameters are used to configure the generators of the FEROL40. If the Stdev values for the event size of the delay between events is set different from 0, the corresponding values are generated according to a log-normal distribution.");
	createuint32( "N_Descriptors", 1024, "", PROCESS, "This parameter is only used in the Ferol40 event generator. The maximum number of descriptors is 1024." ); //JRF TODO check if we still use this
	createuint32( "Event_Length_bytes", 0x1000, "", PROCESS, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration." ); 
    	createuint32( "Event_Length_Stdev_bytes", 0x0, "", PROCESS, "If set to 0 a fixed size events are generated" );
    	createuint32( "Event_Length_Max_bytes", 0x10000, "", PROCESS, "Only in use if Event_Length_Stdev_bytes is larger than 0! The event generators truncate at the given maximal event size. No events larger than this size will be generated." );
    	createuint32( "Event_Delay_ns", 20, "", PROCESS, "This delay is given in nanoseconds. The FRL hardware can only generate delays in steps of 10ns. 20 bits are available for the delay: therefore the maximal possible delay int the Frl is 10485750ns (10.48ms).  The Ferol40 hardware can generate delays in steps of 20ns. 16 bits are available for the delay: therefore the maximal possible delay in the Ferol40 is 1310700ns (1.31ms)" );
    	createuint32( "Event_Delay_Stdev_ns", 0, "", PROCESS, "If set to 0 a fixed delay between events is generated" );
    	createuint32( "Seed", 12345, "", PROCESS, "Used by the random generator." );


}


void
ferol40::StreamConfigInfoSpaceHandler::registerTrackerItems( utils::DataTracker &tracker )
{
    //JRF TODO, register the correct tracker items here if there are any.... Discuss with Dom and Christoph 
    //
    /*
    tracker.registerRateTracker( "", "", utils::DataTracker::HW64, 8.0);
    */
}

