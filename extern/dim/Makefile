#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2024, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini, D. Simelevicius                      #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# This is the extern/dim Makefile
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../..

BUILD_SUPPORT=build
PROJECT_NAME=worksuite
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
Package=extern/dim

PACKAGE_REQUIRED_PACKAGE_LIST = \
	motif

PackageName=dim

PACKAGE_VER_MAJOR=20
PACKAGE_VER_MINOR=37
PACKAGE_VER_PATCH=0

Summary=Distributed Information Management System

Description=\
DIM is a communication system for distributed environments.\
It provides a network transparent inter-process communication layer.

Link=http://www.cern.ch/dim
UNPACKDIR=dim_v20r37
TARFILE=dim_v20r37.zip
CFLAGS:=$(CFlags)
export CFLAGS

export XERCESCROOT=$(XDAQ_ROOT)/daq/extern/cgicc/$(XDAQ_OS)$(XDAQ_PLATFORM)
export OS=Linux
export GUI=no

export CC
export CXX
LD:=$(LDD)
export LD
#Requires tcsh because the .setup is a csh script
export SHELL=/bin/bash

_all: all

default: all

$(PACKING_DIR)/setup.sh:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(PACKING_DIR) 
	unzip -oa $(TARFILE)
	rm $(UNPACKDIR)/linux/*
	mv $(UNPACKDIR) $(PACKING_DIR)
	sed -i".bak" "s/^CFLAGS = /CFLAGS = $(CFlags) /" $(PACKING_DIR)/makefile_common

all: $(PACKING_DIR)/setup.sh
	cd $(PACKING_DIR); \
	source ./setup.sh; \
	make;
	mkdir -p $(XDAQ_PLATFORM)/include/dim $(XDAQ_PLATFORM)/lib $(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/dim/*.{h,hxx} $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/include/dim
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/*.{so,a} $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/lib
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/checkDns $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/did $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/DimBridge $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dim_get_service $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dim_send_command $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dns $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/test_client $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/testClient $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/test_server $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/testServer $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin

_installall: install

install: $(PACKING_DIR)/setup.sh
	mkdir -p $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/include/dim $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/lib $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/dim/*.{h,hxx} $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/include/dim
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/*.{so,a} $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/lib
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/checkDns $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/did $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/DimBridge $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dim_get_service $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dim_send_command $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dns $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/test_client $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/testClient $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/test_server $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/testServer $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin

_cleanall: clean

clean:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(PACKING_DIR)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfExternRPM.rules

