# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2020, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

BUILD_HOME:=$(shell pwd)/../../..

BUILD_SUPPORT=build
PROJECT_NAME=worksuite
PROJECT_FULL_NAMESPACE=cmsos-$(PROJECT_NAME)-
export BUILD_SUPPORT
export PROJECT_NAME
export PROJECT_FULL_NAMESPACE

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)
#
# Adapted Makefile for use with XDAQ make system
#
Project=$(PROJECT_NAME)
Package=extern/caen/a2818
export Project
export Package

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules

MHOME = $(BUILD_HOME)/$(Package)

ifeq ($(XDAQ_PLATFORM),x86_64_centos8)
export KBUILD_NOPEDANTIC=1
endif
ifeq ($(XDAQ_PLATFORM),x86_64_centoss8)
export KBUILD_NOPEDANTIC=1
endif
ifeq ($(XDAQ_PLATFORM),x86_64_centoss9)
export KBUILD_NOPEDANTIC=1
endif
ifeq ($(XDAQ_PLATFORM),x86_64_rhel8)
export KBUILD_NOPEDANTIC=1
endif
ifeq ($(XDAQ_PLATFORM),x86_64_alma9)
export KBUILD_NOPEDANTIC=1
endif

ARCH=intel_linux

KMOD_BASE = a2818
KMOD = $(addsuffix .ko,$(KMOD_BASE))
UNPACKDIR=A2818Drv-1.26
TARFILENAME=A2818Drv-1.26.tar

SRCDIR = $(MHOME)/$(UNPACKDIR)
OBJDIR = $(MHOME)/$(UNPACKDIR)
LIBDIR = $(MHOME)/lib/linux/$(XDAQ_PLATFORM)
INCDIR = $(MHOME)/$(UNPACKDIR)

#ifndef KERNEL_VERSION
KERNEL_VERSION := $(shell uname -r)
#endif

KERNEL_INCLUDE=-I/lib/modules/$(KERNEL_VERSION)/build/include
KERNEL_MAJOR   := $(shell echo $(KERNEL_VERSION) | perl -p -e 's/^(\d+\.\d+)\..*/$$1/')

KERNEL_VERSION_FULL=$(basename $(KERNEL_VERSION))

KERNEL_MAJOR:=$(shell echo $(KERNEL_VERSION) | perl -p -e 's/^(\d+\.\d+)\..*/$$1/')

KERNEL_DIR?=/usr/src/kernels/$(KERNEL_VERSION)

KERNEL_INCLUDE=-I/usr/src/kernels/$(KERNEL_VERSION)/include

INCLUDE = $(KERNEL_INCLUDE) -I$(INCDIR)

CFLAGS = -Wall -Wstrict-prototypes  \
	-D$(ARCH) -D_KERNEL_ -D_KERNEL -D__KERNEL

CFLAGS.intel_linux = -DPRINT_LEVEL=0 -O2

CPLUS = g++ -static -DASSERT $(CFLAGS) $(CFLAGS.$(ARCH))


_buildall: $(LIBDIR)/$(KMOD_BASE)

echo_% :
	@echo $* = '${$*}'



#
# Delegate to Makefile in src/common
# Use make M=dir to specify directory of external module to build
#

unpack:
	-rm -rf $(RELEASE_DIR) $(RELEASE_TAR) ./RPMBUILD
	tar -xvf $(TARFILENAME)
	cp $(BUILD_HOME)/$(Package)/$(UNPACKDIR)/Makefile.2.6-3.x $(BUILD_HOME)/$(Package)/$(UNPACKDIR)/Makefile

$(SRCDIR)/$(KMOD_BASE).c: unpack

$(LIBDIR)/$(KMOD_BASE): $(SRCDIR)/$(KMOD_BASE).c
	mkdir -p $(LIBDIR)
	make -C  $(KERNEL_DIR) M=$(SRCDIR) \
	LDDINCDIR=$(INCDIR) modules
	cp $(OBJDIR)/$(KMOD_BASE).ko $(LIBDIR)/.


_cleanall:
	rm -f *~ $(OBJDIR)/*.o $(LIBDIR)/*.{so,a} $(LIBDIR)/$(KMOD_BASE).ko
	-rm -rf $(BUILD_HOME)/$(Package)/$(UNPACKDIR) $(RELEASE_DIR) $(RELEASE_TAR) ./RPMBUILD $(BUILD_HOME)/$(Package)/lib

load: _loadall

_loadall:
	/bin/rm -f /dev/$(KMOD_BASE)
	/sbin/insmod ./$(LIBDIR)/$(KMOD)
	/bin/mknod /dev/$(KMOD_BASE) c `grep $(KMOD_BASE) /proc/devices | cut -d ' ' -f 1` 0
	/bin/chmod a+rw /dev/$(KMOD_BASE)

unload: _unloadall

_unloadall:
	/sbin/rmmod $(KMOD_BASE)

reload:
	make unload
	make load

_releaseall:




######################## make rpms  #####################
ifndef BUILD_VERSION
BUILD_VERSION=1
endif

ifndef BUILD_COMPILER
BUILD_COMPILER :=$(CC)$(shell $(CC) -dumpversion | sed -e 's/\./_/g')
endif

ifndef BUILD_DISTRIBUTION
BUILD_DISTRIBUTION := $(shell $(XDAQ_ROOT)/$(BUILD_SUPPORT)/checkos.sh)
endif

ifndef CONFIG_DIR
ifeq ( exists, $(shell [ -d $(BUILD_HOME)/$(BUILD_SUPORT) ] ) )
CONFIG_DIR=$(BUILD_HOME)
else
CONFIG_DIR=$(XDAQ_ROOT)
endif
endif



PACKAGE_NAME :=$(PROJECT_FULL_NAMESPACE)caen$(KMOD_BASE)
RPM_VER      :=$(shell cat $(BUILD_HOME)/$(Package)/version)
PACKAGE_VER  :=$(shell cat $(BUILD_HOME)/$(Package)/version | perl -p -e 's/^(\d+\.\d+)\.(\d+)/\1/' )
PACKAGE_PAT  :=$(shell cat $(BUILD_HOME)/$(Package)/version | perl -p -e 's/^(\d+\.\d+)\.(\d+)/\2/' )
PACKAGE_REL  :=$(BUILD_VERSION).$(PACKAGE_RELEASE).$(BUILD_DISTRIBUTION).$(BUILD_COMPILER)
RELEASE_DIR  :=$(PACKAGE_NAME)-$(RPM_VER)
RELEASE_TAR  :=$(RELEASE_DIR).tar.gz
DISTRIB      :=$(shell ${XDAQ_ROOT}/$(BUILD_SUPPORT)/checkos.sh)

PackageName=$(PACKAGE_NAME).$(XDAQ_PLATFORM)

RELEASE_SUBDIRS = $(Project)/$(Package)/$(UNPACKDIR) $(BUILD_SUPPORT)

RELEASE_FILES = $(Project)/$(Package)/spec.template   \
		$(Project)/$(Package)/$(TARFILENAME) \
		$(Project)/$(Package)/$(KMOD_BASE) \
		$(Project)/$(Package)/Makefile \
		$(Project)/$(Package)/Makefile.module \
		$(Project)/$(Package)/version \
                $(Project)/$(Package)/$(UNPACKDIR)/$(KMOD_BASE).h \
                $(Project)/$(Package)/$(UNPACKDIR)/$(KMOD_BASE).c \
                $(Project)/$(Package)/$(UNPACKDIR)/Makefile 

# treat them extra since they can come from different places
CONFIG_FILES =  $(BUILD_SUPPORT)/mfAutoconf.rules \
                $(BUILD_SUPPORT)/mfDefs.linux \
                $(BUILD_SUPPORT)/mfDefs.version \
                $(BUILD_SUPPORT)/mfRPM.release \
                $(BUILD_SUPPORT)/checkos.sh \
                $(BUILD_SUPPORT)/Makefile.rules


# spec_update #######################################################
.PHONY: spec_update
spec_update:
	perl -p -i -e 's/^(Version:).*/\1 $(RPM_VER)/' $(PackageName).spec # set release version in RPM
	perl -p -i -e 's/^(Release:).*/\1 $(PACKAGE_REL)/' $(PackageName).spec
	perl -p -i -e 's/__libversion__/$(PACKAGE_VER)/' $(PackageName).spec
	perl -p -i -e 's/__projectnamespace__/$(PROJECT_FULL_NAMESPACE)/' $(PackageName).spec
	perl -p -i -e 's/__projectname__/$(PROJECT_NAME)/' $(PackageName).spec
	perl -p -i -e 's/__platform__/$(XDAQ_PLATFORM)/' $(PackageName).spec
	perl -p -i -e 's/__author__/$(Authors)/' $(PackageName).spec


# package ###########################################################
.PHONY: release
release: 
	rm -rf $(RELEASE_DIR)                                              # make directories
	mkdir $(RELEASE_DIR)
	for i in $(RELEASE_SUBDIRS); do mkdir -p $(RELEASE_DIR)/$$i; done;
	cd ../../../..; for i in $(RELEASE_FILES); do cp -v $$i  $(Project)/$(Package)/$(RELEASE_DIR)/$$i; done;  # copy release files
	cd ../../../..; cp -v $(Project)/$(Package)/spec.template $(Project)/$(Package)/$(RELEASE_DIR)/$(Project)/$(Package)/$(PackageName).spec
	cd ../../../..; pwd; for i in $(CONFIG_FILES); do cp -v $(CONFIG_DIR)/$$i $(Project)/$(Package)/$(RELEASE_DIR)/$$i; done;  # copy config files
	make -C $(RELEASE_DIR)/$(Project)/$(Package) spec_update				   # set release version in RPM
	tar cvfz $(RELEASE_TAR) $(RELEASE_DIR)                             # make tarball
	rm -rf $(RELEASE_DIR)						   # cleanup

# srpm ###############################################################
.PHONY: srpm
srpm: release
	mkdir -p rpm
	mkdir -p RPMBUILD/{RPMS/{i386,i586,i686,x86_64},SPECS,BUILD,SOURCES,SRPMS}
	rpmbuild -ts --define "_topdir $(BUILD_HOME)/$(Package)/RPMBUILD" --define "kernel $(KERNEL_VERSION)" --define "kernelfull $(KERNEL_VERSION_FULL)"  \./$(RELEASE_TAR) ./$(RELEASE_TAR)
	find ./RPMBUILD -name "*.rpm" | xargs -i mv {} .
	rm -rf ./RPMBUILD rpm{rc,macros}.local
	mv $(PACKAGE_NAME)-$(RPM_VER)-$(PACKAGE_REL).src.rpm rpm/.
# In this version no distribution indicator in the rpm name
#	mv $(PACKAGE_NAME)-$(RPM_VER)-$(PACKAGE_REL).src.rpm rpm/$(PACKAGE_NAME)-$(RPM_VER)-$(PACKAGE_REL).$(DISTRIB).src.rpm 

.PHONY: rpm
# rpm ###############################################################
rpm: srpm
	mkdir -p rpm
	mkdir -p RPMBUILD/{RPMS/{i386,i586,i686,x86_64},SPECS,BUILD,SOURCES,SRPMS}
	rpmbuild -tb  --define "_topdir $(BUILD_HOME)/$(Package)/RPMBUILD" --define "kernel $(KERNEL_VERSION)" --define "kernelfull $(KERNEL_VERSION_FULL)" \./$(RELEASE_TAR)
	find ./RPMBUILD -name "*.rpm" | xargs -i mv {} .
	rm -rf ./RPMBUILD rpm{rc,macros}.local
	mv *.rpm rpm/.

_rpmall: rpm

# rpm installation rules #############################################
BUILD_HOME     ?= $(XDAQ_ROOT)
INSTALL_PREFIX ?= $(BUILD_HOME)

.PHONY: cleanrpm
cleanrpm:
	-rm -rf rpm

_cleanrpmall: cleanrpm

.PHONY: installrpm
installrpm:
	mkdir -p $(INSTALL_PREFIX)/rpm
	cp rpm/*.rpm $(INSTALL_PREFIX)/rpm

_installrpmall: installrpm

.PHONY: changelog
changelog:
	cd $(BUILD_HOME)/$(Package);\
	git show --summary > ChangeLog

_changelogall: changelog

