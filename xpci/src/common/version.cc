// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A.Petrucci	         			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xpci/version.h"

GETPACKAGEINFO(xpci)

void xpci::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);   
	CHECKDEPENDENCY(xcept);   
	CHECKDEPENDENCY(toolbox);   
}

std::set<std::string, std::less<std::string> > xpci::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,toolbox); 
  
	return dependencies;
}	
	
