#ifndef _interface_bril_shared_Utils_h
#define _interface_bril_shared_Utils_h
#include <vector>
#include <string>

namespace interface
{
	namespace bril
	{
		namespace shared
		{
			namespace Utils
			{
				static std::vector<std::string> splitstring(const std::string& input, char separator = ' ')
				{
					const char* str=input.c_str();
					std::vector<std::string> result;
					do
					{
						const char* begin = str;
						while(*str != separator && *str)
						{
							str++;
						}
						result.push_back(std::string(begin,str));
					} while (0 != *str++);
					return result;
				}
			}
		}
	}
}
#endif
