// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _interface_shared_version_h_
#define _interface_shared_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_INTERFACESHARED_VERSION_MAJOR 1
#define WORKSUITE_INTERFACESHARED_VERSION_MINOR 15
#define WORKSUITE_INTERFACESHARED_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_INTERFACESHARED_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_INTERFACESHARED_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_INTERFACESHARED_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_INTERFACESHARED_VERSION_MAJOR,WORKSUITE_INTERFACESHARED_VERSION_MINOR,WORKSUITE_INTERFACESHARED_VERSION_PATCH)
#ifndef WORKSUITE_INTERFACESHARED_PREVIOUS_VERSIONS
#define WORKSUITE_INTERFACESHARED_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_INTERFACESHARED_VERSION_MAJOR,WORKSUITE_INTERFACESHARED_VERSION_MINOR,WORKSUITE_INTERFACESHARED_VERSION_PATCH)
#else 
#define WORKSUITE_INTERFACESHARED_FULL_VERSION_LIST  WORKSUITE_INTERFACESHARED_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_INTERFACESHARED_VERSION_MAJOR,WORKSUITE_INTERFACESHARED_VERSION_MINOR,WORKSUITE_INTERFACESHARED_VERSION_PATCH)
#endif 


namespace interfaceshared
{
	const std::string project = "worksuite";
	const std::string package  =  "interfaceshared";
   	const std::string versions = WORKSUITE_INTERFACESHARED_FULL_VERSION_LIST;
	const std::string summary = "Header files shared by event builder and FED builder";
	const std::string description = "The header files are only required for development purposes";
	const std::string authors = "Steven Murray";
	const std::string link = "http://cms-ru-builder.web.cern.ch/cms-ru-builder";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
