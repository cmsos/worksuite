#include "interface/shared/GlobalEventNumber.h"
#include "interface/shared/GlobalEventNumber.i"

namespace evtn{

    bool evm_board_sense(const unsigned char *p, size_t size)
    {
      switch(size){
      case BST32_3BX:
	EVM_GTFE_BLOCK = EVM_GTFE_BLOCK_V0000;
	EVM_FDL_NOBX = 3;
	break;
      case BST32_5BX:
	EVM_GTFE_BLOCK = EVM_GTFE_BLOCK_V0000;
	EVM_FDL_NOBX = 5;
	break;
      case BST52_3BX:
	EVM_GTFE_BLOCK = EVM_GTFE_BLOCK_V0011;
	EVM_FDL_NOBX = 3;
	break;
      case BST52_5BX:
	EVM_GTFE_BLOCK = EVM_GTFE_BLOCK_V0011;
	EVM_FDL_NOBX = 5;
	break;
      default:
	EVM_GTFE_BLOCK = EVM_GTFE_BLOCK_V0000;
	EVM_FDL_NOBX = 3;	    
      }
      return (*(unsigned int*)(p + sizeof(fedh_t) + EVM_BOARDID_OFFSET * SLINK_WORD_SIZE / 2) >> EVM_BOARDID_SHIFT) == EVM_BOARDID_VALUE;
    }


    bool set_evm_board_sense(const unsigned char *p)
    {
        unsigned int boardId = 
            (*(unsigned int*)(p + sizeof(fedh_t) + EVM_BOARDID_OFFSET * SLINK_WORD_SIZE / 2)
                >> EVM_BOARDID_SHIFT);
        if (boardId != EVM_BOARDID_VALUE) return false;
        
        unsigned int setupVersion =
            (*(unsigned int*)(p + sizeof(fedh_t) + EVM_GTFE_SETUPVERSION_OFFSET * SLINK_WORD_SIZE / 2));
        
        if ( (setupVersion & EVM_GTFE_SETUPVERSION_MASK) == EVM_GTFE_SETUPVERSION_MASK )
        {
            EVM_GTFE_BLOCK = EVM_GTFE_BLOCK_V0011;
        }
        else
        {
            EVM_GTFE_BLOCK = EVM_GTFE_BLOCK_V0000;
        }
        
        unsigned int fdl_mode = 
            (*(unsigned int*)(p + sizeof(fedh_t) + EVM_GTFE_FDLMODE_OFFSET * SLINK_WORD_SIZE / 2));
        if ( (fdl_mode & EVM_GTFE_FDLMODE_MASK) == EVM_GTFE_FDLMODE_MASK )
        {
            EVM_FDL_NOBX = 5;
        }
        else
        {
            EVM_FDL_NOBX = 3;
        }
        
        return true;
    }
    
    bool has_evm_tcs(const unsigned char *p)
    {
        unsigned int boardId =
            (*(unsigned int*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK*2 + EVM_TCS_BOARDID_OFFSET) * SLINK_WORD_SIZE / 2))
            >> EVM_TCS_BOARDID_SHIFT;
        return (boardId == EVM_TCS_BOARDID_VALUE);
    }

    bool has_evm_fdl(const unsigned char *p)
    {
        unsigned int boardId =
            (*(unsigned int*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK + EVM_TCS_BLOCK 
                    + EVM_FDL_BLOCK * (EVM_FDL_NOBX/2) ) * SLINK_WORD_SIZE +
                EVM_FDL_BOARDID_OFFSET * SLINK_HALFWORD_SIZE))
            >> EVM_FDL_BOARDID_SHIFT;
        return (boardId == EVM_FDL_BOARDID_VALUE);
    }

    unsigned int offset(bool evm)
    {
      if(evm)
	return sizeof(fedh_t) + (EVM_GTFE_BLOCK*2 + EVM_TCS_TRIGNR_OFFSET) * SLINK_WORD_SIZE / 2;
      else
	return sizeof(fedh_t) + DAQ_TOTTRG_OFFSET * SLINK_WORD_SIZE / 2;
    }
    unsigned int get(const unsigned char *p, bool evm)
    {
      return *(unsigned int*)( p+offset(evm) );
    }
    unsigned int gtpe_get(const unsigned char *p)
    {
      return *(unsigned int*)( p + GTPE_TRIGNR_OFFSET*SLINK_HALFWORD_SIZE );
    }
    unsigned int getlbn(const unsigned char *p)
    { 
      return (*(unsigned int*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK*2 + EVM_TCS_LSBLNR_OFFSET) * SLINK_WORD_SIZE / 2)) 
	& EVM_TCS_LSBLNR_MASK;
    }
    unsigned int gtpe_getlbn(const unsigned char *p)
    { 
      return gtpe_getorbit(p)/0x00100000;
    }
    unsigned int getgpslow(const unsigned char *p)
    { 
      return (*(unsigned int*)( p+sizeof(fedh_t) + EVM_GTFE_BSTGPS_OFFSET * SLINK_WORD_SIZE / 2));
    }
    unsigned int getgpshigh(const unsigned char *p)
    { 
      return (*(unsigned int*)( p+sizeof(fedh_t) + EVM_GTFE_BSTGPS_OFFSET * SLINK_WORD_SIZE / 2 + SLINK_HALFWORD_SIZE));
    }
    unsigned int getorbit(const unsigned char *p)
    { 
      return (*(unsigned int*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK*2 + EVM_TCS_ORBTNR_OFFSET) * SLINK_WORD_SIZE / 2));
    }
    unsigned int getevtyp(const unsigned char *p)
    { 
      return (((*(unsigned int*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK*2 + EVM_TCS_LSBLNR_OFFSET) * SLINK_WORD_SIZE / 2)) 
	      & EVM_TCS_EVNTYP_MASK) >> EVM_TCS_EVNTYP_SHIFT);
    }
    unsigned int gtpe_getorbit(const unsigned char *p)
    { 
      return (*(unsigned int*)( p + GTPE_ORBTNR_OFFSET * SLINK_HALFWORD_SIZE));
    }
    unsigned int getfdlbx(const unsigned char *p)
    { 
      return (*(unsigned int*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK + EVM_TCS_BLOCK 
						    + EVM_FDL_BLOCK * (EVM_FDL_NOBX/2) ) * SLINK_WORD_SIZE +
				EVM_FDL_BCNRIN_OFFSET * SLINK_HALFWORD_SIZE)) &  EVM_TCS_BCNRIN_MASK;
    }
    unsigned int getfdlbxevt(const unsigned char *p)
    { 
        return (((*(unsigned int*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK + EVM_TCS_BLOCK 
                    + EVM_FDL_BLOCK * (EVM_FDL_NOBX/2) ) * SLINK_WORD_SIZE +
                EVM_FDL_BCNRIN_OFFSET * SLINK_HALFWORD_SIZE))
                & EVM_FDL_BCNRINEVT_MASK) >> EVM_FDL_BCNRINEVT_SHIFT);
    }
    unsigned int gtpe_getbx(const unsigned char *p)
    { 
      return (*(unsigned int*)( p + GTPE_BCNRIN_OFFSET * SLINK_HALFWORD_SIZE)) &  GTPE_BCNRIN_MASK;
    }
    unsigned int getfdlpsc(const unsigned char *p)
    { 
      return (*(unsigned int*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK + EVM_TCS_BLOCK 
						    + EVM_FDL_BLOCK * (EVM_FDL_NOBX/2)) * SLINK_WORD_SIZE +
				EVM_FDL_PSCVSN_OFFSET * SLINK_HALFWORD_SIZE));
    }
    unsigned long long getfdlttr(const unsigned char *p)
    {
       return (*(unsigned long long*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK + EVM_TCS_BLOCK 
							   + EVM_FDL_BLOCK * (EVM_FDL_NOBX/2)) * SLINK_WORD_SIZE +
				EVM_FDL_TECTRG_OFFSET * SLINK_HALFWORD_SIZE));
    }
    unsigned long long getfdlta1(const unsigned char *p)
    {
       return (*(unsigned long long*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK + EVM_TCS_BLOCK 
							   + EVM_FDL_BLOCK * (EVM_FDL_NOBX/2)) * SLINK_WORD_SIZE +
				EVM_FDL_ALGOB1_OFFSET * SLINK_HALFWORD_SIZE));
    }
    unsigned long long getfdlta2(const unsigned char *p)
    {
       return (*(unsigned long long*)( p+sizeof(fedh_t) + (EVM_GTFE_BLOCK + EVM_TCS_BLOCK 
							   + EVM_FDL_BLOCK * (EVM_FDL_NOBX/2)) * SLINK_WORD_SIZE +
				EVM_FDL_ALGOB2_OFFSET * SLINK_HALFWORD_SIZE));
    }
}
