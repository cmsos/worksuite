// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_TStoreAPI_h_
#define _tstore_TStoreAPI_h_

#include "tstore/Connection.h"
#include "xoap/MessageReference.h"
#include "tstore/Mapping.h"
#include "tstore/exception/Exception.h"
#include "xoap/SOAPEnvelope.h"

#define TSTORE_NS_URI "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd"

namespace tstore 
{

	class ForeignKey {
		public:
		std::string childTableName;
		std::map<std::string, std::string, xdata::Table::ci_less> linkedColumns; //key is the referencing column, value is the referenced column
		bool operator==(const ForeignKey &that) const {
			if (childTableName==that.childTableName) {
				return (linkedColumns==that.linkedColumns);
			}
			return false;
		}
	};

	void processForeignKeys(std::vector<tstore::ForeignKey> &processedKeys,xdata::Table &foreignKeysInDatabase) ;

	template <class OutputIterator>
	void separateKeys(OutputIterator output,const std::string &keys) {
		std::istringstream is(toolbox::toupper(keys));
		std::string key;
		while (std::getline(is,key,',')) {
			//std::cout << "\t'" << key << "'" << std::endl;
			key=toolbox::trim(key);
			*output++=key;
		}
	}

	/* View subclasses will receive a pointer to a TStoreAPI as an argument to most of their methods.
	The TStoreAPI provides access to everything you should need to process a TStore message. */

	class TStoreAPI {
		public:
		TStoreAPI(tstore::Connection *connection,xoap::MessageReference request,xoap::MessageReference response,tstore::MappingList &mappings,bool dryRun=false,DOMNode *config=NULL,std::map<const std::string,std::string> *tableNames=NULL) ;
		~TStoreAPI();
		//returns an open connection to the database specified in the view's configuration.
		tstore::Connection *connection();

		//call this from your query() or definition() method to add a table to be returned to the sender of the message as a SOAP attachment.
		//contentId is the Content-ID of the SOAP attachment. Your view should document the Content-ID(s) of the table(s) it returns.
		//You can add more than one table, but they should have different content IDs.
		void addResultTable(xdata::Table &result,const std::string &contentId) ;

		//call this from your insert(), update() or delete() methods to get the first table attached to the SOAP message. 
		//If you expect more than one table, you should use getTable and specify a Content-ID so that you know which table is which.
		//returns whether a table was found and successfully read.
		bool getFirstTable(xdata::Table&) ;

		//call this from your insert(), update() or delete() methods to get the table with the given Content-ID attached to the SOAP message.
		//returns whether a table with the given ID was found and successfully read.
		bool getTable(xdata::Table&,std::string contentId) ;

		//call this to get type mappings for the specified database table.
		//If you pass in a blank table name, then all mappings will be returned.
		void getMappingsForTable(std::string tableName,tstore::MappingList &mappings) ;

		//call this during an addTables or removeTables method to find the names of the tables listed in the message
		//with <table name="" key=""> tags. If you call it from any other method then it will throw an exception.
		//keys in the map are table names, values are the name(s) of the key columns
		void getTableNames(std::map<const std::string,std::string> &tableNames) ;

		//as above but only returns the table names, not the keys.
		void getTableNames(std::vector<std::string> &tableNames) ;

		//call this during an addTables or removeTables method to get the DOMNode for this view's configuration, so that
		//you can modify it according to the views being added or removed.
		//If you call it from any other method then it will throw an exception.
		DOMNode *getConfiguration() ;

		void addTable(const std::string &tableName,const std::string &key,xdata::Table &definition) ;
		void removeTable(const std::string &tableName) ;
		void removeTableMetadata(const std::string &tableName) ;
		void addTableMetadata(/*const*/ xdata::Table &table,const std::string &tableName,const std::string &key) ;
		void addTableMetadata(/*const*/ xdata::Table &table,const std::string &tableName,const std::string &key,const std::vector<tstore::ForeignKey> &foreignKeys) ;

		void getForeignKeysForTable(const std::string &tableName,std::vector<tstore::ForeignKey> &foreignKeysInDatabase) ;

		//add the foreign key to both the metadata and the database
		//both this and addForeignKeysToDatabase do not add the constraints immediately,
		//they wait until the rest of the sync/addTable/etc operation is finished, so that you have a chance to create the referenced tables.
		void addForeignKey
		(
			const std::string &tableName,
			const std::string &referencedTableName,
			const std::map<std::string,std::string, xdata::Table::ci_less> &columns
		) 
		;

		//adds the foreign key constraints to the database, but not to the configuration	
		template <class InputIterator>
		void addForeignKeysToDatabase(const std::string &tableName,InputIterator begin,InputIterator end)  {
			InputIterator key;
			for (key=begin;key!=end;++key) {
				keysToAdd_.insert(std::pair<std::string,tstore::ForeignKey>(tableName,*key));
			}
		}
		//adds the foreign key to te configuration, but not to the database.
		void addForeignKeyMetadata
		(
			const std::string &tableName,
			const std::string &referencedTableName,
			const std::map<std::string,std::string, xdata::Table::ci_less> &columns
		) 
		;
		
		private:
		
		TStoreAPI(TStoreAPI &);
		DOMNode *getNodeForTable(const std::string &tableName);
		bool hasTable(const std::string &tableName);
		void removeTableMetadataFromConfig(const std::string &tableName);
		void addTableMetadataToConfig(/*const*/ xdata::Table &table,const std::string &tableName,const std::string &key) ;
		void addColumnDefinition(tstore::MappingList &mappings, const std::string &xdataType,const std::string &columnName,const std::string &tableName);
		xoap::SOAPElement addLogElement(const std::string &elementName,const std::string &tableName);

		tstore::Connection *connection_;
		xoap::MessageReference request_;
		xoap::MessageReference response_;
		tstore::MappingList &mappings_;
		std::map<const std::string,std::string> *tableNames_;
		DOMNode *config_;
		xoap::SOAPElement mainResponseElement_;
		bool dryRun_;
		std::multimap<std::string,tstore::ForeignKey> keysToAdd_;
	};

}

#endif
