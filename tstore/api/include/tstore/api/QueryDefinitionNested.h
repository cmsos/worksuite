// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_QueryDefinitionNested_h_
#define _tstore_api_QueryDefinitionNested_h_

#include <string>
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/exception/Exception.h"
#include "tstore/api/Request.h"
#include "xdata/Table.h"

namespace tstore
{
namespace api
{
	/*!
		Use this request message to retrieve the table definition for the
		tables used in an insert statement identified by \param insertStatementName.
		The response contains the xdata::Table objects used in the insert statement
		as attachements to the message. The tables returned are empty.
	*/
	class QueryDefinitionNested: public tstore::api::Request
	{
		public:
		
		QueryDefinitionNested
		(
			const std::string& connectionId,
			const std::string& tableName,
			size_t depth
		);
		
		QueryDefinitionNested
		(
			xoap::MessageReference& msg
		) 
		;
        
		virtual ~QueryDefinitionNested();
				
		xoap::MessageReference toSOAP();

		std::string getConnectionId();
		
		std::string getTableName();

		private:

		std::string connectionId_;
		std::string tableName_;
		size_t depth_;
	};
}}

#endif
