// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstoreapi_version_h_
#define _tstoreapi_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_TSTOREAPI_VERSION_MAJOR 1
#define WORKSUITE_TSTOREAPI_VERSION_MINOR 1
#define WORKSUITE_TSTOREAPI_VERSION_PATCH 2
// If any previous versions available E.g. #define WORKSUITE_TSTOREAPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"

#undef WORKSUITE_TSTOREAPI_PREVIOUS_VERSIONS

//
// Template macros
//
#define WORKSUITE_TSTOREAPI_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TSTOREAPI_VERSION_MAJOR,WORKSUITE_TSTOREAPI_VERSION_MINOR,WORKSUITE_TSTOREAPI_VERSION_PATCH)
#ifndef WORKSUITE_TSTOREAPI_PREVIOUS_VERSIONS
#define WORKSUITE_TSTOREAPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TSTOREAPI_VERSION_MAJOR,WORKSUITE_TSTOREAPI_VERSION_MINOR,WORKSUITE_TSTOREAPI_VERSION_PATCH)
#else 
#define WORKSUITE_TSTOREAPI_FULL_VERSION_LIST  WORKSUITE_TSTOREAPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TSTOREAPI_VERSION_MAJOR,WORKSUITE_TSTOREAPI_VERSION_MINOR,WORKSUITE_TSTOREAPI_VERSION_PATCH)
#endif 

namespace tstoreapi
{
	const std::string project = "worksuite";
	const std::string package  =  "tstoreapi";
	const std::string versions = WORKSUITE_TSTOREAPI_FULL_VERSION_LIST;
	const std::string description = "C++ wrappers for generating TStore service SOAP commands";
	const std::string authors = "Luciano Orsini and JOhannes Gutleber";
	const std::string summary = "Table store service library (SOAP C++ wrappers)";	
	const std::string link = "http://xdaqwiki.cern.ch/index.php/TStore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
