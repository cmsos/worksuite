// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_AddView_h_
#define _tstore_api_admin_AddView_h_

#include <string>
#include "toolbox/net/exception/Exception.h"
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/admin/exception/Exception.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
namespace admin
{
	class AddView: public tstore::api::Request
	{
		public:
		
		AddView
		(
			const std::string& viewId,
			const std::string& fileName,
			const std::string& database
		);
		
		AddView(xoap::MessageReference& msg) ;
        
		virtual ~AddView();
				
		xoap::MessageReference toSOAP();

		std::string getViewId();

		std::string getFileName();
		
		std::string getDatabase();
		

		private:

		std::string viewId_;
		std::string fileName_;
		std::string database_;
	};
}}}

#endif
