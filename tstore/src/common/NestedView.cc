// $Id$
// A view which recusively fetches data that is related to each row of a given table by foreign key constraint
// THIS IS A DRAFT, not all features have been implemented

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/NestedView.h"
#include "xoap/domutils.h"
#include "xercesc/dom/DOMNode.hpp"
#include "xdata/TableIterator.h"
#include "toolbox/string.h"

//these should be templates
void getValuesOfMap(std::map<const std::string,std::string> &pairs,std::set<std::string> &values) {
	std::map<const std::string,std::string>::iterator thisPair;
	for (thisPair=pairs.begin();thisPair!=pairs.end();thisPair++) {
		values.insert((*thisPair).second);
	}
}

void getKeysOfMap(std::map<const std::string,std::string> &pairs,std::set<std::string> &values) {
	std::map<const std::string,std::string>::iterator thisPair;
	for (thisPair=pairs.begin();thisPair!=pairs.end();thisPair++) {
		values.insert((*thisPair).first);
	}
}

//this should ideally be done within the Connection object, since the interface for foreign keys depends on the RDBMS
xdata::Table &tstore::NestedView::constraints(tstore::Connection *connection)  {
	if (!constraintsLoaded_) {
		try {
			connection->getForeignKeys(constraints_);
		} catch (xdata::exception::Exception &e) {
			XCEPT_RETHROW (tstore::exception::Exception, "Could not load constraints information. "+e.message(), e);
		} catch (tstore::exception::Exception &e) {
			XCEPT_RETHROW (tstore::exception::Exception, "Could not load constraints information. "+e.message(), e);
		}
	}
	constraintsLoaded_=true;
	return constraints_;
}			

void tstore::NestedView::addConstraintToList(constraintList &constraintsForTable,const std::string &tableName,std::map<const std::string,std::string> &joinColumns) {
	if (!tableName.empty()) {
		if (!joinColumns.empty()) { //joinColumns can be empty if the constraint didn't actually apply to this table
			constraintsForTable.insert	(
											std::pair<const std::string,std::map<const std::string,std::string> >(tableName,joinColumns)
										);
			joinColumns.clear();
		}
	}
}

void tstore::NestedView::getConstraintsForTable(tstore::Connection *connection,const std::string &tableName,constraintList &constraintsForTable,bool bothWays)  {
	try {
		xdata::Table &allConstraints=constraints(connection);
		std::map<const std::string,std::string> joinColumns;
		xdata::Table::iterator constraint;
		std::string thisConstraint,lastConstraint;
		std::string lastTable;
		for (constraint=allConstraints.begin();constraint!=allConstraints.end();++constraint) {
			std::string parent=(*constraint).getField(tstore::parentField)->toString();
			std::string child=(*constraint).getField(tstore::childField)->toString();
			std::string parentColumn=(*constraint).getField(tstore::parentColumnField)->toString();
			std::string childColumn=(*constraint).getField(tstore::childColumnField)->toString();
			thisConstraint=(*constraint).getField(tstore::constraintField)->toString();
			if (!lastConstraint.empty() && thisConstraint!=lastConstraint) {
				addConstraintToList(constraintsForTable,lastTable,joinColumns);
			}
			if (parent==toolbox::toupper(tableName)) {
				//constraintsForTable.insert(*constraint);
				joinColumns[parentColumn]=childColumn;
				lastTable=child;
			} else if (bothWays && child==toolbox::toupper(tableName)) {
				joinColumns[childColumn]=parentColumn;
				lastTable=parent;
			}
			lastConstraint=thisConstraint;
		}
		addConstraintToList(constraintsForTable,lastTable,joinColumns);
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception, "Could not load constraints information for table '"+tableName+"'", e);
	}
}

//run this to check whether a column name for a nested column clashes with an existing column
//this simply throws an exception saying what the problem is (if people want such dual links then
//we will have to go back to the old naming system in this situation. But for now nobody seems to want it.)
//tableName is the name of the outer table, it is only used to make the error message more descriptive.
void tstore::NestedView::checkForExistingColumn(xdata::Table &results,const std::string &tableName,const std::string &columnName)  {
	const std::map<std::string, std::string, xdata::Table::ci_less > &columnDefinitions=results.getTableDefinition();
	if ( columnDefinitions.find(columnName) != columnDefinitions.end() )
	{
		std::string columnType=columnDefinitions.at(columnName);
		if (columnType=="table") {
			XCEPT_RAISE(tstore::exception::Exception,"There is more than one foreign key relationship between "+tableName+" and "+columnName);
		} else {
			XCEPT_RAISE(tstore::exception::Exception,"Name of column to contain records from table "+columnName+" clashes with an existing column of type "+columnType);
		}
	}
}

const static std::string childParentSeparator="->";
const static std::string tableColumnsSeparator="_"; //I should change this to : to reduce ambiguity

std::string tstore::NestedView::columnNameForChildColumn(const std::string &tableName,std::map<const std::string,std::string> &joinColumns)  {
	std::string columnName(tableName);
	std::map<const std::string,std::string>::iterator joinColumn;
	//instead of making a column name which consists of the table name and columns involved to ensure uniqueness
	//when there are many foreign key relationships,
	//just use the name of the 'child' table.
	//On insert, if this is ambiguous (i.e. there are several foreign key relationships between the same 
	//two tables) then an error will be returned. Johannes and Luciano don't think anyone will use such
	//relationships anyway.
	return columnName;
/*	for (joinColumn=joinColumns.begin();joinColumn!=joinColumns.end();++joinColumn) {
		columnName+=tableColumnsSeparator+(*joinColumn).first+childParentSeparator+(*joinColumn).second; //in fact, to ensure the column name is unique, we only need the column names from whichever table is actually the parent. But this could look confusing when some columns have the name of a column in this table, and others have the name of a column in a subtable
	}*/
	//std::cout << columnName << std::endl;
	return columnName;
}

bool tstore::NestedView::parentColumnExists
(
	std::map<std::string, std::string, xdata::Table::ci_less > &columns,
	const std::string &columnName
) 
{
	if (columns.count(columnName)) 
	{
		return columns[columnName]!="table";
	}
	return false;
}

void tstore::NestedView::splitParentColumn
(
	std::string &parentColumn,
	std::string &childColumn,
	std::string &source,
	std::map<std::string, std::string, xdata::Table::ci_less > &columns
) 
	 
{
	std::string::size_type end=source.size();
	//std::cout << "splitting " << source << std::endl;
	std::string potentialParentColumnName;
	std::string potentialChildColumnName;
	do {
		//loop to find where the real tablename/parentColumn separator is, which gives a parent column that exists
		std::string::size_type endOfFirstPart=source.rfind(tableColumnsSeparator,end);
		if (endOfFirstPart==std::string::npos || //there is no _
			endOfFirstPart==end-tableColumnsSeparator.size()) { //there is a _ but there is nothing after it
			XCEPT_RAISE(tstore::exception::Exception,"No parent column name given, or the parent column did not match another column in the table.");
		}
		std::string::size_type parentColumnStart=tableColumnsSeparator.size()+endOfFirstPart;
		potentialParentColumnName=source.substr(parentColumnStart,end-parentColumnStart);
		potentialChildColumnName=source.substr(0,parentColumnStart-tableColumnsSeparator.size());
		//std::cout << "potentialParentColumnName: " << potentialParentColumnName << " potentialChildColumnName: " << potentialChildColumnName << std::endl;
	} while (!parentColumnExists(columns,potentialParentColumnName));
	parentColumn=potentialParentColumnName;
	childColumn=potentialChildColumnName;
}

void tstore::NestedView::addColumn(std::vector<std::string> &columns,const std::string &newColumn,const std::string &name)  {
	//std::cout << "adding " << name << " column '" << newColumn << "'" << std::endl;
	if (newColumn.empty()) {
		XCEPT_RAISE(tstore::exception::Exception,"Empty "+name+" column.");
	}
	if (std::find(columns.begin(),columns.end(),newColumn)!=columns.end()) {
		XCEPT_RAISE(tstore::exception::Exception,"More than one occurrence of the "+name+" column '"+newColumn+"'");
	}
	columns.push_back(newColumn);
} 

//the column should be named the same way as one returned from columnNameForChildColumn()
//i.e. TABLENAME_PARENTCOLUMN->CHILDCOLUMN
//this could be ambiguous since both a table name and a column name could contain a _, but some ambiguity is removed
//by the fact that we know all possible PARENTCOLUMNs.
//well, actually now columnNameForChildColumn has been simplified and just returns the name of the child table.
//but we will leave this function the same, since it is only used by addTable which is deprecated anyway,
//and all the information is still necessary for creating the table.
void tstore::NestedView::parseChildColumnName
(
	std::string &tableName,
	std::map<std::string, std::string, xdata::Table::ci_less> &linkedColumns,
	const std::string &columnName,
	std::map<std::string, std::string, xdata::Table::ci_less> &columns
) 
	 
{
	try {
		std::vector<std::string> parentColumns;
		std::vector<std::string> childColumns;
		std::string::size_type endParentColumn, begin=0;
		while (true) {
			endParentColumn = columnName.find(childParentSeparator, begin);
			if (endParentColumn==std::string::npos) {
				if (begin==0) {
					XCEPT_RAISE(tstore::exception::Exception,"No child column name given.");
				}
				addColumn(childColumns,columnName.substr(begin),"child");
				break;
			} else {
				std::string newParent;
				std::string tableAndParent=columnName.substr(begin,endParentColumn-begin); //the first time through the loop, this is TABLENAME_PARENTCOLUMN. Any other times, it is CHILDCOLUMN_PARENTCOLUMN
				if (begin==0) {
					splitParentColumn(newParent,tableName,tableAndParent,columns);
				} else {
					std::string newChild;
					splitParentColumn(newParent,newChild,tableAndParent,columns);
					addColumn(childColumns,newChild,"child");
				}
				addColumn(parentColumns,newParent,"parent");
			}
			begin = endParentColumn + childParentSeparator.size();
		}
		if (parentColumns.size()!=childColumns.size()) {
			//this situation should have been picked up earlier
			std::ostringstream error;
			error << "Number of child columns (" << childColumns.size() << ") does not equal to number of parent columns (" << parentColumns.size() << ") specified.";
			XCEPT_RAISE(tstore::exception::Exception,error.str());
		} else {
			//copy parents and children as keys and values into map so they're easier to handle.
			std::vector<std::string>::iterator child,parent;
			for (child=childColumns.begin(),parent=parentColumns.begin();child!=childColumns.end();++child,++parent) {
				linkedColumns[*child]=*parent;
			}
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not understand column name '"+columnName+"': "+e.what()+" Names of columns of type 'table' should be in the format TABLENAME_PARENTCOLUMN->CHILDCOLUMN where PARENTCOLUMN matches the name of another column in the table.");
	}
}

std::string tstore::NestedView::condition(std::pair<const std::string,std::string> &joinColumn) {
	return joinColumn.second+"=:"+joinColumn.first; //it doesn't matter what the bind parameter is called, but we'll name it after the column which the value comes from
}

std::string tstore::NestedView::selectStatementForChildColumn(const std::string &tableName,std::map<const std::string,std::string> &joinColumns)  {
	std::string whereClause;
	std::map<const std::string,std::string>::iterator joinColumn;
	
	//construct the where clause, and the name of the column where we will put the results
	for (joinColumn=joinColumns.begin();joinColumn!=joinColumns.end();++joinColumn) {
		if (whereClause.empty()) whereClause="where ";
		else whereClause+=" and ";
		whereClause+=condition(*joinColumn);
	}
	
	//todo
	//this should change to select specific columns, most likely all columns except for the join columns
	//that way there won't be endless looping between two tables that link to each other,
	//nor will there be a whole lot of rows with the same values in them.
	//however Frank says not to do this since it would make it difficult to just pass a single sub-table around
	//it still could be a useful option though, to save bandwidth etc.
	std::string selectStatement="SELECT * from "+tableName+" "+whereClause;
	//std::cout << selectStatement << std::endl;
	return selectStatement;
}

void tstore::NestedView::setValueNULL(tstore::SQLQuery &query,std::pair<const std::string,std::string> &joinColumn) {
	std::string statement=query.getProperty("statement");
	std::string existingCondition(condition(joinColumn));
	std::string::size_type location=statement.find(existingCondition);
	if (location!=std::string::npos) statement.replace(location,existingCondition.size(),joinColumn.second+" is NULL");
	//std::cout << "After replacing " << existingCondition << " " << statement << std::endl;
	query.setProperty("statement",statement);
}

void tstore::NestedView::runSubQuery(xdata::Table &results,TStoreAPI *API,const std::string &tableName,std::map<const std::string,std::string> &joinColumns,unsigned int recursionDepth)  {
	//todo
	//depending on the ratio of distinct combinations of keys in the parent table and the child table,
	//it might be more efficient to just select everything from the child table and then go through the results to put them into 
	//the right rows.
	//also, it might be a good idea to reuse results from the parent table, since you will reach each table again two levels down
	//going parent->child->parent etc. However then you would have more recursion depth than necessary and all sorts of weird problems
	//with copying parts of the table that are still being written to.
	//std::cout << "recursionDepth=" << recursionDepth << std::endl;
	std::string selectStatement;
	std::string columnName;
	
	selectStatement=selectStatementForChildColumn(tableName,joinColumns);
	columnName=columnNameForChildColumn(tableName,joinColumns);
	
	checkForExistingColumn(results,"this table",columnName);
	results.addColumn(columnName,"table");

	tstore::SQLQuery query(selectStatement,tableName);
	tstore::MappingList mappings;
	
	//todo
	//this might change if they want to define more complicated mappings depending on the 'path' to the current table
	API->getMappingsForTable(tableName,mappings);
	unsigned int rowCount=results.getRowCount();
	unsigned int rowIndex;
	
	try {
		std::map<const std::string,long int> loaded;
		std::string keyCombination;
		std::map<const std::string,std::string>::iterator joinColumn;
		for (rowIndex=0;rowIndex<rowCount;rowIndex++) {
			xdata::Table childTable;
			unsigned int columnIndex=0;
			keyCombination.clear();
			query.setProperty("statement",selectStatement);
			for (joinColumn=joinColumns.begin();joinColumn!=joinColumns.end();++joinColumn,++columnIndex) {
				xdata::Serializable *joinValue=results.getValueAt(rowIndex,(*joinColumn).first);
				//this needs to change to set an XData value directly, so that dates can be entered properly
				std::string joinString=joinValue->toString();
				if (joinString=="NaN" && joinValue->type()!="string") {
					setValueNULL(query,*joinColumn);
					--columnIndex;
				} else {
					query.setParameterAtIndex(columnIndex,joinValue);
				}
				keyCombination+=joinString+"_";
			}
		
			//if we have already fetched the results for this combination of keys, just copy the results we already have
			if (loaded.count(keyCombination)) {
				xdata::Serializable *preloadedChildTable=results.getValueAt(loaded[keyCombination],columnName);
				results.setValueAt(rowIndex,columnName,*preloadedChildTable);
			} else {
				queryCount_++;
				API->connection()->execute(query,childTable,mappings);
				if (recursionDepth>0) getSubTables(childTable,API,recursionDepth-1,tableName);
				results.setValueAt(rowIndex,columnName,childTable);
				loaded[keyCombination]=rowIndex;
			}
		}
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception, "Could not query table '"+tableName+"': "+e.message(), e);
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception, "Could not query table '"+tableName+"': "+e.message(), e);
	}
}

void tstore::NestedView::getSubTables(xdata::Table &results,TStoreAPI *API,unsigned int recursionDepth,const std::string &parentTable)  {
	constraintList constraintsForTable;
	std::string parentTableName=parentTable;
	if (parentTableName.empty()) parentTableName=mainTable_;
	try {
		getConstraintsForTable(API->connection(),parentTableName,constraintsForTable);
		
		constraintList::iterator constraint;
		std::string lastTable;
		std::string thisTable;
		std::string childColumn;
		std::string parentColumn;
		for (constraint=constraintsForTable.begin();constraint!=constraintsForTable.end();++constraint) {
			runSubQuery(results,API,(*constraint).first,(*constraint).second,recursionDepth);
		}
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception, "Could not get subtables for table '"+parentTable+"': "+e.message(), e);
	}
}

void tstore::NestedView::getChildTableDefinition(TStoreAPI *API,xdata::Table &results,const std::string &tableName,std::set<std::string> &columnsToExclude,unsigned int recursionDepth)  {
	try {
		tstore::MappingList mappings;
		std::map<const std::string,std::string> types;
		API->getMappingsForTable(tableName,mappings);
		API->connection()->getColumnTypes(tableName,types,mappings);
		std::map<const std::string,std::string>::iterator columnIterator;
		for (columnIterator=types.begin();columnIterator!=types.end();columnIterator++) {
			std::string columnName=(*columnIterator).first;
			if (!columnsToExclude.count(columnName)) {
				std::string columnType=(*columnIterator).second;
				results.addColumn(columnName,columnType);
			}
		}
		if (recursionDepth>0) {
			//add additional "table" columns, with one row filled with the result of getChildTableDefinition on each child table
			constraintList constraintsForTable;
			getConstraintsForTable(API->connection(),tableName,constraintsForTable,false); //only follow links in one direction, so nothing is duplicated
			constraintList::iterator constraint;
			std::set<std::string> childColumnsToExclude;
			for (constraint=constraintsForTable.begin();constraint!=constraintsForTable.end();constraint++) {
				getValuesOfMap((*constraint).second,childColumnsToExclude);
			}
			for (constraint=constraintsForTable.begin();constraint!=constraintsForTable.end();constraint++) {
				xdata::Table childTable;
				std::string columnName=columnNameForChildColumn((*constraint).first,(*constraint).second);
				//std::map<const std::string,std::string>::iterator joinColumn;
				//getValuesOfMap((*constraint).second,childColumnsToExclude);
				/*for (joinColumn=(*constraint).second.begin();joinColumn!=(*constraint).second.end();joinColumn++) {
					childColumnsToExclude.insert((*joinColumn).second);
				}*/
				getChildTableDefinition(API,childTable,(*constraint).first,childColumnsToExclude,recursionDepth-1);
				checkForExistingColumn(results,tableName,columnName);
				results.addColumn(columnName,"table");
				results.setValueAt(0,columnName,childTable);
			}
		}
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception,"Could not get definition. "+e.message(),e);
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception,"Could not get definition. "+e.message(),e);
	}
}

void tstore::NestedView::insertChildTables(TStoreAPI *API,const std::string &tableName,xdata::Table &newRows,unsigned int recursionDepth)  {
	constraintList constraintsForTable;
	getConstraintsForTable(API->connection(),tableName,constraintsForTable,false); //only follow links in one direction, so nothing is duplicated
	constraintList::iterator constraint;
	std::set<std::string> columnNames;
	for (constraint=constraintsForTable.begin();constraint!=constraintsForTable.end();++constraint) {
		std::string columnName(columnNameForChildColumn((*constraint).first,(*constraint).second));
		std::pair<std::set<std::string>::iterator, bool> insertedName;
		insertedName=columnNames.insert(columnName);
		if (!insertedName.second) {
			XCEPT_RAISE(tstore::exception::Exception,"There is more than one foreign key relationship between "+tableName+" and "+(*constraint).first); //in fact, we could determine which foreign key relationship it is by seeing which columns are missing from the child table, as those must be the ones that need to be filled in from the parent table. But this implies that people are actually using the correct columns according to the definition, and if they were doing that they'd have already had a problem creating two columns with the same name. So we'll cross that bridge if we come to it.
		}
		try {
			if (newRows.getColumnType(columnName)=="table") {
				xdata::TableIterator row;
				xdata::Table childKeyValues;
				std::map<const std::string,std::string> joinColumns((*constraint).second);
				std::map<const std::string,std::string>::iterator joinColumn;

				for (joinColumn=joinColumns.begin();joinColumn!=joinColumns.end();++joinColumn) {
					childKeyValues.addColumn((*joinColumn).second,newRows.getColumnType((*joinColumn).first));
					//std::cout << "adding column " << (*joinColumn).second << std::endl;
				}
				
				for (row=newRows.begin();row!=newRows.end();++row) {
					xdata::Table *childTable=(xdata::Table*)((*row).getField(columnName));
					for (joinColumn=joinColumns.begin();joinColumn!=joinColumns.end();++joinColumn) {
						childKeyValues.setValueAt(0,(*joinColumn).second,*(*row).getField((*joinColumn).first));
					}
					insert(API,(*constraint).first,*childTable,childKeyValues,recursionDepth);
				}
			} else {
				XCEPT_RAISE(tstore::exception::Exception,"The column "+columnName+" should be of type table.");
			}
		} catch (xdata::exception::Exception &e) {
			XCEPT_RETHROW(tstore::exception::Exception,"Could not insert contents of "+columnName+": "+e.what(),e);
		}
	}
}

void tstore::NestedView::insert(TStoreAPI *API,
									const std::string &tableName,
									xdata::Table &newRows,
									xdata::Table &keyValues, //the values of key columns, which we already know from the parent table
									unsigned int recursionDepth)
								 {
	try {
		tstore::MappingList mappings;
		API->getMappingsForTable(tableName,mappings);
		std::map<const std::string,std::string> types;
		
		//find out which columns are really in this table
		API->connection()->getColumnTypes(tableName,types,mappings);
		std::set<std::string> columnsToInsert;
		getKeysOfMap(types,columnsToInsert);
		tstore::SQLInsert insert(columnsToInsert,tableName);
		
		//fill in the extra columns which contain the keys from the parent table
		std::map<std::string, std::string, xdata::Table::ci_less > keyColumns(keyValues.getTableDefinition());
		std::map<std::string, std::string, std::less<std::string> >::iterator keyColumn;
		for (keyColumn=keyColumns.begin();keyColumn!=keyColumns.end();keyColumn++) {
			try {
				newRows.addColumn((*keyColumn).first,(*keyColumn).second);
			} catch (xdata::exception::Exception &e) {
				//most likely this is an exception due to the table already having a column for the key.
				//should really have the name of the column here.
				XCEPT_RETHROW(tstore::exception::Exception, "You have supplied a column named "+(*keyColumn).first+" in a child table where that value is already specified by the parent table.",e);
			}
			xdata::TableIterator row;
			for (row=newRows.begin();row!=newRows.end();row++) {
				(*row).setField((*keyColumn).first,*keyValues.getValueAt(0,(*keyColumn).first));
			}
		}
		//todo: here we could check the table definition against types to make sure there are no columns missing
		
		
		insert.data=&newRows;
		API->connection()->execute(insert,mappings);
	} catch (xcept::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception, "Could not insert into "+tableName+". "+e.message(),e);
	}
	//now go through the child columns
	if (recursionDepth>0) {
		insertChildTables(API,tableName,newRows,recursionDepth-1);
	}
		
}


void tstore::NestedView::parseWhereClause(DOMNode *node,const std::string &tableName)  {
	DOMNodeList *children=node->getChildNodes();
	int childrenCount=children->getLength();
	int childIndex;
	std::pair<parameterList,parameterList> defaultParameters;
	//std::cout << "Adding table '" << tableName << "'" << std::endl;
	for (childIndex=0;childIndex<childrenCount;childIndex++) {
		DOMNode *child=children->item(childIndex);
		if (child->getNodeType()==DOMNode::CDATA_SECTION_NODE) {
			whereClauses_[tableName]=toolbox::trim(xoap::XMLCh2String(child->getNodeValue()));
			//std::cout << "Added where clause for table '" << tableName << "':" << whereClauses_[tableName] << std::endl;
		} else if (child->getNodeType()==DOMNode::ELEMENT_NODE && xoap::XMLCh2String(child->getLocalName())=="parameter") {
			tstore::View::readParameterDefinition(defaultParameters.first,defaultParameters.second,child);
		}
	}
	defaultTableParameters_[tableName]=defaultParameters;
}

/////////////////////standard view interface
tstore::NestedView::NestedView(std::string configurationPath,std::string name)  : View(configurationPath,name) {
	constraintsLoaded_=false;
	//should read list of tables from the configuration
	DOMNode *config=getConfiguration(configurationPath);
	if (config) {
		DOMNodeList *children=config->getChildNodes();
		int childrenCount=children->getLength();
		int childIndex;
		for (childIndex=0;childIndex<childrenCount;childIndex++) {
			DOMNode *child=children->item(childIndex);
			std::string name=xoap::XMLCh2String(child->getLocalName());
			std::string nodePrefix=xoap::XMLCh2String(child->getNamespaceURI());
			if (nodePrefix==namespaceURI()) {
				try {
					if (child->getNodeType()==DOMNode::ELEMENT_NODE && xoap::XMLCh2String(child->getLocalName())=="table") {
						std::string tableName=toolbox::toupper(xoap::getNodeAttribute(child, "name"));
						if (!tableName.empty()) {
							std::string defaultRecursion=xoap::getNodeAttribute(child, "depth");
							unsigned int recursionDepth=0;
							if (!defaultRecursion.empty()) {
								std::istringstream depth(defaultRecursion);
								depth >> recursionDepth;
							}
							defaultDepths_[tableName]=recursionDepth;
							parseWhereClause(child,tableName);
						} else {
							XCEPT_RAISE(tstore::exception::Exception,"Table tag with no name attribute found");
						}
					} else {
						XCEPT_RAISE(tstore::exception::Exception,"Unrecognised tag: "+xoap::XMLCh2String(child->getLocalName()));
					}
				} catch (xcept::Exception &e) {
					XCEPT_RETHROW(tstore::exception::Exception,e.what(),e);
				}
			}
		}
	}	
	resetParameters();
}

bool tstore::NestedView::canQuery() const {
	return true;
}

bool tstore::NestedView::canUpdate() const {
	return false;
}

bool tstore::NestedView::canInsert() const {
	return true;
}

bool tstore::NestedView::canCreate() const {
	return false;
}

bool tstore::NestedView::canRemove() const {
	return true;
}

void tstore::NestedView::create(TStoreAPI *API)  {
}

//throws an exception if there is no table parameter.
void tstore::NestedView::checkTableSpecified()  {
	if (mainTable_.empty()) {
		XCEPT_RAISE(tstore::exception::Exception, "No table specified for view '"+name()+"'. Check the 'table' parameter.");
	}
}

void tstore::NestedView::query(TStoreAPI *API)  {
	xdata::Table results;
	queryCount_=1;
	tstore::MappingList mappings;
	std::pair<parameterList,parameterList> parameters;
	checkTableSpecified();
	separateParameterValues(parameters);
	//maybe this should share some code with runSubQuery 
	try {
		std::vector<std::string> bindParameters;
		std::string parsedWhereClause=tstore::View::substituteParameters(&bindParameters,parameters.first,parameters.second,whereClause_);
		API->getMappingsForTable(mainTable_,mappings);
		tstore::SQLQuery query("SELECT * from "+mainTable_+" "+parsedWhereClause,bindParameters,mainTable_);
		//set the bind parameters
		//add parameters
		API->connection()->execute(query,results,mappings);
		if (recursionDepth_>0) getSubTables(results,API,recursionDepth_-1);
		//std::cout << queryCount_ << " queries" << std::endl;
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception, "Could not query table '"+mainTable_+"': "+e.message(), e);
	}
	API->addResultTable(results,mainTable_);
}

std::string tstore::NestedView::namespaceURI() {
	return "urn:tstore-view-Nested";
}

void tstore::NestedView::insert(TStoreAPI *API)  {
	xdata::Table newRows;
	xdata::Table keyValues;
	checkTableSpecified();
	if (API->getFirstTable(newRows)) {
		try {
			insert(API,mainTable_,newRows,keyValues,recursionDepth_);
			API->connection()->commit();
		} catch (tstore::exception::Exception &e) {
			API->connection()->attemptRollback(); //if one fails, roll back the updates of all the tables
			XCEPT_RETHROW (tstore::exception::Exception, "Could not insert into "+mainTable_+". "+e.message(), e);
		}
	} else {
		XCEPT_RAISE(tstore::exception::Exception, "No data given");
	}
}

/*In order to prevent inconsistencies in inserts, the definition is a bit different from the table structure returned from a query.
It only follow links in one direction, so there can be no repetition of the same table in the insert (unless there are actually links in both directions)
It also omits the key columns of child tables since they should be the same as the relevant columns in the parent.*/
void tstore::NestedView::definition(TStoreAPI *API)  {
	xdata::Table results;
	std::set<std::string> columnsToExclude; //include all columns in the head table
	checkTableSpecified();
	getChildTableDefinition(API,results,mainTable_,columnsToExclude,recursionDepth_);
	API->addResultTable(results,mainTable_);
}

void tstore::NestedView::clear(TStoreAPI *API)  {
	checkTableSpecified();
	try {
		tstore::SQLDelete deletion(mainTable_);
		API->connection()->execute(deletion);
		API->connection()->commit();
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception, "Could not clear "+name()+". "+e.message(), e);
	}
}

//for the sake of simplicity, this just deletes from the main table,
//hoping that the foreign keys are set up with cascade delete. Unless people will want to delete only some rows of the
//subtables and null the keys in the others, there is no need making TStore do what the database can do itself.
//
//Since the linkage goes both ways, it might be necessary to manually remove things from 'child' tables which are 
//really parent tables, in which case we could
//just navigate to the highest parent table and delete there (letting the cascade rule do the rest.) However that
//would mean that other children of that item would also be deleted, even though they weren't included in the delete command.
void tstore::NestedView::remove(TStoreAPI *API)  {
	xdata::Table rowsToDelete;
	checkTableSpecified();
	if (API->getFirstTable(rowsToDelete)) {
		try {
			tstore::SQLDelete deletion(mainTable_);
			deletion.data=&rowsToDelete;
			API->connection()->execute(deletion);
			API->connection()->commit();
		} catch (tstore::exception::Exception &e) {
			XCEPT_RETHROW (tstore::exception::Exception, "Could not delete from "+name()+". "+e.message(), e);
		}
	} else {
		XCEPT_RAISE(tstore::exception::Exception, "No data given");
	}
}

void tstore::NestedView::separateParameterValues(std::pair<parameterList,parameterList> &parameters)  {
	parameters=defaultTableParameters_[mainTable_];
	parameterList::iterator parameter;
	
	try
	{
		for (parameter=tableParameters_.begin();parameter!=tableParameters_.end();++parameter) 
		{
			tstore::View::setParameter(parameters.first,parameters.second,(*parameter).first,(*parameter).second);
		}
	}
	catch (tstore::exception::InvalidView& e)
	{
		XCEPT_RETHROW (tstore::exception::Exception, "Set parameter error", e);
	}
}

void tstore::NestedView::setParameter(const std::string &parameterName,const std::string &value)  {
	if (parameterName=="table") {
		std::string table=toolbox::toupper(value);
		mainTable_=table;
		if (defaultDepths_.count(table)) {
			//use the default depth for that table
			if (!depthSet_) recursionDepth_=defaultDepths_[table];
			whereClause_=whereClauses_[table];
		}
	} else if (parameterName=="depth") {
		std::istringstream depth(value);
		depth >> recursionDepth_;
		depthSet_=true;
	} else {
		tableParameters_[parameterName]=value;
	}
}

void tstore::NestedView::resetParameters() {
	mainTable_="";
	recursionDepth_=0;
	depthSet_=false;
	tableParameters_.clear();
	whereClause_="";
}

void tstore::NestedView::createTable(TStoreAPI *API,const std::string &tableName,const std::string &primaryKey,xdata::Table &definition,std::map<const std::string,std::string> &tablesCreated)  {
	if (!tablesCreated.count(tableName)) {
		try {
			API->addTable(tableName,primaryKey,definition);
		} catch (tstore::exception::Exception &e) {
			XCEPT_RETHROW(tstore::exception::Exception,"Could not create table '"+tableName+"'",e);
		}
	} else if (!keysMatch(tablesCreated[tableName],primaryKey)) {
		XCEPT_RAISE(tstore::exception::Exception,"Already created a table named "+tableName+" with primary key "+tablesCreated[tableName]+". Cannot create another table of that name with primary key "+primaryKey);
	}
	tablesCreated[tableName]=primaryKey;
}

bool tstore::NestedView::keysMatch(const std::string &keys1,const std::string &keys2) {
	std::set<std::string> keySet1,keySet2;
	std::insert_iterator<std::set<std::string> > iterator1(keySet1,keySet1.begin());
	std::insert_iterator<std::set<std::string> > iterator2(keySet2,keySet2.begin());
	//std::cout << "getting keys1" << std::endl;
	separateKeys(iterator1,keys1);
	//std::cout << "getting keys2" << std::endl;
	separateKeys(iterator2,keys2);
	return keySet1==keySet2;
}

void tstore::NestedView::addTable(TStoreAPI *API,xdata::Table &definition,const std::string &tableName,std::map<const std::string,std::string> &tablesCreatedWithKeys,std::vector<std::string> &tablesCreated,const std::string &suppliedPrimaryKey)  {
	std::map<std::string, std::string, xdata::Table::ci_less > columns=definition.getTableDefinition();
	std::string primaryKey=suppliedPrimaryKey;
	try {
		//todo: this vector is not necessary any more, since TStoreAPI itself waits until the entire operation is over before creating the foreign key constraints
		std::vector<tstore::ForeignKey> foreignKeyStatements;
		//std::cout << "adding table " << tableName << std::endl;
		for(std::map<std::string, std::string, xdata::Table::ci_less >::iterator column=columns.begin(); column!=columns.end(); ++column) {
			if ((*column).second=="table") {
				//create the sub-table, with a foreign key linking it to this one
				//std::string childTableName;
				std::string foreignKey;
				//std::map<std::string, std::string> linkedColumns;
				ForeignKey key;
				parseChildColumnName(key.childTableName,key.linkedColumns,(*column).first,columns);
				
				xdata::Table childDefinition;
				try {
					std::string newPrimaryKey;
					childDefinition=*static_cast<xdata::Table *>(definition.getValueAt(0,(*column).first));
					//add the extra columns which are part of the foreign key
					
					for (std::map<std::string, std::string>::iterator linkedColumn=key.linkedColumns.begin();linkedColumn!=key.linkedColumns.end();++linkedColumn) {
						//The primary key of the parent table is determined by the parent columns in the column name of the child table
						if (!newPrimaryKey.empty()) newPrimaryKey+=",";
						newPrimaryKey+=(*linkedColumn).second;
						//the child columns in the column name of the child table have to be added to the child table. They should have the same
						//types as the corresponding parent columns.
						//std::cout << "adding column " << (*linkedColumn).second << " with type " << columns[(*linkedColumn).first] << " to match " << (*linkedColumn).first << std::endl;
						childDefinition.addColumn((*linkedColumn).first,columns[(*linkedColumn).second]);
						//std::cout << "added" << std::endl;
					}
					if (primaryKey.empty()) {
						primaryKey=newPrimaryKey;
					} else {
						if (!keysMatch(primaryKey,newPrimaryKey)) { //make sure they are the same columns
							XCEPT_RAISE(tstore::exception::Exception,"Can not determine the primary key column(s) of table "+tableName+". One child table links by "+primaryKey+" while another links by "+newPrimaryKey+".");
						}
					}
					
				} catch (xdata::exception::Exception &e) {
					XCEPT_RETHROW(tstore::exception::Exception,"No definition supplied for table "+key.childTableName,e);
				}
				
				addTable(API,childDefinition,key.childTableName,tablesCreatedWithKeys,tablesCreated);
				//these need to be saved in a vector to be executed after the parent table has been created.
				foreignKeyStatements.push_back(key);
				definition.removeColumn((*column).first);
			}
		}
		//std::cout << "adding table " << tableName << " with primary key " << primaryKey << std::endl;
		bool newTable=!tablesCreatedWithKeys.count(tableName);
		createTable(API,tableName,primaryKey,definition,tablesCreatedWithKeys);
		if (newTable) {
			tablesCreated.push_back(tableName);
		}
		std::vector<tstore::ForeignKey>::iterator foreignKey;
		//now that the table has been created, add the constraints to it.
		for (foreignKey=foreignKeyStatements.begin();foreignKey!=foreignKeyStatements.end();++foreignKey) {
			//API->connection()->addForeignKey((*foreignKey).childTableName,tableName,(*foreignKey).linkedColumns);
			API->addForeignKey((*foreignKey).childTableName,tableName,(*foreignKey).linkedColumns);
		}
		constraintsLoaded_=false;
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not create table "+tableName,e);
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not create table "+tableName+" with primary key "+primaryKey,e);
	}
}

void tstore::NestedView::addTables(TStoreAPI *API)  {
	std::map<const std::string,std::string> tableNames;
	API->getTableNames(tableNames);
	std::map<const std::string,std::string>::const_iterator tableName;
	std::map<const std::string,std::string> tablesCreatedWithKeys;
	std::vector<std::string> tablesCreated;
	try {
		for (tableName=tableNames.begin();tableName!=tableNames.end();++tableName) {
			xdata::Table definition;
			API->getTable(definition,(*tableName).first);
			addTable(API,definition,(*tableName).first,tablesCreatedWithKeys,tablesCreated,(*tableName).second);
			//should also add mappings for subtables.
			//there also needs to be a removeTables which removes subtables.
		}
	} catch (tstore::exception::Exception &addException) {
		std::string errorMessage="Could not create table '"+(*tableName).first+"' or its subtables.";
		//loop through tablesCreated in reverse order and remove the tables if possible
		std::vector<std::string>::reverse_iterator tableToRemove;
		for (tableToRemove=tablesCreated.rbegin();tableToRemove!=tablesCreated.rend();++tableToRemove) {
			try {
				//std::cout << "CLEANUP: removing " << *tableToRemove << std::endl;
				API->removeTable(*tableToRemove);
			} catch (tstore::exception::Exception &removeException) {
				errorMessage+=" Additionally, encountered the following error while trying to clean up by removing '"+*tableToRemove+"':"+removeException.what();
			}
		}
		XCEPT_RETHROW(tstore::exception::Exception,errorMessage,addException);
	}

}

void tstore::NestedView::removeTables(TStoreAPI *API)  {
	std::vector<std::string> tableNames;
	std::set<std::string> tablesRemoved; //keep track of the tables we have removed to make sure we don't remove the same one twice due to dual links
	API->getTableNames(tableNames);
	//std::cout << "removing " << tableNames.size() << " tables." << std::endl;
	std::vector<std::string>::const_iterator tableName;
	for (tableName=tableNames.begin();tableName!=tableNames.end();++tableName) {
		removeTable(API,*tableName,tablesRemoved);
		constraintsLoaded_=false;
	}
}

void tstore::NestedView::removeTable(TStoreAPI *API,const std::string &tableName,std::set<std::string> &tablesRemoved)  {
	try {
		//std::cout << "removing " << tableName << std::endl;
		constraintList constraintsForTable;
		getConstraintsForTable(API->connection(),tableName,constraintsForTable,false); //only follow links in one direction, so nothing is duplicated
		constraintList::iterator constraint;
		//std::cout << constraintsForTable.size() << " subtables." << std::endl;
		for (constraint=constraintsForTable.begin();constraint!=constraintsForTable.end();constraint++) {
			removeTable(API,toolbox::toupper((*constraint).first),tablesRemoved);
		}
		if (!tablesRemoved.count(tableName)) {
			//std::cout << "really removing " << tableName << std::endl;
			API->removeTable(tableName);
			tablesRemoved.insert(tableName);
		}
		//std::cout << "removed " << tableName << std::endl;
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not remove table '"+tableName+"'",e);
	}

}
