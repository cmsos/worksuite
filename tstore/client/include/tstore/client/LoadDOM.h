// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#ifndef _tstoreclient_LoadDOM_h_
#define _tstoreclient_LoadDOM_h_


namespace tstoreclient
{
	std::string parsePath(const std::string& pathname) ;
	std::string parseLocalPath(const std::string &path) ;

	void writeXML(DOMNode *pDoc,const std::string &path) ;
	std::string writeXML(DOMNode *pDoc) ;
	void writeXML(DOMNode *pDoc,XMLFormatTarget *target) ;
	
	void removeBlankTextNodes(DOMNode *node);

	std::vector<DOMNode *> nodesWithPrefix(DOMNode *node,const std::string &prefix);	
}


#endif
