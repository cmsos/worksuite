// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xcept/Exception.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/NamespaceURI.h"
#include "toolbox/Runtime.h"
#include "xoap/domutils.h"
#include "xercesc/dom/DOMLSSerializer.hpp"
#include "xercesc/framework/LocalFileFormatTarget.hpp"
#include "xercesc/framework/MemBufFormatTarget.hpp"
#include "tstore/client/LoadDOM.h"
#include <sstream>

std::string tstoreclient::parsePath(const std::string& pathname)  {
	std::vector<std::string> files;
	try {
		files = toolbox::getRuntime()->expandPathName(pathname);
	} 
	catch (toolbox::exception::Exception& tbe) {
		
		XCEPT_RETHROW (xcept::Exception, "Cannot parse pathname " + pathname, tbe);
	}
	
	if (files.size() == 1) {		
		if ((files[0].find("file:/") == std::string::npos) && (files[0].find("http://"))) {
			files[0].insert(0, "file:");
		}
		return files[0];
	}
	else {
		XCEPT_RAISE (xcept::Exception, "Cannot load ambiguous pathname " + pathname);
	}
}

void tstoreclient::removeBlankTextNodes(DOMNode *node) {
	DOMNodeList *children=node->getChildNodes();
	signed int childrenCount=children->getLength();
	signed int childIndex=childrenCount;
	while (childIndex--) { //going backwards avoids index problems when nodes are deleted
		DOMNode *child=children->item(childIndex);
		if (child->getNodeType()==DOMNode::TEXT_NODE) {
			std::string text=xoap::XMLCh2String(child->getNodeValue());
			if (text.find_first_not_of("\n\r\t ")==std::string::npos) {
				node->removeChild(child);
			}
		} else {
			removeBlankTextNodes(child);
		}
	}
}

std::string tstoreclient::parseLocalPath(const std::string &path)  {
	std::string parsedPath(parsePath(path));
	if (0==parsedPath.find("file:/")) {
		parsedPath.replace(0,5,"");
	} 
	else {
		XCEPT_RAISE(xcept::Exception,"The path "+parsedPath+" is not local.");
	}
	return parsedPath;
}

void tstoreclient::writeXML(DOMNode *pDoc,XMLFormatTarget *target) 
{
	DOMImplementation *pImplement = NULL;
	DOMLSSerializer *pSerializer = NULL;

	try
	{
		DOMNode *mutableNode=pDoc; //pDoc->cloneNode(true);
		removeBlankTextNodes(mutableNode);
		// get a serializer, an instance of DOMWriter (the "LS" stands for load-save).
		pImplement = DOMImplementationRegistry::getDOMImplementation(xoap::XStr("LS"));
		if (!pImplement)
		{
			XCEPT_RAISE(xcept::Exception,"Could not get DOM implementation");
		}

		pSerializer = ( (DOMImplementationLS*)pImplement )->createLSSerializer();
		DOMLSOutput* theOutput = ((DOMImplementationLS*)pImplement)->createLSOutput();
		theOutput->setByteStream(target);
		theOutput->setEncoding( xoap::XStr("UTF-8") );
		// set user specified end of line sequence and output encoding
		pSerializer->setNewLine( xoap::XStr("\n") );

		// set feature if the serializer supports the feature/mode
		if ( pSerializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTSplitCdataSections, false) )
		{
			pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTSplitCdataSections, false);
		}
		
		if ( pSerializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTDiscardDefaultContent, false) )
		{
			pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTDiscardDefaultContent, false);
		}
		
		if ( pSerializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true) )
		{
			pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);
		}
		
		if ( pSerializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTBOM, false) )
		{
			pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTBOM, false);
		}

		pSerializer->write(pDoc, theOutput);

		pSerializer->release();
		theOutput->release();
	}
	catch (DOMException &e)
	{
		XCEPT_RAISE(xcept::Exception,"Error writing XML: " + xoap::XMLCh2String(e.msg));
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xcept::Exception,"Error writing XML", e);
	}
}

std::string tstoreclient::writeXML(DOMNode *pDoc)  {
	MemBufFormatTarget *target=NULL;
	try {
		target=new MemBufFormatTarget(80960);
		tstoreclient::writeXML(pDoc,target);
		const XMLByte *buffer=target->getRawBuffer();
		std::ostringstream xml;
		if (buffer) {
			xml << buffer;
		}
		return xml.str();
		delete target;
	} catch (xcept::Exception &) {
		if (target) delete target;
		throw;
	}
}

void tstoreclient::writeXML(DOMNode *pDoc,const std::string &path)  {
	XMLFormatTarget *target = NULL;
	std::string parsedPath;
	try {
		parsedPath=parseLocalPath(path);
		target= new LocalFileFormatTarget(xoap::XStr(parsedPath));
		writeXML(pDoc,target);
		delete target;
	} catch (DOMXPathException &e) {
		XCEPT_RAISE(xcept::Exception,"Could not create LocalFileFormatTarget for file "+parsedPath+": "+xoap::XMLCh2String(e.msg));
	} catch (xcept::Exception &e) {
		if (target) delete target;
		XCEPT_RETHROW(xcept::Exception,"Error writing file "+parsedPath,e);
	}
}

std::vector<DOMNode *> tstoreclient::nodesWithPrefix(DOMNode *node,const std::string &prefix) {
	std::vector<DOMNode *> prefixedNodes;
	DOMNodeList* nodes = node->getChildNodes();
	unsigned long nodeCount=nodes->getLength();
	for (unsigned long nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++) {
		DOMNode *node = nodes->item(nodeIndex);
		std::string nodePrefix=xoap::XMLCh2String(node->getNamespaceURI());
		if (nodePrefix==prefix) {
			prefixedNodes.push_back(node);
		}
	}
	return prefixedNodes;
}
