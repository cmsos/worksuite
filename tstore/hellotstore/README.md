# Hello TStore

This is the complete source code for a [Web Application](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/WebApplication) which queries an [SQLView](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/SQLView) and logs the result. It uses a helper class called `Request` to create the [SOAP messages](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreSOAPServiceProtocol). Unlike [Your first XDAQ application](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/YourFirstXDAQApplication), it does not do everything in the constructor, since at that point the [TStore](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStore) application descriptor hasn't been loaded yet. Instead, it has a simple web interface to call the query method. When you click the link on the web interface, the HelloTStore interface [connects](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreSOAPServiceProtocol#Connect) to TStore, performs the [query](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreSOAPServiceProtocol#Query), [disconnects](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreSOAPServiceProtocol#Connect), and then loops through the `xdata::Table` and writes the results to the log destination and the web interface using [Log4cplus](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/Log4cplus) and the [CGICC](https://www.gnu.org/software/cgicc/doc/index.html) class. To use it, you have to change the view name and connection details in the source code to the values of the database to connect to.

## Implementation
There are two classes in this project. The `HelloTStore` is the application, implements the HTML and callback. It defines the connection, query and disconnection.

```c++
#ifndef _HelloTStore_h_
#define _HelloTStore_h_

#include "xdaq/Application.h"
#include "xdaq/WebApplication.h"
#include "xdata/Table.h"

namespace tstore {
	class HelloTStore: public xdaq::Application
	{
		public:
			HelloTStore(xdaq::ApplicationStub * s);
			void query(xgi::Input * in, xgi::Output * out );

		private:
			xoap::MessageReference sendSOAPMessage(xoap::MessageReference &message);
			std::string connect();
			void disconnect(const std::string &connectionID);
			void query(const std::string &connectionID,xdata::Table &results);
			void Default(xgi::Input * in, xgi::Output * out );

	};
}
#endif
```
The `Request` simplifies creating a SOAP message with TStore-wide parameters and [View parameter](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/ViewParameter)s. You initialise it with the name of the command (the first element in the SOAP message) e.g. connect or query. If your message will have any parameters specific to a [TStore view](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreView) class, then you will also need to supply the name of the view class, which is used as the namespace for those parameters. If your message will not have any view-specific parameters, for example a [connect](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreSOAPServiceProtocol#Connect) or [disconnect](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreSOAPServiceProtocol#Disconnect) message, then you can initialise it without a view class.

```c++
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPElement.h"

namespace tstore {
	class Request {
		public:
			Request(const std::string &commandName,const std::string &viewClass="");
			void addTStoreParameter(const std::string &parameterName,const std::string &parameterValue);
			void addViewSpecificParameter(const std::string &parameterName,const std::string &parameterValue);
			xoap::MessageReference toSOAP();

		private:
			std::string viewClass_;
			std::string commandName_;
			std::map<const std::string,std::string> generalParameters_;
			std::map<const std::string,std::string> viewSpecificParameters_;
			void addParametersWithNamespace(xoap::SOAPElement &element,
							xoap::SOAPEnvelope &envelope,
							std::map<const std::string,std::string> &parameters,
							const std::string &namespaceURI,
							const std::string &namespacePrefix);
			xoap::MessageReference sendSOAPMessage(xoap::MessageReference &message);
	};
}
```
## Configuration
In general, services are defined in `.profile` and applications in `.xml` configs. Profiles are used to pre-load frequently used applications into the XDAQ processes, thus avoiding the need for explicit configuration. To add TStore as a service, we extend the `default.profile` by the TStore service.

```xml
<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->
	<xp:Application class="executive::Application" id="0" group="profile" service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libi2outils.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>

	<xp:Application class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local" service="pthttp">
		 <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
		 	<documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
            <aliasName xsi:type="xsd:string">tmp</aliasName>
            <aliasPath xsi:type="xsd:string">/tmp</aliasPath>
            <expiresByType xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[6]">
			    <item xsi:type="soapenc:Struct" soapenc:position="[0]">
			        <name xsi:type="xsd:string">image/png</name>
			        <value xsi:type="xsd:string">PT4300H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[1]">
			        <name xsi:type="xsd:string">image/jpg</name>
			        <value xsi:type="xsd:string">PT4300H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[2]">
			        <name xsi:type="xsd:string">image/gif</name>
			        <value xsi:type="xsd:string">PT4300H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[3]">
			        <name xsi:type="xsd:string">application/x-shockwave-flash</name>
			        <value xsi:type="xsd:string">PT120H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[4]">
			        <name xsi:type="xsd:string">application/font-woff</name>
			        <value xsi:type="xsd:string">PT8600H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[5]">
			        <name xsi:type="xsd:string">text/css</name>
			        <value xsi:type="xsd:string">PT4300H</value>
			    </item>
		   </expiresByType>
		   <httpHeaderFields xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[3]">
		   		<item xsi:type="soapenc:Struct" soapenc:position="[0]">
		        	<name xsi:type="xsd:string">Access-Control-Allow-Origin</name>
		        	<value xsi:type="xsd:string">*</value>
		    	</item>
		    	<item xsi:type="soapenc:Struct" soapenc:position="[1]">
			        <name xsi:type="xsd:string">Access-Control-Allow-Methods</name>
			        <value xsi:type="xsd:string">POST, GET, OPTIONS</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[2]">
			        <name xsi:type="xsd:string">Access-Control-Allow-Headers</name>
			        <value xsi:type="xsd:string">x-requested-with, soapaction</value>
		    	</item>
		   </httpHeaderFields>
    	</properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>

	<xp:Application class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local" service="ptfifo"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>

	<!-- XRelay -->
	<xp:Application class="xrelay::Application" id="4"  service="xrelay" group="profile" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libxrelay.so</xp:Module>

	<!-- HyperDAQ -->
	<xp:Application class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>

	<!-- TStore -->
	<xp:Application heartbeat="true" class="tstore::TStore" id="11" publish="true" group="xmas,database1" service="tstore" network="local" logpolicy="inherit">
		<properties xmlns="urn:xdaq-application:tstore::TStore" xsi:type="soapenc:Struct">
			  <configurationRootDirectory xsi:type="xsd:string">{$TSTORE}/hellotstore/configs/store</configurationRootDirectory>
		</properties>
    </xp:Application>
    <xp:Module>${XDAQ_ROOT}/lib/libxalanMsg.so</xp:Module>
    <xp:Module>${XDAQ_ROOT}/lib/libxalan-c.so</xp:Module>
    <xp:Module>${XDAQ_ROOT}/lib/libxoapfilter.so</xp:Module>
    <xp:Module>${XDAQ_ROOT}/lib/libtstoreutils.so</xp:Module>
    <xp:Module>${XDAQ_ROOT}/lib/libtstoreclient.so</xp:Module>
    <xp:Module>${XDAQ_ROOT}/lib/libtstore.so</xp:Module>

</xp:Profile>
```

The `HelloTStore` class is started as an application called `hellotstore` using the following config.
```xml
<xc:Partition  xsi:schemaLocation="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<xc:Context url="http://localhost:10000">
        <xc:Application class="tstore::HelloTStore" id="121" instance="1" network="local" service="hellotstore">
                <properties xmlns="urn:xdaq-application:HelloTStore" xsi:type="soapenc:Struct"/>
        </xc:Application>
	<xc:Module>${XDAQ_ROOT}/lib/libhellotstore.so</xc:Module>
</xc:Context>
```

## Necessary Changes

### helloworld.view

In the file `configs/store/helloworld.view` fill the `dbname` with the information from the database in the form of `hostname:port/servicename` in the following line.

```xml 
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<tstore:configuration xmlns:tstore="urn:xdaq-tstore:1.0">
  <tstore:view id="urn:tstore-view-SQL:HelloTStore">
    <tstore:connection dbname="hostname:port/servicename" xmlns:tstore="urn:xdaq-tstore:1.0"/>
    <sql:query xmlns:sql="urn:tstore-view-SQL" name="hello">
	    <![CDATA[select 'hello world' from dual]]>
    </sql:query>
  </tstore:view>
</tstore:configuration>
```

### HelloTStore.cc
In the main source file, fill in the username and password for the database. This information is needed in the `connect` method to set up the initial connection.

```c++
std::string tstore::HelloTStore::connect() {
        Request request("connect");

        //add the view ID
        request.addTStoreParameter("id","urn:tstore-view-SQL:HelloTStore");

        //this parameter is mandatory. "basic" is the only value allowed at the moment
        request.addTStoreParameter("authentication","basic");

        //login credentials in format username/password
        request.addTStoreParameter("credentials","user/pw");

        //connection will time out after 10 minutes
        toolbox::TimeInterval timeout(600,0);
        request.addTStoreParameter("timeout",timeout.toString("xs:duration"));

        xoap::MessageReference message=request.toSOAP();
        xoap::MessageReference response=sendSOAPMessage(message);

        //use the TStore client library to extract the response from the reply
        return tstoreclient::connectionID(response);
}
```

## Running HelloTStore

To run HelloTStore, change the DB specific information above.
For a fresh intallation compile the client using make. If this library should be used for the application, the following line in the file `xml/hellotstore.xml` must be changed to the path of the compiled library seen below.

```bash
<xc:Module>${XDAQ_ROOT}/lib/libhellotstore.so</xc:Module>
```

The package uses the same Makefile setup as the TStore client package. It loads the path to the TStore service and utils package through the worksuite definitions and links it. Then it builds a dynamic library `libhellotstore.so` and stores in `$TSTORE/hellotstore/lib/$XDAQ_OS/$XDAQ_ARCH/`. Here, `$TSTORE` is the path to the `tstore` package, `XDAQ_OS` defines the OS where the package is compiled, `linux` or  `mac`, and `XDAQ_ARCH` the architecture, for example `x86_64_alma9`.
When the building was successful, the project can be run with the following command.
To use the configs without changings, export the `$TSTORE` with the correct path.

```bash
$XDAQ_ROOT/bin/xdaq.exe -p 10000 -l DEBUG -c $TSTORE/hellotstore/xml/hellotstore.xml -e $TSTORE/hellotstore/xml/tstore.profile
```

By omitting `-l DEBUG` the log level is set to `INFO` and the SOAP messages are not logged.

## Adding more functionality

The [TStore insert programming example](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreInsertProgrammingExample) also uses the `TStoreRequest` class, and can use the same `postSOAP` implementation. All the code from this example can be directly added to HelloTStore to add insert functionality. You will also need to add some way of triggering the new code, for example by binding it to a [web callback](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/WebApplication#Binding_a_callback).

