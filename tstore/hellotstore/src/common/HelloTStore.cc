#include "tstore/hellotstore/HelloTStore.h"
#include "tstore/hellotstore/Request.h"
#include "toolbox/TimeInterval.h"
#include "tstore/client/AttachmentUtils.h"
#include "tstore/client/Client.h"
#include "xcept/tools.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "cgicc/HTMLClasses.h"

XDAQ_INSTANTIATOR_IMPL(hellotstore::HelloTStore)

hellotstore::HelloTStore::HelloTStore(xdaq::ApplicationStub * s) : xdaq::Application(s) {   
	s->getDescriptor()->setAttribute("icon","/tstore/hellotstore/images/hellotstore-icon.png");

	xgi::bind(this, &hellotstore::HelloTStore::Default, "Default");
	xgi::bind(this, &hellotstore::HelloTStore::query, "query");
}

void hellotstore::HelloTStore::Default(xgi::Input * in, xgi::Output * out ) {
	//a link to the call query method below
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	url += "/query";	

	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
   	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("Hello TStore") << std::endl;
    	*out << cgicc::a("Query Hello World!").set("href", url) << std::endl;
}

void hellotstore::HelloTStore::query(xgi::Input * in, xgi::Output * out ) {
	try {
		xdata::Table results;
		
		std::string connectionID=connect();
		query(connectionID, results);
		disconnect(connectionID);
	
		*out << cgicc::p("Result from TStore Query:") << std::endl;	
		
		std::vector<std::string> columns=results.getColumns();
		for (unsigned long rowIndex=0;rowIndex<results.getRowCount();rowIndex++ ) {
			for (std::vector<std::string>::iterator column=columns.begin(); column!=columns.end(); ++column) {
				std::string value=results.getValueAt(rowIndex,*column)->toString();
	    		LOG4CPLUS_INFO(this->getApplicationLogger(),*column+": "+value);
			*out << cgicc::p(*column+": "+value) << std::endl;
	    	}
	    }
	} catch (xcept::Exception &e) {
		LOG4CPLUS_ERROR(this->getApplicationLogger(),xcept::stdformat_exception_history(e));
	}
}

xoap::MessageReference hellotstore::HelloTStore::sendSOAPMessage(xoap::MessageReference &message) {
	xoap::MessageReference reply;
	
	std::string msg;
	message->writeTo(msg);
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Message: " + msg);

	try {
		const xdaq::ApplicationDescriptor * tstoreDescriptor = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("tstore::TStore", 0);
	    	const xdaq::ApplicationDescriptor * tstoretestDescriptor=this->getApplicationDescriptor();
		reply = getApplicationContext()->postSOAP(message,*tstoretestDescriptor, *tstoreDescriptor);
	} 
	catch (xdaq::exception::Exception& e) {
		LOG4CPLUS_ERROR(this->getApplicationLogger(),xcept::stdformat_exception_history(e));
		XCEPT_RETHROW(xcept::Exception, "Could not post SOAP message. ", e);
	}
	
	xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();
	reply->writeTo(msg);
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Response: " + msg);
		
	if (body.hasFault()) {
		XCEPT_RAISE (xcept::Exception, body.getFault().getFaultString());
	}
	return reply;
}

std::string hellotstore::HelloTStore::connect() {
	Request request("connect");
	
	//add the view ID
	request.addTStoreParameter("id","urn:tstore-view-SQL:HelloTStore");
	
	//this parameter is mandatory. "basic" is the only value allowed at the moment
	request.addTStoreParameter("authentication","basic");
	
	//login credentials in format username/password
	request.addTStoreParameter("credentials","user/pw");
	
	//connection will time out after 10 minutes
	toolbox::TimeInterval timeout(600,0); 
	request.addTStoreParameter("timeout",timeout.toString("xs:duration"));
	
	xoap::MessageReference message=request.toSOAP();
	xoap::MessageReference response=sendSOAPMessage(message);
	
	//use the TStore client library to extract the response from the reply
	return tstoreclient::connectionID(response);
}

void hellotstore::HelloTStore::disconnect(const std::string &connectionID) {
	Request request("disconnect");
	
	//add the connection ID
	request.addTStoreParameter("connectionID",connectionID);
	
	xoap::MessageReference message=request.toSOAP();
	sendSOAPMessage(message);
}

void hellotstore::HelloTStore::query(const std::string &connectionID,xdata::Table &results) {
	//for a query, we need to send some parameters which are specific to SQLView.
	//these use the namespace tstore-view-SQL. 
	
	//In general, you might have the view name in a variable, so you won't know the view class. In this
	//case you can find out the view class using the TStore client library:
	std::string viewClass=tstoreclient::classNameForView("urn:tstore-view-SQL:HelloTStore");
	
	//If we give the name of the view class when constructing the Request, 
	//it will automatically use that namespace for
	//any view specific parameters we add.
	Request request("query",viewClass);
	
	//add the connection ID
	request.addTStoreParameter("connectionID",connectionID);
	
	//for an SQLView, the name parameter refers to the name of a query section in the configuration
	//We'll use the "hello" one.
	request.addViewSpecificParameter("name","hello");
	
	xoap::MessageReference message=request.toSOAP();
	xoap::MessageReference response=sendSOAPMessage(message);
	
	//use the TStore client library to extract the first attachment of type "table"
	//from the SOAP response
	if (!tstoreclient::getFirstAttachmentOfType(response,results)) {
		XCEPT_RAISE (xcept::Exception, "Server returned no data");
	}
}
