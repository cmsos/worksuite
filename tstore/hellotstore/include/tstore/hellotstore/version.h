/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _hellotstore_version_h_
#define _hellotstore_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_HELLOTSTORE_VERSION_MAJOR 2
#define WORKSUITE_HELLOTSTORE_VERSION_MINOR 3
#define WORKSUITE_HELLOTSTORE_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_HELLOTSTORE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_HELLOTSTORE_PREVIOUS_VERSIONS "2.1.0,2.1.1,2.1.2,2.2.0"

//
// Template macros
//
#define WORKSUITE_HELLOTSTORE_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_HELLOTSTORE_VERSION_MAJOR,WORKSUITE_HELLOTSTORE_VERSION_MINOR,WORKSUITE_HELLOTSTORE_VERSION_PATCH)
#ifndef WORKSUITE_HELLOTSTORE_PREVIOUS_VERSIONS
#define WORKSUITE_HELLOTSTORE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_HELLOTSTORE_VERSION_MAJOR,WORKSUITE_HELLOTSTORE_VERSION_MINOR,WORKSUITE_HELLOTSTORE_VERSION_PATCH)
#else 
#define WORKSUITE_HELLOTSTORE_FULL_VERSION_LIST  WORKSUITE_HELLOTSTORE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_HELLOTSTORE_VERSION_MAJOR,WORKSUITE_HELLOTSTORE_VERSION_MINOR,WORKSUITE_HELLOTSTORE_VERSION_PATCH)
#endif 

namespace hellotstore
{
	const std::string project = "worksuite";
	const std::string package = "hellotstore";
	const std::string versions = WORKSUITE_HELLOTSTORE_FULL_VERSION_LIST;
	const std::string description = "Hello world example for TStore XDAQ application";
	const std::string summary = "Hello world example for TStore XDAQ application";
	const std::string authors = "Angela Brett, Christine Zeh";
	const std::string link = "https://gitlab.cern.ch/cmsos/worksuite/-/tree/baseline_sulfur_16/tstore/hellotstore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

