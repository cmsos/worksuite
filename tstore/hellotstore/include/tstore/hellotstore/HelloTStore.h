#ifndef _HelloTStore_h_
#define _HelloTStore_h_

#include "xdaq/Application.h"
#include "xdaq/WebApplication.h"
#include "xdata/Table.h"

namespace hellotstore {
	class HelloTStore: public xdaq::Application  
	{
		public:
			XDAQ_INSTANTIATOR();

			HelloTStore(xdaq::ApplicationStub * s);
			void query(xgi::Input * in, xgi::Output * out );
		
		private:
			xoap::MessageReference sendSOAPMessage(xoap::MessageReference &message);
			std::string connect();
			void disconnect(const std::string &connectionID);
			void query(const std::string &connectionID,xdata::Table &results);
			void Default(xgi::Input * in, xgi::Output * out );

	};
}
#endif
