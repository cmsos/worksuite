# TStoreTest

This is a client application to test the functionality of the TStore for client applications. The client application connects to a TStore service running in a different process. It allows to test the creation, altering and deletion of SQLViews and the querying and insertion of NestedViews. To achieve this, two view configurations are provided, or a new one can be created through the web interface.

## Views

### SQL View

A test SQL view is provided, with tables and values built automatically by the randon data class of the TStoreTest. For SQL views, all functionality, so the creation of new views and tables is easily possible.
The provided view includes all needed elements to test query, insert, update and delete requests. To add the defined table to the DB use the sync button with the option `to database`. For this, a connection to the DB must be established.

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<tstore:configuration xmlns:tstore="urn:xdaq-tstore:1.0">

  <tstore:view id="urn:tstore-view-SQL:HelloTStore">
    <tstore:connection dbname="hostname:port/service" xmlns:tstore="urn:xdaq-tstore:1.0"/>
    <sql:query xmlns:sql="urn:tstore-view-SQL" name="sql_table">
      <sql:table name="sql_table"/>
    </sql:query>
    <sql:insert xmlns:sql="urn:tstore-view-SQL" name="sql_table">
      <sql:table name="sql_table"/>
    </sql:insert>
    <sql:update xmlns:sql="urn:tstore-view-SQL" name="sql_table">
      <sql:table name="sql_table"/>
    </sql:update>
    <sql:delete xmlns:sql="urn:tstore-view-SQL" name="sql_table">
      <sql:table name="sql_table"/>
    </sql:delete>
    <tstore:table key="column3primary" name="sql_table">
      <tstore:column name="column0" type="double"/>
      <tstore:column name="column1" type="time"/>
      <tstore:column name="column2" type="unsigned int 32"/>
      <tstore:column name="column3primary" type="unsigned int 64"/>
      <tstore:column name="column4" type="bool"/>
      <tstore:column name="column5" type="string"/>
    </tstore:table>
  </tstore:view>

</tstore:configuration>

```

### Nested View

For an example of nested views a [nursery rhyme](https://en.wikipedia.org/wiki/As_I_was_going_to_St_Ives)  is used. A more indepth instruction can be found on the xwiki about [nested tables](https://twiki.cern.ch/twiki/bin/view/XdaqWiki/NestedView). To run this example, the tables have to be prepared in the database, the view should connect to. The following command can be used.

```sql
create table nested_wife (
  name varchar2(30) primary key,
  age integer
);

create table nested_sack (
  id integer primary key,
  volume binary_double,
  owner varchar2(30) references nested_wife (name)
);

create table nested_cat (
  name varchar2(30) primary key,
  breed varchar2(30),
  sack integer references nested_sack (id)
);

insert into nested_wife values ('Jane',21);
insert into nested_sack values (1,2.3,'Jane');
insert into nested_cat values ('Tiger','tabby',1);
insert into nested_cat values ('Fluffy','Persian',1);
insert into nested_wife values ('Joan',42);
insert into nested_sack values (2,1.6,'Joan');
insert into nested_cat values ('Smokey','tiger',2);
insert into nested_sack values (3,2.8,'Joan');
insert into nested_cat values ('Mittens','Siamese',3);
```
Nested views allow the querying and inserting into tables directly. Therefore, the view configuration only includes the connection tag and the table tag with the main table, the other sub tables are related to by foreign key. The depth attribute, defines the maximum number of subtables related to, in a single chain. The chain being `wife -> sack -> cat`.
```xml
<tstore:configuration xmlns:tstore="urn:xdaq-tstore:1.0" xmlns:nested="urn:tstore-view-Nested">
  <tstore:view id="urn:tstore-view-Nested:Cats">
    <tstore:connection dbname="hostname:port/service" xmlns:tstore="urn:xdaq-tstore:1.0"/>
		<nested:table name="nested_wife" depth="2"/>
  </tstore:view>
</tstore:configuration>
```
To use the NestedView, the Checkbox for it must be activated.

## Configuration

The TStore service is started using the default profile, extended by the following application information.

```xml
<xp:Application heartbeat="true" class="tstore::TStore" id="11" instance="0" publish="true" group="database" service="tstore" network="local" logpolicy="inherit">
	<properties xmlns="urn:xdaq-application:tstore::TStore" xsi:type="soapenc:Struct">
		<configurationRootDirectory xsi:type="xsd:string">${TSTORE_STORE}</configurationRootDirectory>
	</properties>
</xp:Application>
<xp:Module>${XDAQ_ROOT}/lib/libxalanMsg.so</xp:Module>
<xp:Module>${XDAQ_ROOT}/lib/libxalan-c.so</xp:Module>
<xp:Module>${XDAQ_ROOT}/lib/libxoapfilter.so</xp:Module>
<xp:Module>${XDAQ_ROOT}/lib/libtstoreutils.so</xp:Module>
<xp:Module>${XDAQ_ROOT}/lib/libtstoreclient.so</xp:Module>
<xp:Module>${XDAQ_ROOT}/lib/libtstore.so</xp:Module>
```
Here the environment variable `TSTORE` defines the path to the TStore root directory and `TSTORE_STORE` to the store directory of the TStoreTest.
For the test application the `tstoretest.xml` configuration sets up the client and defines the address of the TStore service. It is important to change the port for the `tstoreURL` if a different port is used at the start up of the TStore service.
```xml
<xc:Partition  xsi:schemaLocation="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xc:Context url="http://localhost:10000">
		<xc:Application class="tstoretest::TStoreTest" id="121" instance="1" network="local" service="tstoretest">
			<properties xmlns="urn:xdaq-application:tstoretest::TStoreTest" xsi:type="soapenc:Struct">
				<tstoreURL xsi:type="xsd:string">http://localhost:10001</tstoreURL>
				<tstoreLid xsi:type="xsd:unsignedInt">11</tstoreLid>
				<tStoreRetries xsi:type="xsd:unsignedInt">10</tStoreRetries>
				<tStoreRetrySleepSeconds xsi:type="xsd:unsignedInt">60</tStoreRetrySleepSeconds>
			</properties>
		</xc:Application>
		<xc:Module>${XDAQ_ROOT}/lib/libtstoreutils.so</xc:Module>
		<xc:Module>${XDAQ_ROOT}/lib/libtstoreclient.so</xc:Module>
		<xc:Module>${XDAQ_ROOT}/lib/libtstoretest.so</xc:Module>
	</xc:Context>
</xc:Partition>
```

## Running TStoreTest

To run TStoreTest, change the DB specific information above. In the file `store/cats.view` fill the `dbname` with the information from the database in the form of `hostname:port/servicename` in line 3.
```xml
 <tstore:connection dbname="hostname:port/surface" xmlns:tstore="urn:xdaq-tstore:1.0"/>
 ```

For a fresh intallation compile the client using `make`. If this library should be used for the application, the following line in the file `xml/tstoretest.xml` must be changed to the path of the compiled library seen below.

```xml
 <xc:Module>${XDAQ_ROOT}/lib/libtstoretest.so</xc:Module>
 ```

The package uses the same Makefile setup as the TStore client package. It loads the path to the TStore service and utils package through the worksuite definitions and links it. Then it builds a dynamic library libtstoretest.so and stores in `$TSTORE/tstoretest/lib/$XDAQ_OS/$XDAQ_ARCH/`. Here, `$TSTORE` is the path to the tstore package, `XDAQ_OS` defines the OS where the package is compiled, linux or  mac, and `XDAQ_ARCH` the architecture, for example x86_64_alma9.
When the building was successful, the project can be run with the following commands.
To use the configs without changings, export the `$TSTORE_XML` with the correct path to the xml folder of TStoreTest.


Run the following command to start up the TStore service.
```bash
$XDAQ_ROOT/bin/xdaq.exe -p 10001 -l DEBUG -e $TSTORE_XML/tstoretest.profile
```

In another terminal run the next command to start up the test client.
```bash
$XDAQ_ROOT/bin/xdaq.exe -p 10000 -l DEBUG -c $TSTORE_XML/tstoretest.xml
```

## Usage
![Web Interface to test standard and admin functionality](images/web_interface.png)

To start running the tests, a list of all views have to be requested, by pressing the action button `Get Views`. Afterwards, the **admin functionality** can be run, by selecting the appropriate view from the dropdown. For the admin functionality to connection to the DB is needed (except sync). This is, because only the config file is requested and changed. By pressing the sync button, the changes made in the configuration are ported to the DB. 

Options:
 - **Get Views:** Requests a list of all `.view` files in the store folder and adds them to the dropdown inputs.
 - **Add View:** Stores a new `.view` file in the store folder with a initial config. Input the id of the view in `View ID` including the type of view, e.g. `urn:tstore-view-Nested:Cats`, `urn:tstore-view-SQL:Shop`. Set a filename for the store folder, without the path or extension and the path (host:port/service) of the DB. Defining the initial config an arbitrary parent element containing the needed namespaces must be presented, e.g. `</tstore:view>`, with the wanted tables and commands as child elements.
 - **Remove View:** Deletes config file from store folder.
 - **Get Config:** Requests the config of selected view and displays it in textarea below.
 - **Set Config:** Swaps the config of the view selected in dropdown with the config of the textarea above.
 - **Add Table:** If a SQL view is selected, a table cosisting of random column definition is added to the textarea above. At this stage the Table is not saved to the table yet. For this, the `Set Config` must be selected.
 - **Sync:** If a connection to a view is currently open, the config and DB values can be compared and updated using `Sync`. By selecting the mode, it can be managed, if the config, the DB or both can be changed. A pattern is used to bind tables of a similar group. This pattern must be added, to know which tables to sync. (It is autofilled it a table is added using the standard interface.)

After connecting to a specific view the standard functionality of client-service communication can be tested. To connect, enter the username and password of the DB and a timeout value in seconds (e.g. 600). If the checkbox `Test` is checked, the request will not send to the DB, but regarded "as if" they would have been send to it.
 
Options:
 - **Disconnect:** Closes the connection to the DB.
 - **Renew:** Reconnect a disconnected old connection, by resending a connection ID. Allows to resume after a long request which resulted in a crash of connection.
 - **Add Parameter:** To test the correct appending of parameters to request, they can be filled. A parameter can be removed, by setting a blank value. For example, set the name of the query via a parameter and leave the textfield next to the query blank.
 - **Set Special Behaviour:** Define if special values are to be inserted into tables, `NULL` or `Infinity` or `Long String` to input up to 100 characters. An `end` is appended to the string, to check for truncation. If a nested view is connected `Nested View` must be checked, to switch behaviour. To just check the SOAP messages for correctnes, `TEST` can be checked. This prevents an update of the DB.
 - **Query:** Request a query of the view. For the SQL view, the tag `<sql:query name="test-query"/>` with instructions must be present in the config. As a child element, it must either have a table with its name, `<sql:table name="test_table0"/>`, or a valid SQL call, `<![CDATA[select 'hello world' from dual]]>`. By entering the name of the query in to the textfield, the query can then be requested. For nested views this is not necessary, a query of the table can be requested directly, by entering the table name.
 - **Update:** Update the view with random data. This call is only possible for SQL views. Again, a tag with `<sql:update name="test-update"/>` must be presented in the config with a table as child element.
 - **Insert:** Insert random data into the view. Again, a tag with `<sql:insert name="test-insert"/>` must be presented in the config with a table as child element. For nested views just input the table name.
 - **Definition:** Get the table definition for a table defined in the view config. To be able to request this information, an insert tag must be present with this table as child element. For nested views just input the table name.
 - **Delete:** Delete random three rows of the table. It's only possible if one or multiple elements `<sql:delete name="test-delete"/>` exist in the config. The rows are deleted from the table defined as child element in the delete element. Delete is not implemented for Nested views.
 - **Clear:** Empties the table defined in the delete element. Clear is not implemented for Nested views.
 - **Add Table:** For a SQL view three random tables and definitions are generated named `prefix_table-x, x=1...3` with the prefix provided in the textfield. If `Test` is not checked, they are saved in the config and DB.
 - **Remove Table:** The tbaled with the provided prefix are deleted from the configuration and DB, if `Test` is not checked.
 - **Destroy:** Purges table from DB and configuration.

For each SOAP request to the TStore service, the sent and received message is provided at the text area at the bottom. Sometime it is necessary to reload the website, to be able to see the most recent SOAP messages.

![Query result of SQL View and information about sent and received SOAP message.](images/sql_view_query.png)

![Query result of Nested View and information about sent and received SOAP message.](images/nested_view_query.png)
