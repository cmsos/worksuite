// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstoretest_version_h_
#define _tstoretest_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_TSTORETEST_VERSION_MAJOR 1
#define WORKSUITE_TSTORETEST_VERSION_MINOR 16
#define WORKSUITE_TSTORETEST_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_TSTORETEST_PREVIOUS_VERSIONS "3.8.0,3.8.1"

#define WORKSUITE_TSTORETEST_PREVIOUS_VERSIONS ""

//
// Template macros
//
#define WORKSUITE_TSTORETEST_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TSTORETEST_VERSION_MAJOR,WORKSUITE_TSTORETEST_VERSION_MINOR,WORKSUITE_TSTORETEST_VERSION_PATCH)
#ifndef WORKSUITE_TSTORETEST_PREVIOUS_VERSIONS
#define WORKSUITE_TSTORETEST_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TSTORETEST_VERSION_MAJOR,WORKSUITE_TSTORETEST_VERSION_MINOR,WORKSUITE_TSTORETEST_VERSION_PATCH)
#else 
#define WORKSUITE_TSTORETEST_FULL_VERSION_LIST  WORKSUITE_TSTORETEST_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TSTORETEST_VERSION_MAJOR,WORKSUITE_TSTORETEST_VERSION_MINOR,WORKSUITE_TSTORETEST_VERSION_PATCH)
#endif 

namespace tstoretest
{
	const std::string project = "worksuite";
	const std::string package  =  "tstoretest";
	const std::string versions = WORKSUITE_TSTORETEST_FULL_VERSION_LIST;
	const std::string description = "Table store test application";
	const std::string summary = "Table store test application";
	const std::string authors = "Angela Brett & Christine Zeh";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/TStore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
