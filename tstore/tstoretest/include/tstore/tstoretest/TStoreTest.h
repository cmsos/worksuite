// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _TStoreTest_h_
#define _TStoreTest_h_

#include "xdaq/Application.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdaq/WebApplication.h"
#include "toolbox/Properties.h"
#include "xdata/Table.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPEnvelope.h"
#include "xdata/TimeVal.h"
#include "xdata/Mime.h"
#include "xdata/Vector.h"
#include "tstore/tstoretest/RandomData.h"
#include "tstore/tstoretest/Configuration.h"

namespace tstoretest {

class TStoreTest: public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener {
public:

	XDAQ_INSTANTIATOR();

	TStoreTest(xdaq::ApplicationStub *s);
	void Default(xgi::Input *in, xgi::Output *out);
	void configureRandomData();
	void actionPerformed(toolbox::Event &event);

	//test standard interface
	void connect(std::string viewName, std::string password, std::string username, std::string timeout);
	void disconnect(std::string connectionID);
	void testQuery(xgi::Input *in, xgi::Output *out);
	void testDefinition(xgi::Input *in, xgi::Output *out);
	void testUpdate(xgi::Input *in, xgi::Output *out);
	void testInsert(xgi::Input *in, xgi::Output *out);
	void testConnect(xgi::Input *in, xgi::Output *out);
	void testRenew(xgi::Input *in, xgi::Output *out);
	void testDisconnect(xgi::Input *in, xgi::Output *out);
	void testClear(xgi::Input *in, xgi::Output *out);
	void testDelete(xgi::Input *in, xgi::Output *out);
	void testAddTable(xgi::Input *in, xgi::Output *out);
	void testAddTableToConfig(xgi::Input *in, xgi::Output *out);
	void testRemoveTable(xgi::Input *in, xgi::Output *out);
	void testDestroy(xgi::Input *in, xgi::Output *out);

	//test administrative interface
	void testGetViews(xgi::Input *in, xgi::Output *out);
	void testAddView(xgi::Input *in, xgi::Output *out);
	void testRemoveView(xgi::Input *in, xgi::Output *out);
	void testGetConfig(xgi::Input *in, xgi::Output *out);
	void testSetConfig(xgi::Input *in, xgi::Output *out);
	void testSync(xgi::Input *in, xgi::Output *out);

	//other interface
	void addParameter(xgi::Input *in, xgi::Output *out);
	void setParameters(xgi::Input *in, xgi::Output *out);

private:
	//web interface
	void outputCheckbox(xgi::Output *out, bool on, const std::string &name);
	void outputException(xgi::Output *out, xcept::Exception &e);
	void outputTable(xgi::Output *out, xdata::Table &results);
	void outputParameters(xgi::Output *out);
	void outputOption(xgi::Output *out, const std::string &name);
	void outputViewSelector(xgi::Output *out);
	void outputConnectionSelector(xgi::Output *out);
	void outputAdministrativeInterface(xgi::Output *out);
	void outputStandardInterface(xgi::Output *out);
	void outputControls(xgi::Input *in, xgi::Output *out);

	void setBoolean(xgi::Input *in, bool &on, const std::string &name);
	std::string getParameter(cgicc::Cgicc &cgi, const std::string &name);

	//constructing message
	std::string connection(xgi::Input *in);
	std::string connection(cgicc::Cgicc &cgi);
	std::string classNameForConnection(const std::string &view);
	void addParameter(xoap::SOAPEnvelope &envelope, xoap::SOAPElement &element, const std::string &parameterName, const std::string &parameterValue,
			const std::string &viewClass);
	void addParametersToMessage(xoap::SOAPEnvelope &envelope, xoap::SOAPElement &element, const std::string &viewClass);
	xoap::SOAPElement addCommand(xoap::SOAPEnvelope &envelope, const std::string &command, const std::string &connectionID);
	xoap::SOAPElement addAdministrativeCommand(xoap::SOAPEnvelope &envelope, const std::string &command, const std::string &attributeName = "",
			const std::string &value = "");
	static std::string keyString(const std::vector<std::string> &keyColumns);
	static void addTableTag(xoap::SOAPEnvelope &envelope, xoap::SOAPElement &command, const std::string &tableName, const std::vector<std::string> &tableNames =
			std::vector<std::string>());
	static DOMDocument* parseString(const std::string &config);
	static void copyNamespaceDeclarations(xoap::SOAPElement &destination, const DOMNode *source);
	void addConfigurationToMessage(xoap::SOAPElement &commandElement, const std::string &configuration);

	//sending message
	xoap::MessageReference sendSyncMessage(const std::string &connectionID, const std::string &pattern, const std::string &mode);
	xoap::MessageReference sendCommandToView(const std::string &viewName, const std::string &command);
	xoap::MessageReference postSOAPMessage(xoap::MessageReference msg);
	xoap::SOAPElement addConfigCommand(xoap::MessageReference msg, cgicc::Cgicc &cgi, const std::string &command, bool newView=false);
	void testTableRequest(xgi::Input *in, xgi::Output *out, const std::string &command);
	void runTableRequest(const std::string &viewName, const std::string &command, xdata::Table &results);
	void runTableSend(const std::string &connectionID, const std::string &command, xdata::Table &results);

	//processing reply
	void getNodesNamed(std::vector<DOMNode*> &nodes, xoap::MessageReference msg, const std::string &nodeName);
	DOMNode* getNodeNamed(xoap::MessageReference msg, const std::string &nodeName);
	bool responseHasSyncActions(xoap::MessageReference response);

	//manipulating tables

	void changeCaseOfColumnNames(xdata::Table &table);
	std::string writeXML(DOMNode *node);

	tstoretest::Configuration configuration_;
	xdaq::ApplicationDescriptor *tstoreDescriptor_;

	std::map<const std::string, std::string> queryParameters_;
	std::string view_;
	std::string newview_;
	std::string connectionID_;
	std::map<std::string, std::string> connectionIDs_;
	std::vector<std::string> views_;
	std::string lastMessage_; //the latest SOAP message
	std::string lastResponse_; //the response of the latest SOAP message
	std::string config_; //the results of the latest getConfig message
	std::string initialViewConfig_; //initial config for the last view added
	std::string dbname_; //database name for the view to be added
	std::string username_; //username for the view to be added
	std::string path_;
	std::string prefix_;
	std::string name_;
	std::string pattern_;
	std::string viewPath_; //path to add a new view to
	bool insertNulls_;
	bool insertInfinities_;
	bool longStrings_;
	bool dryRun_ = false;
	bool subtables_ = false;
	RandomData randomData_;
};
}

#endif
