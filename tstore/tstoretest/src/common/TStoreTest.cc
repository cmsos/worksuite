// $Id$
// A test application which communicates with the TStore application via SOAP.

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/tstoretest/TStoreTest.h"
#include "tstore/tstoretest/Configuration.h"

#include <cgicc/FormEntry.h>
#include <cgicc/Cgicc.h>
#include <sstream>

#include "cgicc/HTMLClasses.h"
#include "xcept/Exception.h"

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationDescriptorFactory.h"
#include "xdaq/NamespaceURI.h"

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/TableIterator.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/DOMParserFactory.h"

#include "xercesc/dom/DOMNode.hpp"

#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "xgi/framework/Method.h"
#include "xercesc/dom/DOMNode.hpp"

#include "xcept/tools.h"

#include "tstore/client/AttachmentUtils.h"
#include "tstore/client/LoadDOM.h"
#include "tstore/client/Client.h"

//
// provides factory method for instantion of TStoreTest application
//
XDAQ_INSTANTIATOR_IMPL(tstoretest::TStoreTest)

tstoretest::TStoreTest::TStoreTest(xdaq::ApplicationStub *s) :
		xdaq::Application(s), xgi::framework::UIManager(this), configuration_(s->getInfoSpace()) {

	getApplicationContext()->addActionListener(this);
	s->getDescriptor()->setAttribute("icon","/tstore/tstoretest/images/tstoretest-icon.png");

	xgi::framework::deferredbind(this, this, &TStoreTest::Default, "Default");

	//standard interface
	xgi::framework::deferredbind(this, this, &TStoreTest::testQuery, "queryTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testUpdate, "updateTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testDefinition, "definitionTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testInsert, "insertTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testConnect, "connectTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testRenew, "renewTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testDisconnect, "disconnectTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testDelete, "deleteTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testClear, "clearTest");

	//administrative interface
	xgi::framework::deferredbind(this, this, &TStoreTest::testAddTable, "addTableTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testAddTableToConfig, "addTableToConfigTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testRemoveTable, "removeTableTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testDestroy, "destroyTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testGetConfig, "getConfigTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testSetConfig, "setConfigTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testSync, "syncTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testGetViews, "getViewsTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testAddView, "addViewTest");
	xgi::framework::deferredbind(this, this, &TStoreTest::testRemoveView, "removeViewTest");

	//other actions
	xgi::framework::deferredbind(this, this, &TStoreTest::addParameter, "addParameter");
	xgi::framework::deferredbind(this, this, &TStoreTest::setParameters, "setParameters");

	configureRandomData();
}

void tstoretest::TStoreTest::configureRandomData() {
	randomData_.createNulls = insertNulls_;
	randomData_.createInfinities = insertInfinities_;
	randomData_.createSubtables = subtables_;
	randomData_.maxStringLength = longStrings_ ? 100 : 20;
}

void tstoretest::TStoreTest::actionPerformed(toolbox::Event &event) {
	if (event.type() == "urn:xdaq-event:configuration-loaded") {
		LOG4CPLUS_DEBUG(getApplicationLogger(), "configuration-loaded");
		//Create a tstore descriptor

		auto tstoreContext = new xdaq::ContextDescriptor(configuration_.tstoreURL());
		LOG4CPLUS_DEBUG(getApplicationLogger(),
				"Add Application Description for context " + configuration_.tstoreURL() + " and Lid " + std::to_string(configuration_.tstoreLid()));

		std::set<std::string> zones;
		zones.insert(getApplicationContext()->getDefaultZoneName());

		std::set<std::string> groups;
		xdaq::ApplicationDescriptorFactory *applicationDescriptorFactory = xdaq::ApplicationDescriptorFactory::getInstance();

		try {
			tstoreDescriptor_ = applicationDescriptorFactory->createApplicationDescriptor(tstoreContext, "tstore::TStore", configuration_.tstoreLid(), zones, groups);

		} catch (xdaq::exception::DuplicateApplicationDescriptor &e) {
			XCEPT_RETHROW(xcept::Exception, "Application descriptor for 'tstore' already exists", e);
		} catch (xdaq::exception::InvalidZone &e) {
			XCEPT_RETHROW(xcept::Exception, "Invalid zone for application descriptor for 'tstore.", e);
		}
	}
}

///////////////////////Functions for dealing with the web interface (these probably are not of interest to anyone writing their own application)

//looks at the GET parameter named \a name and sets \a on to true if the parameter is "on", false otherwise
void tstoretest::TStoreTest::setBoolean(xgi::Input *in, bool &on, const std::string &name) {
	cgicc::Cgicc cgi(in);
	std::string value = getParameter(cgi, name);
	if (value == "on")
		on = true;
	else
		on = false;
}

void tstoretest::TStoreTest::setParameters(xgi::Input *in, xgi::Output *out) {
	setBoolean(in, insertNulls_, "NULL");
	setBoolean(in, insertInfinities_, "Infinities");
	setBoolean(in, dryRun_, "Test");
	setBoolean(in, subtables_, "NestedView");
	setBoolean(in, longStrings_, "Long Strings");
	configureRandomData();
	outputControls(in, out);
}

//this should of course be in some other, non-TStore-related class...
void tstoretest::TStoreTest::outputTable(xgi::Output *out, xdata::Table &results) {
	//this uses raw HTML tags because cgicc can't handle any nested tags, which makes it pretty much useless
	std::vector<std::string> columns = results.getColumns();
	std::vector<std::string>::iterator columnIterator;
	*out << results.getRowCount() << " rows";
	*out << "<table border=\"2\">";
	*out << "<tr>";
	for (columnIterator = columns.begin(); columnIterator != columns.end(); ++columnIterator) {
		*out << "<td>" << *columnIterator << " (" << results.getColumnType(*columnIterator) << ")" << "</td>";
	}
	*out << "</tr>";
	unsigned long rowIndex;
	for (rowIndex = 0; rowIndex < results.getRowCount(); rowIndex++) {
		*out << "<tr>";
		for (columnIterator = columns.begin(); columnIterator != columns.end(); columnIterator++) {
			*out << "<td>";
			if (results.getColumnType(*columnIterator) == "table") {
				outputTable(out, *static_cast<xdata::Table*>(results.getValueAt(rowIndex, *columnIterator)));
			} else if (results.getColumnType(*columnIterator) == "mime") {
				*out << "mime";
			} else {
				*out << results.getValueAt(rowIndex, *columnIterator)->toString();
			}
			*out << "</td>";

		}
		*out << "</tr>";
	}
	*out << "</table>";
}

//adds or updates a parameter value in the list of parameters which will be added to each SOAP message
//deletes the parameter if the value supplied is empty
void tstoretest::TStoreTest::addParameter(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	std::string parameterName = getParameter(cgi, "name");
	std::string parameterValue = getParameter(cgi, "value");
	if (parameterValue == "")
		queryParameters_.erase(parameterName);
	else
		queryParameters_[parameterName] = parameterValue;
	Default(in, out);
}

void tstoretest::TStoreTest::outputException(xgi::Output *out, xcept::Exception &e) {
	std::cout << e.message() << std::endl;
	*out << e.message();
}

/*I'm sure that just **cgi[name] used to work, but now trying to access a value that wasn't set corrupts some memory
 so here is a function to do it.*/
std::string tstoretest::TStoreTest::getParameter(cgicc::Cgicc &cgi, const std::string &name) {
	std::vector<cgicc::FormEntry> result;
	if (cgi.getElement(name, result)) {
		return result[0].getValue();
	} else {
		std::cout << "no " << name << " specified" << std::endl;
		return "";
	}
}

//use this one if you need the cgi for something else as well, since it will crash if you try to create two Cgiccs from the same Input
std::string tstoretest::TStoreTest::connection(cgicc::Cgicc &cgi) {
	return getParameter(cgi, "connection");
}

std::string tstoretest::TStoreTest::connection(xgi::Input *in) {
	cgicc::Cgicc cgi(in);
	return connection(cgi);
}

//sends a SOAP message with the command \a command (e.g. query or definition) the connection ID from the GET parameter, and all the parameters previously added on the web interface
//extracts a table from the SOAP response
//displays all the normal web interface plus the result table, or details of any exception that occurs.
void tstoretest::TStoreTest::testTableRequest(xgi::Input *in, xgi::Output *out, const std::string &command) {
	outputControls(in, out);
	std::string connectionID = connection(in);
	if (!connectionID.empty()) {
		xdata::Table results;
		try {
			cgicc::Cgicc cgi(in);
			name_ = getParameter(cgi, "name");

			runTableRequest(connectionID, command, results);
			*out << "results of " << command << cgicc::br();
			outputTable(out, results);
		} catch (xcept::Exception &e) {
			outputException(out, e);
		}
	}
}

void tstoretest::TStoreTest::outputParameters(xgi::Output *out) {
	std::map<const std::string, std::string>::iterator parameterIterator;
	*out << cgicc::table();
	for (parameterIterator = queryParameters_.begin(); parameterIterator != queryParameters_.end(); parameterIterator++) {
		*out << cgicc::tr();
		*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/addParameter", getApplicationDescriptor()->getURN().c_str()))
				<< std::endl;
		*out << cgicc::td();
		*out << cgicc::input().set("type", "hidden").set("name", "name").set("value", (*parameterIterator).first);
		*out << (*parameterIterator).first << ":";
		*out << cgicc::td() << cgicc::td();
		*out << cgicc::input().set("type", "text").set("name", "value").set("value", (*parameterIterator).second) << std::endl;
		*out << cgicc::td() << cgicc::td();
		*out << cgicc::input().set("type", "submit").set("value", "Change");
		*out << cgicc::td() << cgicc::form() << cgicc::tr();
	}
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/addParameter", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << cgicc::tr().set("height", "7") << cgicc::td() << cgicc::td() << cgicc::tr();
	*out << cgicc::tr() << cgicc::td() << "Parameter name: " << cgicc::td();
	*out << cgicc::td() << cgicc::input().set("type", "text").set("name", "name") << cgicc::td();
	//blank third column for this line as there is no button
	*out << cgicc::td() << cgicc::td();
	*out << cgicc::td() << cgicc::td();
	*out << cgicc::tr() << cgicc::tr();
	*out << cgicc::td() << "Value: " << cgicc::td() << cgicc::td() << cgicc::input().set("type", "text").set("name", "value") << cgicc::td();
	*out << cgicc::td() << cgicc::input().set("type", "submit").set("value", "Add Parameter") << cgicc::td();
	*out << cgicc::tr() << cgicc::table() << cgicc::form() << std::endl;
}

void tstoretest::TStoreTest::outputCheckbox(xgi::Output *out, bool on, const std::string &name) {
	if (on) {
		*out << cgicc::input().set("type", "checkbox").set("name", name).set("checked", "checked");
	} else {
		*out << cgicc::input().set("name", name).set("type", "checkbox");
	}
	*out << " " << name << " ";
}

//outputs an option for a select element, with both the value and the displayed name as \a name
void tstoretest::TStoreTest::outputOption(xgi::Output *out, const std::string &name) {
	*out << cgicc::option().set("value", name) << name << cgicc::option() << std::endl;
}

void tstoretest::TStoreTest::outputViewSelector(xgi::Output *out) {
	if (views_.empty()) {
		*out << cgicc::input().set("type", "text").set("name", "view").set("value", view_) << std::endl;
	} else {
		*out << cgicc::select().set("name", "view");
		for (std::vector<std::string>::iterator view = views_.begin(); view != views_.end(); ++view) {
			outputOption(out, *view);
		}
		*out << cgicc::select();
	}
}

void tstoretest::TStoreTest::outputConnectionSelector(xgi::Output *out) {
	*out << cgicc::select().set("name", "connection"); //<< cgicc::input().set("type","text").set("name","view").set("value",view) << std::endl;
	for (std::map<std::string, std::string>::reverse_iterator connection = connectionIDs_.rbegin(); connection != connectionIDs_.rend(); ++connection) {
		*out << cgicc::option().set("value", (*connection).first) << (*connection).first << " (" << (*connection).second << ")" << cgicc::option() << std::endl;
	}
	*out << cgicc::select();
}

void tstoretest::TStoreTest::outputStandardInterface(xgi::Output *out) {
	//test connect
	*out << cgicc::h2("Manage Connection to View");
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/connectTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << cgicc::br() << "Connect to view: ";
	outputViewSelector(out);
	*out << cgicc::br() << "Username: " << cgicc::input().set("type", "text").set("name", "username") << std::endl;
	*out << cgicc::br() << "Password: " << cgicc::input().set("type", "password").set("name", "password") << std::endl;
	*out << cgicc::br() << "Timeout: " << cgicc::input().set("type", "text").set("name", "timeout") << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Connect") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br()
			<< cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/disconnectTest", getApplicationDescriptor()->getURN().c_str()))
			<< std::endl;
	*out << "Disconnect: ";
	outputConnectionSelector(out);
	*out << cgicc::input().set("type", "submit").set("value", "Run") << std::endl;
	*out << cgicc::form() << std::endl;

	//renew connection
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/renewTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << cgicc::br() << "Renew: ";
	outputConnectionSelector(out);
	*out << cgicc::br() << "Timeout: " << cgicc::input().set("type", "text").set("name", "timeout") << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Renew") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::h2("Set Parameter");
	outputParameters(out);

	*out << cgicc::h2("Select Special Behaviour");
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/setParameters", getApplicationDescriptor()->getURN().c_str())) << std::endl;

	*out << cgicc::h3("Test Run");
	outputCheckbox(out, dryRun_, "Test");
	*out << cgicc::br() << "If checked, the requests will not be forwarded to the Oracle DB." << cgicc::br();

	//form for deciding whether insert can send e.g. NULLs (turn this off if there are columns which can't be NULL)
	*out << cgicc::h3("Insert and Update Operation");

	outputCheckbox(out, insertNulls_, "NULL");
	outputCheckbox(out, insertInfinities_, "Infinities");
	outputCheckbox(out, longStrings_, "Long Strings"); //create longer random strings to test CLOB storage

	*out << cgicc::br() << "Select if NULL and infinity can be set for non primary key columns. Long string allows for up to 100 characters." << cgicc::br();

	*out << cgicc::h3("Table Definition");
	outputCheckbox(out, subtables_, "NestedView"); //sending an empty MIME crashes the serializer, so we can not have subtables of a table with Mime columns
	*out << cgicc::br() << "Creates nested tables, which are built in the DB as multiple tables connected by foreign keys." << cgicc::br();

	*out << cgicc::input().set("type", "submit").set("value", "Set") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::h2("Manage Table Content");

	//the standard interface commands which manage and query table content
	std::string commDML[] = { "query", "definition", "insert" };
	std::string sqlOnly[] = { "update", "delete", "clear" };
	std::vector<std::string> keysDML(commDML, commDML + sizeof(commDML) / sizeof(std::string));
	for (std::vector<std::string>::iterator key = keysDML.begin(); key != keysDML.end(); ++key) {
		*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/%sTest", getApplicationDescriptor()->getURN().c_str(), (*key).c_str()))
				<< std::endl;
		*out << *key << ": ";
		outputConnectionSelector(out);
		if(subtables_)
			*out << " Table: ";
		else
			*out << " Name: ";

		*out << cgicc::input().set("type", "text").set("name", "name").set("value", name_) << std::endl;

		*out << cgicc::input().set("type", "submit").set("value", "Run") << std::endl;
		*out << cgicc::br() << cgicc::form() << std::endl;
	}

	std::vector<std::string> keysOnly(commDML, commDML + sizeof(commDML) / sizeof(std::string));
	for (std::vector<std::string>::iterator key = keysOnly.begin(); key != keysOnly.end(); ++key) {
			*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/%sTest", getApplicationDescriptor()->getURN().c_str(), (*key).c_str()))
					<< std::endl;
			*out << *key << ": ";
			outputConnectionSelector(out);
			*out << " Name: ";

			*out << cgicc::input().set("type", "text").set("name", "name").set("value", name_) << std::endl;
			*out << cgicc::input().set("type", "submit").set("value", "Run").set(subtables_ ? "disabled" : "enabled") << std::endl;
			*out << cgicc::br() << cgicc::form() << std::endl;
		}


	*out << cgicc::h2("Manage Table Definition");
	*out << "Remove table is deprecated, but still working correctly. Use adaption of config and syncing to remove tables." << cgicc::br() << std::endl;
	*out << "addTable and removeTable work on all tables with the prefix." << cgicc::br() << std::endl;
	*out << "destroy removes a single table with the defined name." << std::endl;

	//the standard interface commands which add or delete tables
	std::string commDDL[] = { "addTable", "removeTable" };
	std::vector<std::string> keysDDL(commDDL, commDDL + sizeof(commDDL) / sizeof(std::string));
	for (std::vector<std::string>::iterator key = keysDDL.begin(); key != keysDDL.end(); ++key) {
		*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/%sTest", getApplicationDescriptor()->getURN().c_str(), (*key).c_str()))
				<< std::endl;
		*out << *key << ": ";
		outputConnectionSelector(out);
		*out << "Prefix: " << cgicc::input().set("type", "text").set("name", "prefix").set("value", prefix_) << std::endl;
		*out << cgicc::input().set("type", "submit").set("value", "Run").set(subtables_ ? "disabled" : "enabled") << cgicc::br() << std::endl;
		*out << cgicc::form() << std::endl;
	}

	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/destroyTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << cgicc::br() << "Destroy: ";
	outputConnectionSelector(out);
	*out << "Name: " << cgicc::input().set("type", "text").set("name", "name").set("value", name_) << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Run") << std::endl;
	*out << cgicc::form() << std::endl;
}

void tstoretest::TStoreTest::outputAdministrativeInterface(xgi::Output *out) {
	*out << cgicc::h2("Load Views");
	*out << "Load all views to get started testing TStore." << cgicc::br();
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/getViewsTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << cgicc::br() << "Get Views : " << cgicc::input().set("type", "submit").set("value", "Run") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br() << cgicc::h2("Add View");
	*out << cgicc::form().set("method", "POST").set("action", toolbox::toString("/%s/addViewTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << "View ID: " << cgicc::input().set("type", "text").set("name", "newview").set("value", newview_) << std::endl;
	*out << "Filename (No Extension): " << cgicc::input().set("type", "text").set("name", "configpath").set("value", viewPath_) << std::endl;
	*out << "Database Name: " << cgicc::input().set("type", "text").set("name", "dbname").set("value", dbname_) << std::endl;
	*out << cgicc::br() << "Initial Config: " << std::endl;
	*out << cgicc::textarea().set("name", "config").set("cols", "60").set("rows", "15") << initialViewConfig_ << cgicc::textarea() << std::endl;
	*out << cgicc::br();
	*out << cgicc::input().set("type", "submit").set("value", "Add View") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br();
	*out << cgicc::form().set("method", "POST").set("action", toolbox::toString("/%s/removeViewTest", getApplicationDescriptor()->getURN().c_str()));
	outputViewSelector(out);
	*out << cgicc::input().set("type", "submit").set("value", "Remove view") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br() << cgicc::h2("Edit Config") << "Select View: ";
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/getConfigTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	outputViewSelector(out);
	*out << cgicc::input().set("type", "submit").set("value", "Get Config") << std::endl;
	*out << cgicc::br() << "Config:" << cgicc::br();
	*out << cgicc::form() << std::endl;

	*out << cgicc::form().set("method", "POST").set("action", toolbox::toString("/%s/setConfigTest", getApplicationDescriptor()->getURN().c_str()));
	*out << cgicc::textarea().set("name", "config").set("cols", "60").set("rows", "15") << config_ << cgicc::textarea() << cgicc::br() << std::endl;
	outputViewSelector(out);
	*out << cgicc::input().set("type", "submit").set("value", "Set Config") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br();
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/addTableToConfigTest", getApplicationDescriptor()->getURN().c_str()));
	*out << "Add random table definition to temporary config: " << cgicc::br();
	*out << "Name: " << cgicc::input().set("type", "text").set("name", "tablename");
	*out << cgicc::br() << cgicc::input().set("type", "submit").set("value", "Add Table").set(subtables_ ? "disabled" : "enabled") << " (To store changes to TStore press 'Set Config')" << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br() << cgicc::h2("Sync View and Database:");
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/syncTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	outputConnectionSelector(out);
	*out << "mode: " << cgicc::select().set("name", "mode") << std::endl;
	outputOption(out, "to database");
	outputOption(out, "from database");
	outputOption(out, "both ways");
	*out << cgicc::select();
	*out << "pattern: " << cgicc::input().set("type", "text").set("name", "pattern").set("value", pattern_) << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Sync") << std::endl;

	*out << cgicc::form() << std::endl;
}

//outputs all the usual controls and information
void tstoretest::TStoreTest::outputControls(xgi::Input *in, xgi::Output *out) {
	*out << "<table width=\"100%\">";
	*out << "<tr><td style=\"width:50%\"><h1>Standard Interface</h1></td><td><h1>Administrative Interface</h1></td></tr><tr><td valign=\"top\">";
	outputStandardInterface(out);
	*out << "</td><td valign=\"top\">";
	outputAdministrativeInterface(out);
	*out << "</td></tr></table>";
	*out << cgicc::hr();

	*out << cgicc::h2("Last SOAP Messages");
	*out << cgicc::h4("Sent Message:");
	*out << cgicc::textarea().set("name", "lastResponse").set("cols", "120").set("rows", "5") << std::endl;
	*out << lastMessage_ << cgicc::textarea();

	*out << cgicc::h4("Received Message:");
	*out << cgicc::textarea().set("name", "lastResponse").set("cols", "120").set("rows", "5") << std::endl;
	*out << lastResponse_ << cgicc::textarea() << cgicc::br();
}

void tstoretest::TStoreTest::Default(xgi::Input *in, xgi::Output *out) {
	try {
		outputControls(in, out);
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

///////////////////////General functions for manipulating SOAP messages

/*posts a SOAP message with \a table as an attachment, and the format:
 <tstore:command connectionID="connectionID" ...other parameters.../>
 where other parameters is all the parameters added via the web interface, using the namespace of the view class
 Throws an exception if the attachment could not be added, the message could not be sent, or the response contains
 a SOAP fault.
 */
void tstoretest::TStoreTest::runTableSend(const std::string &connectionID, const std::string &command, xdata::Table &table) {
	std::cout << std::endl << "Sending command " << command << std::endl << std::endl;
	xoap::MessageReference msg = xoap::createMessage();
	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
	xoap::SOAPElement element = addCommand(envelope, command, connectionID);
	std::string className = classNameForConnection(connectionID);

	if(!name_.empty()) {
		if(subtables_)
			addParameter(envelope, element, "table", name_, className);
		else
			addParameter(envelope, element, "name", name_, className);
	}

	addParametersToMessage(envelope, element, className);

	tstoreclient::addAttachment(msg, table, command);

	xoap::MessageReference reply = postSOAPMessage(msg);
}

//adds a command with view ID and path parameters (taken from the GET parameters) required for getting or setting configuration
xoap::SOAPElement tstoretest::TStoreTest::addConfigCommand(xoap::MessageReference msg, cgicc::Cgicc &cgi, const std::string &command, bool newView) {
	path_ = getParameter(cgi, "path");
	auto view = getParameter(cgi, "view");

	if(newView)
		view = getParameter(cgi, "newview");

	try {
		xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
		xoap::SOAPElement commandElement = addAdministrativeCommand(envelope, command, "id", view);
		if (!path_.empty()) {
			xoap::SOAPName path = envelope.createName("path", "tstoresoap", TSTORE_NS_URI);
			commandElement.addAttribute(path, path_);
		}
		return commandElement;
	} catch (xoap::exception::Exception &e) {
		XCEPT_RETHROW(xcept::Exception, "Could not construct SOAP message", e);
	}
}

//adds a <tstore:command> element to envelope, and returns it. If attributeName and value are not empty then they are added as an attribute/value to the node.
xoap::SOAPElement tstoretest::TStoreTest::addAdministrativeCommand(xoap::SOAPEnvelope &envelope, const std::string &command,
		const std::string &attributeName, const std::string &value) {
	xoap::SOAPName msgName = envelope.createName(command, "tstoresoap", TSTORE_NS_URI);
	xoap::SOAPElement element = envelope.getBody().addBodyElement(msgName);
	std::cout << attributeName << ": " << value << std::endl;
	if (!attributeName.empty() && !value.empty()) {
		xoap::SOAPName id = envelope.createName(attributeName, "tstoresoap", TSTORE_NS_URI);
		element.addAttribute(id, value);
	}
	if (dryRun_) {
		xoap::SOAPName dryRun = envelope.createName("test", "tstoresoap", TSTORE_NS_URI);
		element.addAttribute(dryRun, "true");
	}
	return element;
}

//adds a <tstore:command connectionID="connectionID"> element to envelope, and returns it
xoap::SOAPElement tstoretest::TStoreTest::addCommand(xoap::SOAPEnvelope &envelope, const std::string &command, const std::string &connectionID) {
	connectionID_ = connectionID;
	return addAdministrativeCommand(envelope, command, "connectionID", connectionID);
}

//when we are only writing the XML for debugging purposes, if something goes wrong, just output the error instead
//but let everything else continue.
std::string tstoretest::TStoreTest::writeXML(DOMNode *node) {
	try {
		return tstoreclient::writeXML(node);
	} catch (xcept::Exception &e) {
		return e.what();
	}
}

//posts the given message to TStore and returns the response. Raises an exception if the message can not be sent
//or the response contains a SOAP fault.
//also sets lastMessage_ and lastResponse_ with the XML of the message and response, so they can be displayed
//on the web interface.
xoap::MessageReference tstoretest::TStoreTest::postSOAPMessage(xoap::MessageReference msg) {
	xoap::MessageReference reply;
	std::cout << "Message: " << std::endl;
	msg->writeTo(std::cout);
	std::cout << std::endl;
	lastMessage_ = writeXML(msg->getSOAPPart().getEnvelope().getBody().getDOM());
	try {
		const xdaq::ApplicationDescriptor *tstoretestDescriptor = this->getApplicationDescriptor();
		reply = getApplicationContext()->postSOAP(msg, *tstoretestDescriptor, *tstoreDescriptor_);
	} catch (xdaq::exception::Exception &e) {
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		XCEPT_RETHROW(xcept::Exception, "Could not post SOAP message. " + std::string(e.what()), e);
	}

	xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();

	std::cout << std::endl << "Response: " << std::endl;
	reply->writeTo(std::cout);
	std::cout << std::endl;
	lastResponse_ = writeXML(body.getDOM());

	if (body.hasFault()) {
		XCEPT_RAISE(xcept::Exception, body.getFault().getFaultString());
	}
	return reply;
}

//adds an attribute in the namespace of the view class to \a element, with the name \a parameterName 
//and the value \a parameterValue
//assumes a namespace called "viewtype" has already been declared with the URI being the view class
//see http://xdaqwiki.cern.ch/index.php/View_parameter
void tstoretest::TStoreTest::addParameter(xoap::SOAPEnvelope &envelope, xoap::SOAPElement &element, const std::string &parameterName,
		const std::string &parameterValue, const std::string &viewClass) {
	xoap::SOAPName property = envelope.createName(parameterName, "viewtype", viewClass);
	element.addAttribute(property, parameterValue);
}

//adds an attribute to the element \a element for each of the view parameters added via the web interface
//the attributes are in the namespace of the view class.
void tstoretest::TStoreTest::addParametersToMessage(xoap::SOAPEnvelope &envelope, xoap::SOAPElement &element, const std::string &viewClass) {
	try {
		//it doesn't matter what we call the namespace as long as the URI is the view class
		element.addNamespaceDeclaration("viewtype", viewClass);
		std::map<const std::string, std::string>::iterator parameterIterator;
		for (parameterIterator = queryParameters_.begin(); parameterIterator != queryParameters_.end(); parameterIterator++) {
			addParameter(envelope, element, (*parameterIterator).first, (*parameterIterator).second, viewClass);
		}
	} catch (xoap::exception::Exception &e) {
		XCEPT_RAISE(xcept::Exception, (std::string )"Could not add parameter to message. " + e.what());
	}
}

//we need to know the class name for the view the message is for, because it is the namespace used for that view's parameters.
//views are named urn:class-name:view-name
std::string tstoretest::TStoreTest::classNameForConnection(const std::string &connectionID) {
	std::string viewName(connectionIDs_[connectionID]);
	std::string className = tstoreclient::classNameForView(viewName);
	if (className.empty()) {
		XCEPT_RAISE(xcept::Exception,
				"The class name for the connection " + connectionID + " is not known, I don't know which namespace to use for the parameter(s).");
	}
	return className;
}

/*posts a SOAP message with the format:
 <tstore:command connectionID="connectionID" ...other parameters.../> (with no other XML in the message)
 where other parameters is all the parameters added via the web interface, using the namespace of the view class
 Throws an exception if the attachment could not be added, the message could not be sent, or the response contains
 a SOAP fault.
 for most messages you want to send or receive an attached table so it is more convenient to use
 runTableSend() or runTableRequest()
 */
xoap::MessageReference tstoretest::TStoreTest::sendCommandToView(const std::string &connectionID, const std::string &command) {
	//this is basically the sample code from the wiki
	xoap::MessageReference msg = xoap::createMessage();
	xoap::MessageReference reply;
	try {
		xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
		xoap::SOAPElement queryElement = addCommand(envelope, command, connectionID);
		std::string className = classNameForConnection(connectionID);

		if(!name_.empty()) {
			if(subtables_)
				addParameter(envelope, queryElement, "table", name_, className);
			else
				addParameter(envelope, queryElement, "name", name_, className);
		}

		addParametersToMessage(envelope, queryElement, className);
	} catch (xoap::exception::Exception &e) {
		XCEPT_RETHROW(xcept::Exception, "Could not construct SOAP message", e);
	}

	reply = postSOAPMessage(msg);

	return reply;
}

//sends a SOAP message to TStore which returns an xdata table, and copies the table into results.
//call with command="query" or "definition" for example
void tstoretest::TStoreTest::runTableRequest(const std::string &connectionID, const std::string &command, xdata::Table &results) {
	std::cout << std::endl << "retrieving table " << command << std::endl << std::endl;
	xoap::MessageReference reply = sendCommandToView(connectionID, command);
	if (!tstoreclient::getFirstAttachmentOfType(reply, results)) {
		XCEPT_RAISE(xcept::Exception, "Server returned no data");
	}
}

//maybe this should be added to the client library
std::string tstoretest::TStoreTest::keyString(const std::vector<std::string> &keyColumns) {
	std::string keyColumnsString;
	for (std::vector<std::string>::const_iterator keyColumn = keyColumns.begin(); keyColumn != keyColumns.end(); ++keyColumn) {
		if (!keyColumnsString.empty())
			keyColumnsString += ",";
		keyColumnsString += *keyColumn;
	}
	return keyColumnsString;
}

//adds a tag suitable for addTable or removeTable message, in the form:
//<table name="tableName"/>
//as a child of command
void tstoretest::TStoreTest::addTableTag(xoap::SOAPEnvelope &envelope, xoap::SOAPElement &command, const std::string &tableName,
		const std::vector<std::string> &keyColumns) {
	xoap::SOAPName tableElement = envelope.createName("table", "tstoresoap", TSTORE_NS_URI);
	xoap::SOAPElement element = command.addChildElement(tableElement);
	xoap::SOAPName nameAttribute = envelope.createName("name");
	element.addAttribute(nameAttribute, tableName);
	if (!keyColumns.empty()) {

		xoap::SOAPName keyAttribute = envelope.createName("key");
		element.addAttribute(keyAttribute, keyString(keyColumns));
	}
}

//the following functions are used by getConfig/setConfig 

//returns a DOMDocument created from the XML string in \a config
DOMDocument* tstoretest::TStoreTest::parseString(const std::string &config) {
	try {
		// Reuse the parser that was used to parse XML configurations for the Executive
		//
		xoap::DOMParser *p = xoap::getDOMParserFactory()->get("configure");
		DOMDocument *doc;
		doc = p->parse(config);
		return doc;
	} catch (xoap::exception::Exception &xe) {
		XCEPT_RETHROW(xcept::Exception, "Cannot parse XML: " + config, xe);
	}
}

//copies the namespace declarations from \a source to \a destination
//only copies the declarations in the nodes themselves, not in any child elements.
void tstoretest::TStoreTest::copyNamespaceDeclarations(xoap::SOAPElement &destination, const DOMNode *source) {
	DOMNamedNodeMap *attributes = source->getAttributes();
	for (unsigned int attributeIndex = 0; attributeIndex < attributes->getLength(); attributeIndex++) {
		DOMNode *parameterNode = attributes->item(attributeIndex);
		if (xoap::XMLCh2String(parameterNode->getPrefix()) == "xmlns") {
			destination.addNamespaceDeclaration(xoap::XMLCh2String(parameterNode->getLocalName()), xoap::XMLCh2String(parameterNode->getNodeValue()));
		}
	}
}

//parses the XML in \a configurationString and adds it as a child of the SOAP element \a commandElement
void tstoretest::TStoreTest::addConfigurationToMessage(xoap::SOAPElement &commandElement, const std::string &configuration) {
	//add the nodes from config_
	DOMElement *configInMessage = dynamic_cast<DOMElement*>(commandElement.getDOM());
	DOMDocument *configDocument = parseString(configuration);
	DOMElement *alteredConfig = configDocument->getDocumentElement();
	copyNamespaceDeclarations(commandElement, alteredConfig);
	DOMNodeList *bodyList = alteredConfig->getChildNodes();
	for (unsigned int itemIndex = 0; itemIndex < bodyList->getLength(); itemIndex++) {
		configInMessage->appendChild(configInMessage->getOwnerDocument()->importNode(bodyList->item(itemIndex), true));
	}
}

///////////////////////Functions to test the SOAP interface

////////////Administrative interface

//sends a getViews message, extracts the view names from the response and puts them in the instance variable views_
//so that they will be displayed on the web interface.
//see http://xdaqwiki.cern.ch/index.php/TStore_get_views_message
void tstoretest::TStoreTest::testGetViews(xgi::Input *in, xgi::Output *out) {
	try {
		xoap::MessageReference msg = xoap::createMessage();
		xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
		addAdministrativeCommand(envelope, "getViews");
		xoap::MessageReference reply = postSOAPMessage(msg);

		views_.clear();
		DOMNode *responseNode = tstoreclient::getNodeNamed(reply, "getViewsResponse");
		
		//get the view names from the response
		DOMNodeList *viewList = responseNode->getChildNodes();
		for (unsigned int itemIndex = 0; itemIndex < viewList->getLength(); itemIndex++) {
			std::string viewName = xoap::getNodeAttribute(viewList->item(itemIndex), "name");
			views_.push_back(viewName);
		}
		outputControls(in, out); //this has to be done after the view list has been updated

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sends a removeView message for the view specified in the GET parameter
//see http://xdaqwiki.cern.ch/index.php/TStore_remove_table_message
void tstoretest::TStoreTest::testRemoveView(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	try {
		xoap::MessageReference msg = xoap::createMessage();
		msg->getSOAPPart().getEnvelope();
		addConfigCommand(msg, cgi, "removeView");

		xoap::MessageReference reply = postSOAPMessage(msg);
		//perhaps should call getViews to check that the view was really removed

		outputControls(in, out); //this has to be done after the view list has been updated
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sends an addView message to add a new view containing some initial configuration
//The initial configuration sent is the XML given in the config POST parameter
//(from the textarea on the web interface) plus a <tstore:connection/> tag containing the database name and username 
//specified on the web interface.
//see http://xdaqwiki.cern.ch/index.php/TStore_add_view_message
void tstoretest::TStoreTest::testAddView(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	try {
		xoap::MessageReference msg = xoap::createMessage();
		xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
		viewPath_ = getParameter(cgi, "configpath");
		dbname_ = getParameter(cgi, "dbname");
		xoap::SOAPElement commandElement = addConfigCommand(msg, cgi, "addView", true);
		xoap::SOAPName path = envelope.createName("fileName", "tstoresoap", TSTORE_NS_URI);
		commandElement.addAttribute(path, viewPath_);

		//add the initial configuration
		initialViewConfig_ = getParameter(cgi, "config");

		//this must have an outer element to serve as the document element, it will be
		//ignored except for the namespace declarations.
		if (!initialViewConfig_.empty()) {
			addConfigurationToMessage(commandElement, initialViewConfig_);
		}

		//demonstration of how to construct a connection element, if the XML for it is not entered by the user
		xoap::SOAPName connection = envelope.createName("connection", "tstore", "urn:xdaq-tstore:1.0");
		xoap::SOAPElement connectionElement = commandElement.addChildElement(connection);
		xoap::SOAPName dbname = envelope.createName("dbname", "", "");
		connectionElement.addAttribute(dbname, dbname_);



		xoap::MessageReference reply = postSOAPMessage(msg);
		//perhaps should call getViews to check that the view was really added
		outputControls(in, out); //this has to be done after the view list has been updated
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sends a removeTable message to remove three tables with the names prefix_table1, prefix_table2 and prefix_table3
//(prefix_ must have been previously specified on the web interface for an addTable operation)
//note that the removeTable method is deprecated in favour of removing tables manually from the config and then using setConfig and sync messages.
void tstoretest::TStoreTest::testRemoveTable(xgi::Input *in, xgi::Output *out) {
	outputControls(in, out);
	std::string connectionID = connection(in);

	try {
		if (!connectionID.empty()) {
			cgicc::Cgicc cgi(in);
			prefix_ = getParameter(cgi, "prefix");

			xoap::MessageReference msg = xoap::createMessage();
			xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
			xoap::SOAPElement command = addCommand(envelope, "removeTable", connectionID);
			addParametersToMessage(envelope, command, classNameForConnection(connectionID));
			int tableCount = 3;
			while (tableCount--) {
				std::ostringstream tableName;
				tableName << prefix_ << "_table" << tableCount;

				//display the table on the web interface
				*out << "removing table: " << tableName.str() << "<br/>";

				addTableTag(envelope, command, tableName.str());
			}
			postSOAPMessage(msg);
		}
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//adds a table using the addTable SOAP message. This message is deprecated (though it is still nice and convenient)
//instead, you should add a table to the configuration using getConfig and setConfig, and then use a sync to database
//testAddTableToConfig uses the TStore client library to add a table to the config this way.
//see http://xdaqwiki.cern.ch/index.php/TStore_add_table_message
void tstoretest::TStoreTest::testAddTable(xgi::Input *in, xgi::Output *out) {
	std::string connectionID = connection(in);

	try {
		cgicc::Cgicc cgi(in);
		prefix_ = getParameter(cgi, "prefix");
		if (pattern_.empty())
			pattern_ = "^" + toolbox::toupper(prefix_) + "_.*";
		outputControls(in, out);
		if (!connectionID.empty()) {
			xoap::MessageReference msg = xoap::createMessage();
			xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
			xoap::SOAPElement command = addCommand(envelope, "addTable", connectionID);
			addParametersToMessage(envelope, command, classNameForConnection(connectionID));
			int tableCount = 3;
			int subtableCount = 0;
			while (tableCount--) {
				xdata::Table table;
				std::vector<std::string> keyColumns;
				std::ostringstream tableName;
				tableName << prefix_ << "_table" << tableCount;

				randomData_.randomTableDefinition(table, keyColumns, subtableCount);
				LOG4CPLUS_DEBUG(getApplicationLogger(), "After table definition generation");

				//display the table on the web interface
				*out << tableName.str() << " definition: ";
				outputTable(out, table);
				LOG4CPLUS_DEBUG(getApplicationLogger(), "After table printing");
				addTableTag(envelope, command, tableName.str(), keyColumns);

				tstoreclient::addAttachment(msg, table, tableName.str());
				LOG4CPLUS_DEBUG(getApplicationLogger(), "After adding attachement");
			}
			postSOAPMessage(msg);
			LOG4CPLUS_DEBUG(getApplicationLogger(), "After sending message");
		}
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//this adds a random table definition to the _config in memory. It does not send any message to TStore.
//you should check the new table definition on the config displayed on the web interface, and then use
//setConfig and perhaps a sync to database to actually create the new table.
//this assumes that _config is the entire config for the view, or at least that the setConfig will be with
//an XPath such that the nodes will be added at the root of the configuration. Adding a <table/> tag elsewhere
//would not make sense.
void tstoretest::TStoreTest::testAddTableToConfig(xgi::Input *in, xgi::Output *out) {
	std::string connectionID = connection(in);
	xdata::Table table;
	std::string tableName;

	try {
		cgicc::Cgicc cgi(in);
		int subtableCount = 0;

		DOMDocument *configDocument = parseString(config_);
		DOMElement *alteredConfigDOM = configDocument->getDocumentElement();
		xoap::SOAPElement alteredConfig(alteredConfigDOM);

		//perhaps we should check whether that table name already exists in the config.
		std::vector<std::string> keyColumns;
		tableName = getParameter(cgi, "tablename");

		randomData_.randomTableDefinition(table, keyColumns, subtableCount);

		DOMElement *newTableElement = tstoreclient::createDOMElementForTable(configDocument, table, tableName, keyString(keyColumns));

		//now we should add the new table element
		alteredConfig.addChildElement(newTableElement);

		config_ = tstoreclient::writeXML(alteredConfig.getDOM());
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
	outputControls(in, out); //must do this after altering the config so that the changes will show up

	//display the table on the web interface
	*out << tableName << " definition: ";
	outputTable(out, table);
}

//sends destroy message
//see http://xdaqwiki.cern.ch/index.php/TStore_Administrative_SOAP_Service_Protocol#Destroy
void tstoretest::TStoreTest::testDestroy(xgi::Input *in, xgi::Output *out) {
	outputControls(in, out);
	std::string connectionID = connection(in);
	xdata::Table table;
	if (!connectionID.empty()) {
		try {
			cgicc::Cgicc cgi(in);
			name_ = getParameter(cgi, "name");

			xoap::MessageReference reply = sendCommandToView(connectionID, "destroy");
			*out << "Destroyed view.";
		} catch (xcept::Exception &e) {
			outputException(out, e);
		}
	}
}

//sends getConfig message and puts the configuration into the instance variable config_, which is displayed in a textarea on the web interface.
void tstoretest::TStoreTest::testGetConfig(xgi::Input *in, xgi::Output *out) {
	try {
		cgicc::Cgicc cgi(in);
		xoap::MessageReference msg = xoap::createMessage();
		addConfigCommand(msg, cgi, "getConfiguration");

		xoap::MessageReference reply = postSOAPMessage(msg);
		DOMNode *configNode = tstoreclient::getNodeNamed(reply, "getConfigurationResponse");

		//get the text and put it in config_
		config_ = tstoreclient::writeXML(configNode);
		std::cout << "config is: " << config_ << std::endl;
		outputControls(in, out); //this has to be done after config_ has been updated

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sets the configuration for the selected view and the XPath path specified in the path parameter to the XML in the config parameter (from the textarea on the
//web interface)
void tstoretest::TStoreTest::testSetConfig(xgi::Input *in, xgi::Output *out) {
	try {
		cgicc::Cgicc cgi(in);
		xoap::MessageReference msg = xoap::createMessage();
		xoap::SOAPElement commandElement = addConfigCommand(msg, cgi, "setConfiguration");

		config_ = getParameter(cgi, "config");
		addConfigurationToMessage(commandElement, config_);

		postSOAPMessage(msg);

		outputControls(in, out); //this has to be done after config_ has been updated

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sends a TStore sync command with the given connection ID, pattern and sync mode
//returns the SOAP response
xoap::MessageReference tstoretest::TStoreTest::sendSyncMessage(const std::string &connectionID, const std::string &pattern, const std::string &mode) {
	xoap::MessageReference msg = xoap::createMessage();
	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
	xoap::SOAPElement commandElement = addCommand(envelope, "sync", connectionID);
	xoap::SOAPName modeName = envelope.createName("mode", "tstoresoap", TSTORE_NS_URI);
	commandElement.addAttribute(modeName, mode);
	xoap::SOAPName patternName = envelope.createName("pattern", "tstoresoap", TSTORE_NS_URI);
	commandElement.addAttribute(patternName, pattern_);

	return postSOAPMessage(msg);
}

//sends a sync message with the sync mode and pattern specified in the web interface.
//see http://xdaqwiki.cern.ch/index.php/TStore_sync_message
//If the 'test' checkbox is checked then it will have the test parameter set to true.
//if the 'test' checkbox is not checked, then this will actually send two messages... one with test mode off, and a second with all other parameters the same
//but with test mode on. The response to the second message is then checked to make sure no actions would have been carried out, since after the first message
//there should be nothing more to do to get the database and the config in sync. If there are any actions in the second message, then there must be an error in TStore.
//this second message is not necessary in other client applications, it's just an extra check for the testing of TStore.
void tstoretest::TStoreTest::testSync(xgi::Input *in, xgi::Output *out) {
	try {
		cgicc::Cgicc cgi(in);
		std::string mode = getParameter(cgi, "mode");
		pattern_ = getParameter(cgi, "pattern");
		std::string connectionID = connection(cgi);

		sendSyncMessage(connectionID, pattern_, mode);

		//the following code is just for testing that TStore does what it should.
		//if we were not in test mode, send another message with the same parameters but with test mode on
		//and make sure that nothing would have been done, since by now everything should already be synchronised
		//so a second sync should have no effect.
		if (!dryRun_) {
			std::cout << "sending a second sync message in test mode to make sure nothing else needs to be done." << std::endl;

			//record the real sync message and response,
			//since it would be more useful to display them on the web interface
			//rather than the ones for this extra test message.
			std::string oldLastMessage = lastMessage_;
			std::string oldLastResponse = lastResponse_;
			dryRun_ = true;
			xoap::MessageReference response = sendSyncMessage(connectionID, pattern_, mode);
			dryRun_ = false;
			lastMessage_ = oldLastMessage;
			if (tstoreclient::responseHasSyncActions(response)) {
				//this is an error in TStore.
				lastResponse_ = "Response to initial sync message:\n\n" + oldLastResponse + "\n\nResponse to second message in test mode:\n\n" + lastResponse_;
				*out
						<< "A second sync (in test mode) indicated that after the initial sync there are still some changes to be made for the sync to complete. This could indicate a bug in TStore.<br/>"
						<< std::endl;
			} else {
				lastResponse_ = oldLastResponse;
			}
		}

		outputControls(in, out);

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

////////////Standard interface
/// The messages in the standard interface are all sent with the view parameters previously added on the web interface
/// and the connection ID chosen in the menu on the web interface. The connection ID determines which view is affected.
/// For most messages, some parameters are required to specify which data should be affected

//this runs a query, reverses all the strings in the returned table and adds 42 to all the floats in the table, then sends the table back in an update SOAP message. 
//It then queries again to check what was written to the database. The three tables (unaltered, altered, and the altered version as retrieved from the database) are
//displayed on the web interface.
//This won't work if the altered columns are primary keys in the database, since TStore will not be able to tell which rows to update.
//see http://xdaqwiki.cern.ch/index.php/TStore_update_programming_example
void tstoretest::TStoreTest::testUpdate(xgi::Input *in, xgi::Output *out) {
	outputControls(in, out);
	std::string updateName = connection(in);
	xdata::Table table;
	if (!updateName.empty()) {
		try {
			cgicc::Cgicc cgi(in);
			name_ = getParameter(cgi, "name");

			runTableRequest(updateName, "query", table);
			*out << "Old table: ";
			outputTable(out, table);

			std::vector<std::string> columns = table.getColumns();
			std::vector<std::string>::iterator columnIterator;
			unsigned long rowCount = table.getRowCount();
			unsigned long currentRow;
			for (currentRow = 0; currentRow < rowCount; currentRow++) {
				for (columnIterator = columns.begin(); columnIterator != columns.end(); columnIterator++) {
					if ((*columnIterator).find("PRIMARY") == std::string::npos) {

						std::string columnType = table.getColumnType(*columnIterator);

						if (columnType == "string") {
							std::string value = (std::string) table.getValueAt(currentRow, *columnIterator)->toString();
							std::reverse(value.begin(), value.end());
							xdata::String xdataValue(value);
							table.setValueAt(currentRow, *columnIterator, xdataValue);
						} else if (columnType == "float") {
							xdata::Float *value = static_cast<xdata::Float*>(table.getValueAt(currentRow, *columnIterator));
							*value = (float) (*value) + 42;
							table.setValueAt(currentRow, *columnIterator, *value);
						} else { //set a new random value for any other types
							xdata::Serializable *value = randomData_.randomValue(columnType);
							table.setValueAt(currentRow, *columnIterator, *value);
							delete value;
						}
					}
				}
			}

			*out << "Altered table in memory: ";
			outputTable(out, table);

			runTableSend(updateName, "update", table);

			runTableRequest(updateName, "query", table);
			*out << "Altered table in database: ";
			outputTable(out, table);
		} catch (xcept::Exception &e) {
			outputException(out, e);
		}
	}
}

//this sends a clear message, then a query to verify that the resulting table really has been cleared
//see http://xdaqwiki.cern.ch/index.php/TStore_clear_programming_example
void tstoretest::TStoreTest::testClear(xgi::Input *in, xgi::Output *out) {
	outputControls(in, out);
	std::string connectionID = connection(in);
	xdata::Table table;
	if (!connectionID.empty()) {
		try {
			cgicc::Cgicc cgi(in);
			name_ = getParameter(cgi, "name");

			xoap::MessageReference reply = sendCommandToView(connectionID, "clear");
			runTableRequest(connectionID, "query", table);
			*out << "Altered table in database: ";
			outputTable(out, table);
		} catch (xcept::Exception &e) {
			outputException(out, e);
		}
	}
}

//This method sends a query SOAP message, picks a few rows from the resulting table at random, and sends them back in a delete message.
//Then sends another query message to find out whether the rows were really deletes.
//It displays the initial query results, the rows to be deleted, and the results of the second query on the web interface.
void tstoretest::TStoreTest::testDelete(xgi::Input *in, xgi::Output *out) {
	outputControls(in, out);
	std::string updateName = connection(in);
	xdata::Table table;
	if (!updateName.empty()) {
		try {
			cgicc::Cgicc cgi(in);
			name_ = getParameter(cgi, "name");

			runTableRequest(updateName, "query", table);
			*out << "Old table: ";
			outputTable(out, table);
			srand(time(NULL));
			std::map<std::string, std::string, xdata::Table::ci_less> tableDefinition = table.getTableDefinition();
			//tableDefinition.erase("DA"); //test how it handles deleting when not all key columns are supplied
			xdata::Table rowsToDelete(tableDefinition);
			unsigned long rowCount = table.getRowCount();
			if (!rowCount) {
				XCEPT_RAISE(xcept::Exception, "Nothing to delete.");
			}
			unsigned int rowIndex = rand() % rowCount;
			std::vector<std::string> columns = table.getColumns();
			for (std::vector<std::string>::iterator columnIterator = columns.begin(); columnIterator != columns.end(); columnIterator++) {
				xdata::Serializable *value = table.getValueAt(rowIndex, *columnIterator);
				if (value /*&& *columnIterator!="DA"*/)
					rowsToDelete.setValueAt(0, *columnIterator, *value);
			}
			//insertRandomRow(rowsToDelete); //test how it handles deleting a row that doesn't exist.
			*out << "Rows to delete: ";
			outputTable(out, rowsToDelete);

			runTableSend(updateName, "delete", rowsToDelete);

			runTableRequest(updateName, "query", table);
			*out << "Altered table in database: ";
			outputTable(out, table);
		} catch (xcept::Exception &e) {
			outputException(out, e);
		}
	}
}

//sends a connect message and sets connectionID_ to the returned connection ID.
void tstoretest::TStoreTest::connect(std::string viewName, std::string password, std::string username, std::string timeout) {
	xoap::MessageReference msg = xoap::createMessage();
	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
	xoap::SOAPElement commandElement = addAdministrativeCommand(envelope, "connect", "id", viewName);

	//the attribute authentication="basic" is mandatory
	xoap::SOAPName authentication = envelope.createName("authentication", "tstoresoap", TSTORE_NS_URI);
	commandElement.addAttribute(authentication, "basic");

	//with authentication mode "basic" the credentials should be in the form username/password
	xoap::SOAPName credentials = envelope.createName("credentials", "tstoresoap", TSTORE_NS_URI);
	commandElement.addAttribute(credentials, username + "/" + password);

	//add the timeout
	toolbox::TimeInterval timeInt(stod(timeout));
	xoap::SOAPName timeoutName = envelope.createName("timeout", "tstoresoap", TSTORE_NS_URI);
	commandElement.addAttribute(timeoutName, timeInt.toString("xs:duration"));

	xoap::MessageReference reply = postSOAPMessage(msg);
	connectionID_ = tstoreclient::connectionID(reply);
	connectionIDs_[connectionID_] = viewName;
}

//this sends a connect message using the view id, username and password specified on the web interface
//it then adds the returned connection ID and view name to the instance variable connectionIDs_ so that it can be shown
//in the menus of connection IDs on the web interface.
void tstoretest::TStoreTest::testConnect(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	std::string viewName = getParameter(cgi, "view");
	std::string password = getParameter(cgi, "password");
	std::string username = getParameter(cgi, "username");
	std::string timeout = getParameter(cgi, "timeout");

	try {
		//the loop is just for testing, as there was a bug (1919765) which occurred when connecting and disconnecting a lot.
		// you only need to connect once
//		for (int i=0; i<20; i++) {
//			connect(viewName,password,username,timeout);
//			disconnect(connectionID_);
//		}
		connect(viewName, password, username, timeout);
		view_ = viewName;
		outputControls(in, out); //do this after getting the response, so that the new connection will be in the menus
	} catch (xcept::Exception &e) {
		outputControls(in, out);
		outputException(out, e);
	}
}

void tstoretest::TStoreTest::testRenew(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	std::string connectionID = connection(cgi);
	std::string timeout = getParameter(cgi, "timeout");

	xoap::MessageReference msg = xoap::createMessage();
	try {
		xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
		xoap::SOAPElement commandElement = addCommand(envelope, "renew", connectionID);

		//add the timeout
		toolbox::TimeInterval timeInt(stod(timeout));
		xoap::SOAPName timeoutName = envelope.createName("timeout", "tstoresoap", TSTORE_NS_URI);
		commandElement.addAttribute(timeoutName, timeInt.toString("xs:duration"));

		postSOAPMessage(msg);
		outputControls(in, out);
	} catch (xcept::Exception &e) {
		outputControls(in, out);
		outputException(out, e);
	}
}

void tstoretest::TStoreTest::disconnect(std::string connectionID) {
	xoap::MessageReference msg = xoap::createMessage();
	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
	addCommand(envelope, "disconnect", connectionID);
	connectionIDs_.erase(connectionID); //if there is an error actually closing it, it's probably because the connection doesn't exist any more anyway (maybe TStore was restarted) so delete it from the interface anyway
	postSOAPMessage(msg);
}

//this sends a disconnect message for the connection ID specified on the web interface
//even if the message fails, it will remove the connection ID from the menus on the web interface.
//this is because the most likely reason for the message failing is that you restarted TStore and TStore no longer recognises the connection ID
//so it can not (and does not need to) disconnect it.
void tstoretest::TStoreTest::testDisconnect(xgi::Input *in, xgi::Output *out) {
	std::string connectionID = connection(in);
	//xoap::MessageReference msg = xoap::createMessage();
	try {
		disconnect(connectionID);
		outputControls(in, out);
	} catch (xcept::Exception &e) {
		outputControls(in, out);
		outputException(out, e);
	}
}

//sends a query message and displays the result on the web interface
void tstoretest::TStoreTest::testQuery(xgi::Input *in, xgi::Output *out) {
	testTableRequest(in, out, "query");
}

//sends a definition message and displays the result on the web interface
void tstoretest::TStoreTest::testDefinition(xgi::Input *in, xgi::Output *out) {
	testTableRequest(in, out, "definition");
}

//this is just to test whether TStore can handle column names with the wrong case
void tstoretest::TStoreTest::changeCaseOfColumnNames(xdata::Table &table) {
	std::map<std::string, std::string, xdata::Table::ci_less> columns = table.getTableDefinition();
	std::map<std::string, std::string, xdata::Table::ci_less>::iterator column;
	for (column = columns.begin(); column != columns.end(); column++) {
		xdata::Table innerTable;
		if ((*column).second == "table") {
			innerTable = *static_cast<xdata::Table*>(table.getValueAt(0, (*column).first));
			changeCaseOfColumnNames(innerTable);
		}
		table.removeColumn((*column).first);
		std::string newColumnName = toolbox::tolower((*column).first);
		std::cout << "adding column " << newColumnName << std::endl;
		table.addColumn(newColumnName, (*column).second);
		if ((*column).second == "table") {
			table.setValueAt(0, newColumnName, innerTable);
		}
	}
}

//gets the table definition using a definition message, inserts some rows with random data, then sends the table in an insert message.
//sends a query message to check whether the new rows were really inserted.
//won't work when there are constraints on the data which means a random value will not be accepted
void tstoretest::TStoreTest::testInsert(xgi::Input *in, xgi::Output *out) {
	xdata::Table table;
	outputControls(in, out);
	std::string connectionID = connection(in);
	if (!connectionID.empty()) {
		srand(time(NULL));
		//for (int i=0; i<600;i++) {
		try {
			cgicc::Cgicc cgi(in);
			std::string name = getParameter(cgi, "name");

			runTableRequest(connectionID, "definition", table);

			//it seems SQLView can no longer handle column names with different cases
			//the table definitions do not compare equal, even though they use xdata::Table::ci_less
			//changeCaseOfColumnNames(table); //this is just to test that TStore can cope with it
			randomData_.insertRandomRowsIntoTable(table);
			*out << "Altered table in memory: ";
			outputTable(out, table);

			runTableSend(connectionID, "insert", table);

			runTableRequest(connectionID, "query", table);
			*out << "Altered table in database: ";
			outputTable(out, table);
		} catch (xcept::Exception &e) {
			outputException(out, e);
			//break;
		}
	}
	//}
}
