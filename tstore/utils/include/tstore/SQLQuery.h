// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_SQLQuery_h_
#define _toolbox_SQLQuery_h_


#include <string>
#include <vector>

#include "tstore/Query.h" 
#include "tstore/exception/Exception.h"
#include "xdata/Serializable.h" 
#include "xdata/Table.h"

namespace tstore {

class SQLStatement : public toolbox::Properties {
	public:
	//create an SQLStatement from an SQL statement
	//the SQL statement can also be set using setProperty("statement",statement);
	SQLStatement( const std::string statement );
	
	//if your statement contains bind variables, then the bindVariables parameter should contain the values for them in the order
	//that they are mentioned in the SQL statement.
	//to set bind variables which can not be expressed as strings, use setParameterAtIndex after creating the query instead.
	SQLStatement(const std::string statement,const std::vector<std::string> &bindValues);

	unsigned int parameterCount();
	xdata::Serializable *parameterAtIndex(unsigned parameterIndex) ;
	void setParameterAtIndex(unsigned int parameterIndex,const std::string &parameterValue) ;
	void setParameterAtIndex(unsigned int parameterIndex,/*const*/ xdata::Serializable *parameterValue) ;
	private:
	xdata::Table bindValues_;
	std::string columnName(unsigned int index) ;
};

class SQLQuery: /*public tstore::Query :*/ public tstore::SQLStatement {

	public:
	
	//create an SQLQuery from an SQL statement
	//the SQL statement can also be set using setProperty("statement",statement);
	SQLQuery( const std::string &statement,const std::string &tableName="") : SQLStatement(statement) {
		tableName_=tableName;
	}
	
	//if your statement contains bind variables, then the bindVariables parameter should contain the values for them in the order
	//that they are mentioned in the SQL statement.
	SQLQuery(const std::string &statement,const std::vector<std::string> &bindValues,const std::string &tableName) : SQLStatement(statement,bindValues) {
		tableName_=tableName;
	}
	
	std::string tableName() {
		return tableName_;
	}
	
	private:
	std::string tableName_; //to match with mappings
}; 


}

#endif
