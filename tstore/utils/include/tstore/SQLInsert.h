// $Id#include "toolbox/Properties.h": SQLInsert.h,v 1.1 2005/11/17 10:41:25 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_Insert_h_
#define _tstore_Insert_h_

#include "tstore/SQLDataManipulation.h"

namespace tstore {
class SQLInsert: public tstore::SQLDataManipulation { 
	public:
	SQLInsert(std::string tableName) : SQLDataManipulation(tableName) {}
	SQLInsert(const std::set<std::string> &columns,std::string tableName) : SQLDataManipulation(columns,tableName) {}
	private:
};
}

#endif
