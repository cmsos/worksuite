/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tstoreutils_version_h_
#define _tstoreutils_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_TSTOREUTILS_VERSION_MAJOR 3
#define WORKSUITE_TSTOREUTILS_VERSION_MINOR 4
#define WORKSUITE_TSTOREUTILS_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_TSTOREUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_TSTOREUTILS_PREVIOUS_VERSIONS "3.1.0,3.1.1,3.1.2,3.2.0,3.2.1"

//
#define WORKSUITE_TSTOREUTILS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TSTOREUTILS_VERSION_MAJOR,WORKSUITE_TSTOREUTILS_VERSION_MINOR,WORKSUITE_TSTOREUTILS_VERSION_PATCH)
#ifndef WORKSUITE_TSTOREUTILS_PREVIOUS_VERSIONS
#define WORKSUITE_TSTOREUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TSTOREUTILS_VERSION_MAJOR,WORKSUITE_TSTOREUTILS_VERSION_MINOR,WORKSUITE_TSTOREUTILS_VERSION_PATCH)
#else 
#define WORKSUITE_TSTOREUTILS_FULL_VERSION_LIST  WORKSUITE_TSTOREUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TSTOREUTILS_VERSION_MAJOR,WORKSUITE_TSTOREUTILS_VERSION_MINOR,WORKSUITE_TSTOREUTILS_VERSION_PATCH)
#endif 

namespace tstoreutils
{
	const std::string project = "worksuite";
	const std::string package  =  "tstoreutils";
	const std::string versions = WORKSUITE_TSTOREUTILS_FULL_VERSION_LIST;
	const std::string description = "Table store utility library for use with Oracle RDBMS";
	const std::string authors = "Angela Brett";
	const std::string summary = "Table store utility library for use with Oracle RDBMS";	
	const std::string link = "http://xdaqwiki.cern.ch/index.php/TStore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
