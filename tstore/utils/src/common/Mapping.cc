#include "tstore/Mapping.h"
//#include <regex.h>
#include <exception>
#include <stdexcept>
#include "toolbox/regex.h"
#include "toolbox/string.h"

tstore::Mapping::Mapping(std::string xdataType,std::string columnName,std::string tableName,std::string dbType,std::string format) :
	xdataType_(xdataType), dbType_(toolbox::toupper(dbType)), format_(format) {
	/*if (columnName.empty()) columnName_="*";
	else */columnName_=toolbox::toupper(columnName);
	/*if (tableName.empty()) tableName_="*";
	else */tableName_=toolbox::toupper(tableName);
	exactMatch_=false;
}

bool tstore::Mapping::matchesTable(std::string tableName) const  {
	if (exactMatch_) return toolbox::toupper(tableName)==tableName_;
	if (tableName_.empty()) return true;
	try {
		return toolbox::regx_match_nocase( toolbox::toupper(tableName), tableName_ );
	} catch (std::bad_alloc &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not check if table name '"+tableName+"' matches regular expression. "+(std::string)e.what());
	} catch (std::invalid_argument &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not check if table name '"+tableName+"' matches regular expression. "+(std::string)e.what());
	}
}

bool tstore::Mapping::matchesColumn(std::string columnName) const  {
	if (exactMatch_) return toolbox::toupper(columnName)==columnName_;
	if (columnName_.empty()) return true;
	try {
		return toolbox::regx_match_nocase( toolbox::toupper(columnName), columnName_ );
	} catch (std::bad_alloc &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not check if column name '"+columnName+"' matches regular expression. "+(std::string)e.what());
	} catch (std::invalid_argument &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not check if column name '"+columnName+"' matches the regular expression '"+columnName_+"'"+(std::string)e.what());
	}
}
