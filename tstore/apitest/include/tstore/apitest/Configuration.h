#ifndef _apitest_configuration_h_
#define _apitest_configuration_h_

#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"

namespace apitest {

class Configuration {
public:

	Configuration(xdata::InfoSpace*);

	const std::string& tstoreURL() const {
		return tstoreURL_.value_;
	}
	const size_t tstoreLid() const {
		return tstoreLid_.value_;
	}
	const uint32_t tStoreRetries() const {
		return tStoreRetries_.value_;
	}
	const uint32_t tStoreRetrySleepSeconds() const {
		return tStoreRetrySleepSeconds_.value_;
	}

private:

	xdata::UnsignedInteger32 tStoreRetries_;           // how many retries in tStore messages
	xdata::UnsignedInteger32 tStoreRetrySleepSeconds_; // how long to sleep in seconds between retries in tStore messages
	xdata::String tstoreURL_;                          // TStore URL, e.g. http://kvm-s3562-1-ip151-109.cms:9947
	xdata::UnsignedInteger32 tstoreLid_;               // TStore lid

};

}

#endif /* CONFIGURATON_H_ */
