// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _apitest_version_h_
#define _apitest_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_APITEST_VERSION_MAJOR 1
#define WORKSUITE_APITEST_VERSION_MINOR 0
#define WORKSUITE_APITEST_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_APITEST_PREVIOUS_VERSIONS "3.8.0,3.8.1"

#define WORKSUITE_APITEST_PREVIOUS_VERSIONS ""

//
// Template macros
//
#define WORKSUITE_APITEST_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_APITEST_VERSION_MAJOR,WORKSUITE_APITEST_VERSION_MINOR,WORKSUITE_APITEST_VERSION_PATCH)
#ifndef WORKSUITE_APITEST_PREVIOUS_VERSIONS
#define WORKSUITE_APITEST_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_APITEST_VERSION_MAJOR,WORKSUITE_APITEST_VERSION_MINOR,WORKSUITE_APITEST_VERSION_PATCH)
#else 
#define WORKSUITE_APITEST_FULL_VERSION_LIST  WORKSUITE_APITEST_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_APITEST_VERSION_MAJOR,WORKSUITE_APITEST_VERSION_MINOR,WORKSUITE_APITEST_VERSION_PATCH)
#endif 

namespace apitest
{
	const std::string project = "worksuite";
	const std::string package  =  "apitest";
	const std::string versions = WORKSUITE_APITEST_FULL_VERSION_LIST;
	const std::string description = "Test application for SOAP message builder TStoreAPI";
	const std::string summary = "Test application using TStoreAPI";
	const std::string authors = "Angela Brett & Christine Zeh";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/TStore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
