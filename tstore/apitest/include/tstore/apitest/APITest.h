// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _apitest_h_
#define _apitest_h_

#include "xdaq/Application.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdaq/WebApplication.h"
#include "toolbox/Properties.h"
#include "xdata/Table.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPEnvelope.h"
#include "xdata/TimeVal.h"
#include "xdata/Mime.h"
#include "xdata/Vector.h"

#include "tstore/apitest/RandomData.h"
#include "tstore/apitest/Configuration.h"

#include "tstore/api/admin/ConfigurationImpl.h"

namespace apitest {

class APITest: public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener {
public:

	XDAQ_INSTANTIATOR();

	APITest(xdaq::ApplicationStub *s);
	void Default(xgi::Input *in, xgi::Output *out);
	void configureRandomData();
	void actionPerformed(toolbox::Event &event);

	//test standard interface
	void testDefinition(xgi::Input *in, xgi::Output *out);
	void testInsert(xgi::Input *in, xgi::Output *out);
	void testConnect(xgi::Input *in, xgi::Output *out);
	void testRenew(xgi::Input *in, xgi::Output *out);
	void testDisconnect(xgi::Input *in, xgi::Output *out);
	void testClear(xgi::Input *in, xgi::Output *out);
	void testAddTableToConfig(xgi::Input *in, xgi::Output *out);

	//test administrative interface
	void testGetViews(xgi::Input *in, xgi::Output *out);
	void testAddView(xgi::Input *in, xgi::Output *out);
	void testRemoveView(xgi::Input *in, xgi::Output *out);
	void testGetConfig(xgi::Input *in, xgi::Output *out);
	void testSetConfig(xgi::Input *in, xgi::Output *out);
	void testSync(xgi::Input *in, xgi::Output *out);

private:
	//web interface
	void outputCheckbox(xgi::Output *out, bool on, const std::string &name);
	void outputException(xgi::Output *out, xcept::Exception &e);
	void outputTable(xgi::Output *out, xdata::Table::Reference results);
	void outputOption(xgi::Output *out, const std::string &name);
	void outputViewSelector(xgi::Output *out);
	void outputConnectionSelector(xgi::Output *out);
	void outputAdministrativeInterface(xgi::Output *out);
	void outputStandardInterface(xgi::Output *out);
	void outputControls(xgi::Input *in, xgi::Output *out);

	void setBoolean(xgi::Input *in, bool &on, const std::string &name);
	std::string getParameter(cgicc::Cgicc &cgi, const std::string &name);
	void setParameters(xgi::Input *in, xgi::Output *out);

	//constructing message
	std::string connection(xgi::Input *in);
	std::string connection(cgicc::Cgicc &cgi);
	static std::string keyString(const std::vector<std::string> &keyColumns);
	static std::string parseConfig(DOMNode* domNode);

	//sending message
	xoap::MessageReference postSOAPMessage(xoap::MessageReference msg);
	void sendQuery(const std::string &viewName, const std::string name, xdata::Table::Reference results);

	//manipulating tables
	std::string writeXML(DOMNode *node);

	apitest::Configuration configuration_;
	xdaq::ApplicationDescriptor *tstoreDescriptor_;

	std::map<const std::string, std::string> queryParameters_;
	std::string view_;
	std::string newview_;
	std::string connectionID_;
	std::map<std::string, std::string> connectionIDs_;
	std::vector<std::string> sqlViews_;
	std::vector<std::string> nestedViews_;
	std::string lastMessage_; //the latest SOAP message
	std::string lastResponse_; //the response of the latest SOAP message
	tstore::api::admin::ConfigurationImpl config_; //the results of the latest getConfig message
	std::string dbname_; //database name for the view to be added
	std::string username_; //username for the view to be added
	std::string path_;
	std::string prefix_;
	std::string name_;
	std::string pattern_;
	std::string viewPath_; //path to add a new view to
	bool insertNulls_ = false;
	bool insertInfinities_ = false;
	bool longStrings_ = false;
	bool dryRun_ = false;
	bool subtables_ = false;
	RandomData randomData_;
};
}

#endif
