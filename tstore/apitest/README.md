# APITest

This is a client application to test the functionality of the TStore API library. The client application connects to a TStore service running in a different process. It allows to test the creation, altering and deletion of SQLViews and the querying and insertion of NestedViews. To achieve this, two view configurations are provided, or a new one can be created through the web interface.
It has only the functionality provided by the API, so for testing the TStore, use TStoreTest.

This application is based on the TStoreTest application, so the documentation there can also be used here. Please refer to the README in the folder `tstore/tstoretest` for explanations about views, configuration and building.

## Running APITEST

To run APITest, change the DB specific information in the views. In the files `store/cats.view` and `testview.view` fill the `dbname` with the information from the database in the form of `hostname:port/servicename` in the line
```xml
 <tstore:connection dbname="hostname:port/surface" xmlns:tstore="urn:xdaq-tstore:1.0"/>
 ```
To use the configs without changings, export the `$TSTORE_XML` with the correct path to the folder containing the views.


Run the following command to start up the TStore service.
```bash
$XDAQ_ROOT/bin/xdaq.exe -p 10001 -l DEBUG -e $TSTORE_XML/tstore.profile
```

In another terminal run the next command to start up the test client.
```bash
$XDAQ_ROOT/bin/xdaq.exe -p 10000 -l DEBUG -c $TSTORE_XML/apitest.xml
```

## Usage

The usage of the web interface is similar to the TStoreTest. However, some small changes have been implemented.

- The View dropdown is only showing SQL views by default. To switch to Nested views, enable the checkbox `NestedViews` and press `Set`.
- When creating a new view, just input the view id withoug the View namespace, like `urn::tstore-view-SQL`. This will automatically added when creating the SOAP message. To create a Nested view check the corresponding box.
- The config is now stored as a class object and not as string. Therefore, it is not possible to change it directly in the textarea. To test the `setConfig` request, use the `AddTable` functionality.
- When working with Nested views, the `depth` can be added directly and is a mandatory field to fill.
