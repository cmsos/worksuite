#include "tstore/apitest/Configuration.h"

apitest::Configuration::Configuration(xdata::InfoSpace *infoSpace) :
		tStoreRetries_(6), tStoreRetrySleepSeconds_(20), tstoreURL_(""), tstoreLid_(0) {
	infoSpace->fireItemAvailable("tstoreURL", &tstoreURL_);
	infoSpace->fireItemAvailable("tstoreLid", &tstoreLid_);
	infoSpace->fireItemAvailable("tStoreRetries", &tStoreRetries_);
	infoSpace->fireItemAvailable("tStoreRetrySleepSeconds", &tStoreRetrySleepSeconds_);
}

