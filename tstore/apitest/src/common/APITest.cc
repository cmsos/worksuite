// $Id$
// A test application which communicates with the TStore application via the TStoreAPI library.

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/apitest/APITest.h"
#include "tstore/apitest/Configuration.h"

#include <cgicc/FormEntry.h>
#include <cgicc/Cgicc.h>
#include <sstream>

#include "cgicc/HTMLClasses.h"
#include "xcept/Exception.h"

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationDescriptorFactory.h"
#include "xdaq/NamespaceURI.h"

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/TableIterator.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/DOMParserFactory.h"

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/dom/DOMLSSerializer.hpp>
#include <xercesc/dom/DOMLSOutput.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>

#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"

#include "tstore/client/AttachmentUtils.h"
#include "tstore/client/LoadDOM.h"
#include "tstore/client/Client.h"

#include "tstore/api/NS.h"
#include "tstore/api/admin/GetViews.h"
#include "tstore/api/admin/GetViewsResponse.h"
#include "tstore/api/admin/AddSQLView.h"
#include "tstore/api/admin/AddNestedView.h"
#include "tstore/api/admin/RemoveView.h"
#include "tstore/api/admin/Sync.h"
#include "tstore/api/admin/GetConfigurationResponse.h"
#include "tstore/api/admin/GetConfiguration.h"
#include "tstore/api/admin/SetConfiguration.h"
#include "tstore/api/Connect.h"
#include "tstore/api/Renew.h"
#include "tstore/api/Disconnect.h"
#include "tstore/api/QueryDefinition.h"
#include "tstore/api/QueryDefinitionNested.h"
#include "tstore/api/QueryDefinitionResponse.h"
#include "tstore/api/Clear.h"
#include "tstore/api/ClearNested.h"
#include "tstore/api/Insert.h"
#include "tstore/api/InsertNested.h"


//
// provides factory method for instantion of APITest application
//
XDAQ_INSTANTIATOR_IMPL(apitest::APITest)

apitest::APITest::APITest(xdaq::ApplicationStub *s) :
		xdaq::Application(s), xgi::framework::UIManager(this), configuration_(s->getInfoSpace()){

	getApplicationContext()->addActionListener(this);
	s->getDescriptor()->setAttribute("icon","/tstore/apitest/images/apitest-icon.png");

	xgi::framework::deferredbind(this, this, &APITest::Default, "Default");

	//standard interface
	xgi::framework::deferredbind(this, this, &APITest::testDefinition, "definitionTest");
	xgi::framework::deferredbind(this, this, &APITest::testInsert, "insertTest");
	xgi::framework::deferredbind(this, this, &APITest::testConnect, "connectTest");
	xgi::framework::deferredbind(this, this, &APITest::testRenew, "renewTest");
	xgi::framework::deferredbind(this, this, &APITest::testDisconnect, "disconnectTest");
	xgi::framework::deferredbind(this, this, &APITest::testClear, "clearTest");

	//administrative interface
	xgi::framework::deferredbind(this, this, &APITest::testAddTableToConfig, "addTableToConfigTest");
	xgi::framework::deferredbind(this, this, &APITest::testGetConfig, "getConfigTest");
	xgi::framework::deferredbind(this, this, &APITest::testSetConfig, "setConfigTest");
	xgi::framework::deferredbind(this, this, &APITest::testSync, "syncTest");
	xgi::framework::deferredbind(this, this, &APITest::testGetViews, "getViewsTest");
	xgi::framework::deferredbind(this, this, &APITest::testAddView, "addViewTest");
	xgi::framework::deferredbind(this, this, &APITest::testRemoveView, "removeViewTest");

	xgi::framework::deferredbind(this, this, &APITest::setParameters, "setParameters");

	configureRandomData();

	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& e) {
		char* message = XMLString::transcode(e.getMessage());
		std::cerr << "Error during Xerces initialization: " << message << std::endl;
		XMLString::release(&message);
	}

}

void apitest::APITest::configureRandomData() {
	randomData_.createNulls = insertNulls_;
	randomData_.createInfinities = insertInfinities_;
	randomData_.createSubtables = subtables_;
	randomData_.maxStringLength = longStrings_ ? 100 : 20;
}

void apitest::APITest::actionPerformed(toolbox::Event &event) {
	if (event.type() == "urn:xdaq-event:configuration-loaded") {
		LOG4CPLUS_DEBUG(getApplicationLogger(), "configuration-loaded");
		//Create a tstore descriptor

		auto tstoreContext = new xdaq::ContextDescriptor(configuration_.tstoreURL());
		LOG4CPLUS_DEBUG(getApplicationLogger(),
				"Add Application Description for context " + configuration_.tstoreURL() + " and Lid " + std::to_string(configuration_.tstoreLid()));

		std::set<std::string> zones;
		zones.insert(getApplicationContext()->getDefaultZoneName());

		std::set<std::string> groups;
		xdaq::ApplicationDescriptorFactory *applicationDescriptorFactory = xdaq::ApplicationDescriptorFactory::getInstance();

		try {
			tstoreDescriptor_ = applicationDescriptorFactory->createApplicationDescriptor(tstoreContext, "tstore::TStore", configuration_.tstoreLid(), zones, groups);

		} catch (xdaq::exception::DuplicateApplicationDescriptor &e) {
			XCEPT_RETHROW(xcept::Exception, "Application descriptor for 'tstore' already exists", e);
		} catch (xdaq::exception::InvalidZone &e) {
			XCEPT_RETHROW(xcept::Exception, "Invalid zone for application descriptor for 'tstore.", e);
		}
	}
}

///////////////////////Functions for dealing with the web interface (these probably are not of interest to anyone writing their own application)

//looks at the GET parameter named \a name and sets \a on to true if the parameter is "on", false otherwise
void apitest::APITest::setBoolean(xgi::Input *in, bool &on, const std::string &name) {
	cgicc::Cgicc cgi(in);
	std::string value = getParameter(cgi, name);
	if (value == "on")
		on = true;
	else
		on = false;
}

void apitest::APITest::setParameters(xgi::Input *in, xgi::Output *out) {
	setBoolean(in, insertNulls_, "NULL");
	setBoolean(in, insertInfinities_, "Infinities");
	setBoolean(in, dryRun_, "Test");
	setBoolean(in, subtables_, "NestedView");
	setBoolean(in, longStrings_, "Long Strings");
	configureRandomData();
	outputControls(in, out);
}

//this should of course be in some other, non-TStore-related class...
void apitest::APITest::outputTable(xgi::Output *out, xdata::Table::Reference results) {
	//this uses raw HTML tags because cgicc can't handle any nested tags, which makes it pretty much useless
	std::vector<std::string> columns = results->getColumns();
	std::vector<std::string>::iterator columnIterator;
	*out << results->getRowCount() << " rows";
	*out << "<table border=\"2\">";
	*out << "<tr>";
	for (columnIterator = columns.begin(); columnIterator != columns.end(); ++columnIterator) {
		*out << "<td>" << *columnIterator << " (" << results->getColumnType(*columnIterator) << ")" << "</td>";
	}
	*out << "</tr>";
	unsigned long rowIndex;
	for (rowIndex = 0; rowIndex < results->getRowCount(); rowIndex++) {
		*out << "<tr>";
		for (columnIterator = columns.begin(); columnIterator != columns.end(); columnIterator++) {
			*out << "<td>";
			if (results->getColumnType(*columnIterator) == "table") {
				xdata::Table subtable = *static_cast<xdata::Table*>(results->getValueAt(rowIndex, *columnIterator));
				xdata::Table::Reference tab = std::make_shared<xdata::Table>(subtable);
				outputTable(out, tab);
			} else if (results->getColumnType(*columnIterator) == "mime") {
				*out << "mime";
			} else {
				*out << results->getValueAt(rowIndex, *columnIterator)->toString();
			}
			*out << "</td>";

		}
		*out << "</tr>";
	}
	*out << "</table>";
}

void apitest::APITest::outputException(xgi::Output *out, xcept::Exception &e) {
	std::cout << e.message() << std::endl;
	*out << e.message();
}

/*I'm sure that just **cgi[name] used to work, but now trying to access a value that wasn't set corrupts some memory
 so here is a function to do it.*/
std::string apitest::APITest::getParameter(cgicc::Cgicc &cgi, const std::string &name) {
	std::vector<cgicc::FormEntry> result;
	if (cgi.getElement(name, result)) {
		return result[0].getValue();
	} else {
		std::cout << "no " << name << " specified" << std::endl;
		return "";
	}
}

//use this one if you need the cgi for something else as well, since it will crash if you try to create two Cgiccs from the same Input
std::string apitest::APITest::connection(cgicc::Cgicc &cgi) {
	return getParameter(cgi, "connection");
}

std::string apitest::APITest::connection(xgi::Input *in) {
	cgicc::Cgicc cgi(in);
	return connection(cgi);
}

void apitest::APITest::outputCheckbox(xgi::Output *out, bool on, const std::string &name) {
	if (on) {
		*out << cgicc::input().set("type", "checkbox").set("name", name).set("checked", "checked");
	} else {
		*out << cgicc::input().set("name", name).set("type", "checkbox");
	}
	*out << " " << name << " ";
}

//outputs an option for a select element, with both the value and the displayed name as \a name
void apitest::APITest::outputOption(xgi::Output *out, const std::string &name) {
	*out << cgicc::option().set("value", name) << name << cgicc::option() << std::endl;
}

void apitest::APITest::outputViewSelector(xgi::Output *out) {
	if (sqlViews_.empty() && nestedViews_.empty()) {
		*out << cgicc::input().set("type", "text").set("name", "view").set("value", view_) << std::endl;
	} else {
		*out << cgicc::select().set("name", "view");

		if(subtables_) {
			for (std::vector<std::string>::iterator view = nestedViews_.begin(); view != nestedViews_.end(); ++view) {
				outputOption(out, *view);
			}
		} else {
			for (std::vector<std::string>::iterator view = sqlViews_.begin(); view != sqlViews_.end(); ++view) {
				outputOption(out, *view);
			}
		}

		*out << cgicc::select();
	}
}

void apitest::APITest::outputConnectionSelector(xgi::Output *out) {
	*out << cgicc::select().set("name", "connection"); //<< cgicc::input().set("type","text").set("name","view").set("value",view) << std::endl;
	for (std::map<std::string, std::string>::reverse_iterator connection = connectionIDs_.rbegin(); connection != connectionIDs_.rend(); ++connection) {
		*out << cgicc::option().set("value", (*connection).first) << (*connection).first << " (" << (*connection).second << ")" << cgicc::option() << std::endl;
	}
	*out << cgicc::select();
}

void apitest::APITest::outputStandardInterface(xgi::Output *out) {
	//test connect
	*out << cgicc::h2("Manage Connection to View");
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/connectTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << cgicc::br() << "Connect to view: ";
	outputViewSelector(out);
	*out << cgicc::br() << "Username: " << cgicc::input().set("type", "text").set("name", "username") << std::endl;
	*out << cgicc::br() << "Password: " << cgicc::input().set("type", "password").set("name", "password") << std::endl;
	*out << cgicc::br() << "Timeout: " << cgicc::input().set("type", "text").set("name", "timeout") << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Connect") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br()
			<< cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/disconnectTest", getApplicationDescriptor()->getURN().c_str()))
			<< std::endl;
	*out << "Disconnect: ";
	outputConnectionSelector(out);
	*out << cgicc::input().set("type", "submit").set("value", "Run") << std::endl;
	*out << cgicc::form() << std::endl;

	//renew connection
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/renewTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << cgicc::br() << "Renew: ";
	outputConnectionSelector(out);
	*out << cgicc::br() << "Timeout: " << cgicc::input().set("type", "text").set("name", "timeout") << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Renew") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::h2("Select Special Behaviour");
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/setParameters", getApplicationDescriptor()->getURN().c_str())) << std::endl;

	*out << cgicc::h3("Test Run");
	outputCheckbox(out, dryRun_, "Test");
	*out << cgicc::br() << "If checked, the requests will not be forwarded to the Oracle DB." << cgicc::br();

	//form for deciding whether insert can send e.g. NULLs (turn this off if there are columns which can't be NULL)
	*out << cgicc::h3("Insert and Update Operation");

	outputCheckbox(out, insertNulls_, "NULL");
	outputCheckbox(out, insertInfinities_, "Infinities");
	outputCheckbox(out, longStrings_, "Long Strings"); //create longer random strings to test CLOB storage

	*out << cgicc::br() << "Select if NULL and infinity can be set for non primary key columns. Long string allows for up to 100 characters." << cgicc::br();

	*out << cgicc::h3("Table Definition");
	outputCheckbox(out, subtables_, "NestedView"); //sending an empty MIME crashes the serializer, so we can not have subtables of a table with Mime columns
	*out << cgicc::br() << "Creates nested tables, which are built in the DB as multiple tables connected by foreign keys." << cgicc::br();

	*out << cgicc::input().set("type", "submit").set("value", "Set") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::h2("Manage Table Content");

	//the standard interface commands which manage and query table content
	std::string commDML[] = { "definition", "insert", "clear" };
	std::vector<std::string> keysDML(commDML, commDML + sizeof(commDML) / sizeof(std::string));
	for (std::vector<std::string>::iterator key = keysDML.begin(); key != keysDML.end(); ++key) {
		*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/%sTest", getApplicationDescriptor()->getURN().c_str(), (*key).c_str()))
				<< std::endl;
		*out << *key << ": ";
		outputConnectionSelector(out);
		if(subtables_)
			*out << " Table: ";
		else
			*out << " Name: ";

		*out << cgicc::input().set("type", "text").set("name", "name").set("value", name_) << std::endl;

		if(subtables_) {
			*out << " Depth: ";
			*out << cgicc::input().set("type", "number").set("name", "depth").set("value", "0") << std::endl;
		}


		*out << cgicc::input().set("type", "submit").set("value", "Run").set(subtables_ && *key == "clear" ? "disabled" : "enabled") << std::endl;
		*out << cgicc::br() << cgicc::form() << std::endl;
	}
}

void apitest::APITest::outputAdministrativeInterface(xgi::Output *out) {
	*out << cgicc::h2("Load Views");
	*out << "Load all views to get started testing TStore." << cgicc::br();
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/getViewsTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << cgicc::br() << "Get Views : " << cgicc::input().set("type", "submit").set("value", "Run") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br() << cgicc::h2("Add View") << cgicc::br();
	*out << cgicc::form().set("method", "POST").set("action", toolbox::toString("/%s/addViewTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	*out << "View ID: " << cgicc::input().set("type", "text").set("name", "newview").set("value", newview_) << cgicc::br() << std::endl;
	*out << "Filename (No Extension): " << cgicc::input().set("type", "text").set("name", "configpath").set("value", viewPath_) << cgicc::br() << std::endl;
	*out << "Database Name: " << cgicc::input().set("type", "text").set("name", "dbname").set("value", dbname_) << cgicc::br() << std::endl;
	*out << "Nested View: " << cgicc::input().set("name", "nested").set("type", "checkbox") << cgicc::br() << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Add View") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br();
	*out << cgicc::form().set("method", "POST").set("action", toolbox::toString("/%s/removeViewTest", getApplicationDescriptor()->getURN().c_str()));
	outputViewSelector(out);
	*out << cgicc::input().set("type", "submit").set("value", "Remove view") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br() << cgicc::h2("Edit Config") << "Select View: ";
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/getConfigTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	outputViewSelector(out);
	*out << cgicc::input().set("type", "submit").set("value", "Get Config") << std::endl;
	*out << cgicc::br() << "Config:" << cgicc::br();
	*out << cgicc::form() << std::endl;

	*out << cgicc::form().set("method", "POST").set("action", toolbox::toString("/%s/setConfigTest", getApplicationDescriptor()->getURN().c_str()));
	*out << cgicc::textarea().set("name", "config").set("cols", "60").set("rows", "15").set("readonly") << std::endl;

	*out << parseConfig(config_.getDocument());

	*out << cgicc::textarea() << cgicc::br() << std::endl;
	outputViewSelector(out);
	*out << cgicc::input().set("type", "submit").set("value", "Set Config") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br();
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/addTableToConfigTest", getApplicationDescriptor()->getURN().c_str()));
	*out << "Add random table definition to temporary config: " << cgicc::br();
	*out << "Name: " << cgicc::input().set("type", "text").set("name", "tablename");
	*out << cgicc::br() << cgicc::input().set("type", "submit").set("value", "Add Table").set(subtables_ ? "disabled" : "enabled") << " (To store changes to TStore press 'Set Config')" << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br() << cgicc::h2("Sync View and Database:");
	*out << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/syncTest", getApplicationDescriptor()->getURN().c_str())) << std::endl;
	outputConnectionSelector(out);
	*out << "mode: " << cgicc::select().set("name", "mode") << std::endl;
	outputOption(out, "to database");
	outputOption(out, "from database");
	outputOption(out, "both ways");
	*out << cgicc::select();
	*out << "pattern: " << cgicc::input().set("type", "text").set("name", "pattern").set("value", pattern_) << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Sync") << std::endl;

	*out << cgicc::form() << std::endl;
}

//outputs all the usual controls and information
void apitest::APITest::outputControls(xgi::Input *in, xgi::Output *out) {
	*out << "<table width=\"100%\">";
	*out << "<tr><td style=\"width:50%\"><h1>Standard Interface</h1></td><td><h1>Administrative Interface</h1></td></tr><tr><td valign=\"top\">";
	outputStandardInterface(out);
	*out << "</td><td valign=\"top\">";
	outputAdministrativeInterface(out);
	*out << "</td></tr></table>";
	*out << cgicc::hr();

	*out << cgicc::h2("Last SOAP Messages");
	*out << cgicc::h4("Sent Message:");
	*out << cgicc::textarea().set("name", "lastResponse").set("cols", "120").set("rows", "5") << std::endl;
	*out << lastMessage_ << cgicc::textarea();

	*out << cgicc::h4("Received Message:");
	*out << cgicc::textarea().set("name", "lastResponse").set("cols", "120").set("rows", "5") << std::endl;
	*out << lastResponse_ << cgicc::textarea() << cgicc::br();
}

void apitest::APITest::Default(xgi::Input *in, xgi::Output *out) {
	try {
		outputControls(in, out);
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

///////////////////////General functions for manipulating SOAP messages


//when we are only writing the XML for debugging purposes, if something goes wrong, just output the error instead
//but let everything else continue.
std::string apitest::APITest::writeXML(DOMNode *node) {
	try {
		return tstoreclient::writeXML(node);
	} catch (xcept::Exception &e) {
		return e.what();
	}
}

//posts the given message to TStore and returns the response. Raises an exception if the message can not be sent
//or the response contains a SOAP fault.
//also sets lastMessage_ and lastResponse_ with the XML of the message and response, so they can be displayed
//on the web interface.
xoap::MessageReference apitest::APITest::postSOAPMessage(xoap::MessageReference msg) {
	xoap::MessageReference reply;
	std::cout << "Message: " << std::endl;
	msg->writeTo(std::cout);
	std::cout << std::endl;
	lastMessage_ = writeXML(msg->getSOAPPart().getEnvelope().getBody().getDOM());
	try {
		const xdaq::ApplicationDescriptor *apitestDescriptor = this->getApplicationDescriptor();
		reply = getApplicationContext()->postSOAP(msg, *apitestDescriptor, *tstoreDescriptor_);
	} catch (xdaq::exception::Exception &e) {
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		XCEPT_RETHROW(xcept::Exception, "Could not post SOAP message. " + std::string(e.what()), e);
	}

	xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();

	std::cout << std::endl << "Response: " << std::endl;
	reply->writeTo(std::cout);
	std::cout << std::endl;
	lastResponse_ = writeXML(body.getDOM());

	if (body.hasFault()) {
		XCEPT_RAISE(xcept::Exception, body.getFault().getFaultString());
	}
	return reply;
}


//sends a SOAP message to TStore which returns an xdata table, and copies the table into results.
//call with command="query" or "definition" for example
void apitest::APITest::sendQuery(const std::string &connectionID, std::string name, xdata::Table::Reference results) {
	std::cout << std::endl << "retrieving table query" << std::endl << std::endl;

	xoap::MessageReference msg = xoap::createMessage();
	xoap::MessageReference reply;

	try {
			xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();

			xoap::SOAPName msgName = envelope.createName("query", "tstoresoap", TSTORE_NS_URI);
			xoap::SOAPElement element = envelope.getBody().addBodyElement(msgName);
			if (!connectionID.empty()) {
				xoap::SOAPName id = envelope.createName("connectionID", "tstoresoap", TSTORE_NS_URI);
				element.addAttribute(id, connectionID);
			}

			std::string viewName(connectionIDs_[connectionID]);
			std::string className = tstoreclient::classNameForView(viewName);
			if (className.empty()) {
				XCEPT_RAISE(xcept::Exception,
						"The class name for the connection " + connectionID + " is not known, I don't know which namespace to use for the parameter(s).");
			}

			if(!name.empty()) {
				element.addNamespaceDeclaration("viewtype", className);
				if(subtables_) {
					xoap::SOAPName property = envelope.createName("table", "viewtype", className);
					element.addAttribute(property, name);
				}
				else {
					xoap::SOAPName property = envelope.createName("name", "viewtype", className);
					element.addAttribute(property, name);
				}
			}

		} catch (xoap::exception::Exception &e) {
			XCEPT_RETHROW(xcept::Exception, "Could not construct SOAP message", e);
		}

		reply = postSOAPMessage(msg);

	if (!tstoreclient::getFirstAttachmentOfType(reply, *results)) {
		XCEPT_RAISE(xcept::Exception, "Server returned no data");
	}
}

//maybe this should be added to the client library
std::string apitest::APITest::keyString(const std::vector<std::string> &keyColumns) {
	std::string keyColumnsString;
	for (std::vector<std::string>::const_iterator keyColumn = keyColumns.begin(); keyColumn != keyColumns.end(); ++keyColumn) {
		if (!keyColumnsString.empty())
			keyColumnsString += ",";
		keyColumnsString += *keyColumn;
	}
	return keyColumnsString;
}

//the following function is used to parse the DOMNode into a string, allowing it to be printed
std::string apitest::APITest::parseConfig(DOMNode* domNode) {
	DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(XMLString::transcode("LS"));

	DOMLSSerializer* serializer = impl->createLSSerializer();

	if (serializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true)) {
		serializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);
	}

	XMLCh* xmlString = serializer->writeToString(domNode);
	return XMLString::transcode(xmlString);
}

///////////////////////Functions to test the SOAP interface

////////////Administrative interface

//sends a getViews message, extracts the view names from the response and puts them in the instance variable views_
//so that they will be displayed on the web interface.
//see http://xdaqwiki.cern.ch/index.php/TStore_get_views_message
void apitest::APITest::testGetViews(xgi::Input *in, xgi::Output *out) {
	try {
		tstore::api::admin::GetViews getviews;
		xoap::MessageReference msg = getviews.toSOAP();
		xoap::MessageReference reply = postSOAPMessage(msg);
		tstore::api::admin::GetViewsResponse getviewsresp(reply);

		sqlViews_.clear();
	    std::for_each(getviewsresp.getSQLViews().begin(), getviewsresp.getSQLViews().end(), [](auto& s){ s.insert(0, tstore::api::sql::NamespaceUri + ":");});
		sqlViews_.insert(sqlViews_.end(), getviewsresp.getSQLViews().begin(), getviewsresp.getSQLViews().end());

		nestedViews_.clear();
		std::for_each(getviewsresp.getNestedViews().begin(), getviewsresp.getNestedViews().end(), [](auto& s){ s.insert(0, tstore::api::nested::NamespaceUri + ":");});
		nestedViews_.insert(nestedViews_.end(), getviewsresp.getNestedViews().begin(), getviewsresp.getNestedViews().end());

		outputControls(in, out); //this has to be done after the view list has been updated

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sends a removeView message for the view specified in the GET parameter
//see http://xdaqwiki.cern.ch/index.php/TStore_remove_table_message
void apitest::APITest::testRemoveView(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	try {
		std::string view = getParameter(cgi, "view");

		tstore::api::admin::RemoveView removeview(view);
		xoap::MessageReference msg = removeview.toSOAP();
		xoap::MessageReference reply = postSOAPMessage(msg);

		outputControls(in, out); //this has to be done after the view list has been updated
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sends an addView message to add a new view containing some initial configuration
//The initial configuration sent is the XML given in the config POST parameter
//(from the textarea on the web interface) plus a <tstore:connection/> tag containing the database name and username 
//specified on the web interface.
//see http://xdaqwiki.cern.ch/index.php/TStore_add_view_message
void apitest::APITest::testAddView(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	try {

		std::string viewPath = getParameter(cgi, "configpath");
		std::string dbname = getParameter(cgi, "dbname");
		std::string view = getParameter(cgi, "newview");
		std::string nested = getParameter(cgi, "nested");

		xoap::MessageReference msg;

		if (nested == "on") {
			tstore::api::admin::AddNestedView addview(view, viewPath, dbname);
			msg = addview.toSOAP();
		} else {
			tstore::api::admin::AddSQLView addview(view, viewPath, dbname);
			msg = addview.toSOAP();
		}

		xoap::MessageReference reply = postSOAPMessage(msg);
		outputControls(in, out); //this has to be done after the view list has been updated
	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//this adds a random table definition to the _config in memory. It does not send any message to TStore.
//you should check the new table definition on the config displayed on the web interface, and then use
//setConfig and perhaps a sync to database to actually create the new table.
//this assumes that _config is the entire config for the view, or at least that the setConfig will be with
//an XPath such that the nodes will be added at the root of the configuration. Adding a <table/> tag elsewhere
//would not make sense.
void apitest::APITest::testAddTableToConfig(xgi::Input *in, xgi::Output *out) {
	std::string connectionID = connection(in);
	xdata::Table table;
	xdata::Table::Reference ref = std::make_shared<xdata::Table>(table);
	std::string tableName;

	try {
		cgicc::Cgicc cgi(in);
		int subtableCount = 0;

		//perhaps we should check whether that table name already exists in the config.
		std::vector<std::string> keyColumns;
		tableName = getParameter(cgi, "tablename");

		randomData_.randomTableDefinition(ref, keyColumns, subtableCount);
		DOMElement *newTableElement = tstoreclient::createDOMElementForTable(config_.getDocument(), *ref, tableName, keyString(keyColumns));


		DOMDocument * document = config_.getDocument();
		DOMElement * element = document->getDocumentElement();
		element->appendChild(newTableElement);

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
	outputControls(in, out); //must do this after altering the config so that the changes will show up

	//display the table on the web interface
	*out << tableName << " definition: ";
	outputTable(out, ref);
}


//sends getConfig message and puts the configuration into the instance variable config_, which is displayed in a textarea on the web interface.
void apitest::APITest::testGetConfig(xgi::Input *in, xgi::Output *out) {
	try {
		cgicc::Cgicc cgi(in);
		auto view = getParameter(cgi, "view");

		tstore::api::admin::GetConfiguration getConf("", view);
		xoap::MessageReference reply = postSOAPMessage(getConf.toSOAP());
		tstore::api::admin::GetConfigurationResponse response(reply);

		//get the text and put it in config_
		config_.import( dynamic_cast<tstore::api::admin::ConfigurationImpl&>(response.getConfiguration()) );
		outputControls(in, out); //this has to be done after config_ has been updated

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sets the configuration for the selected view and the XPath path specified in the path parameter to the XML in the config parameter (from the textarea on the
//web interface)
void apitest::APITest::testSetConfig(xgi::Input *in, xgi::Output *out) {
	try {
		cgicc::Cgicc cgi(in);
		std::string view = getParameter(cgi, "view");

		tstore::api::admin::SetConfiguration setConf(view, "urn:view-class", "");
		setConf.setConfiguration(config_);
		postSOAPMessage(setConf.toSOAP());

		outputControls(in, out); //this has to be done after config_ has been updated

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

//sends a sync message with the sync mode and pattern specified in the web interface.
//see http://xdaqwiki.cern.ch/index.php/TStore_sync_message
//If the 'test' checkbox is checked then it will have the test parameter set to true.
//if the 'test' checkbox is not checked, then this will actually send two messages... one with test mode off, and a second with all other parameters the same
//but with test mode on. The response to the second message is then checked to make sure no actions would have been carried out, since after the first message
//there should be nothing more to do to get the database and the config in sync. If there are any actions in the second message, then there must be an error in TStore.
//this second message is not necessary in other client applications, it's just an extra check for the testing of TStore.
void apitest::APITest::testSync(xgi::Input *in, xgi::Output *out) {
	try {
		cgicc::Cgicc cgi(in);
		std::string mode = getParameter(cgi, "mode");
		std::string pattern = getParameter(cgi, "pattern");
		std::string connectionID = connection(cgi);

		tstore::api::admin::Sync sync(connectionID, mode, pattern);
		xoap::MessageReference msg = sync.toSOAP();
		xoap::MessageReference reply = postSOAPMessage(msg);

		//the following code is just for testing that TStore does what it should.
		//if we were not in test mode, send another message with the same parameters but with test mode on
		//and make sure that nothing would have been done, since by now everything should already be synchronised
		//so a second sync should have no effect.
		if (!dryRun_) {
			std::cout << "sending a second sync message in test mode to make sure nothing else needs to be done." << std::endl;

			//record the real sync message and response,
			//since it would be more useful to display them on the web interface
			//rather than the ones for this extra test message.
			std::string oldLastMessage = lastMessage_;
			std::string oldLastResponse = lastResponse_;
			dryRun_ = true;
			reply = postSOAPMessage(msg);
			dryRun_ = false;
			lastMessage_ = oldLastMessage;
			if (tstoreclient::responseHasSyncActions(reply)) {
				//this is an error in TStore.
				lastResponse_ = "Response to initial sync message:\n\n" + oldLastResponse + "\n\nResponse to second message in test mode:\n\n" + lastResponse_;
				*out << "A second sync (in test mode) indicated that after the initial sync there are still some changes to be made for the sync to complete. This could indicate a bug in TStore.<br/>" << std::endl;
			} else {
				lastResponse_ = oldLastResponse;
			}
		}

		outputControls(in, out);

	} catch (xcept::Exception &e) {
		outputException(out, e);
	}
}

////////////Standard interface
/// The messages in the standard interface are all sent with the view parameters previously added on the web interface
/// and the connection ID chosen in the menu on the web interface. The connection ID determines which view is affected.
/// For most messages, some parameters are required to specify which data should be affected

//this sends a clear message, then a query to verify that the resulting table really has been cleared
//see http://xdaqwiki.cern.ch/index.php/TStore_clear_programming_example
void apitest::APITest::testClear(xgi::Input *in, xgi::Output *out) {
	outputControls(in, out);
	std::string connectionID = connection(in);
	xdata::Table table;
	xdata::Table::Reference ref = std::make_shared<xdata::Table>(table);

	if (!connectionID.empty()) {
		try {
			cgicc::Cgicc cgi(in);
			std::string name = getParameter(cgi, "name");

			tstore::api::Clear clear(connectionID, name);
			xoap::MessageReference msg = clear.toSOAP();
			xoap::MessageReference reply = postSOAPMessage(msg);

			sendQuery(connectionID, name, ref);
			*out << "Altered table in database: ";
			outputTable(out, ref);
		} catch (xcept::Exception &e) {
			outputException(out, e);
		}
	}
}

//this sends a connect message using the view id, username and password specified on the web interface
//it then adds the returned connection ID and view name to the instance variable connectionIDs_ so that it can be shown
//in the menus of connection IDs on the web interface.
void apitest::APITest::testConnect(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	std::string viewName = getParameter(cgi, "view");
	std::string password = getParameter(cgi, "password");
	std::string username = getParameter(cgi, "username");
	std::string timeout = getParameter(cgi, "timeout");
	std::string credentials = username + "/" + password;
	toolbox::TimeInterval timeInt(stod(timeout));

	try {
		tstore::api::Connect connect(viewName, "basic", credentials ,timeInt.toString("xs:duration"));
		xoap::MessageReference msg = connect.toSOAP();

		xoap::MessageReference reply = postSOAPMessage(msg);
		connectionID_ = tstoreclient::connectionID(reply);
		connectionIDs_[connectionID_] = viewName;
		view_ = viewName;

		outputControls(in, out); //do this after getting the response, so that the new connection will be in the menus
	} catch (xcept::Exception &e) {
		outputControls(in, out);
		outputException(out, e);
	}
}

void apitest::APITest::testRenew(xgi::Input *in, xgi::Output *out) {
	cgicc::Cgicc cgi(in);
	std::string connectionID = connection(cgi);
	std::string timeout = getParameter(cgi, "timeout");
	toolbox::TimeInterval timeInt(stod(timeout));

	try {
		tstore::api::Renew renew(connectionID, timeInt.toString("xs:duration"));
		xoap::MessageReference msg = renew.toSOAP();

		postSOAPMessage(msg);
		outputControls(in, out);
	} catch (xcept::Exception &e) {
		outputControls(in, out);
		outputException(out, e);
	}
}

//this sends a disconnect message for the connection ID specified on the web interface
//even if the message fails, it will remove the connection ID from the menus on the web interface.
//this is because the most likely reason for the message failing is that you restarted TStore and TStore no longer recognises the connection ID
//so it can not (and does not need to) disconnect it.
void apitest::APITest::testDisconnect(xgi::Input *in, xgi::Output *out) {
	std::string connectionID = connection(in);
	try {
		tstore::api::Disconnect disconnect(connectionID);
		xoap::MessageReference msg = disconnect.toSOAP();
		postSOAPMessage(msg);
		connectionIDs_.erase(connectionID);

		outputControls(in, out);
	} catch (xcept::Exception &e) {
		outputControls(in, out);
		outputException(out, e);
	}
}

//sends a definition message and displays the result on the web interface
void apitest::APITest::testDefinition(xgi::Input *in, xgi::Output *out) {
	std::string connectionID = connection(in);
	if (!connectionID.empty()) {
		try {
			cgicc::Cgicc cgi(in);
			std::string name = getParameter(cgi, "name");

			xoap::MessageReference msg;
			if(subtables_) {
				size_t depth = toolbox::toLong(getParameter(cgi, "depth"));
				tstore::api::QueryDefinitionNested definition(connectionID, name, depth);
				msg = definition.toSOAP();
			} else {
				tstore::api::QueryDefinition definition(connectionID, name);
				msg = definition.toSOAP();
			}
			xoap::MessageReference reply = postSOAPMessage(msg);

			tstore::api::QueryDefinitionResponse definitionResponse(reply);

			outputControls(in, out);
			outputTable(out, definitionResponse.getTable());

		} catch (xcept::Exception &e) {
			outputException(out, e);
		}
	}

}

//gets the table definition using a definition message, inserts some rows with random data, then sends the table in an insert message.
//sends a query message to check whether the new rows were really inserted.
//won't work when there are constraints on the data which means a random value will not be accepted
void apitest::APITest::testInsert(xgi::Input *in, xgi::Output *out) {
	xdata::Table::Reference table;
	outputControls(in, out);
	std::string connectionID = connection(in);
	if (!connectionID.empty()) {
		try {
			cgicc::Cgicc cgi(in);
			std::string name = getParameter(cgi, "name");

			xoap::MessageReference msg;
			if(subtables_) {
				size_t depth = toolbox::toLong(getParameter(cgi, "depth"));
				tstore::api::QueryDefinitionNested definition(connectionID, name, depth);
				msg = definition.toSOAP();
			} else {
				tstore::api::QueryDefinition definition(connectionID, name);
				msg = definition.toSOAP();
			}
			xoap::MessageReference reply = postSOAPMessage(msg);

			tstore::api::QueryDefinitionResponse definitionResponse(reply);
			table = definitionResponse.getTable();

			randomData_.insertRandomRowsIntoTable(*table);
			*out << "Altered table in memory: ";
			outputTable(out, table);

			if(subtables_) {
				tstore::api::InsertNested insert(connectionID, name, table, 2);
				msg = insert.toSOAP();
			} else {
				tstore::api::Insert insert(connectionID, name, table);
				msg = insert.toSOAP();
			}

			reply = postSOAPMessage(msg);

			sendQuery(connectionID, name, table);
			*out << "Altered table in database: ";
			outputTable(out, table);
		} catch (xcept::Exception &e) {
			outputException(out, e);
		}
	}
}
