#!/bin/bash

# export XDAQ_ROOT
echo 'define XDAQ_ROOT in jcStart.sh'
export XDAQ_ROOT=
export XDAQ_DOCUMENT_ROOT=$XDAQ_ROOT/daq

# start xdaq.exe on port 39999
$XDAQ_ROOT/daq/xdaq/scripts/xdaq.sh -h $HOSTNAME -p 39997 -e $XDAQ_ROOT/daq/jobcontrol/install/xdaq-jc.profile

