// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include <vector>
#include <map>
#include <string>
#include "xcept/Exception.h"
#include "xdata/Table.h"
#include "toolbox/TimeVal.h"
#include "xmas/probe/Repository.h"


xmas::probe::Repository::Repository()
{
}

xmas::probe::Repository::~Repository()
{
	this->clear();
}





void xmas::probe::Repository::add( const toolbox::TimeVal& time, xdata::Table::Reference & table) 
	
{	
	
	std::map<toolbox::TimeVal, xdata::Table::Reference>::iterator k = tables_.find(time);
	if (k != tables_.end())
	{
		// This error should never occurs. Means the time precision is too coarse
		XCEPT_RAISE(xmas::probe::exception::Exception, "Cannot add table, entry for given time exists");
	}
	// add if table not yet existing or timestamp not existing
	tables_[time] = table;	
}

void xmas::probe::Repository::add ( xdata::Table::Reference & table)
	
{
	this->add( toolbox::TimeVal::gettimeofday(), table);		
}

// Remove all tables of a flashlist, older than or equal the given absolute time
void xmas::probe::Repository::remove( const toolbox::TimeVal& time) 
	
{
	
	std::vector<toolbox::TimeVal> toErase; // remember what to erase

	std::map<toolbox::TimeVal, xdata::Table::Reference>::iterator k;
	for (k = tables_.begin(); k != tables_.end(); k++)
	{
		if ( (*k).first <= time )
		{
			toErase.push_back((*k).first);
		}				
	}

	for (std::vector<toolbox::TimeVal>::size_type i = 0; i < toErase.size(); i++)
	{
		tables_.erase(toErase[i]); // delete map entry					
	}	
}				

void xmas::probe::Repository::clear() 
{

	// remove the entry for the flashlist
	// tables_.erase(flashlist); 
	tables_.clear(); // don't delete inner map, just clear it	
}

// Get all tables of a flashlist that are younger than "from" and older than "to"
std::map<toolbox::TimeVal, xdata::Table::Reference> xmas::probe::Repository::getHistory(
	 const toolbox::TimeVal& from, const toolbox::TimeVal& to)
{
	std::map<toolbox::TimeVal, xdata::Table::Reference> history;
	
	
	std::map<toolbox::TimeVal, xdata::Table::Reference>::iterator k;
	for (k = tables_.begin(); k != tables_.end(); k++)
	{
	
		//std::cout << "T1:" << (*k).first.toString(toolbox::TimeVal::gmt) << " T2:" << to.toString(toolbox::TimeVal::gmt) << std::endl;
		if (( (*k).first >= from ) && ((*k).first <= to) )
		{
			history[ (*k).first ] = (*k).second;
		}
	}
	

	return history; // return an empty map if not found (may not exist or not collected)
}

// Return the latest added table, throw if nothing found
xdata::Table::Reference xmas::probe::Repository::last() 
{
	
	if ( !tables_.empty() )
	{
		/*std::map<toolbox::TimeVal, xdata::Table*>::iterator k = (*j).second.begin();
		return (*k).second;
		*/
		std::map<toolbox::TimeVal, xdata::Table::Reference>::reverse_iterator k = tables_.rbegin();
		return (*k).second;
	}
	else
	{
		XCEPT_RAISE (xmas::probe::exception::Exception, "No tables available");
	}
	
}

