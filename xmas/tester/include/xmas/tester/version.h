// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_tester_version_h_
#define _xmas_tester_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASTESTER_VERSION_MAJOR 1
#define WORKSUITE_XMASTESTER_VERSION_MINOR 11 
#define WORKSUITE_XMASTESTER_VERSION_PATCH 2
// If any previous versions available E.g. #define WORKSUITE_XMASTESTER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_XMASTESTER_PREVIOUS_VERSIONS "1.11.1"


//
// Template macros
//
#define WORKSUITE_XMASTESTER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASTESTER_VERSION_MAJOR,WORKSUITE_XMASTESTER_VERSION_MINOR,WORKSUITE_XMASTESTER_VERSION_PATCH)
#ifndef WORKSUITE_XMASTESTER_PREVIOUS_VERSIONS
#define WORKSUITE_XMASTESTER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASTESTER_VERSION_MAJOR,WORKSUITE_XMASTESTER_VERSION_MINOR,WORKSUITE_XMASTESTER_VERSION_PATCH)
#else 
#define WORKSUITE_XMASTESTER_FULL_VERSION_LIST  WORKSUITE_XMASTESTER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASTESTER_VERSION_MAJOR,WORKSUITE_XMASTESTER_VERSION_MINOR,WORKSUITE_XMASTESTER_VERSION_PATCH)
#endif 

namespace xmastester
{
	const std::string project = "worksuite";
	const std::string package  =  "xmastester";
	const std::string versions = WORKSUITE_XMASTESTER_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "XDAQ Monitoring and Alarming System tester";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
