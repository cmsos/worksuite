// $Id$

/*************************************************************************
 * XDAQ XMAS Heartbeat Probe               								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _xmas_heartbeat_probe_Application_h_
#define _xmas_heartbeat_probe_Application_h_

#include <string>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/Serializer.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xmas/PulserSettings.h"
#include "xmas/exception/Exception.h"

#include "toolbox/task/AsynchronousEventDispatcher.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/mem/Pool.h"

#include "b2in/nub/Method.h"


#include "eventing/api/Member.h"


namespace xmas
{
	namespace heartbeat
	{
		namespace probe
		{
			class Application : public xdaq::Application, public xgi::framework::UIManager,  public xdata::ActionListener, public toolbox::task::TimerListener, public eventing::api::Member
			{

				public:

					XDAQ_INSTANTIATOR();

					Application (xdaq::ApplicationStub* s) ;
					~Application ();

					void actionPerformed (xdata::Event& e);

					void timeExpired (toolbox::task::TimerEvent& e);

					void Default (xgi::Input * in, xgi::Output * out) ;
					void browseSelection (xgi::Input * in, xgi::Output * out) ;

				protected:

					void StatisticsTabPage (xgi::Output * out);
					void TabPanel (xgi::Output * out);

				private:
					void heartbeat ();

					std::set<std::string> scanLocalServices ();


					xdata::String publishGroup_;
					xdata::String heartbeatGroup_;


					xdata::String heartbeatWatchdog_;

					xdata::String heartbeatdURL_;

					xdata::UnsignedInteger64 outgoingHeartbeatCounter_;
					xdata::UnsignedInteger64 outgoingHeartbeatLostCounter_;

					xdata::String outputBus_;

			};
		}
	}
}
#endif
