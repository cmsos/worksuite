// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xmas_heartbeat_version_h_
#define _xmas_heartbeat_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASHEARTBEAT_VERSION_MAJOR 2
#define WORKSUITE_XMASHEARTBEAT_VERSION_MINOR 6
#define WORKSUITE_XMASHEARTBEAT_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_XMASHEARTBEAT_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_XMASHEARTBEAT_PREVIOUS_VERSIONS "2.5.0"

//
// Template macros
//
#define WORKSUITE_XMASHEARTBEAT_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASHEARTBEAT_VERSION_MAJOR,WORKSUITE_XMASHEARTBEAT_VERSION_MINOR,WORKSUITE_XMASHEARTBEAT_VERSION_PATCH)
#ifndef WORKSUITE_XMASHEARTBEAT_PREVIOUS_VERSIONS
#define WORKSUITE_XMASHEARTBEAT_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASHEARTBEAT_VERSION_MAJOR,WORKSUITE_XMASHEARTBEAT_VERSION_MINOR,WORKSUITE_XMASHEARTBEAT_VERSION_PATCH)
#else 
#define WORKSUITE_XMASHEARTBEAT_FULL_VERSION_LIST  WORKSUITE_XMASHEARTBEAT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASHEARTBEAT_VERSION_MAJOR,WORKSUITE_XMASHEARTBEAT_VERSION_MINOR,WORKSUITE_XMASHEARTBEAT_VERSION_PATCH)
#endif 

namespace xmasheartbeat
{
	const std::string project = "worksuite";
	const std::string package  =  "xmasheartbeat";
	const std::string versions = WORKSUITE_XMASHEARTBEAT_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Penelope Roberts, Dainius Simelevicius";
	const std::string summary = "XDAQ Monitoring and Alarming System heartbeat server";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
