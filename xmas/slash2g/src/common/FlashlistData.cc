// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. OrsiniA. Petro						 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "xmas/slash2g/FlashlistData.h"
#include "xmas/slash2g/exception/Exception.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/regex.h"

#include "xdata/TimeVal.h"
#include "xdata/Table.h"
#include "xdata/TableAlgorithms.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

xmas::slash2g::FlashlistData::FlashlistData( std::set<std::string> & hashKey)
	: mutex_(toolbox::BSem::FULL,true)
{
	hashKey_ = hashKey;
	ref_ = 0;
	lastUpdate_ = toolbox::TimeVal::zero();
	table_ = 0;
	latencyDistribution_.resize(61);
}

xmas::slash2g::FlashlistData::~FlashlistData()
{

	if ( ref_ != 0 )
	{
		ref_->release();
	}
        ref_ = 0;
	if ( table_ != 0 )
	{
		delete table_;
	}

}

void xmas::slash2g::FlashlistData::lock()
{
	mutex_.take();
}

void xmas::slash2g::FlashlistData::unlock()
{
	mutex_.give();
}

bool xmas::slash2g::FlashlistData::hasData()
{
	bool retVal = false;
	mutex_.take();
	if ( ref_ != 0 )
	{
		retVal = true;
	}
	mutex_.give();
	return retVal;
}

//void xmas::slash2g::FlashlistData::setData(xdata::Table::Reference& data, xdata::Properties & plist)
void xmas::slash2g::FlashlistData::setData(toolbox::mem::Reference * ref, xdata::Properties & plist)
	 
{

	mutex_.take();
	lastUpdate_ = toolbox::TimeVal::gettimeofday();
	version_ = plist.getProperty("urn:xmas-flashlist:version");
	lastOriginator_ = plist.getProperty("urn:xmas-flashlist:originator");
	tags_ = plist.getProperty("urn:xmas-flashlist:tag");
	name_ = plist.getProperty("urn:xmas-flashlist:name");


	// Serialize received table
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) ref->getDataLocation(), ref->getDataSize());		

	xdata::Table * t = new xdata::Table();
	try
	{
		serializer_.import(t, &inBuffer);
	}
	catch (xdata::exception::Exception& e)
	{
		delete t;
		mutex_.give();
		XCEPT_RETHROW (xmas::slash2g::exception::Exception, "Failed to deserialize flashlist table", e);
	}
	catch(...)
	{
		delete t;
		mutex_.give();
		XCEPT_RAISE (xmas::slash2g::exception::Exception, "bad import");

	}

/* LO AP
	// add diagnostics column , current time
	t->addColumn("dflcurrenttime","time");
	t->addColumn("dflage","unsigned long");
	xdata::TimeVal dflcurrenttime;
	xdata::UnsignedLong dflage;
	dflage = 0;
	dflcurrenttime = toolbox::TimeVal::gettimeofday();
	for (size_t i = 0; i < t->getRowCount(); ++i)
	{
		 t->setValueAt(i, "dflcurrenttime", dflcurrenttime );
		 t->setValueAt(i, "dflage", dflage  );
	}
*/
	

	if ( table_ != 0 )
	{
		// check received table
		try
		{
			xdata::merge(table_, t, hashKey_);
		}
		catch (xdata::exception::Exception & e)
		{
			delete t;
			mutex_.give();
			std::stringstream msg;
			msg << "Failed to merge table for '" << name_  << "'";

			XCEPT_RETHROW (xmas::slash2g::exception::Exception, msg.str() ,e);
		}
		delete t;
	}
	else
	{
		table_ = t;
	}

	// release previous
	if ( ref_ != 0 )
		ref_->release();
	// get current
	ref_ = ref;
	mutex_.give();
}

xdata::Table::iterator xmas::slash2g::FlashlistData::find(xdata::Table * destination,  xdata::Table::Row & row, std::set<std::string> & key)
	
{
	if (destination == 0)
	{
		XCEPT_RAISE (xdata::exception::Exception, "Destination table is empty");
	}
			
	xdata::Table::iterator ti;
	for ( ti = destination->begin(); ti != destination->end(); ti++)
        {
		bool match = true;
		for (std::set<std::string>::iterator i = key.begin(); i != key.end(); ++i)
		{
			xdata::Serializable * s = row.getField(*i);
			xdata::Serializable * d = (*ti).getField(*i);
			if ( ! s->equals(*d) )
			{
				match = false;
				break;
			}
			
		}
		
		if ( match )
			return ti;
		
	}
	return 	destination->end();
	
}

void xmas::slash2g::FlashlistData::processStatistics()
{
/* LO AP
	std::cout << "process Statistics for " << name_ << std::endl;
	mutex_.take();
	for (size_t i = 0; i<= 60; i++)
	{
		latencyDistribution_[i] = 0;
	}
        for ( size_t j = 0; j <  table_->getRowCount(); j++ )
        {
                        xdata::TimeVal * dflcurrenttime = dynamic_cast<xdata::TimeVal*>(table_->getValueAt(j, "dflcurrenttime"));
			xdata::TimeVal currenttime;
        		currenttime = toolbox::TimeVal::gettimeofday();
			toolbox::TimeInterval age = currenttime - *dflcurrenttime; 
			std::cout << "currenttime is:" << currenttime.toString() << " dflcurrenttime was: " << dflcurrenttime->toString() << " age :" << age.toString() << std::endl;
			if ( age.sec() < 60 )
			{
				latencyDistribution_[age]++;
			}
			else
			{
				latencyDistribution_[60]++;
			}
        		xdata::UnsignedLong dflage;
			dflage = age.sec();
			table_->setValueAt(j,"dflage",dflage);
	}
	mutex_.give();
*/
		
}


/* LO AP
void xmas::slash2g::FlashlistData::mergeAndCheck(xdata::Table * destination,  xdata::Table * source, std::set<std::string> & key)
	
{
	if (destination == 0)
	{
		XCEPT_RAISE (xmas::slash2g::exception::Exception, "Destination table is empty");
	}
		
	if (source == 0) return;	

	
	
	std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >& dstColumnData = destination->columnData_;
	std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >& srcColumnData = source->columnData_;

	// check if the two tables have equal column definitions
	std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >::iterator i;
	for (i = dstColumnData.begin(); i != dstColumnData.end(); ++i)
	{
		if (srcColumnData.find( (*i).first ) == srcColumnData.end())
		{
			std::stringstream msg;
			msg << "Failed to find column '" << (*i).first << "' in source table. Merge failed";
			XCEPT_RAISE (xmas::slash2g::exception::Exception, msg.str());
		}
	}

	std::set<std::string>::iterator ki;
		
	for (size_t srci = 0; srci < source->numberOfRows_; ++srci)
	{
		bool merged = false;
		for (size_t dsti = 0; dsti < destination->numberOfRows_; ++dsti)
		{
			size_t matches = 0;
			for (ki = key.begin(); ki != key.end(); ++ki)
			{
				std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >::iterator dstKey = dstColumnData.find(*ki);
				if (dstKey == dstColumnData.end())
				{
					std::stringstream msg;
					msg << "Failed to merge tables, hashkey '" << (*ki) << "' not found in destination table";
					XCEPT_RAISE (xmas::slash2g::exception::Exception,msg.str());
				}
				xdata::Serializable* dstSerializable = (*dstKey).second->elementAt(dsti);
				
				std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >::iterator srcKey = srcColumnData.find(*ki);
				if (srcKey == srcColumnData.end())
				{
					std::stringstream msg;
					msg << "Failed to merge tables, hashkey '" << (*ki) << "' not found in source table";
					XCEPT_RAISE (xmas::slash2g::exception::Exception,msg.str());
				}
				xdata::Serializable* srcSerializable = (*srcKey).second->elementAt(srci);
				
				if (dstSerializable->equals(*srcSerializable))
				{
					++matches;				
				}
			}
			
			if (matches == key.size())
			{
				// if the timestamp has changed then is a new update
				// then proceed to merge		
				// otherwise no merge needed, entry is getting older
				xdata::TimeVal* previousTimestamp = dynamic_cast<xdata::TimeVal *>(dstColumnData["timestamp"]->elementAt(dsti));
				xdata::TimeVal* newTimestamp = dynamic_cast<xdata::TimeVal *>(srcColumnData["timestamp"]->elementAt(srci));
				//std::cout << " new time stamp: " << newTimestamp->toString() << std::endl;
				//std::cout << " previous time stamp: " << previousTimestamp->toString() << std::endl;
				if ( *previousTimestamp != *newTimestamp )
				{

					//std::cout <<  "they differ, therefore update" << std::endl;
					merged = true;
					// copy src into dst
					std::map<std::string, std::string, xdata::Table::ci_less >::iterator ci;
					for (ci = destination->columnDefinitions_.begin(); ci != destination->columnDefinitions_.end(); ++ci)
					{
						try
						{
							dstColumnData[(*ci).first]->elementAt(dsti)->setValue(*(srcColumnData[(*ci).first]->elementAt(srci)));
						}
						catch (xdata::exception::Exception& e)
						{
							std::stringstream msg;
							msg << "Failed to merge column definition '" << (*ci).first << "'";
							XCEPT_RETHROW (xmas::slash2g::exception::Exception, msg.str(), e);
						}
					}	
				}
			}
		}
		
		if (!merged)
		{
			// append the row of the src table
			std::map<std::string, std::string, xdata::Table::ci_less >::iterator ci;
			size_t row = destination->numberOfRows_;
			for (ci = destination->columnDefinitions_.begin(); ci != destination->columnDefinitions_.end(); ++ci)
			{
				//std::cout << "Insert, Number of rows in dst table: " << row << std::endl;
				try
				{
					destination->setValueAt(row, (*ci).first, *(srcColumnData[(*ci).first]->elementAt(srci)) );
				}
				catch (xdata::exception::Exception& e)
				{
					std::stringstream msg;
					msg << "Failed to merge column '" << (*ci).first << "' row " << row;
					XCEPT_RETHROW (xmas::slash2g::exception::Exception, msg.str(), e);
				}
			}
		}		
	}	
}
*/


size_t xmas::slash2g::FlashlistData::getSize()
{
	size_t size = 0;
	
	mutex_.take();
	if ( ref_ != 0 )
	{
		size = ref_->getDataSize();
	}
	mutex_.give();
	return size;
}


toolbox::TimeVal& xmas::slash2g::FlashlistData::getLastUpdate()
{
	return lastUpdate_;
}

std::string xmas::slash2g::FlashlistData::getVersion()
{
	return version_;
}

std::string xmas::slash2g::FlashlistData::getTags()
{
	return tags_;
}

std::string xmas::slash2g::FlashlistData::getLastOriginator()
{
	return lastOriginator_;
}

void xmas::slash2g::FlashlistData::toHTML(std::ostream& out, std::map<std::string, std::string>& filter)
	
{	
	/*
	out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	out << "<html>" << std::endl;
	
	// Link stylesheet for tab panes and scripts	
	out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tablesort.css\">" << std::endl;
	out << "<script type=\"text/javascript\" src=\"/xgi/html/tablesort.js\"></script>" << std::endl;
	// favicon
	out << "<script type=\"text/javascript\" src=\"/xgi/html/favicon/util.js\"></script>" << std::endl;
	out << "<script type=\"text/javascript\" src=\"/xgi/html/favicon/ajaxCaller.js\"></script>" << std::endl;
	out << "<script type=\"text/javascript\" src=\"/xgi/html/favicon/favicon.js\"></script>" << std::endl;
	
	out << "</head><body>" << std::endl;
	*/
	mutex_.take();

	if ( table_ != 0 )
	{
		this->displayTableToHTML ( out, table_ );
	}
	mutex_.give();
	
	//out << "</body></html>" << std::endl;
}

/*
void xmas::slash2g::FlashlistData::summaryToHTML( std::ostream& out)
{
	out << "<table border=\"0\" onclick=\"sortColumn(event)\" style=\"font-size: 8pt; font-family: arial; text-align: left; width: 100%;color: rgb(0,3,255); background-color: rgb(255,255,255);\">";	
	for (size_t i = 0; i <=60; i++ )
	{
		out << "<tr>";
		out << "<td title=\"" << latencyDistribution_[i]  << "\"style=\"vertical-align: top; font-weight: bold;\">" << localName << "</td>";
		out << "<td title=\"" << columns[i] << "\"style=\"vertical-align: top; font-weight: bold;\">" << localName << "</td>";
		out << "<tr>";
			
	}

}
*/
void xmas::slash2g::FlashlistData::displayTableToHTML( std::ostream& out, xdata::Serializable * s )
{
	xdata::Table * tref = dynamic_cast<xdata::Table*>(s);

	if (tref->getRowCount() == 0)
	{
		mutex_.give();
		return;
	}
	out << "<table class=\"xdaq-table\">";
	out << "<thead>";
	out << "<tr>";
	// By default it denormalize table
	
	
	std::vector<std::string> columns = tref->getColumns();
        std::sort(columns.begin(), columns.end());
	for (std::vector<std::string>::size_type i = 0; i < columns.size(); i++ )
	{
		std::string localName = columns[i].substr(columns[i].rfind(":")+1);
		//*out << cgicc::td(localName).set("title",columns[i]).set("style","vertical-align: top; font-weight: bold;");
		out << "<th title=\"" << columns[i] << "\" class=\"xdaq-sortable\">" << localName << "</th>";
	}
	out << "<tr>" << std::endl;
	out << "</thead>";
	
	out << "<tbody>";

	for ( size_t j = 0; j <  tref->getRowCount(); j++ )
	{
		out << "<tr>" << std::endl;
		for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
		{
			xdata::Serializable * s = tref->getValueAt(j, columns[k]);
			
			if (s->type() == "mime")
			{
				
				//*out << cgicc::td("MIME").set("style","vertical-align: top;");
				out << "<td>MIME</td>" ;
				//xdata::Mime* m = dynamic_cast<xdata::Mime*>(s);
				//std::cout << *(m->getEntity()) << std::endl;
			}
			else if (s->type() == "table")
			{
					out << "<td>";
					this->displayTableToHTML(  out, s); 
					out << "</td>" ;

			}
			else
			{						
				//*out << cgicc::td(s->toString()).set("style","vertical-align: top;");
				out << "<td style=\"vertical-align: top;\">" << s->toString() << "</td>" ;
			}
		}
		out << "</tr>";
	}
	out << "</tbody>";	
	out << "</table>";
}

void xmas::slash2g::FlashlistData::toEXDR(std::ostream& out, std::map<std::string, std::string>& filter)


{
	mutex_.take();

	xdata::Table *  t;
	try
	{
		t = this->getDataTable();
	}
	catch(xmas::slash2g::exception::Exception & e)
	{
		mutex_.give();
		XCEPT_RETHROW (xmas::slash2g::exception::Exception, "failed to convert binary data to table", e);
	}

	xdata::exdr::Serializer serializer;
	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;

	if (!filter.empty())
	{
		xdata::Table newTable(t->getTableDefinition());
		xdata::Table::iterator ti = t->begin();
		for ( size_t j = 0; j <  t->getRowCount(); j++, ti++ )
		{
			if ( this->match (t, j, filter ) )
			{
				newTable.insert( *ti );
			}
		}

		try
		{
			serializer.exportAll( &newTable, &outBuffer );
		}
		catch (xdata::exception::Exception& e)
		{
			mutex_.give();
			XCEPT_RETHROW (xmas::slash2g::exception::Exception, "Failed to serialize in exdr format", e);
		}
	}
	else
	{
		try
		{
			serializer.exportAll( t, &outBuffer );
		}
		catch (xdata::exception::Exception& e)
		{
			mutex_.give();
			XCEPT_RETHROW (xmas::slash2g::exception::Exception, "Failed to serialize in exdr format", e);
		}
	}

	out.write(outBuffer.getBuffer(),outBuffer.tellp());

	mutex_.give();
}

void xmas::slash2g::FlashlistData::toJSON(std::ostream& out, std::map<std::string, std::string>& filter)
	
{
	mutex_.take();

	xdata::Table *  t;
	try
	{
		t = this->getDataTable();
	}
	catch(xmas::slash2g::exception::Exception & e)
	{
		mutex_.give();
		XCEPT_RETHROW (xmas::slash2g::exception::Exception, "failed to convert data to JSON format", e);
	}
	
		
	out << "{\"table\":{";
	
	out << "\"properties\":{";
	out << "\"Name\":\"" << name_ << "\"," ;
	out << "\"LastUpdate\":\"" <<this->getLastUpdate().toString("%a, %b %d %Y %H:%M:%S GMT", toolbox::TimeVal::gmt) << "\",";
	out << "\"Version\":\"" << this->getVersion() << "\",";
	out << "\"Tag\":\"" << this->getTags() << "\",";
	out << "\"LastOriginator\":\"" << this->getLastOriginator() << "\",";
	out << "\"Rows\":" << t->getRowCount();
	out << "},";

	// definition
	out << "\"definition\":[";
		
	std::vector<std::string> columns = t->getColumns();
	std::sort(columns.begin(),columns.end());

	std::vector<std::string>::size_type i = 0;
	while(i < columns.size())
	{
		std::string localName = columns[i].substr(columns[i].rfind(":")+1);
		std::string localType =  t->getColumnType(columns[i]);
		out <<  "{\"key\":\"" << localName << "\"" << ",\"type\":\"" << localType<< "\"}";
		i++;
		if ( i < columns.size())
		{
			out << ",";
		}		
	}
	out << "],";
		
	// rows
	bool hasInserted = false;
	out << "\"rows\":[";
	size_t j = 0; 
	
	bool requireFilter = true;
	if (filter.empty())
	{
		requireFilter = false;
	}
	
	while ( j <  t->getRowCount() )
	{
		// Perform a match only if a filter is provided
		bool rowMatches = true;
		if (requireFilter)
		{
			rowMatches = this->match (t, j, filter );
		}
		
		if ( rowMatches )
                {  
			if ( hasInserted) // not done for the first row
			{
				out << ","; // next row
			}
			hasInserted = true;
			out << "{";
			std::vector<std::string>::size_type k = 0;
			while ( k < columns.size())
			{
				xdata::Serializable * s = t->getValueAt(j, columns[k]);
				if (s->type() == "mime")
				{
					out << "\"" << columns[k]  << "\":\"" << "MIME" << "\"";
				} 
				else if (s->type() == "time")
				{
					out << "\"" << columns[k]  << "\":\"" << dynamic_cast<xdata::TimeVal*>(s)->value_.toString("", toolbox::TimeVal::gmt) << "\"";

				} 
				else if (s->type() == "table")
				{					
					this->toJSON(out,dynamic_cast<xdata::Table*>(s), columns[k]); // recursive, but no filter anymore possible
				}
				else if (s->type() == "string")
				{	
					out << "\"" << columns[k]  << "\":\"" << toolbox::jsonquote(s->toString()) << "\"";
				}
				else if (s->type().find("vector") != std::string::npos )
				{
					xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(s);
					if ( v->getElementType() == "string" )
					{
						out << "\"" << columns[k]  << "\":[";
						size_t w = 0;
						while ( w < v->elements())
						{
							out << "\"" << toolbox::jsonquote(v->elementAt(w)->toString()) << "\"";
							w++;
							if ( w < v->elements() )
							{
								out << ","; // next element
							}	
						}
						out << "]";
					}
					else if ( v->getElementType() == "time" )
					{
						out << "\"" << columns[k]  << "\":[";
						size_t w = 0;
						while ( w < v->elements())
						{
							out << "\"" << dynamic_cast<xdata::TimeVal*>(v->elementAt(w))->value_.toString("", toolbox::TimeVal::gmt) << "\"";
							w++;
							if ( w < v->elements() )
							{
								out << ","; // next element
							}	
						}
						out << "]";
					}
					else
					{
						// Flex 3 decoder is currently not able to parse 'NaN' and 'infinite', so
						// temporarily we need to solve this by setting the value to 0
						//
						std::string value = s->toString();
						if ((value == "NaN") || (value == "infinite"))
						{
							out << "\"" << columns[k]  << "\":" << 0;
						}
						else
						{
							out << "\"" << columns[k]  << "\":" << value;
						}
					}
				}
				else
				{
					out << "\"" << columns[k]  << "\":" << toolbox::jsonquote(s->toString());
				}

				k++;
				if ( k < columns.size() )
				{
					out << ","; // next column
				}
			}
		out << "}";
		} // end of match for filter row
		j++;
	}
	out << "]}}";	
	
	mutex_.give();
}

void xmas::slash2g::FlashlistData::toJSON(std::ostream& out, xdata::Table* table, const std::string & tablename)
{
  	out << "\"" << tablename << "\":{";

	
	// definition
	out << "\"definition\":[";
		
	std::vector<std::string> columns = table->getColumns();
        std::sort(columns.begin(), columns.end());

	std::vector<std::string>::size_type i = 0;
	while(i < columns.size())
	{
		std::string localName = columns[i].substr(columns[i].rfind(":")+1);
		std::string localType =  table->getColumnType(columns[i]);
		out <<  "{\"key\":\"" << localName << "\"" << ",\"type\":\"" << localType<< "\"}";
		i++;
		if ( i < columns.size())
		{
			out << ",";
		}		
	}
	out << "],";
		
	// rows
	bool hasInserted = false;
	out << "\"rows\":[";
	size_t j = 0; 
	
	while ( j <  table->getRowCount() )
	{
			if ( hasInserted) // not done for the first row
			{
				out << ","; // next row
			}
			hasInserted = true;
			out << "{";
			std::vector<std::string>::size_type k = 0;
			while ( k < columns.size())
			{
				xdata::Serializable * s = table->getValueAt(j, columns[k]);
				if (s->type() == "mime")
				{
					out << "\"" << columns[k]  << "\":\"" << "MIME" << "\"";
				} 
				else if (s->type() == "time")
				{
					out << "\"" << columns[k]  << "\":\"" << dynamic_cast<xdata::TimeVal*>(s)->value_.toString("", toolbox::TimeVal::gmt) << "\"";
				} 
				else if (s->type() == "table")
				{					
					this->toJSON(out,dynamic_cast<xdata::Table*>(s),columns[k]); // recursive, but no filter anymore possible
				}
				else if (s->type() == "string")
				{	
					out << "\"" << columns[k]  << "\":\"" << toolbox::jsonquote(s->toString()) << "\"";
				}
				else if (s->type().find("vector") != std::string::npos )
				{
					xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(s);
					if ( v->getElementType() == "string" )
					{
						out << "\"" << columns[k]  << "\":[";
						size_t w = 0;
						while ( w < v->elements())
						{
							out << "\"" << toolbox::jsonquote(v->elementAt(w)->toString()) << "\"";
							w++;
							if ( w < v->elements() )
							{
								out << ","; // next element
							}	
						}
						out << "]";
					}
					else if ( v->getElementType() == "time" )
					{
						out << "\"" << columns[k]  << "\":[";
						size_t w = 0;
						while ( w < v->elements())
						{
							out << "\"" << dynamic_cast<xdata::TimeVal*>(v->elementAt(w))->value_.toString("", toolbox::TimeVal::gmt) << "\"";
							w++;
							if ( w < v->elements() )
							{
								out << ","; // next element
							}	
						}
						out << "]";
					}
					else
					{
						// Flex 3 decoder is currently not able to parse 'NaN' and 'infinite', so
						// temporarily we need to solve this by setting the value to 0
						//
						std::string value = s->toString();
						if ((value == "NaN") || (value == "infinite"))
						{
							out << "\"" << columns[k]  << "\":" << 0;
						}
						else
						{
							out << "\"" << columns[k]  << "\":" << value;
						}
					}
				}
				else
				{
					out << "\"" << columns[k]  << "\":" << toolbox::jsonquote(s->toString());
				}

				k++;
				if ( k < columns.size() )
				{
					out << ","; // next column
				}
			}
		out << "}";
		j++;
	}
	out << "]}";		
}

void xmas::slash2g::FlashlistData::toCSV(std::ostream& out, std::map<std::string, std::string>& filter)
	
{
	mutex_.take();
	
	xdata::Table * t;
	try
	{
		t = this->getDataTable();
	}
	catch(xmas::slash2g::exception::Exception & e)
	{
		mutex_.give();
		XCEPT_RETHROW (xmas::slash2g::exception::Exception, "failed to convert data to JSON format", e);
	}

	
	// Figure out if there is a special delimiter character
	std::map<std::string, std::string>::iterator d = filter.find("delimiter");
	
	std::string delimiter = ","; // default
	if (d != filter.end())
	{
		delimiter = (*d).second;
		
		// Now remove the delimiter from the map of filters in order
		// not to disrupt real data filtering
		filter.erase(d);
	}

	std::vector<std::string> columns = t->getColumns();
	std::sort(columns.begin(), columns.end());
	
	for (std::vector<std::string>::size_type i = 0; i < columns.size(); i++ )
	{
		if (i != 0) 
		{
			out << delimiter;
		}
		out << columns[i];
	}

	out << std::endl;

	for ( size_t j = 0; j < t->getRowCount(); ++j )
	{
		if ( this->match (t, j, filter ) )
		{		
			for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
			{
				if (k != 0) 
				{
					out << delimiter;
				}
				xdata::Serializable * s = t->getValueAt(j, columns[k]);
				if (s != 0)
				{
					out << '"' << s->toString() << '"';
				}
				else
				{
					out << "null";
				}
			}
			out << std::endl;
		}
	}
	
	mutex_.give();
}


void xmas::slash2g::FlashlistData::reset()
{
	mutex_.take();
	lastUpdate_ = toolbox::TimeVal::zero();
	if ( ref_ != 0 )
	{
		ref_->release();
	}
	ref_ = 0;
	if ( table_ != 0 )
	{
		delete table_;
	}
	table_ = 0;
	mutex_.give();
}

bool xmas::slash2g::FlashlistData::match
(
        xdata::Table * t,
        size_t row,
        std::map<std::string, std::string>& filter
)
{
        // Algorithm requires that all filter expressions match!
        //
        std::map<std::string, std::string>::iterator i;
        for (i = filter.begin(); i != filter.end(); ++i)
        {
                try
                {
                        xdata::Serializable * s = t->getValueAt(row, (*i).first);
                        //if (s->toString().find((*i).second) == std::string::npos)
                        if ( ! toolbox::regx_match_nocase(s->toString(), (*i).second ) )
                        {
                                return false;
                        }
                }
                catch (xdata::exception::Exception& e)
                {
                        // doesn't match the column name
                        return false;
                }
        }
        return true;
}

xdata::Table *   xmas::slash2g::FlashlistData::getDataTable()
        
{
	if ( ref_ != 0 )
        {
		return table_; // cached table
	}
        else
        {
                XCEPT_RAISE(xmas::slash2g::exception::Exception, "No data available");

        }
/* LO AP
        if ( ref_ != 0 )
        {
                if ( lastSerialized_ > lastUpdate_ )
                {
                        return table_; // cached table
                }

                xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) ref_->getDataLocation(), ref_->getDataSize());

                xdata::Table * t = new xdata::Table();
                try
                {
                        serializer_.import(t, &inBuffer);
                        if ( table_ != 0 )
                        {
                                delete table_;
                        }
                        table_ = t; // new update cache table
                        lastSerialized_ = toolbox::TimeVal::gettimeofday();

                        return t;
                }
                catch (xdata::exception::Exception& e)
                {
                        delete t;
                        XCEPT_RETHROW (xmas::las2g::exception::Exception, "Failed to deserialize flashlist table", e);
                }
        }
        else
        {
                        XCEPT_RAISE(xmas::las2g::exception::Exception, "No data available");

        }
*/

}


