import urllib.request
import urllib.error
import json
import os
import glob
import datetime
from threading import Thread
import traceback
import time

class CollectingThread(Thread):
  def __init__(self, serverurl, outputdir, formats):
    #Execute the base constructor
    Thread.__init__(self)
    #Store arguments
    self._serverurl = serverurl
    self._outputdir = outputdir
    self._formats = formats
    self._dynamiccatalog = None
    self._staticcatalog = None

  def run(self):
    self._exc = None
    try:
      self._dynamiccatalog = self.getcatalog(self._serverurl, False)
      self._staticcatalog = self.getcatalog(self._serverurl, True)
      self.downloadflashlists(self._serverurl, self._outputdir, self._formats, self._dynamiccatalog)
    except Exception as e:
      self._exc = e

  def downloadflashlists(self, serverurl, outputdir, formats, catalog):
    urls = self.createurllist(serverurl, outputdir, formats, catalog)
    for url in urls:
      self.downloadflashlist(url["url"], outputdir, url["flashlist"])

  def createurllist(self, serverurl, outputdir, formats, catalog):
    prefix = "urn:xdaq-flashlist:"
    urls = []
    for row in catalog["table"]["rows"]:
      flashlist = row["Name"]
      flashlistname = flashlist[len(prefix):]
      for format in formats:
        flashlisturl = "http://" + serverurl + "/urn:xdaq-application:service=xmaslas2g/retrieveCollection?" + \
        "fmt=" + format["format"] + \
        "&flash=" + flashlist + \
        format["option"]
        flashlistfile = flashlistname + "." + format["format"]
        urls.append({"url": flashlisturl, "flashlist": flashlistfile})
    return urls

  def downloadflashlist(self, url, outputdir, filename):
    filepath = outputdir + filename
    try:
      response = urllib.request.urlopen(url, timeout=30)
      flashlistdata = response.read()
      with open(filepath + ".tmp", "wb") as file:
        file.write(flashlistdata)
      try:
        os.replace(filepath + ".tmp", filepath)
      except OSError as e:
        pass # Expecting file replace to fail when xmas-admin is clearing files
    except urllib.error.HTTPError as e:
      raise Exception("Failed to download flashlist from " + url) from e
    except urllib.error.URLError as e:
      raise Exception("Failed to download flashlist from " + url) from e

  def getcatalog(self, slash, static):
    if static:
      url = "http://" + slash + "/urn:xdaq-application:service=xmaslas2g/retrieveStaticCatalog?fmt=json"
    else:
      url = "http://" + slash + "/urn:xdaq-application:service=xmaslas2g/retrieveCatalog?fmt=json"
    try:
      response = urllib.request.urlopen(url, timeout=30)
    except urllib.error.HTTPError as e:
      raise Exception("Failed to download catalog from " + url) from e
    except urllib.error.URLError as e:
      raise Exception("Failed to download catalog from " + url) from e
    else:
      data = json.loads(response.read().decode())
    return data

def collect(servers, outputdir, formats, maxerrorcount = 30, sleeptime = 5):
  #Creating directory to store flashlist data
  os.makedirs(outputdir, exist_ok = True)

  #Removing all files from outputdir
  files = glob.glob(os.path.join(outputdir, "*"))
  for f in files:
    try:
      os.remove(f)
    except OSError as e:
      print("Error occurred while deleting a file: " + f, flush=True)

  #Main loop
  errorcount = 0
  while True:
    # start = datetime.datetime.now()

    #Shows if exception(s) occurred in this iteration
    exceptionoccurred = False

    #Creating thread per server to download flashlist data
    threads = []
    for server in servers:
      serverurl = server["url"]
      thread = CollectingThread(serverurl, outputdir, formats)
      thread.start()
      threads.append(thread)

    #Joining threads (monitoring data was downloaded)
    for thread in threads:
      thread.join()

    #Checking if errors occurred inside threads
    dynamiccatalogs = []
    staticcatalogs = []
    for thread in threads:
      exception = thread._exc
      if not exception:
        dynamiccatalogs.append(thread._dynamiccatalog)
        staticcatalogs.append(thread._staticcatalog)
      else:
        print("An error occurred while downloading flashlists.", flush=True)
        traceback.print_exception(Exception, exception, None)
        print(flush=True)
        exceptionoccurred = True
        break

    if exceptionoccurred:
      errorcount = errorcount + 1
      if errorcount > maxerrorcount:
        print("Too many exceptions. Exiting.", flush=True)
        exit(1)
      #Sleeping if exceptions occurred during this iteration
      print("Sleeping for " + str(sleeptime) + " seconds to limit log flooding.", flush=True)
      time.sleep(sleeptime)
    else:
      #Creating joined catalogs
      createjoinedcatalogs(dynamiccatalogs, outputdir, formats, "catalog")
      createjoinedcatalogs(staticcatalogs, outputdir, formats, "staticcatalog")
      #Resetting error counter since one loop iteration was successful
      errorcount = 0

    time.sleep(1)

    # end = datetime.datetime.now()
    # duration = end - start
    # print("Time of downloading of all flashlists from all servers: " + str(duration))
    # print

def createjoinedcatalogs(catalogs, outputdir, formats, name):
  for format in formats:
    createjoinedcatalog(catalogs, outputdir, format["format"], name)

def createjoinedcatalog(catalogs, outputdir, format, name):
  if format == "json":
    createjsoncatalog(catalogs, outputdir, name)
  elif format == "csv":
    createcsvcatalog(catalogs, outputdir, name)

def createjsoncatalog(catalogs, outputdir, name):
  if len(catalogs) > 0:
    #Taking the json document of the first catalog to have a structure for a joined catalog
    joinedjsoncatalog = catalogs[0]
    #Starting to iterate from the second catalog, because the first is already included
    for catalog in catalogs[1:]:
      for row in catalog["table"]["rows"]:
        joinedjsoncatalog["table"]["rows"].append(row)
    jsoncatalogfilename = outputdir + name + ".json"
    with open(jsoncatalogfilename + ".tmp", "w") as catalogfile:
      json.dump(joinedjsoncatalog, catalogfile)
    try:
      os.replace(jsoncatalogfilename + ".tmp", jsoncatalogfilename)
    except OSError as e:
      pass # Expecting file replace to fail when xmas-admin is clearing files

def createcsvcatalog(catalogs, outputdir, name):
  flashlists = []
  for catalog in catalogs:
    for row in catalog["table"]["rows"]:
      flashlist = row["Name"]
      flashlists.append(flashlist)
  joinedcsvcatalog = "Name\r\n"
  for flashlist in flashlists:
    joinedcsvcatalog += flashlist + "\r\n"
  csvcatalogfilename = outputdir + name + ".csv"
  with open(csvcatalogfilename + ".tmp", "w") as catalogfile:
    catalogfile.write(joinedcsvcatalog)
  try:
    os.replace(csvcatalogfilename + ".tmp", csvcatalogfilename)
  except OSError as e:
    pass # Expecting file replace to fail when xmas-admin is clearing files

#References:
#https://docs.python.org/3/library/os.html
#https://man7.org/linux/man-pages/man2/rename.2.html
#https://man7.org/linux/man-pages/man1/mv.1p.html
#https://stackoverflow.com/questions/8858008/how-to-move-a-file-in-python
#https://stackoverflow.com/questions/21405095/in-python-how-to-make-atomic-rewrite-of-a-file-without-renaming-it
#https://stackoverflow.com/questions/29261648/atomic-writing-to-file-on-linux
#https://www.youtube.com/watch?v=-9eXCb3yvyY
