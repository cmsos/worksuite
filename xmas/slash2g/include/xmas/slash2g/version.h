/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xmas_slash2g_version_h_
#define _xmas_slash2g_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASSLASH2G_VERSION_MAJOR 2
#define WORKSUITE_XMASSLASH2G_VERSION_MINOR 11
#define WORKSUITE_XMASSLASH2G_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_XMASSLASH2G_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_XMASSLASH2G_PREVIOUS_VERSIONS ""


//
// Template macros
//
#define WORKSUITE_XMASSLASH2G_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASSLASH2G_VERSION_MAJOR,WORKSUITE_XMASLAS2_VERSION_MINOR,WORKSUITE_XMASSLASH2G_VERSION_PATCH)
#ifndef WORKSUITE_XMASSLASH2G_PREVIOUS_VERSIONS
#define WORKSUITE_XMASSLASH2G_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASSLASH2G_VERSION_MAJOR,WORKSUITE_XMASSLASH2G_VERSION_MINOR,WORKSUITE_XMASSLASH2G_VERSION_PATCH)
#else 
#define WORKSUITE_XMASSLASH2G_FULL_VERSION_LIST  WORKSUITE_XMASSLASH2G_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASSLASH2G_VERSION_MAJOR,WORKSUITE_XMASSLASH2G_VERSION_MINOR,WORKSUITE_XMASSLASH2G_VERSION_PATCH)
#endif 

namespace xmasslash2g
{
	const std::string project = "worksuite";
	const std::string package  =  "xmasslash2g";
	const std::string versions = WORKSUITE_XMASSLASH2G_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Roland Moser, Luciano Orsini";
	const std::string summary = "XDAQ Monitoring and Alarming System Smart Life Access Server Hub for B2IN";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

