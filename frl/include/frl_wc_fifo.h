/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: frl_wc_fifo.h,v 1.1 2004/07/05 09:35:09 cschwick Exp $
*/
#ifndef _FRL_WC_FIFO_H_
#define _FRL_WC_FIFO_H_

static char *rcsid_wc_fifo_h = "@(#) $Id: frl_wc_fifo.h,v 1.1 2004/07/05 09:35:09 cschwick Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_wc_fifo_h);
#endif
/**
 * @function _frl_wc_fifo_free_slot_count
 * @return the number of free slots in the word count FIFO
 */
inline int _frl_wc_fifo_free_slot_count (struct _FRL_wc_fifo * wc_fifo)
{
	int wr = wc_fifo->write & _FRL_wc_fifo_mask; /* read this value once as it can change anytime */
	int rd = wc_fifo->read & _FRL_wc_fifo_mask;
	
	int used = (wr - rd) & _FRL_wc_fifo_mask;
	
	if (used > _FRL_wc_fifo_depth) {
		eprintf ("ERROR in wc_fifo_free_slot_count : got more than _FRL_wc_fifo_depth used pointers in wc FIFO\n"
                 "wr=0x%x, rd=0x%x, used=0x%0x\n", wr, rd, used);
		return 0;
	}
	
	return _FRL_wc_fifo_depth - used;
}

/**
 * @function _frl_wc_fifo_used_slot_count
 * @return the number of free slots in the word count FIFO
 */
inline int _frl_wc_fifo_used_slot_count (struct _FRL_wc_fifo * wc_fifo)
{
	int wr = wc_fifo->write & _FRL_wc_fifo_mask; /* read this value once as it can change anytime */
	int rd = wc_fifo->read & _FRL_wc_fifo_mask;
	
	int used = (wr - rd) & _FRL_wc_fifo_mask;
	
	return used;
}

/**
 * @function _frl_send_wc_fifo_params_to_board 
 * this function retrives the physical addresses of the Dbuff used to implement
 * the wordcount fifo and writes base address of the fifo and address of the 
 * read counter in the right registers of the board. the board can the use the FIFO.
 */
inline void _frl_send_wc_fifo_params_to_board (struct frl_receiver * rec)
{

  struct _FRL_wc_fifo * phy_wc_fifo = (struct _FRL_wc_fifo *)rec->wc_fifo_physical;
    
    idprintf ("Sending wc params to board : wc fifo_elements[0]=0x%08x, write=0x%08x, phy_wc_fifo=0x%08x\n",
              (U32)&(phy_wc_fifo->fifo_elements[0]), (U32) &(phy_wc_fifo->write), (U32)phy_wc_fifo);
    rec->map[_FRL_WCFADDR_OFFSET/4] = (U32) &(phy_wc_fifo->fifo_elements[0]);
    rec->map[_FRL_WCFWADDR_OFFSET/4] = (U32) &(phy_wc_fifo->write);
    /* rec->map[_FRL_WCFR_OFFSET/4] = rec->wc_fifo->read; no readpointer in FRL */
}

/**
 *	@function _frl_pop_wc (struct frl_receiver * rec)
 * @return word count for the next event
 * the function pops one word count from the word count FIFO.
 * this includes updating the registers in the board
 */
 
inline struct _FRL_wc_fifo_element _frl_pop_wc (struct frl_receiver * rec)
{
	struct _FRL_wc_fifo_element ret={~0,1,1,1};
	#ifndef NO_FRL_PTHREAD
	pthread_mutex_lock (&(rec->global_mutex));
	#endif
	if (_frl_wc_fifo_used_slot_count (rec->wc_fifo) > 0) {
		rec->wc_fifo->read  = (rec->wc_fifo->read + 1) & _FRL_wc_fifo_mask; 
		ret = rec->wc_fifo->fifo_elements[rec->wc_fifo->read & _FRL_wc_fifo_address_mask];
                ret.no_fragment = 0;
		idprintf ("In _frl_pop_wc new read = %d\n", rec->wc_fifo->read);
		/*rec->map[_FRL_WCFR_OFFSET/4] = rec->wc_fifo->read; NOT FOR THE FRL !*/
	}
	/*idprintf ("in _frl_pop_wc ret=%d\n", ret);*/
	#ifndef NO_FRL_PTHREAD
	pthread_mutex_unlock (&(rec->global_mutex));
	#endif

	return ret;
}

#endif /* ndef _FRL_WC_FIFO_H_ */
