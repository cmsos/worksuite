#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "frl.h"
#include "frl-types.h"
#include <iostream>

using namespace std;

void checkStat ( int status );

int main( int argc, char** argv ) {

    int index = 2;
    
    cout << "index " << index << endl;

    int status = 0;
    //  printf("in main\n");
    //  int file = open ( "/dev/frl0" , O_RDONLY);
    //  //  int file2 = open ( "/dev/frl1" , O_RDONLY);
    //  if ( ! file ) {
    //    printf("device file could not be opened\n Bye\n");
    //    return -1 ;
    //  }
    //  printf( "opened device frl0 \n ");

    struct frl_receiver *frlrec;
    frlrec = frl_open( index, &status);
    checkStat( status );
    status = frl_set_block_size (frlrec, 4096);
    checkStat( status );
    status = frl_set_header_size( frlrec, 0x40 );
    checkStat( status );
    status = frl_set_block_number (frlrec, 16);
    checkStat( status );
    status = frl_set_receive_pattern( frlrec, 1 );
    checkStat( status );

    // other calls
    // fedkit_provide_block    : if started with noalloc option
    // frag = fedkit_frag_get (receivers_[currentport_], 0);   ; get a fragment
    // fedkit_frag : fragment handle routines
    // fedkit_frag_release(_norecycle) : give back a block : norecycle if sart_noalloc

    bool loop = true;
    unsigned long option;
  
    //============== loop here=====================//
  
    while ( loop ) {

        cout << endl;
        cout << "0) end the program" << endl;
        cout << "1) enable local daq" << endl;
        cout << "2) frl_start" << endl;
        cout << "Enter option : ";
        cin >> option;
    
        switch (option) {

        case 0:
            loop = false;
            break;
        case 1:
            frl_set_localdaq( frlrec );
            break;
        case 2:
            status = frl_start( frlrec );
            checkStat( status );
            break;
        default:
            cout << "You typed an invalid option number!" << endl;
            break;

        }

    }
  
    cout << "Closing the frl receiver" << endl;
    frl_close( frlrec );
  
    return 0;
}

void checkStat ( int status ) {
    const char* txt = frl_get_error_string( status );
    printf ("Status : %s\n", txt);
    return;
}

