#ifndef __AXILinuxDeviceIdentifier
#define __AXILinuxDeviceIdentifier

#include <string>
#include "hal/AXIDeviceIdentifier.hh"
#include "hal/BusAdapterException.hh"

namespace HAL {

/**
*
*
*     @short The DeviceIdentifier for the AXILinuxBusAdapter.
*            
*            The  class   contains  data   which  is  needed   by  the 
*            AXILinuxBusAdapter to perform configuration space accesses.
*            In addition  it keeps pointers to the  address maps which 
*            the  AXIBusAdapter  created  for memory  mapped  hardware 
*            accesss.  They are  used when 
*            unregistering  the   device  in  order   to  destroy  the 
*            mapping. 
*
*       @see AXILinuxBusAdapter
*    @author Christoph Schwick
* $Revision: 1.1 $
*     $Date: 2007/03/05 17:54:12 $
*
*
**/

class AXILinuxDeviceIdentifier : public AXIDeviceIdentifier {
public :

  AXILinuxDeviceIdentifier( bool swapFlag = false );

  /**
   * Destruction of the Identifier and the maps.
   * The memory maps which are related to the hardware device identified
   * by this identifier are destroyed by this desructor.
   */
  virtual ~AXILinuxDeviceIdentifier();

  /**
   * Returns an internal state variable, set in the constructor.
   */
  bool doSwap() const;

  std::string printString() const;


private:
  bool swapFlag;
};

} /* namespace HAL */

#endif /* __AXILinuxDeviceIdentifier */
