#include "hal/AXILinuxDeviceIdentifier.hh"


HAL::AXILinuxDeviceIdentifier::AXILinuxDeviceIdentifier( bool swapFlag )
  : swapFlag(swapFlag) { 
}

HAL::AXILinuxDeviceIdentifier::~AXILinuxDeviceIdentifier( ) {
}

bool HAL::AXILinuxDeviceIdentifier::doSwap() const {
  return swapFlag;
}

std::string HAL::AXILinuxDeviceIdentifier::printString() const {
  return "nothing to print in HAL::AXILinuxDeviceIdentifier::printString()";
}
