#ifndef __WebServer
#define __WebServer

#include "log4cplus/logger.h"
#include "d2s/utils/Monitor.hh"
#include "cgicc/HTMLClasses.h"
#include "xgi/Method.h"
#include "toolbox/lang/Class.h"
#include "d2s/utils/WebTabIF.hh"
#include "d2s/utils/WebTableTab.hh"


namespace utils
{
    class WebServer
    {
    public:

        static std::string jsonEscape( std::string );

        WebServer( utils::Monitor &monitor, std::string url, std::string urn, Logger logger );

        void monitoringWebPage( xgi::Input *in, xgi::Output *out );

        // for the Ajax update callback of the monitoring pages.
        void jsonUpdate( xgi::Input *in, xgi::Output *out );

        // return all infospaces in json format
        void jsonInfoSpaces( xgi::Input *in, xgi::Output *out );

        utils::WebTableTab* registerTableTab( std::string name, 
                                              std::string description,
                                              uint32_t columns);

        void registerTab( WebTabIF *webtabptr );

    public:
        // print all the Tabs on the page (iterate through them)
        virtual void printTabs( xgi::Output * out );
        
        // print table header pf tje web page
        virtual void printHeader( xgi::Output * out );
        // print the body of the webpage (calls essentially printTabs)
        virtual void printBody( xgi::Output * out );
        // print the footer of the page
        virtual void printFooter( xgi::Output * out );

        WebTabIF* getTab(std::string name);

        void setCSS(std::string css);
        std::string getCSS();

    protected:
        std::string url_;
        std::string urn_;
        Logger logger_;

        
        // for Ajax: return a set of monitoring items in json
        //        std::string jsonTable( const webtable &table );
        //prints an html table of a set of monitoring items
        //        std::string htmlTable( const webtable &table );

        utils::Monitor &monitor_;

        std::list< WebTabIF* > tabList_;
        std::string css_;
    };
}
#endif /* __WebServer */
