#ifndef __WebTable
#define __WebTable

#include <string>
#include <iostream>
#include "d2s/utils/Monitor.hh"

namespace utils
{
    class WebTable {

    public:
        WebTable( std::string name,
                  std::string description,
                  std::string itemSetName,
                  utils::Monitor &monitor );

        void print( std::ostream *out, bool isAlt = false );
        void jsonUpdate( std::ostream *out );
        std::string getItemSetName();
        
        //This function is here, rather than just being handled in
        //the monitor, so that the level can be set to one of the ones
        //available in the dropdown box. This avoids annoyances when
        //switching from global to local level control. 
        void setItemSetLevel(uint32_t level);

        bool isLocalMonitoring();
        void isLocalMonitoring(bool localMon);
        
    private:
        std::string htmlEscape( std::string orig ) const;
        std::string name_;
        std::string description_;
        std::string itemSetName_;
        utils::Monitor &monitor_;
        std::set<uint32_t> levels_;
        bool localMon_;
    };
}

#endif /* __WebTable */
