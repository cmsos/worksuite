
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: Jonathan Fulcher, Luciano Orsini, Chrstoph Schwic, Dainius   * 
 *  Simeleviciusk					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _d2s_utils_version_h_
#define _d2s_utils_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_D2SUTILS_VERSION_MAJOR 1
#define WORKSUITE_D2SUTILS_VERSION_MINOR 0
#define WORKSUITE_D2SUTILS_VERSION_PATCH 0
#undef WORKSUITE_D2SUTILS_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_D2SUTILS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_D2SUTILS_VERSION_MAJOR,WORKSUITE_D2SUTILS_VERSION_MINOR,WORKSUITE_D2SUTILS_VERSION_PATCH)
#ifndef WORKSUITE_UTILS_PREVIOUS_VERSIONS
#define WORKSUITE_D2SUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_D2SUTILS_VERSION_MAJOR,WORKSUITE_D2SUTILS_VERSION_MINOR,WORKSUITE_D2SUTILS_VERSION_PATCH)
#else 
#define WORKSUITE_D2SUTILS_FULL_VERSION_LIST  WORKSUITE_D2SUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_D2SUTILS_VERSION_MAJOR,WORKSUITE_D2SUTILS_VERSION_MINOR,WORKSUITE_D2SUTILS_VERSION_PATCH)
#endif 
namespace d2sutils
{
	const std::string project = "worksuite";
	const std::string package  =  "d2sutils";
	const std::string versions = WORKSUITE_D2SUTILS_FULL_VERSION_LIST;
	const std::string description = "D2S Utilities";
	const std::string authors = "Jonathan Fulcher, Christoph Schwick, Luciano Orsini, Dainius Simelevicius";
	const std::string summary = "Contains useful utilities to be shared by hardware controller applications such as ferol, amc13controller, ferol40.";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
