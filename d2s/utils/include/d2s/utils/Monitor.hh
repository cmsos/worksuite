#ifndef __Monitor
#define __Monitor

#include <list>
#include <vector>
#include <sstream>
#include <tr1/unordered_map>

#include "log4cplus/logger.h"
#include "xdaq/Application.h"
#include "toolbox/lang/Class.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "d2s/utils/InfoSpaceHandler.hh"
//#include "amc13controller/ApplicationInfoSpaceHandler.hh"
#include "d2s/utils/ApplicationStateMachineIF.hh"

//#include "amc13controller/MonitoringUpdater.hh"

namespace utils
{
    class Monitor : public toolbox::lang::Class
    {
    public: 

        typedef struct {
            std::string name;
            utils::InfoSpaceHandler *is;
            std::string format;
            int32_t streamNo; // -1 means not a "stream" item
        } Item;

        /**
         *
         *     @short Keeps track of used Infospaces.
         *            
         *            This  function can  be  called in  order  to register  an 
         *            Infospace for tracking by this Monitor class. 
         *            
         *            When an  item is added  with the addItem  call Infospaces 
         *            are  also automatically  added  to the  internal list  of 
         *            used InfoSpaces.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void addInfoSpace( utils::InfoSpaceHandler *is );


        /**
         *
         *     @short Defines a new set of Items to be montored on hyperdaq page
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void newItemSet( const std::string setName, const uint32_t level=1 );
        void setItemSetLevel( const std::string setName, const uint32_t level );
        uint16_t getItemSetLevel(const std::string setName);

        void addItem( const std::string setName, 
                      const std::string itemName, 
                      utils::InfoSpaceHandler *is,
                      const std::string format = "", 
                      int32_t streamNo = -1 );

        std::list< std::vector<std::string> > getFormattedItemSet( std::string setName );

        std::string dumpInfoSpaces();

        std::list< std::pair<std::string, Item> > getItems(std::string set);
        
        /**
         *
         *     @short Starts the monitoring.
         *            
         *            After the monitoring is started nobody should add
         *            infospaces or  itemsets or items  anymore.  The
         *            data structures holding * infospaces and itemsets
         *            are not threadsafe.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void startMonitoring();

        
        /**
         *
         *     @short Do one round of updating of monitoring information.
         *            
         *            This  function loops  through  all InfospaceHandlers  and 
         *            calls  the  updateInfoSpace  function on  all  registered 
         *            handlers  for these  Infospaces. This  routing  is useful 
         *            to be  public since  it can be  for exampled  called just 
         *            before the  hardware devices are shut down  in order that 
         *            the  monitoring data  reflect the  state of  the hardware 
         *            just  before the  shutdown (i.e.  connections  shut down, 
         *            links not enabled, etc...)
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void updateAllInfospaces();

        const std::tr1::unordered_map<std::string, utils::InfoSpaceHandler *> & getInfoSpaceMap();
    protected:

        Monitor( Logger logger, utils::InfoSpaceHandler &appIS,  xdaq::Application* xdaq, ApplicationStateMachineIF &fsm, std::string workloopID = "FerolMonitoringThread_");

        utils::InfoSpaceHandler *getInfoSpaceHandler( std::string );

    private:

        bool monitoringThread(toolbox::task::WorkLoop*);

    private:
        

        Logger logger_;
        utils::InfoSpaceHandler &appIS_;
        ApplicationStateMachineIF &fsm_;

        std::tr1::unordered_map< std::string, 
                                 std::list< std::pair< std::string, Item > > > itemSets_;
        std::tr1::unordered_map< std::string, uint16_t > setLevels_;

        // this list is maintained in order to update all infospaces at one time.
        std::tr1::unordered_map< std::string,utils::InfoSpaceHandler * > infoSpaceMap_;

        toolbox::task::WorkLoop *monitoringThread_;
        bool stopMonitoring_;
        bool monitoringActive_;
        uint32_t monLoopCnt_;

    };
}
#endif /* __Monitor */
