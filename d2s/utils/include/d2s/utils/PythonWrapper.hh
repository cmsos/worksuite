#ifndef _PythonWrapper_
#define _PythonWrapper_
//#include "d2s/utils/Exception.hh"


namespace utils {
    class PythonWrapper {
        public:
            PythonWrapper () {}
            ~PythonWrapper () {}
            char const * greet() {
                return "hello, world";
            }
    };
}


#endif
