#include "d2s/utils/HardwareDebugItem.hh"

utils::HardwareDebugItem::HardwareDebugItem( std::string item,
                                             std::string description,
                                             std::string adrStr,
                                             std::string valStr,
                                             uint32_t address,
                                             uint32_t value )
    : item(item),
      description(description),
      adrStr(adrStr),
      valStr(valStr), 
      address(address),
      value(value)
{ };
