// $Id: FEDEmulatorCard.cc,v 1.11 2008/07/22 13:01:46 cschwick Exp $

#include "d2s/fedemulator/FEDEmulatorCard.hh"
#include "toolbox/math/random.h"

#include <fstream>
#include "log4cplus/loggingmacros.h"
/**
 * This constructor opens the device by Bus Location (pcibus) 
 *
 */
d2s::FEDEmulatorCard::FEDEmulatorCard( HAL::PCIAddressTable& fedEmulatorAddressTable,
				       HAL::PCIBusAdapterInterface& busAdapter,
				       const HAL::PCIDevice::PCILocationIdentifier busLocation,
				       Logger logger)
  : PCIDevice( fedEmulatorAddressTable , 
	       busAdapter,
	       busLocation,
	       false), 
    logger_(logger) {
    LOG4CPLUS_DEBUG (logger_,"constructed FEDEmulatorCard in pci slot " << std::hex << busLocation.busID << ":" <<  busLocation.slotID << std::dec );
  }

/**
 * This constructor opens the device by pciIndex 
 *
 */
d2s::FEDEmulatorCard::FEDEmulatorCard( HAL::PCIAddressTable& fedEmulatorAddressTable,
				       HAL::PCIBusAdapterInterface& busAdapter,
				       uint32_t pciIndex,
				       Logger logger)
  : PCIDevice( fedEmulatorAddressTable , 
	       busAdapter,
	       FEDEMULATOR_VENDORID,
	       FEDEMULATOR_DEVICEID,
	       pciIndex,
	       false), 
    logger_(logger) {

}


d2s::FEDEmulatorCard::~FEDEmulatorCard( ) {
}


/**
 * The event size includes header and trailer and therefore 
 * must be larger than 16
 */

void d2s::FEDEmulatorCard::setFixedEventSize( uint32_t eventSize,
					      uint32_t source ,
					      uint32_t deltaT ) {
  
  LOG4CPLUS_DEBUG (logger_," setfixedsize " <<  eventSize );
  if ( eventSize < 24 ) {
    LOG4CPLUS_WARN (logger_, " eventSize must be at least 24 (SLINK header and trailer + 1 data word)\n Forcing event size to 24.");
    eventSize = 24;
  }
  
  if ( eventSize % 8 != 0 ) {
    LOG4CPLUS_WARN ( logger_,  "eventSize not 8 byte aligned (hex): " 
		     << std::hex << eventSize << std::dec << ". Masking 0x7");
    eventSize &= ~0x7;
  }
  
  this->resetBit( "eventPreload" );
  this->write( "sdramMask" , 0 );
  this->setBit( "eventCounterOn" );
  this->write( "eventLength", eventSize);
  this->write( "source", source );
  this->write( "seed", FEDEMULATOR_SEED  );
  this->write( "bx", 1 );
  this->write( "deltaT", deltaT  );
  this->write( "eventId", 0x7  );
  this->write( "errorEvId", 0 ); 
  this->write( "errorCRC", 0 ); 
  
}

void d2s::FEDEmulatorCard::setLogNormalEventSize( uint32_t mean,
					     uint32_t source,
					     uint32_t deltaT,
					     uint32_t logNEvt,
					     uint32_t seed,
					     uint32_t stdDev,
					     uint32_t minSize,
					     uint32_t maxSize ) {
  
  
  double sum, sum2, cmean, crms;
  
  if ( minSize < 24 ) {
    LOG4CPLUS_WARN (logger_, "Requested minSize " << minSize << 
		    " is too small. Set to 24 byte");
    minSize = 24;
  }

  if ( mean < minSize ) {
    LOG4CPLUS_WARN (logger_, "Requested mean " << mean << 
		    " is smaller than minSize. Set equal to minSize");
    mean = minSize;
  }

  if ( maxSize <= mean ) {
    LOG4CPLUS_WARN (logger_, "Requested maxSize " << maxSize << 
		    " is smaller than the mean  " << mean << 
		    ". Set to 'mean + rms'.");
    maxSize = mean + stdDev;
  }

  toolbox::math::LogNormalGen lognorm( seed,
				       mean,
				       stdDev);
  
  if ( logNEvt > 21 ) {
    LOG4CPLUS_DEBUG (logger_, "maximal 2 Mega evts therefore logNEvt <= 22. \n"
		    << "Setting to maximal event table size.");
    logNEvt = 21;
  }

  uint32_t mask = (1 << logNEvt) - 1;
  uint32_t nevt = mask + 1;
  uint32_t newSize;
  uint32_t bxCount = 0;
  
  LOG4CPLUS_DEBUG (logger_,  "generating " << nevt << " log normal events.");
  
  this->resetBit( "eventPreload");
  this->write( "sdramMask", mask );
  this->setBit( "eventCounterOn" );

  sum = 0.0;
  sum2 = 0.0;
  for ( uint32_t ic=0; ic<nevt; ic++ ) {
    if ( (ic % 100000) == 0 && ic > 0 ) {
      LOG4CPLUS_DEBUG (logger_, "generated " << ic << " event descriptors...");
    }
    newSize = lognorm.getRawRandomSize();
    newSize &= ~0x7;

    if ( newSize < minSize  ||   newSize > maxSize) {
      ic--;
      continue;
    }
    sum  += newSize;
    sum2 += (newSize * newSize);
    this->write( "eventLength", newSize, HAL::HAL_NO_VERIFY, ic*16 );
    this->write( "source", source, HAL::HAL_NO_VERIFY, ic*16 );
    this->write( "seed", FEDEMULATOR_SEED, HAL::HAL_NO_VERIFY, ic*16 );
    this->write( "bx", bxCount, HAL::HAL_NO_VERIFY, ic*16 );
    this->write( "deltaT", deltaT , HAL::HAL_NO_VERIFY, ic*16  );
    this->write( "eventId", 0x7, HAL::HAL_NO_VERIFY, ic*16 );
    this->write( "errorEvId", 0, HAL::HAL_NO_VERIFY, ic*16 );
    this->write( "errorCRC", 0, HAL::HAL_NO_VERIFY, ic*16 );
    
    bxCount = 0xfff & (bxCount+1);
  }

  cmean = sum / nevt;
  crms = sqrt(sum2/nevt - (cmean*cmean));
  LOG4CPLUS_DEBUG (logger_, "Fragsize distribution: " << cmean << " +- " << crms ); 
}


void d2s::FEDEmulatorCard::preloadEvents( std::string fileName ) {

  // open the event file
  std::ifstream infile( fileName.c_str(), std::ios::binary|std::ios::in );
  if ( ! infile ) {
    std::string text = "Cannot open file : " + fileName ;
    throw (HAL::NoSuchFileException( text, __FILE__, __LINE__, __FUNCTION__ ));
  }
  uint32_t offset, icnt, word1, word2;
  uint32_t evtCnt, length, startAdr, endAdr, adr;
  offset = 0;
  adr = 0;
  evtCnt = 0;
  length = 0;
  startAdr = 0;
  endAdr = 0;
  word1 = 0;
  word2 = 0;
  icnt = 0;
  char *ptr;
  while ( ! infile.eof() ) {
    ptr = (char*) &word1;
    adr += 4;
    infile.read( ptr, sizeof(word1));
    if ( infile.eof() ) {
      LOG4CPLUS_DEBUG(logger_, toolbox::toString( "EOF reached. Last word: %08x %08x", word2, word1 ));
      break;
    }
    ptr = (char*) &word2;
    adr += 4;
    infile.read( ptr, sizeof(word2));
    if ( infile.eof() ) {
      std::string text = "File format error : " + fileName ;
      throw (HAL::NoSuchFileException( text, __FILE__, __LINE__, __FUNCTION__ ));
    }
    if ( length == 0 ) {
      length = word1;
      startAdr = adr;
      endAdr = startAdr + length;
    }
    if ( adr == endAdr ) {
      LOG4CPLUS_DEBUG(logger_, toolbox::toString("event %d length %08x startAdr %08x endAdr %08x", evtCnt, length,
						startAdr, endAdr));
      length = 0;
      evtCnt++;
    }
    if ( icnt++ < 100 ) {
      LOG4CPLUS_DEBUG(logger_, toolbox::toString("     %x %x", word2, word1 ));    
    }
    this->write( "memoryStart", word1, HAL::HAL_NO_VERIFY, offset );
    offset += 4;
    this->write( "memoryStart", word2, HAL::HAL_NO_VERIFY, offset );
    offset += 4;
  }

  this->setBit("eventPreload");
  this->setBit( "eventCounterOn" );
  
};


