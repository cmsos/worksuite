/**
 * @file fedkit-Dbuff.c (in driver section)
 * kernel side of DMA buffers allocation and mmapping
 * recycling of code from xdaq-shell.
 */

/* fedkit documentation is at at http://cern.ch/cano/fedkit/ */

/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-Dbuff.c,v 1.3 2007/10/24 08:37:51 cano Exp $
*/
static char *rcsid_dbuff = "@(#) $Id: fedkit-Dbuff.c,v 1.3 2007/10/24 08:37:51 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_dbuff);
#endif

#include <linux/fs.h>
#include <linux/mm.h>
#include <asm/io.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <asm/page.h>
#include "../include/fedkit-private.h"

/* file operation for DMA buffers */

/* this is still made optional by ifdesf, but it should be there all the time */
#define PAGES_NAILING

/* attempted workaround for failing __get_free_pages (formerly bigphys) allocation : we try to reallocate the same size,
__get_free_pages(..) will be called only for changing sizes */

void * prev_dbuff = NULL;
int prev_dbuff_pages = 0;
extern struct semaphore kfedkit_prev_dbuff_sem/* = MUTEX*/; /* declared and initialised in fedkit.c */

/* ---------------------------------------------------------------------- */

/** this is called for an ioctl(..) with FEDKIT_ALLOC_DBUFF.

    @param arg is a pointer (in user space) to a _fedkit_dbuff structure.
 */
#ifdef UNLOCKED
int kfedkit_ioctl_alloc_dbuff (struct file *f,
			       unsigned int cmd, unsigned long arg)
#else
int kfedkit_ioctl_alloc_dbuff (struct inode *inode_p, struct file *f,
			       unsigned int cmd, unsigned long arg)
#endif
{
        /* will contain user space information (provided by the caller) */
 	struct _fedkit_dbuff dbuff;

	/* the corresponding information in kernel space (?) */
	struct kfedkit_dbuff_t * kfedkit_dbuff;

	if (raw_copy_from_user ((void *)&dbuff, (void *)arg, sizeof (struct _fedkit_dbuff))) {
		eprintf ("raw_copy_from_user returned non-zero\n");
		eprintf ("with parameters : &dbuff=%p, arg=0x%08lx, size = 0x%08lx\n",
			&dbuff, arg, sizeof (struct _fedkit_dbuff));
		return -1;
	}

	idprintf ("In kfedkit_ioctl_alloc_dbuff : will try to allocate 0x%x size (0x%lx pages)\n", 
		dbuff.size, (dbuff.size - 1) / PAGE_SIZE + 1);

	/* mutithread safety, not really required, but you never know */
	down (&kfedkit_prev_dbuff_sem);

	if ( (prev_dbuff != NULL) && (prev_dbuff_pages == (dbuff.size - 1) / PAGE_SIZE + 1)) {

                /* give the user a previously allocated piece of memory (re-use) ??? */
		dbuff.kernel_address = prev_dbuff;
		prev_dbuff = NULL;
		prev_dbuff_pages = 0;
		up (&kfedkit_prev_dbuff_sem);
	} else {
	        /* no previously allocated memory available (to be given
                   to the user), allocated new (kernel) memory. */

		/*dbuff.kernel_address = kmalloc (dbuff.size, GFP_KERNEL);*/
		up (&kfedkit_prev_dbuff_sem);
		idprintf ("Really allocating 0x%x size (0x%lx pages)\n", 
			dbuff.size, (dbuff.size - 1) / PAGE_SIZE + 1);

		/* allocate kernel memory of the same size ?! */
		dbuff.kernel_address = 
		  /* bigphysarea_alloc_pages ((dbuff.size - 1) / PAGE_SIZE + 1, 0, GFP_KERNEL); */
		  (void *) __get_free_pages(GFP_KERNEL, get_order(dbuff.size)); 
		  // dbuff.kernel_address = pci_alloc_consistent(receiver->device, dbuff.size, 
		  //       				      &(dbuff.kernel_address_dma_addr));

		idprintf ("Really allocating ; done\n");
	}

	idprintf ("In kfedkit_ioctl_alloc_dbuff : pci_alloc_consistent returned 0x%px (virt)\n", dbuff.kernel_address);
	if (dbuff.kernel_address == NULL) {
		eprintf ("Could not allocate %ld page(s) for Dbuff\n", (int)(dbuff.size - 1) / PAGE_SIZE + 1);
		return -1;
	}
	/*#ifdef I2ODEBUG*/
		else {
		idprintf ("In kfedkit_ioctl_alloc_dbuff : allocated dbuff : 0x%8px\n", dbuff.kernel_address);	
	} 
	/*#endif*/ /* def I2ODEBUG */

	/* nail those memory areas */
#ifdef PAGES_NAILING
	{
		/* we will nail all the pages */
		#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,3,0)
			struct page * page;
			for (page = virt_to_page(dbuff.kernel_address); 
					page <= virt_to_page(dbuff.kernel_address + dbuff.size -1); 
					page++) 
				// set_bit(PG_reserved, &mem_map[page_to_pfn(page)].flags);
	                          set_bit(PG_reserved, &(page->flags));
		#else
			int i;
			int start = (int)dbuff.kernel_address;
			int end = start + (((dbuff.size - 1) / PAGE_SIZE + 1) * PAGE_SIZE) - 1;
			for (i=MAP_NR(start); i <= MAP_NR (end); i++)
				set_bit(PG_reserved, &mem_map[i].flags); 
		#endif
	}
#endif
	
	dbuff.physical_address = virt_to_phys (dbuff.kernel_address);
	if (raw_copy_to_user ((void *)arg, (void *)&dbuff, sizeof (struct _fedkit_dbuff))) {
		eprintf ("raw_copy_to_user returned non-zero\n");
		eprintf ("with parameters : arg=0x%08lx, &dbuff=%p, size = 0x%08lx\n",
			arg, &dbuff, sizeof (struct _fedkit_dbuff));
		/* can't return the memory to the user all the process has to be reversed */
		#ifdef PAGES_NAILING
		{
			/* we will unnail all the pages */
			#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,3,0)
				struct page * page;
				for (page = virt_to_page(dbuff.kernel_address); 
						page <= virt_to_page(dbuff.kernel_address + dbuff.size - 1); 
						page++) 
					// clear_bit(PG_reserved, &mem_map[page_to_pfn(page)].flags); 
					clear_bit(PG_reserved, &(page->flags)); 
			#else
				int i;
				int start = (int)dbuff.kernel_address;
				int end = start + (((dbuff.size - 1) / PAGE_SIZE + 1) * PAGE_SIZE) - 1;
				for (i=MAP_NR(start); i <= MAP_NR (end); i++)
					clear_bit(PG_reserved, &mem_map[i].flags); 
			#endif
		}
		#endif
		/* bigphysarea_free_pages (dbuff.kernel_address); */
		free_pages((unsigned long)dbuff.kernel_address, get_order(dbuff.size));
		dbuff.kernel_address = NULL; /* make sure we don't free more than once */
		return -1;
	}

	/* keep track of the dbuffs allocated to this file descriptor  */
	/* (mainly to deallocate in case of segfault of program or	   */
	/* ungracefull exit) */
	if (NULL != (kfedkit_dbuff = (struct kfedkit_dbuff_t*)kmalloc (sizeof (struct kfedkit_dbuff_t), GFP_KERNEL))) {

                /* if we can't keep track, well too bad, we might have a memory leak, but that's very unlikely */
		kfedkit_dbuff->next = ((struct kfedkit_board_t*)f->private_data)->dbuffs;
		((struct kfedkit_board_t*)f->private_data)->dbuffs = kfedkit_dbuff;
		kfedkit_dbuff->kernel_address = dbuff.kernel_address;
		kfedkit_dbuff->size           = dbuff.size;
		idprintf ("In kxdsh_ioctl_alloc_dbuff : registering : %p\n", kfedkit_dbuff->kernel_address);
	}
	return 0;
}

/* ---------------------------------------------------------------------- */

#ifdef UNLOCKED
int kfedkit_ioctl_free_dbuff (struct file *f,
	      unsigned int cmd, unsigned long arg)
#else
int kfedkit_ioctl_free_dbuff (struct inode *inode_p, struct file *f,
	      unsigned int cmd, unsigned long arg)
#endif
{
	struct _fedkit_dbuff dbuff;
	struct kfedkit_dbuff_t ** pdbuffs; 
	if (raw_copy_from_user ((void *)&dbuff, (void *)arg, sizeof (struct _fedkit_dbuff))) {
		eprintf ("raw_copy_from_user returned non-zero\n"); 
		eprintf ("with parameters : &dbuff=%p, arg=0x%08lx, size = 0x%08lx\n",
			&dbuff, arg, sizeof (struct _fedkit_dbuff));
		return -1;
	}
	#ifdef PAGES_NAILING
	{
		/* we will unnail all the pages */
		#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,3,0)
			struct page * page;
			for (page = virt_to_page(dbuff.kernel_address); 
					page <= virt_to_page(dbuff.kernel_address + dbuff.size -1); 
					page++) 
				// clear_bit(PG_reserved, &mem_map[page_to_pfn(page)].flags);
				clear_bit(PG_reserved, &(page->flags));
		#else
			int i;
			int start = (int)dbuff.kernel_address;
			int end = start + (((dbuff.size - 1) / PAGE_SIZE + 1) * PAGE_SIZE) - 1;
			for (i=MAP_NR(start); i <= MAP_NR (end); i++)
				clear_bit(PG_reserved, &mem_map[i].flags);
		#endif
	}
	#endif
	idprintf ("In kfedkit_ioctl_free_dbuff : freeing Dbuff : 0x%8p\n", dbuff.kernel_address);
	down (&kfedkit_prev_dbuff_sem);
	if (prev_dbuff != NULL) {
		idprintf ("Really freeing Dbuff : 0x%8p\n", prev_dbuff);

		/* bigphysarea_free_pages (prev_dbuff); */
		/* assumes that prev_dbuff_pages is actually correctly set.
		   Note that we probably could store the size or the 'order'
		   (instead of the number of pages) in prev_dbuff_pages
		   but in order not to possibly break the logic of the existing
		   code, we prefer to keep this variable for the moment. */
		free_pages((unsigned long)prev_dbuff, get_order(prev_dbuff_pages * PAGE_SIZE));

		idprintf ("Really freeing Dbuff : done\n");
		prev_dbuff = NULL; /* make sure we don't free more than once */

		prev_dbuff_pages = 0;
	} /* so we know pref dbuff contains nothing, let's put our stuff (hoping we will be able to reuse it */

	prev_dbuff = dbuff.kernel_address;
	prev_dbuff_pages = ((dbuff.size - 1) / PAGE_SIZE + 1);
	up (&kfedkit_prev_dbuff_sem);
	/* remove this dbuff from the list of the dbuff held by this file descritor */
	pdbuffs = &(((struct kfedkit_board_t *)(f->private_data))->dbuffs);
	while (*pdbuffs != NULL) {
		struct kfedkit_dbuff_t * to_free = *pdbuffs;
		if (to_free->kernel_address == dbuff.kernel_address) {
			idprintf ("In kfedkit_ioctl_free_dbuff : unregistering %p\n", dbuff.kernel_address);
			*pdbuffs = to_free->next;
			kfree (to_free);			
		} else {
			pdbuffs = &(to_free->next);
		}
	}
	idprintf ("In kfedkit_ioctl_free_dbuff : freeing Dbuff : done\n");
	return 0;
}

/* ---------------------------------------------------------------------- */

/* this function is called by the release function for the fedkit file descriptors (struct kfedkit_board_t)*/
/* this function releases any bigphys zone still allocated under the name of this 
file (in case of segfault or ungracefull exit (kill)) */
void kfedkit_dbuffs_free (struct kfedkit_dbuff_t * dbuffs)
{
	struct kfedkit_dbuff_t * next;
	idprintf ("In kfedkit_dbuffs_free : start\n");
	while (dbuffs != NULL) {
		next = dbuffs->next;
		idprintf ("In kxdsh_dbuffs_free : freeing : %p\n", dbuffs->kernel_address);

		/* bigphysarea_free_pages (dbuffs->kernel_address); */
		free_pages((unsigned long)dbuffs->kernel_address, get_order(dbuffs->size));
		dbuffs->kernel_address = NULL; /* make sure we don't free more than once */

		kfree (dbuffs);
		dbuffs = next;
	}
	idprintf ("In kxdsh_dbuffs_free : end\n");
}

/* ---------------------------------------------------------------------- */

int kfedkit_Dbuff_mmap (struct file *file_p, struct vm_area_struct *vma)
{
#ifndef RED_HAT_LINUX_KERNEL
#define RED_HAT_LINUX_KERNEL 0
#endif

/*	if (remap_page_range (vma_get_start(vma), vma_get_offset(vma), 
			vma_get_end (vma) - vma_get_start(vma), vma_get_page_prot(vma))) { -- before 2.4 kernel*/
    unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,10) 
    if (remap_pfn_range (vma, offset, vma->vm_pgoff, 
                         vma->vm_end - vma->vm_start, vma->vm_page_prot) ) {
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
    if (remap_page_range (vma, vma->vm_start, offset, 
                          vma->vm_end - vma->vm_start, vma->vm_page_prot)) {
#elif LINUX_VERSION_CODE > KERNEL_VERSION(2,4,18) && RED_HAT_LINUX_KERNEL
    if (remap_page_range (vma, vma->vm_start, offset, 
                          vma->vm_end - vma->vm_start, vma->vm_page_prot)) {
#else
    if (remap_page_range (vma->vm_start, offset, 
                              vma->vm_end - vma->vm_start, vma->vm_page_prot)) {
#endif
	    eprintf ("xdsh_Dbuff_mmap: fail to remap start=0x%08lx, end=0x%08lx, ofset = 0x%08lx\n", 
		     vma->vm_start, vma->vm_end, offset);
	    return -EAGAIN;
	}
    edprintf ("In xdsh_Dbuff_mmap : mapped successfully vma with :\n");
    edprintf ("start  = 0x%08x\n",(int)vma->vm_start);
    edprintf ("offset = 0x%08x\n",(int)offset);
    edprintf ("end =    0x%08x\n",(int)vma->vm_end);
    edprintf ("(size =  0x%08x)\n",(int)(vma->vm_end-vma->vm_start));
    return (0);   /* happy */
}

/* ---------------------------------------------------------------------- */
