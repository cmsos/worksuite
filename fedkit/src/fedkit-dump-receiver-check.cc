/** @file fedkit_dump_receiver_check.c program that performs simple data integrity checks (incl. CRC) on the flight
 *
 * Test program that simply runs on a link of a receiver until killed. A summary
 * of the checks performed is dumped every N fragments (configurable). 
 * The program is based on fedkit_dump_receiver.c by Eric Cano.
 *
 * Author: Tim Christiansen, CERN-PH/CMG <Tim.Christiansen@cern.ch>
 * Maintainer: Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 * 
 
 $Id: fedkit-dump-receiver-check.cc,v 1.4 2009/02/27 17:31:51 cano Exp $

*/
static const char *rcsid = "@(#) $Id: fedkit-dump-receiver-check.cc,v 1.4 2009/02/27 17:31:51 cano Exp $";
/* The following lines will prevent `gcc' version 2.X and later
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid);
#endif

#include <iostream>
#include <cstdlib>
#include <cstring>

#include <vector>
#include <ostream> 
#include <iomanip>
#include <vector>
#include <stdint.h>

#include <sstream>

#include <signal.h>
#include "fedkit.h"

using namespace std;
//using std::cout;
//using std::endl;
//using std::cerr;
//using std::hex;
//using std::dec;

/* Command line parameters */

int receiver_unit = 0;
int receive_link_pattern = 0;

int nsummary = 10000;
uint32_t nerr=0, nerr_evt=0, nerr_CRC=0, nerr_wdcnt=0;
int firsteventerror=1;
int evt=-1;
int ncount = 0;    
vector <uint32_t> nstat;
bool TestCRC=true;

void PrintSummary();
void PrintWord(vector<uint32_t> word_msw, vector<uint32_t> word_lsw);

struct b64 {
  bool bits[64];
  static int bs;
  bool & operator [] (size_t i) { return bits[i ^ bs]; }
  bool   operator [] (size_t i) const { return bits[i ^ bs]; }
};

int b64::bs = 0;

namespace command_line {
    void usage (const char *progname) {
        cout << progname << ": dumps one of the links of a fedkit receiver" << endl;
        cout << "Usage : "<< progname << " [-r <receiver unit number>] [-l <receiver link pattern> ] [-n|-c] [-v <summary every N evts>]" << endl;
	cout << " options: \n"
	     << "  -n     : do not check CRC\n"
	     << "  -c     : check CRC (default)\n"
	     << "  -v <N> : print summary every <N> event fragments"
	     <<endl;
    }

    void summarize(const char *progname)
    {
        cout << "To run again : " << endl
             << progname << " -r " << receiver_unit << " -l " << receive_link_pattern << endl;
    }

    void check_arg_number (int index, int argc, const char * option_name)
    {
        if (index+1 >= argc) {
            cerr << "Argument expected after option \'" << option_name << "\'" << endl;
            exit(1);
        }
    }

    void get_positive_numeric_arg (const char *string, int *ret, const char* error_string)
    {
        char *lastchar;
        *ret  = strtol (string, &lastchar, 10);
        if ((*lastchar != '\0') || (ret < 0)) {
            cerr << error_string << string << endl;
            exit (1);
        }
    }

    void parse (int argc, char *argv[])
    {
        for (int i=1; i<argc; i++) {
            /* usage */
            if (!strcmp (argv[i], "--help") || !strcmp (argv[i], "-help")
		|| !strcmp (argv[i], "-h")) {
                usage (argv[0]);
                exit (0);
                /* get receiver index */
            } else if (!strcmp (argv[i], "-r")) {
                check_arg_number (i, argc, "-r");
                get_positive_numeric_arg (argv[++i], &receiver_unit, "Invalid receiver index : ");
                /* get link number index */
            } else if (!strcmp (argv[i], "-l")) {
                check_arg_number (i, argc, "-l");
                get_positive_numeric_arg (argv[++i], &receive_link_pattern, "Invalid link number : ");
            } else if (argv [i][0]=='-' && argv [i][1]=='c' && i+1<argc) {
	      printf("using option -c\n");
	      TestCRC=true;
            } else if (argv [i][0]=='-' && argv [i][1]=='n' && i+1<argc) {
	      printf("using option -n\n");
	      TestCRC=false;
            } else if (argv [i][0]=='-' && argv [i][1]=='v' && i+1<argc) {
	      get_positive_numeric_arg (argv[++i], &nsummary, "Invalid link print-summary no. : ");
	      printf("using option -v %d (print summary every %d events)\n",nsummary,nsummary);
            } else if (argv [i][0] == '-') {
                cerr << "Invalid option -- " << (char *)argv[i]+1 << endl;
                cerr << "Try \'" << argv[0]<< " --help\' for more instructions." << endl;
                exit (1);
            } else {
                cerr << "Invalid argument " << (char *)argv[i] << endl;
                cerr << "Try \'" << argv[0]<< " --help\' for more instructions." << endl;
                exit (1);
            }
        }
    }
}; /* end of namespace command_line */

namespace main_parts {
    bool stop_run = false;

    void sigint_handler (int signal)
    {
        if (signal != SIGINT) {
            cout << "Funny, the sigint handler received signal " << signal << endl;
        }
        cout << "Interrupting link dump operations..." << endl;
        stop_run = true;
    }
    
};

/** The CRC CHECK *********************************************************************/

int calc_crc(const std::vector<b64> & bit, int init = 0xffff, int debug = 0) {
  bool crc[16],tmp[16];
  for (int i = 0; i < 16; i++) {
    crc[i]=init & 1;
    tmp[i]=crc[i];
    init >>= 1;
  }
  for (size_t k = 0; k < bit.size(); ++k) {
    if (debug & 1) {
      int crc_r = 0;
      for (int i = 0; i < 16; i++) {
        crc_r = crc_r + (crc[i] << i);
      }
      std::cout << " ...          0x" << std::setw(4) << std::hex << crc_r << std::endl;
    }
    int n;
    if (debug & 2) {
      std::cout << "\n D=";
      for (int i = 63; i >= 0; --i) { std::cout << bit[k][i]; }
      std::cout << " O=";
      for (int i = 15; i >= 0; --i) { std::cout << tmp[i]; }
      std::cout <<" (" << std::dec;
    }
    n = bit[k][63] + bit[k][62] + bit[k][61] + bit[k][60] + bit[k][55] + bit[k][54] +
      bit[k][53] + bit[k][52] + bit[k][51] + bit[k][50] + bit[k][49] + bit[k][48] +
      bit[k][47] + bit[k][46] + bit[k][45] + bit[k][43] + bit[k][41] + bit[k][40] +
      bit[k][39] + bit[k][38] + bit[k][37] + bit[k][36] + bit[k][35] + bit[k][34] +
      bit[k][33] + bit[k][32] + bit[k][31] + bit[k][30] + bit[k][27] + bit[k][26] +
      bit[k][25] + bit[k][24] + bit[k][23] + bit[k][22] + bit[k][21] + bit[k][20] +
      bit[k][19] + bit[k][18] + bit[k][17] + bit[k][16] + bit[k][15] + bit[k][13] +
      bit[k][12] + bit[k][11] + bit[k][10] + bit[k][9]  + bit[k][8]  + bit[k][7]  +
      bit[k][6]  + bit[k][5]  + bit[k][4]  + bit[k][3]  + bit[k][2]  + bit[k][1]  +
      bit[k][0]  + tmp[0]     + tmp[1]     + tmp[2]     + tmp[3]     + tmp[4]     +
      tmp[5]     + tmp[6]     + tmp[7]     + tmp[12]    + tmp[13]    + tmp[14]    +
      tmp[15];
    crc[0] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][63] + bit[k][62] + bit[k][61] + bit[k][56] + bit[k][55] + bit[k][54] +
      bit[k][53] + bit[k][52] + bit[k][51] + bit[k][50] + bit[k][49] + bit[k][48] +
      bit[k][47] + bit[k][46] + bit[k][44] + bit[k][42] + bit[k][41] + bit[k][40] +
      bit[k][39] + bit[k][38] + bit[k][37] + bit[k][36] + bit[k][35] + bit[k][34] +
      bit[k][33] + bit[k][32] + bit[k][31] + bit[k][28] + bit[k][27] + bit[k][26] +
      bit[k][25] + bit[k][24] + bit[k][23] + bit[k][22] + bit[k][21] + bit[k][20] +
      bit[k][19] + bit[k][18] + bit[k][17] + bit[k][16] + bit[k][14] + bit[k][13] +
      bit[k][12] + bit[k][11] + bit[k][10] + bit[k][9]  + bit[k][8]  + bit[k][7]  +
      bit[k][6]  + bit[k][5]  + bit[k][4]  + bit[k][3]  + bit[k][2]  + bit[k][1]  +
      tmp[0]     + tmp[1]     + tmp[2]     + tmp[3]     + tmp[4]     + tmp[5]     +
      tmp[6]     + tmp[7]     + tmp[8]     + tmp[13]    + tmp[14]    + tmp[15];
    crc[1] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][61] + bit[k][60] + bit[k][57] + bit[k][56] + bit[k][46] + bit[k][42] +
      bit[k][31] + bit[k][30] + bit[k][29] + bit[k][28] + bit[k][16] + bit[k][14] +
      bit[k][1]  + bit[k][0]  + tmp[8]     + tmp[9]     + tmp[12]    + tmp[13];
    crc[2] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][62] + bit[k][61] + bit[k][58] + bit[k][57] + bit[k][47] + bit[k][43] +
      bit[k][32] + bit[k][31] + bit[k][30] + bit[k][29] + bit[k][17] + bit[k][15] +
      bit[k][2]  + bit[k][1]  + tmp[9]     + tmp[10]    + tmp[13]    + tmp[14];
    crc[3] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][63] + bit[k][62] + bit[k][59] + bit[k][58] + bit[k][48] + bit[k][44] +
      bit[k][33] + bit[k][32] + bit[k][31] + bit[k][30] + bit[k][18] + bit[k][16] +
      bit[k][3]  + bit[k][2]  + tmp[0]     + tmp[10]    + tmp[11]    + tmp[14]    +
      tmp[15];
    crc[4] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][63] + bit[k][60] + bit[k][59] + bit[k][49] + bit[k][45] + bit[k][34] +
      bit[k][33] + bit[k][32] + bit[k][31] + bit[k][19] + bit[k][17] + bit[k][4]  +
      bit[k][3]  + tmp[1]     + tmp[11]    + tmp[12]    + tmp[15];
    crc[5] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][61] + bit[k][60] + bit[k][50] + bit[k][46] + bit[k][35] + bit[k][34] +
      bit[k][33] + bit[k][32] + bit[k][20] + bit[k][18] + bit[k][5]  + bit[k][4]  +
      tmp[2]     + tmp[12]    + tmp[13];
    crc[6] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][62] + bit[k][61] + bit[k][51] + bit[k][47] + bit[k][36] + bit[k][35] +
      bit[k][34] + bit[k][33] + bit[k][21] + bit[k][19] + bit[k][6]  + bit[k][5]  +
      tmp[3]     + tmp[13]    + tmp[14];
    crc[7] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][63] + bit[k][62] + bit[k][52] + bit[k][48] + bit[k][37] + bit[k][36] +
      bit[k][35] + bit[k][34] + bit[k][22] + bit[k][20] + bit[k][7]  + bit[k][6]  +
      tmp[0]     + tmp[4]     + tmp[14]    + tmp[15];
    crc[8] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][63] + bit[k][53] + bit[k][49] + bit[k][38] + bit[k][37] + bit[k][36] +
      bit[k][35] + bit[k][23] + bit[k][21] + bit[k][8]  + bit[k][7]  + tmp[1]     +
      tmp[5]     + tmp[15];
    crc[9] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][54] + bit[k][50] + bit[k][39] + bit[k][38] + bit[k][37] + bit[k][36] +
      bit[k][24] + bit[k][22] + bit[k][9]  + bit[k][8]  + tmp[2]     + tmp[6];
    crc[10] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][55] + bit[k][51] + bit[k][40] + bit[k][39] + bit[k][38] + bit[k][37] +
      bit[k][25] + bit[k][23] + bit[k][10] + bit[k][9]  + tmp[3]     + tmp[7];
    crc[11] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][56] + bit[k][52] + bit[k][41] + bit[k][40] + bit[k][39] + bit[k][38] +
      bit[k][26] + bit[k][24] + bit[k][11] + bit[k][10] + tmp[4]     + tmp[8];
    crc[12] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][57] + bit[k][53] + bit[k][42] + bit[k][41] + bit[k][40] + bit[k][39] +
      bit[k][27] + bit[k][25] + bit[k][12] + bit[k][11] + tmp[5]     + tmp[9];
    crc[13] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][58] + bit[k][54] + bit[k][43] + bit[k][42] + bit[k][41] + bit[k][40] +
      bit[k][28] + bit[k][26] + bit[k][13] + bit[k][12] + tmp[6]     + tmp[10];
    crc[14] = n & 1;
    if (debug & 2) std::cout << n << ",";
    n = bit[k][63] + bit[k][62] + bit[k][61] + bit[k][60] + bit[k][59] + bit[k][54] +
      bit[k][53] + bit[k][52] + bit[k][51] + bit[k][50] + bit[k][49] + bit[k][48] +
      bit[k][47] + bit[k][46] + bit[k][45] + bit[k][44] + bit[k][42] + bit[k][40] +
      bit[k][39] + bit[k][38] + bit[k][37] + bit[k][36] + bit[k][35] + bit[k][34] +
      bit[k][33] + bit[k][32] + bit[k][31] + bit[k][30] + bit[k][29] + bit[k][26] +
      bit[k][25] + bit[k][24] + bit[k][23] + bit[k][22] + bit[k][21] + bit[k][20] +
      bit[k][19] + bit[k][18] + bit[k][17] + bit[k][16] + bit[k][15] + bit[k][14] +
      bit[k][12] + bit[k][11] + bit[k][10] + bit[k][9]  + bit[k][8]  + bit[k][7]  +
      bit[k][6]  + bit[k][5]  + bit[k][4]  + bit[k][3]  + bit[k][2]  + bit[k][1]  +
      bit[k][0]  + tmp[0]     + tmp[1]     + tmp[2]     + tmp[3]     + tmp[4]     +
      tmp[5]     + tmp[6]     + tmp[11]    + tmp[12]    + tmp[13]    + tmp[14]    +
      tmp[15];
    crc[15] = n & 1;
    if (debug & 2) std::cout << n << ") ";

    for (int i = 0; i < 16; i++) tmp[i]=crc[i];
		
  }
  if (debug & 2) std::cout << "\n\n";
  int crc_r = 0;
  for (int i = 0; i < 16; i++) {
    crc_r = crc_r + (crc[i] << i);
  }
  return crc_r;
}



/** The M A I N *********************************************************************/

#define ISHEADER(msw) (((msw>>28)&0xf) == 0x5)
#define ISTRAILER(msw) (((msw>>28)&0xf) == 0xa)

int main (int argc, char *argv[])
{
    struct fedkit_receiver * receiver = NULL;
    using namespace main_parts;

    command_line::parse (argc, argv);
    command_line::summarize(argv[0]);
    if (receive_link_pattern > 3) {
        cerr << "Invalid link number, defaulting to 1" << endl;
        receive_link_pattern = 1;
    }
    receiver = NULL;
    receiver = fedkit_open (receiver_unit, NULL);
    if (receiver == NULL) {
        cerr << "Can\'t open receiver unit " << receiver_unit << endl;
        exit (1);
    } 
     
    cout << "Sucessfully opened a FEDkit receiver with FPGA version " << hex << "0x" << fedkit_get_FPGA_version (receiver) << dec << endl;
    if (fedkit_set_receive_pattern (receiver, receive_link_pattern)) {
            cout << "Failed while trying to set receive pattern. Reverting to default." << endl;
    }
     fedkitdump_start(receiver);
    if (FK_OK != fedkit_enable_link_dump (receiver)) {
        cerr << "Failed to switch to dump mode. Aborting." << endl;
        fedkit_close (receiver);
        exit (1);
    }
    signal (SIGINT, sigint_handler);
    bool IsInWord=false;
    nstat.resize(14);
    vector<uint32_t> word_msw, word_lsw;
    
      
    
    while (!stop_run) {
        uint32_t lsw, msw;
        int control;
        int ret = fedkit_link_dump (receiver, &lsw, &msw, &control);
        if (FK_OK == ret) {
	  // printf(" %c 0x%08x%08x\n", control?'K':'.',msw,lsw);
	  word_msw.push_back(msw);
	  word_lsw.push_back(lsw);
	  if (control){
	    //printf("control = %d 4bits are 0x%x\n",control, ((msw>>28)&0xf));
	    if (ISHEADER(msw)){
	      if (ncount > 1 && ncount%nsummary == 0) PrintSummary();
	      ++ncount;
	      if (IsInWord){
		++nerr;
		printf("ERROR: Unexpected Header word!\n");
		PrintWord(word_msw,word_lsw);
	      }
	      int newevt = (msw&0xffffff);
	      if (evt<0){
		if (newevt!=1){
		  printf("WARNING: 1st event is No. %d != 1!\n",newevt);
		  PrintWord(word_msw,word_lsw);
		  ++nerr;
		  //++nerr_evt;
		  firsteventerror = newevt;
		}
	      }else{
		if (newevt != evt+1 && evt<0xffffff){
		  printf("WARNING: Expect evt %d, but read %d",evt+1,newevt);
		  PrintWord(word_msw,word_lsw);
		  ++nerr;
		  ++nerr_evt;
		}
	      }
	      evt = newevt;
	      
	      // this is the header, so start a new word:
	      word_msw.clear(); word_lsw.clear();
	      word_msw.push_back(msw);
	      word_lsw.push_back(lsw);
	      
	      
	      IsInWord = true;
	    }else if (ISTRAILER(msw)){
	      if (!IsInWord){
		++nerr;
		PrintWord(word_msw,word_lsw);
		printf("ERROR: Unecpected Trailer word!\n");
	      }
	      uint32_t wdcnt = ((msw&0xffffff));
	      if (word_msw.size()<2 || 
		  wdcnt != word_msw.size()) {
		// z is the length specifier for size_t, see e.g. http://www.pixelbeat.org/programming/gcc/format_specs.html
		printf("ERROR: Word-count error: counted %zd but read %d (incl. header+trailer)\n",
		       word_msw.size(),wdcnt);
		PrintWord(word_msw,word_lsw);
		++nerr;
		++nerr_wdcnt;
	      }
	      
	      // word length statistics:
	      ++nstat[(word_msw.size()>=nstat.size() ? nstat.size()-1 :
		       word_msw.size())];

	      if (TestCRC){
		// this is the header, so fill the bit vector and calculate:
		if (word_msw.size()>1){
		  std::vector<b64> bit;
		  bit.resize(word_msw.size());
		  for (size_t j=0; j<word_msw.size(); ++j){
		    for (size_t n=0; n<32; ++n){
		      //cout << "bit[j][n]="<<bit[j][n]<<endl;
		      bit[j][n]    = (word_lsw[j]>>n) & 1;
		      bit[j][n+32] = (word_msw[j]>>n) & 1;
		    }
		  }    
		  for (int i = 16; i < 32; ++i) bit[word_msw.size()-1][i] = 0;
		  
		  int CRC_read=((lsw>>16)&0xffff);
		  b64::bs = 0;
		  int CRC_calc = calc_crc(bit, 0xffff /* , true */);
		  if (CRC_read != CRC_calc){
		    ++nerr; ++nerr_CRC;
		    printf("ERROR: Wrong CRC: read 0x%x but calculated 0x%x\n",
			   CRC_read,CRC_calc);
		    PrintWord(word_msw,word_lsw);
		  }
		}
	      }
	      IsInWord = false;
	    }else{
	      ++nerr; 
	      printf("ERROR: Invalid control word: %c 0x%08x%08x\n", 
		     control?'K':'.',msw,lsw);
	      PrintWord(word_msw,word_lsw);
	    }
	  }
	  fflush (stdout);
        } else if (FK_empty != ret) {
            cerr << "Unexpected error while dumping the link. Stopping the run." << endl;
            stop_run = true;
        }
    }
    fedkit_close (receiver);
    return 0;
}

void PrintSummary(){
  printf("*** Summary: ************************************************************\n");
  printf("Number of fragments received: %d (last evt-id: %d)\n",ncount,evt);
  printf("Errors:");
  printf("\nTotal:             %d",nerr);
  if (firsteventerror!=1) printf(" (first event was no. %d!)",firsteventerror);
  printf("\nEvent errors:      %d",nerr_evt);
  printf("\nCRC errors:        %d",nerr_CRC);
  if (!TestCRC) printf(" (CRC check switched off!)");
  printf("\nWord count errors: %d",nerr_wdcnt);
  printf("\nWord-count statistics: \n");
  for (size_t i=2; i<nstat.size(); ++i){
    if (nstat[i]>0){
      if (i+1 == nstat.size()) printf("  >%zd:%d",nstat.size()-2,nstat[i]);
      else printf("  %zd:%d",i,nstat[i]);
    }
  }
  printf("\n\n");

}

void PrintWord(vector<uint32_t> word_msw, vector<uint32_t> word_lsw){
  printf("fragment %d: \n",ncount);
  for (size_t i=0; i<word_msw.size(); ++i){
    printf("%2zd) 0x%8x%8x\n",i,word_msw[i],word_lsw[i]);
  }
}
