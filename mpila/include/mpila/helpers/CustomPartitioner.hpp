/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * CustomPartitioner.hpp
 *
 *  Created on: Mar 27, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/helpers/Partitioner.hpp"

namespace mpila
{
namespace helpers
{

class CustomPartitioner : public Partitioner
{
	public:

    CustomPartitioner(size_t commSize, const std::vector<size_t>& senderRanks,
                      const std::vector<size_t>& receiverRanks);
};

} /* namespace helpers */
}
