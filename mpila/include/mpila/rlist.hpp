/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * rlist.hpp
 *
 *  Created on: Jan 30, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 *
 *  adapted from J. Gutleber and L. Orsini
 *  https://svnweb.cern.ch/trac/cmsos/browser/releases/baseline11/trunk/daq/toolbox/include/toolbox/rlist.h
 */

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#pragma once

#include <string.h>
#include <string>
#include <sstream>
#include <stdexcept>

namespace mpila
{

template <class Type>
class rlist
{

   public:
    explicit rlist(size_t size = 1024)
    {
        size_ = size + 1;
        first_ = 0;
        last_ = 0;
        v_ = new Type[size + 1];
        name_ = "undefined";
    }

    rlist(const rlist<Type> &l) = delete;
    rlist &operator=(const rlist<Type> &l) = delete;

    ~rlist()
    {
        delete[] v_;
    }

    void setName(std::string name)
    {
        name_ = name;
    }

    //! gives the number of element contained in the rlist
    size_t elements()
    {
        if (first_ <= last_)
            return (last_ - first_);
        else
            return ((size_ - first_) + last_);
    }

    //! Reinitializes the rlist
    void clear()
    {
        while (!empty()) pop_front();
    }

    //! set the max number of element the rlist can contain
    void resize(size_t size)
    {
        // Add a check. If the rlist if currently
        // not empty, throw an exception
        if (!empty())
        {
            throw std::runtime_error("rlist cannot be resized, contains elements.");
        }

        Type *v = new Type[size + 1];

        if (size >= size_)
        {
            // copy all existing elements
            memcpy((char *)&v[0], (char *)&v_[0], size_ * sizeof(Type));
        }
        else
        {
            // copy only element up to the new size
            memcpy((char *)&v[0], (char *)&v_[0], (size + 1) * sizeof(Type));
        }

        // new size
        size_ = size + 1;

        delete[] v_;  // delete old
        v_ = v;
        return;
    }

    bool empty()
    {
        if (first_ == last_)
        {
            return (true);
        }
        return (false);
    }

    void pop_front()
    {
        if (first_ != last_)
        {
            first_ = (first_ + 1) % size_;
            return;
        }
        else
        {
            std::stringstream msg;
            msg << "empty rlist %s" << name_.c_str();
            throw std::runtime_error(msg.str());
        }
    }

    Type &front()
    {
        return (v_[first_]);
    }

    void push_back(const Type &i)
    {
        if (((last_ + 1) % size_) != first_)
        {
            v_[last_] = i;
            last_ = (last_ + 1) % size_;
            return;
        }
        else
        {
            std::string msg = "rlist overflow: ";
            msg += name_;
            throw std::runtime_error(msg);
        }
    }

   protected:
    size_t size_;
    volatile size_t first_;
    volatile size_t last_;
    Type *v_;
    std::string name_;
};
}
