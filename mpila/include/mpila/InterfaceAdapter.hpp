/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * InterfaceAdapter.hpp
 *
 *  Created on: Jan 25, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include <vector>

#include "mpila/CompletionQueue.hpp"
#include "mpila/QueuePair.hpp"
#include "mpila/Transport.hpp"

namespace mpila
{
class InterfaceAdapter
{
   public:
    InterfaceAdapter(size_t pipelineDepth, TransportMode mode);

    QueuePair *createQueuePair(int rank, size_t capacity, CompletionQueue *const completionQueue);

    void operator()();

    std::string getStats();

    void clearQPs();

   private:
    size_t pipelineDepth_;
    TransportMode transportMode_;
    std::vector<std::unique_ptr<QueuePair> > queuePairs_;
    std::unique_ptr<Transport> transport_;
};
} /* namespace mpila */
