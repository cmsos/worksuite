/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * CompletionQueue.hpp
 *
 *  Created on: Jan 11, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/CompletionEvent.hpp"
#include "mpila/rlist.hpp"

namespace mpila
{
using CompletionQueue = rlist<CompletionEvent>;
} /* namespace mpila */
