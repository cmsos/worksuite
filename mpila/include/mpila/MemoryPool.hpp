/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * Mempool.hpp
 *
 *  Created on: Jan 11, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include <vector>
#include <memory>
#include <atomic>

#include "mpila/rlist.hpp"

namespace mpila
{

using value_t = unsigned char;
using memoryBuffer_t = value_t;

class MemoryPool
{
   public:
    MemoryPool(size_t poolSize, size_t bufferSize, value_t value = 0);

    ~MemoryPool();

    void returnBuffer(memoryBuffer_t *buffer);

    bool empty();

    memoryBuffer_t *requestBuffer();

    size_t getSize();

   private:
    value_t *memoryPool_;
    size_t poolSize_;
    rlist<memoryBuffer_t *> memoryQueue_;
};

} /* namespace mpila */
