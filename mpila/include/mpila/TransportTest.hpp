/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * TransportTest.hpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/Transport.hpp"
#include "mpila/QueuePair.hpp"

namespace mpila
{

class TransportTest : public Transport
{
   public:
    TransportTest(size_t pipelineDepth);

    void operator()() override;

    void clearQPs() override;
};
} /* namespace mpila */
