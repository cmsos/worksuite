/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelTestAll.hpp
 *
 *  Created on: Mar 23, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/ChannelTestMultiple.hpp"

namespace mpila
{
class ChannelTestAll : public ChannelTestMultiple
{
   public:
    ChannelTestAll(size_t pipelineDepth, WorkRequestType type);

    rlist<WorkRequest>& getPendingWorkRequests();

   private:
    rlist<WorkRequest> pendingWorkRequests_;
};

} /* namespace mpila */
