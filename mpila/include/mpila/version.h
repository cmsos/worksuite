// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Michael Lettrich, L.Orsini			 	                   *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _mpila_version_h_
#define _mpila_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_MPILA_VERSION_MAJOR 1
#define WORKSUITE_MPILA_VERSION_MINOR 0
#define WORKSUITE_MPILA_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_MPILA_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_MPILA_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_MPILA_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_MPILA_VERSION_MAJOR,WORKSUITE_MPILA_VERSION_MINOR,WORKSUITE_MPILA_VERSION_PATCH)
#ifndef WORKSUITE_MPILA_PREVIOUS_VERSIONS
#define WORKSUITE_MPILA_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_MPILA_VERSION_MAJOR,WORKSUITE_MPILA_VERSION_MINOR,WORKSUITE_MPILA_VERSION_PATCH)
#else 
#define WORKSUITE_MPILA_FULL_VERSION_LIST  WORKSUITE_MPILA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_MPILA_VERSION_MAJOR,WORKSUITE_MPILA_VERSION_MINOR,WORKSUITE_MPILA_VERSION_PATCH)
#endif 
namespace mpila
{
	const std::string project = "worksuite";
    const std::string package  =  "mpila";
    const std::string versions = WORKSUITE_MPILA_FULL_VERSION_LIST;
    const std::string summary = "MPI Layered Architecture";
    const std::string description = "MPI Layered Architecture based on MPI API";
    const std::string authors = "Michael Lettrich, Luciano Orsini";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

