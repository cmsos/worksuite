/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelCounters.cpp
 *
 *  Created on: Mar 22, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include <mpila/ChannelCounters.hpp>
#include <string>

namespace mpila
{

ChannelCounters::ChannelCounters(WorkRequestType workRequestType)
    : idleWaitCycleCounter_(0),
      idlePostCounter_(0),
      starvingCycleCounter_(0),
      postCounter_(0),
      completionCounter_(0),
      openRequestCounter_(0)
{
    switch (workRequestType)
    {
        case WorkRequestType::SEND:
            type_ = "send";
            break;
        case WorkRequestType::RECV:
            type_ = "recv";
            break;
        case WorkRequestType::UNDEFINED:
            throw std::runtime_error("No useful statistics for queue of undefined type");
            break;
        default:
            throw std::runtime_error("Unknown work request Type for statistics counter");
    }
}

void ChannelCounters::postedRequest()
{
    postCounter_++;
    openRequestCounter_++;
}

void ChannelCounters::completedRequest()
{
    completionCounter_++;
    openRequestCounter_--;
}


std::string ChannelCounters::getStats()
{
	std::stringstream stats;

	stats << "idleWaitCycles@" << type_ << "[" << idleWaitCycleCounter_ << "]";
	stats << ", idlePostCounter@" << type_ << "[" << idlePostCounter_ << "]";
	stats << ", postCounter@" << type_ << "[" << postCounter_ << "]";
	stats << ", completionCounter@" << type_ << "[" << completionCounter_ << "]";
	stats << ", starvingCycleCounter@" << type_ << "[" << starvingCycleCounter_ << "]";
	stats << ", openRequestCounter@" << type_ << "[" << openRequestCounter_ << "]";
	return stats.str();
}

} /* namespace mpila */
