/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * channelFactory.cpp
 *
 *  Created on: Mar 22, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/channelFactory.hpp"

#include <memory>

#include "mpila/ChannelTest.hpp"
#include "mpila/ChannelTestAll.hpp"
#include "mpila/ChannelTestSome.hpp"
#include "mpila/helpers/helpers.hpp"

namespace mpila
{

std::unique_ptr<Channel> channelFactory::buildChannel(TransportMode mode, size_t pipelineDepth, WorkRequestType type)
{
    switch (mode)
    {
        case TransportMode::TEST:
            return std::make_unique<ChannelTest>(pipelineDepth, type);
            break;
        case TransportMode::TEST_SOME:
            return std::make_unique<ChannelTestSome>(pipelineDepth, type);
            break;
        case TransportMode::TEST_ALL:
            return std::make_unique<ChannelTestAll>(pipelineDepth, type);
            break;
        default:
            throw std::runtime_error("Encountered unsupported transport mode when building Channel");
            break;
    }
}

} /* namespace mpila */
