/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * FullDuplexTransportTestSome.cpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include <mpila/TransportTest.hpp>
#include <iostream>
#include <string>
#include <thread>
#include <memory>

#include "mpila/ChannelTest.hpp"

namespace mpila
{

TransportTest::TransportTest(size_t pipelineDepth) : Transport(pipelineDepth)
{
}

void TransportTest::operator()()
{
    // for each send queue
    for (auto qp : queuePairs_)
    {
        auto cq = qp->completionQueue_;

        auto sendChannel = static_cast<ChannelTest *>(qp->sendChannel_.get());
        auto &sq = qp->sendQueue_;
        auto &senderFreeList = sendChannel->getFreeList();
        auto &senderUrgentList = sendChannel->getUrgentList();
        auto &sendChannelCounters = sendChannel->getCounters();

        auto recvChannel = static_cast<ChannelTest *>(qp->recvChannel_.get());
        auto &rq = qp->recvQueue_;
        auto &recvFreeList = recvChannel->getFreeList();
        auto &recvUrgentList = recvChannel->getUrgentList();
        auto &recvChannelCounters = recvChannel->getCounters();

        bool canSend = false;
        bool canReceive = false;
        do
        {
            //-------------------------------------
            // receive
            //-------------------------------------
            canReceive = !recvFreeList.empty() && !rq.empty();
            if (canReceive)
            {
                PendingRequest *pendingRequest = recvFreeList.front();
                recvFreeList.pop_front();
                const WorkRequest &workRequest = rq.front();
                pendingRequest->fromWorkRequest(workRequest);
                rq.pop_front();
#ifndef DISABLE_MPI
#ifdef DEBUG
                if (pendingRequest->workRequest.workRequestType != mpila::WorkRequestType::RECV)
                {
                    std::cerr << "FATAL ERROR - trying to recv ";
                }
#endif
                int ierr = MPI_Irecv(pendingRequest->workRequest.memoryBuffer, pendingRequest->workRequest.size,
                                     MPI_BYTE, qp->destinationRank_, pendingRequest->workRequest.tag, MPI_COMM_WORLD,
                                     &pendingRequest->mpiRequest);
                handleMpiError(ierr, "Receiver failed at MPI_Irecv");
#endif
                recvUrgentList.push_back(pendingRequest);
                recvChannelCounters.postedRequest();
            }
            else
            {
                recvChannelCounters.idlePostCounter_++;
            }

            //-------------------------------------
            // send
            //-------------------------------------
            bool canSend = !senderFreeList.empty() && !sq.empty();
            if (canSend)
            {
                PendingRequest *pendingRequest = senderFreeList.front();
                senderFreeList.pop_front();
                const WorkRequest &workRequest = sq.front();
                pendingRequest->fromWorkRequest(workRequest);
                sq.pop_front();
#ifndef DISABLE_MPI
#ifdef DEBUG
                if (pendingRequest->workRequest.workRequestType != mpila::WorkRequestType::SEND)
                {
                    std::cerr << "FATAL ERROR - trying to send ";
                }
#endif
                int ierr = MPI_Issend(pendingRequest->workRequest.memoryBuffer, pendingRequest->workRequest.size,
                                     MPI_BYTE, qp->destinationRank_, pendingRequest->workRequest.tag, MPI_COMM_WORLD,
                                     &pendingRequest->mpiRequest);
                handleMpiError(ierr, "Sender failed at MPI_Isend");
#endif
                senderUrgentList.push_back(pendingRequest);
                sendChannelCounters.postedRequest();
            }
            else
            {
                sendChannelCounters.idlePostCounter_++;
            }
        } while (canSend || canReceive);

        //-------------------------------------
        // check completion of sends
        //-------------------------------------
        while (!senderUrgentList.empty())
        {
            int completed = false;
#ifndef DISABLE_MPI
            int ierr = MPI_Test(&senderUrgentList.front()->mpiRequest, &completed, MPI_STATUS_IGNORE);
            handleMpiError(ierr, "Sender failed at MPI_Test");
#endif
            if (completed)
            {
                PendingRequest *pendingRequest = senderUrgentList.front();
                senderUrgentList.pop_front();
                CompletionEvent opCompleted(pendingRequest->workRequest, pendingRequest->workRequest.size,
                                            EventStatus::COMPLETED);
                senderFreeList.push_back(pendingRequest);
                cq->push_back(opCompleted);
                sendChannelCounters.completedRequest();
            }
            else
            {
                break;
            }
        }

        //-------------------------------------
        // check completion of recvs
        //-------------------------------------
        while (!recvUrgentList.empty())
        {
            int completed = false;
            int recvSize = -1;

#ifndef DISABLE_MPI
            MPI_Status status;
            int ierr = MPI_Test(&recvUrgentList.front()->mpiRequest, &completed, &status);
            handleMpiError(ierr, "Receiver failed at MPI_Test");
#endif
            if (completed)
            {
#ifndef DISABLE_MPI
                MPI_Get_count(&status, MPI_BYTE, &recvSize);
#endif

                PendingRequest *pendingRequest = recvUrgentList.front();
                if (recvUrgentList.front()->mpiRequest != MPI_REQUEST_NULL)
                {
                    throw std::runtime_error("MPI request was not successfully freed");
                }

                recvUrgentList.pop_front();

                CompletionEvent opCompleted(pendingRequest->workRequest, recvSize, EventStatus::COMPLETED);
                recvFreeList.push_back(pendingRequest);
                cq->push_back(opCompleted);
                recvChannelCounters.completedRequest();
            }
            else
            {
                break;
            }
        }
    }
}

void TransportTest::clearQPs()
{
    for (auto qp : queuePairs_)
    {
        auto cq = qp->completionQueue_;

        auto sendChannel = static_cast<ChannelTest *>(qp->sendChannel_.get());
        auto &sq = qp->sendQueue_;
        auto &senderUrgentList = sendChannel->getUrgentList();

        auto recvChannel = static_cast<ChannelTest *>(qp->recvChannel_.get());
        auto &rq = qp->recvQueue_;
        auto &recvUrgentList = recvChannel->getUrgentList();
        auto &recvChannelCounters = recvChannel->getCounters();

#ifdef DEBUG
        int rank = 0;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif

        //-------------------------------------
        // check if sends have been completed
        //-------------------------------------
        if (!senderUrgentList.empty() && !sq.empty())
        {
#ifdef DEBUG
            std::stringstream ss;
            ss << "rank [" << rank << "]pending requests: " << senderUrgentList.elements()
               << " sq contains: " << sq.elements() << std::endl;
            std::cout << ss.str();
#endif

            throw std::logic_error("to clear the QPs all sends must have been terminated");
            return;
        }

        //-------------------------------------
        // cancel all pending requests
        //-------------------------------------
        while (!recvUrgentList.empty())
        {
            MPI_Status status;
            int flag = false;
            int ierr = MPI_Cancel(&recvUrgentList.front()->mpiRequest);
            handleMpiError(ierr, "failed to cancel MPI request while clearing QP");
            ierr = MPI_Wait(&recvUrgentList.front()->mpiRequest, &status);
            handleMpiError(ierr, "failed completing cancellation of MPI request while clearing QP");
            ierr = MPI_Test_cancelled(&status, &flag);
            handleMpiError(ierr, "failed checking cancellation completion of MPI request while clearing QP");
            if (!flag)
            {
                throw std::runtime_error("Failed to cancel a Irecv request while clearing QP");
            }

            PendingRequest *pendingRequest = recvUrgentList.front();
            recvUrgentList.pop_front();

            CompletionEvent opCompleted(pendingRequest->workRequest, 0, EventStatus::ERROR);
            recvChannelCounters.completedRequest();
            cq->push_back(opCompleted);
        }

        //-------------------------------------
        // clear the RQ
        //-------------------------------------
        while (!rq.empty())
        {
            const WorkRequest &workRequest = rq.front();
            CompletionEvent opCompleted(workRequest, workRequest.size, EventStatus::ERROR);
            rq.pop_front();
            cq->push_back(opCompleted);
        }
    }
}

} /* namespace mpila */
