/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * Partioner.cpp
 *
 *  Created on: Mar 27, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/helpers/Partitioner.hpp"

namespace mpila
{

namespace helpers
{
helpers::Partitioner::Partitioner(size_t commSize) : partitioning_(commSize, Partitioner::PeerType::BIDIRECTIONAL)
{
}

helpers::Partitioner::~Partitioner()
{
}

Partitioner::PeerType Partitioner::getRankType(size_t rank) const
{
    return partitioning_[rank];
}

} /* namespace helpers */
}
