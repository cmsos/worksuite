/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * FullDuplexTransport.cpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include <mpila/TransportTestSome.hpp>
#include <iostream>
#include <string>
#include <thread>
#include <memory>
#include <vector>

#include "mpila/ChannelTestSome.hpp"

namespace mpila
{

TransportTestSome::TransportTestSome(size_t pipelineDepth) : Transport(pipelineDepth)
{
}

void TransportTestSome::operator()()
{
    // for each send queue
    for (auto qp : queuePairs_)
    {
        auto cq = qp->completionQueue_;

        auto &sq = qp->sendQueue_;
        auto sendChannel = static_cast<ChannelTestSome *>(qp->sendChannel_.get());
        auto &senderPendingworkRequests = sendChannel->getPendingWorkRequests();
        auto &senderFreeRequestsList = sendChannel->getFreeIndexList();
        auto &sendChannelCounters = sendChannel->getCounters();

        auto &rq = qp->recvQueue_;
        auto recvChannel = static_cast<ChannelTestSome *>(qp->recvChannel_.get());
        auto &recvPendingWorkRequests = recvChannel->getPendingWorkRequests();
        auto &recvFreeRequestsList = recvChannel->getFreeIndexList();
        auto &recvChannelCounters = recvChannel->getCounters();

        bool canSend = false;
        bool canReceive = false;
        do
        {
            //-------------------------------------
            // receive
            //-------------------------------------
            canReceive = !rq.empty() && !recvFreeRequestsList.empty();
            if (canReceive)
            {
                int recvIndex = recvFreeRequestsList.front();
                recvFreeRequestsList.pop_front();

                recvPendingWorkRequests[recvIndex] = rq.front();
                rq.pop_front();
#ifndef DISABLE_MPI
                WorkRequest &workRequest = recvPendingWorkRequests[recvIndex];
#ifdef DEBUG
                if (workRequest.workRequestType != mpila::WorkRequestType::RECV)
                {
                    std::cerr << "FATAL ERROR - trying to recv ";
                }
#endif
                int ierr = MPI_Irecv(workRequest.memoryBuffer, workRequest.size, MPI_BYTE, qp->destinationRank_,
                                     workRequest.tag, MPI_COMM_WORLD, &recvChannel->getMpiRequests()[recvIndex]);
                handleMpiError(ierr, "Receiver failed at MPI_Irecv");
#endif
                recvChannelCounters.postedRequest();
            }
            else
            {
                recvChannelCounters.idlePostCounter_++;
            }

            //-------------------------------------
            // send
            //-------------------------------------
            canSend = !sq.empty() && !senderFreeRequestsList.empty();
            if (canSend)
            {
                int sendIndex = senderFreeRequestsList.front();
                senderFreeRequestsList.pop_front();

                senderPendingworkRequests[sendIndex] = sq.front();
                sq.pop_front();
#ifndef DISABLE_MPI
                WorkRequest &workRequest = senderPendingworkRequests[sendIndex];
#ifdef DEBUG
                if (workRequest.workRequestType != mpila::WorkRequestType::SEND)
                {
                    std::cerr << "FATAL ERROR - trying to send ";
                }
#endif
                int ierr = MPI_Issend(workRequest.memoryBuffer, workRequest.size, MPI_BYTE, qp->destinationRank_,
                                     workRequest.tag, MPI_COMM_WORLD, &sendChannel->getMpiRequests()[sendIndex]);
                handleMpiError(ierr, "Sender failed at MPI_Isend");
#endif
                sendChannelCounters.postedRequest();
            }
            else
            {
                sendChannelCounters.idlePostCounter_++;
            }
        } while (canSend || canReceive);

        //-------------------------------------
        // check completion of sends
        //-------------------------------------
        if (senderFreeRequestsList.elements() < pipelineDepth_)
        {
#ifndef DISABLE_MPI
            int completionCount = 0;
            std::vector<int> completionFlags(sendChannel->getMpiRequests().size());

            int ierr = MPI_Testsome(sendChannel->getMpiRequests().size(), sendChannel->getMpiRequests().data(),
                                    &completionCount, completionFlags.data(), MPI_STATUSES_IGNORE);
            handleMpiError(ierr, "Sender failed at MPI_Testsome");
#else
            int completionCount = senderFreeRequestsList.elements();
#endif

            if (completionCount > 0 && completionCount != MPI_UNDEFINED)
            {
                for (int i = 0; i < completionCount; i++)
                {
#ifndef DISABLE_MPI
                    const WorkRequest &workRequest = senderPendingworkRequests[completionFlags[i]];
#else
                    const WorkRequest &workRequest = senderPendingworkRequests[i];
#endif
#ifdef DEBUG
                    if (workRequest.workRequestType != mpila::WorkRequestType::SEND)
                    {
                        std::cerr << "FATAL ERROR - trying to comlete send ";
                    }
#endif
                    CompletionEvent opCompleted(workRequest, 0, EventStatus::COMPLETED);

                    cq->push_back(opCompleted);
#ifndef DISABLE_MPI
                    senderFreeRequestsList.push_back(completionFlags[i]);
#else
                    senderFreeRequestsList.push_back(i);
#endif
                    sendChannelCounters.completedRequest();
                }
            }
            else
            {
                sendChannelCounters.idleWaitCycleCounter_++;
            }
        }
        else
        {
            sendChannelCounters.starvingCycleCounter_++;
        }

        //-------------------------------------
        // check completion of recvs
        //-------------------------------------
        if (recvFreeRequestsList.elements() < pipelineDepth_)
        {
#ifndef DISABLE_MPI
            int completionCount = 0;
            std::vector<int> completionFlags(recvChannel->getMpiRequests().size());
            int ierr = MPI_Testsome(recvChannel->getMpiRequests().size(), recvChannel->getMpiRequests().data(),
                                    &completionCount, completionFlags.data(), recvChannel->getMpiStatuses().data());
            handleMpiError(ierr, "Receiver failed at MPI_Testsome");
#else
            int completionCount = recvFreeRequestsList.elements();
#endif
            if (completionCount > 0 && completionCount != MPI_UNDEFINED)
            {
                for (int i = 0; i < completionCount; i++)
                {
#ifndef DISABLE_MPI
                    const WorkRequest &workRequest = recvPendingWorkRequests[completionFlags[i]];
#else
                    const WorkRequest &workRequest = recvPendingWorkRequests[i];
#endif
#ifdef DEBUG
                    if (workRequest.workRequestType != mpila::WorkRequestType::RECV)
                    {
                        std::cerr << "FATAL ERROR - trying to comlete send ";
                    }
#endif

                    int recvSize = -1;
#ifndef DISABLE_MPI
                    const MPI_Status &status = recvChannel->getMpiStatuses()[completionFlags[i]];
                    MPI_Get_count(&status, MPI_BYTE, &recvSize);
#endif
                    const CompletionEvent opCompleted(workRequest, recvSize, EventStatus::COMPLETED);
                    cq->push_back(opCompleted);
#ifndef DISABLE_MPI
                    recvFreeRequestsList.push_back(completionFlags[i]);
#else
                    recvFreeRequestsList.push_back(i);
#endif
                    recvChannelCounters.completedRequest();
                }
            }
            else
            {
                recvChannelCounters.idleWaitCycleCounter_++;
            }
        }
        else
        {
            recvChannelCounters.starvingCycleCounter_++;
        }
    }
}

void TransportTestSome::clearQPs()
{
    for (auto qp : queuePairs_)
    {
        auto cq = qp->completionQueue_;

        auto sendChannel = static_cast<ChannelTestSome *>(qp->sendChannel_.get());
        auto &sq = qp->sendQueue_;
        auto &senderFreeIndexList = sendChannel->getFreeIndexList();

        auto recvChannel = static_cast<ChannelTestSome *>(qp->recvChannel_.get());
        auto &rq = qp->recvQueue_;
        auto &recvPendingRequests = recvChannel->getPendingWorkRequests();

#ifdef DEBUG
        int rank = 0;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif

        //-------------------------------------
        // check if sends have been completed
        //-------------------------------------
        if (!(senderFreeIndexList.elements() == pipelineDepth_ && sq.empty()))
        {
#ifdef DEBUG
            std::stringstream ss;
            ss << "rank [" << rank << "]pending requests: " << pipelineDepth_ - senderFreeIndexList.elements()
               << " sq contains: " << sq.elements() << std::endl;
            std::cout << ss.str();
#endif
            throw std::logic_error("to clear the QPs all sends must have been terminated");
        }
#ifdef DEBUG
        else
        {
            std::stringstream ss;
            ss << "rank [" << rank << "] senders are clear for destination " << qp->getRank() << std::endl;
            std::cout << ss.str();
        }
#endif

        //-------------------------------------
        // cancel all pending requests
        //-------------------------------------
        size_t numPending = recvPendingRequests.size();
        for (size_t i = 0; i < numPending; i++)
        {
            MPI_Request *req = &recvChannel->getMpiRequests()[i];
            // only cancel active requests
            if (*req != MPI_REQUEST_NULL)
            {
                MPI_Status status;
                int flag = false;
                int ierr = MPI_Cancel(req);
                handleMpiError(ierr, "failed to cancel MPI request while clearing QP");

#ifdef DEBUG
                std::stringstream ss;
                ss << "rank [" << rank << "] waiting for cancellation of receives from rank " << qp->getRank()
                   << std::endl;
#endif

                ierr = MPI_Wait(req, &status);
                handleMpiError(ierr, "failed completing cancellation of MPI request while clearing QP");
                ierr = MPI_Test_cancelled(&status, &flag);
                handleMpiError(ierr, "failed checking cancellation completion of MPI request while clearing QP");
                if (!flag)
                {
                    throw std::logic_error("Failed to cancel a Irecv request while clearing QP");
                }
                const WorkRequest &workRequest = recvPendingRequests[i];
                CompletionEvent opCompleted(workRequest, workRequest.size, EventStatus::ERROR);
                cq->push_back(opCompleted);
            }
        }

        //-------------------------------------
        // clear all recvRequest in RQ
        //-------------------------------------
        while (!rq.empty())
        {
            const WorkRequest &workRequest = rq.front();
            CompletionEvent opCompleted(workRequest, workRequest.size, EventStatus::ERROR);
            rq.pop_front();
            cq->push_back(opCompleted);
        }
    }
}
} /* namespace mpila */
