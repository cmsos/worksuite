/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * PendingRequest.cpp
 *
 *  Created on: Jan 17, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */
#include "mpila/PendingRequest.hpp"

namespace mpila
{

PendingRequest::PendingRequest() : workRequest(), mpiRequest(MPI_REQUEST_NULL)
{
}

void PendingRequest::fromWorkRequest(const WorkRequest& req)
{
    workRequest = req;
    mpiRequest = MPI_REQUEST_NULL;
}

} /* namespace mpila */
