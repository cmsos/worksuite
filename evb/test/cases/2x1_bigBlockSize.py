from TestCase import TestCase
from Context import RU,BU


class case_2x1_bigBlockSize(TestCase):

    def runTest(self):
        self.configureEvB()
        self.enableEvB()
        self.checkEVM(12*4096,10)
        self.checkRU(12*4096)
        self.checkBU(24*4096,10)
        self.haltEvB()


    def fillConfiguration(self,symbolMap):
        blockSize='0xfffc0'
        ruParams = [
            ('inputSource','string','Local'),
            ('blockSize','unsignedInt',blockSize)]
        ruParams.extend(self.getFedParams(range(13,25),4096,0))
        self._config.add( RU(symbolMap,ruParams,blockSize) )
        self._config.add( RU(symbolMap,ruParams,blockSize) )
        self._config.add( BU(symbolMap,[
             ('dropEventData','boolean','true'),
             ('lumiSectionTimeout','unsignedInt','0'),
             ('eventsPerRequest','unsignedInt','64'),
             ('maxEvtsUnderConstruction','unsignedInt','256')
            ],blockSize) )
