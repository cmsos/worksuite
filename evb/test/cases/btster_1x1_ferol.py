from TestCase import TestCase
from Context import FEROL,RU,BU,BTESTER
import time

class case_btster_1x1_ferol(TestCase):

    def runTest(self):
        self.configureBT()
        self.enableBT()
        sec = float(input("Enter the value of seconds to run the test: "))
        print('Going to sleep for', sec, 'seconds.')
        time.sleep(int(sec))
        print('Enough of sleeping, I Quit!')
        self.stopBT()
        self.haltBT()


    def fillConfiguration(self,symbolMap):
        bTester = BTESTER(symbolMap,[
             ('sampleTime','string','PT5S')
            ])
        self._config.add( FEROL(symbolMap,bTester,512) )
        self._config.add( bTester )
