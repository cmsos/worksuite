import os
import xmlrpc.client
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import QName as QN

import Application
import SymbolMap
import XMLtools


class Context:
    ptInstance = 0

    def __init__(self,role,evbType,hostinfo):
        self.role = role
        self.evbType = evbType
        self.hostinfo = hostinfo
        # FEROL application does not have i2oHostaname and does not need policyElements
        if self.hostinfo.get('i2oHostname') == None:
            self.policyElements = []
        #elif 'd3vrubu-c2e33-06-01' in self.hostinfo['i2oHostname'] or 'd3vrubu-c2e33-08-01' in self.hostinfo['i2oHostname'] or\
        #        'd3vrubu-c2e33-10-01' in self.hostinfo['i2oHostname'] or 'd3vrubu-c2e33-12-01' in self.hostinfo['i2oHostname'] or\
        #'rubu-c2a15' in self.hostinfo['i2oHostname'] or 'rubu-c2a14' in self.hostinfo['i2oHostname'] or 'rubu-c2a11' in self.hostinfo['i2oHostname']:
        #            self.policyElements = []
        else:
            self.polns = 'http://xdaq.web.cern.ch/xdaq/xsd/2013/XDAQPolicy-10'
            try:
                self.policyElements = self.getPolicyElements()
            except KeyError:
                self.policyElements = []
        self.applications = [Application.Application('xmem::probe::Application',Context.ptInstance,[]),]


    def getPolicyElements(self):
        return []


    def getContext(self,ns,useNuma,fullConfig=True):
        context = ET.Element(QN(ns,'Context'))
        context.set('url',"http://%(soapHostname)s:%(soapPort)s" % self.hostinfo)
        if useNuma and fullConfig:
            self.addPolicy(context)
        for app in self.applications:
            if fullConfig:
                app.addInContext(context,ns)
            else:
                app.addI2OEndpointInContext(context,ns)
                if app.params['class'].startswith('evb::') or app.params['class'].startswith('gevb2g::'):
                    context.append( app.getApplicationContext(ns) )
        return context


    def addPeerTransport(self,maxMessageSize):
        try:
            # Current DAQ3VAL machineswith CC8 and RoCE
            if 'd3vrubu-c2e33-06-01' in self.hostinfo['i2oHostname'] or 'd3vrubu-c2e33-08-01' in self.hostinfo['i2oHostname'] or\
                'd3vrubu-c2e33-10-01' in self.hostinfo['i2oHostname'] or 'd3vrubu-c2e33-12-01' in self.hostinfo['i2oHostname'] or\
                'rubu-c2a11' in self.hostinfo['i2oHostname'] or 'rubu-c2a14' in self.hostinfo['i2oHostname'] or 'rubu-c2a15' in self.hostinfo['i2oHostname']:
                app = self.getPtIbvApplication(maxMessageSize,True)
                #app = self.getPtUtcpApplication(maxMessageSize)
            elif 'd3v' in self.hostinfo['i2oHostname'] or 'rbs1v0' in self.hostinfo['i2oHostname'] or 'ebs0v0' in self.hostinfo['i2oHostname'] or 'ebs1v0' in self.hostinfo['i2oHostname']:
                app = self.getPtIbvApplication(maxMessageSize,False)
            else:
                app = self.getPtUtcpApplication(maxMessageSize)
            app.params['i2oHostname'] = self.hostinfo['i2oHostname']
            app.params['i2oPort'] =  self.hostinfo['i2oPort']
            app.params['network'] = 'evb'
            self.applications.append(app)
            Context.ptInstance += 1
        except KeyError as e:
            raise KeyError("Cannot find key "+str(e)+" for "+self.hostinfo['soapHostname'])


    def getPtUtcpApplication(self,maxMessageSize):
        properties = [
            ('protocol','string','utcp'),
            ('maxClients','unsignedInt','9'),
            ('autoConnect','boolean','false'),
            ('ioQueueSize','unsignedInt','65536'),
            ('eventQueueSize','unsignedInt','65536'),
            ('maxReceiveBuffers','unsignedInt','12'),
            ('maxBlockSize','unsignedInt',maxMessageSize)
            ]
        app = Application.Application('pt::utcp::Application',Context.ptInstance,properties)
        app.params['protocol'] = 'utcp'
        app.params['network'] = 'local'
        app.params['logpolicy'] = 'inherit'
        return app

    def getPtIbvApplication(self,maxMessageSize,isRoCE):
        interface = SymbolMap.getI2OInterfaceName(self.hostinfo['i2oHostname'])
        properties = []
        if not isRoCE:
            properties = [('iaName','string',interface)]
        else:
            properties = [
                ('networkInterface','string',interface),
                ('GIDType','string','v2'),
                ('portNumber','unsignedInt','1')
                ]
        properties.extend([('deviceMTU','unsignedInt','4096')])
        if self.evbType == 'GEVB':
            if self.role != 'EVM':
                properties.extend([
                    ('maxMessageSize','unsignedInt',maxMessageSize)
                    ])
        else:
            properties.extend([
                ('sendPoolName','string','sudapl'),
                ('recvPoolName','string','rudapl'),
                ('memAllocTimeout','string','PT1S'),
                ('sendWithTimeout','boolean','true'),
                ('useRelay','boolean','false'),
                ('maxMessageSize','unsignedInt',maxMessageSize)
                ])
        if self.role == 'EVM':
            if self.evbType == 'GEVB':
                properties.extend([
                    ('maxMessageSize','unsignedInt','65300'),
                    ('senderPoolSize','unsignedLong','0xA95C300'),
                    ('receiverPoolSize','unsignedLong','0x80000000'),
                    ('completionQueueSize','unsignedInt','65536'),
                    ('sendQueuePairSize','unsignedInt','1024'),
                    ('recvQueuePairSize','unsignedInt','512')
                ])
            else:
                properties.extend([
                    ('senderPoolSize','unsignedLong','0x2A570C00'),
                    ('receiverPoolSize','unsignedLong','0x5D70A400'),
                    ('completionQueueSize','unsignedInt','482160'),
                    ('sendQueuePairSize','unsignedInt','5840'),
                    ('recvQueuePairSize','unsignedInt','80')
                ])
        elif self.role == 'RU':
            if self.evbType == 'GEVB':
                properties.extend([
                    ('senderPoolSize','unsignedLong','0x200000000'), # 23 RUs and 23 BUs 0x80000000
                    ('receiverPoolSize','unsignedLong','0x2000000'),
                    ('completionQueueSize','unsignedInt','90000'),
                    ('sendQueuePairSize','unsignedInt','8192'), # 23 RUs and 23 BUs 8192
                    ('recvQueuePairSize','unsignedInt','64')
                ])
            else:
                properties.extend([
                    ('senderPoolSize','unsignedLong','0xBAE14800'),
                    ('receiverPoolSize','unsignedLong','0x5D70A400'),
                    ('completionQueueSize','unsignedInt','23860'),
                    ('sendQueuePairSize','unsignedInt','320'),
                    ('recvQueuePairSize','unsignedInt','500')
                ])
        elif self.role == 'BU':
            if self.evbType == 'GEVB':
                properties.extend([
                    ('senderPoolSize','unsignedLong','0x4000000'),
                    ('receiverPoolSize','unsignedLong','0x200000000'),
                    ('completionQueueSize','unsignedInt','32768'),
                    ('sendQueuePairSize','unsignedInt','64'),
                    ('recvQueuePairSize','unsignedInt','384')
                ])
            else:
                properties.extend([
                    ('senderPoolSize','unsignedLong','0xA3D800'),
                    ('receiverPoolSize','unsignedLong','0x1788E3800'),
                    ('completionQueueSize','unsignedInt','32400'),
                    ('sendQueuePairSize','unsignedInt','80'),
                    ('recvQueuePairSize','unsignedInt','320')
                ])
        elif self.role == 'EVMBU':
            if self.evbType == 'GEVB':
                properties.extend([
                    ('senderPoolSize','unsignedLong','0xFA7D0000'),
                    ('receiverPoolSize','unsignedLong','0xFA7D0000'), #maximum pool size with 65536 buffers of 131072 bytes
                    ('completionQueueSize','unsignedInt','16384'),
                    ('sendQueuePairSize','unsignedInt','8192'),
                    ('recvQueuePairSize','unsignedInt','4092')
                ])
            else:
                properties.extend([
                    ('senderPoolSize','unsignedLong','0x200000000'),
                    ('receiverPoolSize','unsignedLong','0x200000000'), #maximum pool size with 65536 buffers of 131072 bytes
                    ('completionQueueSize','unsignedInt','32400'),
                    #('sendQueuePairSize','unsignedInt','1024'),
                    #('recvQueuePairSize','unsignedInt','1024')
                    ('sendQueuePairSize','unsignedInt','512'),
                    ('recvQueuePairSize','unsignedInt','512')
                ])
        elif self.role == 'RUBU':
            if self.evbType == 'GEVB':
                properties.extend([
                    ('senderPoolSize','unsignedLong','0x200000000'),
                    ('receiverPoolSize','unsignedLong','0x175555554'),  # 0x155555554 previuos memory pool till 40 RUBUs
                    ('completionQueueSize','unsignedInt','40960'),
                    ('sendQueuePairSize','unsignedInt','384'), # large buffers 4096
                    ('recvQueuePairSize','unsignedInt','384')
                ])
            else:
                properties.extend([
                    ('senderPoolSize','unsignedLong','0x200000000'),
                    ('receiverPoolSize','unsignedLong','0x200000000'), #maximum pool size with 65536 buffers of 131072 bytes
                    #('completionQueueSize','unsignedInt','40960'),
                    ('completionQueueSize','unsignedInt','32400'),
                    #('sendQueuePairSize','unsignedInt','384'),
                    ('sendQueuePairSize','unsignedInt','512'),
                    #('recvQueuePairSize','unsignedInt','384')
                    ('recvQueuePairSize','unsignedInt','512')
                ])

        app = Application.Application('pt::ibv::Application',Context.ptInstance,properties)
        app.params['protocol'] = 'ibv'
        return app


    def getNumaInfo(self):
        try:
            server = xmlrpc.client.ServerProxy("http://%s:%s" % (self.hostinfo['soapHostname'],self.hostinfo['launcherPort']))
            #print("Hostname=",self.hostinfo['soapHostname']," numaInfo=",server.getNumaInfo())
            return server.getNumaInfo()
        except xmlrpc.client.Fault:
            return {
                # Default setup of R7515
                '0': ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61'],
                'ethCPU': '0'
                }
        except Exception as e:
            raise Exception("Failed to get NUMA information from "+self.hostinfo['soapHostname']+": "+str(e))


    def addPolicy(self,context):
        if self.policyElements:
            policy = ET.Element(QN(self.polns,'policy'))
            for element in self.policyElements:
                el = ET.Element(QN(self.polns,'element'),element)
                policy.append(el)
            context.append(policy)


    def extractPolicy(self,context):
        policy = context.find(QN(self.polns,'policy').text)
        if policy is not None:
            for element in policy:
                self.policyElements.append(element.attrib)


class FEROL(Context):
    instance = 0

    def __init__(self,symbolMap,destination,fedId,properties=[]):
        Context.__init__(self,'FEROL','EVM',symbolMap.getHostInfo('FEROL'+str(FEROL.instance)))
        if FEROL.instance % 2:
            frlPort = 'frlPort1'
        else:
            frlPort = 'frlPort2'
        prop = [
            ('fedId','unsignedInt',str(fedId)),
            ('sourceHost','string',self.hostinfo['frlHostname']),
            ('sourcePort','unsignedInt',str(self.hostinfo[frlPort])),
            ('destinationHost','string',destination.hostinfo['frlHostname']),
            ('destinationPort','unsignedInt',str(destination.hostinfo[frlPort]))
            ]
        prop.extend(properties)
        app = Application.Application('evb::test::DummyFEROL',FEROL.instance,prop)
        self.applications.append(app)
        FEROL.instance += 1

        ferolSource = {}
        ferolSource['frlHostname'] = self.hostinfo['frlHostname']
        ferolSource['frlPort'] = self.hostinfo[frlPort]
        ferolSource['fedId'] = str(fedId)
        for app in destination.applications:
            app.addFerolSource(ferolSource)


    def __del__(self):
        FEROL.instance = 0


class GEVB_RU(Context):
    instance = 0

    def __init__(self,symbolMap,properties=[],maxMessageSize='0x40000'):
        evbType = 'GEVB'
        if RU.instance == 0:
            role = 'EVM'
        else:
            role = 'RU'
        Context.__init__(self,role,evbType,symbolMap.getHostInfo('RU'+str(RU.instance)))
        self.addPeerTransport(maxMessageSize)
        self.addRuApplication(maxMessageSize,properties)
        RU.instance += 1

    def addRuApplication(self,maxMessageSize,properties):

        if self.role in ('EVM','EVMBU'):
            app = Application.Application('gevb2g::EVM',RU.instance,properties)
            app.params['tid'] = '1'
            app.params['id'] = '50'
            app.params['network'] = 'evb'
        else:
            currentInstane = RU.instance-1
            app = Application.Application('gevb2g::RU',currentInstane,properties)
            app.params['tid'] = str(10+currentInstane)
            app.params['id'] = str(50+currentInstane)
            app.params['network'] = 'evb'
            # Add Input Emulator
            inputEmulatorProperties = [
                ('StdDev','unsignedLong','256'),
                ('Mean','unsignedLong','12'),
                ('MinFragmentSize','unsignedLong','16'),
                ('MaxFragmentSize','unsignedLong',maxMessageSize),
                ('destinationClassName','string','gevb2g::RU'),
                ('destinationClassInstance','unsignedLong',str(currentInstane)),
                ('maxDataFrameSize','unsignedLong',maxMessageSize),
                ('frameSendCounter','unsignedLong','0'),
                ('fixedSize','boolean','true'),
                ('rate','unsignedLong','1000000'),
                ('createPool','boolean','false'),
                ('poolName','string','sibv')
                ]
            inputEmulatorApp= Application.Application('gevb2g::InputEmulator',currentInstane,inputEmulatorProperties)
            inputEmulatorApp.params['tid'] = str(200+currentInstane)
            inputEmulatorApp.params['id'] = str(200+currentInstane)
            inputEmulatorApp.params['network'] = 'evb'
            self.applications.append(inputEmulatorApp)
        self.applications.append(app)

    def getPolicyElements(self):
        policyElements = []
        return policyElements

class GEVB_BU(Context):
    instance = 0


    def __init__(self,symbolMap,properties=[],maxMessageSize='0x40000'):
        evbType = 'GEVB'
        role = 'BU'
        Context.__init__(self,role,evbType,symbolMap.getHostInfo('BU'+str(BU.instance)))
        self.addPeerTransport(maxMessageSize)
        self.addBuApplication(properties)
        BU.instance += 1


    def addBuApplication(self,properties):
        app = Application.Application('gevb2g::BU',BU.instance,properties)
        app.params['tid'] = str(100+BU.instance)
        app.params['id'] = str(100+BU.instance)
        app.params['network'] = 'evb'
        self.applications.append(app)

    def getPolicyElements(self):
        policyElements = []
        return policyElements

class GEVB_RUBU(GEVB_RU):
    def __init__(self,symbolMap,ruProperties=[],buProperties=[],maxMessageSize='0x40000'):
        evbType = 'GEVB'
        if RU.instance == 0:
            role = 'EVMBU'
        else:
            role = 'RUBU'
        Context.__init__(self,role,evbType,symbolMap.getHostInfo('RU'+str(RU.instance)))
        self.addPeerTransport(maxMessageSize)
        self.addRuApplication(maxMessageSize,ruProperties)
        self.addBuApplication(buProperties)
        RU.instance += 1


    def addBuApplication(self,properties):
        currentInstane = RU.instance-1
        app = Application.Application('gevb2g::BU',currentInstane,properties)
        app.params['tid'] = str(100+currentInstane)
        app.params['id'] = str(100+currentInstane)
        app.params['network'] = 'evb'
        self.applications.append(app)

    def getPolicyElements(self):
        policyElements = []
        return policyElements


class BTESTER(Context):
    instance = 0

    #def __init__(self,symbolMap,properties=[],maxMessageSize='0x40000'):
    def __init__(self,symbolMap,properties=[],maxMessageSize='0x3fff0'):
        evbType = 'EVB'
        role = 'EVM'
        Context.__init__(self,role,evbType,symbolMap.getHostInfo('RU'+str(RU.instance)))
        #self.addPeerTransport(maxMessageSize)
        self.addBtesterApplication(properties)
        RU.instance += 1


    def addBtesterApplication(self,properties):
        app = Application.Application('pt::blit::btester',RU.instance,properties)
        app.params['tid'] = '1'
        app.params['id'] = '50'
        app.params['network'] = 'local'
        inputSource = app.getInputSource()
        self.addPtBlitApplication()
        self.applications.append(app)

    def addPtBlitApplication(self):
        #maxBlockSize='0x40000'
        maxBlockSize='0x3fff0'
        properties = [
            ('maxClients','unsignedInt','64'),
            ('maxReceiveBuffers','unsignedInt','16'),
            ('maxBlockSize','unsignedInt',maxBlockSize)
            ]
        app = Application.Application('pt::blit::Application',Context.ptInstance,properties)
        Context.ptInstance += 1
        app.params['frlHostname'] = self.hostinfo['frlHostname']
        app.params['frlPort1'] = self.hostinfo['frlPort1']
        app.params['frlPort2'] = self.hostinfo['frlPort2']
        app.params['maxbulksize'] = maxBlockSize
        self.applications.append(app)


    def getPolicyElements(self):
        numaInfo = self.getNumaInfo()
        #print self.hostinfo['soapHostname'],numaInfo

        # extracted from XML policy with
        # egrep -o '[[:alnum:]]+=".*"' scripts/tmp.txt |sed -re 's/([[:alnum:]]+)=/"\1":/g'|tr "\"" "'"|tr " " ","|sed -re 's/(.*)/{\1},/'
        policyElements = [
            {'cpunodes':numaInfo['ethCPU'],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::acceptor(.*)/waiting','type':'thread'},
            {'cpunodes':numaInfo['ethCPU'],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::eventworkloop/polling','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][1],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloopr(.*)/polling','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][3],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloops(.*)/polling','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][8],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_2/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][12],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_3/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][13],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_4/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][10],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_5/waiting','type':'thread'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-ibv-receiver(.+*):ibvla','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-ibv-sender(.*):ibvla','type':'alloc'},
            {'cpunodes':numaInfo['ibvCPU'],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/monitoring/waiting','type':'thread'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:fragmentRequestFIFO(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:fragmentFIFO_FED(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:frameFIFO_BU(.+)','type':'alloc'},
            {'affinity':numaInfo[numaInfo['ethCPU']][7],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia(.+)/'+str(self.hostinfo['i2oHostname'])+':btcp/(.*)','type':'thread'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-blit-socket(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:pt-blit-inputpipe-rlist(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:socketBufferFIFO(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:grantFIFO(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:tcpla-(.*)/'+str(self.hostinfo['i2oHostname'])+'(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:pt::ibv::(.*)','type':'alloc'}
        ]
        try:
            policyElements.extend([
                {'affinity':numaInfo[numaInfo['ethCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort1'])+'/(.*)','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][6],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort2'])+'/(.*)','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort1'])+'/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][6],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort2'])+'/waiting','type':'thread'}
                ])
        except KeyError:
            pass

        if RU.instance == 0: #EVM
            workerCores = (1,3,6,9,11,12,14,15)
            policyElements.extend([
                {'affinity':numaInfo[numaInfo['ibvCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][5],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/processRequests/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][7],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/buPoster/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][7],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/dummySuperFragment/waiting','type':'thread'},
                {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:readoutMsgFIFO(.+)','type':'alloc'}
                ])
        else: #RU
            workerCores = (0,1,3,6,7,9,11,14,15)
            policyElements.extend([
                {'affinity':numaInfo[numaInfo['ibvCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][5],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/buPoster/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/dummySuperFragment/waiting','type':'thread'}
                ])
        policyElements.extend([
                {'affinity':','.join([numaInfo[numaInfo['ibvCPU']][i] for i in workerCores]),'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/parseSocketBuffers_(.*)/waiting','type':'thread'},
                {'affinity':','.join([numaInfo[numaInfo['ibvCPU']][i] for i in workerCores]),'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/generating_(.*)/waiting','type':'thread'}
                ])
        return policyElements

class RU(Context):
    instance = 0

    #def __init__(self,symbolMap,properties=[],maxMessageSize='0x40000'):
    def __init__(self,symbolMap,properties=[],maxMessageSize='0x3fff0'):
        evbType = 'EVB'
        if RU.instance == 0:
            role = 'EVM'
        else:
            role = 'RU'
        Context.__init__(self,role,evbType,symbolMap.getHostInfo('RU'+str(RU.instance)))
        self.addPeerTransport(maxMessageSize)
        self.addRuApplication(properties)
        RU.instance += 1


    def addRuApplication(self,properties):
        if self.role in ('EVM','EVMBU'):
            app = Application.Application('evb::EVM',RU.instance,properties)
            app.params['tid'] = '1'
            app.params['id'] = '50'
        else:
            app = Application.Application('evb::RU',RU.instance,properties)
            app.params['tid'] = str(10+RU.instance)
            app.params['id'] = str(50+RU.instance)
        if 'useLock' not in (p[0] for p in properties):
            properties.append(('useLock','boolean','false'));
        app.params['network'] = 'evb'
        inputSource = app.getInputSource()
        if inputSource == "Socket":
            self.addPtBlitApplication()
        self.applications.append(app)

    def addPtBlitApplication(self):
        #maxBlockSize='0x40000'
        maxBlockSize='0x3fff0'
        properties = [
            ('maxClients','unsignedInt','64'),
            ('maxReceiveBuffers','unsignedInt','16'),
            ('maxBlockSize','unsignedInt',maxBlockSize)
            ]
        app = Application.Application('pt::blit::Application',Context.ptInstance,properties)
        Context.ptInstance += 1
        app.params['frlHostname'] = self.hostinfo['frlHostname']
        app.params['frlPort1'] = self.hostinfo['frlPort1']
        app.params['frlPort2'] = self.hostinfo['frlPort2']
        app.params['maxbulksize'] = maxBlockSize
        self.applications.append(app)


    def getPolicyElements(self):
        numaInfo = self.getNumaInfo()
        #print self.hostinfo['soapHostname'],numaInfo

        # extracted from XML policy with
        # egrep -o '[[:alnum:]]+=".*"' scripts/tmp.txt |sed -re 's/([[:alnum:]]+)=/"\1":/g'|tr "\"" "'"|tr " " ","|sed -re 's/(.*)/{\1},/'
        policyElements = [
            {'cpunodes':numaInfo['ethCPU'],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::acceptor(.*)/waiting','type':'thread'},
            {'cpunodes':numaInfo['ethCPU'],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::eventworkloop/polling','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][1],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloopr(.*)/polling','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][3],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloops(.*)/polling','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][8],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_2/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][12],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_3/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][13],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_4/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][10],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_5/waiting','type':'thread'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-ibv-receiver(.+*):ibvla','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-ibv-sender(.*):ibvla','type':'alloc'},
            {'cpunodes':numaInfo['ibvCPU'],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/monitoring/waiting','type':'thread'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:fragmentRequestFIFO(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:fragmentFIFO_FED(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:frameFIFO_BU(.+)','type':'alloc'},
            {'affinity':numaInfo[numaInfo['ethCPU']][7],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia(.+)/'+str(self.hostinfo['i2oHostname'])+':btcp/(.*)','type':'thread'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-blit-socket(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:pt-blit-inputpipe-rlist(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:socketBufferFIFO(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:grantFIFO(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:tcpla-(.*)/'+str(self.hostinfo['i2oHostname'])+'(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:pt::ibv::(.*)','type':'alloc'}
        ]
        try:
            policyElements.extend([
                {'affinity':numaInfo[numaInfo['ethCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort1'])+'/(.*)','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][6],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort2'])+'/(.*)','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort1'])+'/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][6],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort2'])+'/waiting','type':'thread'}
                ])
        except KeyError:
            pass

        if RU.instance == 0: #EVM
            workerCores = (1,3,6,9,11,12,14,15)
            policyElements.extend([
                {'affinity':numaInfo[numaInfo['ibvCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][5],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/processRequests/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][7],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/buPoster/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][7],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/dummySuperFragment/waiting','type':'thread'},
                {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:readoutMsgFIFO(.+)','type':'alloc'}
                ])
        else: #RU
            workerCores = (0,1,3,6,7,9,11,14,15)
            policyElements.extend([
                {'affinity':numaInfo[numaInfo['ibvCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][5],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/buPoster/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/dummySuperFragment/waiting','type':'thread'}
                ])
        policyElements.extend([
                {'affinity':','.join([numaInfo[numaInfo['ibvCPU']][i] for i in workerCores]),'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/parseSocketBuffers_(.*)/waiting','type':'thread'},
                {'affinity':','.join([numaInfo[numaInfo['ibvCPU']][i] for i in workerCores]),'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/generating_(.*)/waiting','type':'thread'}
                ])
        return policyElements


class BU(Context):
    instance = 0
    #def __init__(self,symbolMap,properties=[],maxMessageSize='0x40000'):
    def __init__(self,symbolMap,properties=[],maxMessageSize='0x3fff0'):
        evbType = 'EVB'
        role = 'BU'
        Context.__init__(self,role,evbType,symbolMap.getHostInfo('BU'+str(BU.instance)))
        self.addPeerTransport(maxMessageSize)
        self.addBuApplication(properties)
        BU.instance += 1


    def addBuApplication(self,properties):
        app = Application.Application('evb::BU',BU.instance,properties)
        app.params['tid'] = str(100+BU.instance)
        app.params['id'] = str(100+BU.instance)
        app.params['network'] = 'evb'
        if 'useLock' not in (p[0] for p in properties):
            properties.append(('useLock','boolean','false'));
        self.applications.append(app)


    def getPolicyElements(self):
        numaInfo = self.getNumaInfo()
        #print self.hostinfo['soapHostname'],numaInfo

        policyElements = [
            {'affinity':numaInfo[numaInfo['ethCPU']][1],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_0/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_1/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][3],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_2/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_3/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][5],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_4/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][6],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_5/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][7],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_6/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][8],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_7/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][9],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_8/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][10],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_9 /waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][15],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/requestFragments/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][14],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/fileMover/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][13],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/lumiAccounting/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][11],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/resourceMonitor/waiting','type':'thread'},
            {'affinity':numaInfo[numaInfo['ethCPU']][12],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/monitoring/waiting','type':'thread'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:superFragmentFIFO(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:fileStatisticsFIFO_stream(.+):alloc','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:resourceFIFO:alloc','type':'alloc'},
            {'cpunodes':numaInfo['ethCPU'],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::acceptor-(.+)/waiting','type':'thread'},
            {'cpunodes':numaInfo['ethCPU'],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::eventworkloop/polling','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][5],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloops(.*)/polling','type':'thread'},
            {'affinity':numaInfo[numaInfo['ibvCPU']][6],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloopr(.*)/polling','type':'thread'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-ibv(.*):ibvla','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:pt::ibv::(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:undefined','type':'alloc'}
            ]
        return policyElements


class RUBU(RU):

    #def __init__(self,symbolMap,ruProperties=[],buProperties=[],maxMessageSize='0x40000'):
    def __init__(self,symbolMap,ruProperties=[],buProperties=[],maxMessageSize='0x3fff0'):
        type = 'EVB'
        if RU.instance == 0:
            role = 'EVMBU'
        else:
            role = 'RUBU'
        Context.__init__(self,role,type,symbolMap.getHostInfo('RU'+str(RU.instance)))
        self.addPeerTransport(maxMessageSize)
        self.addRuApplication(ruProperties)
        self.addBuApplication(buProperties)
        RU.instance += 1


    def addBuApplication(self,properties):
        app = Application.Application('evb::BU',RU.instance,properties)
        app.params['tid'] = str(200+RU.instance)
        app.params['id'] = str(200+RU.instance)
        app.params['network'] = 'evb'
        self.applications.append(app)


    def getPolicyElements(self):
        numaInfo = self.getNumaInfo()
        #print("Hostname=",self.hostinfo['soapHostname']," numaInfo=",numaInfo)
        dualSocket = True
        try:
            numaInfo['ibvCPU']
        except KeyError:
            dualSocket = False
        if not dualSocket:
            policyElements = [
                {'affinity':numaInfo[numaInfo['ibvCPU']][0],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/dummySuperFragment/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][6],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_2/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][8],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_3/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][12],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_4/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][14],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_5/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][16],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloops(.*)/polling','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][9],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloopr(.*)/polling','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][18],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:fifo/PeerTransport/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][9],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/buPoster/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][1],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][11],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_2/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][18],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_3/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][21],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_4/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][22],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_5/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][2],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][4],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][7],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_2/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][8],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_3/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][10],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_4/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][16],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_5/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][23],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_6/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][3],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/requestFragments/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][2],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/fileAccounting/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ibvCPU']][1],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/resourceMonitor/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][17],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/eventFragment/waiting','type':'thread'},
                {'cpunodes':numaInfo[numaInfo['ethCPU']][2],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::acceptor(.*)/waiting','type':'thread'},
                {'cpunodes':numaInfo[numaInfo['ethCPU']][5],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::eventworkloop/polling','type':'thread'},
                {'cpunodes':numaInfo[numaInfo['ethCPU']][13],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/monitoring/waiting','type':'thread'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:superFragmentFIFO(.+)','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:fileInfoFIFO:alloc','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:closedFileInfoFIFO_(.+):alloc','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:resourceFIFO:alloc','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:eventFIFO:alloc','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:eventFragmentFIFO:alloc','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-ibv-receiver(.*):ibvla','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-ibv-sender(.*):ibvla','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:pt::ibv::(.*)','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:fragmentRequestFIFO(.+)','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:fragmentFIFO_FED(.+)','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ethCPU'],'package':'numa','pattern':'urn:frameFIFO_BU(.+)','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:toolbox-mem-allocator-blit-socket(.*)','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:pt-blit-inputpipe-rlist(.*)','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:socketBufferFIFO(.*)','type':'alloc'},
                {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:grantFIFO(.*)','type':'alloc'}
                ]

            try:
                policyElements.extend([
                    {'affinity':numaInfo[numaInfo['ibvCPU']][3],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort1'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ibvCPU']][23],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort2'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ibvCPU']][6],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort3'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ibvCPU']][15],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort4'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ibvCPU']][19],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort1'])+'/waiting','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ibvCPU']][1],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort2'])+'/waiting','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ibvCPU']][27],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort3'])+'/waiting','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ibvCPU']][28],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort4'])+'/waiting','type':'thread'}
                    ])
            except KeyError:
                pass

            if RU.instance == 0: #EVM
                policyElements.extend([
                    {'affinity':numaInfo[numaInfo['ibvCPU']][15],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/processRequests/waiting','type':'thread'},
                    {'mempolicy':'onnode','node':numaInfo['ibvCPU'],'package':'numa','pattern':'urn:readoutMsgFIFO(.+)','type':'alloc'}
                    ])

            socketThreads = (1,5,9,13,17,21)
            policyElements.extend([
                {'affinity':','.join([numaInfo[numaInfo['ibvCPU']][i] for i in socketThreads]),'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/parseSocketBuffers_(.*)/waiting','type':'thread'},
                {'affinity':','.join([numaInfo[numaInfo['ibvCPU']][i] for i in socketThreads]),'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/generating_(.*)/waiting','type':'thread'},
                ])
        else:
            # DELL 7515
            socketThreads = (2,4,5,9,10,11,15,20,21,24,28,32,36,37,43,46,49,53,54,55,57,58,60,63)
            policyElements = [
                {'cpunodes':numaInfo[numaInfo['ethCPU']][15],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::acceptor(.*)/waiting','type':'thread'},
                {'cpunodes':numaInfo[numaInfo['ethCPU']][45],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::eventworkloop/polling','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][58],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloops(.*)/polling','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][22],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloopr(.*)/polling','type':'thread'},
                {'affinity':','.join([numaInfo[numaInfo['ethCPU']][i] for i in socketThreads]),'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/parseSocketBuffers_(.*)/waiting','type':'thread'},
                {'affinity':','.join([numaInfo[numaInfo['ethCPU']][i] for i in socketThreads]),'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/generating_(.*)/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][3],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][39],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][30],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_2/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][16],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_3/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][29],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_4/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][38],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_5/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][8],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_6/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][42],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Responder_7/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][20],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:fifo/PeerTransport/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][9],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/buPoster/waiting','type':'thread'},
                {'cpunodes':numaInfo[numaInfo['ethCPU']][55],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/monitoring/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][2],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][18],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][14],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_2/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][26],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_3/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][56],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_4/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][40],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Builder_5/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][12],'memnode':numaInfo['ibvCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_0/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][27],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_1/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][19],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_2/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][25],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_3/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][23],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_4/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][0],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_5/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][35],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Writer_6/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][8],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/eventFragment/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][23],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/requestFragments/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][57],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/fileAccounting/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][15],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/resourceMonitor/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][39],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-acceptor-dispatcher/(.*)/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][15],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-connector-dispatcher/(.*)/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][15],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-sender-dispatcher/(.*)/waiting','type':'thread'},
                {'affinity':numaInfo[numaInfo['ethCPU']][15],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-workloop/(.*)/waiting','type':'thread'}
                ]

            try:
                policyElements.extend([
                    {'affinity':numaInfo[numaInfo['ethCPU']][58],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort1'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][36],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort2'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][7],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort3'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][45],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort4'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][50],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort1'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][9],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort2'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][11],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort3'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][31],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+str(self.hostinfo['frlHostname'])+'([:/])'+str(self.hostinfo['frlPort4'])+'/(.*)','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][59],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort1'])+'/waiting','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][48],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort2'])+'/waiting','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][54],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort3'])+'/waiting','type':'thread'},
                    {'affinity':numaInfo[numaInfo['ethCPU']][38],'memnode':numaInfo['ethCPU'],'mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:evb::(.+)/Pipe_'+str(self.hostinfo['frlHostname'])+':'+str(self.hostinfo['frlPort4'])+'/waiting','type':'thread'}
                    ])
            except KeyError:
                pass

        return policyElements


def resetInstanceNumbers():
    # resets all instance numbers of the above classes
    # (these are class wide / static variables)
    Context.ptInstance = 0
    FEROL.instance = 0
    RU.instance = 0
    BU.instance = 0


if __name__ == "__main__":
    useNuma=True
    symbolMap = SymbolMap.SymbolMap(os.environ["EVB_TESTER_HOME"]+"/cases/standaloneSymbolMap.txt")
    xcns = 'http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30'
    partition = ET.Element(QN(xcns,'Partition'))
    evm = RU(symbolMap,[
        ('inputSource','string','Socket')
        ])
    for fedId in range(3):
        ferol = FEROL(symbolMap,evm,fedId)
        partition.append( ferol.getContext(xcns,useNuma) )
    partition.append( evm.getContext(xcns,useNuma) )

    ru1 = RU(symbolMap,[
        ('inputSource','string','Local'),
        ('fedSourceIds','unsignedInt',list(range(6,10)))
        ])
    partition.append( ru1.getContext(xcns,useNuma) )

    ru2 = RU(symbolMap,[
        ('inputSource','string','FEROL')
        ])
    for fedId in range(10,12):
        ferol = FEROL(symbolMap,ru2,fedId)
        partition.append( ferol.getContext(xcns,useNuma) )
    partition.append( ru2.getContext(xcns,useNuma) )

    bu = BU(symbolMap,[
        ('dropEventData','boolean','true'),
        ('lumiSectionTimeout','unsignedInt','0')
        ])
    partition.append( bu.getContext(xcns,useNuma) )

    rubu = RUBU(symbolMap,[
        ('inputSource','string','Local'),
        ('fedSourceIds','unsignedInt',range(6,10))],
        [('dropEventData','boolean','true'),
        ('lumiSectionTimeout','unsignedInt','0')
        ])
    partition.append( rubu.getContext(xcns,useNuma) )

    XMLtools.indent(partition)
    print( ET.tostring(partition) )
