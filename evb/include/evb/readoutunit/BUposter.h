#ifndef _evb_readoutunit_BUposter_h_
#define _evb_readoutunit_BUposter_h_

#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>

#include <atomic>
#include <cstdint>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <utility>

#include "cgicc/HTMLClasses.h"
#include "evb/Constants.h"
#include "evb/OneToOneQueue.h"
#include "evb/PerformanceMonitor.h"
#include "evb/readoutunit/StateMachine.h"
#include "i2o/utils/AddressMap.h"
#include "toolbox/lang/Class.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "xdata/Double.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Vector.h"
#include "xgi/Output.h"


namespace evb {

  namespace readoutunit { // namespace evb::readoutunit

    /**
     * \ingroup xdaqApps
     * \brief Post I2O messages to BU
     */

    template<class ReadoutUnit>
    class BUposter : public toolbox::lang::Class
    {

    public:

      BUposter(ReadoutUnit*);

      ~BUposter();

      /**
       * Send the bufRef to the BU
       */
      void sendFrame(const I2O_TID,toolbox::mem::Reference*);

      /**
       * Start processing events
       */
      void startProcessing();

      /**
       * Drain events
       */
      void drain() const;

      /**
       * Stop processing events
       */
      void stopProcessing();

      /**
       * Append the info space items to be published in the
       * monitoring info space to the InfoSpaceItems
       */
      void appendMonitoringItems(InfoSpaceItems&);

      /**
       * Update all values of the items put into the monitoring
       * info space. The caller has to make sure that the info
       * space where the items reside is locked and properly unlocked
       * after the call.
       */
      void updateMonitoringItems();

      /**
       * Return the poster FIFOs as HTML snipped
       */
      cgicc::div getPosterFIFOs() const;

      /**
       * Return a cgicc::table containing information for each BU connection
       */
      cgicc::table getStatisticsPerBU() const;


    private:

      void startPosterWorkLoop();
      bool postFrames(toolbox::task::WorkLoop*);

      ReadoutUnit* readoutUnit_;

      using FrameFIFO = OneToOneQueue<toolbox::mem::Reference*>;
      using FrameFIFOPtr = std::unique_ptr<FrameFIFO>;
      struct BUconnection {
        const I2O_TID tid;
        const xdaq::ApplicationDescriptor* bu;
        FrameFIFOPtr frameFIFO;
        mutable std::mutex frameFIFOmutex;
        uint64_t throughput;
        uint32_t i2oRate;
        double retryRate;
        uint32_t retryCount;
        PerformanceMonitor perf;
        mutable std::mutex perfMutex;

        BUconnection(const I2O_TID tid,FrameFIFOPtr&&);
      };
      using BUconnectionPtr = std::unique_ptr<BUconnection>;
      using BUconnections = std::map<I2O_TID,BUconnectionPtr>;
      BUconnections buConnections_;
      mutable boost::shared_mutex buConnectionsMutex_;

      toolbox::task::WorkLoop* posterWL_;
      toolbox::task::ActionSignature* posterAction_;
      volatile std::atomic<bool> active_;

      xdata::Vector<xdata::UnsignedInteger32> buTids_;
      xdata::Vector<xdata::UnsignedInteger64> throughputPerBU_;
      xdata::Vector<xdata::UnsignedInteger32> fragmentRatePerBU_;
      xdata::Vector<xdata::Double> retryRatePerBU_;
      xdata::Vector<xdata::UnsignedInteger32> retryCountPerBU_;

    };

  } } //namespace evb::readoutunit


////////////////////////////////////////////////////////////////////////////////
// Implementation follows                                                     //
////////////////////////////////////////////////////////////////////////////////


template<class ReadoutUnit>
evb::readoutunit::BUposter<ReadoutUnit>::BUposter(ReadoutUnit* readoutUnit) :
readoutUnit_(readoutUnit),
active_(false)
{
  startPosterWorkLoop();
}


template<class ReadoutUnit>
evb::readoutunit::BUposter<ReadoutUnit>::~BUposter()
{
  if ( posterWL_ && posterWL_->isActive() )
    posterWL_->cancel();
}


template<class ReadoutUnit>
void evb::readoutunit::BUposter<ReadoutUnit>::appendMonitoringItems(InfoSpaceItems& items)
{
  buTids_.clear();
  throughputPerBU_.clear();
  fragmentRatePerBU_.clear();
  retryRatePerBU_.clear();
  retryCountPerBU_.clear();

  items.add("buTids", &buTids_);
  items.add("throughputPerBU", &throughputPerBU_);
  items.add("fragmentRatePerBU", &fragmentRatePerBU_);
  items.add("retryRatePerBU", &retryRatePerBU_);
  items.add("retryCountPerBU", &retryCountPerBU_);
}


template<class ReadoutUnit>
void evb::readoutunit::BUposter<ReadoutUnit>::updateMonitoringItems()
{
  boost::shared_lock<boost::shared_mutex> sl(buConnectionsMutex_);

  const uint32_t nbConnections = buConnections_.size();
  buTids_.clear();
  buTids_.reserve(nbConnections);

  throughputPerBU_.clear();
  throughputPerBU_.reserve(nbConnections);

  fragmentRatePerBU_.clear();
  fragmentRatePerBU_.reserve(nbConnections);

  retryRatePerBU_.clear();
  retryRatePerBU_.reserve(nbConnections);
  retryCountPerBU_.clear();
  retryCountPerBU_.reserve(nbConnections);

  for (auto const& buConnection : buConnections_)
  {
    std::lock_guard<std::mutex> guard(buConnection.second->perfMutex);

    const double deltaT = buConnection.second->perf.deltaT();
    buConnection.second->throughput = buConnection.second->perf.throughput(deltaT);
    buConnection.second->i2oRate = buConnection.second->perf.i2oRate(deltaT);
    buConnection.second->retryRate = buConnection.second->perf.retryRate(deltaT);
    buConnection.second->retryCount = buConnection.second->perf.retryCount;
    buConnection.second->perf.reset();

    buTids_.push_back(buConnection.first);
    throughputPerBU_.push_back(buConnection.second->throughput);
    fragmentRatePerBU_.push_back(buConnection.second->i2oRate);
    retryRatePerBU_.push_back(buConnection.second->retryRate);
    retryCountPerBU_.push_back(buConnection.second->retryCount);
  }
}


template<class ReadoutUnit>
void evb::readoutunit::BUposter<ReadoutUnit>::startPosterWorkLoop()
{
  try
  {
    posterWL_ = toolbox::task::getWorkLoopFactory()->
      getWorkLoop( readoutUnit_->getIdentifier("buPoster"), "waiting" );

    posterAction_ =
      toolbox::task::bind(this, &evb::readoutunit::BUposter<ReadoutUnit>::postFrames,
                          readoutUnit_->getIdentifier("postFrames") );

    if ( ! posterWL_->isActive() )
      posterWL_->activate();

    posterWL_->submit(posterAction_);
  }
  catch(xcept::Exception& e)
  {
    std::string msg = "Failed to start workloop 'buPoster'";
    XCEPT_RETHROW(exception::WorkLoop, msg, e);
  }
}


template<class ReadoutUnit>
void evb::readoutunit::BUposter<ReadoutUnit>::startProcessing()
{
  {
    boost::unique_lock<boost::shared_mutex> ul(buConnectionsMutex_);
    buConnections_.clear();
  }
}


template<class ReadoutUnit>
void evb::readoutunit::BUposter<ReadoutUnit>::drain() const
{
  bool haveFrames = false;
  do
  {
    boost::shared_lock<boost::shared_mutex> sl(buConnectionsMutex_);

    haveFrames = false;
    for (auto const& buConnection : buConnections_)
    {
      haveFrames |= ( ! buConnection.second->frameFIFO->empty() );
    }
  } while ( haveFrames && ::usleep(1000) );
}


template<class ReadoutUnit>
void evb::readoutunit::BUposter<ReadoutUnit>::stopProcessing()
{
  while (active_) ::usleep(1000);
}


template<class ReadoutUnit>
void evb::readoutunit::BUposter<ReadoutUnit>::sendFrame(const I2O_TID tid, toolbox::mem::Reference* bufRef)
{
  boost::upgrade_lock<boost::shared_mutex> sl(buConnectionsMutex_);

  auto pos = buConnections_.find(tid);
  if ( pos == buConnections_.end() )
  {
    // new TID
    boost::upgrade_to_unique_lock<boost::shared_mutex> ul(sl);

    std::ostringstream name;
    name << "frameFIFO_BU" << tid;
    FrameFIFOPtr frameFIFO = std::make_unique<FrameFIFO>(readoutUnit_,name.str());
    frameFIFO->resize(readoutUnit_->getConfiguration()->fragmentRequestFIFOCapacity);
    pos = buConnections_.emplace_hint(pos,tid,std::make_unique<BUconnection>(tid,std::move(frameFIFO)));
  }
  {
    std::lock_guard<std::mutex> guard(pos->second->frameFIFOmutex);
    pos->second->frameFIFO->enqWait(std::move(bufRef));
  }
}


template<class ReadoutUnit>
bool evb::readoutunit::BUposter<ReadoutUnit>::postFrames(toolbox::task::WorkLoop*)
{
  bool workDone = false;
  toolbox::mem::Reference* bufRef;

  try
  {
    active_ = true;
    do {
      boost::shared_lock<boost::shared_mutex> sl(buConnectionsMutex_);

      workDone = false;
      for (auto const& buConnection : buConnections_)
      {
        if ( buConnection.second->frameFIFO->deq(bufRef) )
        {
          try
          {
            const uint32_t payloadSize = bufRef->getDataSize();
            const uint32_t retries = readoutUnit_->postMessage(bufRef,buConnection.second->bu);
            {
              std::lock_guard<std::mutex> guard(buConnection.second->perfMutex);
              buConnection.second->perf.retryCount += retries;
              buConnection.second->perf.sumOfSizes += payloadSize;
              buConnection.second->perf.sumOfSquares += payloadSize*payloadSize;
              buConnection.second->perf.i2oCount++;
            }
          }
          catch(exception::I2O& e)
          {
            std::ostringstream msg;
            msg << "Failed to send frame to BU TID ";
            msg << buConnection.first;
            XCEPT_RETHROW(exception::I2O, msg.str(), e);
          }
          workDone = true;
        }
      }
    } while ( workDone );

    active_ = false;
    ::usleep(100);
  }
  catch(xcept::Exception& e)
  {
    active_ = false;
    readoutUnit_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    active_ = false;
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, e.what());
    readoutUnit_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    active_ = false;
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, "unkown exception");
    readoutUnit_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }

  return true;
}


template<class ReadoutUnit>
cgicc::div evb::readoutunit::BUposter<ReadoutUnit>::getPosterFIFOs() const
{
  using namespace cgicc;

  cgicc::div div;
  boost::shared_lock<boost::shared_mutex> sl(buConnectionsMutex_);

  for (auto const& buConnection : buConnections_)
  {
    div.add(buConnection.second->frameFIFO->getHtmlSnipped());
  }

  return div;
}


template<class ReadoutUnit>
cgicc::table evb::readoutunit::BUposter<ReadoutUnit>::getStatisticsPerBU() const
{
  using namespace cgicc;

  table table;
  table.set("title","Statistics per BU connection");

  table.add(tr()
            .add(th("Statistics per BU").set("colspan","5")));
  table.add(tr()
            .add(td("Instance"))
            .add(td("TID"))
            .add(td("Throughput (MB/s)"))
            .add(td("I2O rate (Hz)"))
            .add(td("Retry rate (Hz)"))
            .add(td("Retry count")));

  boost::shared_lock<boost::shared_mutex> sl(buConnectionsMutex_);

  for (auto const& buConnection : buConnections_)
  {
    const xdaq::ApplicationDescriptor* bu = i2o::utils::getAddressMap()->getApplicationDescriptor(buConnection.first);
    const std::string url = bu->getContextDescriptor()->getURL() + "/" + bu->getURN();
    table.add(tr()
              .add(td()
                   .add(a("BU "+std::to_string(bu->getInstance())).set("href",url).set("target","_blank")))
              .add(td(std::to_string(buConnection.first)))
              .add(td(doubleToString(buConnection.second->throughput / 1e6,2)))
              .add(td(std::to_string(buConnection.second->i2oRate)))
              .add(td(doubleToString(buConnection.second->retryRate,2)))
              .add(td(std::to_string(buConnection.second->retryCount))));
  }

  return table;
}


template<class ReadoutUnit>
evb::readoutunit::BUposter<ReadoutUnit>::BUconnection::BUconnection
(
  const I2O_TID tid,
  FrameFIFOPtr&& frameFIFO
)
  : tid(tid),frameFIFO(std::move(frameFIFO))
{
  try
  {
    bu = i2o::utils::getAddressMap()->getApplicationDescriptor(tid);
  }
  catch(xcept::Exception& e)
  {
    std::ostringstream msg;
    msg << "Failed to get application descriptor for BU with tid ";
    msg << tid;
    XCEPT_RAISE(exception::I2O, msg.str());
  }
}


#endif // _evb_readoutunit_BUposter_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
