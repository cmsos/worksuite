#ifndef _evb_version_h_
#define _evb_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_EVB_VERSION_MAJOR 6
#define WORKSUITE_EVB_VERSION_MINOR 12
#define WORKSUITE_EVB_VERSION_PATCH 1
#define WORKSUITE_EVB_PREVIOUS_VERSIONS "6.5.0,6.5.1,6.5.2,6.6.0,6.6.1,6.7.0,6.7.1,6.8.0,6.8.1,6.8.2,6.9.0,6.9.1,6.9.2,6.9.3,6.9.4,6.9.5,6.9.6,6.9.7,6.9.8,6.9.9,6.9.10,6.9.11,6.9.12,6.9.13,6.10.0,6.11.0,6.11.1,6.11.2,6.12.0"

#define WORKSUITE_EVB_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_EVB_VERSION_MAJOR,WORKSUITE_EVB_VERSION_MINOR,WORKSUITE_EVB_VERSION_PATCH)
#ifndef WORKSUITE_EVB_PREVIOUS_VERSIONS
#define WORKSUITE_EVB_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_EVB_VERSION_MAJOR,WORKSUITE_EVB_VERSION_MINOR,WORKSUITE_EVB_VERSION_PATCH)
#else
#define WORKSUITE_EVB_FULL_VERSION_LIST  WORKSUITE_EVB_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_EVB_VERSION_MAJOR,WORKSUITE_EVB_VERSION_MINOR,WORKSUITE_EVB_VERSION_PATCH)
#endif

namespace evb
{
  const std::string project = "worksuite";
  const std::string package = "evb";
  const std::string versions = WORKSUITE_EVB_FULL_VERSION_LIST;
  const std::string version = PACKAGE_VERSION_STRING(WORKSUITE_EVB_VERSION_MAJOR,WORKSUITE_EVB_VERSION_MINOR,WORKSUITE_EVB_VERSION_PATCH);
  const std::string description = "The CMS event builder";
  const std::string summary = "Event builder library";
  const std::string authors = "Remi Mommsen, Andrea Petrucci";
  const std::string link = "https://svnweb.cern.ch/trac/cmsos";

  config::PackageInfo getPackageInfo();

  void checkPackageDependencies();

  std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif
