#include <memory>
#include <sched.h>
#include <utility>

#include "evb/EVM.h"
#include "evb/evm/Configuration.h"
#include "evb/readoutunit/BUproxy.h"
#include "evb/readoutunit/FedFragment.h"
#include "evb/readoutunit/FerolConnectionManager.h"
#include "evb/readoutunit/MetaDataRetrieverDIPBridge.h"

#include "evb/readoutunit/Input.h"
#include "evb/readoutunit/States.h"
#include "interface/shared/GlobalEventNumber.h"
#include "xgi/Method.h"


evb::EVM::EVM(xdaq::ApplicationStub* app) :
  readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>(app,"/evb/images/evm64x64.gif")
{
  this->stateMachine_ = std::make_shared<readoutunit::StateMachine<EVM>>(this);
  this->input_ = std::make_shared<readoutunit::Input<EVM,evm::Configuration>>(this);
  this->ferolConnectionManager_ = std::make_shared<readoutunit::FerolConnectionManager<EVM,evm::Configuration>>(this);
  this->ruProxy_ = std::make_shared<evm::RUproxy>(this,this->stateMachine_);
  this->buProxy_ = std::make_shared<readoutunit::BUproxy<EVM>>(this);

  this->initialize();

  LOG4CPLUS_INFO(this->getApplicationLogger(), "End of constructor");
}

void evb::EVM::timeExpired (toolbox::task::TimerEvent& e) {

	xdata::String maskedDipTopics =  this->configuration_->maskedDipTopics;
	evb::readoutunit::MetaDataRetrieverDIPBridgePtr dipRetriver = this->input_.get()->getMetaDataRetrieverDIPBridgePtr();
	dipRetriver->registerToDipBridge(maskedDipTopics);

}

void evb::EVM::do_appendMonitoringInfoSpaceItems(InfoSpaceItems& items)
{
  readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>::do_appendMonitoringInfoSpaceItems(items);
  ruProxy_->appendMonitoringItems(items);
}


void evb::EVM::do_updateMonitoringInfo()
{
  readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>::do_updateMonitoringInfo();
  ruProxy_->updateMonitoringItems();
}


void evb::EVM::do_handleItemChangedEvent(const std::string& item)
{
  if (item == "maxTriggerRate")
  {
    const uint32_t triggerRate = this->configuration_->maxTriggerRate;
    std::ostringstream msg;
    msg << "Setting maxTriggerRate to " << triggerRate << " Hz";
    LOG4CPLUS_INFO(this->getApplicationLogger(),msg.str());
    buProxy_->setMaxTriggerRate(triggerRate);
  }
  else
  {
    readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>::do_handleItemChangedEvent(item);
  }
}


namespace evb {
  namespace readoutunit {

    template<>
    uint32_t evb::readoutunit::Input<EVM,evm::Configuration>::getLumiSectionFromTCDS(const DataLocations& dataLocations) const
    {
      using namespace evtn;

      uint32_t offset = sizeof(fedh_t) + 7 * SLINK_WORD_SIZE + SLINK_HALFWORD_SIZE;
      DataLocations::const_iterator it = dataLocations.begin();
      const DataLocations::const_iterator itEnd = dataLocations.end();

      while ( it != itEnd && offset > it->iov_len )
      {
        offset -= it->iov_len;
        ++it;
      }

      if ( it == itEnd )
      {
        std::ostringstream msg;
        msg << "Premature end of TCDS data block from FED " << TCDS_FED_ID;
        XCEPT_RAISE(exception::TCDS, msg.str());
      }

      return *(uint32_t*)((unsigned char*)it->iov_base + offset) & 0xffffffff;
    }


    template<>
    uint32_t evb::readoutunit::Input<EVM,evm::Configuration>::getLumiSectionFromGTPe(const DataLocations& dataLocations) const
    {
      using namespace evtn;

      uint32_t offset = GTPE_ORBTNR_OFFSET * SLINK_HALFWORD_SIZE; // includes FED header

      DataLocations::const_iterator it = dataLocations.begin();
      const DataLocations::const_iterator itEnd = dataLocations.end();

      while ( it != itEnd && offset > it->iov_len )
      {
        offset -= it->iov_len;
        ++it;
      }

      if ( it == itEnd )
      {
        std::ostringstream msg;
        msg << "Premature end of GTPe data block from FED " << GTPe_FED_ID;
        XCEPT_RAISE(exception::TCDS, msg.str());
      }

      const uint32_t orbitNumber = *(uint32_t*)((unsigned char*)it->iov_base + offset);

      return (orbitNumber / ORBITS_PER_LS) + 1;
    }


    template<>
    void evb::readoutunit::Input<EVM,evm::Configuration>::setMasterStream()
    {
      auto const& configuration = readoutUnit_->getConfiguration();

      masterStream_ = ferolStreams_.end();

      if ( ferolStreams_.empty() || configuration->dropInputData ) return;

      EvBidFactory::LumiSectionFunction lumiSectionFunction = 0;

      if ( configuration->getLumiSectionFromTrigger )
      {
        masterStream_ = ferolStreams_.find(TCDS_FED_ID);
        if ( masterStream_ != ferolStreams_.end() )
        {
          lumiSectionFunction = boost::bind(&evb::readoutunit::Input<EVM,evm::Configuration>::getLumiSectionFromTCDS, this, _1);
          LOG4CPLUS_INFO(readoutUnit_->getApplicationLogger(), "Using TCDS as lumi section source");
        }
        else
        {
          masterStream_ = ferolStreams_.find(GTPe_FED_ID);
          if ( masterStream_ != ferolStreams_.end() )
          {
            lumiSectionFunction = boost::bind(&evb::readoutunit::Input<EVM,evm::Configuration>::getLumiSectionFromGTPe, this, _1);
            LOG4CPLUS_INFO(readoutUnit_->getApplicationLogger(), "Using GTPe as lumi section source");
          }
        }
      }

      // If no magic FED id has been found, pick the first available FED in fedSourceIds
      xdata::Vector<xdata::UnsignedInteger32>::const_iterator it = configuration->fedSourceIds.begin();
      while ( masterStream_ == ferolStreams_.end() &&
              it != configuration->fedSourceIds.end() )
      {
        masterStream_ = ferolStreams_.find(*it);
        ++it;
      }

      // At this point the master stream must be defined
      assert( masterStream_ != ferolStreams_.end() );

      masterStream_->second->useAsMaster();

      const EvBidFactoryPtr& evbIdFactory = masterStream_->second->getEvBidFactory();
      if ( lumiSectionFunction )
      {
        evbIdFactory->setLumiSectionFunction(lumiSectionFunction);
      }
      else
      {
        const uint32_t lsDuration = configuration->fakeLumiSectionDuration;
        evbIdFactory->setFakeLumiSectionDuration(lsDuration);

        std::ostringstream msg;
        msg << "Emulating a lumi section duration of " << lsDuration << "s";
        LOG4CPLUS_INFO(readoutUnit_->getApplicationLogger(),msg.str());
      }
    }


    template<>
    void BUproxy<EVM>::handleRequest(const msg::EventRequest* eventRequest, FragmentRequestPtr& fragmentRequest)
    {
      fragmentRequest->nbDiscards = eventRequest->nbRequests; //Always keep nb discards == nb requests for RUs
      fragmentRequest->ruTids = readoutUnit_->getRUtids();

      boost::upgrade_lock<boost::shared_mutex> sl(fragmentRequestFIFOsMutex_);

      auto pos = fragmentRequestFIFOs_.find(eventRequest->buTid);
      if ( pos == fragmentRequestFIFOs_.end() )
      {
        // new TID
        boost::upgrade_to_unique_lock< boost::shared_mutex > ul(sl);
        PrioritizedFragmentRequestFIFOs prioritizedFragmentRequestFIFOs;
        prioritizedFragmentRequestFIFOs.reserve(evb::LOWEST_PRIORITY+1);
        for (uint16_t priority = 0; priority <= evb::LOWEST_PRIORITY; ++priority)
        {
          std::ostringstream name;
          name << "fragmentRequestFIFO_BU" << eventRequest->buTid << "_priority" << priority;
          FragmentRequestFIFOPtr requestFIFO = std::make_unique<FragmentRequestFIFO>(readoutUnit_,name.str());
          requestFIFO->resize(readoutUnit_->getConfiguration()->fragmentRequestFIFOCapacity);
          prioritizedFragmentRequestFIFOs.push_back(std::move(requestFIFO));
        }
        pos = fragmentRequestFIFOs_.emplace_hint(pos,
                                                 eventRequest->buTid,
                                                 std::move(prioritizedFragmentRequestFIFOs));
        nextBU_ = pos; //make sure the nextBU points to a valid location
      }

      if ( eventRequest->priority > evb::LOWEST_PRIORITY || eventRequest->priority < 0 )
      {
        std::ostringstream msg;
        msg << "Received an event request with an invalid priority of " << eventRequest->priority;
        msg << " from BU TID " << eventRequest->buTid;
        XCEPT_RAISE(exception::I2O, msg.str());
      }

      pos->second[eventRequest->priority]->enqWait(std::move(fragmentRequest));
    }


    template<>
    void BUproxy<EVM>::doLumiSectionTransition()
    {
      // Assure that each BU gets a request served when the lumi section changes.
      // Start with the lowest priority queue to eventually
      // unblock any old requests sent with lower priortity.
      // In case that there was no event request during the last lumisection,
      // return all requests to the BU without assigning any events.

      std::lock_guard<std::mutex> guard(requestMonitoringMutex_);
      boost::unique_lock<boost::shared_mutex> ul(fragmentRequestFIFOsMutex_);

      for (auto& requestFIFO : fragmentRequestFIFOs_)
      {
        FragmentRequestPtr fragmentRequest;

        if ( requestMonitoring_.buTimestamps[requestFIFO.first] < lastLumiTransition_ ) // BU is stale
        {
          SuperFragments superFragments;
          for ( uint16_t p = 0; p <= evb::LOWEST_PRIORITY; ++p )
          {
            while ( requestFIFO.second[p]->deq(fragmentRequest) )
            {
              sendData(fragmentRequest,superFragments);
            }
          }
        }
        else
        {
          for ( uint16_t p = evb::LOWEST_PRIORITY; p > 0; --p )
          {
            if ( requestFIFO.second[p]->deq(fragmentRequest) )
            {
              requestFIFO.second[0]->enqWait(std::move(fragmentRequest));
              break;
            }
          }
        }
      }

      lastLumiTransition_ = getTimeStamp();
    }


    template<>
    void BUproxy<EVM>::waitForNextTrigger(const uint16_t nbRequests)
    {
      if ( availableTriggers_ >= nbRequests )
      {
        availableTriggers_ -= nbRequests;
        return;
      }

      uint64_t now = 0;
      while ( availableTriggers_ < nbRequests )
      {
        now = getTimeStamp();
        if ( lastTime_ == 0 )
          availableTriggers_ = nbRequests;
        else
          availableTriggers_ = static_cast<uint32_t>(now>lastTime_ ? (now-lastTime_)/1e9*maxTriggerRate_ : 0);
      }
      lastTime_ = now;
      availableTriggers_ -= nbRequests;
    }


    template<>
    bool BUproxy<EVM>::processRequest(FragmentRequestPtr& fragmentRequest, SuperFragments& superFragments)
    {
      std::lock_guard<std::mutex> guard(processingRequestMutex_);

      SuperFragmentPtr superFragment;

      try
      {
        {
          boost::shared_lock<boost::shared_mutex> frm(fragmentRequestFIFOsMutex_);

          if ( ! doProcessing_ ) return false;

          if ( fragmentRequestFIFOs_.empty() ) return false;

          // search the next request FIFO with highest priority (lowest number) which is non-empty
          const FragmentRequestFIFOs::iterator lastBU = nextBU_;
          uint16_t priority = 0;
          while ( priority <= evb::LOWEST_PRIORITY &&
                  nextBU_->second[priority]->empty() )
          {
            if ( ++nextBU_ == fragmentRequestFIFOs_.end() )
              nextBU_ = fragmentRequestFIFOs_.begin();

            if ( lastBU == nextBU_ ) ++priority;
          }

          if ( priority > evb::LOWEST_PRIORITY ) return false;

          if ( !readoutUnit_->getInput()->getNextAvailableSuperFragment(superFragment) ) return false;

          // this must succeed as we checked that the queue is non-empty
          assert( nextBU_->second[priority]->deq(fragmentRequest) );
          if ( ++nextBU_ == fragmentRequestFIFOs_.end() )
            nextBU_ = fragmentRequestFIFOs_.begin();
        }

        if ( maxTriggerRate_ > 0 ) waitForNextTrigger(fragmentRequest->nbRequests);

        fragmentRequest->evbIds.clear();
        fragmentRequest->evbIds.reserve(fragmentRequest->nbRequests);

        fragmentRequest->evbIds.push_back( superFragment->getEvBid() );
        superFragments.push_back(std::move(superFragment));

        uint32_t remainingRequests = fragmentRequest->nbRequests - 1;
        const uint64_t maxTries = readoutUnit_->getConfiguration()->maxTriggerAgeMSec*10;
        uint64_t tries = 0;
        while ( remainingRequests > 0 && tries < maxTries )
        {
          if ( readoutUnit_->getInput()->getNextAvailableSuperFragment(superFragment) )
          {
            fragmentRequest->evbIds.push_back( superFragment->getEvBid() );
            superFragments.push_back(std::move(superFragment));
            --remainingRequests;
          }
          else
          {
            ::usleep(100);
            ++tries;
          }
        }
        --requestMonitoring_.activeRequests;
      }
      catch(exception::HaltRequested&)
      {
        return false;
      }

      fragmentRequest->nbRequests = fragmentRequest->evbIds.size();

      readoutUnit_->getRUproxy()->sendRequest(fragmentRequest);

      return true;
    }


    template<>
    bool BUproxy<EVM>::isEmpty()
    {
      {
        std::lock_guard<std::mutex> guard(dataMonitoringMutex_);
        if ( dataMonitoring_.outstandingEvents != 0 ) return false;
      }
      {
        std::lock_guard<std::mutex> guard(processesActiveMutex_);
        if ( processesActive_.any() ) return false;
      }
      return true;
    }


    template<>
    std::string BUproxy<EVM>::getHelpTextForBuRequests() const
    {
      return "Event requests from the BUs. If no events are requested, the BU appliances are busy, have no FUs to process data or have failed.";
    }


    template<>
    void Configuring<EVM>::doConfigure(const EVM* evm)
    {
      evm->getRUproxy()->configure();
      evm->getBUproxy()->setMaxTriggerRate(evm->getConfiguration()->maxTriggerRate);
    }


    template<>
    void Running<EVM>::doStartProcessing(const EVM* evm, const uint32_t runNumber)
    {
      evm->getRUproxy()->startProcessing();
    }


    template<>
    void Running<EVM>::doStopProcessing(const EVM* evm)
    {
      evm->getRUproxy()->stopProcessing();
    }


    template<>
    void Draining<EVM>::doDraining(const EVM* evm)
    {
      evm->getRUproxy()->drain();
    }


    template<>
    void readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>::addComponentsToWebPage
    (
      cgicc::table& table
    ) const
    {
      using namespace cgicc;

      table.add(tr()
                .add(td(this->input_->getHtmlSnipped()).set("class","xdaq-evb-component").set("rowspan","2"))
                .add(td(img().set("src","/evb/images/arrow_e.gif").set("alt","")))
                .add(td(dynamic_cast<const EVM*>(this)->getRUproxy()->getHtmlSnipped()).set("class","xdaq-evb-component")));

      table.add(tr()
                .add(td(img().set("src","/evb/images/arrow_e.gif").set("alt","")))
                .add(td(buProxy_->getHtmlSnipped()).set("class","xdaq-evb-component")));
    }
  }
}


/**
 * Provides the factory method for the instantiation of EVM applications.
 */
XDAQ_INSTANTIATOR_IMPL(evb::EVM)



/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
