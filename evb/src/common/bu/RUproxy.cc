#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"
#include "interface/shared/i2ogevb2g.h"
#include "evb/BU.h"
#include "evb/bu/EventBuilder.h"
#include "evb/bu/ResourceManager.h"
#include "evb/bu/RUproxy.h"
#include "evb/Constants.h"
#include "evb/EvBid.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xcept/tools.h"
#include "xdaq/ApplicationDescriptor.h"

#include <limits>
#include <memory>
#include <string>
#include <utility>


evb::bu::RUproxy::RUproxy
(
  BU* bu,
  std::shared_ptr<EventBuilder> eventBuilder,
  std::shared_ptr<ResourceManager> resourceManager
) :
  bu_(bu),
  eventBuilder_(eventBuilder),
  resourceManager_(resourceManager),
  msgPool_(bu->getMsgPool()),
  configuration_(bu->getConfiguration()),
  eventFragmentFIFO_(bu,"eventFragmentFIFO"),
  doProcessing_(false),
  eventFragmentActive_(false),
  requestFragmentsActive_(false),
  tid_(0),
  roundTripTimeSampling_(0)
{
  resetMonitoringCounters();
  startEventFragmentWorkLoop();
  startRequestFragmentsWorkLoop();
}


evb::bu::RUproxy::~RUproxy()
{
  if ( requestFragmentsWL_ && requestFragmentsWL_->isActive() )
    requestFragmentsWL_->cancel();
  if ( eventFragmentWL_ && eventFragmentWL_->isActive() )
    eventFragmentWL_->cancel();
}


void evb::bu::RUproxy::superFragmentCallback(toolbox::mem::Reference* bufRef)
{
  std::lock_guard<std::mutex> guard(eventFragmentFIFOmutex_);

  eventFragmentFIFO_.enqWait(std::move(bufRef));
}


void evb::bu::RUproxy::startEventFragmentWorkLoop()
{
  try
  {
    eventFragmentWL_ = toolbox::task::getWorkLoopFactory()->
      getWorkLoop( bu_->getIdentifier("eventFragment"), "waiting" );

    eventFragmentAction_ =
      toolbox::task::bind(this, &evb::bu::RUproxy::eventFragment,
                          bu_->getIdentifier("ruProxyEventFragment") );

    if ( ! eventFragmentWL_->isActive() )
      eventFragmentWL_->activate();
  }
  catch(xcept::Exception& e)
  {
    std::string msg = "Failed to start workloop 'eventFragment'";
    XCEPT_RETHROW(exception::WorkLoop, msg, e);
  }
}


void evb::bu::RUproxy::startRequestFragmentsWorkLoop()
{
  try
  {
    requestFragmentsWL_ = toolbox::task::getWorkLoopFactory()->
      getWorkLoop( bu_->getIdentifier("requestFragments"), "waiting" );

    requestFragmentsAction_ =
      toolbox::task::bind(this, &evb::bu::RUproxy::requestFragments,
                          bu_->getIdentifier("ruProxyRequestFragments") );

    if ( ! requestFragmentsWL_->isActive() )
      requestFragmentsWL_->activate();
  }
  catch(xcept::Exception& e)
  {
    std::string msg = "Failed to start workloop 'requestFragments'";
    XCEPT_RETHROW(exception::WorkLoop, msg, e);
  }
}


void evb::bu::RUproxy::startProcessing()
{
  resetMonitoringCounters();

  doProcessing_ = true;
  requestFragmentsWL_->submit(requestFragmentsAction_);
  eventFragmentWL_->submit(eventFragmentAction_);
}


void evb::bu::RUproxy::drain()
{
  while ( !eventFragmentFIFO_.empty() || !dataBlockMap_.empty() ||
          requestFragmentsActive_ || eventFragmentActive_ ) ::usleep(1000);
}


void evb::bu::RUproxy::stopProcessing()
{
  doProcessing_ = false;
  while ( requestFragmentsActive_ || eventFragmentActive_ ) ::usleep(1000);
  eventFragmentFIFO_.clear();
}


bool evb::bu::RUproxy::eventFragment(toolbox::task::WorkLoop*)
{
  if ( ! doProcessing_ ) return false;

  eventFragmentActive_ = true;

  toolbox::mem::Reference* bufRef = 0;

  try
  {
    while ( eventFragmentFIFO_.deq(bufRef) )
    {
      handleEventFragment(bufRef);
    }
  }
  catch(xcept::Exception& e)
  {
    if ( bufRef ) bufRef->release();
    eventFragmentActive_ = false;
    bu_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    if ( bufRef ) bufRef->release();
    eventFragmentActive_ = false;
    XCEPT_DECLARE(exception::SuperFragment,
                  sentinelException, e.what());
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    if ( bufRef ) bufRef->release();
    eventFragmentActive_ = false;
    XCEPT_DECLARE(exception::SuperFragment,
                  sentinelException, "unkown exception");
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }

  eventFragmentActive_ = false;

  ::usleep(1000);

  return doProcessing_;
}


void evb::bu::RUproxy::handleEventFragment(toolbox::mem::Reference* bufRef)
{
  do
  {
    toolbox::mem::Reference* nextRef = bufRef->getNextReference();
    bufRef->setNextReference(0);

    const I2O_MESSAGE_FRAME* stdMsg =
      (I2O_MESSAGE_FRAME*)bufRef->getDataLocation();
    const msg::DataBlockMsg* dataBlockMsg =
      (msg::DataBlockMsg*)stdMsg;
    const uint32_t payload = stdMsg->MessageSize << 2;
    const I2O_TID ruTid = stdMsg->InitiatorAddress;
    const bool isEVM = (ruTid == evm_.tid);

    {
      std::lock_guard<std::mutex> guard(fragmentMonitoringMutex_);

      ++fragmentMonitoring_.perf.i2oCount;
      fragmentMonitoring_.perf.sumOfSizes += payload;
      fragmentMonitoring_.perf.sumOfSquares += payload*payload;

      auto statsPerRU = fragmentMonitoring_.countsPerRU.find(ruTid);
      if ( statsPerRU == fragmentMonitoring_.countsPerRU.end() )
      {
        statsPerRU = fragmentMonitoring_.countsPerRU.emplace_hint(statsPerRU,ruTid,StatsPerRU());
      }
      statsPerRU->second.payload += payload;

      if ( dataBlockMsg->blockNb == 1 ) //only the first block contains the EvBid
      {
        const uint64_t now = getTimeStamp();
        const uint64_t deltaT = now>dataBlockMsg->timeStampNS ? now-dataBlockMsg->timeStampNS : 0;
        statsPerRU->second.roundTripTime = (roundTripTimeSampling_*deltaT) + (1-roundTripTimeSampling_)*statsPerRU->second.roundTripTime;

        const uint32_t nbSuperFragments = dataBlockMsg->nbSuperFragments;
        if ( nbSuperFragments > 0 )
        {
          const EvBid* lastEvBid = (EvBid*)(&dataBlockMsg->evbIds + (nbSuperFragments-1)*sizeof(EvBid));
          const uint32_t lastEventNumber = lastEvBid->eventNumber();

          if (isEVM)
          {
            fragmentMonitoring_.lastEventNumberFromEVM = lastEventNumber;
          }
          else
          {
            fragmentMonitoring_.lastEventNumberFromRUs = lastEventNumber;
          }

          fragmentMonitoring_.perf.logicalCount += nbSuperFragments;
          statsPerRU->second.logicalCount += nbSuperFragments;
        }
      }
    }

    {
      std::lock_guard<std::mutex> guard(dataBlockMapMutex_);

      TIDandResourceId index = std::make_pair(ruTid,dataBlockMsg->buResourceId);
      auto result = dataBlockMap_.emplace(index,std::make_unique<FragmentChain>(dataBlockMsg->nbBlocks));
      if ( result.second // new data block
           && dataBlockMsg->blockNb != 1 )
      {
        std::ostringstream msg;
        msg << "Received a first super-fragment block from RU tid " << ruTid;
        msg << " for BU resource id " << dataBlockMsg->buResourceId;
        msg << " which is already block number " <<  dataBlockMsg->blockNb;
        msg << " of " << dataBlockMsg->nbBlocks;
        XCEPT_RAISE(exception::SuperFragment, msg.str());
      }

      const uint16_t builderId = resourceManager_->underConstruction(dataBlockMsg,isEVM);
      const bool superFragmentComplete = result.first->second->append(bufRef);
      bufRef = nextRef;

      if ( superFragmentComplete )
      {
        eventBuilder_->addSuperFragment(builderId,std::move(result.first->second));
        dataBlockMap_.erase(result.first);
      }
    }
  } while ( bufRef );
}


bool evb::bu::RUproxy::requestFragments(toolbox::task::WorkLoop*)
{
  if ( ! doProcessing_ ) return false;

  requestFragmentsActive_ = true;

  try
  {
    sendRequests();
  }
  catch(xcept::Exception& e)
  {
    requestFragmentsActive_ = false;
    bu_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    requestFragmentsActive_ = false;
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, e.what());
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    requestFragmentsActive_ = false;
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, "unkown exception");
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }

  requestFragmentsActive_ = false;

  ::usleep(1000000/configuration_->maxRequestRate);

  return doProcessing_;
}


void evb::bu::RUproxy::sendRequests()
{
  ResourceManager::BUresources resources;
  resourceManager_->getAllAvailableResources(resources);
  const uint32_t nbRequests = resources.size();

  if ( nbRequests > 0 )
  {
    const uint32_t msgSize = sizeof(msg::ReadoutMsg) + nbRequests*sizeof(msg::EventRequest);
    uint32_t nbEventsRequested = 0;

    toolbox::mem::Reference* rqstBufRef = 0;
    do
    {
      try
      {
        rqstBufRef = toolbox::mem::getMemoryPoolFactory()->
          getFrame(msgPool_, msgSize);
        rqstBufRef->setDataSize(msgSize);
      }
      catch(toolbox::mem::exception::Exception&)
      {
        rqstBufRef = 0;
        ::usleep(100);
      }
    } while ( !rqstBufRef );

    I2O_MESSAGE_FRAME* stdMsg =
      (I2O_MESSAGE_FRAME*)rqstBufRef->getDataLocation();
    I2O_PRIVATE_MESSAGE_FRAME* pvtMsg = (I2O_PRIVATE_MESSAGE_FRAME*)stdMsg;
    msg::ReadoutMsg* readoutMsg = (msg::ReadoutMsg*)stdMsg;
    stdMsg->VersionOffset    = 0;
    stdMsg->MsgFlags         = 0;
    stdMsg->MessageSize      = msgSize >> 2;
    stdMsg->InitiatorAddress = tid_;
    stdMsg->TargetAddress    = evm_.tid;
    stdMsg->Function         = I2O_PRIVATE_MESSAGE;
    pvtMsg->OrganizationID   = XDAQ_ORGANIZATION_ID;
    pvtMsg->XFunctionCode    = I2O_SHIP_FRAGMENTS;
    readoutMsg->nbRequests   = nbRequests;

    const uint64_t now = getTimeStamp();
    unsigned char* payload = (unsigned char*)&readoutMsg->requests[0];
    for (auto const& resource : resources)
    {
      msg::EventRequest* eventRequest = (msg::EventRequest*)payload;
      eventRequest->msgSize      = sizeof(msg::EventRequest);
      eventRequest->buTid        = tid_;
      eventRequest->priority     = resource.priority;
      eventRequest->timeStampNS  = now;
      eventRequest->buResourceId = resource.id;
      eventRequest->nbRequests   = resource.id>0 ? configuration_->eventsPerRequest.value_ : 0;;
      eventRequest->nbDiscards   = resource.eventsToDiscard;
      eventRequest->nbRUtids     = 0; // will be filled by EVM

      nbEventsRequested += eventRequest->nbRequests;
      payload += sizeof(msg::EventRequest);
    }

    // Send the request to the EVM
    uint32_t retries = 0;
    try
    {
      retries = bu_->postMessage(rqstBufRef,evm_.descriptor);
    }
    catch(exception::I2O& e)
    {
      {
        std::lock_guard<std::mutex> guard(requestMonitoringMutex_);
        requestMonitoring_.perf.retryCount += retries;
      }

      std::ostringstream msg;
      msg << "Failed to send message to EVM TID ";
      msg << evm_.tid;
      XCEPT_RETHROW(exception::I2O, msg.str(), e);
    }

    {
      std::lock_guard<std::mutex> guard(requestMonitoringMutex_);

      requestMonitoring_.perf.sumOfSizes += msgSize;
      requestMonitoring_.perf.sumOfSquares += msgSize*msgSize;
      requestMonitoring_.perf.logicalCount += nbEventsRequested;
      ++requestMonitoring_.perf.i2oCount;
      requestMonitoring_.perf.retryCount += retries;
    }
  }
}


uint64_t evb::bu::RUproxy::getTimeStamp() const
{
  if ( configuration_->roundTripTimeSamples == 0U )
    return 0;
  else
    return evb::getTimeStamp();
}


void evb::bu::RUproxy::appendMonitoringItems(InfoSpaceItems& items)
{
  requestRate_ = 0;
  requestRetryRate_ = 0;
  requestRetryCount_ = 0;
  fragmentRate_ = 0;
  fragmentCountPerRU_.clear();
  payloadPerRU_.clear();
  slowestRUtid_ = 0;

  items.add("requestRate", &requestRate_);
  items.add("requestRetryRate", &requestRetryRate_);
  items.add("requestRetryCount", &requestRetryCount_);
  items.add("fragmentRate", &fragmentRate_);
  items.add("fragmentCountPerRU", &fragmentCountPerRU_);
  items.add("payloadPerRU", &payloadPerRU_);
  items.add("slowestRUtid", &slowestRUtid_);
}


void evb::bu::RUproxy::updateMonitoringItems()
{
  {
    std::lock_guard<std::mutex> guard(requestMonitoringMutex_);

    const double deltaT = requestMonitoring_.perf.deltaT();
    requestMonitoring_.throughput = requestMonitoring_.perf.throughput(deltaT);
    requestMonitoring_.requestRate = requestMonitoring_.perf.logicalRate(deltaT);
    requestMonitoring_.i2oRate = requestMonitoring_.perf.i2oRate(deltaT);
    requestMonitoring_.retryRate = requestMonitoring_.perf.retryRate(deltaT);
    requestMonitoring_.retryCount = requestMonitoring_.perf.retryCount;
    requestMonitoring_.packingFactor = requestMonitoring_.perf.packingFactor();
    requestMonitoring_.perf.reset();
    requestRate_ = requestMonitoring_.i2oRate;
    requestRetryRate_ = requestMonitoring_.retryRate;
    requestRetryCount_ = requestMonitoring_.retryCount;
  }
  {
    std::lock_guard<std::mutex> guard(fragmentMonitoringMutex_);

    const double deltaT = fragmentMonitoring_.perf.deltaT();
    fragmentMonitoring_.incompleteSuperFragments = dataBlockMap_.size();
    fragmentMonitoring_.throughput = fragmentMonitoring_.perf.throughput(deltaT);
    fragmentMonitoring_.fragmentRate = fragmentMonitoring_.perf.logicalRate(deltaT);
    fragmentMonitoring_.i2oRate = fragmentMonitoring_.perf.i2oRate(deltaT);
    fragmentMonitoring_.packingFactor = fragmentMonitoring_.perf.packingFactor();
    fragmentRate_ = fragmentMonitoring_.i2oRate;

    fragmentCountPerRU_.clear();
    fragmentCountPerRU_.reserve(fragmentMonitoring_.countsPerRU.size());
    payloadPerRU_.clear();
    payloadPerRU_.reserve(fragmentMonitoring_.countsPerRU.size());
    slowestRUtid_ = 0;

    uint32_t minRoundTripTime = std::numeric_limits<uint32_t>::max();
    uint32_t maxRoundTripTime = 0;
    for (auto const& countsPerRU : fragmentMonitoring_.countsPerRU)
    {
      if ( minRoundTripTime > countsPerRU.second.roundTripTime )
      {
        minRoundTripTime = countsPerRU.second.roundTripTime;
      }
      if ( maxRoundTripTime < countsPerRU.second.roundTripTime )
      {
        maxRoundTripTime = countsPerRU.second.roundTripTime;
        slowestRUtid_ = countsPerRU.first;
      }
    }

    for (auto& countsPerRU : fragmentMonitoring_.countsPerRU)
    {
      fragmentCountPerRU_.push_back(countsPerRU.second.logicalCount);
      payloadPerRU_.push_back(countsPerRU.second.payload);
      countsPerRU.second.deltaTns = countsPerRU.second.roundTripTime - minRoundTripTime;
    }
    fragmentMonitoring_.perf.reset();
  }
}


void evb::bu::RUproxy::resetMonitoringCounters()
{
  {
    std::lock_guard<std::mutex> guard(requestMonitoringMutex_);
    requestMonitoring_.requestRate = 0;
    requestMonitoring_.requestRetryRate = 0;
    requestMonitoring_.retryCount = 0;
    requestMonitoring_.perf.reset();
  }
  {
    std::lock_guard<std::mutex> guard(fragmentMonitoringMutex_);
    fragmentMonitoring_.lastEventNumberFromEVM = 0;
    fragmentMonitoring_.lastEventNumberFromRUs = 0;
    fragmentMonitoring_.incompleteSuperFragments = 0;
    fragmentMonitoring_.fragmentRate = 0;
    fragmentMonitoring_.perf.reset();
    fragmentMonitoring_.countsPerRU.clear();
  }
}


void evb::bu::RUproxy::configure()
{
  {
    std::lock_guard<std::mutex> guard(dataBlockMapMutex_);
    dataBlockMap_.clear();
  }

  eventFragmentFIFO_.clear();
  eventFragmentFIFO_.resize(configuration_->eventFragmentFIFOCapacity);

  roundTripTimeSampling_ = configuration_->roundTripTimeSamples>0U ? 1./configuration_->roundTripTimeSamples : 0;

  getApplicationDescriptors();
}


void evb::bu::RUproxy::getApplicationDescriptors()
{
  try
  {
    tid_ = i2o::utils::getAddressMap()->
      getTid(bu_->getApplicationDescriptor());
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::I2O,
                  "Failed to get I2O TID for this application", e);
  }

  getApplicationDescriptorForEVM();
}


void evb::bu::RUproxy::getApplicationDescriptorForEVM()
{
  if (configuration_->evmInstance.value_ < 0)
  {
    // Try to find instance number by assuming the first EVM found is the
    // one to be used.

    std::set<const xdaq::ApplicationDescriptor*> evmDescriptors;

    try
    {
      evmDescriptors =
        bu_->getApplicationContext()->
        getDefaultZone()->
        getApplicationDescriptors("evb::EVM");
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(exception::Configuration,
                    "Failed to get EVM application descriptor", e);
    }

    if ( evmDescriptors.empty() )
    {
      XCEPT_RAISE(exception::Configuration,
                  "Failed to get EVM application descriptor");
    }

    evm_.descriptor = *(evmDescriptors.begin());
  }
  else
  {
    try
    {
      evm_.descriptor =
        bu_->getApplicationContext()->
        getDefaultZone()->
        getApplicationDescriptor("evb::EVM",
                                 configuration_->evmInstance.value_);
    }
    catch(xcept::Exception& e)
    {
      std::ostringstream msg;
      msg << "Failed to get application descriptor of EVM";
      msg << configuration_->evmInstance.toString();
      XCEPT_RETHROW(exception::Configuration, msg.str(), e);
    }
  }

  try
  {
    evm_.tid = i2o::utils::getAddressMap()->getTid(evm_.descriptor);
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::I2O,
                  "Failed to get the I2O TID of the EVM", e);
  }
}


void evb::bu::RUproxy::lumisectionInfoCallback(toolbox::mem::Reference* bufRef)
{
  try
  {
    const I2O_MESSAGE_FRAME* stdMsg =
      (I2O_MESSAGE_FRAME*)bufRef->getDataLocation();
    const msg::LumiSectionInfoMsg* lumiSectionInfoMsg =
      (msg::LumiSectionInfoMsg*)stdMsg;

    {
      std::lock_guard<std::mutex> guard(lumiSectionInfoMutex_);

      auto pos = lumiSectionInfo_.find(lumiSectionInfoMsg->lumiSection);
      pos->second.set_value(lumiSectionInfoMsg->numberOfEventsBuilt);
      lumiSectionInfo_.erase(pos);
    }
    bufRef->release();
  }
  catch(xcept::Exception& e)
  {
    if (bufRef) bufRef->release();
    bu_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    if (bufRef) bufRef->release();
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, e.what());
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    if (bufRef) bufRef->release();
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, "unkown exception");
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
}


uint32_t evb::bu::RUproxy::getTotalEventsInLumiSection(const uint32_t lumiSection)
{
  toolbox::mem::Reference* rqstLumiInfoBufRef = 0;
  std::future<uint32_t> totalEventsInLumisection;
  const uint32_t msgSize = sizeof(msg::RequestLumiSectionInfoMsg);

  {
    std::lock_guard<std::mutex> guard(lumiSectionInfoMutex_);

    LumiSectionInfo::iterator pos = lumiSectionInfo_.emplace(std::make_pair(lumiSection,std::promise<uint32_t>()));
    totalEventsInLumisection = pos->second.get_future();
  }

  do
  {
    try
    {
      rqstLumiInfoBufRef = toolbox::mem::getMemoryPoolFactory()->
        getFrame(msgPool_, msgSize);
      rqstLumiInfoBufRef->setDataSize(msgSize);
    }
    catch(toolbox::mem::exception::Exception&)
    {
      rqstLumiInfoBufRef = 0;
      ::usleep(100);
    }
  } while ( !rqstLumiInfoBufRef );

  I2O_MESSAGE_FRAME* stdMsg =
    (I2O_MESSAGE_FRAME*)rqstLumiInfoBufRef->getDataLocation();
  I2O_PRIVATE_MESSAGE_FRAME* pvtMsg = (I2O_PRIVATE_MESSAGE_FRAME*)stdMsg;
  msg::RequestLumiSectionInfoMsg* rqstLumiInfo = (msg::RequestLumiSectionInfoMsg*)stdMsg;
  stdMsg->VersionOffset     = 0;
  stdMsg->MsgFlags          = 0;
  stdMsg->MessageSize       = msgSize >> 2;
  stdMsg->InitiatorAddress  = tid_;
  stdMsg->TargetAddress     = evm_.tid;
  stdMsg->Function          = I2O_PRIVATE_MESSAGE;
  pvtMsg->OrganizationID    = XDAQ_ORGANIZATION_ID;
  pvtMsg->XFunctionCode     = I2O_REQUEST_LUMISECTION_INFO;
  rqstLumiInfo->lumiSection = lumiSection;

  try
  {
    bu_->postMessage(rqstLumiInfoBufRef,evm_.descriptor);
  }
  catch(exception::I2O& e)
  {
    std::ostringstream msg;
    msg << "Failed to send lumisection info request to EVM TID ";
    msg << evm_.tid;
    XCEPT_RETHROW(exception::I2O, msg.str(), e);
  }

  auto status = totalEventsInLumisection.wait_for(std::chrono::seconds(5));

  if (status != std::future_status::ready)
  {
    std::ostringstream msg;
    msg << "Failed to receive total event count for LS " << lumiSection;
    msg << " from EVM with 5 seconds.";
    msg << " Is the fragmentRequestFIFOCapacity on the EVM too small?";
    XCEPT_RAISE(exception::I2O, msg.str());
  }

  return totalEventsInLumisection.get();
}

void evb::bu::RUproxy::runInfoCallback(toolbox::mem::Reference* bufRef)
{
  try
  {
    const I2O_MESSAGE_FRAME* stdMsg =
      (I2O_MESSAGE_FRAME*)bufRef->getDataLocation();
    const msg::RunInfoMsg* runInfoMsg =
      (msg::RunInfoMsg*)stdMsg;

    {
      std::lock_guard<std::mutex> guard(runInfoMutex_);

      auto pos = runInfo_.find(runInfoMsg->runNumber);
      pos->second.set_value(runInfoMsg->numberOfEventsBuilt);
      runInfo_.erase(pos);
    }
    bufRef->release();
  }
  catch(xcept::Exception& e)
  {
    if (bufRef) bufRef->release();
    bu_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    if (bufRef) bufRef->release();
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, e.what());
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    if (bufRef) bufRef->release();
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, "unkown exception");
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
}

uint64_t evb::bu::RUproxy::getTotalEvents(const uint32_t runNumber)
{
  toolbox::mem::Reference* rqstRunInfoBufRef = 0;
  std::future<uint64_t> totalEvents;
  const uint32_t msgSize = sizeof(msg::RequestRunInfoMsg);

  {
    std::lock_guard<std::mutex> guard(runInfoMutex_);

    RunInfo::iterator pos = runInfo_.emplace(std::make_pair(runNumber,std::promise<uint64_t>()));
    totalEvents = pos->second.get_future();
  }

  do
  {
    try
    {
      rqstRunInfoBufRef = toolbox::mem::getMemoryPoolFactory()->
        getFrame(msgPool_, msgSize);
      rqstRunInfoBufRef->setDataSize(msgSize);
    }
    catch(toolbox::mem::exception::Exception&)
    {
      rqstRunInfoBufRef = 0;
      ::usleep(100);
    }
  } while ( !rqstRunInfoBufRef );

  I2O_MESSAGE_FRAME* stdMsg =
    (I2O_MESSAGE_FRAME*)rqstRunInfoBufRef->getDataLocation();
  I2O_PRIVATE_MESSAGE_FRAME* pvtMsg = (I2O_PRIVATE_MESSAGE_FRAME*)stdMsg;
  msg::RequestRunInfoMsg* rqstRunInfo = (msg::RequestRunInfoMsg*)stdMsg;
  stdMsg->VersionOffset     = 0;
  stdMsg->MsgFlags          = 0;
  stdMsg->MessageSize       = msgSize >> 2;
  stdMsg->InitiatorAddress  = tid_;
  stdMsg->TargetAddress     = evm_.tid;
  stdMsg->Function          = I2O_PRIVATE_MESSAGE;
  pvtMsg->OrganizationID    = XDAQ_ORGANIZATION_ID;
  pvtMsg->XFunctionCode     = I2O_REQUEST_RUN_EVENTS_INFO;
  rqstRunInfo->runNumber = runNumber;

  try
  {
    bu_->postMessage(rqstRunInfoBufRef,evm_.descriptor);
  }
  catch(exception::I2O& e)
  {
    std::ostringstream msg;
    msg << "Failed to send lumisection info request to EVM TID ";
    msg << evm_.tid;
    XCEPT_RETHROW(exception::I2O, msg.str(), e);
  }

  auto status = totalEvents.wait_for(std::chrono::seconds(5));

  if (status != std::future_status::ready)
  {
    std::ostringstream msg;
    msg << "Failed to receive total event count for run number " << runNumber;
    msg << " from EVM with 5 seconds.";
    msg << " Is the fragmentRequestFIFOCapacity on the EVM too small?";
    XCEPT_RAISE(exception::I2O, msg.str());
  }

  return totalEvents.get();
}


cgicc::div evb::bu::RUproxy::getHtmlSnipped() const
{
  using namespace cgicc;

  cgicc::div div;
  div.add(p("RUproxy"));

  {
    table table;
    table.set("title","Statistics of super fragments received from the EVM/RUs.");

    std::lock_guard<std::mutex> guard(fragmentMonitoringMutex_);

    table.add(tr()
              .add(td("last event number from EVM"))
              .add(td(std::to_string(fragmentMonitoring_.lastEventNumberFromEVM))));
    table.add(tr()
              .add(td("last event number from RUs"))
              .add(td(std::to_string(fragmentMonitoring_.lastEventNumberFromRUs))));
    table.add(tr()
              .add(td("# incomplete super fragments"))
              .add(td(std::to_string(fragmentMonitoring_.incompleteSuperFragments))));
    table.add(tr()
              .add(th("Event data").set("colspan","2")));
    table.add(tr()
              .add(td("throughput (MB/s)"))
              .add(td(doubleToString(fragmentMonitoring_.throughput / 1e6,2))));
    table.add(tr()
              .add(td("fragment rate (Hz)"))
              .add(td(std::to_string(fragmentMonitoring_.fragmentRate))));
    table.add(tr()
              .add(td("I2O rate (Hz)"))
              .add(td(std::to_string(fragmentMonitoring_.i2oRate))));
    table.add(tr()
              .add(td("Fragments/I2O"))
              .add(td(doubleToString(fragmentMonitoring_.packingFactor,1))));
    div.add(table);
  }

  div.add(eventFragmentFIFO_.getHtmlSnipped());

  {
    table table;
    table.set("title","Statistics of requests sent to the EVM.");

    std::lock_guard<std::mutex> guard(requestMonitoringMutex_);

    table.add(tr()
              .add(th("Event requests").set("colspan","2")));
    table.add(tr()
              .add(td("throughput (kB/s)"))
              .add(td(doubleToString(requestMonitoring_.throughput / 1e3,2))));
    table.add(tr()
              .add(td("request rate (Hz)"))
              .add(td(std::to_string(requestMonitoring_.requestRate))));
    table.add(tr()
              .add(td("I2O rate (Hz)"))
              .add(td(std::to_string(requestMonitoring_.i2oRate))));
    table.add(tr()
              .add(td("I2O retry rate (Hz)"))
              .add(td(doubleToString(requestMonitoring_.retryRate,2))));
    table.add(tr()
              .add(td("I2O retry count"))
              .add(td(std::to_string(requestMonitoring_.retryCount))));
    table.add(tr()
              .add(td("Events requested/I2O"))
              .add(td(doubleToString(requestMonitoring_.packingFactor,1))));
    div.add(table);
  }

  div.add(getStatisticsPerRU());

  return div;
}


cgicc::table evb::bu::RUproxy::getStatisticsPerRU() const
{
  using namespace cgicc;

  std::lock_guard<std::mutex> guard(fragmentMonitoringMutex_);

  table table;
  table.set("title","Statistics of received super fragments and total payload per EVM/RU.");

  table.add(tr()
            .add(th("Statistics per RU").set("colspan","5")));
  table.add(tr()
            .add(td("Instance"))
            .add(td("TID"))
            .add(td("Fragments"))
            .add(td("Payload (MB)"))
            .add(td("&Delta;T (ns)")));

  for (auto const& countsPerRU : fragmentMonitoring_.countsPerRU)
  {
    try
    {
      const xdaq::ApplicationDescriptor* ru = i2o::utils::getAddressMap()->getApplicationDescriptor(countsPerRU.first);
      const std::string url = ru->getContextDescriptor()->getURL() + "/" + ru->getURN();

      const std::string label = (countsPerRU.first == evm_.tid) ? "EVM" :
        "RU "+std::to_string(ru->getInstance());

      table.add(tr()
                .add(td()
                     .add(a(label).set("href",url).set("target","_blank")))
                .add(td(std::to_string(countsPerRU.first)))
                .add(td(std::to_string(countsPerRU.second.logicalCount)))
                .add(td(std::to_string(countsPerRU.second.payload / 1000000)))
                .add(td(std::to_string(countsPerRU.second.deltaTns))));
    }
    catch (xdaq::exception::ApplicationDescriptorNotFound& e)
    {
      const std::string label = (countsPerRU.first == evm_.tid) ? "EVM" : "RU";

      table.add(tr()
                .add(td(label))
                .add(td(std::to_string(countsPerRU.first)))
                .add(td(std::to_string(countsPerRU.second.logicalCount)))
                .add(td(std::to_string(countsPerRU.second.payload / 1000000)))
                .add(td(std::to_string(countsPerRU.second.deltaTns))));
    }
  }
  return table;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
