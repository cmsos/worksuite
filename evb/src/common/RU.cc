#include <memory>
#include <utility>

#include "evb/RU.h"
#include "evb/readoutunit/BUproxy.h"
#include "evb/readoutunit/Configuration.h"
#include "evb/readoutunit/FerolConnectionManager.h"
#include "evb/readoutunit/Input.h"
#include "evb/readoutunit/States.h"


evb::RU::RU(xdaq::ApplicationStub* app) :
  readoutunit::ReadoutUnit<RU,readoutunit::Configuration,readoutunit::StateMachine<RU>>(app,"/evb/images/ru64x64.gif")
{
  this->stateMachine_ = std::make_shared<readoutunit::StateMachine<RU>>(this);
  this->input_ = std::make_shared<readoutunit::Input<RU,readoutunit::Configuration>>(this);
  this->ferolConnectionManager_ = std::make_shared<readoutunit::FerolConnectionManager<RU,readoutunit::Configuration>>(this);
  this->buProxy_ = std::make_shared<readoutunit::BUproxy<RU>>(this);

  this->initialize();

  LOG4CPLUS_INFO(this->getApplicationLogger(), "End of constructor");
}

void evb::RU::timeExpired (toolbox::task::TimerEvent& e) {

	xdata::String maskedDipTopics =  this->configuration_->maskedDipTopics;
	evb::readoutunit::MetaDataRetrieverDIPBridgePtr dipRetriver = this->input_.get()->getMetaDataRetrieverDIPBridgePtr();
	dipRetriver->registerToDipBridge(maskedDipTopics);

}


namespace evb {
  namespace readoutunit {

    template<>
    void BUproxy<RU>::handleRequest(const msg::EventRequest* eventRequest, FragmentRequestPtr& fragmentRequest)
    {
      eventRequest->getEvBids(fragmentRequest->evbIds);
      eventRequest->getRUtids(fragmentRequest->ruTids);

      fragmentRequestFIFO_.enqWait(std::move(fragmentRequest));
    }


    template<>
    bool BUproxy<RU>::processRequest(FragmentRequestPtr& fragmentRequest, SuperFragments& superFragments)
    {
      std::lock_guard<std::mutex> guard(processingRequestMutex_);

      if ( doProcessing_ && fragmentRequestFIFO_.deq(fragmentRequest) )
      {
        try
        {
          for (uint32_t i=0; i < fragmentRequest->nbRequests; ++i)
          {
            const EvBid& evbId = fragmentRequest->evbIds.at(i);
            SuperFragmentPtr superFragment;
            readoutUnit_->getInput()->getSuperFragmentWithEvBid(evbId, superFragment);
            superFragments.push_back(std::move(superFragment));
          }
          --requestMonitoring_.activeRequests;

          return true;
        }
        catch(exception::HaltRequested&)
        {
          return false;
        }
      }

      return false;
    }


    template<>
    bool BUproxy<RU>::isEmpty()
    {
      std::lock_guard<std::mutex> guard(processesActiveMutex_);
      return ( fragmentRequestFIFO_.empty() && processesActive_.none() );
    }


    template<>
    std::string BUproxy<RU>::getHelpTextForBuRequests() const
    {
      return "BU event requests forwarded by the EVM. If no events are requested, the EVM has not got any requests or has no data.";
    }


    template<>
    void readoutunit::ReadoutUnit<RU,readoutunit::Configuration,readoutunit::StateMachine<RU>>::addComponentsToWebPage
    (
      cgicc::table& table
    ) const
    {
      using namespace cgicc;

      table.add(tr()
                .add(td(input_->getHtmlSnipped()).set("class","xdaq-evb-component"))
                .add(td(img().set("src","/evb/images/arrow_e.gif").set("alt","")))
                .add(td(buProxy_->getHtmlSnipped()).set("class","xdaq-evb-component")));
    }
  }
}


/**
 * Provides the factory method for the instantiation of RU applications.
 */
XDAQ_INSTANTIATOR_IMPL(evb::RU)



/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
