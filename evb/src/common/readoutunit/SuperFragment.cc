#include <sstream>
#include <utility>

#include "evb/Exception.h"
#include "evb/readoutunit/SuperFragment.h"


evb::readoutunit::SuperFragment::SuperFragment(const EvBid& evbId, const std::string& subSystem)
  : evbId_(evbId),subSystem_(subSystem),size_(0)
{}


void evb::readoutunit::SuperFragment::discardFedId(const uint16_t fedId)
{
  missingFedIds_.push_back(fedId);
}


void evb::readoutunit::SuperFragment::append(FedFragmentPtr&& fedFragment)
{
  size_ += fedFragment->getFedSize();
  fedFragments_.emplace_back(std::move(fedFragment));
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
