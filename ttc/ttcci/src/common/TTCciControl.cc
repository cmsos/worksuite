#include "ttc/ttcci/TTCciControl.hh"

#include "ttc/ttcci/TTCciAddresses.hh"
#include "ttc/ttcci/TTCciControlSoapHandler.hh"
#include "ttc/ttcci/version.h"

#include "ttc/utils/Utils.hh"
#include "ttc/utils/HTMLMacros.hh"
#include "ttc/utils/HTMLTable.hh"
#include "ttc/utils/RAMTriggers.hh"
#include "ttc/utils/CgiUtils.hh"

#include "hal/HardwareAccessException.hh"
#include "toolbox/string.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "xcept/tools.h"
#include "xcept/Exception.h"
#include "toolbox/fsm/exception/Exception.h"

#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPHTMLHeader.h"

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <sstream>
#include <map>


using namespace std;


// class ttc::TTCciProxy

ttc::TTCciProxy::TTCciProxy()
:
    bTimeCorrection_(0),
    delayT2Correction_(0)
{}


ttc::BoardTempLocker<ttc::TTCci> ttc::TTCciProxy::operator->()
{
  return BoardTempLocker<TTCci>(*this);
}


void ttc::TTCciProxy::initAdditionalParams(
    const uint32_t& bTimeCorrection,
    const uint32_t& delayT2Correction)
{
  bTimeCorrection_ = bTimeCorrection;
  delayT2Correction_ = delayT2Correction;
}


ttc::TTCci* ttc::TTCciProxy::createPtr(HAL::VMEBusAdapterInterface* busAdapter)
{
  return new ttc::TTCci(
        *busAdapter,
        boardSlot_,
        bTimeCorrection_,
        delayT2Correction_,
        enableVMEWrite_);
}


// struct ttc::CountersPopupData

namespace ttc
{

//! class representing the BGO counters, used to compare values between different updates of the popup
struct CountersPopupData
{
  /// Time (approximately...) when this information was read.
  SimpleTime time;

  unsigned event_counter;
  vector<unsigned> bgos_requested;
  vector<unsigned> bgos_cancelled;

  /** Empty constructor for STL vector. */
  CountersPopupData() :
      event_counter(0)
  {
  }

  /** Reads the information from the TTCci and sets the current
   time. */
  void refresh(TTCciProxy& ttcciProxy)
  {
    SimpleTime now;
    now.setNow();

    if (time == 0 || ((now - time) > 1))
    {
      event_counter = ttcciProxy->ReadEventCounter();

      unsigned num_channels = ttcciProxy->NChannels();
      bgos_requested.resize(num_channels);
      bgos_cancelled.resize(num_channels);

      for (unsigned ch = 0; ch < num_channels; ++ch)
      {
        // Note that we don't use the methods
        // TTCci::GetNumBgosRequested() and
        // TTCci::GetNumBgosCancelled() because we can extract both of
        // these values (for one channel) with one VME read.
        const uint32_t rmc = ttcciProxy->Read(TTCciAdd::CHRMC, ch, "(TTCciControl)");
        bgos_requested[ch] = ttcciProxy->BGORequestCounter(rmc);
        bgos_cancelled[ch] = ttcciProxy->BGOCancelledCounter(rmc);
      } // loop over all BGO channels

      time.setNow();
    }
  }

  double BgosPerOrbit(const CountersPopupData& previous, unsigned channel) const
  {
    if (channel >= bgos_requested.size() || channel >= previous.bgos_requested.size())
    {
      return 0;
    }

    SimpleTime now(time);
    SimpleTime before(previous.time);
    SimpleTime diff = now - before;
    double time_diff = double(diff);

    time_diff = ::max(time_diff, 1.e-8);

    return (bgos_requested[channel] - previous.bgos_requested[channel]) * GenericTTCModule::nominal_orbit_time_in_sec
        / time_diff;
  }

  std::string BgosPerOrbitString(const CountersPopupData& previous, unsigned channel) const
  {
    ostringstream buf;
    buf << boost::format("%.1f") % BgosPerOrbit(previous, channel);
    return buf.str();
  }
}; //struct CountersPopupData

}


// class ttc::TTCciControl

XDAQ_INSTANTIATOR_IMPL(ttc::TTCciControl);


ttc::TTCciControl::TTCciControl(xdaq::ApplicationStub* stub)
:
    TTCXDAQBase(stub, "TTCciControl", "TTCci"),
    // info space items
    BTimeCorrection_(DEFAULT_TTCCI_BTC),
    DelayT2Correction_(DEFAULT_TTCCI_DT2),
    TTCciObjectCreated_(false),
    previousEventCounter_(0),
    EventCounter_(0),
    previousOrbitCounter_(0),
    OrbitCounter_(0),
    previousStrobeCounter_(0),
    StrobeCounter_(0),
    BoardStatus_(0),
    QPLLLocked_(false),
    NumBgosRequested_(ttc::TTCci::NChannels()),
    NumBgosCancelled_(ttc::TTCci::NChannels()),
    ClockSource_("Undefined"),
    ClockFrequency_(0),
    OrbitSource_("Undefined"),
    TriggerSource_("Undefined"),
    BGOSource_("Undefined"),
    currentBGO_(0),
    debugb_short_(true),
    debugbdata_(0),
    soapHandler_(0),
    monQPLL_(this),
    monStatus_(this),
    monScalers_(this),
    monConfig_(this)
{
  // info space items and listeners

  addItem("BTimeCorrection",   BTimeCorrection_);
  addItem("DelayT2Correction", DelayT2Correction_);

  addItemRetrievable("TTCciObjectCreated",  TTCciObjectCreated_);
  addItemRetrievable("EventCounter",        EventCounter_);
  addItemRetrievable("OrbitCounter",        OrbitCounter_);
  addItemRetrievable("StrobeCounter",       StrobeCounter_);
  addItemRetrievable("BoardStatus",         BoardStatus_);
  addItemRetrievable("QPLLLocked",          QPLLLocked_);
  for (unsigned i = 0; i < ttc::TTCci::NChannels(); ++i)
  {
    string s = boost::str(boost::format("%02d") % i);
    addItemRetrievable("NumBgosRequested"+s, NumBgosRequested_[i]);
    addItemRetrievable("NumBgosCancelled"+s, NumBgosCancelled_[i]);
  }

  addItemRetrievableChangeable("ClockSource",    ClockSource_);
  addItemRetrievableChangeable("ClockFrequency", ClockFrequency_);
  addItemRetrievableChangeable("OrbitSource",    OrbitSource_);
  addItemRetrievableChangeable("TriggerSource",  TriggerSource_);
  addItemRetrievableChangeable("BGOSource",      BGOSource_);

  // SOAP bindings
  soapHandler_ = new TTCciControlSoapHandler(this);

  // CGI bindings
  cgi_bind(this, &TTCciControl::HTMLPageMainConfiguration,               "MainConfiguration", "Main Config");
  cgi_bind(this, &TTCciControl::HTMLPageBGOConfiguration,                "BGOConfiguration",  "BGO Config");
  cgi_bind(this, &TTCciControl::HTMLPageSequences,                       "Sequences",         "Sequences");
  cgi_bind(this, &TTCciControl::HTMLPageCyclicGenerators,                "CyclicGenerators",  "Cyclic Gen.");
  cgi_bind(this, &TTCciControl::HTMLPageSummary,                         "SummaryPage",       "Summary");
  cgi_bind(this, &TTCciControl::HTMLPageRegisterAccess,                  "RegisterAccess",    "Registers");
  cgi_bind(this, &TTCciControl::HTMLPageCountersPopup,                   "ShowCountersPopup");
  cgi_bind(this, &TTCciControl::HTMLPageCommandNewConfigFile,            "NewConfigurationFile");
  cgi_bind(this, &TTCciControl::HTMLPageCommandWriteConfigFile,          "WriteConfigurationFile");
  cgi_bind(this, &TTCciControl::HTMLPageCommandMainConfig,               "MainConfigCommand");
  cgi_bind(this, &TTCciControl::HTMLPageCommandTriggerRules,             "TriggerRules");
  cgi_bind(this, &TTCciControl::HTMLPageCommandSelectBGO,                "BGOSelectCommand");
  cgi_bind(this, &TTCciControl::HTMLPageCommandConfigureBGO,             "BGOConfigCommand");
  cgi_bind(this, &TTCciControl::HTMLPageCommandSelectSequence,           "SequenceSelectCommand");
  cgi_bind(this, &TTCciControl::HTMLPageCommandEditSequence,             "SequenceEditCommand");
  cgi_bind(this, &TTCciControl::HTMLPageCommandConfigureCyclicGenerator, "CyclicConfigCommand");
  cgi_bind(this, &TTCciControl::HTMLPageCommandSendVMEBData,             "VMEBData");
  cgi_bind(this, &TTCciControl::HTMLPageCommandRegisterAccess,           "RegisterAccessCommand");
}


ttc::TTCciControl::~TTCciControl()
{
  if (soapHandler_)
  {
    delete soapHandler_;
  }
  soapHandler_ = 0;
}


ttc::TTCciProxy& ttc::TTCciControl::boardLockingProxy()
{
  return ttcciProxy_;
}


string ttc::TTCciControl::softwareVersion()
{
  ostringstream oss;
  oss << WORKSUITE_TTCTTCCI_VERSION_MAJOR << "." << WORKSUITE_TTCTTCCI_VERSION_MINOR << "." << WORKSUITE_TTCTTCCI_VERSION_PATCH;
  return oss.str();
}


void ttc::TTCciControl::itemRetrieveAction(xdata::ItemRetrieveEvent& e)
{
  TTCXDAQBase::itemRetrieveAction(e);

  std::string itemName = e.itemName();

  if (itemName == "TTCciObjectCreated")
  {
    TTCciObjectCreated_ = boardLockingProxy().boardResourcesInitialized();
  }
  else if (
      itemName == "EventCounter" ||
      itemName == "OrbitCounter" ||
      itemName == "StrobeCounter" ||
      itemName == "BoardStatus" ||
      itemName == "QPLLLocked")
  {
    ReadTTCciCounters();
  }
  else if (itemName == "ClockSource")
  {
    ClockSource_ = INAME(boardLockingProxy()->getSelectedClockSource());
  }
  else if (itemName == "ClockFrequency")
  {
    ClockFrequency_ = boardLockingProxy()->GetQPLLFrequencyBits();
  }
  else if (itemName == "OrbitSource")
  {
    OrbitSource_ = INAME(boardLockingProxy()->getSelectedOrbitSource());
  }
  else if (itemName == "TriggerSource")
  {
    std::vector<ExternalInterface> vinf = boardLockingProxy()->getSelectedTriggerSources();
    TriggerSource_ = "UNDEFINED";
    for (size_t i = 0; i < vinf.size(); ++i)
      TriggerSource_ = INAME(vinf[i]);  //< FIXME ?
  }
  else if (itemName == "BGOSource")
  {
    std::vector<ExternalInterface> vinf = boardLockingProxy()->CheckBGOSource();
    BGOSource_ = "UNDEFINED";
    for (size_t i = 0; i < vinf.size(); ++i)
      BGOSource_ = INAME(vinf[i]); //< FIXME ?
  }
  else {
    for (unsigned i = 0; i < ttc::TTCci::NChannels(); ++i)
    {
      string s = boost::str(boost::format("%02d") % i);

      if (itemName == "NumBgosRequested"+s)
        NumBgosRequested_[i] = boardLockingProxy()->GetNumBgosRequested(i);
      else if (itemName == "NumBgosCancelled"+s)
        NumBgosCancelled_[i] = boardLockingProxy()->GetNumBgosCancelled(i);
      else continue;

      break;
    }
  }
}


void ttc::TTCciControl::itemChangedAction(xdata::ItemChangedEvent& e)
{
  TTCXDAQBase::itemChangedAction(e);

  std::string itemName = e.itemName();

  if (itemName == "ClockSource")
  {
    ttc::ExternalInterface inf = INTERFACE(ClockSource_);
    if (inf != ttc::UNDEFINED)
    {
      boardLockingProxy()->SelectClock(inf);
    }
    else
    {
      string msg = "Invalid value for " + itemName + "='" + ClockSource_.toString() + "'";
      ClockSource_ = INAME(boardLockingProxy()->getSelectedClockSource());
      XCEPT_RAISE(xcept::Exception, msg);
    }
  }

  else if (itemName == "ClockFrequency")
  {
    boardLockingProxy()->SetQPLLFrequencyBits(ClockFrequency_);
    ClockFrequency_ = boardLockingProxy()->GetQPLLFrequencyBits();
  }

  else if (itemName == "OrbitSource")
  {
    ttc::ExternalInterface inf = INTERFACE(OrbitSource_);
    if (inf != ttc::UNDEFINED)
    {
      boardLockingProxy()->SelectOrbitSource(inf);
    }
    else
    {
      string msg = "Invalid value for " + itemName + "='" + OrbitSource_.toString() + "'";
      OrbitSource_ = INAME(boardLockingProxy()->getSelectedOrbitSource());
      XCEPT_RAISE(xcept::Exception, msg);
    }
  }

  else if (itemName == "TriggerSource")
  {
    ttc::ExternalInterface inf = INTERFACE(TriggerSource_);
    if (inf != ttc::UNDEFINED)
    {
      std::vector<ExternalInterface> vinf;
      vinf.push_back(inf);
      boardLockingProxy()->SelectTrigger(vinf);
    }
    else
    {
      string msg = "Invalid value for " + itemName + "='" + TriggerSource_.toString() + "'";
      std::vector<ExternalInterface> vinf = boardLockingProxy()->getSelectedTriggerSources();
      TriggerSource_ = "UNDEFINED";
      for (size_t i = 0; i < vinf.size(); ++i)
        TriggerSource_ = INAME(vinf[i]); //< FIXME ?

      XCEPT_RAISE(xcept::Exception, msg);
    }
  }
  else if (itemName == "BGOSource")
  {
    ttc::ExternalInterface inf = INTERFACE(BGOSource_);
    if (inf != ttc::UNDEFINED)
    {
      std::vector<ExternalInterface> vinf;
      vinf.push_back(inf);
      boardLockingProxy()->SelectBGOSource(vinf);
    }
    else
    {
      string msg = "Invalid value for " + itemName + "='" + BGOSource_.toString() + "'";
      std::vector<ExternalInterface> vinf = boardLockingProxy()->CheckBGOSource();
      BGOSource_ = "UNDEFINED";
      for (size_t i = 0; i < vinf.size(); ++i)
        BGOSource_ = INAME(vinf[i]); //< FIXME ?
      XCEPT_RAISE(xcept::Exception, msg);
    }
  }
}


void ttc::TTCciControl::setDefaultValuesAction()
{
  boardLockingProxy().initAdditionalParams(BTimeCorrection_, DelayT2Correction_);
  TTCXDAQBase::setDefaultValuesAction();

  // finally init monitoring, now that the BoardProxy is ready
  if (!DisableMonitoring_)
  {
    monQPLL_.init();
    monStatus_.init();
    monScalers_.init();
    monConfig_.init();
  }
}


void ttc::TTCciControl::ConfigureAction(toolbox::Event::Reference e)
{
  if (ReadConfigFromFile_)
  {
    string filePath = asciConfigurationFilePath_.toString();

    LOG4CPLUS_INFO(
        logger_,
        "TTCciControl::ConfigureAction(): Input file = '" + filePath + "'.");

    ifstream in(filePath.c_str(), ios_base::in);
    if (!in)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "Cannot open input file '" + filePath + "' for reading");
    }

    try {
      boardLockingProxy()->Configure(in);
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(
          xcept::Exception,
          "TTCci::Configure with configuration from file'" + filePath + "' failed", e);
    }
  }
  else
  {
    LOG4CPLUS_INFO(
        logger_,
        "TTCciControl::ConfigureAction(): Reading configuration from XML parameter 'Configuration'");

    stringstream input;
    input << ConfigurationString_.toString() << endl;

    try {
      boardLockingProxy()->Configure(input);
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(
          xcept::Exception,
          "TTCci::Configure with configuration from XML parameter 'Configuration' failed", e);
    }
  }

  boardLockingProxy()->ResetCounters();

  try {
    boardLockingProxy()->ExecuteSequence("configure");
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(
        xcept::Exception,
        "ExecuteSequence(configure) failed", e);
  }

  if (boardLockingProxy()->IsL1AEnabled())
  {
    std::string msg =
        "L1A is enabled after 'configure'. "
        "Please enable L1A only in the 'enable' sequence.";

    error_messages.add("WARNING") << msg;
    LOG4CPLUS_WARN(logger_, msg);
  }
}


void ttc::TTCciControl::EnableAction(toolbox::Event::Reference e)
{
  try {
    boardLockingProxy()->ExecuteSequence("enable");
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(
        xcept::Exception,
        "ExecuteSequence(enable) failed", e);
  }

  if (!boardLockingProxy()->IsL1AEnabled())
  {
    std::string msg =
        "L1A is disabled after 'enable'. "
        "Please enable L1A in the 'enable' sequence.";

    error_messages.add("WARNING") << msg;
    LOG4CPLUS_WARN(logger_, msg);
  }
}


void ttc::TTCciControl::StopAction(toolbox::Event::Reference e)
{
  try {
    boardLockingProxy()->ExecuteSequence("stop");
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(
        xcept::Exception,
        "ExecuteSequence(stop) failed", e);
  }

  if (boardLockingProxy()->IsL1AEnabled())
  {
    std::string msg =
        "L1A is enabled after 'stop'. "
        "Please disable L1A in the 'stop' and 'suspend' sequences.";

    error_messages.add("WARNING") << msg;
    LOG4CPLUS_WARN(logger_, msg);
  }

  ReadTTCciCounters();
}


void ttc::TTCciControl::SuspendAction(toolbox::Event::Reference e)
{
  try {
    boardLockingProxy()->ExecuteSequence("suspend");
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(
        xcept::Exception,
        "ExecuteSequence(suspend) failed", e);
  }

  if (boardLockingProxy()->IsL1AEnabled())
  {
    std::string msg =
        "L1A is enabled after 'Suspend'. "
        "Please disable L1A in the 'stop' and 'suspend' sequences.";

    error_messages.add("WARNING") << msg;
    LOG4CPLUS_WARN(logger_, msg);
  }
}


void ttc::TTCciControl::CommandImpl(cgicc::Cgicc& cgi, const string& command)
{
  LOG4CPLUS_INFO(
      logger_,
      "TTCciControl::CommandImpl: Executing CGI command '" + command + "'");

  string sdummy;

  if (command == "ReadCounters"
      || command == "ResetCounters"
      || command == "ResetStatus"
      || command == "DumpVMEHistory"
      || command == "Send VME-BGO"
      || command == "Send BGO-Start"
      || command == "Send BGO-Stop"
      || command == "Read BGO configuration from TTCci")
  {
    map<string, string> params;
    params["CommandPar"] = command;
    sendSOAPCommandToMe("userCommand", params);
  }
  else if (
      ttc::FindString(command, "Execute", sdummy) &&
      ttc::FindString(command, "Sequence", sdummy))
  {
    // Execute a sequence (button clicked on the sequence editing page).
    std::map<std::string, std::string> vars;
    vars["CommandPar"] = "Execute Sequence";
    vars["sequence_name"] = SequenceSelector.GetDefaultTitle();
    sendSOAPCommandToMe("userCommand", vars);
  }
  else
  {
    XCEPT_RAISE(xcept::Exception, "Unknown TTCci CGI command '" + command + "'");
  }
}


void ttc::TTCciControl::HTMLPageMain(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageMain");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << cgicc::title("TTCci '" + name_.toString() + "'") << endl;

  if (autoRefresh_)
  {
    xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"3; URL=" << lastPage_ << "\">" << endl;
  }

  WriteJavascriptPopupCode(xoh);

  xoh << "</head>\n";
  xoh << "<body>\n";

  PrintHTMLHeader(xoh);

  ReadTTCciCounters();

  ErrorStatement(xoh);

  // The Finite State Machine "table".
  PrintFSMTable(xoh);

  // The table with the 'execute sequence' buttons.
  PrintSequenceTable(xoh);

  // The status table.
  HTMLTable tab(xoh, 1, 10, 0, "", "center");
  tab.NewRow();
  tab.NewCell();
  xoh << "<b>Status</b>";
  tab.NewCell();

  // Laser status.
  if (boardLockingProxy()->IsLaserOn())
  {
    xoh << GREEN << "Laser on" << NOCOL;
  }
  else
  {
    xoh << RED << "Laser off" << NOCOL;
  }

  // Periodic sequence status.
  tab.NewCell();
  if (!boardLockingProxy()->PeriodicSequenceEnabled() || (boardLockingProxy()->Periodicity() < 0.0))
  {
    xoh << "Periodic seq.: " << GREY << "Off" << endl;
    xoh << NOCOL << endl;
  }
  else
  {
    xoh << "Periodic seq. :" << RED << "On (" << boardLockingProxy()->Periodicity() << " s)" << NOCOL;
  }

  // Detailed board status.
  tab.NewRow();
  tab.NewCell("", 3);

  xoh
      << UNDERL << "Board status:" << NOUNDERL<< MONO << BLUE << hex << " 0x"
      << BoardStatus_ << NOCOL << NOMONO << dec << endl;

  string defaultcol = GREEN, alertcol = RED;

  {
    bool isok = false;
    bool isok2 = false;
    string col = "";

    // BDataCancelled.
    isok = !boardLockingProxy()->IsBDataCancelled();
    isok2 = !boardLockingProxy()->IsBDataCancelled_Latched();
    col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "BData cancelled? " << col << (isok ? "no" : "yes") << NOCOL;
    col = (isok2 ? defaultcol : alertcol);
    xoh << " (" << col << (isok2 ? "no" : "yes") << NOCOL << ")" << endl;

    // DataClkError.
    isok = !boardLockingProxy()->IsDataClkError();
    isok2 = !boardLockingProxy()->IsDataClkError_Latched();
    col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "Data-to-clock sync error? " << col << (isok ? "no" : "yes") << NOCOL;
    col = (isok2 ? defaultcol : alertcol);
    xoh << " (" << col << (isok2 ? "no" : "yes") << NOCOL << ")" << endl;

    isok = !boardLockingProxy()->OrbitSyncError();
    isok2 = !boardLockingProxy()->OrbitSyncError_Latched();
    col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "Orbit sync error? " << col << (isok ? "no" : "yes") << NOCOL;
    col = (isok2 ? defaultcol : alertcol);
    xoh << " (" << col << (isok2 ? "no" : "yes") << NOCOL << ")" << endl;

    // IsClkSingleEvtUpset.
    isok = !boardLockingProxy()->IsClkSingleEvtUpset();
    isok2 = !boardLockingProxy()->IsClkSingleEvtUpset_Latched();
    col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "Clock single-event upset? " << col << (isok ? "no" : "yes") << NOCOL;
    col = (isok2 ? defaultcol : alertcol);
    xoh << " (" << col << (isok2 ? "no" : "yes") << NOCOL << ")" << endl;

    isok = !boardLockingProxy()->TriggerSuppressed();
    isok2 = !boardLockingProxy()->TriggerSuppressed_Latched();
    col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "L1A vetoed by trigger rules? " << col << (isok ? "no" : "yes") << NOCOL;
    col = (isok2 ? defaultcol : alertcol);
    xoh << " (" << col << (isok2 ? "no" : "yes") << NOCOL << ")" << endl;

    // MissedL1A.
    isok = !boardLockingProxy()->MissedL1A();
    isok2 = !boardLockingProxy()->MissedL1A_Latched();
    col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "L1A missed @ 40MHz? " << col << (isok ? "no" : "yes") << NOCOL;
    col = (isok2 ? defaultcol : alertcol);
    xoh << " (" << col << (isok2 ? "no" : "yes") << NOCOL << ")" << endl;

    // DoubleL1Aat40MHz.
    isok = !boardLockingProxy()->DoubleL1Aat40MHz();
    isok2 = !boardLockingProxy()->DoubleL1Aat40MHz_Latched();
    col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "Double-L1A @ 40MHz? " << col << (isok ? "no" : "yes") << NOCOL;
    col = (isok2 ? defaultcol : alertcol);
    xoh << " (" << col << (isok2 ? "no" : "yes") << NOCOL << ")" << endl;

    // Clock locked?
    // NOTE: On the internal clock the QPLL never says it's locked!
    bool locked = boardLockingProxy()->ClockLocked();
    bool clockInternal = (boardLockingProxy()->getSelectedClockSource() == INTERNAL);
    std::string res = "";
    std::string resLatched = "";
    string colLatched = "";
    if (locked)
    {
      col = defaultcol;
      res = "yes";
    }
    else
    {
      if (clockInternal)
      {
        col = defaultcol;
        res = "n/a on internal clock";
      }
      else
      {
        col = alertcol;
        res = "no";
      }
    }
    if (!clockInternal)
    {
      bool lockedLatched = boardLockingProxy()->ClockLocked_Latched();
      if (lockedLatched)
      {
        colLatched = defaultcol;
        resLatched = "yes";
      }
      else
      {
        colLatched = alertcol;
        resLatched = "no";
      }
    }
    xoh << "<br>" << "Clock locked? " << col << res << NOCOL;
    if (!clockInternal)
    {
      xoh << " (" << colLatched << resLatched << ")" << NOCOL;
    }
    xoh << endl;

    // Clock inverted?
    bool inv = boardLockingProxy()->ClockInverted();
    xoh << "<br>" << "Clock inverted? " << (inv ? "yes" : "no") << endl;

    xoh << SMALLER << "<br> &nbsp;&nbsp;&nbsp;&nbsp;" << "(...) = latched<br>"
        << "Latched bits are regularly cleared in the QPLL monitoring thread." << NOSMALLER << endl;
  }

  tab.Close();

  // Counters table.
  std::string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/Command";
  tab.Open();

  tab.NewRow();
  tab.NewCell();

  xoh
      << BOLD << "<center> Counters " << NOBOLD << "<br/>"
      << "<font size=\"-2\">" << "(<a href=\"ShowCountersPopup\" " << "target=\"rates_popup_frame\" "
      << "onclick=\"return openPopup('" << getFullURL() << "/ShowCountersPopup',"
      << "'ttcci_counter_popup_frame',500,500);\">" << "show counters popup</a>)" << endl << "</font>" << endl
      << "</center>";

  // Print all values read.
  tab.NewCell("", 1, 2);

  xoh
      << UNDERL << "Counters" << NOUNDERL << endl
      << "<br>" << "Evt-ID: " << EventCounter_ << endl
      << "<br>" << "Orbit: " << OrbitCounter_ << endl
      << "<br>" << "BGO-Req.Cntr: " << StrobeCounter_ << endl;

  {
    const double dt = difftime(tnow_, tprevious_);
    double diff = double(EventCounter_ - previousEventCounter_);
    double odiff = double(OrbitCounter_ - previousOrbitCounter_);

    xoh
        << "<br><br>"
        << UNDERL << "L1A rate (after " << dt << " s):" << NOUNDERL << "<br>" << endl
        << int(diff) << " L1As in " << odiff << " orbs.<br>" << endl;

    if ((odiff > 0.0) && (dt > 0.0))
      xoh << RED << (diff / (odiff * 3564 * 25.0e-9)) << " Hz" << NOCOL << "<br>" << endl;
    else
      xoh << RED << "nan" << " Hz" << NOCOL << "<br>" << endl;

    if (odiff > 0.0)
      xoh << (diff / odiff) << " L1As/orbit <br>" << endl;
    else
      xoh << "nan" << " L1As/orbit <br>" << endl;

    if (diff > 0.0)
      xoh << (odiff / diff) << " orbits/L1A <br>" << endl;
    else
      xoh << "nan" << " orbits/L1A <br>" << endl;

    diff = double(StrobeCounter_ - previousStrobeCounter_);

    xoh
        << UNDERL << "BGO-req.rate:" << NOUNDERL << "<br>" << endl
        << int(diff) << " BGOs<br>" << endl;

    if (dt > 0.0)
      xoh << RED << (diff / (odiff * 3564 * 25.0e-9)) << " Hz" << NOCOL << "<br>" << endl;
    else
      xoh << RED << "nan" << " Hz" << NOCOL << "<br>" << endl;

    if (odiff > 0.0)
      xoh << (diff / odiff) << " BGOs/orbit <br>" << endl;
    else
      xoh << "nan" << " BGOs/orbit <br>" << endl;

    if (diff > 0.0)
      xoh << (odiff / diff) << " orbits/BGO <br>" << endl;
    else
      xoh << "nan" << " orbits/BGO <br>" << endl;
  }

  xoh << NOMONO;

  // Read all counters.
  tab.NewRow();
  tab.NewCell();

  xoh << cgicc::form()
        .set("method", "get").set("action", command_url).set("enctype", "multipart/form-data") << endl
      << cgicc::input()
        .set("type", "submit").set("name", "Command").set("value", "ReadCounters")
      << cgicc::form();

  xoh << cgicc::form()
        .set("method", "get").set("action", command_url).set("enctype", "multipart/form-data") << endl;

  if (fsm_.getCurrentState() == 'E')
  {
    xoh << cgicc::input()
          .set("type", "submit").set("name", "Command").set("value", "ResetCounters").set("disabled", "true");
  }
  else
  {
    xoh << cgicc::input()
        .set("type", "submit").set("name", "Command").set("value", "ResetCounters");
  }

  xoh << cgicc::form();

  tab.Close();

  // General info.
  xoh
      << "<p>" << endl
      << BOLD << "General Info" << NOBOLD << "<br>" << endl
      << "BTimeCorrection: " << int32_t(BTimeCorrection_) << " BX, " << endl
      << "DelayT2Correction: " << int32_t(DelayT2Correction_) << " BX" << endl;

  { // Configration file display & change
    std::string confurl = "/";
    confurl += getApplicationDescriptor()->getURN();
    confurl += "/NewConfigurationFile";

    xoh << cgicc::form().set("method", "post").set("action", confurl).set("enctype", "multipart/form-data") << endl;

    GetFileList();

    xoh
        << cgicc::fieldset() << endl
        << cgicc::legend("Configure TTCci FROM file") << endl
        << cgicc::label("Current configuration source: ");

    if (ReadConfigFromFile_)
    {
      xoh << "File (" << MONO << asciConfigurationFilePath_.toString() << NOMONO << ")" << endl;
    }
    else
    {
      xoh << "NOT from file but directly from the xml input-string 'Configuration'" << endl;
    }
    xoh << "<br>" << endl;

    InputFileList.Write(xoh);

    xoh
        << cgicc::label("&nbsp;&nbsp;&nbsp;&nbsp;(enter absolute path): ") << endl
        << cgicc::input().set("type", "text").set("name", "ConfigurationFile").set("size", "60").set("value", "")
        << cgicc::p() << endl
        << "<p>" << endl
        << SMALLER
        << "N.B.: Submitting will only change the path to the configuration file, the "
        << "configuration itself (i.e. opening the file and reading in the configuration) "
        << "will happen when requesting 'configure' in the State Machine."
        << NOSMALLER << "<br>" << endl
        << cgicc::fieldset() << endl
        << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Submit")
        << "<p>" << endl
        << cgicc::form() << endl;

    // Write Configration to new file
    confurl = "/";
    confurl += getApplicationDescriptor()->getURN();
    confurl += "/WriteConfigurationFile";

    xoh
        << cgicc::form().set("method", "post").set("action", confurl).set("enctype", "multipart/form-data") << endl
        << cgicc::fieldset() << endl;

    char s[1024];
    gethostname(s, sizeof s);
    string machine = s;
    size_t pos = machine.find_first_of(".");

    if (pos < machine.size())
    {
      machine.erase(machine.begin() + pos, machine.end());
    }

    machine = string(" (on ") + MONO + machine + NOMONO + ")";

    xoh
        << cgicc::legend((string("Write current configuration TO file") + machine).c_str()) << endl
        << cgicc::label("(Default) File path: ")
        << MONO << asciConfigurationFilePath_.toString() << NOMONO
        << "<p>" << endl
        << cgicc::label("New file (enter absolute path): ") << endl
        << cgicc::input()
        .set("type", "text")
        .set("name", "ConfigurationFile")
        .set("size", "60").set("value", ReadConfigFromFile_ ? asciConfigurationFilePath_.toString() : "")
        << cgicc::p() << endl
        << "<p>" << endl
        << cgicc::input().set("type", "checkbox").set("name", "overwrite") << endl
        << "force overwrite (if file exists)" << endl
        << cgicc::fieldset() << endl
        << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Write")
        << "<p>" << endl
        << cgicc::form() << endl;
  }

  PrintHTMLFooter(xoh);
}


void ttc::TTCciControl::HTMLPageMainConfiguration(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageMainConfiguration");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
  xoh << cgicc::title("TTCci '" + name_.toString() + "'") << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  xoh << "Finite State Machine: ";
  PrintFSMState(xoh);

  xoh << cgicc::p() << cgicc::p() << endl;

  std::string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/MainConfigCommand";

  {
    xoh
        << cgicc::form()
        .set("method", "post")
        .set("action", command_url)
        .set("enctype", "multipart/form-data") << endl
        << cgicc::fieldset() << endl;

    stringstream my;
    my
        << "Input Sources (0x"
        << hex << boardLockingProxy()->ReadInputSelection() << dec
        << ") & QPLL";

    xoh << cgicc::legend(my.str().c_str()) << endl;

    // Table.
    HTMLTable tab(xoh, 0, 2, 2, "", "", 100);
    tab.SetCellStyle("top");
    tab.NewCell();

    string sel = UNDERL, unsel = NOUNDERL;

    xoh << BIGGER<< BOLD << "Clock Source: " << NOBOLD << NOBIGGER << endl;

    string dums = INAME(boardLockingProxy()->getSelectedClockSource());
    ClockSourceList.SetDefault(dums);
    ClockSourceList.Write(xoh);

    xoh
        << " (" << MONO << dums << NOMONO << ")" << "<br>" << endl
        << BIGGER << "Reset QPLL at configure" << NOBIGGER
        << SMALLER << " (if external clock)" << NOSMALLER
        << BIGGER << ":<br> " << NOBIGGER << endl;

    if (boardLockingProxy()->ConfiguringResetsQPLL() && boardLockingProxy()->getSelectedClockSource() != INTERNAL)
    {
      xoh
          << cgicc::input().set("type", "radio").set("name", "QPLLReset").set("value", "YES").set("checked", "checked")
          << sel << "Reset" << unsel
          << cgicc::input().set("type", "radio").set("name", "QPLLReset").set("value", "NO")
          << "No Reset" << endl;
    }
    else
    {
      xoh
          << cgicc::input().set("type", "radio").set("name", "QPLLReset").set("value", "YES")
          << "Reset"
          << cgicc::input().set("type", "radio").set("name", "QPLLReset").set("value", "NO").set("checked", "checked")
          << sel << "No Reset" << unsel << endl;
    }

    xoh
        << "<br>" << endl
        << BIGGER<< "QPLLCTRL Bit 4" << NOBIGGER
        << SMALLER<< " (if external clock)" << NOSMALLER
        << BIGGER << ":<br> " << NOBIGGER<< endl;

    if (boardLockingProxy()->getSelectedClockSource() != INTERNAL && boardLockingProxy()->Is_AutoRestartQPLL())
    {
      xoh
          << GREEN<< "AutoRestart" << NOCOL<< ": " << endl
          << cgicc::input()
          .set("type", "radio")
          .set("name", "QPLLAutoRestart")
          .set("value", "ON").set("checked", "checked")
          << sel << "ON (1)" << unsel
          << cgicc::input()
          .set("type", "radio")
          .set("name", "QPLLAutoRestart")
          .set("value", "OFF")
          << "OFF (0)" << endl;
    }
    else
    {
      xoh
          << RED << "AutoRestart" << NOCOL<< ": " << endl
          << cgicc::input()
          .set("type", "radio")
          .set("name", "QPLLAutoRestart")
          .set("value", "ON")
          << "ON (1) "
          << cgicc::input()
          .set("type", "radio")
          .set("name", "QPLLAutoRestart")
          .set("value", "OFF").set("checked", "checked")
          << sel << "OFF (0)" << unsel << endl;
    }
    xoh << "<br>" << endl;

    {
      // QPLL frequency bits.
      bool clockExternal = boardLockingProxy()->getSelectedClockSource() != INTERNAL;

      uint32_t theQPLLFreqBits = boardLockingProxy()->GetQPLLFrequencyBits(clockExternal /* only4LSBs */);
      string strQPLLFreqBits = Long2HexString(theQPLLFreqBits);

      xoh
          << "QPLL Freq. bits: <br>" << MONO << "0x" << NOMONO
          << cgicc::input()
          .set("type", "text")
          .set("name", "freqbits")
          .set("size", "3").set("value", strQPLLFreqBits.c_str())
          << SMALLER << " (6 bits for INTERNAL clock, 4 otherwise)" << NOSMALLER
          << endl;
    }
    xoh << "<br><br>" << BOLD << "BGO Source<br>" << NOBOLD << endl;

    vector<ttc::ExternalInterface> itf = boardLockingProxy()->CheckBGOSource();
    BGOSourceList_other.UnCheckAll();
    if (find(itf.begin(), itf.end(), ttc::CTC) != itf.end())
    {
      BGOSourceList.SetDefault(INAME(ttc::CTC));
    }
    else if (find(itf.begin(), itf.end(), ttc::LTCIN) != itf.end())
    {
      BGOSourceList.SetDefault(INAME(ttc::LTCIN));
    }
    else
    {
      BGOSourceList.SetDefault("other");
      BGOSourceList_other.UnCheckAll();
      for (size_t i = 0; i < itf.size(); ++i)
      {
        BGOSourceList_other.Check(INAME(itf[i]));
      }
    }

    BGOSourceList.Write(xoh);
    HTMLTable tab2(xoh, 0, 2, 0, "");
    tab2.NewCell();
    xoh << "&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp" << endl;
    tab2.NewCell();
    BGOSourceList_other.Write(xoh);
    tab2.Close();

    // Next table cell.
    tab.NewCell();

    xoh << BOLD << "Orbit Source" << NOBOLD << endl;

    OrbitSourceList.SetDefault((dums = INAME(boardLockingProxy()->getSelectedOrbitSource())));
    OrbitSourceList.Write(xoh);
    xoh
        << " (" << MONO<<dums << NOMONO<< ")<br>"
        << "<br>" << endl
        << BOLD << "Trigger Source (";

    if (boardLockingProxy()->IsL1AEnabled())
    {
      xoh << GREEN << "Enabled" << NOCOL;
    }
    else
    {
      xoh << RED << "Disabled" << NOCOL;
    }
    xoh << ")<br>" << NOBOLD << endl;

    itf = boardLockingProxy()->getSelectedTriggerSources();
    TriggerSourceList_other.UnCheckAll();
    if (find(itf.begin(), itf.end(), ttc::CTC) != itf.end())
    {
      TriggerSourceList.SetDefault(INAME(ttc::CTC));
    }
    else if (find(itf.begin(), itf.end(), ttc::LTCIN) != itf.end())
    {
      TriggerSourceList.SetDefault(INAME(ttc::LTCIN));
    }
    else
    {
      TriggerSourceList.SetDefault("other");
      for (size_t i = 0; i < itf.size(); ++i)
      {
        TriggerSourceList_other.Check(INAME(itf[i]));
      }
    }

    TriggerSourceList.Write(xoh);

    tab2.Open();
    tab2.NewCell();
    tab2.NewCell();
    xoh << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" << endl;
    tab2.NewCell();

    TriggerSourceList_other.Write(xoh);

    if (boardLockingProxy()->GetRAMTriggers()->CanSetInternalTriggerFrequency())
    {
      // Trigger Frequency
      xoh << cgicc::label("&nbsp;&nbsp;&nbsp;&nbsp;Freq.: ");
      char freq[20];
      double trigfreq = boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerFrequency();
      if (trigfreq < 0)
      {
        sprintf(freq, "1");
      }
      else if (trigfreq == double(int(trigfreq)))
      {
        sprintf(freq, "%.0f", trigfreq);
      }
      else
      {
        sprintf(freq, "%.2f", trigfreq);
      }
      xoh
          << cgicc::input()
          .set("type", "text")
          .set("name", "TriggerFrequency")
          .set("size", "6").set("value", string(freq)) << " Hz\n" << endl;

      if (boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerRandom())
      {
        xoh
            << cgicc::input()
            .set("type", "checkbox")
            .set("name", "TriggerRandom")
            .set("checked", "checked")
            << GREEN << " Random" << NOCOL << endl;
      }
      else
      {
        xoh
            << cgicc::input()
            .set("type", "checkbox")
            .set("name", "TriggerRandom") << " Random" << endl;
      }
      xoh
          << "<br>&nbsp;&nbsp;&nbsp;&nbsp;"
          << SMALLER
          << "(if Trigger Source = " << MONO << "INTERNAL" << NOMONO << ")"
          << NOSMALLER << endl;
    }
    else
    {
      xoh
          << GREY
          << "&nbsp;&nbsp;&nbsp;&nbsp;Freq.: not applicable.<br>"
          << "&nbsp;&nbsp;&nbsp;&nbsp;(\"" << MONO << "TRIGGER_INTERVAL x" << NOMONO << "\" "
          << "in config. file)"
          << NOCOL << endl;
    }

    tab2.Close();

    // external trigger 0/1 input delay configuration
    {
      vector<unsigned> delayable_ext_trig_inputs = boardLockingProxy()->GetDelayableExternalTrigInputs();

      for (unsigned i = 0; i < delayable_ext_trig_inputs.size(); ++i)
      {
        unsigned input_num = delayable_ext_trig_inputs[i];

        xoh
            << "<br/><br/>"
            << "External trigger input " << input_num << " delay: "
            << "<input "
                "type=\"text\" "
                "name=\"ExtTrig" << input_num << "InputDelay\" "
                "size=\"3\" "
                "value=\"" << boardLockingProxy()->GetExtTrigInputDelay(input_num) << "\" "
            << "/>"
            << " BX (range: 0..255)";
      }
      tab2.CloseRow();
    }

    // End of table.
    tab.Close();

    // End fieldset.
    xoh
        << cgicc::fieldset() << endl
        << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply")
        << cgicc::form() << endl;
  }

  {
    // Trigger rules.
    std::string myurl = "/";
    myurl += getApplicationDescriptor()->getURN();
    myurl += "/TriggerRules";
    xoh << cgicc::form().set("method", "post").set("action", myurl).set("enctype", "multipart/form-data") << endl;
    xoh << cgicc::fieldset() << endl;
    xoh << cgicc::legend("Trigger Rules (apply to all but CTC and LTC inputs!)") << endl;
    for (size_t i = boardLockingProxy()->FirstTriggerRule(); i < boardLockingProxy()->TriggerRuleSize(); ++i)
    {
      if (i > boardLockingProxy()->FirstTriggerRule())
      {
        xoh << "<br> ";
      }
      xoh << "No more than " << i << " triggers in ";
      xoh
          << cgicc::input().set("type", "text").set("name", ("trigrule  " + UnsignedLong2String(i)).c_str()).set(
              "size", "6").set("value", UnsignedLong2String(boardLockingProxy()->GetTriggerRule(i)).c_str()) << endl;
      xoh << " consecutive BXs" << endl;
    }

    xoh
        << cgicc::fieldset() << endl
        << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply")
        << cgicc::form() << endl;
  }

  PrintHTMLFooter(xoh);
}


void ttc::TTCciControl::HTMLPageBGOConfiguration(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageBGOConfiguration");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
  xoh << cgicc::title("TTCci \"" + name_.toString() + "\"") << std::endl;

  if (autoRefresh_)
  {
    xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"5; URL=" << lastPage_ << "\">" << endl;
  }

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  if (CurrentBGOList.GetDefaultInt() != int(currentBGO_))
  {
    std::ostringstream msg;
    msg << "TTCciControl::BGOConfiguration(): " << "CurrentBGOList.GetDefaultInt() != int(CurrentBGO) ("
        << CurrentBGOList.GetDefaultInt() << " != " << int(currentBGO_) << ")";
    LOG4CPLUS_WARN(logger_, msg.str());
    CurrentBGOList.SetDefault(currentBGO_);
  }

  const size_t ich = currentBGO_;

  xoh << "State Machine: ";
  PrintFSMState(xoh);

  xoh << cgicc::p() << cgicc::p() << endl;

  { // BGO Channel Select
    HTMLTable tab(xoh, 0, 0, 1, "", "", 100);
    tab.NewCell();

    std::string select_url = "/";
    select_url += getApplicationDescriptor()->getURN();
    select_url += "/BGOSelectCommand";

    xoh << cgicc::form().set("method", "post").set("action", select_url).set("enctype", "multipart/form-data")
        << std::endl;

    // Begin of fieldset.
    xoh << cgicc::fieldset() << std::endl;

    ostringstream selectedBGOStr;
    selectedBGOStr
        << "Selected BGO channel: "
        << BROWN << "<b>" << CurrentBGOList.GetDefaultTitle() << "</b>" << NOCOL;

    xoh << cgicc::legend(selectedBGOStr.str()) << std::endl;

    xoh << "Switch to BGO Channel: " << endl;
    CurrentBGOList.Write(xoh);

    xoh << "<p>" << endl;
    // End of fieldset.
    xoh << cgicc::fieldset() << std::endl;

    xoh << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Show channel");

    xoh << cgicc::form() << std::endl;
    tab.NewCell();
    xoh << "VME BGO-" << (ich > 9 ? "" : "0") << ich << ":" << endl;

    { // VME BGO Button
      std::string cmd_url = "/";
      cmd_url += getApplicationDescriptor()->getURN();
      cmd_url += "/Command";
      xoh << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data")
          << std::endl;

      if (!boardLockingProxy()->isBGOSourceSelected(ttc::VME))
      {
        xoh
            << cgicc::input().set("type", "submit").set("name", "Command").set("value", "Send VME-BGO").set("disabled",
                "true");
      }
      else
      {
        xoh << cgicc::input().set("type", "submit").set("name", "Command").set("value", "Send VME-BGO");
      }
      xoh << cgicc::form();
    }
    tab.Close();
  }

  std::string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/BGOConfigCommand";

  xoh << cgicc::form().set("method", "post").set("action", command_url).set("enctype", "multipart/form-data")
      << std::endl;
  xoh << cgicc::fieldset() << std::endl; // Begin Fieldset
  xoh << cgicc::legend("Configuration of BGO " + CurrentBGOList.GetDefaultTitle()) << std::endl;

  // The table.
  HTMLTable tab(xoh, 0, 0, 1, "", "", 100);
  tab.NewCell("top");
  // The left big cell for the main BGO config of that channel.
  const ttc::BChannel* bch = boardLockingProxy()->GetBChannel(ich);
  string sel = string(UNDERL) + string(GREEN), unsel = string(NOCOL) + string(NOUNDERL);
  xoh << BOLD<<BIGGER<<"Data Properties<br>"<<NOBIGGER<<NOBOLD<<endl;
  xoh << "Command Length:<br>" << endl;

  if (bch->IsSingleCommand())
  {
    xoh
        << cgicc::input().set("type", "radio").set("name", "commandlength").set("value", "single").set("checked",
            "checked");
    xoh << sel << "SINGLE" << unsel << " ";
  }
  else
  {
    xoh << cgicc::input().set("type", "radio").set("name", "commandlength").set("value", "single");
    xoh << "SINGLE ";
  }

  xoh << "<br>" << endl;

  if (bch->IsDoubleCommand())
  {
    xoh
        << cgicc::input().set("type", "radio").set("name", "commandlength").set("value", "double").set("checked",
            "checked");
    xoh << sel << "DOUBLE" << unsel << " ";
  }
  else
  {
    xoh << cgicc::input().set("type", "radio").set("name", "commandlength").set("value", "double");
    xoh << "DOUBLE ";
  }

  xoh << "<br>" << endl;

  if (bch->IsBlockCommand())
  {
    xoh
        << cgicc::input().set("type", "radio").set("name", "commandlength").set("value", "block").set("checked",
            "checked");
    xoh << sel << "BLOCK" << unsel;
  }
  else
  {
    xoh << cgicc::input().set("type", "radio").set("name", "commandlength").set("value", "block");
    xoh << "BLOCK";
  }
  xoh << endl;
  xoh << "<br>" << endl;

  if (bch->IsRepetitive())
  {
    xoh << GREEN<<BOLD;
    xoh << cgicc::input().set("type", "checkbox")
    .set("name", "repetitive").set("checked", "checked") << std::endl;
  }
  else
  {
    xoh << cgicc::input().set("type", "checkbox")
    .set("name", "repetitive") << std::endl;
  }
  xoh << cgicc::label("Repetitive");
  if (bch->IsRepetitive())
    xoh << NOBOLD<<NOCOL;
  xoh << "<br>" << endl;

  xoh << "Delay " << ITALIC<<"t1" << NOITALIC<<": ";
  xoh
      << cgicc::input().set("type", "text").set("name", "delay1").set("size", "6").set("value",
          UnsignedLong2String(bch->GetDelayTime1()).c_str()) << " &times; BX" << endl;
  xoh << "<br>Delay " << ITALIC<<"t2" << NOITALIC<<": ";
  xoh
      << cgicc::input().set("type", "text").set("name", "delay2").set("size", "6").set("value",
          UnsignedLong2String(bch->GetDelayTime2()).c_str()) << " &times; BX" << endl;

  xoh << "<br>Prescale: ";
  xoh
      << cgicc::input().set("type", "text").set("name", "prescale").set("size", "6").set("value",
          UnsignedLong2String(bch->GetPrescale()).c_str()) << endl;
  xoh << SMALLER<<" [0,65535]" << NOSMALLER<<"<br>Postscale: ";
  xoh
      << cgicc::input().set("type", "text").set("name", "postscale").set("size", "6").set("value",
          UnsignedLong2String(bch->GetPostscale()).c_str()) << SMALLER<<" [0,65535]" << NOSMALLER<<"<br>Init.offset: "
      << endl;
  xoh
      << cgicc::input().set("type", "text").set("name", "offset").set("size", "6").set("value",
          UnsignedLong2String(bch->GetInitialPrescale()).c_str()) << " requests" << endl;

  //----------------------------------------

  // Begin inner right table.
  tab.NewCell();
  xoh << BOLD<< BIGGER << CurrentBGOList.GetDefaultTitle() <<dec
  << " BGO Commands:<br>" << NOBIGGER << NOBOLD << endl;

  bool has_individual_b_postscale_numbers = boardLockingProxy()->HasIndividualBPostScaleNumbers();

  HTMLTable tab2(xoh, 1, 2, 2, "", "", 100);
  tab2.NewRow();
  tab2.NewCell("top");
  xoh << "No." << endl;
  tab2.NewCell();
  xoh << "Data" << endl;
  tab2.NewCell();
  xoh << "Type" << endl;

  if (has_individual_b_postscale_numbers)
  {
    tab2.NewCell();
    xoh << "Postscale" << endl;
  }

  tab2.NewCell();
  xoh << MONO<<"CH" << (ich > 9 ? "" : "0") << ich << "D/P" << NOMONO<<endl;
  tab2.NewCell();
  xoh << "Remove" << endl;
  char dummy[30];
  vector<string> values, titles;
  titles.push_back("New");
  values.push_back("New");

  bool is_double_cmd = bch->GetDataLength() == DOUBLE;

  for (size_t n = 0; n < bch->NWords(); ++n)
  {
    tab2.NewRow();

    const bool LAST = bch->IsLastWord(n);
    if (!LAST)
    {
      sprintf(dummy, "%zu)", n);
      titles.push_back(string(dummy));
      sprintf(dummy, "%zu", n);
      values.push_back(string(dummy));
    }

    string bgdcol = (LAST ? "grey" : (bch->IsACommand(n) ? "orange" : "yellow"));
    xoh << BIGGER<<endl;

    //--------------------
    // Print the row number.
    //--------------------
    tab2.NewCell(bgdcol);
    if (0 && LAST)
    {
      xoh << "Last";
    }
    else
    {
      xoh << dec << n << ")";
      if (is_double_cmd)
      {
        xoh << "[" << n / 2 << ' ';
        if ((n % 2) == 0)
        {
          xoh << "first";
        }
        else
        {
          xoh << "second";
        }
        xoh << "]";
      }
    }
    xoh << endl;

    //--------------------
    // Print B frame content.
    //--------------------
    tab2.NewCell(bgdcol);
    if (LAST)
    {
      xoh << ">>LastWord<<";
    }
    else
    {
      xoh << MONO<<"0x" << hex << bch->DataWord_D(n) << dec << NOMONO;
    }
    xoh << endl;
    tab2.NewCell(bgdcol);
    if (bch->IsACommand(n))
    {
      xoh << "A-Cmnd./L1A";
    }
    else
    {
      xoh << (bch->IsShortWord(n) ? "Short" : "Long");
    }
    xoh << endl;

    //--------------------
    // Individual postscale value.
    //--------------------

    if (has_individual_b_postscale_numbers)
    {
      tab2.NewCell(bgdcol);

      if (!LAST)
      {
        string value = boost::lexical_cast < string > (bch->GetIndividualPostScaleValue(n));

        xoh
            << cgicc::input().set("type", "text").set("name", str(boost::format("indiv_postscale%d") % n)).set("size",
                "8").set("value", value) << endl;
        ;
      }
    }

    //--------------------
    // Raw D/P ram values.
    //--------------------
    tab2.NewCell(bgdcol);
    xoh << SMALLER<< MONO << BLUE << "0x"<<hex<<GetBGOWord(ich,n,true)<<NOCOL
    <<dec<<" / "<<BLUE
    <<"0x"<<hex<<GetBGOWord(ich,n,false)<<NOCOL<<dec<<NOMONO<<NOSMALLER
    <<endl;
    xoh << "<br>";

    //--------------------
    // Delete checkbox.
    //--------------------
    tab2.NewCell(bgdcol);
    sprintf(dummy, "delete%zu", n);
    if (!LAST)
    {
      xoh << cgicc::input().set("type", "checkbox").set("name", dummy) << endl;
    }
    xoh << NOBIGGER << endl;
  } // loop over all words of this BGO command

  //--------------------
  // Last row (add new word / modify existing word).
  //--------------------
  tab2.NewRow();

  tab2.NewCell("lightgreen");
  HTMLFieldElement DataList(HTMLFieldElement::DROPDOWNMENU, 0, "datawordselect", values, titles);
  DataList.SetDefault(values[0]);
  DataList.Write(xoh);

  tab2.NewCell();
  xoh << MONO<<"0x" << MONO;
  xoh << cgicc::input().set("type", "text").set("name", "newdata").set("size", "8").set("value", "") << endl;

  tab2.NewCell();
  DataTypeSelector.Write(xoh);

  if (has_individual_b_postscale_numbers)
  {
    tab2.NewCell();
    xoh << cgicc::input().set("type", "text").set("name", "new_indiv_postscale").set("size", "8").set("value", "")
        << endl;
  }

  tab2.NewCell();
  tab2.NewCell();
  xoh << "append" << endl;
  tab2.Close();
  xoh << "<br>" << endl;
  xoh << MONO<<"CHIHB #" << dec << ich << " = " << BLUE<<"0x" << hex
      << boardLockingProxy()->Read(TTCciAdd::CHIHB, ich) << dec << NOCOL<<NOMONO
      << MONO<<" CHPRESC #"<<dec<<ich<<" = "
      <<BLUE<<"0x"<<hex<<boardLockingProxy()->Read(TTCciAdd::CHPRESC,ich)
      <<dec<<NOCOL<<NOMONO
      <<dec<<"<br>"<<endl;
  // End inner right table
  tab.Close();

  // End fieldset.
  xoh << cgicc::fieldset() << std::endl;

  xoh << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply");

  xoh << cgicc::form() << std::endl;

  { // RST & RMC
    std::string cmd_url = "/";
    cmd_url += getApplicationDescriptor()->getURN();
    cmd_url += "/Command";

    const uint32_t rmc = boardLockingProxy()->Read(TTCciAdd::CHRMC, currentBGO_);

    bool has_cancelled_data = boardLockingProxy()->HasCancelledBData(rmc);
    bool is_ram_buffer_at_end = boardLockingProxy()->IsRamBufferAtEnd(rmc);
    uint32_t bgo_request_counter = boardLockingProxy()->BGORequestCounter(rmc);
    uint32_t cancelcntr = boardLockingProxy()->BGOCancelledCounter(rmc);

    HTMLTable tab2(xoh, 0, 2, 1, "", "", 100);
    tab2.NewCell("top");
    xoh << BOLD<< UNDERL << "Counters for BGO Channel " << dec
    << currentBGO_
    << NOUNDERL << NOBOLD << "<br>" << endl;
    xoh << "Last cancelled data: " << endl;

    if (has_cancelled_data)
    {
      xoh << RED <<"yes (= error)" << NOCOL;
    }
    else
    {
      xoh << "no (= o.k.)";
    }
    xoh << "<br>" << endl;
    xoh
        << "RAM: Buffer "
        << (is_ram_buffer_at_end ? string(UNDERL) + "reached" + string(NOUNDERL) :
                                   string(UNDERL) + "not" + string(NOUNDERL) + " at") << " end<br>" << endl;
    xoh << "Request Counter: " << dec << bgo_request_counter;
    xoh << "<br>" << endl;

    xoh << "Cancel Counter: " << dec;
    if (cancelcntr > 0)
      xoh << RED<< BOLD << cancelcntr << NOBOLD << NOCOL;
      else
      xoh << cancelcntr;
    xoh << endl;
    tab2.NewCell();

    // Read BGO config from TTCci.
    xoh << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data")
        << std::endl;
    xoh
        << cgicc::input().set("type", "submit").set("name", "Command").set("value",
            "Read BGO configuration from TTCci");

    xoh << cgicc::form();
    tab2.Close();
  }

  PrintHTMLFooter(xoh);
}


void ttc::TTCciControl::HTMLPageSequences(xgi::Input* /*in*/, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageSequences");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
  xoh << cgicc::title("TTCci '" + name_.toString() + "'") << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  xoh << "Finite State Machine: ";
  PrintFSMState(xoh);
  xoh << cgicc::p() << cgicc::p() << endl;

  string SeqTitle = SequenceSelector.GetDefaultTitle();
  const vector<string> newseqvalues = boardLockingProxy()->GetSequenceNames();
  SequenceSelector.Set(HTMLFieldElement::DROPDOWNMENU, 0, "sequenceselect", newseqvalues, newseqvalues);
  if (!SequenceSelector.HasValue(SeqTitle))
    SeqTitle = newseqvalues.size() ? newseqvalues[0] : "";
  SequenceSelector.SetDefault(SeqTitle);

  xoh << BIGGER<<BOLD << "TTCci Sequence '"
  << BLUE<<SeqTitle<< NOCOL << "'" << NOBOLD << NOBIGGER
  << "<br>" << endl;
  {

    // The First Table with 2 cells:
    HTMLTable tab(xoh, 0, 0, 0, "", "", 100);
    tab.NewCell();

    // Select Sequence to be displayed:
    std::string select_url = "/";
    select_url += getApplicationDescriptor()->getURN();
    select_url += "/SequenceSelectCommand";
    xoh << cgicc::form().set("method", "post").set("action", select_url).set("enctype", "multipart/form-data")
        << endl;
    xoh << cgicc::fieldset() << endl; // Begin Fieldset
    xoh << cgicc::legend(SeqTitle + " Sequence") << endl;
    xoh << "Select sequence: " << endl;
    SequenceSelector.Write(xoh);
    vector<string> seqchce, seqchce_tit;
    seqchce.push_back("display");
    seqchce_tit.push_back("display");
    seqchce.push_back("remove");
    seqchce_tit.push_back("remove");
    seqchce.push_back("add");
    seqchce_tit.push_back("add new:");

    HTMLFieldElement seqchoice(
            HTMLFieldElement::RADIOBUTTON,
            0,
            "seqchoice",
            seqchce,
            seqchce_tit);

    seqchoice.Write(xoh);

    xoh
        << " "
        << cgicc::input()
        .set("type", "text")
        .set("name", "newsequence")
        .set("size", "10")
        .set("value", "") << endl
        << "<p>" << endl
        << cgicc::fieldset() << endl
        << cgicc::input()
        .set("type", "submit")
        .set("name", "submit")
        .set("value", "Select")
        << cgicc::form() << endl;

    tab.NewCell();
    {
      // Button for executing the currently selected sequence.
      std::string cmd_url = "/";
      cmd_url += getApplicationDescriptor()->getURN();
      cmd_url += "/Command";

      xoh
          << cgicc::form()
          .set("method", "get")
          .set("action", cmd_url)
          .set("enctype", "multipart/form-data") << endl
          << cgicc::input()
          .set("type", "submit")
          .set("name", "Command")
          .set("value", ("Execute " + SequenceSelector.GetDefaultTitle() + " Sequence").c_str())
          << cgicc::form();
    }
    tab.Close();
  }

  Sequence* myseq = boardLockingProxy()->GetSequence(SeqTitle);

  { // Edit current sequence
    std::string select_url = "/";
    select_url += getApplicationDescriptor()->getURN();
    select_url += "/SequenceEditCommand";

    xoh
        << cgicc::form()
        .set("method", "post")
        .set("action", select_url)
        .set("enctype", "multipart/form-data") << endl
        << cgicc::fieldset() << endl
        << cgicc::legend("Edit " + SeqTitle + " Sequence") << endl;

    // The Edit Table with 2 cells:
    HTMLTable tab(xoh, 1, 1, 0, "", "", 100);
    tab.NewCell();
    // Show the currecnt content of this sequence as list of radio buttons:
    vector<string> seqsel = myseq->Get();
    seqsel.push_back("< Append >");
    HTMLFieldElement sequence(HTMLFieldElement::RADIOBUTTON, 0, "sequenceline", seqsel, seqsel, "vertical green");
    sequence.SetDefault(seqsel[seqsel.size() - 1]);

    if (seqsel.size() <= 1)
    {
      xoh << "empty sequence.<br>" << endl;
    }

    sequence.Write(xoh);

    tab.NewCell(); // 2nd cell

    xoh << "Edit action:<br>" << endl;
    vector<string> edit, edit_tit;
    edit.push_back("new");
    edit_tit.push_back("New line (insert above)");
    edit.push_back("modify");
    edit_tit.push_back("Modify line");
    edit.push_back("delete");
    edit_tit.push_back("Delete line");
    HTMLFieldElement edittab(HTMLFieldElement::RADIOBUTTON, 0, "edit", edit, edit_tit, "vertical");
    edittab.SetDefault(edit[0]);

    edittab.Write(xoh);
    xoh << endl;
    xoh << "Command: " << endl;

    vector<string> edit2, edit2_tit;
    edit2.push_back("Choose");
    edit2_tit.push_back("&lt; Choose &gt;");
    edit2.push_back("EnableL1A");
    edit2_tit.push_back("EnableL1A");
    edit2.push_back("DisableL1A");
    edit2_tit.push_back("DisableL1A");
    edit2.push_back("Sleep");
    edit2_tit.push_back("Sleep &lt;value&gt; seconds");
    edit2.push_back("mSleep");
    edit2_tit.push_back("mSleep &lt;value&gt; millisec.");
    edit2.push_back("uSleep");
    edit2_tit.push_back("uSleep &lt;value&gt; microsec.");
    edit2.push_back("BGO");
    edit2_tit.push_back("BGO &lt;value&gt; (&lt;value&gt;=Channel)");
    edit2.push_back("ResetCounters");
    edit2_tit.push_back("ResetCounters");
    edit2.push_back("ResetStatus");
    edit2_tit.push_back("ResetStatus");
    edit2.push_back("BGOSource LTC");
    edit2_tit.push_back("Set BGO-source to LTC");
    edit2.push_back("BGOSource CTC");
    edit2_tit.push_back("Set BGO-source to CTC");
    edit2.push_back("BGOSource VME");
    edit2_tit.push_back("Set BGO-source to VME (i.e. OFF)");
    edit2.push_back("BGOSource CYCLIC");
    edit2_tit.push_back("Set BGO-source to CYCLIC");
    edit2.push_back("Periodic On");
    edit2_tit.push_back("Periodic On");
    edit2.push_back("Periodic Off");
    edit2_tit.push_back("Periodic Off");
    edit2.push_back("Periodic");
    edit2_tit.push_back("Periodic interval &lt;value&gt; [sec]");
    edit2.push_back("ResumeCyclicAtStart yes");
    edit2_tit.push_back("ResumeCyclicAtStart yes");
    edit2.push_back("ResumeCyclicAtStart no");
    edit2_tit.push_back("ResumeCyclicAtStart no");
    edit2.push_back("SendShortBDATA");
    edit2_tit.push_back("SendShortBDATA &lt;value&gt;");
    edit2.push_back("SendLongBDATA");
    edit2_tit.push_back("SendLongBDATA &lt;value&gt;");
    edit2.push_back("SendBST");
    edit2_tit.push_back("SendBST");
    HTMLFieldElement edittab2(HTMLFieldElement::DROPDOWNMENU, 0, "edit2", edit2, edit2_tit);
    edittab2.SetDefault(edit2[0]);

    edittab2.Write(xoh);

    xoh
        << endl << " &lt;value&gt; = "
        << cgicc::input().set("type", "text").set("name", "x").set("size", "4").set("value", "")
        << "(hex values must start with 0x)"
        << "<br>" << endl;

    edit2.clear();
    edit2_tit.clear();
    edit2.push_back("Choose");
    edit2_tit.push_back("< Choose >");

    for (size_t k = 0; k < boardLockingProxy()->NChannels(); ++k)
    {
      string val = "BGO ";
      string dum = boardLockingProxy()->GetBChannel(k)->GetName();
      if (dum[0] == 'C' && dum[1] == 'h' && dum[2] == 'a')
      {
        char si[3];
        sprintf(si, " %zu", k);
        val += string(si);
      }
      else
      {
        val += dum;
      }
      string tit = val;
      edit2.push_back(val);
      edit2_tit.push_back(tit);
    }

    xoh << "&nbsp;&nbsp; or BGO: ";

    HTMLFieldElement edittab3(HTMLFieldElement::DROPDOWNMENU, 0, "edit3", edit2, edit2_tit);
    edittab3.SetDefault(edit2[0]);
    edittab3.Write(xoh);

    tab.Close();

    xoh
        << "<p>" << endl
        << cgicc::fieldset() << endl
        << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply")
        << cgicc::form() << endl;
  }

  PrintHTMLFooter(xoh);
}


void ttc::TTCciControl::HTMLPageCyclicGenerators(xgi::Input* /*in*/, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCyclicGenerators");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
  xoh << cgicc::title("TTCci '" + name_.toString() + "'") << endl;

  if (autoRefresh_)
  {
    xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"5; URL=" << lastPage_ << "\">" << endl;
  }

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  { // Start & Stop Buttons
    {
      std::string cmd_url = "/";
      cmd_url += getApplicationDescriptor()->getURN();
      cmd_url += "/Command";
      HTMLTable buttontab(xoh, 0, 2, 2);
      buttontab.NewCell();

      // VME BGO Start Button:
      xoh << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data") << endl;

      if (!boardLockingProxy()->isBGOSourceSelected(ttc::VME))
      {
        xoh
            << cgicc::input()
            .set("type", "submit")
            .set("name", "Command")
            .set("value", "Send BGO-Start")
            .set("disabled", "true") << endl;
      }
      else
      {
        xoh
            << cgicc::input()
            .set("type", "submit")
            .set("name", "Command")
            .set("value", "Send BGO-Start") << endl;
      }
      xoh << cgicc::form();

      buttontab.NewCell();

      // Stop Button:
      xoh
          << cgicc::form()
          .set("method", "get")
          .set("action", cmd_url)
          .set("enctype", "multipart/form-data") << endl;

      if (!boardLockingProxy()->isBGOSourceSelected(ttc::VME))
      {
        xoh
            << cgicc::input()
            .set("type", "submit")
            .set("name", "Command")
            .set("value", "Send BGO-Stop")
            .set("disabled", "true") << endl
            << "(" << INAME(VME)<< " is NOT selected as BGO source)" << endl;
      }
      else
      {
        xoh
            << cgicc::input()
            .set("type", "submit")
            .set("name", "Command")
            .set("value", "Send BGO-Stop") << endl;
      }
      xoh << cgicc::form();

      buttontab.Close();
    }
  }

  bool triggersource = false, bgosource = false;
  {
    std::vector<ExternalInterface> myitf = boardLockingProxy()->getSelectedTriggerSources();

    if (find(myitf.begin(), myitf.end(), CYCLIC) != myitf.end())
      triggersource = true;

    myitf = boardLockingProxy()->CheckBGOSource();

    if (find(myitf.begin(), myitf.end(), CYCLIC) != myitf.end())
      bgosource = true;
  }

  {
    xoh << "Finite State Machine: ";
    PrintFSMState(xoh);

    xoh
        << " - Source Selection: " << BOLD
        << "BGO=" << (bgosource ? GREEN : RED)
        << (bgosource ? "enabled" : "disabled") << NOCOL
        << " Trigger=" << (triggersource ? GREEN : RED)
        << (triggersource ? "enabled" : "disabled") << NOCOL
        << NOBOLD << endl
        << cgicc::p() << cgicc::p() << endl;
  }

  std::string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/CyclicConfigCommand";

  xoh
      << cgicc::form()
      .set("method", "post")
      .set("action", command_url)
      .set("enctype", "multipart/form-data") << endl
      << cgicc::fieldset() << endl
      << cgicc::legend("Cyclic Generator Configuration") << endl;

  { // The Table
    HTMLTable maintab(xoh, 0);
    // First row:
    maintab.NewRow();
    maintab.NewCell();
    xoh << "Name";

    maintab.NewCell();
    xoh << "Configuration";

    maintab.CloseRow();

    vector<string> enable_val, enable_tit;
    enable_val.push_back("enable");
    enable_tit.push_back("enable");
    enable_val.push_back("disable");
    enable_tit.push_back("disable");

    for (size_t ik = 0; ik < (boardLockingProxy()->NCyclicBGO() + boardLockingProxy()->NCyclicTrigger()); ++ik)
    {
      char genc[23];
      sprintf(genc, "%zu__", ik);
      const string sgen = string(genc);
      bool trigger = (ik >= boardLockingProxy()->NCyclicBGO());
      const size_t i = (!trigger ? ik : ik - boardLockingProxy()->NCyclicBGO());
      const CyclicTriggerOrBGO *cycl = boardLockingProxy()->GetCyclic(trigger, i);

      // New row
      // First cell:
      maintab.NewRow();
      maintab.NewCell();

      const bool enabled = cycl->IsEnabled();
      xoh << cycl->GetName() << "<br>" << endl;

      HTMLFieldElement enable(
          HTMLFieldElement::RADIOBUTTON,
          0,
          sgen + "enable",
          enable_val,
          enable_tit,
          "vertical bold blue");

      enable.SetDefault((enabled ? 0 : 1));
      enable.Write(xoh);

      // Second Cell:
      maintab.NewCell();
      { // Configuration Cell
        string bgdcol = "";

        if (enabled)
        {
          if ((trigger && triggersource) || (!trigger && bgosource))
            bgdcol = "lightgreen";
          else
            bgdcol = "green";
        }
        else if (!enabled)
        {
          if ((trigger && triggersource) || (!trigger && bgosource))
            bgdcol = "lightgrey";
          else
            bgdcol = "grey";
        }

        HTMLTable inner(xoh, 1, 3, 0, bgdcol);

        // first config row:
        inner.NewRow();
        // StartBX
        inner.NewCell();

        xoh
            << "StartBX: " << endl
            << cgicc::input()
            .set("type", "text")
            .set("name", sgen + "startbx")
            .set("size", "3")
            .set("value", Long2String(cycl->GetStartBX()).c_str()) << endl;

        // Prescale
        inner.NewCell();

        xoh
            << "Prescale: " << endl
            << cgicc::input()
            .set("type", "text")
            .set("name", sgen + "prescale")
            .set("size", "3")
            .set("value", Long2String(cycl->GetPrescale()).c_str()) << endl;

        // InitPrescale
        inner.NewCell();

        xoh
            << "InitWait [Orb]: " << endl
            << cgicc::input()
            .set("type", "text")
            .set("name", sgen + "initprescale")
            .set("size", "3")
            .set("value", Long2String(cycl->GetInitialPrescale()).c_str()) << endl;

        // B Channels:
        inner.NewCell();

        if (!trigger)
        {
          HTMLFieldElement BGOList = CurrentBGOList;
          BGOList.SetName(sgen + "channelno");
          BGOList.SetDefault(cycl->GetBChannel());
          BGOList.Write(xoh);
        }

        // Next config line.
        inner.NewRow();

        // Postscale.
        inner.NewCell();

        xoh
            << cgicc::input()
            .set("type", "text")
            .set("name", sgen + "postscale")
            .set("size", "3")
            .set("value", Long2String(cycl->GetPostscale()).c_str()) << endl
            << " times" << endl;

        // Pause.
        inner.NewCell();

        xoh
            << "Pause [Orb]: " << endl
            << cgicc::input()
            .set("type", "text")
            .set("name", sgen + "pause")
            .set("size", "3")
            .set("value", Long2String(cycl->GetPause()).c_str()) << endl;

        // IsRepetitive.
        inner.NewCell();

        if (cycl->IsRepetitive())
        {
          xoh
              << cgicc::input()
              .set("type", "checkbox")
              .set("name", sgen + "repetitive")
              .set("checked", "checked");
        }
        else
        {
          xoh
              << cgicc::input()
              .set("type", "checkbox")
              .set("name", sgen + "repetitive");
        }
        xoh << "repetitive" << endl;

        // IsPermanent.
        inner.NewCell();

        if (cycl->IsPermanent())
        {
          xoh
              << cgicc::input()
              .set("type", "checkbox")
              .set("name", sgen + "permanent")
              .set("checked", "checked");
        }
        else
        {
          xoh << cgicc::input().set("type", "checkbox").set("name", sgen + "permanent");
        }
        xoh << "permanent" << endl;

        inner.Close();
      }
    }
    // End of main table.
    maintab.Close();
  }

  // Begin fieldset.
  xoh
      << cgicc::fieldset() << endl
      << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply")
      << cgicc::form() << endl;

  PrintHTMLFooter(xoh);
}


void ttc::TTCciControl::HTMLPageSummary(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageSummary");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
  xoh << cgicc::title("TTCci '" + name_.toString() + "'") << endl;

  if (autoRefresh_)
  {
    xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"5; URL=" << lastPage_ << "\">" << endl;
  }

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  // Trying to fix up the table a bit.
  {
    const string emph = DARKBLUE, deflt = BLACK, alert = RED;
    // Status summary.
    const string url = "/" + getApplicationDescriptor()->getURN() + "/";

    ReadTTCciCounters();

    HTMLTable stab(xoh, 1, 2, 0, "", "", 100);
    stab.NewCell("top", 1, 1);

    xoh << "Finite State Machine: ";
    PrintFSMState(xoh);

    stab.NewCell("top", 1, 1);
    xoh
        << "L1A: " << BOLD
        << (boardLockingProxy()->IsL1AEnabled() ? GREEN : RED)
        << (boardLockingProxy()->IsL1AEnabled() ? "Enabled" : "Disabled")
        << NOCOL << NOBOLD << endl;

    stab.NewCell("top", 1, 1);
    xoh << "Laser: ";

    if (boardLockingProxy()->IsLaserOn())
    {
      xoh << GREEN << "on" << NOCOL;
    }
    else
    {
      xoh << RED << "off" << NOCOL;
    }
    stab.NewCell("top", 1, 1);
    stab.NewCell("top", 1, 1);

    { // Clock locked?
      string defaultcol = GREEN, alertcol = RED;
      bool lock = boardLockingProxy()->ClockLocked();
      string col = (lock ? defaultcol : alertcol);
      xoh << "Clock: " << col << (lock ? "locked" : "not locked") << NOCOL<< endl;
    }

    { // Clock inverted?
      bool inv = boardLockingProxy()->ClockInverted();
      xoh << (inv ? ", inverted" : ", not inverted") << endl;
    }

    // Status & Counters
    stab.NewRow();
    stab.NewCell("top lightyellow");
    xoh << UNDERL<< "Counters / Status" << NOUNDERL<< endl;

    stab.NewCell();
    xoh
        << UNDERL << "Counters" << NOUNDERL << endl
        << "<br>" << "Evt-ID: " << EventCounter_ << endl
        << "<br>" << "Orbit: " << OrbitCounter_ << endl
        << "<br>" << "BGO-Req.Cntr: " << StrobeCounter_ << endl;

    stab.NewCell();
    {
      const double dt = difftime(tnow_, tprevious_);
      xoh << UNDERL<< "L1A rate (after " << dt << " s):" << NOUNDERL<< "<br>" << endl;

      double diff = double(EventCounter_ - previousEventCounter_);
      double odiff = double(OrbitCounter_ - previousOrbitCounter_);
      xoh << int(diff) << " L1As in " << odiff << " orbs.<br>" << endl;

      if (odiff > 0.0 && dt > 0.0)
      {
        xoh
            << RED << (diff / (odiff * 3564 * 25.0e-9)) << " Hz" << NOCOL
            << "<br>" << endl;
      }
      else
      {
        xoh << RED << "nan" << " Hz" << NOCOL<< "<br>" << endl;
      }

      if (odiff > 0.)
      {
        xoh << (diff / odiff) << " L1As/orbit <br>" << endl;
      }
      else
      {
        xoh << "nan" << " L1As/orbit <br>" << endl;
      }

      if (diff > 0.)
      {
        xoh << (odiff / diff) << " orbits/L1A <br>" << endl;
      }
      else
      {
        xoh << "nan" << " orbits/L1A" << endl;
      }

      stab.NewCell();
      diff = double(StrobeCounter_ - previousStrobeCounter_);

      xoh
          << UNDERL << "BGO-req. rate:" << NOUNDERL << "<br>" << endl
          << int(diff) << " BGOs<br>" << endl;

      if (dt > 0.)
      {
        xoh << RED <<(diff / (odiff * 3564 * 25.0e-9)) << " Hz" << NOCOL<< "<br>" << endl;
      }
      else
      {
        xoh << RED << "nan" << " Hz" << NOCOL<< "<br>" << endl;
      }

      if (odiff > 0.)
      {
        xoh << (diff / odiff) << " BGOs/orbit <br>" << endl;
      }
      else
      {
        xoh << "nan" << " BGOs/orbit <br>" << endl;
      }

      if (diff > 0.)
      {
        xoh << (odiff / diff) << " orbits/BGO <br>" << endl;
      }
      else
      {
        xoh << "nan" << " orbits/BGO" << endl;
      }
    }

    stab.NewCell();
    {
      xoh
          << UNDERL << "Board status:" << NOUNDERL
          << MONO << BLUE << hex << " 0x" << BoardStatus_ << NOCOL << NOMONO << dec << endl
          << SMALLER << "&nbsp;(...) = latched" << NOSMALLER << endl;

      string defaultcol = GREEN, alertcol = RED;

      { // BDataCancelled:
        bool isok = !boardLockingProxy()->IsBDataCancelled();
        bool isok2 = !boardLockingProxy()->IsBDataCancelled_Latched();

        string col = (isok?defaultcol:alertcol);
        xoh << "<br>" << "BData Cancelled? " << col << (isok?"no":"yes") << NOCOL;

        col = (isok2?defaultcol:alertcol);
        xoh << " (" << col << (isok2?"no":"yes") << NOCOL << ")" << endl;
      }

      { // DataClkError:
        bool isok = !boardLockingProxy()->IsDataClkError();
        bool isok2 = !boardLockingProxy()->IsDataClkError_Latched();

        string col = (isok?defaultcol:alertcol);
        xoh << "<br>" << "Dat/Clk sync error? " << col << (isok?"no":"yes")<< NOCOL;

        col = (isok2?defaultcol:alertcol);
        xoh << " (" <<col<<(isok2?"no":"yes")<< NOCOL<< ")" << endl;
      }

      { // IsClkSingleEvtUpset:
        bool isok = !boardLockingProxy()->IsClkSingleEvtUpset();
        bool isok2 = !boardLockingProxy()->IsClkSingleEvtUpset_Latched();

        string col = (isok?defaultcol:alertcol);
        xoh << "<br>" << "Clk. single-evt. upset? " << col << (isok?"no":"yes")<< NOCOL;

        col = (isok2?defaultcol:alertcol);
        xoh << " (" << col << (isok2?"no":"yes") << NOCOL << ")" << endl;
      }

      { // MissedL1A:
        bool isok = !boardLockingProxy()->MissedL1A();
        bool isok2 = !boardLockingProxy()->MissedL1A_Latched();

        string col = (isok?defaultcol:alertcol);
        xoh << "<br>" << "L1A missed @ 40MHz? " << col << (isok?"no":"yes")<< NOCOL;

        col = (isok2?defaultcol:alertcol);
        xoh << " (" << col << (isok2?"no":"yes")<< NOCOL << ")" << endl;
      }

      { // DoubleL1Aat40MHz:
        bool isok = !boardLockingProxy()->DoubleL1Aat40MHz();
        bool isok2 = !boardLockingProxy()->DoubleL1Aat40MHz_Latched();

        string col = (isok?defaultcol:alertcol);
        xoh << "<br>" << "Double-L1A @ 40MHz? " <<col << (isok ? "no" : "yes") << NOCOL;

        col = (isok2 ? defaultcol : alertcol);
        xoh << " (" << col << (isok2?"no":"yes") << NOCOL << ")" << endl;
      }
    }
      // Sources:
    stab.NewRow();
    stab.NewCell("top lightblue");

    bool cycltrig = false;

    xoh
        << "<a href=\"" << url << "MainConfiguration\">"
        << "Input Sources" << "</a>" << endl;

    stab.NewCell();
    xoh << BOLD << "Clock:" << NOBOLD << "<br>- " << INAME(boardLockingProxy()->getSelectedClockSource()) << endl;

    stab.NewCell();
    xoh << BOLD << "Orbit:" << NOBOLD << "<br>- " << INAME(boardLockingProxy()->getSelectedOrbitSource()) << endl;

    stab.NewCell();
    xoh << BOLD << "BGO:" << NOBOLD << endl;

    std::vector<ExternalInterface> bsource = boardLockingProxy()->CheckBGOSource();
    for (size_t i = 0; i < bsource.size(); ++i)
    {
      xoh << "<br>- " << INAME(bsource[i]) << endl;
    }

    stab.NewCell();
    xoh << BOLD << "Trigger:" << NOBOLD << endl;

    std::vector<ExternalInterface> tsource = boardLockingProxy()->getSelectedTriggerSources();
    for (size_t i = 0; i < tsource.size(); ++i)
    {
      if (tsource[i] == CYCLIC)
      {
        cycltrig = true;
      }

      xoh << "<br>- " << INAME(tsource[i]) << endl;
      if (boardLockingProxy()->GetRAMTriggers()->CanSetInternalTriggerFrequency() && tsource[i] == ttc::INTERNAL)
      {
        xoh << "(" << dec << boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerFrequency() << " Hz)";
      }
    }
    if (!cycltrig)
    {
      stab.CloseRow();
    }

    // Cyclic triggers:
    if (cycltrig)
    {
      for (size_t i = 0; i < boardLockingProxy()->NCyclicTrigger(); ++i)
      {
        CyclicTriggerOrBGO *cycl = boardLockingProxy()->GetCyclic(true, i);
        if (!cycl->IsEnabled())
          continue;

        stab.NewRow();
        stab.NewCell("lightred");
        xoh
            << "<a href=\"" << url << "CyclicGenerators\">"
            << "Cycl. Trig " << cycl->GetID() << "</a>" << endl;

        stab.NewCell("", 4, 1);
        xoh
            << (cycl->GetStartBX() > 0 ? emph : deflt)
            << "StartBX: " << cycl->GetStartBX() << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->GetPrescale() > 0 ? emph : deflt)
            << "Prescale: " << cycl->GetPrescale() << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->GetInitialPrescale() > 0 ? emph : deflt)
            << "InitWait: " << cycl->GetInitialPrescale() << NOCOL
            << " orbits" << "&nbsp;&nbsp;&nbsp;&nbsp;" << endl
            << "<br>" << endl
            << (cycl->GetPostscale() > 0 ? emph : deflt) << cycl->GetPostscale()
            << " &times;" << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->GetPause() > 0 ? emph : deflt) << "Pause: " << cycl->GetPause()
            << " orbits" << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->IsRepetitive() ? deflt : emph)
            << (cycl->IsRepetitive() ? "(repetitive)" : "(non-repetitive)")
            << "&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->IsPermanent() ? alert : deflt) << (cycl->IsPermanent() ? "(permanent)" : "(non-permanent)")
            << NOCOL << endl;
      }
    }

    // Cyclic BGOs:
    if (cycltrig)
    {
      for (size_t i = 0; i < boardLockingProxy()->NCyclicBGO(); ++i)
      {
        CyclicTriggerOrBGO* cycl = boardLockingProxy()->GetCyclic(false, i);
        if (!cycl->IsEnabled())
        {
          continue;
        }
        stab.NewRow();
        stab.NewCell("lightorange");

        xoh
            << "<a href=\"" << url << "CyclicGenerators\">"
            << "Cycl. BGO " << cycl->GetID() << "</a>" << endl;

        stab.NewCell("", 4, 1);
        xoh
            << (cycl->GetStartBX() > 0 ? emph : deflt) << "StartBX: "
            << cycl->GetStartBX() << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->GetPrescale() > 0 ? emph : deflt) << "Prescale: "
            << cycl->GetPrescale() << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->GetInitialPrescale() > 0 ? emph : deflt)
            << "InitWait: " << cycl->GetInitialPrescale() << " orbits" << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" << endl
            << "Channel: " << emph << cycl->GetBChannel() << NOCOL
            << " (\"" << emph << boardLockingProxy()->GetBChannel(cycl->GetBChannel())->GetName() << NOCOL
            << "\")"
            << "<br>" << endl
            << (cycl->GetPostscale() > 0 ? emph : deflt) << cycl->GetPostscale() << " &times;" << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->GetPause() > 0 ? emph : deflt) << "Pause: "
            << cycl->GetPause() << " orbits" << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->IsRepetitive() ? deflt : emph)
            << (cycl->IsRepetitive() ? "(repetitive)" : "(non-repetitive)")
            << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->IsPermanent() ? (cycl->GetBChannel() != 1 ? alert : emph) : deflt)
            << (cycl->IsPermanent() ? "(permanent)" : "(non-permanent)") << NOCOL << endl;
      }
    }

    // BGO channels:
    size_t nch = 0;
    for (size_t i = 0; i < boardLockingProxy()->NChannels(); ++i)
    {
      BChannel* bch = boardLockingProxy()->GetBChannel(i);
      for (size_t j = 0; j < bch->NWords(); ++j)
      {
        if (!bch->IsLastWord(j))
        {
          ++nch;
          break;
        }
      }
    }
    if (nch > 0)
    {
      stab.NewRow();
      stab.NewCell("middle lightgreen", 1, nch + 1);
      xoh << "<a href=\"" << url << "BGOConfiguration\">";
      xoh << "BGO</a>" << endl;

      for (size_t i = 0; i < boardLockingProxy()->NChannels(); ++i)
      {
        BChannel* bch = boardLockingProxy()->GetBChannel(i);
        if (bch->NWords() <= 1)
        {
          continue;
        }
        stab.NewRow();
        stab.NewCell("top", 1, 1);
        xoh
            << BOLD
            << "Ch " << emph << i << NOCOL
            << " - " << emph << bch->GetName() << NOCOL
            << NOBOLD << endl;

        const uint32_t rmc = boardLockingProxy()->Read(TTCciAdd::CHRMC, i, "(TTCciControl)");

        xoh
            << "<br>N<sub>requested</sub> = " << boardLockingProxy()->BGORequestCounter(rmc) << endl
            << "<br>" << endl;

        const uint32_t cancelcntr = boardLockingProxy()->BGOCancelledCounter(rmc);

        if (cancelcntr > 0)
          xoh << RED;

        xoh << "N<sub>cancelled</sub> = " << cancelcntr << endl;

        if (cancelcntr > 0)
          xoh << NOCOL;

        stab.NewCell();
        xoh << UNDERL<< "DATA:" << NOUNDERL<< endl;

        for (size_t j = 0; j < bch->NWords(); ++j)
        {
          if (bch->IsLastWord(j))
            continue;
          xoh << "<br>" << MONO<<emph << "0x" << hex << bch->DataWord_D(j) << dec << NOCOL<< " ";
          xoh << (bch->IsACommand(j) ? alert + "(A-command)" + NOCOL:
          (bch->IsShortWord(j) ? "(short)" : "(long)")) << NOMONO<< endl;
        }

        stab.NewCell();
        xoh
            << (bch->IsRepetitive() ? "repetitive" : emph + "non-repetitive" + NOCOL) << "<br>" << endl
            << (bch->IsSingleCommand() ? "SINGLE" :
                                         (bch->IsDoubleCommand() ? emph + "DOUBLE" + NOCOL:emph + "BLOCK" + NOCOL))
            << " cmnd." << endl
            << (bch->GetDelayTime1() > 0 ? emph : deflt)
            << "<br>Delay t1: " << int(bch->GetDelayTime1()) << " BX" << NOCOL<< endl;

        if (!bch->IsSingleCommand())
        {
          xoh
              << (bch->GetDelayTime2() > 0 ? emph : deflt)
              << "<br>Delay t2: " << int(bch->GetDelayTime2()) << " BX" << NOCOL<< endl;
        }

        stab.NewCell();
        xoh
            << (bch->GetPrescale() > 0 ? emph : deflt)
            << "Prescale: " << bch->GetPrescale() << NOCOL<< "<br>" << endl
            << (bch->GetPostscale() > 0 ? emph : deflt)
            << "Postscale: " << bch->GetPostscale() << " times<br>" << NOCOL<< endl
            << (bch->GetInitialPrescale() > 0 ? emph : deflt)
            << "Init. offset: " << bch->GetInitialPrescale() << " req." << NOCOL<< endl;

        // B Counters:
        {
          const uint32_t rmc = boardLockingProxy()->Read(TTCciAdd::CHRMC, i);

          xoh << "<br>Last cancelled data: " << endl;
          if (boardLockingProxy()->HasCancelledBData(rmc))
          {
            xoh << RED << "yes (error!)" << NOCOL;
          }
          else
          {
            xoh << "no (o.k.)";
          }

          xoh
              << "<br>RAM: Buffer "
              << (TTCci::IsRamBufferAtEnd(rmc) ? string(UNDERL) + "reached" + string(NOUNDERL) :
                                                 string(UNDERL) + "not" + string(NOUNDERL) + " at end") << "<br>" << endl;
        }
      }

      // Sequences.
      stab.NewRow();
      stab.NewCell("center lightyellow", 5, 1);
      xoh
          << BOLD << "<a href=\"" << url << "Sequences\">"
          << "State transition sequences</a>" << NOBOLD << endl;

      stab.NewRow();
      for (size_t i = 0; i < 5; ++i)
      {
        bool enabled = false;
        bool disabled = false;
        stab.NewCell("left", 1, 1);
        string myseq;
        if (i == 0)
        {
          myseq = "coldReset";
        }
        else if (i == 1)
        {
          myseq = "configure";
        }
        else if (i == 2)
        {
          myseq = "enable";
        }
        else if (i == 3)
        {
          myseq = "suspend";
        }
        else if (i == 4)
        {
          myseq = "stop";
        }
        else
        {
          assert(false);
        }

        Sequence* seq = boardLockingProxy()->GetSequence(myseq);

        xoh << UNDERL<< BOLD << "'" << seq->GetName() << "'" << NOBOLD << NOUNDERL << endl;

        for (size_t j = 0; j < seq->N(); ++j)
        {
          xoh << "<br>- " << MONO << seq->Get(j) << NOMONO << endl;
          // Check if this command makes sense in this particular
          // sequence.
          if (seq->Get(j) == "DisableL1A")
          {
            disabled = true;
          }

          if (seq->Get(j) == "EnableL1A")
          {
            enabled = true;
          }

          if (myseq == "enable")
          {
            if (seq->Get(j) == "DisableL1A")
            {
              xoh << alert << "<br>&nbsp;&nbsp;disable L1A here?!?" << NOCOL<< endl;
            }
            else if (seq->Get(j) == "ResetCounters")
            {
              xoh << alert << "<br>&nbsp;&nbsp;reset counters here?!?" << NOCOL << endl;
            }
          }
          else if ((myseq == "coldReset") || (myseq == "configure") || (myseq == "suspend") || (myseq == "stop"))
          {
            if (seq->Get(j) == "EnableL1A")
            {
              xoh << alert << "<br>&nbsp;&nbsp;enable L1A here?!?" << NOCOL<< endl;
            }
          }
        }

        if (myseq == "enable")
        {
          if (!enabled)
          {
            xoh << alert << "<br>No " << MONO<< "EnableL1A" << NOMONO << " here?!?" << NOCOL << endl;
          }
        }
        else if ((myseq == "coldReset") || (myseq == "configure") || (myseq == "stop") || (myseq == "suspend"))
        {
          if (!disabled)
          {
            xoh << alert << "<br>No " << MONO << "DisableL1A" << NOMONO << " here?!?" << NOCOL << endl;
          }
        }
      }

      // User sequences.
      stab.NewRow();
      stab.NewCell("center lightyellow", 5, 1);
      xoh << BOLD << "User sequences" << NOBOLD << endl;

      vector<string> const seqNames = boardLockingProxy()->GetSequenceNames();
      for (std::vector<std::string>::const_iterator i = seqNames.begin(); i != seqNames.end(); ++i)
      {
        if ((*i != "coldReset") && (*i != "configure") && (*i != "enable") && (*i != "stop") && (*i != "suspend"))
        {
          stab.NewRow();
          stab.NewCell("center lightyellow", 5, 1);

          Sequence const* const seq = boardLockingProxy()->GetSequence(*i);
          xoh
              << UNDERL << BOLD << "'" << seq->GetName() << "'" << NOBOLD << NOUNDERL << endl;

          for (size_t j = 0; j < seq->N(); ++j)
          {
            xoh << "<br>- " << MONO << seq->Get(j) << NOMONO << endl;
          }
        }
      }
    }

    stab.Close();

    { // General info.
      xoh
          << "<br>" << BOLD << "General Info" << NOBOLD << "<br>" << endl
          << "TTCciLocation: " << Location_
          << " (Actual slot = " << boardLockingProxy()->GetInfo().location() << ")"
          << "<br>" << endl
          << "BTimeCorrection: " << int32_t(BTimeCorrection_) << " BX, " << endl
          << "DelayT2Correction: " << int32_t(DelayT2Correction_) << " BX" << endl;
    }
    PrintHTMLFooter(xoh);
  }
}


void ttc::TTCciControl::HTMLPageRegisterAccess(xgi::Input * /*in*/, xgi::Output * out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageRegisterAccess");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
  xoh << cgicc::title("TTCci '" + name_.toString() + "'") << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  xoh << "Finite State Machine: ";
  PrintFSMState(xoh);

  xoh << cgicc::p() << cgicc::p() << endl;

  {
    // DumpVME commands

    std::string cmd_url = "/";
    cmd_url += getApplicationDescriptor()->getURN();
    cmd_url += "/Command";

    xoh
        << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data") << endl
        << cgicc::input().set("type", "submit").set("name", "Command").set("value", "DumpVMEHistory")
        << cgicc::form();

    // THE VMEDATS/L stuff
    string sel = UNDERL, unsel = NOUNDERL;

    xoh
        << cgicc::form()
        .set("method", "post")
        .set("action", string("/" + getApplicationDescriptor()->getURN() + "/VMEBData"))
        .set("enctype", "multipart/form-data") << endl
        << cgicc::fieldset() << endl
        << cgicc::legend("Send B-Data directly through VME") << endl
        << "BData " << MONO<< "0x" << NOMONO << endl
        << cgicc::input()
        .set("type", "text")
        .set("name", "vmebdata")
        .set("size", "4")
        .set("value", Long2HexString(debugbdata_).c_str()) << "  " << endl;

    if (debugb_short_)
    {
      xoh
          << cgicc::input()
          .set("type", "radio")
          .set("name", "debugshort")
          .set("value", "yes")
          .set("checked", "checked")
          << sel << "SHORT" << unsel << " "
          << cgicc::input()
          .set("type", "radio")
          .set("name", "debugshort")
          .set("value", "no")
          << "LONG ";
    }
    else
    {
      xoh
          << cgicc::input()
          .set("type", "radio")
          .set("name", "debugshort")
          .set("value", "yes")
          << "SHORT" << " "
          << cgicc::input()
          .set("type", "radio")
          .set("name", "debugshort")
          .set("value", "no")
          .set("checked", "checked")
          << sel << "LONG " << unsel;
    }

    xoh
        << cgicc::fieldset() << endl
        << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Send")
        << cgicc::form() << endl;
  }

  std::string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/RegisterAccessCommand";

  xoh
      << cgicc::form()
      .set("method", "post")
      .set("action", command_url)
      .set("enctype", "multipart/form-data") << endl
      << cgicc::fieldset() << endl
      << cgicc::legend("Read from or write to TTCci Registers") << endl
      << cgicc::label("Address Name ") << endl;

  VMEAddrList1.Write(xoh);

  xoh
      << cgicc::label("Index ")
      << cgicc::input()
      .set("type", "text")
      .set("name", "index")
      .set("size", "4")
      .set("value", "0") << cgicc::p() << endl
      << "Value " << MONO << "0x" << NOMONO << endl
      << cgicc::input()
      .set("type", "text")
      .set("name", "Value")
      .set("size", "4")
      .set("value", "0") << cgicc::p() << endl
      << endl
      << cgicc::label("Read ")
      << cgicc::input().set("type", "checkbox").set("name", "readchecked") << endl
      << cgicc::label("Write ")
      << cgicc::input().set("type", "checkbox").set("name", "writechecked") << endl
      << cgicc::fieldset() << endl
      << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply")
      << cgicc::form() << endl;

  PrintHTMLFooter(xoh);
}


void ttc::TTCciControl::HTMLPageCountersPopup(xgi::Input*in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCountersPopup");

  MutexHandler h(countersPopup_mutex_);

  XgiOutputHandler xoh(*out);

  xoh
      << "<html>" << endl
      << "<head>" << endl
      << "<title>BGO counters(TTCci)</title>" << endl
      << "<meta HTTP-EQUIV=\"refresh\" CONTENT=\"2\" />" << endl
      << "</head>" << endl
      << "<body>" << endl;

  const string green = "#00C000";

  static CountersPopupData counters;
  CountersPopupData old_counters(counters);
  counters.refresh(boardLockingProxy());

  bool has_changed = (old_counters.event_counter != counters.event_counter);

  xoh << "<h1>TTCci (" << name_.toString() << ")</h1>\n";
  xoh << "<table>\n";

  string td, end_td;

  if (has_changed)
    td = "<td align=\"center\" colspan=\"2\" bgcolor=\"" + green + "\">";
  else
    td = "<td align=\"center\" colspan=\"2\">";

  xoh << "<tr>";
  xoh << "<td>" << "Event counter ";

  if (!boardLockingProxy()->IsL1AEnabled())
  {
    xoh << "<font color=\"red\">(L1A DISABLED)</font>";
  }

  xoh
      << "</td>" << "<td>" << "&nbsp;" << "</td>" << td << counters.event_counter << "</td>"
      << "</tr>\n"
      << "<tr bgcolor=\"#CCCCCC\">" << "<td>&nbsp;</td>" << "<td>&nbsp;</td>" << "<td>req'd</td>"
      << "<td>can'd</td>" << "</tr>\n";

  unsigned num_bgo_channels = boardLockingProxy()->NChannels();
  bool has_long_per_channel_bgo_counters = boardLockingProxy()->HasLongBGOCountersPerChannel();

  // loop over all BGO channels
  for (size_t i = 0; i < num_bgo_channels; ++i)
  {
    BChannel* bch = boardLockingProxy()->GetBChannel(i);
    bool is_empty = (bch->NWords() <= 1);
    bool requested_has_changed = (
        i >= old_counters.bgos_requested.size())
        || (old_counters.bgos_requested[i] != counters.bgos_requested[i]);

    bool cancelled_has_changed = (
        i >= old_counters.bgos_cancelled.size())
        || (old_counters.bgos_cancelled[i] != counters.bgos_cancelled[i]);

    const string num_col_width = "50";
    const string width_attr = "width=\"" + num_col_width + "\"";

    string requested_td, cancelled_td;
    if (requested_has_changed)
      requested_td = "<td align=\"right\" " + width_attr + " bgcolor=\"" + green + "\">";
    else
      requested_td = "<td align=\"right\" " + width_attr + ">";

    if (cancelled_has_changed)
      cancelled_td = "<td align=\"right\" " + width_attr + " bgcolor=\"red\">";
    else
      cancelled_td = "<td align=\"right\" " + width_attr + ">";

    string font_color, end_font_color;
    if (is_empty)
    {
      font_color = "<font color=\"#C0C0C0\">";
      end_font_color = "</font>";
    }

    xoh
        << "<tr>"
        << "<td>" << font_color << "BGO Channel " << i << end_font_color << "</td>"
        << "<td>" << font_color << bch->GetName() << end_font_color << "</td>"
        << requested_td << font_color << counters.bgos_requested[i];

    // If we have large per channel BGO counters, calculate the
    // number of BGOs/orbit.
    if (has_long_per_channel_bgo_counters)
      xoh << "(" << counters.BgosPerOrbitString(old_counters, i) << "/orb)";

    xoh
        << end_font_color
        << "</td>" << cancelled_td << font_color << counters.bgos_cancelled[i] << end_font_color
        << "</td>"
        << "</tr>\n";
  }

  xoh
      << "</table>\n"
      << "</body>\n"
      << "</html>\n";
}


void ttc::TTCciControl::HTMLPageCommandNewConfigFile(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandNewConfigFile");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("New Configuration File") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  std::string fileName = cgi["ConfigurationFile"]->getValue();
  std::string file_radiobutton = cgi["inputfiles"]->getValue();

  string newpath;

  if (file_radiobutton == "other" && fileName.size() > 0)
    newpath = fileName;
  else
    newpath = file_radiobutton;

  xoh << "New path to configuration file = " << newpath << "<br>" << endl;

  FILE* fp = fopen(newpath.c_str(), "r");
  if (!fp)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Unable to open file '" + newpath + "' for reading");
  }
  else
  {
    asciConfigurationFilePath_ = newpath;
    ReadConfigFromFile_ = true;
    fclose(fp);
  }

  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandWriteConfigFile(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageWriteConfigurationFile");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Write Configuration File") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  std::string fileName = cgi["ConfigurationFile"]->getValue();

  if (fileName.size() > 0)
  {
    asciConfigurationFilePath_ = fileName;
    ReadConfigFromFile_ = true;
    LOG4CPLUS_INFO(
        logger_,
        "Path to new Configuration file = '" + asciConfigurationFilePath_.toString() + "'");
    xoh << "Path to new Configuration file = " << asciConfigurationFilePath_.toString() << "<br>" << endl;
  }

  bool overwrite = cgi.queryCheckbox("overwrite");
  if (!overwrite)
  {
    FILE* fp = fopen(asciConfigurationFilePath_.toString().c_str(), "r");
    if (fp)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "File '" + asciConfigurationFilePath_.toString() + "' "
          "already exists, but 'force overwrite' was not checked!");
    }
  }

  ofstream oo(asciConfigurationFilePath_.toString().c_str(), ios_base::out);
  if (!oo)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Unable to open file '" + asciConfigurationFilePath_.toString() +
        "' for writing");
  }

  boardLockingProxy()->WriteConfiguration(oo, string("(from TTCciControl::") + string("WriteConfigurationFile())"));
  LOG4CPLUS_INFO(logger_, "Configuration written to file '" + asciConfigurationFilePath_.toString() + "'.");
  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandMainConfig(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandMainConfig");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Main Configuration") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  // Trigger sources.
  std::string value = cgi["triggersource"]->getValue();
  if (value.size() == 0)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "No CGI form element 'triggersource'");
  }

  {
    TriggerSourceList.SetDefault(value);
    if (value == "other")
    {
      std::vector<string> all = TriggerSourceList_other.AllVector();
      for (size_t jk = 0; jk < all.size(); ++jk)
      {
        if (cgi.queryCheckbox((string("triggersource2__") + all[jk]).c_str()))
        {
          TriggerSourceList_other.Check(all[jk]);
        }
        else
        {
          TriggerSourceList_other.UnCheck(all[jk]);
        }
      }
      if (boardLockingProxy()->GetRAMTriggers()->CanSetInternalTriggerFrequency())
      {
        double myValue = cgi["TriggerFrequency"]->getDoubleValue(0.0);
        bool myRandom = cgi.queryCheckbox("TriggerRandom");
        if ((myValue > 0.) && (myValue < 40000000.))
        {
          if ((oldfrequency_ != myValue) || (myRandom != boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerRandom()))
          {
            boardLockingProxy()->GetRAMTriggers()->SetInternalTriggerFrequency(myValue, myRandom);
            boardLockingProxy()->GetRAMTriggers()->WriteTriggerDelaysToTTCci();
            oldfrequency_ = myValue;
          }
        }
        else
        {
          XCEPT_RAISE(
              xcept::Exception,
              "Unable to extract trigger frequency from '" +
              cgi["TriggerFrequency"]->getValue() + "'");
        }
      }
    }
  }

  //----------------------------------------
  // External trigger input delay.
  //----------------------------------------
  {
    vector<unsigned> delayable_ext_trig_inputs = boardLockingProxy()->GetDelayableExternalTrigInputs();

    for (unsigned i = 0; i < delayable_ext_trig_inputs.size(); ++i)
    {
      unsigned input_num = delayable_ext_trig_inputs[i];

      int delay = cgi["ExtTrig" + boost::lexical_cast<string>(input_num) + "InputDelay"]->getIntegerValue(-1);
      if ((delay >= 0) && (delay <= 255))
      {
        boardLockingProxy()->SetExtTrigInputDelay(input_num, delay);
      }
    } // loop over all delayable trigger inputs
  }

  //----------------------------------------

  // Clock sources.
  value = cgi["clocksource"]->getValue();
  if (value.size() == 0)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "No CGI form element 'clocksource'");
  }

  ClockSourceList.SetDefault(value);
  LOG4CPLUS_INFO(
      logger_,
      "value = '" << value << "' " <<
      "--> Changed clock source to '" << ClockSourceList.GetDefault() << "'");

  // BGO sources.
  value = cgi["bgosource"]->getValue();
  if (value.size() == 0)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "No CGI form element 'bgosource'");
  }

  BGOSourceList.SetDefault(value);
  LOG4CPLUS_INFO(
      logger_,
      "value = '" << value << "' " <<
      "--> Changed BGO source to '" << BGOSourceList.GetDefault() << "'");

  if (value == "other")
  {
    std::vector<string> all = BGOSourceList_other.AllVector();
    for (size_t jk = 0; jk < all.size(); ++jk)
    {
      if (cgi.queryCheckbox((string("bgosource2__") + all[jk]).c_str()))
      {
        BGOSourceList_other.Check(all[jk]);
      }
      else
      {
        BGOSourceList_other.UnCheck(all[jk]);
      }
    }
  }

  // Orbit sources.
  value = cgi["orbitsource"]->getValue();
  if (value.size() == 0)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "No CGI form element 'orbitsource'");
  }

  OrbitSourceList.SetDefault(value);
  LOG4CPLUS_INFO(
      logger_,
      "value = '" << value << "' " <<
      " --> Changed orbit source to '" << OrbitSourceList.GetDefault() << "'");

  // QPLL settings.
  vector<string> tvec = TriggerSourceList_other.CheckedVector();
  vector<string> bvec = BGOSourceList_other.CheckedVector();
  if ((INTERFACE(TriggerSourceList.GetDefault()) == ttc::CTC)
      || (INTERFACE(TriggerSourceList.GetDefault()) == ttc::LTCIN))
  {
    tvec.clear();
    tvec.push_back(TriggerSourceList.GetDefault());
  }
  if ((INTERFACE(BGOSourceList.GetDefault()) == ttc::CTC) || (INTERFACE(BGOSourceList.GetDefault()) == ttc::LTCIN))
  {
    bvec.clear();
    bvec.push_back(BGOSourceList.GetDefault());
  }

  boardLockingProxy()->SelectInputs(ClockSourceList.GetDefault(), OrbitSourceList.GetDefault(), tvec, bvec);

  bool internalclock = (ClockSourceList.GetDefault() == INAME(ttc::INTERNAL));

  std::string tmpMsg = "";
  if (internalclock)
  {
    tmpMsg = "TRUE (i.e., using internal clock)";
  }
  else
  {
    tmpMsg = "FALSE (i.e., using external clock)";
  }

  LOG4CPLUS_INFO(logger_, tmpMsg);

  if (internalclock)
  {
    boardLockingProxy()->DisableQPLL();
  }
  else
  {
    boardLockingProxy()->EnableQPLL();
  }
  if (!internalclock)
  {
    cgicc::const_form_iterator qpllEnableRadio = cgi.getElement("QPLLReset");
    if (qpllEnableRadio->getValue() == "YES")
    {
      boardLockingProxy()->ConfiguringResetsQPLL(true);
      boardLockingProxy()->ResetQPLL(true);
    }
    else
    {
      boardLockingProxy()->ConfiguringResetsQPLL(false);
      boardLockingProxy()->ResetQPLL(false);
    }
    qpllEnableRadio = cgi.getElement("QPLLAutoRestart");
    if (qpllEnableRadio->getValue() == "ON")
    {
      boardLockingProxy()->AutoRestartQPLL(true);
    }
    else
    {
      boardLockingProxy()->AutoRestartQPLL(false);
    }
  }
  uint32_t freqbits;
  stringstream g(cgi["freqbits"]->getValue());
  g >> hex >> freqbits;
  boardLockingProxy()->SetQPLLFrequencyBits(freqbits, !internalclock);
  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandTriggerRules(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandTriggerRules");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Configure Trigger Rules") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  // Trigger rules.
  for (size_t i = boardLockingProxy()->FirstTriggerRule(); i < boardLockingProxy()->TriggerRuleSize(); ++i)
  {
    uint32_t myValue = cgi[("trigrule  " + UnsignedLong2String(i)).c_str()]->getIntegerValue(0);
    boardLockingProxy()->SetTriggerRule(i, myValue);
  }
  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandSelectBGO(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandSelectBGO");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Select BGO") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  // The BGO selection: which channel?
  uint32_t mybgo = cgi["BGOselect"]->getIntegerValue(0);
  if (mybgo < boardLockingProxy()->NChannels())
  {
    if (mybgo != currentBGO_)
    {
      currentBGO_ = mybgo;
      CurrentBGOList.SetDefault(currentBGO_);
    }
  }
  else
  {
    ostringstream err;
    err << "BGO channel #" << mybgo << " out of range";
    XCEPT_RAISE(xcept::Exception, err.str());
  }

  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandConfigureBGO(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandConfigureBGO");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Configure BGO") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  ttc::BChannel *bch = boardLockingProxy()->GetBChannel(currentBGO_);

  int err = 0;

  //----------------------------------------
  // Individual postscale values.
  //----------------------------------------
  if (boardLockingProxy()->HasIndividualBPostScaleNumbers())
  {
    bool is_modified = false;

    for (size_t index = 0; index < bch->NWords(); ++index)
    {
      string field_name = str(boost::format("indiv_postscale%d") % index);

      // string postscale_value_str = cgi[field_name]->getValue();
      string postscale_value_str = CgiUtils::CgiccGetParameterWithDefault(cgi, field_name);
      if (postscale_value_str.size() > 0)
        try
        {
          short postscale_value = boost::lexical_cast<short>(postscale_value_str);
          bch->SetIndividualPostScaleValue(index, postscale_value);
          is_modified = true;
        }
        catch (boost::bad_lexical_cast const& e)
        {
          ostringstream msg;
          msg << __PRETTY_FUNCTION__ << ": Invalid postscale value '" << postscale_value_str << "'";
          LOG4CPLUS_ERROR(logger_, msg.str());
        }

    } // loop over all active words

    if (is_modified)
      boardLockingProxy()->WriteBGODataToTTCci(currentBGO_);

  } // if the firmware supports individual postscale values

  //----------------------------------------
  // Add/Change a word of a (composite) BGO
  // command
  //----------------------------------------

  string cnewdata = cgi["newdata"]->getValue();
  //"datawordselect"
  string iword = cgi["datawordselect"]->getValue();
  string wdtype = cgi["datatypeselect"]->getValue();
  if (cnewdata.size() > 0 && iword != " ")
  {
    uint32_t newdata = 0; // = cgi["newdata"]->getIntegerValue(0);
    stringstream g(cnewdata);
    if (!(g >> hex >> newdata))
    {
      LOG4CPLUS_ERROR(logger_,
          toolbox::toString("TTCciControl::BGOConfigCommand(): Invalid new data '%s'!", cnewdata.c_str()));
    }
    else
    {
      const bool isACommand = (wdtype == "A-Command" ? true : false);
      const bool islong = ((!isACommand && wdtype == "Long") ? true : false);
      const bool notransmit = false;

      bool has_individual_b_postscale_numbers = boardLockingProxy()->HasIndividualBPostScaleNumbers();

      short postscale_value = 0;

      // Check for individual postscale values
      if (has_individual_b_postscale_numbers)
      {
        string postscale_value_str = CgiUtils::CgiccGetParameterWithDefault(cgi, "new_indiv_postscale");
        if (postscale_value_str.size() > 0)
          try
          {
            postscale_value = boost::lexical_cast<short>(postscale_value_str);
          }
          catch (boost::bad_lexical_cast const& e)
          {
            ostringstream msg;
            msg << __PRETTY_FUNCTION__ << ": Invalid postscale value '" << postscale_value_str << "'";
            LOG4CPLUS_ERROR(logger_, msg.str());
          }

      } // if the firmware supports individual B word postscale values

      if (iword == "New")
      {
        bch->PushBackData(newdata, !islong, isACommand, notransmit);

        if (has_individual_b_postscale_numbers)
          bch->SetIndividualPostScaleValue(bch->NWords() - 2, postscale_value);

        boardLockingProxy()->WriteBGODataToTTCci(currentBGO_);

      }
      else
      {
        size_t iw = 0;
        if (sscanf(iword.c_str(), "%zu", &iw) == 1 && iw < bch->NWords())
        {
          bch->SetData(size_t(iw), newdata, !islong, isACommand, notransmit);

          if (has_individual_b_postscale_numbers)
          {
            bch->SetIndividualPostScaleValue(iw, postscale_value);
          }

          boardLockingProxy()->WriteBGODataToTTCci(currentBGO_);
        }
        else
        {
          LOG4CPLUS_ERROR(logger_,
              toolbox::toString("TTCciControl::BGOConfigCommand(): Invalid word index '%s'!", iword.c_str()));
        }
      }
    }
  } // if the 'newdata' form input has been filled

  //----------------------------------------
  // Deletion of words.
  //
  // Go from high numbers to low numbers since we want to
  // delete.
  //----------------------------------------
  for (int n = bch->NWords() - 1; n >= 0; --n)
  {
    char dummy[30];
    sprintf(dummy, "delete%d", n);
    bool deletethis = cgi.queryCheckbox(dummy);
    if (deletethis)
    {
      bch->DeleteWord(n);
      boardLockingProxy()->WriteBGODataToTTCci(currentBGO_);
    }
  } // loop over all words of this channel

  //--------------------
  // Do not change the words of the data words in this bgo
  // command beyond this point since the indices
  // from the form are not valid any more.
  //--------------------

  // The main config:
  bool repetitive = cgi.queryCheckbox("repetitive");
  cgicc::const_form_iterator commandlength = cgi.getElement("commandlength");
  ttc::BGODataLength L = ttc::SINGLE;
  if (commandlength->getValue() == "single")
  {
    L = ttc::SINGLE;
  }
  else if (commandlength->getValue() == "double")
  {
    L = ttc::DOUBLE;
  }
  else if (commandlength->getValue() == "block")
  {
    L = ttc::BLOCK;
  }
  else
  {
    ++err;
    LOG4CPLUS_ERROR(
        logger_,
        toolbox::toString("TTCciControl::BGOConfigCommand(): Unknown data length '%s'", commandlength->getValue().c_str()));
  }
  uint32_t delayTime1 = cgi["delay1"]->getIntegerValue(0);
  uint32_t delayTime2 = cgi["delay2"]->getIntegerValue(0);
  uint32_t prescale = cgi["prescale"]->getIntegerValue(0);
  uint32_t postscale = cgi["postscale"]->getIntegerValue(0);
  uint32_t initprescale = cgi["offset"]->getIntegerValue(0);
  if (err == 0)
  {
    bch->Set(L, repetitive, delayTime1, delayTime2);
    boardLockingProxy()->Write(TTCciAdd::CHIHB, currentBGO_, bch->InhibitDelayWord_corr(),
        "(inhib/del word)");
    bch->SetPreAndInitPrescale(prescale, initprescale);
    boardLockingProxy()->Write(TTCciAdd::CHPRESC, currentBGO_, bch->PrescaleWord(),
        "(prescale)");
    bch->SetPostscale(postscale);
    boardLockingProxy()->Write(TTCciAdd::CHPOSTSC, currentBGO_, bch->PostscaleWord(),
        "(postscale)");
  }
  else
  {
    ostringstream err;
    err << "Unable to change config for channel '" << currentBGO_ << "'";
    XCEPT_RAISE(xcept::Exception, err.str());
  }

  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandSelectSequence(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandSelectSequence");

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  // The sequence selection: which sequence?
  string seqname = cgi["sequenceselect"]->getValue();
  string seqchoice = cgi["seqchoice"]->getValue();

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Select sequence '" + seqname + "'") << endl;

  if (seqchoice == "remove")
  {
    boardLockingProxy()->DeleteSequence(seqname);
  }
  else if (seqchoice == "add")
  {
    string newname = cgi["newsequence"]->getValue();
    if (newname.size() > 0)
    {
      boardLockingProxy()->AddSequence(newname);
      SequenceSelector.push_back(newname);
      SequenceSelector.SetDefault(newname);
    }
  }
  else if (seqchoice == "display")
  {
    SequenceSelector.SetDefault(seqname);
  }
  else
  {
    XCEPT_RAISE(xcept::Exception, "Invalid sequence action '" + seqchoice + "'");
  }

  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandEditSequence(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandEditSequence");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Edit sequence") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  // Which sequence line (or append)?
  string line = cgi["sequenceline"]->getValue();

  // Which action to take?
  string edit = cgi["edit"]->getValue();

  // Which (sub)action to take?
  string command = cgi["edit2"]->getValue();
  string command2 = cgi["edit3"]->getValue();

  // Which action to take?
  string xstring = cgi["x"]->getValue();
  int32_t x = -99;
  stringstream g(xstring);
  if (!String2Long(xstring, x))
  {
    x = -99;
  }

  const string SeqTitle = SequenceSelector.GetDefaultTitle();

  Sequence* myseq = boardLockingProxy()->GetSequence(SeqTitle);

  if (edit == "delete")
  {
    if (line == "< Append >")
    {
      XCEPT_RAISE(
          xcept::Exception,
          "To delete you have to choose a line other than '< Append >'!");
    }
    else
    {
      int index = myseq->GetIndex(line);
      if (index < 0)
      {
        XCEPT_RAISE(
            xcept::Exception,
            "Unable to find line '" + line + "' in sequence '" + SeqTitle + "'");
      }
      myseq->Delete(size_t(index));
    }
  }
  else if ((edit == "modify") || (edit == "new"))
  {
    if ((command == "Choose") && (command2 == "Choose"))
    {
      XCEPT_RAISE(
          xcept::Exception,
          "To '" + edit + "' you have to select a command other than '" + command + "'");
    }
    string newline = command;
    if (command == "Choose")
    {
      newline = command2;
    }
    else
    {
      if (command == "Sleep" ||
          command == "mSleep" ||
          command == "uSleep" ||
          command == "BGO" ||
          command == "Periodic" ||
          command == "SendShortBDATA" ||
          command == "SendLongBDATA")
      {
        if (
            (x == -99) ||
            ((command == "Sleep") && (x < 0)) ||
            ((command == "BGO") && ((x < 0) || (x > int(boardLockingProxy()->NChannels())))))
        {
          XCEPT_RAISE(
              xcept::Exception,
              "Invalid argument: '" + xstring + "' for command '" + command + "'");
        }
        newline = newline + " " + xstring;
      }
    }
    if (line == "< Append >")
    {
      myseq->PushBack(newline);
    }
    else
    {
      int index = myseq->GetIndex(line);
      if (index < 0)
      {
        XCEPT_RAISE(
            xcept::Exception,
            "Unable to find line '" + line + "' in sequence '" + SeqTitle + "'");
      }
      if (edit == "new")
      {
        myseq->Insert((size_t) index, newline);
      }
      else if (edit == "modify")
      {
        myseq->Set((size_t) index, newline);
      }
      else
      {
        XCEPT_RAISE(
            xcept::Exception,
            "Unknown action '" + edit + "'");
      }
    }
  }
  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandConfigureCyclicGenerator(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandConfigureCyclicGenerator");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Configure Cyclic Generator") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  // Loop over all cyclic generators.
  for (size_t ik = 0; ik < (boardLockingProxy()->NCyclicBGO() + boardLockingProxy()->NCyclicTrigger()); ++ik)
  {
    char genc[23];
    sprintf(genc, "%zu__", ik);
    const string sgen = string(genc);
    bool trigger = (ik >= boardLockingProxy()->NCyclicBGO());
    const size_t i = (!trigger ? ik : ik - boardLockingProxy()->NCyclicBGO());
    CyclicTriggerOrBGO *cycl = boardLockingProxy()->GetCyclic(trigger, i);

    // Start-BX.
    uint32_t val = cgi[(sgen + "startbx").c_str()]->getIntegerValue(0);
    cycl->SetStartBX(val);
    // Prescale
    val = cgi[(sgen + "prescale").c_str()]->getIntegerValue(0);
    cycl->SetPrescale(val);
    // Initial prescale.
    val = cgi[(sgen + "initprescale").c_str()]->getIntegerValue(0);
    cycl->SetInitialPrescale(val);
    // Postscale
    val = cgi[(sgen + "postscale").c_str()]->getIntegerValue(0);
    cycl->SetPostscale(val);
    // Pause
    val = cgi[(sgen + "pause").c_str()]->getIntegerValue(0);
    cycl->SetPause(val);
    // Select the B-Channel:
    if (!trigger)
    {
      string channel = cgi[(sgen + "channelno").c_str()]->getValue();
      val = cgi[(sgen + "channelno").c_str()]->getIntegerValue();
      cycl->SetBChannel(val);
    }
    // repetitive
    bool boolval = cgi.queryCheckbox((sgen + "repetitive").c_str());
    cycl->SetRepetitive(boolval);
    // repetitive
    boolval = cgi.queryCheckbox((sgen + "permanent").c_str());
    cycl->SetPermanent(boolval);
    // enable
    cgicc::const_form_iterator EnableRadio = cgi.getElement((sgen + "enable").c_str());
    cycl->SetEnable((EnableRadio->getValue() == "enable"));

    boardLockingProxy()->WriteCyclicGeneratorToTTCci(trigger, i);
  }
  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandSendVMEBData(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandSendVMEBData");

  XgiOutputHandler xoh(*out);

  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << cgicc::h1("Send VME B-Data") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  cgicc::const_form_iterator debugBdataRadio = cgi.getElement("debugshort");
  debugb_short_ = (debugBdataRadio->getValue() == "yes" ? true : false);

  stringstream g(cgi["vmebdata"]->getValue());
  g >> hex >> debugbdata_;

  if (debugb_short_)
  {
    boardLockingProxy()->Write(TTCciAdd::VMEDATS, debugbdata_, "(VMEDATS)");
  }
  else
  {
    boardLockingProxy()->Write(TTCciAdd::VMEDATL, debugbdata_, "(VMEDATL)");
  }

  RedirectMessage(&xoh, lastPage_);
}


void ttc::TTCciControl::HTMLPageCommandRegisterAccess(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "TTCciControl::HTMLPageCommandRegisterAccess");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::h1("Read/Write VME Register - Results:") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  const std::string parName = cgi["Addresses"]->getValue();

  if (parName != "none")
  {
    bool read = cgi.queryCheckbox("readchecked");
    bool write = cgi.queryCheckbox("writechecked");

    const uint32_t index = cgi["index"]->getIntegerValue(0);

    uint32_t myValue;
    stringstream g(cgi["Value"]->getValue());
    g >> hex >> myValue;

    if (read || write)
    {
      xoh << "<ul>" << endl;
    }

    if (read)
    {
      xoh << "<li>" << endl;
      xoh << "Reading from VME address with name '" << MONO << parName << NOMONO << "', index=" << dec << index
          << ": <br>" << endl;
      uint32_t output = boardLockingProxy()->Read(parName, index);

      xoh << MONO<< BLUE
          << "0x" << hex << output << dec << " = " << output
          << NOCOL<< NOMONO << endl;

      if (write)
      {
        xoh << BLUE << " (before)." << NOCOL << endl;
      }
      xoh << "</li>" << endl;
    }

    if (write)
    {
      xoh << "<li>" << endl;
      xoh << "Writing 0x" << hex << myValue << dec << " = " << myValue << " to VME address with name '"
          << MONO<<parName << NOMONO
          << "', index=" << dec << index << endl;
      boardLockingProxy()->Write(parName, index, myValue);
      xoh << "</li>" << endl;

      if (read)
      {
        xoh << "<li>" << endl;

        xoh << "Reading (again) from VME address with name '" << MONO << parName << NOMONO
        << "', index=" << dec << index << ": <br> " << endl;
        uint32_t output = boardLockingProxy()->Read(parName, index);
        xoh << MONO << BLUE
            << "0x" << hex << output << dec << " = " << output
            << NOMONO << NOCOL << endl
            << BLUE << " (after)." << NOCOL<< endl;

        xoh << "</li>" << endl;
      }
    }

    if (read || write)
      xoh << "</ul>" << endl;
  }

  string oldurl = "/" + getApplicationDescriptor()->getURN() + "/";
  xoh << BOLD<< BIGGER
      << "<a href=\"" << oldurl << "RegisterAccess\"> Back to previous page </a>"
      << NOBIGGER<< NOBOLD << "<br>" << endl;
}


void ttc::TTCciControl::initHTMLFields()
{
  if (areHTMLFieldsInited())
    return;

  { // Trigger sources.
    vector<string> values_all = boardLockingProxy()->GetSourceListNames("Trigger");
    vector<string> values, values_int;
    for (size_t i = 0; i < values_all.size(); ++i)
    {
      if ((values_all[i] == "CTC") || (values_all[i] == "LTC"))
      {
        values.push_back(values_all[i]);
      }
      else
      {
        values_int.push_back(values_all[i]);
      }
    }
    vector<string> titles = values;
    values.push_back("other");
    titles.push_back("other / choose from:");
    TriggerSourceList.Set(HTMLFieldElement::RADIOBUTTON, 0, "triggersource", values, titles, "vertical green bold");
    TriggerSourceList.SetDefault(values[0]);
    TriggerSourceList_other.Set(HTMLFieldElement::CHECKBOX, 0, "triggersource2", values_int, values_int,
        "vertical green bold");
  }
  { // Clock sources.
    vector<string> values = boardLockingProxy()->GetSourceListNames("Clock");
    ClockSourceList.Set(HTMLFieldElement::DROPDOWNMENU, 0, "clocksource", values);
    ClockSourceList.SetDefault(values[0]);
  }
  { // BGO sources.
    vector<string> values_all = boardLockingProxy()->GetSourceListNames("BGO");
    vector<string> values, values_int;
    for (size_t i = 0; i < values_all.size(); ++i)
    {
      if ((values_all[i] == "CTC") || (values_all[i] == "LTC"))
      {
        values.push_back(values_all[i]);
      }
      else
      {
        values_int.push_back(values_all[i]);
      }
    }
    vector<string> titles = values;
    values.push_back("other");
    titles.push_back("other / choose from:");
    BGOSourceList.Set(HTMLFieldElement::RADIOBUTTON, 0, "bgosource", values, titles, "vertical green bold");
    BGOSourceList.SetDefault(values[0]);
    BGOSourceList_other.Set(HTMLFieldElement::CHECKBOX, 0, "bgosource2", values_int, values_int, "vertical green bold");
  }
  { // Orbit sources.
    vector<string> values = boardLockingProxy()->GetSourceListNames("Orbit");
    OrbitSourceList.Set(HTMLFieldElement::DROPDOWNMENU, 0, "orbitsource", values);
    OrbitSourceList.SetDefault(values[0]);
  }

  {
    // The 16 BGO Channels.
    vector<string> values, titles;
    for (size_t i = 0; i < boardLockingProxy()->NChannels(); ++i)
    {
      char si[100];
      sprintf(si, "%zu", i);
      values.push_back(si);
      string name = boardLockingProxy()->GetBChannel(i)->GetName();
      if ((name[0] != 'C') || (name[1] != 'h') || (name[2] != 'a'))
      {
        name += (string(" (") + string(si) + string(")"));
      }
      titles.push_back(name);
    }
    CurrentBGOList.Set(HTMLFieldElement::DROPDOWNMENU, 0, "BGOselect", values, titles);
    currentBGO_ = 0;
    CurrentBGOList.SetDefault(currentBGO_);
  }
  { // The cyclic generators.
    vector<string> values, titles;
    int jj = 0;
    for (size_t i = 0; i < boardLockingProxy()->NCyclicTrigger(); ++i)
    {
      titles.push_back(boardLockingProxy()->GetCyclic(true, i)->GetName());
      char tit[11];
      sprintf(tit, "%d", jj++);
      values.push_back(tit);
    }
    for (size_t i = 0; i < boardLockingProxy()->NCyclicBGO(); ++i)
    {
      titles.push_back(boardLockingProxy()->GetCyclic(false, i)->GetName());
      char tit[11];
      sprintf(tit, "%d", jj++);
      values.push_back(tit);
    }
    CurrentCyclicList.Set(HTMLFieldElement::DROPDOWNMENU, 0, "cyclicselect", values, titles);
    CurrentCyclicList.SetDefault(values[0]);
  }
  { // VME registers.
    vector<string> add1, add1_tit;
    boardLockingProxy()->GetVMEAddresses(add1, add1_tit);
    add1.insert(add1.begin(), string("none"));
    add1_tit.insert(add1_tit.begin(), string("Choose parameter"));
    VMEAddrList1.Set(HTMLFieldElement::DROPDOWNMENU, 0, "Addresses", add1, add1_tit);
    VMEAddrList1.SetDefault("none");
  }

  {
    vector<string> values, titles;
    values.push_back("Short");
    titles.push_back("Short");
    values.push_back("Long");
    titles.push_back("Long");
    values.push_back("A-Command");
    titles.push_back("A-Cmnd/L1A");
    DataTypeSelector.Set(HTMLFieldElement::DROPDOWNMENU, 0, "datatypeselect", values, titles);
    DataTypeSelector.SetDefault(values[0]);
  }

  {
    const vector<string> values = boardLockingProxy()->GetSequenceNames();
    SequenceSelector.Set(HTMLFieldElement::DROPDOWNMENU, 0, "sequenceselect", values, values);
    SequenceSelector.SetDefault(values[0]);
  }

  setHTMLFieldsInited();
}


void ttc::TTCciControl::ErrorStatement(xgi::Output& out)
{
  out << "<div id=\"" << ERROR_MESSAGES_DIV << "\">" << endl;

  if ((boardLockingProxy()->getSelectedClockSource() != INTERNAL) && !boardLockingProxy()->ClockLocked())
  {
    LOG4CPLUS_WARN(logger_, "TTCci Clock NOT locked!");
    error_messages.add("ERROR") << "TTCci Clock NOT locked!";
  }

  if (!error_messages.isEmpty())
  {
    out << error_messages.asHtml() << endl;
    error_messages.clear();
  }

  out << "</div>" << endl;
}


void ttc::TTCciControl::ReadTTCciCounters()
{
  tprevious_ = tnow_;
  time(&tnow_);
  previousEventCounter_ = EventCounter_;
  EventCounter_ = boardLockingProxy()->ReadEventCounter();
  previousOrbitCounter_ = OrbitCounter_;
  OrbitCounter_ = boardLockingProxy()->ReadOrbitCounter();
  previousStrobeCounter_ = StrobeCounter_;
  StrobeCounter_ = boardLockingProxy()->ReadStrobeCounter();
  BoardStatus_ = boardLockingProxy()->BoardStatus();
  QPLLLocked_ = boardLockingProxy()->ClockLocked();
}


void ttc::TTCciControl::GetFileList()
{
  char s[1024];
  gethostname(s, sizeof s);
  string machine = s;
  size_t pos = machine.find_first_of(".");
  if (pos < machine.size())
    machine.erase(machine.begin() + pos, machine.end());
  machine = string(" (on ") + MONO + machine + NOMONO + ")";

  string path = "", suffix = "";
  std::vector<std::string> filelist, filelistvar;
  if (ReadConfigFromFile_)
  {
    path = asciConfigurationFilePath_.toString();
    filelist.push_back(string(MONO) + path + string(NOMONO) + machine);
    filelistvar.push_back(path);
    size_t pos = path.find_last_of("/");
    if (pos < path.size())
    {
      suffix = path;
      path.erase(path.begin() + pos, path.end());
      // find the ending
      suffix.erase(suffix.begin(), suffix.begin() + pos + 1);
      pos = suffix.find_last_of(".");
      if (pos == 0)
        suffix = "";
      else if (pos < suffix.size())
        suffix.erase(suffix.begin(), suffix.begin() + pos);
      else
      {
        std::string msg = "No ending found in file '" + suffix + "'";
        LOG4CPLUS_ERROR(logger_, msg);
        error_messages.add("ERROR") << msg;
        suffix = "";
      }
    }
  }
  std::vector<std::string> addlist = ttc::filelist(path, "", (suffix = ".dat"));
  for (size_t i = 0; i < addlist.size(); ++i)
  {
    if (filelistvar.size() > 0 && addlist[i] == filelistvar[0])
      continue;
    filelist.push_back(string(MONO) + addlist[i] + string(NOMONO) + machine);
    filelistvar.push_back(addlist[i]);
  }
  addlist = ttc::filelist(path, "", (suffix = ".txt"));
  for (size_t i = 0; i < addlist.size(); ++i)
  {
    if (filelistvar.size() > 0 && addlist[i] == filelistvar[0])
      continue;
    filelist.push_back(string(MONO) + addlist[i] + string(NOMONO) + machine);
    filelistvar.push_back(addlist[i]);
  }
  filelist.push_back("other" + machine + ":");
  filelistvar.push_back("other");
  InputFileList.Set(HTMLFieldElement::RADIOBUTTON, 0, "inputfiles", filelistvar, filelist, "vertical bold blue");
  InputFileList.SetDefault(filelistvar[0]);
}


uint32_t ttc::TTCciControl::GetBGOWord(size_t channel, size_t iword, bool Dword)
{
  string Name = "CH";
  if (channel < 10)
  {
    Name += "0";
  }
  char cchan[3];
  sprintf(cchan, "%zu", channel);
  Name += string(cchan) + (Dword ? "D" : "P");
  return boardLockingProxy()->Read(Name, iword);
}
