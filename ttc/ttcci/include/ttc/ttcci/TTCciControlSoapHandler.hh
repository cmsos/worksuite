#ifndef _ttc_ttcci_TTCciControlSoapHandler_hh_
#define _ttc_ttcci_TTCciControlSoapHandler_hh_


#include <map>
#include <string>

#include "log4cplus/logger.h"
#include "xoap/MessageReference.h"
#include "toolbox/lang/Method.h"


namespace ttc
{

class TTCciControl;

//! Class handling SOAP callbacks for TTCciControl
class TTCciControlSoapHandler
{
public:

  TTCciControlSoapHandler(TTCciControl* _ttcci_control);

  void addMethod(toolbox::lang::Method* m, std::string name);

private:

  /**
   * Callback for SOAP message having an element with tag "userCommand" or "ExecuteSequence" in the body.
   * Supports new-style (parameters as attributes in the "userCommand" or "ExecuteSequence" element)
   * and old-style messages (parameters as attributes of the xdaq:Body element).
   */
  xoap::MessageReference userCommand(xoap::MessageReference msg);

  /**
   * Callback for SOAP message having a "User"-tagged element in the body.
   * Executes the special sequence "user" from the TTCci config file.
   */
  xoap::MessageReference userSequence(xoap::MessageReference msg);

  /**
   * Callback for SOAP message having a "GetCurrentConfiguration"-tagged element in the body.
   * Produces a text string out of the current configuration, and sends this as the SOAP reply.
   */
  xoap::MessageReference GetCurrentConfiguration(xoap::MessageReference msg);

  /**
   * The "executeSequence" SOAP action.
   * If the sequence executed corresponds to an state transition the FSM moves accordingly.
   */
  void ExecuteSequenceImplementation(const std::string& sequence_name);


  /**
   * Extracts attributes from a DOMNode @param n
   * as attribute-value pairs into map @param p.
   * The namespace attribute xmlns:xdaq is ignored.
   */
  void extractParametersFromAttributes(DOMNode* n, std::map<std::string, std::string>& p);

  //! pointer to the TTCci control application
  TTCciControl* ttcci_control;

  log4cplus::Logger logger_;
};

}


#endif
