#ifndef _ttc_utils_version_h_
#define _ttc_utils_version_h_

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version !!!
#define WORKSUITE_TTCUTILS_VERSION_MAJOR 1
#define WORKSUITE_TTCUTILS_VERSION_MINOR 11
#define WORKSUITE_TTCUTILS_VERSION_PATCH 0
#undef WORKSUITE_TTCUTILS_PREVIOUS_VERSIONS

#define WORKSUITE_TTCUTILS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TTCUTILS_VERSION_MAJOR,WORKSUITE_TTCUTILS_VERSION_MINOR,WORKSUITE_TTCUTILS_VERSION_PATCH)
#ifndef WORKSUITE_TTCUTILS_PREVIOUS_VERSIONS
#define WORKSUITE_TTCUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TTCUTILS_VERSION_MAJOR,WORKSUITE_TTCUTILS_VERSION_MINOR,WORKSUITE_TTCUTILS_VERSION_PATCH)
#else
#define WORKSUITE_TTCUTILS_FULL_VERSION_LIST  TTCUTILS_VERSION_MAJOR "," TTCUTILS_VERSION_MINOR "," TTCUTILS_VERSION_PATCH
#endif

namespace ttcutils {
	const std::string project = "worksuite";
  const std::string package  = "ttcutils";
  const std::string versions = WORKSUITE_TTCUTILS_FULL_VERSION_LIST;
  const std::string description = "Library of various generic utilities for TTC";
  const std::string authors = "CMS DAQ group";
  const std::string summary = "Library of various generic utilities for TTC";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TTCManual";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
