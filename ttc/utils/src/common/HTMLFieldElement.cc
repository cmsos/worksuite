#include "ttc/utils/HTMLFieldElement.hh"

#include "ttc/utils/HTMLTable.hh"

#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"

#include <algorithm>
#include <sstream>
#include <stdexcept>


using namespace std;


ttc::HTMLFieldElement::HTMLFieldElement()
{}


ttc::HTMLFieldElement::HTMLFieldElement(
    const FieldType type,
    const int flag,
    const string &name,
    const vector<string> &values,
    const vector<string> &titles,
    const string &Options)
:
    _flag(flag),
    _name(name),
    _values(values),
    _titles(titles),
    _checked(values.size(), false),
    _default(0),
    _type(type),
    _options(Options)
{
  if (_titles.size() != _values.size()
      || _values.size() == 0
      || _checked.size() != _values.size())
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::HTMLFieldElement(): "
        << "_titles.size() != _titles.size()=" << _titles.size()
        << " _values.size()=" << _values.size()
        << " _checked.size()=" << _checked.size() << ") (or size=0)";
    throw std::invalid_argument(my.str());
  }

  if (name.size() == 0)
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::HTMLFieldElement(): "
        << "name = '" << name << "' name.size() = " << name.size();
    throw std::invalid_argument(my.str());
  }

  SetOptions(Options);
}


ttc::HTMLFieldElement::HTMLFieldElement(
    const FieldType type,
    const int flag,
    const string &name,
    const vector<string> &values,
    const string &Options)
:
    _default(0)
{
  HTMLFieldElement(type, flag, name, values, values, Options);
}


void ttc::HTMLFieldElement::Set(
    const FieldType type,
    const int flag,
    const string &name,
    const vector<string> &values,
    const vector<string> &titles,
    const string &Options)
{
  _type = type;
  _flag = flag;
  _name = name;
  _values = values;
  _titles = titles;
  _checked.resize(_values.size(), false);

  SetOptions(Options);

  if (_titles.size() != _values.size()
      || _values.size() == 0)
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::Set(): "
        << "_titles.size() != _values.size() "
        << "(" << _titles.size() << " != " << values.size() << ") or size=0";
    throw std::invalid_argument(my.str());
  }

  if (name.size() == 0)
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::Set(): "
        << "name = '" << name << "' name.size() = " << name.size();
    throw std::invalid_argument(my.str());
  }
}


void ttc::HTMLFieldElement::Set(
    const FieldType type,
    const int flag,
    const string &name,
    const vector<string> &values,
    const string &Options)
{
  Set(type, flag, name, values, values, Options);
}


string ttc::HTMLFieldElement::GetName() const
{
  return _name;
}


void ttc::HTMLFieldElement::SetName(const string& Name)
{
  _name = Name;
}


void ttc::HTMLFieldElement::push_back(const string& value, const string& title)
{
  _values.push_back(value);
  _titles.push_back(title.size() > 0 ? title : value);
  _checked.push_back(false);
}


bool ttc::HTMLFieldElement::HasValue(const string& value) const
{
  return (find(_values.begin(), _values.end(), value) != _values.end());
}


void ttc::HTMLFieldElement::SetDefault(const string& value)
{
  if (value == _strdefault)
    return;

  _strdefault = value;
  _default = 0;

  bool found = false;
  for (size_t i = 0; i < _values.size(); ++i)
  {
    if (value == _values[i])
    {
      _default = i;
      found = true;
    }
  }

  if (!found)
  {
    if (value == "UNDEFINED")
    {
      _default = 0;
      _strdefault = _values[_default];
    }
    else
    {
      stringstream my;
      my << "WARNING: HTMLFieldElement::SetDefault(): " << "default value '" << value << "'  not found!";
      throw std::invalid_argument(my.str());
    }
  }
}


void ttc::HTMLFieldElement::SetDefault(const int32_t value)
{
  stringstream g;
  g << dec << value;
  string val = g.str();

  if (HasValue(val))
  {
    SetDefault(val);
  }
  else
  {
    if (value >= 0 && static_cast<size_t>(value) < _values.size())
    {
      _default = value;
      _strdefault = _values[_default];
    }
    else
    {
      cout
          << "ERROR: HTMLFieldElement::SetDefault(int value=" << value << "): "
          << "No element matches this value!" << endl;
    }
  }
}


void ttc::HTMLFieldElement::SetCheck(const size_t index, bool val)
{
  if (_type != CHECKBOX)
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::SetCheck(): "
        << "Type NOT CHECKBOX!";
    cout << my.str();
    return;
  }

  if (index < _checked.size())
  {
    _checked[index] = val;
  }
  else
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::SetCheck(): "
        << "index=" << index << " out of range! (max = " << _checked.size() << ")";
    cout << my.str();
  }
}


void ttc::HTMLFieldElement::Check(const string& value)
{
  SetCheck(value, true);
}


void ttc::HTMLFieldElement::UnCheck(const string& value)
{
  SetCheck(value, false);
}


void ttc::HTMLFieldElement::UnCheckAll()
{
  if (_type != CHECKBOX)
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::UnCheckAll(): "
        << "Type NOT CHECKBOX!";
    cout << my.str();
    return;
  }

  _checked.clear();
  _checked.resize(_values.size(), false);
}


vector<string> ttc::HTMLFieldElement::AllVector() const
{
  return _values;
}


vector<string> ttc::HTMLFieldElement::CheckedVector() const
{
  vector<string> vec;
  if (_type != CHECKBOX)
  {
    stringstream my;
    my << "WARNING: HTMLFieldElement::CheckedVector(): Type NOT CHECKBOX!";
    cout << my.str();
    return vec;
  }

  for (size_t i = 0; i < _checked.size(); ++i)
  {
    if (_checked[i])
    {
      vec.push_back(_values[i]);
    }
  }

  return vec;
}


const string& ttc::HTMLFieldElement::GetDefault() const
{
  return _strdefault;
}


const string&
ttc::HTMLFieldElement::GetDefaultTitle() const
{
  for (size_t i = 0; i < _values.size(); ++i)
  {
    if (_strdefault == _values[i])
    {
      return _titles[i];
    }
  }

  cout
      << "ERROR: HTMLFieldElement::GetDefaultTitle(): "
      << "_strdefault=\"" << _strdefault << "\" not found!" << endl;
  return _strdefault;
}


int ttc::HTMLFieldElement::GetDefaultInt() const
{
  int myint;
  if (1 == sscanf(_strdefault.c_str(), "%d", &myint))
    return myint;
  else
    return -1;
}


void ttc::HTMLFieldElement::Write(ostream& out)
{
  if (_values.size() == 0)
  {
    stringstream my;
    my
        << "ERROR: HTMLFieldElement::Write(): "
        << "_values.size()=" << _values.size() << "!";
    throw std::invalid_argument(my.str());
  }

  switch (_type)
  {
    case DROPDOWNMENU:
      out << "<select name=\"" << _name << "\" width=\"20\">" << endl;
      for (size_t i = 0; i < _values.size(); ++i)
      {
        out << "<option value=\"" << _values[i] << "\"" << (i == _default ? "selected" : "") << ">" << _titles[i]
            << endl;
      }
      out << "</select>" << endl;
      break;

    case RADIOBUTTON:
    {
      for (size_t i = 0; i < _values.size(); ++i)
      {
        if (i == _default)
        {
          if (_BoldSelected)
            out << "<span style=\"font-weight: bold;\">";
          if (_UnderlineSelected)
            out << "<span style=\"text-decoration: underline;\">";
          if (_GreenSelected)
            out << "<span style=\"color: rgb(51, 204, 0);\">";
          if (_BlueSelected)
            out << "<span style=\"color: rgb(51, 51, 255);\">";
          if (_RedSelected)
            out << "<span style=\"color: rgb(255, 0, 0);\">";
        }

        if (_TitleBeforeField)
        {
          out << cgicc::label(_titles[i] + string(" "));
        }

        if (i == _default)
        {
          out
              << cgicc::input().set("type", "radio").set("name", _name).set("value", _values[i]).set("checked",
                  "checked");
        }
        else
        {
          out << cgicc::input().set("type", "radio").set("name", _name).set("value", _values[i]);
        }

        if (!_TitleBeforeField)
        {
          out << cgicc::label(_titles[i] + string(" "));
        }

        if (_arrangeVertically)
        {
          out << "<br>";
        }

        if (i == _default)
        {
          for (int k = 0;
              k
                  < (int(_UnderlineSelected) + int(_BoldSelected) + int(_GreenSelected) + int(_BlueSelected)
                      + int(_RedSelected)); ++k)
          {
            out << "</span>";
          }
        }
        out << endl;
      }
      break;
    }

    case CHECKBOX:
    {
      const bool usetable = (_values.size() > 16 ? true : false);
      HTMLTable *tab = 0;
      if (usetable)
      {
        tab = new HTMLTable(out, 0, 1, 1, "", "");
      }

      for (size_t i = 0; i < _values.size(); ++i)
      {
        if (usetable && i % 16 == 0 && tab)
          tab->NewCell();
        if (_checked[i])
        {
          if (_BoldSelected)
            out << "<span style=\"font-weight: bold;\">";
          if (_UnderlineSelected)
            out << "<span style=\"text-decoration: underline;\">";
          if (_GreenSelected)
            out << "<span style=\"color: rgb(51, 204, 0);\">";
          if (_BlueSelected)
            out << "<span style=\"color: rgb(51, 51, 255);\">";
          if (_RedSelected)
            out << "<span style=\"color: rgb(255, 0, 0);\">";
        }

        if (_TitleBeforeField)
          out << cgicc::label(_titles[i] + string(" "));
        if (_checked[i])
        {
          out
              << cgicc::input().set("type", "checkbox").set("name", _name + "__" + _values[i]).set("checked",
                  "checked");
        }
        else
        {
          out << cgicc::input().set("type", "checkbox").set("name", _name + "__" + _values[i]);
        }
        if (!_TitleBeforeField)
          out << cgicc::label(_titles[i] + string(" "));
        if (_arrangeVertically)
          out << "<br>";
        if (_checked[i])
        {
          for (int k = 0;
              k < (int(_UnderlineSelected) + int(_BoldSelected) + int(_GreenSelected) + int(_BlueSelected) + int(_RedSelected));
              ++k)
          {
            out << "</span>";
          }
        }
        out << endl;
      }
      if (usetable && tab)
      {
        tab->Close();
        delete tab;
      }
      break;
    }

    default:
      stringstream my;
      my << "ERROR: HTMLFieldElement::Write(): " << "ERROR!";
      throw std::invalid_argument(my.str());
  }
}


size_t ttc::HTMLFieldElement::size() const
{
  return _values.size();
}


string ttc::HTMLFieldElement::GetValue(const size_t i)
{
  if (i < size())
  {
    return _values[i];
  }
  else
  {
    cout
        << "ERROR: HTMLFieldElement::GetValue(i=" << i << "): "
        << "Index out of range (name=" << GetName() << ")!" << endl;
    return string("");
  }
}


string ttc::HTMLFieldElement::GetTitle(const size_t i)
{
  if (i < size())
  {
    return _titles[i];
  }
  else
  {
    cout
        << "ERROR: HTMLFieldElement::GetTitle(i=" << i << "): "
        << "Index out of range (name=" << GetName() << ")!" << endl;
    return string("");
  }
}


void ttc::HTMLFieldElement::SetTitle(const size_t i, const string &tit)
{
  if (i < size())
  {
    _titles[i] = tit;
  }
  else
  {
    cout
        << "ERROR: HTMLFieldElement::SetTitle(i=" << i << ", \"" << tit << "\"): "
        << "Index out of range (name=" << GetName() << ")!" << endl;
  }
}


void ttc::HTMLFieldElement::SetCheck(const string& value, bool val)
{
  if (_type != CHECKBOX)
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::SetCheck(): "
        << "Type NOT CHECKBOX!";
    cout << my.str();
    return;
  }

  bool found = false;
  const string value2 = RemovePrefix(value);
  for (size_t i = 0; i < _values.size(); ++i)
  {
    if (value == _values[i] || value2 == _values[i])
    {
      _checked[i] = val;
      found = true;
    }
  }

  if (!found)
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::SetCheck(): "
        << "value '" << value << "'  not found!";
    cout << my.str() << endl;
  }
}


void ttc::HTMLFieldElement::Check(const size_t index)
{
  SetCheck(index, true);
}


void ttc::HTMLFieldElement::UnCheck(const size_t index)
{
  SetCheck(index, false);
}


bool ttc::HTMLFieldElement::IsChecked(const size_t index) const
{
  if (_type != CHECKBOX)
  {
    stringstream my;
    my << "WARNING: HTMLFieldElement::IsChecked(): Type NOT CHECKBOX!";
    cout << my.str();
    return false;
  }

  if (index < _checked.size())
  {
    return _checked[index];
  }

  stringstream my;
  my
      << "WARNING: HTMLFieldElement::IsChecked(): "
      << "index=" << index << " out of range! (max = " << _checked.size() << ")";
  cout << my.str();
  return false;
}


bool ttc::HTMLFieldElement::IsChecked(const string& value) const
{
  if (_type != CHECKBOX)
  {
    stringstream my;
    my
        << "WARNING: HTMLFieldElement::IsChecked(): "
        << "Type NOT CHECKBOX!";
    cout << my.str();
    return false;
  }

  const string value2 = RemovePrefix(value);
  for (size_t i = 0; i < _values.size(); ++i)
  {
    if (value == _values[i] || value2 == _values[i])
    {
      return _checked[i];
    }
  }

  stringstream my;
  my
      << "WARNING: HTMLFieldElement::IsChecked(): "
      << "default value '" << value << "'  not found!";
  cout << my.str() << endl;
  return false;
}


void ttc::HTMLFieldElement::SetOptions(const string& Options)
{
  _options = Options;
  _arrangeVertically = false; // for Radio & Checkbox only
  _TitleBeforeField = false;  // for Radio & Checkbox only
  _BoldSelected = false;      // for Radio & Checkbox only
  _UnderlineSelected = false; // for Radio & Checkbox only
  _GreenSelected = false;     // for Radio & Checkbox only
  _BlueSelected = false;      // for Radio & Checkbox only
  _RedSelected = false;       // for Radio & Checkbox only

  if (FindString(_options, "Vert", "vert", "VERT"))
  {
    _arrangeVertically = true;
  }

  if (FindString(_options, "Horiz", "horiz", "HORIZ"))
  {
    _arrangeVertically = false;
  }

  if (FindString(_options, "Tit", "tit", "TIT"))
  {
    _TitleBeforeField = true;
  }

  if (FindString(_options, "Bold", "bold", "Bold"))
  {
    _BoldSelected = true;
  }

  if (FindString(_options, "Underl", "underl", "UNDERL"))
  {
    _UnderlineSelected = true;
  }

  if (FindString(_options, "Green", "green", "GREEN"))
  {
    _GreenSelected = true;
  }

  if (FindString(_options, "Blue", "blue", "BLUE"))
  {
    _BlueSelected = true;
  }

  if (FindString(_options, "RED", "red", "Red"))
  {
    _RedSelected = true;
  }
}


size_t ttc::HTMLFieldElement::GetDefaultIndex() const
{
  return _default;
}


bool ttc::HTMLFieldElement::IsRadioButton() const
{
  return _type == RADIOBUTTON;
}


bool ttc::HTMLFieldElement::IsDropDownMenu() const
{
  return _type == DROPDOWNMENU;
}


bool ttc::HTMLFieldElement::IsCheckBoxes() const
{
  return _type == CHECKBOX;
}


bool ttc::HTMLFieldElement::FindString(
    const string& basestr,
    const string& str1,
    const string& str2,
    const string& str3) const
{
  if (str1.size() > 0)
  {
    if (search(basestr.begin(), basestr.end(), str1.begin(), str1.end()) != basestr.end())
    {
      return true; // found str1 in basestr
    }
  }

  if (str2.size() > 0)
  {
    if (search(basestr.begin(), basestr.end(), str2.begin(), str2.end()) != basestr.end())
    {
      return true; // found str2 in basestr
    }
  }

  if (str3.size() > 0)
  {
    if (search(basestr.begin(), basestr.end(), str3.begin(), str3.end()) != basestr.end())
    {
      return true; // found str3 in basestr
    }
  }

  return false; // nothing found
}


string ttc::HTMLFieldElement::RemovePrefix(const string& value) const
{
  string out = value;
  size_t begin = value.find(string("__"));
  begin += (string("__")).size();
  if (begin >= value.size())
    return out;
  out.clear();
  for (size_t i = begin; i < value.size(); ++i)
  {
    out.push_back(value[i]);
  }
  return out;
}
