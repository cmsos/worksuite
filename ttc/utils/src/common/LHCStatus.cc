#include "ttc/utils/LHCStatus.hh"

#include <boost/lexical_cast.hpp>


using namespace std;


string ttc::LHCStatus::GetVerboseStatus(Status status)
{
  switch (status)
  {
    case LHC_NOMODE:
      return "No mode, data is not available, not set";
    case LHC_SETUP:
      return "Setup";
    case LHC_INJPILOT:
      return "Pilot injection";
    case LHC_INJINTR:
      return "Intermediate injection";
    case LHC_INJNOMN:
      return "Nominal injection";
    case LHC_PRERAMP:
      return "Before ramp";
    case LHC_RAMP:
      return "Ramp";
    case LHC_FLATTOP:
      return "Flat top";
    case LHC_SQUEEZE:
      return "Squeeze";
    case LHC_ADJUST:
      return "Adjust beam on flat top";
    case LHC_STABLE:
      return "Stable beam for physics";
    case LHC_UNSTABLE:
      return "Unstable beam";
    case LHC_BEAMDUMP:
      return "Beam dump";
    case LHC_RAMPDOWN:
      return "Ramp down";
    case LHC_RECOVERY:
      return "Recovering";
    case LHC_INJDUMP:
      return "Inject and dump";
    case LHC_CIRCDUMP:
      return "Circulate and dump";
    case LHC_ABORT:
      return "Recovery after a beam permit flag drop";
    case LHC_CYCLING:
      return "Pre-cycle before injection, no beam";
    case LHC_WBDUMP:
      return "Warning beam dump";
    case LHC_NOBEAM:
      return "No beam or preparation for beam";
  }

  return "unknown mode " + boost::lexical_cast<string>(static_cast<int>(status));
}


bool ttc::LHCStatus::IsDefinedMode(Status status)
{
  return (status >= LHC_NOMODE) && (status <= LHC_NOBEAM);
}
