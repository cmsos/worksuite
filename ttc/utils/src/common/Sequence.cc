#include "ttc/utils/Sequence.hh"

#include <sstream>
#include <iostream>
#include <stdexcept>


using namespace std;


ttc::Sequence::Sequence()
:
    permanent_(false)
{}


ttc::Sequence::Sequence(
    const vector<string> &sequence,
    const string &name,
    const bool permanent)
:
    name_(name),
    permanent_(permanent)
{
  for (size_t i = 0; i < sequence.size(); ++i)
  {
    string newline = RemoveCommentsAndBlanks(sequence[i]);
    if (newline.size() > 0)
    {
      PushBack(newline);
    }
  }
}


vector<string> ttc::Sequence::Get() const
{
  return seq_;
}


string ttc::Sequence::GetName() const
{
  return name_;
}


bool ttc::Sequence::IsPermanent() const
{
  return permanent_;
}


size_t ttc::Sequence::N() const
{
  return seq_.size();
}


void ttc::Sequence::Clear()
{
  seq_.clear();
}


string ttc::Sequence::Get(size_t i) const
{
  if (i >= seq_.size())
  {
    stringstream err;
    err
        << "ERROR: Sequence::Get(i=" << i << "): "
        << "invalid index i=" << i << " since size(sequence)=" << seq_.size();
    cout << err.str();
    throw std::invalid_argument(err.str());
  }
  return seq_[i];
}


void ttc::Sequence::Set(size_t i, const string& line)
{
  string newline = RemoveCommentsAndBlanks(line);
  if (newline.size() == 0)
    return;
  if (i >= seq_.size())
  {
    stringstream err;
    err
        << "ERROR: Sequence::Set(i=" << i << "): "
        << "invalid index i=" << i << " since size(sequence)=" << seq_.size();
    cout << err.str();
    throw std::invalid_argument(err.str());
  }
  seq_[i] = newline;
}


void ttc::Sequence::PushBack(const string& line)
{
  string newline = RemoveCommentsAndBlanks(line);
  if (newline.size() == 0)
    return;

  seq_.push_back(newline);
}


void ttc::Sequence::Insert(size_t i, const string& line)
{
  string newline = RemoveCommentsAndBlanks(line);
  if (newline.size() == 0)
    return;
  if (i >= seq_.size())
  {
    stringstream err;
    err
        << "ERROR: Sequence::Insert(i=" << i << "): "
        << "invalid index i=" << i << " since size(sequence)=" << seq_.size();
    cout << err.str();
    throw std::invalid_argument(err.str());
  }

  seq_.insert((seq_.begin() + i), newline);
}


void ttc::Sequence::Delete(size_t i)
{
  if (i >= seq_.size())
  {
    stringstream err;
    err
        << "ERROR: Sequence::Delete(i=" << i << "): "
        << "invalid index i=" << i << " since size(sequence)=" << seq_.size();
    cout << err.str();
    throw std::invalid_argument(err.str());
  }

  seq_.erase((seq_.begin() + i));
}


int ttc::Sequence::GetIndex(const string& line) const
{
  for (size_t i = 0; i < N(); ++i)
  {
    if (line == seq_[i])
    {
      return int(i);
    }
  }

  return -1;
}


string ttc::Sequence::RemoveCommentsAndBlanks(const string& line) const
{
  string newline;
  bool blank = false;

  for (size_t i = 0; i < line.size(); ++i)
  {
    char dum = ((char) line[i]);
    if (dum == '#')
      break;

    if (dum == ' ' || dum == '\t')
    {
      if (!blank && i != 0)
      {
        newline += string(" ");
      }
      blank = true;
    }
    else
    {
      newline += line[i];
      blank = false;
    }
  }

  return newline;
}


bool ttc::Sequence::operator==(const Sequence& entry) const
{
  return this->GetName() == entry.GetName();
}


bool ttc::Sequence::operator!=(const Sequence& entry) const
{
  return !((*this) == entry);
}


bool ttc::Sequence::operator==(const string& s) const
{
  return this->GetName() == s;
}


bool ttc::Sequence::operator!=(const string& s) const
{
  return !((*this) == s);
}
