#include "ttc/utils/YuiRadioButtonGroup.hh"

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <cassert>
#include <algorithm>


using namespace std;


ttc::YuiRadioButtonGroup::YuiRadioButtonGroup(
    const string& _name,
    const string& _html_span_name)
:
    YuiWidgetWithPostRequest(_name),
    html_span_name(_html_span_name)
{}


string ttc::YuiRadioButtonGroup::getHtmlCode()
{
  ostringstream buf;
  buf << "<div id=\"" << html_span_name << "\" class=\"yui-buttongroup\">" << endl;

  assert(button_labels.size() == button_values.size());

  for (size_t i = 0; i < button_values.size(); ++i)
  {
    buf
        << "  <span class=\"yui-button yui-radio-button\">" << endl
        << "    <span class=\"first-child\">" << endl
        << "      <button type=\"button\" value=\"" << button_values[i] << "\">" << button_labels[i] << "</button>" << endl
        << "    </span>" << endl
        << "  </span>" << endl;
  }

  buf << "</div>" << endl;

  return buf.str();
}


void ttc::YuiRadioButtonGroup::addButton(const string& label, const string& value)
{
  button_labels.push_back(label);
  button_values.push_back(value);
}


string ttc::YuiRadioButtonGroup::getJavaScriptCreationCode(const string& initial_value)
{
  assert(html_span_name != "");

  // Convert the initial_value to an index.
  size_t initial_index =
      ::find(button_values.begin(), button_values.end(), initial_value)
      - button_values.begin();

  if (initial_index >= button_values.size())
  {
    throw std::invalid_argument("There is no radio group button with the label '" + initial_value + "'");
  }

  // HttpCommand_SetBCRefSource
  ostringstream buf;
  buf
      << "<script type=\"text/javascript\">" << endl
      << "var " << name << " = new YAHOO.widget.ButtonGroup(\"" << html_span_name << "\");" << endl
      << name << ".check(" << initial_index << ");" << endl
      << name << ".on(\"checkedButtonChange\", " << endl
      << makeMakeSubmitCommandInvocationCode(
          "function() { "
          "var val = " + name + ".get('checkedButton').get('value'); "
          "return val;"
          "}") << ");" << endl
      << "</script>" << endl;

  return buf.str();
}
