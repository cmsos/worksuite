#include "ttc/utils/HtmlTag.hh"

#include <sstream>
#include <cassert>


using namespace std;


ttc::HtmlTag::HtmlTag(const string &_tag_name)
:
    tag_name(_tag_name)
{}


void ttc::HtmlTag::SetStyle(const string &name, const string &value)
{
  styles[name] = value;
}


void ttc::HtmlTag::SetAttr(const string &name, const string &value)
{
  attributes[name] = value;
}


void ttc::HtmlTag::PrintOpening(ostream &os)
{
  os << "<" << tag_name;

  // if we have CSS styles set, check that the 'style' attribute
  // is not set at the same time
  if (styles.size() > 0)
  {
    assert(attributes.count("style") == 0);

    os << " style=\"";

    // loop over all CSS styles
    for (map<string, string>::const_iterator
        it = styles.begin();
        it != styles.end(); ++it)
    {
      if (it != styles.begin())
        os << ";";

      os << it->first << ":" << it->second;
    }

    os << "\"";
  }

  // print all the cell attributes
  for (map<string, string>::const_iterator
      it = attributes.begin();
      it != attributes.end(); ++it)
  {
    os << " " << it->first << "=\"" << it->second << "\"";
  }

  os << ">";
}


void ttc::HtmlTag::PrintClosing(ostream &os)
{
  os << "</" << tag_name << ">";
}


void ttc::HtmlTag::Print(ostream &os)
{
  PrintOpening(os);
  os << content;
  PrintClosing(os);
}


void ttc::HtmlTag::ClearAllAttrs()
{
  attributes.clear();
}


void ttc::HtmlTag::ClearAllStyles()
{
  styles.clear();
}


string ttc::HtmlTag::toString()
{
  ostringstream buf;
  Print(buf);
  return buf.str();
}
