#include "ttc/utils/RAMTriggers.hh"

#include "ttc/utils/GenericTTCModule.hh"
#include "ttc/utils/Utils.hh"

#include <cmath>


using namespace std;


// class ttc::RAMTriggers - static

const size_t ttc::RAMTriggers::RAM_TRIG_PATTERN_MAX_LENGTH = 1024;

const unsigned ttc::RAMTriggers::RAM_TRIG_PATTERN_STOP = 1;

const unsigned ttc::RAMTriggers::RAM_TRIG_PATTERN_REPEAT = 0;

const unsigned ttc::RAMTriggers::RAM_TRIG_MAX_DELAY = 0x3ffff;


// class ttc::RAMTriggers - members


ttc::RAMTriggers::RAMTriggers(
    GenericTTCModule& _module,
    const Address& _start_trig_dpram_addr)
:
    DirectlyWriteTriggerDPRAM(false),
    triggerFrequency(-1.),
    randomTrigger(false),
    triggerdelay(0),
    module(_module),
    start_trig_dpram_addr(_start_trig_dpram_addr)
{}


void ttc::RAMTriggers::SetInternalTriggerFrequency(const double frequency, bool random)
{
  if (!CanSetInternalTriggerFrequency())
  {

    LOG4CPLUS_ERROR(
        module.getLogger(),
        "RAMTriggers::SetInternalTriggerFrequency(): "
        "Cannot set internal trigger frequency");
    return;
  }

  // clear the current trigger pattern
  triggerdelay.clear();

  triggerFrequency = frequency;
  randomTrigger = random;

  const double mean = (1.0 / frequency) / 25.0e-9;

  if (random)
  {
    // Random trigger with given mean frequency.

    int32_t Tval = -1;
    for (size_t i = 0; i < RAM_TRIG_PATTERN_MAX_LENGTH; ++i)
    {
      if (Tval < 0)
      {
        Tval = RandomTrigger(mean); // draw a random number (?)
      }

      if ((Tval >= int32_t(RAM_TRIG_MAX_DELAY))
          && (i < (RAM_TRIG_PATTERN_MAX_LENGTH - 1)))
      {
        triggerdelay.push_back(RAM_TRIG_MAX_DELAY);
        Tval -= RAM_TRIG_MAX_DELAY;
      }
      else
      {
        triggerdelay.push_back(Tval > int32_t(RAM_TRIG_PATTERN_STOP) ? Tval : 2);
        Tval = -1;
      }
    }
  }
  else
  {
    // Equidistant triggers.

    int32_t Tval = int32_t(mean + 0.5);

    for (size_t i = 0; i < RAM_TRIG_PATTERN_MAX_LENGTH; ++i)
    {
      if ((Tval >= int32_t(RAM_TRIG_MAX_DELAY))
          && (i < (RAM_TRIG_PATTERN_MAX_LENGTH - 1)))
      {
        triggerdelay.push_back(RAM_TRIG_MAX_DELAY);
        Tval -= RAM_TRIG_MAX_DELAY;
      }
      else
      {
        Tval = (Tval > int32_t(RAM_TRIG_PATTERN_STOP) ? Tval : 2);
        triggerdelay.push_back(Tval);

        if (triggerdelay.size() < RAM_TRIG_PATTERN_MAX_LENGTH)
        {
          // If we reached the maximum length, the pattern will be repeated anyway...
          triggerdelay.push_back(RAM_TRIG_PATTERN_REPEAT);
        }

        // stop writing the pattern after one trigger
        break;
      }
    }

    if (Tval >= int32_t(RAM_TRIG_MAX_DELAY))
    {
      LOG4CPLUS_WARN(
          module.getLogger(),
          "RAMTriggers::SetInternalTriggerFrequency(): "
          "Unable to use frequency = " << frequency << " Hz"
          << " (lowest possible is " << 1. / ((double(RAM_TRIG_PATTERN_MAX_LENGTH) * double(RAM_TRIG_MAX_DELAY) - 1.0) * 25e-9) << " Hz)");
    }
  }

  if (triggerdelay.size() == RAM_TRIG_PATTERN_MAX_LENGTH
      && triggerdelay[RAM_TRIG_PATTERN_MAX_LENGTH - 1] == RAM_TRIG_MAX_DELAY)
  {
    triggerdelay[RAM_TRIG_PATTERN_MAX_LENGTH - 1] = RAM_TRIG_MAX_DELAY - 1;
  }

  // Below this it's just information for logging.
  uint32_t num = 0, min = 0, max = 0, cur = 0;
  double mean0 = 0, ssd = 0;
  bool cont = false, started = false;

  // Std dev formula from: http://www.megspace.com/science/sfe/i_ot_std.html
  for (size_t i = 1; i < triggerdelay.size(); ++i)
  {
    if (cont)
    {
      cur += triggerdelay[i];
    }
    else
    {
      cur = triggerdelay[i];
    }
    cont = (triggerdelay[i] == RAM_TRIG_MAX_DELAY);
    if (cont)
      continue;
    if (triggerdelay[i] < 2)
    {
      break;
    }
    else if (cur == 2)
    {
      cur = 3;
    }
    if (!started)
    {
      started = true;
      num = 1;
      ssd = 0;
      mean0 = min = max = cur;
    }
    else
    {
      num++;
      double d = (cur - mean0) / num;
      mean0 += d;
      ssd += (cur - mean0) * (cur - mean0);
      if (min > cur)
        min = cur;
      if (max < cur)
        max = cur;
    }
  }

  double var = ssd / (num - 1);
  LOG4CPLUS_INFO(
      module.getLogger(),
      "Generated triggers:\n"
      "  Num = " << num << "\n"
      "  Mean0 = " << mean0 << "\n"
      "  Variance = " << var << "\n"
      "  Std dev = " << sqrt(var) << "\n"
      "  Min = " << min << "\n"
      "  Max = " << max);
}


void ttc::RAMTriggers::SetInternalTriggerFrequencyLTC(const double frequency, bool random)
{
  SetInternalTriggerFrequency(frequency, random);
  WriteTriggerDelaysToLTC(true);
}


vector<unsigned>& ttc::RAMTriggers::GetTriggerDelays()
{
  return triggerdelay;
}


bool ttc::RAMTriggers::CanSetInternalTriggerFrequency() const
{
  return !DirectlyWriteTriggerDPRAM;
}


double ttc::RAMTriggers::GetInternalTriggerFrequency() const
{
  return triggerFrequency;
}


bool ttc::RAMTriggers::GetInternalTriggerRandom() const
{
  return randomTrigger;
}


void ttc::RAMTriggers::ClearTriggerDelays()
{
  triggerdelay.clear();
}


void ttc::RAMTriggers::WriteTriggerDelaysToTTCci(bool repeat_sequence)
{
  if (triggerdelay.size() > 0)
  {
    /*
     size_t last = triggerdelay.size()-1;
     if (repeat_sequence && triggerdelay[last]!=1 && triggerdelay[last]!=0){
     triggerdelay.push_back(0);
     }
     */
    bool first = true;
    for (size_t i = 0; i < triggerdelay.size(); ++i)
    {
      if (DirectlyWriteTriggerDPRAM
          && first
          && triggerdelay[i] > RAM_TRIG_PATTERN_STOP
          && triggerdelay[i] < RAM_TRIG_MAX_DELAY - 1)
      {
        first = false;
        module.Write(start_trig_dpram_addr, i, triggerdelay[i] + 1, "(trigger delay)");
      }
      else
      {
        module.Write(start_trig_dpram_addr, i, triggerdelay[i], "(trigger delay)");
      }
    }
  }
  else
  {
    triggerdelay.push_back(RAM_TRIG_PATTERN_STOP);
    for (size_t i = 0; i < triggerdelay.size(); ++i)
    {
      module.Write(start_trig_dpram_addr, i, triggerdelay[i], "(trigger delay)");
    }
  }
}


void ttc::RAMTriggers::WriteTriggerDelaysToLTC(bool repeat_sequence)
{
  if (triggerdelay.size() == 0)
  {
    triggerdelay.push_back(RAM_TRIG_PATTERN_STOP);
  }

  size_t last = triggerdelay.size() - 1;
  if (last < (RAM_TRIG_PATTERN_MAX_LENGTH - 1)
      && triggerdelay[last] >= 2)
  {
    triggerdelay.push_back(repeat_sequence ? RAM_TRIG_PATTERN_REPEAT : RAM_TRIG_PATTERN_STOP);
  }

  for (size_t i = 0; i < triggerdelay.size(); ++i)
  {
    module.Write(start_trig_dpram_addr, i, triggerdelay[i], "(trigger delay)");
  }
}


void ttc::RAMTriggers::ClearDPRAM()
{
  module.Write(start_trig_dpram_addr, 0, RAM_TRIG_PATTERN_STOP);
}


vector<unsigned> ttc::RAMTriggers::GetRAMTriggerDelaysFromTTCci()
{
  vector<unsigned> retval(RAM_TRIG_PATTERN_MAX_LENGTH);
  for (unsigned i = 0; i < RAM_TRIG_PATTERN_MAX_LENGTH; ++i)
  {
    retval[i] = module.Read(start_trig_dpram_addr, i, "(trigger delay)");
  }

  return retval;
}


void ttc::RAMTriggers::SetRAMTriggerDelaysDirectly(const vector<unsigned> &values)
{
  assert(values.size() <= RAM_TRIG_PATTERN_MAX_LENGTH);

  for (unsigned i = 0; i < values.size(); ++i)
  {
    module.Write(start_trig_dpram_addr, i, values[i]);
  }
}
