#include "ttc/utils/JavaScriptUtils.hh"

#include <sstream>


using namespace std;


string JavaScriptUtils::makeSubmitCommand()
{
  ostringstream buf;
  buf
      << "var Dom = YAHOO.util.Dom;" << endl
      << "var Event = YAHOO.util.Event;" << endl
      << endl
      << "/** " << endl
      << "   Produces a function which can be called when e.g. a button is clicked (and then submits a command via HTTP)." << endl
      << endl
      << "   @param getValueFunction is a function which returns the current value of the control (?)." << endl
      << endl
      << "*/" << endl
      << "function makeSubmitCommand(url,args,getValueFunction)" << endl
      << "{" << endl
      << "  var retval = " << endl
      << "      function() " << endl
      << "      {" << endl
      << "        var callback =" << endl
      << "            {" << endl
      << "              failure: function(o) { alert(\"Communication broken\"); }," << endl
      << "              scope:   this " << endl
      << "            };" << endl
      << "      " << endl
      << "      // YAHOO.util.Connect.asyncRequest('POST', url, callback, args);" << endl
      << "      // YAHOO.util.Connect.asyncRequest('POST', url, callback, \"value=\" + slider_var.getValue());" << endl
      << endl
      << "      // copy the args into a new array" << endl
      << "      // since we want to append an argument" << endl
      << "      var args_copy = new Array();" << endl
      << endl
      << "      for (var i in args)" << endl
      << "      {" << endl
      << "        args_copy[i] = args[i];" << endl
      << "        // YAHOO.log(\"i=\" + i); " << endl
      << "      }" << endl
      << endl
      << "      // add a parameter named 'value' whose value is the position of the slider" << endl
      << "      args_copy['value'] = getValueFunction();" << endl
      << endl
      << "      // put together these parameter name/parameter value pairs into an http encoded string" << endl
      << "      var data_string = \"\";" << endl
      << "      var is_first = 1;" << endl
      << "      for (i in args_copy)" << endl
      << "      {" << endl
      << "        if (is_first)" << endl
      << "          is_first = 0;" << endl
      << "        else" << endl
      << "          data_string = data_string + \"&\";" << endl
      << endl
      << "        data_string = data_string + " << endl
      << "           encodeURIComponent(i) + \"=\" + " << endl
      << "           encodeURIComponent(args_copy[i]);" << endl
      << "      }" << endl
      << endl
      << "      // YAHOO.log(\"encoded request: \" + data_string); " << endl
      << endl
      << "// the following (overriding of the default Content-Type) is necessary" << endl
      << "// to prevent CGICC from not finding ANY POST value filled (by default" << endl
      << "// yui seems to send 'application/x-www-form-urlencoded; charset=UTF-8'" << endl
      << "// which causes problems for CGICC" << endl
      << "YAHOO.util.Connect.setDefaultPostHeader(false);" << endl
      << "YAHOO.util.Connect.initHeader('Content-Type', 'application/x-www-form-urlencoded');" << endl
      << "      YAHOO.util.Connect.asyncRequest('POST', url, callback,data_string);" << endl
      << "      }" << endl
      << "   ;" << endl
      << endl
      << "  return retval;" << endl
      << "}" << endl;

  return buf.str();
}


string JavaScriptUtils::MakeSlider()
{
  ostringstream buf;
  buf
      << "function MakeSlider(name,initial_value,min_value,max_value) {" << endl
      << "        var sliderID      = \"slider_\"          + name;" << endl
      << "        var handleID      = \"handle_\"          + name;" << endl
      << "        var fillID        = \"left_fill_\"       + name;" << endl
      << "        var textFieldName = \"sliderTextField_\" + name;" << endl
      << endl
      << endl
      << "        var slider = YAHOO.widget.Slider.getHorizSlider(sliderID, handleID, min_value, max_value);" << endl
      << "        slider.setValue(initial_value);" << endl
      << endl
      << "        this.onChange = function(offset) {" << endl
      << "                Dom.get(fillID).style.width = offset + 'px';" << endl
      << "        }" << endl
      << "        " << endl
      << "        // this changes the text field when the slider is moved" << endl
      << "        this.updateTextField = function()" << endl
      << "        {" << endl
      << "          var text_field = Dom.get(textFieldName);" << endl
      << "          if (text_field != undefined)" << endl
      << "            text_field.value = slider.getValue();" << endl
      << "        }" << endl
      << endl
      << "        // this is called if a new value has been entered" << endl
      << "        // into the text field and the slider needs to be" << endl
      << "        // updated accordingly" << endl
      << "        this.updateSliderValue = function()" << endl
      << "        {" << endl
      << "          var v = parseFloat(Dom.get(textFieldName).value,10);" << endl
      << "          if (isNaN(v)) v = 0;" << endl
      << "          slider.setValue(v);" << endl
      << "          return false;" << endl
      << "        }" << endl
      << endl
      << "        slider.updateSliderValue = this.updateSliderValue;" << endl
      << endl
      << "        slider.subscribe(\"change\", this.onChange);" << endl
      << "        slider.subscribe(\"slideEnd\", this.updateTextField);" << endl
      << endl
      << endl
      << "        slider.keyIncrement = 1;" << endl
      << endl
      << "        return slider;" << endl
      << "}" << endl
      << endl;

  return buf.str();
}


string JavaScriptUtils::setSliderSubmitCommand()
{
  ostringstream buf;

  buf
      << "function setSliderSubmitCommand(slider,submitCommand)" << endl
      << "{" << endl
      << "  slider.subscribe(\"slideEnd\", submitCommand);" << endl
      << "}" << endl
      << endl;

  return buf.str();
}


string JavaScriptUtils::makeCheckButton()
{
  ostringstream buf;
  buf
      << "// produces a button which can be 'switched on and off'" << endl
      << "//" << endl
      << "// html_element_id " << endl
      << "// isChecked is the initial state" << endl
      << "// labelChecked   is the label which the button should bear" << endl
      << "//                when it is in checked state" << endl
      << "// labelUnchecked is the label which the button should bear" << endl
      << "//                when it is in checked state" << endl
      << "// callBack       the function which is called" << endl
      << "//                when the state of the button was changed" << endl
      << "function makeCheckButton(html_element_id,isChecked,labelChecked,labelUnchecked,theCallBack)" << endl
      << "{" << endl
      << "  var initial_label;" << endl
      << "  if (isChecked)" << endl
      << "    initial_label = labelChecked;" << endl
      << "  else" << endl
      << "    initial_label = labelUnchecked;" << endl
      << endl
      << "  var the_button = new YAHOO.widget.Button(" << endl
      << "                        html_element_id, " << endl
      << "                        { " << endl
      << "                            type: \"checkbox\", " << endl
      << "                            checked: isChecked," << endl
      << "                            label: initial_label" << endl
      << "                        }" << endl
      << "                    );" << endl
      << endl
      << "  // avoid calls to the 'checkedChange' listener when we set the value" << endl
      << "  // 'by hand'" << endl
      << "  //the_button.addListener(\"beforeCheckedChange\", function() { return false; }); " << endl
      << endl
      << "  the_button.addListener(\"checkedChange\",function(e) {" << endl
      << "    if (e.newValue)" << endl
      << "      the_button.set('label',labelChecked);" << endl
      << "    else" << endl
      << "      the_button.set('label',labelUnchecked);" << endl
      << endl
      << "    theCallBack(e.newValue);" << endl
      << "    });" << endl
      << endl
      << "  return the_button;" << endl
      << "}" << endl;

  return buf.str();
}


string JavaScriptUtils::getSliderIncrementButtonCommonCode()
{
  ostringstream buf;
  buf
      << "<script type=\"text/javascript\">" << endl
      << "function makeSliderIncrementButton(slider, html_span_id, button_label, increment)" << endl
      << "{" << endl
      << "  var the_button = new YAHOO.widget.Button(" << endl
      << "                          html_span_id, " << endl
      << "                        { " << endl
      << "                            type: \"button\", " << endl
      << "                            label: button_label" << endl
      << "                        }" << endl
      << "                    );" << endl
      << endl
      << "  var callback = function(e)" << endl
      << "  {" << endl
      << "    slider.setValue(slider.getValue() + increment);" << endl
      << "  }" << endl
      << endl
      << "  the_button.addListener(\"click\", callback);" << endl
      << "  " << endl
      << "  return the_button;" << endl
      << endl
      << "}" << endl
      << endl
      << "</script>" << endl;

  return buf.str();
}


string JavaScriptUtils::TextFieldWithIncrementButtons()
{
  ostringstream buf;

  buf
      << " /** Define a class to handle requests and store information related to a text field" << endl
      << "    with increment / decrement buttons. " << endl
      << endl
      << "    @param submit_func_url and submit_func_args are passed to the function makeSubmitCommand(..)" << endl
      << endl
      << "    Note: To me it is a mystery when one has to use this. and when not for member functions..." << endl
      << "*/" << endl
      << "function TextFieldWithIncrementButtons(text_field_id," << endl
      << "                                       min_value," << endl
      << "                                       max_value," << endl
      << "                                       submit_func_url," << endl
      << "                                       submit_func_args)" << endl
      << "{" << endl
      << "  // we can not call Dom.get(text_field_id) here because this function"  << endl
      << "  // might be called before the actual text field is encountered in the" << endl
      << "  // html (because we need to use the result of this function" << endl
      << "  // [the submit callback] before the text field is declared, we need" << endl
      << "  // to defer calling Dom.get(text_field_id) " << endl
      << endl
      << "  this.text_field_id = text_field_id;" << endl
      << endl
      << "  this.min_value = min_value;" << endl
      << "  this.max_value = max_value;" << endl
      << endl
      << "  //----------------------------------------" << endl
      << endl
      << "  function getTextField()" << endl
      << "  {" << endl
      << "    //  alert(\"HHH \" + Dom.get(text_field_id));" << endl
      << "      return Dom.get(text_field_id);" << endl
      << "  }" << endl
      << endl
      << "  //----------------------------------------" << endl
      << endl
      << "  /** This returns:" << endl
      << endl
      << "         - the given value if it is in the range of this control" << endl
      << "         - the minimum or maximum value if this is not the case." << endl
      << "    */" << endl
      << "  function limitValueToRange(value)" << endl
      << "  {" << endl
      << "    if (isNaN(value))" << endl
      << "      return min_value;" << endl
      << endl
      << "    if (value < min_value)" << endl
      << "      return min_value; " << endl
      << endl
      << "    if (value > max_value)" << endl
      << "      return max_value; " << endl
      << endl
      << "    return value;" << endl
      << "  }" << endl
      << "  //----------------------------------------" << endl
      << endl
      << "  function putValueInRange()" << endl
      << "    {" << endl
      << "      // reads the value from the text field, checks whether it is " << endl
      << "      // in the range" << endl
      << endl
      << "      var old_value = parseFloat(getTextField().value,10);" << endl
      << endl
      << "      var new_value = limitValueToRange(old_value);" << endl
      << "      // alert(\"value_changed=\" + value_changed + \" value=\" + value + \" max=\" + max_value + \" min=\" + min_value);" << endl
      << endl
      << "      if (new_value != old_value)" << endl
      << "        getTextField().value = new_value;" << endl
      << "    }" << endl
      << endl
      << "  //----------------------------------------" << endl
      << endl
      << "  function getValueFunction()" << endl
      << "    {" << endl
      << "      // 'sanitize' the value inside the text field" << endl
      << "      // alert(\"GKLGLGL \" + putValueInRange);" << endl
      << "      putValueInRange();" << endl
      << "      return parseFloat(getTextField().value,10);" << endl
      << "    }" << endl
      << endl
      << "  //----------------------------------------" << endl
      << endl
      << "  /** a function which can be used as a callback to the text field's form" << endl
      << "      submission callback. */" << endl
      << "  function onSubmitCallBack()" << endl
      << "    {" << endl
      << "      // 'sanitize' the value inside the text field" << endl
      << "      // alert(\"BALLALA \" + putValueInRange);" << endl
      << "      putValueInRange();" << endl
      << endl
      << "      // submit the corresponding http command" << endl
      << "      this.submit_func();" << endl
      << endl
      << "      // alert(\"BALLALA2\");" << endl
      << endl
      << "      return false;           // to avoid the standard http form submission" << endl
      << "    }" << endl
      << endl
      << "  //----------------------------------------" << endl
      << endl
      << "  /** Generates a YUI button object which increases/decreases the value in the text field by the given amount." << endl
      << endl
      << "      @param html_span_id is the span in which the button is to be displayed (note that each span id must" << endl
      << "             be unique and there can be more than one button associated to each text field)." << endl
      << "    */" << endl
      << "  function makeIncrementButton(html_span_id," << endl
      << "                               button_label," << endl
      << "                               increment)" << endl
      << "  {" << endl
      << "    var parent_class = this;" << endl
      << "    var text_field = getTextField();" << endl
      << "    var the_button = new YAHOO.widget.Button(" << endl
      << "                            html_span_id, " << endl
      << "                          { " << endl
      << "                              type: \"button\", " << endl
      << "                              label: button_label" << endl
      << "                          }" << endl
      << "                      );" << endl
      << "  " << endl
      << "    // this function is called when the button is clicked" << endl
      << "    var callback = function(e)" << endl
      << "    {" << endl
      << "    //alert(e.target + \": \" + url);" << endl
      << endl
      << "      // get the old value from the text field" << endl
      << "      var value = parseFloat(parent_class.getTextField().value,10);" << endl
      << "      if (isNaN(value)) value = 0;" << endl
      << endl
      << "      // apply the increment corresponding to this button" << endl
      << "      value += increment;" << endl
      << endl
      << "      value = parent_class.limitValueToRange(value);" << endl
      << "      parent_class.getTextField().value = value;" << endl
      << endl
      << "      // submit the new value to the server" << endl
      << "      parent_class.submit_func();" << endl
      << endl
      << "      // return false;" << endl
      << "    }" << endl
      << "  " << endl
      << "    the_button.addListener(\"click\", callback);" << endl
      << "    " << endl
      << "    return the_button;" << endl
      << "  }" << endl
      << "  " << endl
      << "  " << endl
      << endl
      << "  //----------------------------------------" << endl
      << "  // for the methods" << endl
      << "  this.getTextField     = getTextField;" << endl
      << "  this.limitValueToRange = limitValueToRange;" << endl
      << "  this.putValueInRange  = putValueInRange;" << endl
      << "  this.getValueFunction = getValueFunction;" << endl
      << "  this.onSubmitCallBack = onSubmitCallBack;" << endl
      << "  this.makeIncrementButton = makeIncrementButton;" << endl
      << "  //----------------------------------------" << endl
      << endl
      << "  this.submit_func = makeSubmitCommand(submit_func_url,submit_func_args,this.getValueFunction);" << endl
      << endl
      << "}" << endl;

  return buf.str();
}
