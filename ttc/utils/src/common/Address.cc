#include "ttc/utils/Address.hh"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>


using namespace std;


namespace ttc
{

//! A class which says whether a given Address object matches a given name and module type.
class AddressFinder
{
public:
  AddressFinder(const string & name, Address::ModuleType _module_type)
  : n(name), module_type(_module_type) {}

  bool operator()(Address* a) const
  {
    return a->GetModuleType() == module_type && a->GetName() == n;
  }
private:
  //! Name we're looking for.
  string n;
  //! Module type we are looking for.
  Address::ModuleType module_type;
};

}


// class ttc::Address

const string ttc::Address::dummy_register_name = "???";


ttc::Address&
ttc::Address::findByName(Address::ModuleType module_type, const string &name)
{
  vector<Address*>::const_iterator it = find_if(
      GetAllKnownAddresses().begin(),
      GetAllKnownAddresses().end(),
      AddressFinder(name, module_type));

  if (it == GetAllKnownAddresses().end())
  {
    cout
        << "AddressFinder::findByName(name='" << name << "'): "
        << "size of list = " << GetAllKnownAddresses().size() << endl;

    int i = 0;
    for (vector<Address*>::const_iterator
        it = GetAllKnownAddresses().begin();
        it != GetAllKnownAddresses().end(); ++it)
    {
      cout << i++ << " " << (*it)->GetName() << endl;
    }

    throw invalid_argument("Register " + name + " not found in GenericTTCModule::findAddressByName()");
  }

  return **it;
}


vector<ttc::Address*>
ttc::Address::getAllAddresses(ModuleType module_type)
{
  vector<Address*> retval;

  for (vector<Address*>::const_iterator
      it = GetAllKnownAddresses().begin();
      it != GetAllKnownAddresses().end(); ++it)
  {
    if ((*it)->module_type == module_type)
      retval.push_back(*it);
  }

  return retval;
}


std::vector<ttc::Address*>&
ttc::Address::GetAllKnownAddresses()
{
  static std::vector<Address*> all_known_addresses;
  return all_known_addresses;
}


ttc::Address::Address()
:
    HAL::VMEHardwareAddress(0, 0, 0),
    name(dummy_register_name),
    rw(NONE),
    nelements(0),
    step(0),
    module_type(NOMODULE)
{}


ttc::Address::Address(
    ModuleType _module_type,
    const string& name,
    uint32_t add,
    uint32_t am,
    ReadWrite rw,
    uint32_t width,
    uint32_t nelem,
    uint32_t step)
:
    HAL::VMEHardwareAddress(add, am, width),
    name(name),
    rw(rw),
    nelements(nelem),
    step(step),
    module_type(_module_type)
{
  registerMe();
}


ttc::Address::Address(
    ModuleType _module_type,
    const string& name,
    const Address& base,
    uint32_t offset,
    uint32_t nelem,
    uint32_t step)
:
    HAL::VMEHardwareAddress(
        base.getAddress() + offset,
        base.getAddressModifier(),
        base.getDataWidth()),
    name(name),
    rw(base.rw),
    nelements(nelem),
    step(step),
    module_type(_module_type)
{
  registerMe();
}


ttc::Address::Address(const string& name, const Address& base)
:
    HAL::VMEHardwareAddress(
        base.getAddress(),
        base.getAddressModifier(),
        base.getDataWidth()),
    name(name),
    rw(base.rw),
    nelements(base.nelements),
    step(base.step)
{
  registerMe();
}


bool ttc::Address::operator==(const Address& add2) const
{
  return name == add2.name;
}


bool ttc::Address::operator!=(const Address& add2) const
{
  return !(*this == add2);
}


bool ttc::Address::isReadable() const
{
  return rw & RO;
}


bool ttc::Address::isWriteable() const
{
  return rw & WO;
}


std::string ttc::Address::GetName() const
{
  return name;
}


ttc::Address::ModuleType
ttc::Address::GetModuleType() const
{
  return module_type;
}


HAL::VMEHardwareAddress
ttc::Address::VMEAddress(uint32_t index) const
{
  if (index < nelements)
  {
    return HAL::VMEHardwareAddress(getAddress() + step * index, getAddressModifier(), getDataWidth());
  }
  else
  {
    ostringstream myerr;
    myerr
        << "ERROR: Address::VMEAddress(index=" << index << ") "
        << "Index out of range (max = " << (nelements - 1) << ") "
        << "name: " << this->GetName();
    throw out_of_range(myerr.str());
  }
}


string ttc::Address::GetDescription() const
{
  ostringstream s;
  s << GetName() << ' ' << GetRWStatus();
  if (nelements > 1)
  {
    s << " [" << nelements << "]";
  }
  return s.str();
}


string ttc::Address::GetRWStatus() const
{
  if (rw == RW)
    return "RW";
  else if (rw == RO)
    return "R ";
  else if (rw == WO)
    return "W ";
  else if (rw == NONE)
    return "NONE";
  else
    return "ERROR in Address::GetRWStatus()!";
}

uint32_t ttc::Address::GetNum() const
{
  return nelements;
}


void ttc::Address::registerMe()
{
  // search for an existing Address with this name and ModuleType...
  vector<Address*>::const_iterator it = find_if(
          GetAllKnownAddresses().begin(),
          GetAllKnownAddresses().end(),
          AddressFinder(GetName(), GetModuleType()));

  // complain if an identically named item exists AND uses a different address
  if (it != GetAllKnownAddresses().end())
  {
    if (*it != this)
    {
      ostringstream buf;
      buf
          << "in " << __PRETTY_FUNCTION__ << ": "
          << "error: trying to declare a register named '" << GetName() << "' "
          << "more than once with different addresses in memory";

      throw invalid_argument(buf.str());
    }
  }
  // else register this
  else
    GetAllKnownAddresses().push_back(this);
}


string ttc::Address::GetModuleDescription() const
{
  switch (module_type)
  {
    case NOMODULE:
      return "(not set)";

    case LTC:
      return "LTC";

    case TTCci:
      return "TTCci";

    default:
      return "(unknown module type)";
  }
}
