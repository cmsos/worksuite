#ifndef _ttc_ltc_version_h_
#define _ttc_ltc_version_h_

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version !!!
#define WORKSUITE_TTCLTC_VERSION_MAJOR 1
#define WORKSUITE_TTCLTC_VERSION_MINOR 7
#define WORKSUITE_TTCLTC_VERSION_PATCH 1
#undef WORKSUITE_TTCUTILS_PREVIOUS_VERSIONS

#define WORKSUITE_TTCLTC_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TTCLTC_VERSION_MAJOR,WORKSUITE_TTCLTC_VERSION_MINOR,WORKSUITE_TTCLTC_VERSION_PATCH)
#ifndef WORKSUITE_TTCLTC_PREVIOUS_VERSIONS
#define WORKSUITE_TTCLTC_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TTCLTC_VERSION_MAJOR,WORKSUITE_TTCLTC_VERSION_MINOR,WORKSUITE_TTCLTC_VERSION_PATCH)
#else
#define WORKSUITE_TTCLTC_FULL_VERSION_LIST  TTCLTC_VERSION_MAJOR "," TTCLTC_VERSION_MINOR "," TTCLTC_VERSION_PATCH
#endif

namespace ttcltc {
	const std::string project = "worksuite";
  const std::string package  = "ttcltc";
  const std::string versions = WORKSUITE_TTCLTC_FULL_VERSION_LIST;
  const std::string description = "Local Trigger Control XDAQ application";
  const std::string authors = "CMS DAQ group";
  const std::string summary = "LTC XDAQ application";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TTCManual";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
