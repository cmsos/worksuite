#include "ttc/ltc/LTCStatusInfo.hh"

#include <stdint.h>
#include <iostream>


using namespace std;


ttc::LTCStatusInfo::LTCStatusInfo()
:
    _tupdate(1, time_t(0)),
    _evt0(1, 0),
    _evt1(1, 0),
    _blocked0(1, 0),
    _blocked1(1, 0),
    _cancelled0(1, 0),
    _cancelled1(1, 0),
    _bgo0(1, 0),
    _bgo1(1, 0),
    _orb0(1, 0),
    _orb1(1, 0)
{}


void ttc::LTCStatusInfo::Set(
    uint32_t orbit,
    uint32_t evtnum,
    uint32_t blockedL1As,
    uint32_t nbgos,
    uint32_t ncancelled)
{
  const size_t n = _tupdate.size();

  if (_tupdate.size() > N()
      || _evt0.size() > n
      || _evt1.size() > n
      || _blocked0.size() > n
      || _blocked1.size() > n
      || _cancelled0.size() > n
      || _cancelled1.size() > n
      || _bgo0.size() > n
      || _bgo1.size() > n
      || _orb0.size() > n
      || _orb1.size() > n)
  {
    cout
        << "ERROR: LTCStatusInfo::Set:"
        << " _tupdate.size()=" << _tupdate.size()
        << " _evt0.size()=" << _evt0.size()
        << " _evt1.size()=" << _evt1.size()
        << " _blocked0.size()=" << _blocked0.size()
        << " _blocked1.size()=" << _blocked1.size()
        << " _cancelled0.size()=" << _cancelled0.size()
        << " _cancelled1.size()=" << _cancelled1.size()
        << " _bgo0.size()=" << _bgo0.size()
        << " _bgo1.size()=" << _bgo1.size()
        << " _orb0.size()=" << _orb0.size()
        << " _orb1.size()=" << _orb1.size() << endl;
  }

  if (n >= N())
  {
    // delete the first element of each of the vectors to make space
    _tupdate.erase(_tupdate.begin(), _tupdate.begin() + 1);
    _evt0.erase(_evt0.begin(), _evt0.begin() + 1);
    _evt1.erase(_evt1.begin(), _evt1.begin() + 1);
    _blocked0.erase(_blocked0.begin(), _blocked0.begin() + 1);
    _blocked1.erase(_blocked1.begin(), _blocked1.begin() + 1);
    _cancelled0.erase(_cancelled0.begin(), _cancelled0.begin() + 1);
    _cancelled1.erase(_cancelled1.begin(), _cancelled1.begin() + 1);
    _bgo0.erase(_bgo0.begin(), _bgo0.begin() + 1);
    _bgo1.erase(_bgo1.begin(), _bgo1.begin() + 1);
    _orb0.erase(_orb0.begin(), _orb0.begin() + 1);
    _orb1.erase(_orb1.begin(), _orb1.begin() + 1);
  }

  _tupdate.push_back(time_t());
  time(&_tupdate[_tupdate.size() - 1]);

  // Orbit:
  _orb0.push_back(*(_orb1.end() - 1));
  _orb1.push_back(orbit);

  // Evt Number:
  _evt0.push_back(*(_evt1.end() - 1));
  _evt1.push_back(evtnum);

  // Blocked triggers:
  _blocked0.push_back(*(_blocked1.end() - 1));
  _blocked1.push_back(blockedL1As);

  // BGOs:
  _bgo0.push_back(*(_bgo1.end() - 1));
  _bgo1.push_back(nbgos);

  // Cancelled trigs:
  _cancelled0.push_back(*(_cancelled1.end() - 1));
  _cancelled1.push_back(ncancelled);

  if (*(_orb1.end() - 1) <= *(_orb0.end() - 1))
  {
    // e.g. after a reset

    size_t m = _orb1.size() - 1;
    _orb0[m] = _orb1[m];
    _evt0[m] = _evt1[m];
    _blocked0[m] = _blocked1[m];
    _bgo0[m] = _bgo1[m];
    _cancelled0[m] = _cancelled1[m];
  }
}


void ttc::LTCStatusInfo::SetTriggerComposition(const TriggerComposition *Composition)
{
  _comp = *Composition;
}


const ttc::TriggerComposition*
ttc::LTCStatusInfo::GetTriggerComposition() const
{
  return (TriggerComposition *) &_comp;
}


double ttc::LTCStatusInfo::DeltaT() const
{
  const unsigned bunch_crossings_per_orbit = 3564;
  const double time_between_bunch_crossings = 25.0E-9; // in units of seconds

  const size_t m = _orb1.size() - 1;
  return (double(_orb1[m] - _orb0[m]) * double(bunch_crossings_per_orbit) * time_between_bunch_crossings);
}


double ttc::LTCStatusInfo::DeltaT_Accumulated() const
{
  size_t i0 = 0, i1 = 0;
  return DeltaT_Accum(i0, i1);
}


double ttc::LTCStatusInfo::L1ARate() const
{
  const size_t m = _orb1.size() - 1;
  if (_orb1[m] - _orb0[m] > 0)
  {
    return (double(_evt1[m] - _evt0[m]) / DeltaT());
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::L1ARate_Accumulated() const
{
  size_t i0 = 0, i1 = 0;
  const double dt = DeltaT_Accum(i0, i1);
  if (_orb1[i1] - _orb0[i0] > 0)
  {
    return (double(_evt1[i1] - _evt0[i0]) / dt);
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::RawTriggerRate() const
{
  const size_t m = _orb1.size() - 1;
  if (_orb1[m] - _orb0[m] > 0)
  {
    return (double((_evt1[m] + _blocked1[m]) - (_evt0[m] + _blocked0[m])) / DeltaT());
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::RawTriggerRate_Accumulated() const
{
  size_t i0 = 0, i1 = 0;
  const double dt = DeltaT_Accum(i0, i1);
  if (_orb1[i1] - _orb0[i0] > 0)
  {
    return (double((_evt1[i1] + _blocked1[i1]) - (_evt0[i0] + _blocked0[i0])) / dt);
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::BlockedTriggerRate() const
{
  const size_t m = _orb1.size() - 1;
  if (_orb1[m] - _orb0[m] > 0)
  {
    return (double(_blocked1[m] - _blocked0[m]) / DeltaT());
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::BlockedTriggerRate_Accumulated() const
{
  size_t i0 = 0, i1 = 0;
  const double dt = DeltaT_Accum(i0, i1);
  if (_orb1[i1] - _orb0[i0] > 0)
  {
    return (double(_blocked1[i1] - _blocked0[i0]) / dt);
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::CancelledRate() const
{
  const size_t m = _orb1.size() - 1;
  if (_orb1[m] - _orb0[m] > 0)
  {
    return (double(_cancelled1[m] - _cancelled0[m]) / DeltaT());
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::CancelledRate_Accumulated() const
{
  size_t i0 = 0, i1 = 0;
  const double dt = DeltaT_Accum(i0, i1);
  if (_orb1[i1] - _orb0[i0] > 0)
  {
    return (double(_cancelled1[i1] - _cancelled0[i0]) / dt);
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::Efficiency() const
{
  const size_t m = _orb1.size() - 1;
  if (_orb1[m] - _orb0[m] > 0 && (_blocked1[m] - _blocked0[m] + _evt1[m] - _evt0[m]) > 0)
  {
    return (double(_evt1[m] - _evt0[m]) / double(_blocked1[m] - _blocked0[m] + _evt1[m] - _evt0[m]));
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::Efficiency_Accumulated() const
{
  size_t i0 = 0, i1 = 0;
  DeltaT_Accum(i0, i1);
  if (_orb1[i1] - _orb0[i0] > 0 && (_blocked1[i1] - _blocked0[i0] + _evt1[i1] - _evt0[i0]) > 0)
  {
    return (double(_evt1[i1] - _evt0[i0]) / double(_blocked1[i1] - _blocked0[i0] + _evt1[i1] - _evt0[i0]));
  }
  else
    return 0.0;
}


size_t ttc::LTCStatusInfo::NAccumulations() const
{
  return N();
}


double ttc::LTCStatusInfo::BGORate() const
{
  const size_t m = _orb1.size() - 1;
  if (_orb1[m] - _orb0[m] > 0)
  {
    return (double(_bgo1[m] - _bgo0[m]) / DeltaT());
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::BGORate_Accumulated() const
{
  size_t i0 = 0, i1 = 0;
  const double dt = DeltaT_Accum(i0, i1);
  if (_orb1[i1] - _orb0[i0] > 0)
  {
    return (double(_bgo1[i1] - _bgo0[i0]) / dt);
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::L1AsPerOrbit() const
{
  const size_t m = _orb1.size() - 1;
  if (_orb1[m] - _orb0[m] > 0)
  {
    return (double(_evt1[m] - _evt0[m]) / double(_orb1[m] - _orb0[m]));
  }
  else
    return 0.0;
}


double ttc::LTCStatusInfo::BGOsPerOrbit() const
{
  const size_t m = _orb1.size() - 1;
  if (_orb1[m] - _orb0[m] > 0)
  {
    return (double(_bgo1[m] - _bgo0[m]) / double(_orb1[m] - _orb0[m]));
  }
  else
    return 0.0;
}


uint32_t ttc::LTCStatusInfo::OrbitDiff() const
{
  return (*(_orb1.end() - 1) - (*(_orb0.end() - 1)));
}


uint32_t ttc::LTCStatusInfo::L1ADiff() const
{
  return (*(_evt1.end() - 1) - (*(_evt0.end() - 1)));
}


uint32_t ttc::LTCStatusInfo::RawDiff() const
{
  return (*(_evt1.end() - 1) - (*(_evt0.end() - 1)) + (*(_blocked1.end() - 1)) - (*(_blocked0.end() - 1)));
}


uint32_t ttc::LTCStatusInfo::BGODiff() const
{
  return ((*(_bgo1.end() - 1)) - (*(_bgo0.end() - 1)));
}


void ttc::LTCStatusInfo::Print() const
{
  size_t i0 = 0, i1 = 0;
  DeltaT_Accum(i0, i1);

  cout
      << "DeltaT_Accum(): i0/1=" << i0 << "/" << i1
      << " _orb1.size()=" << _orb1.size() << endl;

  for (size_t k = 0; k < _orb1.size(); ++k)
  {
    cout
        << "  _orb=" << _orb0[k] << "/" << _orb1[k]
        << "  _evt=" << _evt0[k] << "/" << _evt1[k]
        << "  _blocked=" << _blocked0[k] << "/" << _blocked1[k] << endl;
  }

  cout
      << "  --> DT=" << DeltaT_Accumulated()
      << " Eff=" << Efficiency_Accumulated()
      << " l1arate=" << L1ARate_Accumulated() << endl;
}


size_t ttc::LTCStatusInfo::N() const
{
  return 5;
}


double ttc::LTCStatusInfo::DeltaT_Accum(size_t &i0, size_t &i1) const
{
  i1 = _orb1.size() - 1;
  i0 = i1;

  for (int j = i1; j >= 0; --j)
  {
    if (_orb0[j] == 0 || int(_orb1[i1]) <= int(_orb0[j]) || _orb1[j] <= _orb0[j])
    {
      break;
    }
    i0 = size_t(j);
  }

  return (double(_orb1[i1] - _orb0[i0]) * 3564.0 * 25.0E-9);
}


time_t ttc::LTCStatusInfo::Time() const
{
  return *(_tupdate.end() - 1);
}
