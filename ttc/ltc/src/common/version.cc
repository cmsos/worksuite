#include "ttc/ltc/version.h"

#include "ttc/utils/version.h"
#include "config/version.h"
#include "toolbox/version.h"
#include "xdata/version.h"
#include "xgi/version.h"

GETPACKAGEINFO(ttcltc)

void ttcltc::checkPackageDependencies()
{
  CHECKDEPENDENCY(ttcutils);
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xgi);
}

std::set<std::string, std::less<std::string> > ttcltc::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;
  ADDDEPENDENCY(dependencies, ttcutils);
  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xdata);
  ADDDEPENDENCY(dependencies, xgi);

  return dependencies;
}
