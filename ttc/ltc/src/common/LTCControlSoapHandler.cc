#include "ttc/ltc/LTCControlSoapHandler.hh"

#include "ttc/ltc/LTC.hh"
#include "ttc/ltc/LTCControl.hh"
#include "ttc/utils/TTCXDAQBase.hh"

#include "xoap/Method.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/MessageFactory.h"
#include "xcept/tools.h"
#include "xdaq/NamespaceURI.h"


using namespace std;


ttc::LTCControlSoapHandler::LTCControlSoapHandler(LTCControl* _ltc_control)
:
    ltc_control(_ltc_control),
    logger_(
        log4cplus::Logger::getInstance(ltc_control->getApplicationLogger().getName() + ".LTCControlSoapHandler"))
{
  xoap::bind(this, &LTCControlSoapHandler::userCommand, "ExecuteSequence", XDAQ_NS_URI);
  xoap::bind(this, &LTCControlSoapHandler::userCommand, "ResetCounters", XDAQ_NS_URI);
  xoap::bind(this, &LTCControlSoapHandler::userCommand, "VMETrigger", XDAQ_NS_URI);
  xoap::bind(this, &LTCControlSoapHandler::userCommand, "VMEBGO", XDAQ_NS_URI);
  xoap::bind(this, &LTCControlSoapHandler::userCommand, "L1ATicket", XDAQ_NS_URI);
  xoap::bind(this, &LTCControlSoapHandler::userCommand, "Cyclic", XDAQ_NS_URI);
  xoap::bind(this, &LTCControlSoapHandler::userCommand, "DumpVMEHistory", XDAQ_NS_URI);
  xoap::bind(this, &LTCControlSoapHandler::userCommand, "DumpEventFIFO", XDAQ_NS_URI);
}


void ttc::LTCControlSoapHandler::addMethod(toolbox::lang::Method* m, string name)
{
  ltc_control->addMethod(m, name);
}


xoap::MessageReference
ttc::LTCControlSoapHandler::userCommand(xoap::MessageReference msg)
{
  string cmd;

  try
  {
    string msgStr;
    msg->writeTo(msgStr);
    LOG4CPLUS_INFO(
        logger_,
        "LTCControlSoapHandler::userCommand: SOAP message received:" << endl
        << msgStr);

    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    vector<xoap::SOAPElement> elems = envelope.getBody().getChildElements();

    for (size_t i = 0; i < elems.size(); ++i)
    {
      xoap::SOAPElement& elem = elems[i];

      // only consider "real" elements,
      // i.e. ignore, text not embedded in tags, and comments
      if (elem.getDOMNode()->getNodeType() != DOMNode::ELEMENT_NODE)
        continue;

      xoap::SOAPName cmdname = elem.getElementName();
      cmd = cmdname.getLocalName();

      xoap::SOAPName paramname("Param", "xdaq", XDAQ_NS_URI);

      string param;
      unsigned nparam = 0;

      param = elem.getAttributeValue(paramname);
      nparam = atoi(param.c_str());

      // Execute a sequence via SOAP.

      if (cmd == "ExecuteSequence")
      {
        LOG4CPLUS_INFO(
            logger_,
            "LTCControlSoapHandler::userCommand: "
            "Calling LTC::ExecuteSequence(" << param << ")");

        ltc_control->boardLockingProxy()->ExecuteSequence(param);
      }

      else if (cmd == "ResetCounters")
      {
        LOG4CPLUS_INFO(
            logger_,
            "LTCControlSoapHandler::userCommand: "
            "Calling LTC::ResetCounters(" << nparam << ")");

        ltc_control->boardLockingProxy()->ResetCounters(nparam);
      }

      else if (cmd == "VMETrigger")
      {
        ltc_control->boardLockingProxy()->ExecuteVMETrigger();
      }

      else if (cmd == "VMEBGO")
      {
        if (nparam == 0)
        {
          for (size_t i = 1; i < ltc_control->boardLockingProxy()->NChannels(); ++i)
          {
            if (param == ltc_control->boardLockingProxy()->GetBGOChannel(i)->GetName())
            {
              nparam = i;
              break;
            }
          }
        }
        ltc_control->boardLockingProxy()->ExecuteVMEBGO(nparam);
      }

      else if (cmd == "L1ATicket")
      {
        if (nparam != 0)
        {
          ltc_control->boardLockingProxy()->SetL1ATicket(nparam);
        }
        ltc_control->boardLockingProxy()->SendL1ATicket();
      }

      else if (cmd == "Cyclic")
      {
        if (param == "Start")
        {
          ltc_control->boardLockingProxy()->StartCyclicGenerators();
        }
        else if (param == "Stop")
        {
          ltc_control->boardLockingProxy()->StopCyclicGenerators();
        }
        else if (param == "StartPermanent")
        {
          ltc_control->boardLockingProxy()->StartPermanentCyclicGenerators();
        }
        else if (param == "StopAll")
        {
          ltc_control->boardLockingProxy()->StopAllCyclicGenerators();
        }
        else
        {
          XCEPT_RAISE(
              xcept::Exception,
              "LTCControlSoapHandler::userCommand: Cyclic: "
              "Unknown parameter '" + param + "'");
        }
      }

      else if (cmd == "DumpVMEHistory")
      {
        ltc_control->boardLockingProxy()->PrintVMEHistory();
      }

      else if (cmd == "DumpEventFIFO")
      {
        ltc_control->boardLockingProxy()->DumpEventFIFO();
      }

      else
      {
        XCEPT_RAISE(
            xcept::Exception,
            "LTCControlSoapHandler::userCommand: Unknown command: " + cmd);
      }
    }

    return ttc::createSOAPReplyCommand("LTCResponse");
  }
  catch (xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(xcept::Exception, q,
        string("SOAP callback userCommand for command '" + cmd + "' failed"), e);
    return ltc_control->failSoapCallback(e);
  }
  catch (std::exception& e)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        string("SOAP callback userCommand for command '" + cmd + "' failed with std::exception: " + e.what()));
    return ltc_control->failSoapCallback(q);
  }
  catch (...)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        string("SOAP callback userCommand for command '" + cmd + "' failed with unknown exception"));
    return ltc_control->failSoapCallback(q);
  }
}
