#include "ttc/ltc/LTCControl.hh"

#include "ttc/ltc/LTC.hh"
#include "ttc/ltc/LTCAddresses.hh"
#include "ttc/ltc/LTCControlSoapHandler.hh"
#include "ttc/ltc/version.h"
#include "ttc/utils/TTSInfo.hh"
#include "ttc/utils/CgiUtils.hh"
#include "ttc/utils/HTMLTable.hh"
#include "ttc/utils/HTMLMacros.hh"
#include "ttc/utils/Utils.hh"
#include "ttc/utils/RAMTriggers.hh"

#include "xgi/Utils.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPHTMLHeader.h"

#include "toolbox/fsm/exception/Exception.h"
#include "toolbox/regex.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"

#include <boost/lexical_cast.hpp>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>


using namespace std;


// class ttc::LTCProxy

ttc::LTCProxy::LTCProxy()
:
    bTimeCorrection_(0),
    sLinkSrcId_(0)
{}


ttc::BoardTempLocker<ttc::LTC> ttc::LTCProxy::operator->()
{
  return BoardTempLocker<LTC>(*this);
}


void ttc::LTCProxy::initAdditionalParams(
    const uint32_t& bTimeCorrection,
    const uint32_t& sLinkSrcId)
{
  bTimeCorrection_ = bTimeCorrection;
  sLinkSrcId_ = sLinkSrcId;
}


ttc::LTC* ttc::LTCProxy::createPtr(HAL::VMEBusAdapterInterface* busAdapter)
{
  return new ttc::LTC(
        *busAdapter,
        boardSlot_,
        bTimeCorrection_,
        enableVMEWrite_,
        sLinkSrcId_);
}


// class ttc::LTCControl

XDAQ_INSTANTIATOR_IMPL(ttc::LTCControl);


ttc::LTCControl::LTCControl(xdaq::ApplicationStub* stub)
:
    TTCXDAQBase(stub, "LTCControl", "LTC"),
    // info space items
    FractionOfActiveBunches_(1.),
    BTimeCorrection_(DEFAULT_LTC_BTC),
    SlinkSrcId_(815),
    TTSStatus_("Unknown"),
    SLinkStatus_("Unknown"),
    TriggerCounter_(0),
    EventCounter_(0),
    TriggerInputs_("Undefined"),
    Efficiency_(0.),
    AvEfficiency_(0.),
    L1ARate_(0.),
    AvL1ARate_(0.),
    RawL1ARate_(0.),
    AvRawL1ARate_(0.),
    UptimeSec_(0),
    HWTriggerFraction_(6, 0.),
    HWTriggerName_(6),
    HWTriggerEnable_(6, false),
    HWTriggerDelay_(6, -999.),
    ClockSource_("Undefined"),
    ClockFrequency_(0),
    RunNumber_(0),
    // other members
    resetbits_(LTC_RESET_BGOS | LTC_RESET_ORBIT | LTC_RESET_EVENT | LTC_RESET_BLOCKED),
    OverwriteRunNumber_(false),
    soapHandler_(0)
{
  // info space items and listeners

  addItem("TimeOfLastStateChange",   TimeOfLastStateChange_); //< updated in push mode
  addItem("FractionOfActiveBunches", FractionOfActiveBunches_); //< updated in push mode

  addItem("BTimeCorrection",         BTimeCorrection_);
  addItem("SlinkSrcId",              SlinkSrcId_, true);  //< retrievable for backward compatibility

  addItemRetrievable("TTSStatus",          TTSStatus_);
  addItemRetrievable("SLinkStatus",        SLinkStatus_);
  addItemRetrievable("ClockSignal",        ClockSignal_);
  addItemRetrievable("OrbitSignal",        OrbitSignal_);

  addItemRetrievable("TriggerCounter",     TriggerCounter_);
  addItemRetrievable("EventID",            EventCounter_);
  addItemRetrievable("OrbitCounter",       OrbitCounter_);
  addItemRetrievable("StrobeCounter",      StrobeCounter_);
  addItemRetrievable("BlockedTriggers",    BlockedL1ACntr_);
  addItemRetrievable("BoardStatus",        BoardStatus_);

  addItemRetrievable("TriggerInputs",      TriggerInputs_);
  addItemRetrievable("TotalEfficiency",    TotalEfficiency_);
  addItemRetrievable("Efficiency",         Efficiency_);
  addItemRetrievable("AveragedEfficiency", AvEfficiency_);
  addItemRetrievable("L1ARate",            L1ARate_);
  addItemRetrievable("AveragedL1ARate",    AvL1ARate_);
  addItemRetrievable("RawL1ARate",         RawL1ARate_);
  addItemRetrievable("AveragedRawL1ARate", AvRawL1ARate_);
  addItemRetrievable("BlockedL1ARate",     BlockedL1ARate_);

  addItemRetrievable("DeltaT",             DeltaT_);
  addItemRetrievable("UptimeSec",          UptimeSec_);

  for (size_t i = 0; i<6; ++i)
  {
    string s = boost::lexical_cast<string>(i);
    addItemRetrievable          ("TriggerFraction"+s, HWTriggerFraction_[i]);
    addItemRetrievableChangeable("TriggerName"+s,     HWTriggerName_[i]);
    addItemRetrievableChangeable("TriggerEnable"+s,   HWTriggerEnable_[i]);
    addItemRetrievableChangeable("TriggerDelay"+s,    HWTriggerDelay_[i]);
  }

  addItemRetrievableChangeable("ClockSource",    ClockSource_);
  addItemRetrievableChangeable("ClockFrequency", ClockFrequency_);

  addItem("RunNumber", RunNumber_, false, true); //< no ItemRetrieveEvent listener, updated in ItemChangeEvent only

  // Bind SOAP callbacks for control messages
  soapHandler_ = new LTCControlSoapHandler(this);

  // CGI bindings
  cgi_bind(this, &LTCControl::HTMLPageMainConfiguration,                "MainConfiguration", "Main Config");
  cgi_bind(this, &LTCControl::HTMLPageBGOConfiguration,                 "BGOConfiguration",  "VME");
  cgi_bind(this, &LTCControl::HTMLPageSequences,                        "Sequences",         "Sequences");
  cgi_bind(this, &LTCControl::HTMLPageCyclicGenerators,                 "CyclicGenerators",  "Cyclic Gen.");
  cgi_bind(this, &LTCControl::HTMLPageSummary,                          "SummaryPage",       "Summary");
  cgi_bind(this, &LTCControl::HTMLPageRegisterAccess,                   "RegisterAccess",    "Registers");
  cgi_bind(this, &LTCControl::HTMLPageRatesPopup,                       "ShowRatesPopup");
  cgi_bind(this, &LTCControl::HTMLPageCommandNewConfigFile,             "NewConfigurationFile");
  cgi_bind(this, &LTCControl::HTMLPageCommandWriteConfigFile,           "WriteConfigurationFile");
  cgi_bind(this, &LTCControl::HTMLPageCommandMainConfig,                "MainConfigCommand");
  cgi_bind(this, &LTCControl::HTMLPageCommandTriggerRulesDelays,        "TriggerRules");
  cgi_bind(this, &LTCControl::HTMLPageCommandVMEBGOTiming,              "BGOSelectCommand");
  cgi_bind(this, &LTCControl::HTMLPageCommandConfigureCyclicGenerator,  "CyclicConfigCommand");
  cgi_bind(this, &LTCControl::HTMLPageCommandReadCyclicGeneratorConfig, "ReadCyclicGeneratorsFromLTC");
  cgi_bind(this, &LTCControl::HTMLPageCommandConfigureTTS,              "TTSConfigure");
  cgi_bind(this, &LTCControl::HTMLPageCommandRegisterAccess,            "RegisterAccessCommand");
  cgi_bind(this, &LTCControl::HTMLPageSetControlRegisterBits,           "SetControlRegisterBits");
  cgi_bind(this, &LTCControl::HTMLPageCommandSequenceAction,            "SequenceSelectCommand");
  cgi_bind(this, &LTCControl::HTMLPageCommandEditSequence,              "SequenceEditCommand");
}


ttc::LTCControl::~LTCControl()
{
  if (soapHandler_)
  {
    delete soapHandler_;
  }
  soapHandler_ = 0;
}


ttc::LTCProxy& ttc::LTCControl::boardLockingProxy()
{
  return ltcProxy_;
}


string ttc::LTCControl::softwareVersion()
{
  ostringstream oss;
  oss << WORKSUITE_TTCLTC_VERSION_MAJOR << "." << WORKSUITE_TTCLTC_VERSION_MINOR << "." << WORKSUITE_TTCLTC_VERSION_PATCH;
  return oss.str();
}


void ttc::LTCControl::itemRetrieveAction(xdata::ItemRetrieveEvent& e)
{
  TTCXDAQBase::itemRetrieveAction(e);

  string itemName = e.itemName();

  if (itemName == "SlinkSrcId")
  {
    SlinkSrcId_ = boardLockingProxy()->GetSlinkSrcId();
  }

  else if (itemName == "TTSStatus")
  {
    size_t ttsmask;
    string statusSummary = boardLockingProxy()->TTSStatusSummary(ttsmask);
    TTSStatus_ = statusSummary + "(0x" + ttc::to_string(ttsmask, true) + ")";
  }

  else if (itemName == "SLinkStatus")
  {
    uint32_t stat = boardLockingProxy()->ReadSLinkStatus() & 0x3;
    SLinkStatus_ = (stat == 3 ? "Ready" : (stat == 1 ? "Full" : "LinkDown"));
    if (boardLockingProxy()->IsSlinkBackpressureIgnored())
    {
      SLinkStatus_ = string(SLinkStatus_.toString() + "(ignored)");
    }
  }

  else if (itemName == "ClockSignal")
  {
    stringstream mys;
    if (boardLockingProxy()->IsQPLLExternal())
    {
      mys << "EXTERN_";
      if (boardLockingProxy()->IsClockLocked())
        mys << "LOCKED";
      else
        mys << "NOT_LOCKED";
    }
    else
    {
      mys << "INTERN_0x" << hex << boardLockingProxy()->GetQPLLFrequencyBits() << dec;
    }
    ClockSignal_ = mys.str();
  }

  else if (itemName == "OrbitSignal")
  {
    OrbitSignal_ = (boardLockingProxy()->OrbitInSync() ? "OK" : "OutOfSync");
  }

  else if (itemName == "TriggerCounter")
  {
    TriggerCounter_ = boardLockingProxy()->ReadTriggerCounter();
  }
  else if (itemName == "EventID")
  {
    EventCounter_ = boardLockingProxy()->ReadEventCounter();
  }
  else if (itemName == "OrbitCounter")
  {
    OrbitCounter_ = boardLockingProxy()->ReadOrbitCounter();
  }
  else if (itemName == "StrobeCounter")
  {
    StrobeCounter_ = boardLockingProxy()->ReadStrobeCounter();
  }
  else if (itemName == "BlockedTriggers")
  {
    BlockedL1ACntr_ = boardLockingProxy()->ReadBlockedTriggersCounter();
  }
  else if (itemName == "BoardStatus")
  {
    BoardStatus_ = boardLockingProxy()->BoardStatus();
  }

  else if (itemName == "TriggerInputs")
  {
    TriggerInputs_ = "";
    size_t n = 0;
    stringstream t;
    for (size_t i = 0; i < boardLockingProxy()->Nextern(); ++i)
    {
      if (boardLockingProxy()->IsExternalTriggerEnabled(i))
      {
        ++n;
        if (n > 1)
        {
          t << ".or.";
        }
        t << boardLockingProxy()->GetTriggerName(i);
        t << "(HW" << i << ")";
      }
    }
    if (boardLockingProxy()->IsRAMTrigEnabled())
    {
      ++n;
      if (n > 1)
      {
        t << ".or.";
      }
      t << "Internal(";
      if (boardLockingProxy()->GetRAMTriggers()->CanSetInternalTriggerFrequency())
      {
        t << boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerFrequency() << "Hz ";
        t << (boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerRandom() ? "rndm" : "equi-dist");
      }
      else
      {
        t << "direct trig-RAM config.";
      }
      t << ")";
    }
    if (boardLockingProxy()->IsCyclicTrigEnabled())
    {
      ++n;
      if (n > 1)
      {
        t << ".or.";
      }
      t << "Internal(Cyclic)";
    }
    TriggerInputs_ = t.str();
  }

  else if (itemName == "TotalEfficiency")
  {
    TriggerCounter_ = boardLockingProxy()->ReadTriggerCounter();
    BlockedL1ACntr_ = boardLockingProxy()->ReadBlockedTriggersCounter();
    if (TriggerCounter_ + BlockedL1ACntr_ > 0)
    {
      TotalEfficiency_ = double(TriggerCounter_) / double(TriggerCounter_ + BlockedL1ACntr_);
    }
    else
    {
      TotalEfficiency_ = 0.0;
    }
  }
  else if (itemName == "Efficiency")
  {
    Efficiency_ = boardLockingProxy()->GetLTCStatusInfo()->Efficiency();
  }
  else if (itemName == "AveragedEfficiency")
  {
    AvEfficiency_ = boardLockingProxy()->GetLTCStatusInfo()->Efficiency_Accumulated();
  }
  else if (itemName == "L1ARate")
  {
    L1ARate_ = boardLockingProxy()->GetLTCStatusInfo()->L1ARate();
  }
  else if (itemName == "AveragedL1ARate")
  {
    AvL1ARate_ = boardLockingProxy()->GetLTCStatusInfo()->L1ARate_Accumulated();
  }
  else if (itemName == "RawL1ARate")
  {
    RawL1ARate_ = boardLockingProxy()->GetLTCStatusInfo()->RawTriggerRate();
  }
  else if (itemName == "AveragedRawL1ARate")
  {
    AvRawL1ARate_ = boardLockingProxy()->GetLTCStatusInfo()->RawTriggerRate_Accumulated();
  }
  else if (itemName == "BlockedL1ARate")
  {
    BlockedL1ARate_ = boardLockingProxy()->GetLTCStatusInfo()->BlockedTriggerRate();
  }

  else if (itemName == "DeltaT")
  {
    DeltaT_ = boardLockingProxy()->GetLTCStatusInfo()->DeltaT();
  }
  else if (itemName == "UptimeSec")
  {
    UptimeSec_ = boardLockingProxy()->GetMonitoring()->RunDuration();
  }

  else
  {
    for (size_t i = 0; i<6; ++i)
    {
      string s = boost::lexical_cast<string>(i);

      if (itemName == "TriggerFraction"+s)
      {
        HWTriggerFraction_[i] = 0.0;
        const size_t i = 0;
        if (boardLockingProxy()->IsExternalTriggerEnabled(i))
        {
          const double frac = boardLockingProxy()->GetLTCStatusInfo()->GetTriggerComposition()->GetTriggerFraction(i);
          if (frac >= 0.0)
            HWTriggerFraction_[i] = frac;
        }
      }
      else if (itemName == "TriggerName"+s)
      {
        HWTriggerName_[i] = boardLockingProxy()->GetTriggerName(i);
      }
      else if (itemName == "TriggerEnable"+s)
      {
        HWTriggerEnable_[i] = boardLockingProxy()->IsExternalTriggerEnabled(i);
      }
      else if (itemName == "TriggerDelay"+s)
      {
        vector<float> delays = boardLockingProxy()->GetHWInputDelays();
        HWTriggerDelay_[i] = delays[i];
      }
      else continue;

      break;
    }
  }

  if (itemName == "ClockSource")
  {
    ClockSource_ = (boardLockingProxy()->IsQPLLExternal() ? "External" : "Internal");
  }
  else if (itemName == "ClockFrequency")
  {
    ClockFrequency_ = boardLockingProxy()->GetQPLLFrequencyBits();
  }
}


void ttc::LTCControl::itemChangedAction(xdata::ItemChangedEvent& e)
{
  TTCXDAQBase::itemChangedAction(e);

  string itemName = e.itemName();

  for (size_t i = 0; i<6; ++i)
  {
    string s = boost::lexical_cast<string>(i);
    if (itemName == "TriggerName"+s)
    {
      boardLockingProxy()->SetTriggerName(i, HWTriggerName_[i]);
    }
    else if (itemName == "TriggerEnable"+s)
    {
      boardLockingProxy()->EnableExternalTrigger(i, HWTriggerEnable_[i]);
    }
    else if (itemName == "TriggerDelay"+s)
    {
      vector<float> delays = boardLockingProxy()->GetHWInputDelays();
      delays[i] = HWTriggerDelay_[i];
      boardLockingProxy()->SetHWInputDelays(delays);
    }
    else continue;

    break;
  }

  if (itemName == "ClockFrequency")
  {
    boardLockingProxy()->SetQPLLFrequencyBits(ClockFrequency_, false);
  }
  else if (itemName == "ClockSource")
  {
    if (ClockSource_ == "Internal")
    {
      boardLockingProxy()->SetQPLLExternal(false);
    }
    else if (ClockSource_ == "External")
    {
      boardLockingProxy()->SetQPLLExternal(true);
    }
    else
    {
      ostringstream msg;
      msg << "Invalid value '" << ClockSource_.toString() << " for parameter '" << itemName << "'.";

      ClockSource_ = (boardLockingProxy()->IsQPLLExternal() ? "External" : "Internal");

      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }

  else if (itemName == "RunNumber")
  {
    OverwriteRunNumber_ = true;
    boardLockingProxy()->SetRunNumber(RunNumber_);
  }
}


void ttc::LTCControl::setDefaultValuesAction()
{
  boardLockingProxy().initAdditionalParams(BTimeCorrection_, SlinkSrcId_);
  TTCXDAQBase::setDefaultValuesAction();
}


void ttc::LTCControl::ConfigureAction(toolbox::Event::Reference ev)
{
  for (size_t i = 0; i < boardLockingProxy()->Nextern(); ++i)
  {
    char dum[100];
    sprintf(dum, "External trigger %zu (%s): %s",
        i,
        (i < 2 ? "nim" : "lvds"),
        boardLockingProxy()->GetTriggerName(i).c_str());

    Trigselector.SetTitle(i, string(dum));
  }

  if (ReadConfigFromFile_)
  {
    string filePath = asciConfigurationFilePath_.toString();

    LOG4CPLUS_INFO(
        logger_,
        "LTCControl::ConfigureAction(): Input file = '" + filePath + "'");

    ifstream in(filePath.c_str(), ios_base::in);
    if (!in)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "Cannot open input file '" + filePath + "' for reading");
    }

    try {
      boardLockingProxy()->Configure(in); // TODO check
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(
          xcept::Exception,
          "LTC::Configure with configuration from file'" + filePath + "' failed", e);
    }
  }
  else
  {
    LOG4CPLUS_INFO(
        logger_,
        "LTCControl::ConfigureAction(): Reading configuration from XML parameter 'Configuration'");

    stringstream input;
    input << ConfigurationString_.toString() << endl;

    try {
      boardLockingProxy()->Configure(input);
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(
          xcept::Exception,
          "LTC::Configure with configuration from XML parameter 'Configuration' failed", e);
    }
  }

  // Set the run number if necessary.
  if (OverwriteRunNumber_)
  {
    boardLockingProxy()->SetRunNumber(RunNumber_);
  }

  boardLockingProxy()->ExecuteSequence("configure");

  if (boardLockingProxy()->IsL1AEnabled())
  {
    string msg =
        "L1A is enabled after 'configure'. "
        "Please enable L1A only in the 'enable' sequence.";

    error_messages.add("WARNING") << msg;
    LOG4CPLUS_WARN(logger_, msg);
  }

  FractionOfActiveBunches_ = boardLockingProxy()->GetActiveBXFraction();
  TimeOfLastStateChange_ = ttc::GetCurrentTime();
}


void ttc::LTCControl::EnableAction(toolbox::Event::Reference e)
{
  boardLockingProxy()->ExecuteSequence("enable");

  TimeOfLastStateChange_ = ttc::GetCurrentTime();

  if (!boardLockingProxy()->IsL1AEnabled())
  {
    string msg =
        "L1A is disabled after 'enable'. "
        "Please enable L1A in the 'enable' sequence.";

    error_messages.add("WARNING") << msg;
    LOG4CPLUS_WARN(logger_, msg);
  }
}


void ttc::LTCControl::StopAction(toolbox::Event::Reference e)
{
  boardLockingProxy()->ExecuteSequence("stop");

  TimeOfLastStateChange_ = ttc::GetCurrentTime();

  if (boardLockingProxy()->IsL1AEnabled())
  {
    string msg =
        "L1A is enabled after 'stop'. "
        "Please disable L1A in the 'stop' and 'suspend' sequences.";

    error_messages.add("WARNING") << msg;
    LOG4CPLUS_WARN(logger_, msg);
  }
}


void ttc::LTCControl::SuspendAction(toolbox::Event::Reference e)
{
  boardLockingProxy()->ExecuteSequence("suspend");

  TimeOfLastStateChange_ = ttc::GetCurrentTime();

  if (boardLockingProxy()->IsL1AEnabled())
  {
    string msg =
        "L1A is enabled after 'Suspend'. "
        "Please disable L1A in the 'stop' and 'suspend' sequences.";

    error_messages.add("WARNING") << msg;
    LOG4CPLUS_WARN(logger_, msg);
  }
}


void ttc::LTCControl::CommandImpl(cgicc::Cgicc& cgi, const string& command)
{
  LOG4CPLUS_INFO(
      logger_,
      "LTCControl::CommandImpl: Executing CGI command '" + command + "'");

  if (command == "ResetCounters")
  {
    if (fsm_.getCurrentState() != 'E')
    {
      resetbits_ = 0;
      if (cgi.queryCheckbox("orbitcounter"))
      {
        resetbits_ |= LTC_RESET_ORBIT;
      }
      if (cgi.queryCheckbox("strobecounter"))
      {
        resetbits_ |= LTC_RESET_BGOS;
      }
      if (cgi.queryCheckbox("eventcounter"))
      {
        resetbits_ |= LTC_RESET_EVENT;
      }
      if (cgi.queryCheckbox("triggercounter"))
      {
        resetbits_ |= LTC_RESET_TRIGGER;
      }
      if (cgi.queryCheckbox("btcounter"))
      {
        resetbits_ |= LTC_RESET_BLOCKED;
      }

      boardLockingProxy()->ResetCounters(resetbits_);
    }
  }
  else
  {
    XCEPT_RAISE(xcept::Exception, "Unknown LTC CGI command '" + command + "'");
  }
}


void ttc::LTCControl::HTMLPageMain(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageMain");

  XgiOutputHandler xoh(*out);

  xoh
      << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl
      << "<html>" << endl
      << "<head>" << endl
      << cgicc::title("LTC &quot;" + name_.toString() + "&quot;") << endl;

  if (autoRefresh_)
  {
    xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"5; URL=" << lastPage_ << "\">" << endl;
  }

  WriteJavascriptPopupCode(xoh);

  xoh
      << "</head>" << endl
      << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  PrintHTMLHeader(xoh);

  ReadLTCCounters();

  ErrorStatement(xoh);

  PrintFSMTable(xoh);
  PrintSequenceTable(xoh);

  // The trigger ticket button.
  {
    HTMLTable tab(xoh, 1, 10, 0, "", "center");
    tab.NewCell();
    xoh << "<b>Issue a trigger burst</b><br>" << "(Number of triggers configured on Main Config page.)";
    tab.NewCell();
    xoh
        << cgicc::form().set("method", "get").set("action",
            "/" + getApplicationDescriptor()->getURN() + "/SoapCommand").set("enctype", "multipart/form-data")
        << endl;
    if (fsm_.getCurrentState() == 'E' && boardLockingProxy()->GetL1ATicket() > 0 && boardLockingProxy()->GetFirmwareVersion() >= 12)
    {
      xoh << cgicc::input().set("type", "submit").set("name", "Command").set("value", "L1ATicket") << "<br>"
          << SMALLER
          << "(request " << boardLockingProxy()->GetL1ATicket() << " L1As)" << NOSMALLER;
    }
    else
    {
      xoh
          << cgicc::input().set("type", "submit").set("name", "Command").set("value", "L1ATicket").set("disabled",
              "true");
    }
    xoh << cgicc::form();
    tab.Close();
  }

  // The status table.
  HTMLTable tab(xoh, 1, 10, 0, "", "center");
  tab.NewRow();
  tab.NewCell();
  xoh << "<b>Status</b>";
  tab.NewCell();

  // Run number.
  xoh << "Run no: " << dec << (RunNumber_ = boardLockingProxy()->GetRunNumber());

  // Trigger ticket information.
  tab.NewCell();
  xoh << "Trigger ticket: ";
  if (boardLockingProxy()->GetFirmwareVersion() < 12)
  {
    xoh << "<br>(Requires FW Ver 12+)";
  }
  else if (boardLockingProxy()->GetL1ATicket() > 0)
  {
    xoh << boardLockingProxy()->GetL1ATicket() << "<br>";
    if (boardLockingProxy()->ReadTriggerCounter() >= boardLockingProxy()->GetMaxTrigger())
    {
      xoh << RED<< BOLD << "Blocked" << NOBOLD << NOCOL << endl;
    }
    else
    {
      xoh << GREEN << (boardLockingProxy()->GetMaxTrigger() - boardLockingProxy()->ReadTriggerCounter()) << " remaining" << NOCOL << endl;
    }
  }
  else
  {
    xoh << GREY << "Off" << NOCOL;
  }

    // Periodic sequence status.
  tab.NewCell();
  if (!boardLockingProxy()->PeriodicSequenceEnabled() || (boardLockingProxy()->Periodicity() < 0.0))
  {
    xoh << "Periodic seq.: " << GREY << "Off" << endl;
    xoh << NOCOL << endl;
  }
  else
  {
    xoh << "Periodic seq.: " << RED << "On (" << boardLockingProxy()->Periodicity() << " s)" << NOCOL;
  }

  // Detailed board status.
  string defaultcol = GREEN;
  string alertcol = RED;
  tab.NewRow();
  tab.NewCell("", 4);

  xoh
      << UNDERL << "Board status:" << NOUNDERL
      << MONO << BLUE << hex << " 0x" << BoardStatus_ << NOCOL << NOMONO << dec << endl;

  {
    // Clock sync.
    xoh << "<br>Clock: ";
    if (boardLockingProxy()->IsQPLLExternal())
    {
      xoh << "external ";
      if (boardLockingProxy()->IsClockLocked())
      {
        xoh << "(" << defaultcol << "locked" << NOCOL << ")";
      }
      else
      {
        xoh << "(" << alertcol << "not locked!" << NOCOL << ")";
      }
    }
    else
    {
      xoh << alertcol << "internal" << NOCOL << " (0x" << hex << boardLockingProxy()->GetQPLLFrequencyBits() << dec << ")";
    }

    // Orbit sync.
    bool isok = boardLockingProxy()->OrbitInSync();
    string col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "Orbit in sync.? " << col << (isok ? "yes" : "no") << NOCOL << endl;

    // Cancel counter.
    uint32_t ncan = boardLockingProxy()->CancelledTriggers();
    isok = (ncan == 0);
    col = (isok ? defaultcol : alertcol);
    xoh << "<br>" << "Triggers cancelled by rules: " << col << ncan << NOCOL << endl;

    // TTS status.
    string select_url = "/";
    select_url += getApplicationDescriptor()->getURN();
    select_url += "/TTSConfigure";
    xoh << cgicc::form().set("method", "post").set("action", select_url).set("enctype", "multipart/form-data")
        << endl;
    xoh << cgicc::fieldset() << endl;
    xoh << cgicc::legend("TTS &amp; S-Link Status") << endl;
    xoh << "<center>";
    HTMLTable ttstab(xoh, 1, 2, 2, "", "center");
    ttstab.NewRow();

    TTSInfo mytts(boardLockingProxy()->ReadTTSStatus());

    for (int j = 0; j < int(mytts.NPartitions()) + 1; ++j)
    {
      if (j == 1)
      {
        ttstab.NewCell("white");
        // S-link status.
        if (!boardLockingProxy()->IsSlinkBackpressureIgnored())
        {
          xoh << cgicc::input().set("type", "checkbox").set("name", "slinkbackpressure").set("checked", "checked")
              << BLACK << endl;
        }
        else
        {
          xoh << cgicc::input().set("type", "checkbox").set("name", "slinkbackpressure") << GREY << endl;
        }
        string slstat;
        boardLockingProxy()->ReadSLinkStatus(slstat, true);
        xoh << "S-Link: " << NOCOL << "<br>" << slstat << endl;

        continue;
      }

      int i = 0;
      // Map the position in the HTML table to the number of the TTS
      // input in the LTC firmware?
      switch (j)
      {
        case 0:
          i = 6;
          break;
        case 2:
          i = 4;
          break;
        case 3:
          i = 5;
          break;
        case 4:
          i = 2;
          break;
        case 5:
          i = 3;
          break;
        case 6:
          i = 0;
          break;
        case 7:
          i = 1;
          break;
        default:
          xoh << "TTS ERROR!" << endl;
          j = 99;
          continue;
      }
      if ((j > 0) && ((j % 2) == 0))
      {
        ttstab.NewRow();
      }
      string cellcol = "grey";
      bool enabled = boardLockingProxy()->IsTTSEnabled(i);

      if (!enabled)
        cellcol = "grey";
      else if (mytts.IsReady(i))
        cellcol = "green";
      else if (mytts.IsDisconnected(i))
        cellcol = "lightgrey";
      else if (mytts.IsOutOfSync(i))
        cellcol = "red";
      else if (mytts.IsWarning(i))
        cellcol = "orange";
      else if (mytts.IsError(i))
        cellcol = "red";
      else
        cellcol = "red";

      ttstab.NewCell(cellcol);

      // Checkbox.
      stringstream stm;
      stm << "TTS " << i;
      if (enabled)
      {
        xoh << cgicc::input().set("type", "checkbox").set("name", stm.str()).set("checked", "checked") << endl;
      }
      else
      {
        xoh << cgicc::input().set("type", "checkbox").set("name", stm.str()) << endl;
      }

      if (i == 6)
      {
        // aTTS
        xoh << "aTTS<br>" << endl;
      }
      else
      {
        // sTTS
        xoh << "sTTS(" << i << ")<br>" << endl;
      }

      xoh << BOLD << mytts.GetStatus_String(i, !enabled) << NOBOLD;
    }
    ttstab.Close();
    xoh << "</center>";

    {
      xoh << "Wait " << endl;
      stringstream dum;
      dum << boardLockingProxy()->GetWarningInterval();
      xoh
          << cgicc::input().set("type", "text").set("name", "warningpause").set("size", "3").set("value",
              dum.str().c_str()) << endl;
      xoh << " BX for &quot;Warning&quot;" << endl;
    }

    // End of fieldset.
    xoh << cgicc::fieldset() << endl;
    xoh << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply");
    xoh << cgicc::form() << endl;

    const uint32_t ttswd = mytts.GetStatusWord();
    xoh << "(TTS: 0x" << hex << ttswd << dec << " = <br>";

    for (int i = 4 * int(mytts.NPartitions()) - 1; i >= 0; --i)
    {
      xoh << (((ttswd >> i) & 1) ? "1" : "0");
      if (i % int(mytts.NPartitions()) == 0 && i > 0)
        xoh << "-";
    }
    xoh << ")<br>" << endl;
  }
  tab.Close();

  // Counters table.
  string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/Command";
  tab.Set(1, 10, 0, "", "left");
  tab.Open();

  tab.NewRow();
  tab.NewCell();
  xoh << "<center>" << BOLD << "Counters" << NOBOLD
  << "<br/>" << "<font size=\"-2\">" << "(<a href=\"ShowRatesPopup\"" << "target=\"rates_popup_frame\" "
      << "onclick=\"return openPopup('" << getFullURL() << "/ShowRatesPopup',"
      << "'rates_popup_frame',400,200);\">show rates popup</a>)" << endl << "</font>" << endl << "</center>";

  // Print all values read.
  tab.NewCell("", 1, 2);

  xoh
      << UNDERL << "Counters" << NOUNDERL << endl
      << "<br>" << "Triggers: " << RED<<TriggerCounter_ << NOCOL << endl
      << "<br>" << "Evt-ID: " << RED<<EventCounter_ << NOCOL << endl
      << "<br>" << "Orbit: " << RED<< OrbitCounter_ << NOCOL << endl
      << "(~ " << GenericTTCModule::OrbitCounterToSecondsString(OrbitCounter_) << ")" << endl
      << "<br>" << "BGO Cntr: " << RED << StrobeCounter_ << NOCOL << endl
      << "<br>" << "Blocked L1As: " << RED << BlockedL1ACntr_ << NOCOL << endl;

  {
    const LTCStatusInfo* S = boardLockingProxy()->GetLTCStatusInfo();
    const double dt = S->DeltaT();

    xoh << "<br><br>" << UNDERL << "L1A rate (after " << dt << " s):" << NOUNDERL << "<br>" << endl;
    xoh << S->L1ADiff() << " L1As in " << S->OrbitDiff() << " orbits.<br>" << endl;
    if (S->OrbitDiff() > 0)
    {
      xoh << RED << S->L1ARate() << " Hz" << NOCOL;
      xoh << " (" << int(S->DeltaT_Accumulated() + 0.5) << " s avg.: " << ORANGE << S->L1ARate_Accumulated() << " Hz"
          << NOCOL << ")";
      xoh << "<br>" << endl;
    }
    else
    {
      xoh << RED << "unknown" << NOCOL << "<br>" << endl;
    }
    xoh << "Eff. e = ";
    if (S->RawDiff() > 0)
    {
      xoh << RED<<(100. * S->Efficiency()) << " %" << NOCOL;
      xoh << " (" << int(S->DeltaT_Accumulated() + 0.5) << " s avg.: " << ORANGE<<100. * S->Efficiency_Accumulated()
          << " %" << NOCOL << ")";
      xoh << "<br>" << endl;
      xoh << " (1-e = ";
      xoh << RED<<(100. * (1.0 - S->Efficiency())) << " %" << NOCOL << ")" << "<br>" << endl;
    }
    else
    {
      xoh << RED<< "unknown" << NOCOL
      << "<br>" << endl << " (1-e = " << RED<< "unknown" << NOCOL << ")" << "<br>" << endl;
    }
    if (S->OrbitDiff() > 0)
      xoh << S->L1AsPerOrbit() << " L1As/orbit <br>" << endl;
    else
      xoh << "unknown L1As/orbit <br>" << endl;
    if (S->L1ADiff() > 0)
      xoh << 1.0 / S->L1AsPerOrbit() << " orbits/L1A <br>" << endl;
    else
      xoh << "unknown orbits/L1A <br>" << endl;
    xoh << "<br>" << UNDERL<< "BGO rate (after " << dt << " s):" << NOUNDERL<< "<br>" << endl;
    xoh << S->BGODiff() << " BGOs in " << S->OrbitDiff() << " orbits.<br>" << endl;
    if (S->OrbitDiff() > 0)
      xoh << RED<<S->BGORate() << " Hz" << NOCOL << "<br>" << endl;
    else
      xoh << RED<< "unknown" << NOCOL << "<br>" << endl;
    if (S->OrbitDiff() > 0)
      xoh << S->BGOsPerOrbit() << " BGOs/orbit<br>" << endl;
    else
      xoh << "unknown BGOs/orbit<br>" << endl;
    if (S->BGODiff() > 0)
      xoh << 1.0 / S->BGOsPerOrbit() << " orbits/BGO<br>" << endl;
    else
      xoh << "unknown orbits/BGO <br>" << endl;

    xoh << NOMONO;
    tab.NewRow();
    tab.NewCell();
    // ResetCounters button.
    xoh << BOLD << "Counters:" << NOBOLD << "<br>"
        << cgicc::form().set("method", "get").set("action", command_url).set("enctype", "multipart/form-data") << endl
        << "<input type=\"checkbox\" name =\"orbitcounter\"" << (resetbits_ & LTC_RESET_ORBIT ? " checked" : "")
        << ">Orbits<br>" << "<input type=\"checkbox\" name =\"strobecounter\""
        << (resetbits_ & LTC_RESET_BGOS ? " checked" : "") << ">BGOs<br>"
        << "<input type=\"checkbox\" name =\"eventcounter\"" << (resetbits_ & LTC_RESET_EVENT ? " checked" : "")
        << ">Event ID<br>" << "<input type=\"checkbox\" name =\"triggercounter\""
        << (resetbits_ & LTC_RESET_TRIGGER ? " checked" : "") << ">Total triggers<br>"
        << "<input type=\"checkbox\" name =\"btcounter\"" << (resetbits_ & LTC_RESET_BLOCKED ? " checked" : "")
        << ">Blocked triggers<br>" << endl;

    if (fsm_.getCurrentState() == 'E')
    {
      xoh
          << cgicc::input()
          .set("type", "submit")
          .set("name", "Command")
          .set("value", "ResetCounters")
          .set("disabled", "true");
    }
    else
    {
      xoh << cgicc::input().set("type", "submit").set("name", "Command").set("value", "ResetCounters");
    }
    xoh << cgicc::form();
  }
  tab.Close();
  xoh << "<p>" << endl;

  // General LTC info.
  uint32_t slinkSrcId = boardLockingProxy()->GetSlinkSrcId();
  uint32_t bTimeCorrection = boardLockingProxy()->GetBTimeCorrection();
  xoh
      << "SLink Source ID: " << slinkSrcId << " (0x" << hex << slinkSrcId << dec << ")<br>" << endl
      << "BTimeCorrection: " << bTimeCorrection << " BX " << "<br>" << endl;

  { // Configuration file display and change.
    string confurl = "/";
    confurl += getApplicationDescriptor()->getURN();
    confurl += "/NewConfigurationFile";

    xoh << cgicc::form().set("method", "post").set("action", confurl).set("enctype", "multipart/form-data") << endl;

    GetFileList();
    xoh << cgicc::fieldset() << endl;
    xoh << cgicc::legend("Configure LTC FROM file") << endl;
    xoh << cgicc::label("Current configuration source: ");
    if (ReadConfigFromFile_)
    {
      xoh << "File (" << MONO
      <<asciConfigurationFilePath_.toString() << NOMONO<< ")" << endl;
    }
    else
    {
      xoh << "NOT from file but directly from the xml input-string &quot;Configuration&quot;" << endl;
    }
    xoh << "<br>" << endl;
    InputFileList.Write(xoh);
    xoh << cgicc::label("&nbsp;&nbsp;&nbsp;&nbsp;(enter absolute path): ") << endl;
    xoh << cgicc::input().set("type", "text").set("name", "ConfigurationFile").set("size", "60").set("value", "")
        << cgicc::p() << endl;
    xoh << "<p>" << endl;
    xoh << SMALLER
    << "N.B.: 'Submit' will only change path " << "to the configuration file, the "
        << "configuration itself (i.e. opening the file and " << "reading in the "
        << "configuration) will happen when requesting 'Configure' " << "in the State Machine." << NOSMALLER << "<br>"
        << endl;
    xoh << cgicc::fieldset() << endl;
    xoh << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Submit");
    xoh << "<p>" << endl;
    xoh << cgicc::form() << endl;

    // Write configration to new file.
    confurl = "/";
    confurl += getApplicationDescriptor()->getURN();
    confurl += "/WriteConfigurationFile";

    xoh << cgicc::form().set("method", "post").set("action", confurl).set("enctype", "multipart/form-data") << endl;

    xoh << cgicc::fieldset() << endl;
    char s[1024];
    gethostname(s, sizeof s);
    string machine = s;
    size_t pos = machine.find_first_of(".");
    if (pos < machine.size())
      machine.erase(machine.begin() + pos, machine.end());
    machine = string(" (on ") + MONO + machine + NOMONO + ")";
    xoh << cgicc::legend((string("Write current configuration TO file") + machine).c_str()) << endl;
    xoh << cgicc::label("Destination (enter absolute path): ") << endl;
    xoh
        << cgicc::input().set("type", "text").set("name", "ConfigurationFile").set("size", "60").set("value",
            (ReadConfigFromFile_ ? asciConfigurationFilePath_.toString() : "")) << cgicc::p() << endl;
    xoh << "<p>" << endl;

    xoh << cgicc::input().set("type", "checkbox").set("name", "overwrite") << endl;
    xoh << "force overwrite (if file exists)" << endl;

    xoh << cgicc::fieldset() << endl;
    xoh << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Write");
    xoh << "<p>" << endl;
    xoh << cgicc::form() << endl;
  }

  PrintHTMLFooter(xoh);

  xoh << "</body>" << "</html>" << endl;
}


void ttc::LTCControl::HTMLPageMainConfiguration(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageMainConfiguration");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << cgicc::title("LTC \"" + name_.toString() + "\"") << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  xoh << "Finite State Machine: ";
  PrintFSMState(xoh);
  xoh << cgicc::p() << cgicc::p() << endl;

  string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/MainConfigCommand";

  xoh
      << cgicc::form()
      .set("method", "post")
      .set("action", command_url)
      .set("enctype", "multipart/form-data") << endl
      << cgicc::fieldset() << endl;

  stringstream my;
  my << "Main Config / QPLL";
  xoh << cgicc::legend(my.str().c_str()) << endl;

  // Table
  HTMLTable tab(xoh, 0, 2, 2, "", "", 100);
  tab.NewRow();

  // INPUTS
  {
    tab.NewCell();
    xoh << BOLD<< "Trigger Selection:" << NOBOLD<< "<br>" << endl;
    for (size_t i = 0; i < Trigselector.size(); ++i)
    {
      int ii = atoi(Trigselector.GetValue(i).c_str());
      if (ii == int(boardLockingProxy()->Nextern()))
      {
        Trigselector.SetCheck(i, boardLockingProxy()->IsCyclicTrigEnabled());
      }
      else if (ii == 1 + int(boardLockingProxy()->Nextern()))
      {
        Trigselector.SetCheck(i, boardLockingProxy()->IsRAMTrigEnabled());
      }
      else if (ii < 0 || ii > 1 + int(boardLockingProxy()->Nextern()))
      {
        LOG4CPLUS_ERROR(logger_, "LTCControl::MainConfiguration(): "
        "invalid trigger selection value: " << Trigselector.GetValue(i));
      }
      else
      {
        Trigselector.SetCheck(i, boardLockingProxy()->IsExternalTriggerEnabled(size_t(ii)));
      }
      if (i < boardLockingProxy()->Nextern())
      {
        char dum2[100];
        sprintf(dum2, "External trigger %zu (%s): %s", i, (i < 2 ? "nim" : "lvds"),
            boardLockingProxy()->GetTriggerName(i).c_str());
        Trigselector.SetTitle(i, string(dum2));
      }
    }
    Trigselector.Write(xoh);
    if (boardLockingProxy()->GetRAMTriggers()->CanSetInternalTriggerFrequency())
    { // Trigger Frequency
      xoh << cgicc::label("&nbsp;&nbsp;&nbsp;&nbsp;Freq.: ");
      char freq[20];
      double trigfreq = boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerFrequency();
      if (trigfreq < 0)
        sprintf(freq, "1");
      else if (trigfreq == double(int(trigfreq)))
        sprintf(freq, "%.0f", trigfreq);
      else
        sprintf(freq, "%.2f", trigfreq);
      xoh
          << cgicc::input().set("type", "text").set("name", "TriggerFrequency").set("size", "6").set("value",
              string(freq)) << " Hz" << endl;
      if (boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerRandom())
      {
        xoh << cgicc::input().set("type", "checkbox").set("name", "TriggerRandom").set("checked", "checked")
            << GREEN << " Random" << NOCOL << endl;
      }
      else
      {
        xoh << cgicc::input().set("type", "checkbox").set("name", "TriggerRandom") << " Random" << endl;
      }

    }
    else
    {
      xoh << GREY<< "&nbsp;&nbsp;&nbsp;&nbsp;Freq.: " << "not applicable.<br>";
      xoh << "&nbsp;&nbsp;&nbsp;&nbsp;(\"" << MONO << "TRIGGER_INTERVAL x" << NOMONO
      << "\" in config. file)" << NOCOL << endl;
    }

    xoh << "<br>" << endl;

    xoh << SMALLER << "CONTROL = " << MONO<< BLUE << "0x" << hex
    << boardLockingProxy()->GetControlWord()
    << NOCOL << NOMONO << " (" << MONO << BLUE << "0x" << boardLockingProxy()->ReadControlWord()
    << NOCOL << NOMONO << " on board)<br>" << endl
    << NOSMALLER << endl;

    xoh << BOLD<< "Trigger Status: " << NOBOLD;

    if (boardLockingProxy()->IsL1AEnabled())
    {
      xoh << GREEN<<BOLD<< "L1A Enabled" <<NOBOLD<< NOCOL << endl;
    }
    else
    {
      xoh << RED<<BOLD<< "L1A Disabled" <<NOBOLD<< NOCOL << endl;
    }

    // Trigger Burst
    xoh << "<br>" << BOLD << "L1A/Trigger Ticket:" << NOBOLD;
    if (boardLockingProxy()->GetFirmwareVersion() < 12)
    {
      xoh << " (Requires Firmware V. 12+)";
    }
    else if (boardLockingProxy()->GetL1ATicket() != 0 && boardLockingProxy()->ReadTriggerCounter() >= boardLockingProxy()->GetMaxTrigger())
    {
      xoh << RED<<BOLD<< " (Triggers Blocked)" << NOBOLD << NOCOL;
    }
    else if (boardLockingProxy()->GetL1ATicket() != 0)
    {
      xoh << GREEN<< (boardLockingProxy()->GetMaxTrigger() - boardLockingProxy()->ReadTriggerCounter()) << " remaining" << NOCOL;
    }
    xoh << "<br>\n";
    if (boardLockingProxy()->GetL1ATicket() != 0)
    {
      xoh << cgicc::input().set("type", "checkbox").set("name", "bursten").set("checked", "checked")
          << GREEN << " Enable" << NOCOL << endl;
    }
    else
    {
      xoh << cgicc::input().set("type", "checkbox").set("name", "bursten") << " Enable" << endl;
    }

    {
      char dum[30];
      sprintf(dum, "%u", boardLockingProxy()->GetL1ATicket());
      xoh << " - No. of L1As: "
          << cgicc::input().set("type", "text").set("name", "trigburst").set("size", "6").set("value", dum) << endl;
    }
    xoh << "<br>" << endl << SMALLER << "MAXTRIG = " << MONO<<BLUE<<dec
    <<boardLockingProxy()->GetMaxTrigger()
    << NOCOL <<NOMONO<<NOSMALLER<< endl;

    tab.NewCell();

    // QPLL
    {
      //xoh<< "<br>" << endl;
      string sel = UNDERL, unsel = NOUNDERL;
      xoh << BOLD<< "QPLL Control:" << NOBOLD<< "<br>" << endl;
      xoh << "QPLLCTRL Bit 6" << ":<br> " << endl;

      bool qpll_external_mode = boardLockingProxy()->IsQPLLExternal();

      WriteBinaryRadioButton(xoh, "QPLLext", "External (0)", "Internal (1)", "YES", "NO",
          (qpll_external_mode ? 0 : 1), false // not disabled
          );
      xoh << " clock" << endl;
      xoh << "<br>" << endl;

      if (!qpll_external_mode)
        xoh << "<span style=\"color: grey\";>";

      xoh << "QPLLCTRL Bit 5" << SMALLER << " (ignored for internal clock)" << NOSMALLER
      << ":<br> " << endl;

      WriteBinaryRadioButton(xoh, "QPLLReset", "Reset (0)", "No Reset (1)", "YES", "NO",
          boardLockingProxy()->Is_ResetQPLL() ? 0 : 1, !qpll_external_mode // disabled in internal mode
          );
      xoh << "<br>" << endl;

      //--------------------

      xoh << "QPLLCTRL Bit 4" << SMALLER << " (ignored for internal clock)" << NOSMALLER
      ":<br> " << endl;

      {
        if (boardLockingProxy()->IsQPLLExternal())
        {
          if (boardLockingProxy()->Is_AutoRestartQPLL())
            xoh << GREEN;
          else
            xoh << RED;
        }

        xoh << "AutoRestart";

        if (boardLockingProxy()->IsQPLLExternal())
          xoh << NOCOL;

        xoh << ": " << endl;
      }

      WriteBinaryRadioButton(xoh, "QPLLAutoRestart", "ON (1)", "OFF (0)", "ON", "OFF",
          boardLockingProxy()->Is_AutoRestartQPLL() ? 0 : 1, !qpll_external_mode // disabled in internal mode
          );

      xoh << "<br>" << endl;

      if (!qpll_external_mode)
        xoh << "</span>";

      // QPLL Frequency bits
      {
        // This control is ignored when on external clock.
        if (qpll_external_mode)
        {
          xoh << "<span style=\"color: grey\";>";
        }

        stringstream dum;
        dum << hex << boardLockingProxy()->GetQPLLFrequencyBits();
        xoh << "QPLL Freq. bits: <br>" << MONO<< "0x" << NOMONO;

        xoh << "<input type=\"text\" name=\"freqbits\" size=\"3\" value=\"" << dum.str() << "\"";

        if (qpll_external_mode)
          xoh << " disabled";

        xoh << "/>" << endl;
        xoh << SMALLER << " (0x00..0x3F for internal clock, ignored for external clock)" << NOSMALLER<< endl;

        if (qpll_external_mode)
          xoh << "</span>";
      }

      xoh << "<br>" << SMALLER<< "(QPLLCTRL = " << MONO<<BLUE<< "0x"
      <<hex<<boardLockingProxy()->Read(LTCAdd::QPLLCTRL)<< NOCOL <<NOMONO
      <<dec<< ")" <<NOSMALLER<< endl;

      // GPS Time
      xoh << "<br>" << BOLD<< "BST GPS Time Signal" << NOBOLD<< "<br>" << endl;
      if (boardLockingProxy()->IsBSTGPSviaVME())
      {
        xoh << cgicc::input().set("type", "checkbox").set("name", "gpsfromvme").set("checked", "checked")
            << GREEN << " GPS/BST through VME" << NOCOL << endl;

      }
      else
      {
        xoh << cgicc::input().set("type", "checkbox").set("name", "gpsfromvme") << " GPS/BST through VME" << endl;
      }
      xoh << " (every " << boardLockingProxy()->GetBSTGPSInterval() << " sec)" << endl;
    }

    // Monitoring:
    xoh << "<br>" << BOLD<< "Monitoring/Sampling:" << NOBOLD;
    xoh << "<br>" << endl;
    if (boardLockingProxy()->IsMonitoringEnabled())
    {
      xoh << cgicc::input().set("type", "checkbox").set("name", "domonitoring").set("checked", "checked")
          << GREEN << " Enable" << NOCOL << endl;
    }
    else
    {
      xoh << cgicc::input().set("type", "checkbox").set("name", "domonitoring") << " Enable" << endl;
    }

    xoh << " (Interval: " << endl;
    char dum[20];
    sprintf(dum, "%.0f", boardLockingProxy()->GetMonitoringInterval());
    xoh << cgicc::input().set("type", "text").set("name", "monitoringintv").set("size", "2").set("value", dum)
        << " sec)" << endl;

    if (!boardLockingProxy()->DefaultFIFODumpEnabled())
    {
      xoh << "<br>" << endl;
      if (boardLockingProxy()->DumpEventFIFOToFile())
      {
        xoh << cgicc::input().set("type", "checkbox").set("name", "dofifodump").set("checked", "checked") << endl;

      }
      else
      {
        xoh << cgicc::input().set("type", "checkbox").set("name", "dofifodump") << endl;
      }
      xoh << " ASCII file for FIFO dump: " << endl;
      xoh
          << cgicc::input().set("type", "text").set("name", "fifodumpfile").set("size", "25").set("value",
              boardLockingProxy()->EventFIFOToFilePath().c_str()) << endl;
    }
    else
    {
      xoh << "<br>" << endl;
      xoh << "Automatic FIFO dump enabled (to " << boardLockingProxy()->FIFODumpPrefix() << "*)" << endl;
    }
  }

  tab.Close();
  // End of fieldset.
  xoh << cgicc::fieldset() << endl;

  xoh
      << cgicc::input()
      .set("type", "submit")
      .set("name", "submit")
      .set("value", "Apply");

  xoh << cgicc::form() << endl;

  // Trigger rules and hardware delays.
  string myurl = "/";
  myurl += getApplicationDescriptor()->getURN();
  myurl += "/TriggerRules";

  xoh
      << cgicc::form().set("method", "post").set("action", myurl).set("enctype", "multipart/form-data") << endl
      << cgicc::fieldset() << endl
      << cgicc::legend("Trigger Rules & HW Trigger Delays") << endl;

  HTMLTable trigtab(xoh, 1, 2, 0, "", "", 100);
  trigtab.NewRow();
  // Trigger Rules
  trigtab.NewCell();
  for (size_t i = 0; i < boardLockingProxy()->TriggerRuleSize(); ++i)
  {
    if (i > 0)
      xoh << "<br> ";

    xoh
        << "No more than " << (i + boardLockingProxy()->FirstTriggerRule()) << " triggers in "
        << cgicc::input()
        .set("type", "text")
        .set("name", (string("trigrule") + UnsignedLong2String(i + boardLockingProxy()->FirstTriggerRule())).c_str())
        .set("size", "6")
        .set("value", UnsignedLong2String(boardLockingProxy()->GetTriggerRule(i + boardLockingProxy()->FirstTriggerRule())).c_str()) << endl
        << " consecutive BXs" << endl;
  }

  // HW Delays
  trigtab.NewCell();
  vector<float> delays = boardLockingProxy()->GetHWInputDelays();
  if (boardLockingProxy()->ReadFirmwareVersion() >= 0x14)
  {
    xoh << UNDERL << "HW Delays" << " (0.0, 0.5,... 31.5 BX):" << NOUNDERL << endl;
  }
  else
  {
    xoh << UNDERL << "HW Delays" << " (0.0, 0.5,... 15.5 BX):" << NOUNDERL << endl;
  }
  for (size_t i = 0; i < boardLockingProxy()->Nextern(); ++i)
  {
    if (delays[i] > 0)
      xoh << RED<< endl;
    xoh << "<br>Ext. trig. " << i << ") ";
    char dum1[50], dum2[20], dum3[50];
    sprintf(dum1, "trigdelay %zu", i);
    sprintf(dum3, "trigname %zu", i);
    if ((delays[i] - (1.0 * int(delays[i]))) > 0.1)
    {
      sprintf(dum2, "%.1f", delays[i]);
    }
    else
    {
      sprintf(dum2, "%.0f", delays[i]);
    }
    xoh << cgicc::input().set("type", "text").set("name", dum1).set("size", "3").set("value", dum2) << endl;
    xoh << " BX" << endl;
    if (i < 2)
      xoh << " (nim)" << endl;
    else
      xoh << " (lvds)" << endl;
    if (delays[i] > 0)
      xoh << NOCOL << endl;
    xoh
        << cgicc::input().set("type", "text").set("name", dum3).set("size", "15").set("value",
            boardLockingProxy()->GetTriggerName(i).c_str()) << endl;
  }

  trigtab.Close();
  // End fieldset.
  xoh << cgicc::fieldset() << endl;

  xoh
      << cgicc::input()
      .set("type", "submit")
      .set("name", "submit")
      .set("value", "Apply");

  xoh << cgicc::form() << endl;

  PrintHTMLFooter(xoh);
  xoh << "</body>" << "</html>" << endl;
}


void ttc::LTCControl::HTMLPageBGOConfiguration(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageBGOConfiguration");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << cgicc::title("LTC \"" + name_.toString() + "\"") << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  {
    HTMLTable tab0(xoh, 1, 1, 0, "", "left", 100);
    string cmd_url = "/";
    cmd_url += getApplicationDescriptor()->getURN();
    cmd_url += "/SoapCommand";
    for (size_t i = 1; i < boardLockingProxy()->NChannels(); ++i)
    { // VME BGO Button
      if ((i - 1) % 3 == 0)
        tab0.NewRow();
      tab0.NewCell();

      xoh
          << "" << boardLockingProxy()->GetBGOChannel(i)->GetName() << "<br>" << endl
          << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data") << endl
          << cgicc::input().set("type", "hidden").set("name", "Command").set("value", "VMEBGO") << endl
          << cgicc::input().set("type", "hidden").set("name", "Param").set("value", to_string(i)) << endl
          << cgicc::input().set("type", "submit").set("value", "BGO " + to_string(i))
          << cgicc::form();
    }
    // VME Trigger Button:
    tab0.NewRow();
    tab0.NewCell("", 3, 1);

    xoh
        << "" << RED<<BOLD<< "L1A" <<NOBOLD<< NOCOL << "<br>" << endl
        << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data") << endl
        << cgicc::input().set("type", "submit").set("name", "Command").set("value", "VMETrigger")
        << cgicc::form();

    tab0.Close();
  }

  { // BX selection
    xoh << "<br>" << endl;

    string cmd_url = "/";
    cmd_url += getApplicationDescriptor()->getURN();
    cmd_url += "/BGOSelectCommand";

    xoh
        << cgicc::form().set("method", "post").set("action", cmd_url).set("enctype", "multipart/form-data") << endl
        << cgicc::fieldset() << endl
        << cgicc::legend("VME-BGO Timing") << endl
        << "BX at which VME BGOs will be sent: " << endl
        << cgicc::input().set("type", "text").set("name", "atbx").set("size", "6")
        .set("value", UnsignedLong2String(boardLockingProxy()->GetVMEBGOBX()).c_str()) << endl
        << cgicc::fieldset() << endl;

    xoh << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply");
    xoh << cgicc::form() << endl;
  }
  PrintHTMLFooter(xoh);
  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageSequences(xgi::Input* /*in*/, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageSequences");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << cgicc::title("LTC \"" + name_.toString() + "\"") << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  xoh << "Finite State Machine: ";
  PrintFSMState(xoh);
  xoh << cgicc::p() << cgicc::p() << endl;

  string SeqTitle = SequenceSelector.GetDefaultTitle();

  const vector<string> newseqvalues = boardLockingProxy()->GetSequenceNames();
  SequenceSelector.Set(ttc::HTMLFieldElement::DROPDOWNMENU, 0, "sequenceselect", newseqvalues, newseqvalues);

  if (!SequenceSelector.HasValue(SeqTitle))
  {
    SeqTitle = newseqvalues.size() ? newseqvalues[0] : "";
  }

  SequenceSelector.SetDefault(SeqTitle);

  xoh << BIGGER<<BOLD<<"LTC Sequence \""
  << BLUE<<SeqTitle<<NOCOL <<"\""<<NOBOLD<<NOBIGGER
  <<"<br>"<<endl;
  {

    // The First Table with 2 cells:
    HTMLTable tab(xoh, 0, 0, 0, "", "", 100);
    tab.NewCell();

    // Select sequence to be displayed.
    string select_url = "/";
    select_url += getApplicationDescriptor()->getURN();
    select_url += "/SequenceSelectCommand";
    xoh << cgicc::form().set("method", "post").set("action", select_url).set("enctype", "multipart/form-data")
        << endl;
    xoh << cgicc::fieldset() << endl; // Begin Fieldset
    xoh << cgicc::legend(SeqTitle + " Sequence") << endl;
    xoh << "Select sequence: " << endl;
    SequenceSelector.Write(xoh);
    vector<string> seqchce, seqchce_tit;
    seqchce.push_back("display");
    seqchce_tit.push_back("display");
    seqchce.push_back("remove");
    seqchce_tit.push_back("remove");
    seqchce.push_back("exec");
    seqchce_tit.push_back("execute");
    seqchce.push_back("add");
    seqchce_tit.push_back("add new:");
    HTMLFieldElement seqchoice(ttc::HTMLFieldElement::RADIOBUTTON, 0, "seqchoice", seqchce,
        seqchce_tit /*,"vertical green"*/);
    seqchoice.Write(xoh);

    xoh
        << " " << cgicc::input()
        .set("type", "text")
        .set("name", "newsequence")
        .set("size", "10")
        .set("value", "") << endl
        << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Go") << endl
        << cgicc::fieldset() << endl
        << cgicc::form() << endl;

    tab.Close();
  }

  ttc::Sequence* myseq = boardLockingProxy()->GetSequence(SeqTitle);

  { // Edit current sequence.
    string select_url = "/";
    select_url += getApplicationDescriptor()->getURN();
    select_url += "/SequenceEditCommand";
    xoh << cgicc::form().set("method", "post").set("action", select_url).set("enctype", "multipart/form-data")
        << endl;
    xoh << cgicc::fieldset() << endl; // Begin Fieldset
    xoh << cgicc::legend("Edit " + SeqTitle + " Sequence") << endl;
    // The Edit Table with 2 cells:
    HTMLTable tab(xoh, 1, 1, 0, "", "", 100);
    tab.NewCell();
    // Show the currecnt content of this sequence as list of radio
    // buttons.
    vector<string> seqsel = myseq->Get();
    seqsel.push_back("< Append >");
    HTMLFieldElement sequence(ttc::HTMLFieldElement::RADIOBUTTON, 0, "sequenceline", seqsel, seqsel,
        "vertical green");
    sequence.SetDefault(seqsel[seqsel.size() - 1]);
    if (seqsel.size() <= 1)
      xoh << "empty sequence.<br>" << endl;
    sequence.Write(xoh);

    tab.NewCell(); // 2nd cell

    xoh << "Edit action:<br>" << endl;
    vector<string> edit, edit_tit;
    edit.push_back("new");
    edit_tit.push_back("New line (insert above)");
    edit.push_back("modify");
    edit_tit.push_back("Modify line");
    edit.push_back("delete");
    edit_tit.push_back("Delete line");
    HTMLFieldElement edittab(ttc::HTMLFieldElement::RADIOBUTTON, 0, "edit", edit, edit_tit, "vertical");
    edittab.SetDefault(edit[0]);
    edittab.Write(xoh);
    xoh << endl;
    xoh << "Command: " << endl;
    vector<string> edit2, edit2_tit;
    edit2.push_back("Choose");
    edit2_tit.push_back("< Choose >");
    edit2.push_back("EnableL1A");
    edit2_tit.push_back("EnableL1A");
    edit2.push_back("DisableL1A");
    edit2_tit.push_back("DisableL1A");
    edit2.push_back("L1ATicket");
    edit2_tit.push_back("L1ATicket");
    edit2.push_back("Cyclic StartPermanent");
    edit2_tit.push_back("Cyclic StartPermanent");
    edit2.push_back("Cyclic StopAll");
    edit2_tit.push_back("Cyclic StopAll");
    edit2.push_back("Cyclic Start");
    edit2_tit.push_back("Cyclic Start");
    edit2.push_back("Cyclic Stop");
    edit2_tit.push_back("Cyclic Stop");
    edit2.push_back("Sleep");
    edit2_tit.push_back("Sleep &lt;value&gt; seconds");
    edit2.push_back("mSleep");
    edit2_tit.push_back("mSleep &lt;value&gt; millissec.");
    edit2.push_back("uSleep");
    edit2_tit.push_back("uSleep &lt;value&gt; microsec.");
    edit2.push_back("BGO");
    edit2_tit.push_back("BGO &lt;value&gt; (&lt;value&gt;=Channel)");
    edit2.push_back("ResetCounters");
    edit2_tit.push_back("ResetCounters");
    edit2.push_back("ResetCounters All");
    edit2_tit.push_back("ResetCounters All");
    edit2.push_back("NewRun");
    edit2_tit.push_back("New Run Number (increment)");
    edit2.push_back("Saveplots");
    edit2_tit.push_back("Saveplots (monitoring)");
    edit2.push_back("Periodic On");
    edit2_tit.push_back("Periodic On");
    edit2.push_back("Periodic Off");
    edit2_tit.push_back("Periodic Off");
    edit2.push_back("Periodic");
    edit2_tit.push_back("Periodic interval &lt;value&gt; [sec]");

    edit2.push_back("EnableCyclicBgo");
    edit2_tit.push_back("Enable cyclic BGO generator");
    edit2.push_back("EnableCyclicTrigger");
    edit2_tit.push_back("Enable cyclic trigger generator");
    edit2.push_back("DisableCyclicBgo");
    edit2_tit.push_back("Disable cyclic BGO generator");
    edit2.push_back("DisableCyclicTrigger");
    edit2_tit.push_back("Disable cyclic trigger generator");

    HTMLFieldElement edittab2(ttc::HTMLFieldElement::DROPDOWNMENU, 0, "edit2", edit2, edit2_tit);
    edittab2.SetDefault(edit2[0]);
    edittab2.Write(xoh);
    xoh << endl << " &lt;value&gt; = ";
    xoh << cgicc::input().set("type", "text").set("name", "x").set("size", "4").set("value", "") << endl;
    xoh << "(hex values must start with 0x)" "<br>" << endl;
    edit2.clear();
    edit2_tit.clear();
    edit2.push_back("Choose");
    edit2_tit.push_back("< Choose >");
    for (size_t k = 0; k < boardLockingProxy()->NChannels(); ++k)
    {
      string val = "BGO ", tit;
      string dum = boardLockingProxy()->GetBGOChannel(k)->GetName();
      if (dum[0] == 'C' && dum[1] == 'h' && dum[2] == 'a')
      {
        char si[3];
        sprintf(si, "%zu", k);
        val += string(si);
      }
      else
      {
        val += dum;
      }
      tit = val;
      edit2.push_back(val);
      edit2_tit.push_back(tit);
    }
    xoh << "&nbsp;&nbsp; or BGO: ";
    HTMLFieldElement edittab3(ttc::HTMLFieldElement::DROPDOWNMENU, 0, "edit3", edit2, edit2_tit);
    edittab3.SetDefault(edit2[0]);
    edittab3.Write(xoh);

    tab.Close();
    xoh << "<p>" << endl;
    // End of fieldset.
    xoh << cgicc::fieldset() << endl;
    xoh << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply");
    xoh << cgicc::form() << endl;
  }

  PrintHTMLFooter(xoh);

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCyclicGenerators(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCyclicGenerators");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << cgicc::title("LTC \"" + name_.toString() + "\"") << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  { // Start & Stop Buttons
    string cmd_url = "/";
    cmd_url += getApplicationDescriptor()->getURN();
    cmd_url += "/SoapCommand";
    xoh << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data") << endl
        << cgicc::input().set("type", "hidden").set("name", "Command").set("value", "Cyclic") << endl;
    xoh << "Cyclic generator control: " << endl;
    // VME Start Button:
    xoh << cgicc::input().set("type", "submit").set("name", "Param").set("value", "Start") << endl;
    // VME Stop Button:
    xoh << cgicc::input().set("type", "submit").set("name", "Param").set("value", "Stop") << endl;
    xoh << cgicc::input().set("type", "submit").set("name", "Param").set("value", "StartPermanent") << endl;
    xoh << cgicc::input().set("type", "submit").set("name", "Param").set("value", "StopAll") << endl;
    xoh << cgicc::form();
  }

  bool triggersource = boardLockingProxy()->IsCyclicTrigEnabled();
  bool bgosource = true; //boardLockingProxy()->IsInternalBGOEnabled();

  {
    xoh << "Finite State Machine: ";
    PrintFSMState(xoh);

    xoh
        << " - Source Selection: " << BOLD << "BGO=" << (bgosource ? GREEN : RED)
        << (bgosource ? "selected" : "unselected") << NOCOL
        << " Trigger=" << (triggersource ? GREEN : RED)
        << (triggersource ? "selected" : "unselected") << NOCOL << NOBOLD << endl
        << cgicc::p() << cgicc::p() << endl;
  }

  string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/CyclicConfigCommand";

  xoh << cgicc::form().set("method", "post").set("action", command_url).set("enctype", "multipart/form-data")
      << endl;
  xoh << cgicc::fieldset() << endl; // Begin Fieldset
  xoh << cgicc::legend("Cyclic Generator Configuration") << endl;

  { // The Table
    HTMLTable maintab(xoh, 0);
    // First row:
    maintab.NewRow();
    maintab.NewCell();
    xoh << "Name";
    maintab.NewCell();
    xoh << "Configuration";
    maintab.CloseRow();

    vector<string> enable_val, enable_tit;
    enable_val.push_back("enable");
    enable_tit.push_back("enable");
    enable_val.push_back("disable");
    enable_tit.push_back("disable");

    for (size_t ik = 0; ik < (boardLockingProxy()->NCyclicBGO() + boardLockingProxy()->NCyclicTrigger()); ++ik)
    {
      char genc[23];
      sprintf(genc, "%zu__", ik);
      const string sgen = string(genc);
      bool trigger = (ik >= boardLockingProxy()->NCyclicBGO());
      const size_t i = (!trigger ? ik : ik - boardLockingProxy()->NCyclicBGO());
      const CyclicTriggerOrBGO* cycl = boardLockingProxy()->GetCyclic(trigger, i);
      // New row
      // First cell:
      maintab.NewRow();
      maintab.NewCell();
      const bool enabled = cycl->IsEnabled();
      xoh << cycl->GetName() << "<br>" << endl;
      HTMLFieldElement enable(HTMLFieldElement::RADIOBUTTON, 0, sgen + "enable", enable_val, enable_tit,
          "vertical bold blue");
      enable.SetDefault((enabled ? 0 : 1));
      enable.Write(xoh);

      // Second Cell:
      maintab.NewCell();
      { // Configuration Cell
        string bgdcol = "";
        if (enabled)
        {
          if ((trigger && triggersource) || (!trigger && bgosource))
            bgdcol = "lightgreen";
          else
            bgdcol = "green";
        }
        else if (!enabled)
        {
          if ((trigger && triggersource) || (!trigger && bgosource))
            bgdcol = "lightgrey";
          else
            bgdcol = "grey";
        }
        HTMLTable inner(xoh, 1, 3, 0, bgdcol);
        // first config row:
        inner.NewRow();
        // StartBX
        inner.NewCell();
        xoh << "StartBX: " << endl;
        xoh
            << cgicc::input().set("type", "text").set("name", sgen + "startbx").set("size", "3").set("value",
                Long2String(cycl->GetStartBX()).c_str()) << endl;
        // Prescale
        inner.NewCell();
        xoh << "Prescale: " << endl;
        xoh
            << cgicc::input().set("type", "text").set("name", sgen + "prescale").set("size", "3").set("value",
                Long2String(cycl->GetPrescale()).c_str()) << endl;
        // InitPrescale
        inner.NewCell();
        xoh << "InitWait [Orb]: " << endl;
        xoh
            << cgicc::input().set("type", "text").set("name", sgen + "initprescale").set("size", "3").set("value",
                Long2String(cycl->GetInitialPrescale()).c_str()) << endl;
        // B Channels:
        inner.NewCell();
        if (!trigger)
        {
          HTMLFieldElement BGOList = CurrentBGOList;
          BGOList.SetName(sgen + "channelno");
          BGOList.SetDefault(cycl->GetBChannel());
          BGOList.Write(xoh);
        }

        // next config line
        inner.NewRow();

        // Postscale
        inner.NewCell();
        //sprintf(dum,"%ld",cycl->GetPostscale());
        xoh
            << cgicc::input().set("type", "text").set("name", sgen + "postscale").set("size", "3").set("value",
                Long2String(cycl->GetPostscale()).c_str()) << endl;
        xoh << " times" << endl;
        // Pause
        inner.NewCell();
        xoh << "Pause [Orb]: " << endl;
        //sprintf(dum,"%ld",cycl->GetPause());
        xoh
            << cgicc::input().set("type", "text").set("name", sgen + "pause").set("size", "3").set("value",
                Long2String(cycl->GetPause()).c_str()) << endl;
        // IsRepetitive();
        inner.NewCell();
        if (cycl->IsRepetitive())
        {
          xoh << cgicc::input().set("type", "checkbox").set("name", sgen + "repetitive").set("checked", "checked");
        }
        else
        {
          xoh << cgicc::input().set("type", "checkbox").set("name", sgen + "repetitive");
        }
        xoh << "repetitive" << endl;
        // IsPermanent();
        inner.NewCell();
        if (cycl->IsPermanent())
        {
          xoh << cgicc::input().set("type", "checkbox").set("name", sgen + "permanent").set("checked", "checked");
        }
        else
        {
          xoh << cgicc::input().set("type", "checkbox").set("name", sgen + "permanent");
        }
        xoh << "permanent" << endl;

        inner.Close();
      }
    }
    // End main table.
    maintab.Close();
  }

  // Begin fieldset.
  xoh << cgicc::fieldset() << endl;
  xoh << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply");
  xoh << cgicc::form() << endl;

  {
    string cmd_url = string("/") + getApplicationDescriptor()->getURN() + "/ReadCyclicGeneratorsFromLTC";

    xoh << "<br/>" << endl << "<form method=\"post\" action=\"" << cmd_url << "\">" << endl
        << "  <input type=\"submit\" name=\"submit\" value=\"Read cyclic generator configuration from hardware\"/>"
        << endl << "</form>" << endl;
  }
  PrintHTMLFooter(xoh);
  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageSummary(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageSummary");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << cgicc::title("LTC \"" + name_.toString() + "\"") << endl;

  if (autoRefresh_)
  {
    xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"5; URL=" << lastPage_ << "\">" << endl;
  }

  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  {
    const string emph = DARKBLUE, deflt = BLACK, alert = RED;
    // Status summary.
    const string url = "/" + getApplicationDescriptor()->getURN() + "/";
    ReadLTCCounters();
    HTMLTable stab(xoh, 1, 2, 0, "", "", 100);
    stab.NewCell("top", 1, 1);
    xoh << "Finite State Machine: ";
    PrintFSMState(xoh);
    stab.NewCell("top", 1, 1);
    xoh << "L1A: " << BOLD << (boardLockingProxy()->IsL1AEnabled() ? GREEN : RED)
        << (boardLockingProxy()->IsL1AEnabled() ? "Enabled" : "Disabled") << NOCOL<< NOBOLD << endl;

    stab.NewCell("top", 1, 1);
    { // VME BGO BX
      xoh << "VME-BGO @ BX " << boardLockingProxy()->GetVMEBGOBX();
    }

    stab.NewCell("top", 1, 1);
    { // Cancel counter.
      string defaultcol = GREEN, alertcol = RED;
      uint32_t ncan = boardLockingProxy()->CancelledTriggers();
      bool isok = (ncan == 0);
      string col = (isok ? defaultcol : alertcol);
      xoh << "No. cancelled trigs.: " << col << ncan << NOCOL << endl;
    }

    stab.NewCell("top", 1, 1);
    { // Clock sync
      string defaultcol = GREEN, alertcol = RED;
      if (!boardLockingProxy()->IsQPLLExternal())
      {
        xoh << alertcol << "Internal" << NOCOL << " clock (freq. bits: 0x" << hex
            << boardLockingProxy()->GetQPLLFrequencyBits() << dec << ")" << endl;
      }
      else
      {
        xoh << "External clock (";
        if (boardLockingProxy()->IsClockLocked())
          xoh << defaultcol << "locked";
        else
          xoh << alertcol << "not locked!";
        xoh << NOCOL << ")" << endl;
      }
    }

    { // Orbit sync.
      string defaultcol = GREEN, alertcol = RED;
      bool isok = boardLockingProxy()->OrbitInSync();
      string col = (isok ? defaultcol : alertcol);
      xoh << "<br>Orbit " << col << (isok ? "in" : "out of") << NOCOL << " sync." << endl;
    }

    // Status & Counters.
    stab.NewRow();
    stab.NewCell("top lightyellow");
    xoh << UNDERL<< "Counters / Status" << NOUNDERL<< endl;
    xoh << "<br><br>" << endl;
    xoh << BOLD<< "Run " << dec << (RunNumber_ = boardLockingProxy()->GetRunNumber()) << NOBOLD<< "<br>" << endl;
    if (OverwriteRunNumber_)
      xoh << "&amp;&amp;(set externally)<br>" << endl;
    else
      xoh << "&nbsp;&nbsp;&nbsp;(set by LTC S/W)<br>" << endl;
    xoh << BOLD<< "Events: " << TriggerCounter_ << NOBOLD<< "<br>" << endl;
    if (TriggerCounter_ + BlockedL1ACntr_ > 0)
    {
      TotalEfficiency_ = double(TriggerCounter_) / double(TriggerCounter_ + BlockedL1ACntr_);
    }
    else
    {
      TotalEfficiency_ = 0.0;
    }
    xoh << BOLD<< "Tot. efficieny: " << 100.0 * TotalEfficiency_ << NOBOLD<< " %<br>" << endl;

    stab.NewCell();
    xoh << UNDERL << "Counters" << NOUNDERL << endl;
    xoh << "<br>" << "Triggers: " << RED<<TriggerCounter_ << NOCOL << endl;
    xoh << "<br>" << "Evt-ID: " << RED<<EventCounter_ << NOCOL << endl;
    xoh << "<br>" << "Blocked L1As: " << RED<<BlockedL1ACntr_ << NOCOL << endl;
    xoh << "<br>" << "Orbit: " << RED<<OrbitCounter_ << NOCOL << endl;
    int32_t secs = int32_t(double(OrbitCounter_) * 25.0e-9 * 3564.0);
    int32_t mins = secs / 60;
    secs = secs % 60;
    int32_t hours = mins / 60;
    mins = mins % 60;
    xoh << "<br>(~ " << hours << ":" << (mins > 9 ? "" : "0") << mins << ":" << (secs > 9 ? "" : "0") << secs << ")"
        << endl;
    xoh << "<br>" << "BGO Cntr: " << RED<<StrobeCounter_ << NOCOL << endl;
    stab.NewCell("", 2, 1);
    const LTCStatusInfo* S = boardLockingProxy()->GetLTCStatusInfo();
    {
      const double dt = S->DeltaT(); //difftime(tnow_, tprevious_);
      xoh << UNDERL << "L1A rate (after " << dt << " s):" << NOUNDERL << "<br>" << endl;
      xoh << S->L1ADiff() << " L1As in " << S->OrbitDiff() << " orbs.<br>" << endl;
      if (S->OrbitDiff() > 0 && dt > 0.0)
        xoh << RED<< S->L1ARate() << " Hz" << NOCOL
        << " (" << S->BlockedTriggerRate() << " Hz blocked)" << endl;
      else
        xoh << RED<< "nan" << " Hz" << NOCOL
        << " (" << "nan" << " Hz blocked)" << endl;
      xoh << "<br>" << endl;
      if (S->OrbitDiff() > 0)
      {
        xoh << S->L1AsPerOrbit() << " L1As/orbit <br>" << endl;
      }
      else
      {
        xoh << "nan" << " L1As/orbit <br>" << endl;
      }
      if (S->L1ADiff() > 0)
      {
        xoh << 1.0 / S->L1AsPerOrbit() << " orbits/L1A <br>" << endl;
      }
      else
      {
        xoh << "nan" << " orbits/L1A" << endl;
      }
      if (S->RawDiff() > 0)
      {
        xoh << "<br>Current eff.: " << RED<<100.0 * S->Efficiency() << " %" << NOCOL << endl;
      }
      else
      {
        xoh << "<br>Current eff.: " << RED<< "nan %" << NOCOL << endl;
      }
      stab.NewCell();
      xoh << UNDERL << "BGO. rate (after " << dt << " s):" << NOUNDERL<< "<br>" << endl;
      xoh << S->BGODiff() << " BGOs in " << S->OrbitDiff() << " orbs.<br>" << endl;
      if (dt > 0.0)
      {
        xoh << RED<<S->BGORate() << " Hz" << NOCOL << "<br>" << endl;
      }
      else
      {
        xoh << RED<< "nan" << " Hz" << NOCOL << "<br>" << endl;
      }
      if (S->OrbitDiff() > 0)
      {
        xoh << S->BGOsPerOrbit() << " BGOs/orbit <br>" << endl;
      }
      else
      {
        xoh << "nan" << " BGOs/orbit <br>" << endl;
      }
      if (S->BGODiff() > 0)
      {
        xoh << 1.0 / S->BGOsPerOrbit() << " orbits/BGO <br>" << endl;
      }
      else
      {
        xoh << "nan" << " orbits/BGO" << endl;
      }
    }

    // Empty cell.
    stab.NewCell();

    stab.NewRow();
    stab.NewCell("lightgreen", 1, 1);
    xoh << UNDERL<< "S-Link, TTS, and Trigger Inputs" << NOUNDERL << endl;

    stab.NewCell("lightgreen", 2, 1);

    // S-Link status.
    string slstat;
    boardLockingProxy()->ReadSLinkStatus(slstat, true);

    xoh
        << UNDERL << "S-Link:" << NOUNDERL << "<br>"
        << slstat << (boardLockingProxy()->IsSlinkBackpressureIgnored() ? " (check disabled)" : " (check enabled)")
        << "<br><br>" << endl;

    // TTS status.
    {
      TTSInfo mytts(boardLockingProxy()->ReadTTSStatus());

      xoh << UNDERL << "TTS Status:" << NOUNDERL;
      for (size_t j = 0; j < mytts.NPartitions(); ++j)
      {
        xoh << "<br>";
        if (j == 0)
          xoh << "aTTS: ";
        else
          xoh << "sTTS #" << j << ": ";
        bool enabled = boardLockingProxy()->IsTTSEnabled(j);
        xoh << BOLD << mytts.GetStatus_String(j, true) << NOBOLD;
        if (enabled)
        {
          xoh << " (enabled)" << endl;
        }
        else
        {
          xoh << " (disabled)" << endl;
        }
      }
    }

    // Trigger inputs.
    stab.NewCell("lightgreen", 2, 1);
    // External Triggers
    vector<float> delays = boardLockingProxy()->GetHWInputDelays();
    xoh << BOLD<< UNDERL << "External Triggers Enabled:"
    << NOUNDERL << NOBOLD << endl;

    for (size_t i = 0; i < delays.size(); ++i)
    {
      if (boardLockingProxy()->IsExternalTriggerEnabled(i))
      {
        xoh << "<br>No. " << i << ") " << (i < 2 ? "(nim)" : "(lvds)");
        xoh << " \"" << boardLockingProxy()->GetTriggerName(i) << "\"";
        if (delays[i] > 0)
        {
          xoh << " delay: " << delays[i] << " BX";
        }
        if (S->GetTriggerComposition()->GetTriggerFraction(i) * 100 >= 0.0)
        {
          xoh << " Frac: " << S->GetTriggerComposition()->GetTriggerFraction(i) * 100 << " %";
        }
        xoh << endl;
      }
    }
    xoh << "<br><br>" << endl;
    // RAM triggers.
    xoh << UNDERL << "RAM Triggers:" << NOUNDERL << "<br>" << endl;
    if (!boardLockingProxy()->IsRAMTrigEnabled())
    {
      xoh << "Disabled.";
      xoh << endl;
    }
    else
    {
      if (boardLockingProxy()->GetRAMTriggers()->CanSetInternalTriggerFrequency())
      {
        xoh << "Set to " << boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerFrequency() << " Hz";
      }
      else
      {
        xoh << "trig intervals individually set (c.f. lines containing "
            << "\"TRIGGER_INTERVAL x\" in config. file)";
      }
      if (S->GetTriggerComposition()->GetTriggerFraction(6) * 100 >= 0.0)
      {
        xoh << " Frac: " << S->GetTriggerComposition()->GetTriggerFraction(6) * 100 << " %";
      }
      xoh << endl;
    }

    // Trigger ticket.
    xoh << "<br><br>" << endl;
    xoh << UNDERL<< "L1A/Trigger Tickets:" << NOUNDERL<< "<br>" << endl;
    if (boardLockingProxy()->GetFirmwareVersion() < 12)
    {
      xoh << "(Requires FW Ver 12+)";
    }
    else if (boardLockingProxy()->GetL1ATicket() > 0)
    {
      xoh << boardLockingProxy()->GetL1ATicket() << " L1As per L1ATicket: " << endl;
      if (boardLockingProxy()->ReadTriggerCounter() >= boardLockingProxy()->GetMaxTrigger())
      {
        xoh << RED<< BOLD << "Blocked" << NOBOLD << NOCOL << endl;
      }
      else
      {
        xoh << GREEN << (boardLockingProxy()->GetMaxTrigger() - boardLockingProxy()->ReadTriggerCounter()) << " remaining" << NOCOL << endl;
      }
      }
      else
      {
        xoh << "Off";
      }

    xoh << "<br><br>(L1AEnabled: " << endl;
    if (boardLockingProxy()->IsL1AEnabled())
      xoh << GREEN << "Yes";
    else
      xoh << RED << "No";
    xoh << ")" << NOCOL << endl;

    // Cyclic triggers.
    if (boardLockingProxy()->IsCyclicTrigEnabled())
    {
      for (size_t i = 0; i < boardLockingProxy()->NCyclicTrigger(); ++i)
      {
        CyclicTriggerOrBGO* cycl = boardLockingProxy()->GetCyclic(true, i);
        if (!cycl->IsEnabled())
        {
          continue;
        }
        stab.NewRow();
        stab.NewCell("lightred");
        xoh << "<a href=\"" << url << "CyclicGenerators\">";
        xoh << "Cycl. Trig " << cycl->GetID() << "</a>" << endl;
        stab.NewCell("", 4, 1);
        xoh << (cycl->GetStartBX() > 0 ? emph : deflt) << "StartBX: " << cycl->GetStartBX() << NOCOL
        << "&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->GetPrescale() > 0 ? emph : deflt) << "Prescale: "
            << cycl->GetPrescale() << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->GetInitialPrescale() > 0 ? emph : deflt) << "InitWait: "
            << cycl->GetInitialPrescale() << NOCOL
            << " orbits" << "&nbsp;&nbsp;&nbsp;&nbsp;" << endl;
        xoh << "<br>" << endl;
        xoh << (cycl->GetPostscale() > 0 ? emph : deflt) << cycl->GetPostscale() << " &times;" << NOCOL
        << "&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->GetPause() > 0 ? emph : deflt) << "Pause: " << cycl->GetPause()
            << " orbits" << NOCOL
            << "&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->IsRepetitive() ? deflt : emph)
            << (cycl->IsRepetitive() ? "(repetitive)" : "(non-repetitive)") << "&nbsp;&nbsp;&nbsp;&nbsp;"
            << (cycl->IsPermanent() ? alert : deflt) << (cycl->IsPermanent() ? "(permanent)" : "(non-permanent)")
            << NOCOL << endl;
      }
    }

    // Cyclic BGOs.
    for (size_t i = 0; i < boardLockingProxy()->NCyclicBGO(); ++i)
    {
      CyclicTriggerOrBGO* cycl = boardLockingProxy()->GetCyclic(false, i);
      if (!cycl->IsEnabled())
      {
        continue;
      }
      stab.NewRow();
      stab.NewCell("lightorange");
      xoh << "<a href=\"" << url << "CyclicGenerators\">";
      xoh << "Cycl. BGO " << cycl->GetID() << "</a>" << endl;
      stab.NewCell("", 4, 1);
      xoh << (cycl->GetStartBX() > 0 ? emph : deflt) << "StartBX: " << cycl->GetStartBX() << NOCOL
      << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->GetPrescale() > 0 ? emph : deflt) << "Prescale: "
          << cycl->GetPrescale() << NOCOL
          << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->GetInitialPrescale() > 0 ? emph : deflt) << "InitWait: "
          << cycl->GetInitialPrescale() << " orbits" << NOCOL
          << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" << endl;
      xoh << "Channel: " << emph << cycl->GetBChannel() << NOCOL << " (\"" << emph
          << boardLockingProxy()->GetBGOChannel(cycl->GetBChannel())->GetName() << NOCOL << "\")";
      xoh << "<br>" << endl;
      xoh << (cycl->GetPostscale() > 0 ? emph : deflt) << cycl->GetPostscale() << " &times;" << NOCOL
      << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->GetPause() > 0 ? emph : deflt) << "Pause: "
          << cycl->GetPause() << " orbits" << NOCOL
          << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" << (cycl->IsRepetitive() ? deflt : emph)
          << (cycl->IsRepetitive() ? "(repetitive)" : "(non-repetitive)") << "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
          << (cycl->IsPermanent() ? (cycl->GetBChannel() != 1 ? alert : emph) : deflt)
          << (cycl->IsPermanent() ? "(permanent)" : "(non-permanent)") << NOCOL << endl;
    }

    // Sequences.
    stab.NewRow();
    stab.NewCell("center lightyellow", 5, 1);
    xoh << BOLD<< "<a href=\"" << url << "Sequences\">" << "State-transition sequences</a>" << NOBOLD << endl;
    stab.NewRow();
    for (size_t i = 0; i < 5; ++i)
    {
      bool enabled = false;
      bool disabled = false;
      stab.NewCell("left", 1, 1);
      string myseq;
      if (i == 0)
      {
        myseq = "coldReset";
      }
      else if (i == 1)
      {
        myseq = "configure";
      }
      else if (i == 2)
      {
        myseq = "enable";
      }
      else if (i == 3)
      {
        myseq = "suspend";
      }
      else if (i == 4)
      {
        myseq = "stop";
      }
      else
      {
        assert(false);
      }

      Sequence* seq = boardLockingProxy()->GetSequence(myseq);

      xoh << UNDERL<< BOLD << "\"" << seq->GetName() << "\""
      << NOBOLD << NOUNDERL << endl;
      for (size_t j = 0; j < seq->N(); ++j)
      {
        xoh << "<br>- " << MONO << seq->Get(j) << NOMONO << endl;
        // Check if this command makes sense in this particular
        // sequence.
        if (seq->Get(j) == "DisableL1A")
        {
          disabled = true;
        }
        if (seq->Get(j) == "EnableL1A")
        {
          enabled = true;
        }
        if (myseq == "enable")
        {
          if (seq->Get(j) == "DisableL1A")
          {
            xoh << alert << "<br>&nbsp;&nbsp;disable L1A here?!?" << NOCOL << endl;
          }
          else if (seq->Get(j) == "ResetCounters")
          {
            xoh << alert << "<br>&nbsp;&nbsp;reset counters here?!?" << NOCOL << endl;
          }
        }
        else if ((myseq == "coldReset") || (myseq == "configure") || (myseq == "suspend") || (myseq == "stop"))
        {
          if (seq->Get(j) == "EnableL1A")
          {
            xoh << alert << "<br>&nbsp;&nbsp;enable L1A here?!?" << NOCOL << endl;
          }
        }
      }
      if (myseq == "enable")
      {
        if (!enabled)
        {
          xoh << alert << "<br>No " << MONO << "EnableL1A" << NOMONO
          << " here?!?" << NOCOL << endl;
        }
      }
      else if ((myseq == "coldReset") || (myseq == "configure") || (myseq == "suspend") || (myseq == "stop"))
      {
        if (!disabled)
        {
          xoh << alert << "<br>No " << MONO << "DisableL1A" << NOMONO
          << " here?!?" << NOCOL << endl;
        }
      }
    }

    // Empty cell.
    stab.NewCell("center lightyellow", 3, 1);

    // BX gaps.
    stab.NewRow();
    stab.NewCell("lightred", 5, 1);
    xoh << BOLD<< UNDERL << "Imposed BX Gaps:"
    << NOUNDERL << NOBOLD << endl;
    vector<size_t> begin, end;
    boardLockingProxy()->GetBXGaps(begin, end);
    if (begin.size() == 0)
    {
      xoh << "<br> none" << endl;
    }
    for (size_t i = 0; ((i < begin.size()) && (i < boardLockingProxy()->Nextern())); ++i)
    {
      size_t length = end[i] - begin[i];
      if (begin[i] > end[i])
      {
        length = 3564 - begin[i];
      }
      xoh << "<br> " << i + 1 << ") " << "[" << begin[i] << ", " << end[i] << ") (=" << (length) << " clocks)"
          << endl;
    }

    // User sequences.
    stab.NewRow();
    stab.NewCell("center lightyellow", 5, 1);
    xoh << BOLD << "User sequences" << NOBOLD << endl;
    vector<string> const seqNames = boardLockingProxy()->GetSequenceNames();
    for (vector<string>::const_iterator i = seqNames.begin(); i != seqNames.end(); ++i)
    {
      if ((*i != "coldReset") && (*i != "configure") && (*i != "enable") && (*i != "stop") && (*i != "suspend"))
      {
        stab.NewRow();
        stab.NewCell("center lightyellow", 5, 1);
        Sequence const* const seq = boardLockingProxy()->GetSequence(*i);
        xoh << UNDERL<< BOLD
        << "'" << seq->GetName() << "'"
        << NOBOLD << NOUNDERL
        << endl;
        for (size_t j = 0; j < seq->N(); ++j)
        {
          xoh << "<br>- " << MONO << seq->Get(j) << NOMONO << endl;
        }
      }
    }

    stab.Close();
  }

  {  // General LTC info.
    uint32_t slinkSrcId = boardLockingProxy()->GetSlinkSrcId();
    uint32_t bTimeCorrection = boardLockingProxy()->GetBTimeCorrection();

    // General info.
    xoh << "<br>" << BOLD << "General Info" << NOBOLD << "<br>" << endl
        << "SLink Source ID: " << slinkSrcId << " (0x" << hex << slinkSrcId << dec << ")<br>" << endl
        << "BTimeCorrection: " << bTimeCorrection << " BX " << "<br>" << endl;

    if (!boardLockingProxy()->GetVMEWritesEnabledStatus())
    {
      xoh << RED << "VME writes DISABLED <br/>" << NOCOL << endl;
    }
  }

  PrintHTMLFooter(xoh);
  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageRegisterAccess(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageRegisterAccess");

  XgiOutputHandler xoh(*out);

  xoh << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
  //xoh << cgicc::html().set("lang", "en").set("dir","ltr") << endl;
  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << cgicc::title("LTC \"" + name_.toString() + "\"") << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  PrintHTMLHeader(xoh);

  ErrorStatement(xoh);

  xoh << "Finite State Machine: ";
  PrintFSMState(xoh);
  xoh << cgicc::p() << cgicc::p() << endl;

  {
    HTMLTable tab(xoh, 0, 2, 2, "", "", 100);
    tab.NewCell();
    // DumpVME commands.
    string cmd_url = "/";
    cmd_url += getApplicationDescriptor()->getURN();
    cmd_url += "/SoapCommand";

    xoh
        << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data") << endl
        << cgicc::input().set("type", "submit").set("name", "Command").set("value", "DumpVMEHistory")
        << cgicc::form()
        << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data") << endl
        << cgicc::input().set("type", "submit").set("name", "Command").set("value", "DumpEventFIFO")
        << cgicc::form();

    tab.Close();
  }

  string command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/RegisterAccessCommand";

  xoh
      << cgicc::form().set("method", "post").set("action", command_url).set("enctype", "multipart/form-data") << endl
      << cgicc::fieldset() << endl
      << cgicc::legend("Read from or write to LTC Registers") << endl
      << cgicc::label("Address Name ") << endl;

  VMEAddrList1.Write(xoh);

  xoh
      << cgicc::label("Index ")
      << cgicc::input().set("type", "text").set("name", "index").set("size", "4").set("value", "0") << cgicc::p() << endl
      << "Value " << MONO << "0x" << NOMONO << endl
      << cgicc::input().set("type", "text").set("name", "Value").set("size", "4").set("value", "0") << cgicc::p() << endl
      << endl
      << cgicc::label("Read ")
      << cgicc::input().set("type", "checkbox").set("name", "readchecked") << endl
      << cgicc::label("Write ")
      << cgicc::input().set("type", "checkbox").set("name", "writechecked") << endl
      << cgicc::fieldset() << endl // End Fieldset
      << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply")
      << cgicc::form() << endl;

  command_url = "/";
  command_url += getApplicationDescriptor()->getURN();
  command_url += "/SetControlRegisterBits";

  xoh
      << "<a name=\"regtest\"></a><br>" << endl
      << cgicc::form().set("method", "post").set("action", command_url).set("enctype", "multipart/form-data") << endl
      << cgicc::fieldset() << endl
      << cgicc::legend("Debug the Control Register") << endl;

  uint32_t controlReg = boardLockingProxy()->Read(LTCAdd::CONTROL, "");

  xoh
      << "Control Register: " << MONO << "0x" << hex << controlReg << dec << NOMONO << endl
      << "<a href=\"/" << getApplicationDescriptor()->getURN() << "/" << lastPage_ << "\">" << "[top of page]<a>" << endl
      << " BGOs: " << boardLockingProxy()->ReadStrobeCounter()
      << " EvtID: " << boardLockingProxy()->ReadEventCounter() << endl
      << "<br>" << endl;

  for (int i = 0; i < 32; ++i)
  {
    if (((controlReg >> i) & 1) == 1)
    {
      RegList.SetCheck(31 - i, true);
    }
    else
    {
      RegList.SetCheck(31 - i, false);
    }
  }
  RegList.Write(xoh);

  // End fieldset.

  xoh << cgicc::fieldset() << endl
      << cgicc::input().set("type", "submit").set("name", "submit").set("value", "Apply")
      << cgicc::form() << endl
      << "<br><br>";

  xoh << BOLD << BIGGER
      << "Snap-Shot of Event FIFO:" << NOBIGGER << NOBOLD << "<br>"
      << MONO << endl;

  boardLockingProxy()->DumpEventFIFO(&xoh, true);

  xoh << NOMONO "<br>" << endl;
  PrintHTMLFooter(xoh);

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageRatesPopup(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageRatesPopup");

  XgiOutputHandler xoh(*out);

  const string page_name = "ShowRatesPopup";

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<title>L1A and BGO Rates (LTC)</title>\n";

  // Auto-refresh.
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=" << page_name << "\">" << endl;
  xoh << "</head>\n";

  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  // Print the data.
  {
    const LTCStatusInfo* S = boardLockingProxy()->GetLTCStatusInfo();

    xoh << "<table>\n";
    xoh << "<tr><td>L1 Rate:</td><td>" << S->L1ARate() << " Hz" << "</td></tr>\n";
    xoh << "<tr><td>BGO Rate:</td><td>" << S->BGORate() << " Hz" << "</td></tr>\n";

    uint32_t orbit_counter = boardLockingProxy()->ReadOrbitCounter();

    xoh << "<tr><td>Orbit counter:</td><td>" << orbit_counter << "(~ "
        << GenericTTCModule::OrbitCounterToSecondsString(orbit_counter) << ")" << "</td></tr>\n";

    xoh << "</table>\n";
  }

  xoh << "</body>\n";
  xoh << "</html>\n";
}


void ttc::LTCControl::HTMLPageCommandConfigureTTS(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandConfigureTTS");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Configure TTS and S-Link") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  TTSInfo tts(boardLockingProxy()->ReadTTSStatus());

  for (size_t i = 0; i < tts.NPartitions(); ++i)
  {
    stringstream stm;
    stm << "TTS " << i;
    bool enable = cgi.queryCheckbox(stm.str());
    boardLockingProxy()->EnableTTS(i, enable);
  }
  // Ignore/act on s-link back-pressure:
  boardLockingProxy()->IgnoreSlinkBackpressure(!cgi.queryCheckbox("slinkbackpressure"));
  // "warningpause"
  uint32_t warnpause;
  stringstream g(cgi["warningpause"]->getValue());
  if (g >> warnpause)
  {
    boardLockingProxy()->SetWarningInterval(warnpause);
  }
  else
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Don't understand TTS 'Warning' pause interval "
        "'" + cgi["warningpause"]->getValue() + "'");
  }

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandNewConfigFile(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandConfigureTTS");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("New Configuration File") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  std::string fileName = cgi["ConfigurationFile"]->getValue();
  string file_radiobutton = cgi["inputfiles"]->getValue();

  string newpath;

  if (file_radiobutton == "other" && fileName.size() > 0)
    newpath = fileName;
  else
    newpath = file_radiobutton;

  xoh << "New path to configuration file = " << newpath << "<br>" << endl;

  FILE* fp = fopen(newpath.c_str(), "r");
  if (!fp)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Unable to open file '" + newpath + "' for reading");
  }
  else
  {
    asciConfigurationFilePath_ = newpath;
    ReadConfigFromFile_ = true;
    fclose(fp);
  }

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandWriteConfigFile(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandWriteConfigFile");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"2; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Write Configuration File") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  string fileName = cgi["ConfigurationFile"]->getValue();

  if (fileName.size() > 0)
  {
    asciConfigurationFilePath_ = fileName;
    ReadConfigFromFile_ = true;
  }
  else
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Path to new configuration file is empty");
  }

  bool overwrite = cgi.queryCheckbox("overwrite");
  if (!overwrite)
  {
    FILE* fp = fopen(asciConfigurationFilePath_.toString().c_str(), "r");
    if (fp)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "File '" + asciConfigurationFilePath_.toString() + "' "
          "already exists, but 'force overwrite' was not checked!");
    }
  }

  ofstream oo(asciConfigurationFilePath_.toString().c_str(), ios_base::out);
  if (!oo)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Unable to open file '" + asciConfigurationFilePath_.toString() + "' for writing");
  }

  boardLockingProxy()->WriteConfiguration(oo, string("(from LTCControl::") + string("WriteConfigurationFile())"));
  LOG4CPLUS_INFO(logger_, "Configuration written to file '" + asciConfigurationFilePath_.toString() + "'");

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandMainConfig(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandMainConfig");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Main Configuration") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  // INPUTS:
  bool cyclic = false, ram = false;
  for (size_t i = 0; i < Trigselector.size(); ++i)
  {
    stringstream g;
    g << Trigselector.GetName() << "__" << Trigselector.GetValue(i);
    bool checked = cgi.queryCheckbox(g.str().c_str());
    int ii = atoi(Trigselector.GetValue(i).c_str());
    if (ii < int(boardLockingProxy()->Nextern()))
    {
      boardLockingProxy()->EnableExternalTrigger(ii, checked);
    }
    else if (ii == int(boardLockingProxy()->Nextern()))
    {
      cyclic = checked;
    }
    else if (ii == 1 + int(boardLockingProxy()->Nextern()))
    {
      ram = checked;
    }
    else
    {
      stringstream msg;
      msg << "LTCControl::HTMLPageMainConfigCommand(): " << "Unknown trigsel=" << ii << " extracted from '" << g.str() << "'";
      LOG4CPLUS_ERROR(logger_, msg.str());
    }
  }
  if (boardLockingProxy()->GetRAMTriggers()->CanSetInternalTriggerFrequency())
  {
    double myValue = cgi["TriggerFrequency"]->getDoubleValue(0.0);
    bool myRandom = cgi.queryCheckbox("TriggerRandom");
    if ((myValue > 0.0) && (myValue <= 10000000.0))
    {
      if ((oldfrequency_ != myValue) || (myRandom != boardLockingProxy()->GetRAMTriggers()->GetInternalTriggerRandom()))
      {
        boardLockingProxy()->GetRAMTriggers()->SetInternalTriggerFrequencyLTC(myValue, myRandom);
        oldfrequency_ = myValue;
      }
    }
    else
    {
      LOG4CPLUS_ERROR(
          logger_,
          toolbox::toString("LTCControl::HTMLPageMainConfigCommand(): Unable to extract trigger frequency from \"%s\"!", cgi["TriggerFrequency"]->getValue().c_str()));
    }
  }
  boardLockingProxy()->EnableRAMTrig(ram);
  boardLockingProxy()->StartRAMTrigs();
  boardLockingProxy()->EnableCyclicTrig(cyclic);

  // QPLL
  string str_external = cgi["QPLLext"]->getValue();
  const bool external = (str_external == "YES");

  boardLockingProxy()->SetQPLLExternal(external);
  if (external)
  {
    cgicc::const_form_iterator cgi_it;

    if (CgiUtils::CgiccHasParameter(cgi, "QPLLReset"))
    {
      cgi_it = cgi.getElement("QPLLReset");
      if (cgi_it->getValue() == "YES")
      {
        boardLockingProxy()->ResetQPLL(true);
      }
      else
      {
        boardLockingProxy()->ResetQPLL(false);
      }

    } // if paramter QPLLReset was present

    if (CgiUtils::CgiccHasParameter(cgi, "QPLLAutoRestart"))
    {
      cgi_it = cgi.getElement("QPLLAutoRestart");

      if (cgi_it->getValue() == "ON")
      {
        boardLockingProxy()->AutoRestartQPLL(true);
      }
      else
      {
        boardLockingProxy()->AutoRestartQPLL(false);
      }
    } // if parameter QPLLAutoRestart was given
  } // if external

  if (CgiUtils::CgiccHasParameter(cgi, "freqbits"))
  {
    uint32_t freqbits;
    stringstream g(cgi["freqbits"]->getValue());
    g >> hex >> freqbits;
    boardLockingProxy()->SetQPLLFrequencyBits(freqbits, external);
  }

  // Trigger burst.
  if (cgi.queryCheckbox("bursten"))
  {
    unsigned trigb = 0;
    if (sscanf(cgi["trigburst"]->getValue().c_str(), "%u", &trigb) != 1)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "Don't understand trigger burst size '" + cgi["trigburst"]->getValue() + "'");
    }

    if (trigb == 0)
      trigb = 10;

    boardLockingProxy()->SetL1ATicket(trigb);
  }
  else
  {
    boardLockingProxy()->SetL1ATicket(0);
  }

  // Monitoring.
  bool domonitoring = cgi.queryCheckbox("domonitoring");

  boardLockingProxy()->EnableMonitoring(domonitoring);

  if (domonitoring)
  {
    float monintv = -99.0;
    if (sscanf(cgi["monitoringintv"]->getValue().c_str(), "%f", &monintv) == 1)
    {
      boardLockingProxy()->SetMonitoringInterval(monintv);
    }
    else
    {
      XCEPT_RAISE(
          xcept::Exception,
          "Don't understand monitoring interval '" + cgi["monitoringintv"]->getValue() + "'");
    }
  }

  if (!boardLockingProxy()->DefaultFIFODumpEnabled())
  {
    bool dofifodump = cgi.queryCheckbox("dofifodump");
    string fifopth = string("");
    if (dofifodump)
    {
      fifopth = cgi["fifodumpfile"]->getValue();
    }
    boardLockingProxy()->SetFilePathForFIFODump(dofifodump, fifopth);
  }

  // GPS/BST through VME:
  boardLockingProxy()->EnableBSTGPSviaVME(cgi.queryCheckbox("gpsfromvme"));

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandTriggerRulesDelays(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandTriggerRulesDelays");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Configure Trigger Rules and Hardware Trigger Delays") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  // Trigger Rules
  for (size_t i = 0; i < boardLockingProxy()->TriggerRuleSize(); ++i)
  {
    char dum1[50];
    sprintf(dum1, "trigrule%zu", (i + boardLockingProxy()->FirstTriggerRule()));
    uint32_t myValue = cgi[dum1]->getIntegerValue(0);
    boardLockingProxy()->SetTriggerRule((i + boardLockingProxy()->FirstTriggerRule()), myValue);
  }

  // HW Trigger Delays:
  vector<float> delays;
  for (size_t i = 0; i < boardLockingProxy()->Nextern(); ++i)
  {
    char dum1[50];
    sprintf(dum1, "trigdelay %zu", i);
    delays.push_back(cgi[dum1]->getDoubleValue(0));
  }
  boardLockingProxy()->SetHWInputDelays(delays);

  for (size_t i = 0; i < boardLockingProxy()->Nextern(); ++i)
  {
    char dum1[50], dum2[100];
    sprintf(dum1, "trigname %zu", i);
    boardLockingProxy()->SetTriggerName(i, cgi[dum1]->getValue());
    sprintf(dum2, "External trigger %zu (%s): %s", i, (i < 2 ? "nim" : "lvds"),
        boardLockingProxy()->GetTriggerName(i).c_str());
    Trigselector.SetTitle(i, string(dum2));
  }

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandVMEBGOTiming(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandVMEBGOTiming");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("VME-BGO timing") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  // which BX for following VME BGOs?
  uint32_t mybx = cgi["atbx"]->getIntegerValue(0);

  if (mybx >= NClocksPerOrbit())
  {
    ostringstream err;
    err << "Argument for BX=" << mybx << "is out of range";
    XCEPT_RAISE(xcept::Exception, err.str());
  }

  boardLockingProxy()->SetVMEBX(mybx);

  try {
    boardLockingProxy()->CheckBXConflicts();
  }
  catch(ttc::exception::BXConflictsDetected& e)
  {
    XCEPT_RETHROW(xcept::Exception, "BX conflicts detected", e);
  }

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandSequenceAction(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandSequenceAction");

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  // The Sequence Selection: which sequence?
  string seqname = cgi["sequenceselect"]->getValue();
  string seqchoice = cgi["seqchoice"]->getValue();

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Sequence '" + seqname + "', Action '" + seqchoice + "'") << endl;

  if (seqchoice == "remove")
  {
    boardLockingProxy()->DeleteSequence(seqname);
  }
  else if (seqchoice == "add")
  {
    string newname = cgi["newsequence"]->getValue();
    if (newname.size() > 0)
    {
      boardLockingProxy()->AddSequence(newname);
      SequenceSelector.push_back(newname);
      SequenceSelector.SetDefault(newname);
    }
  }
  else if (seqchoice == "exec")
  {
    SequenceSelector.SetDefault(seqname);
    boardLockingProxy()->ExecuteSequence(seqname);
  }
  else if (seqchoice == "display")
  {
    SequenceSelector.SetDefault(seqname);
  }
  else {
    XCEPT_RAISE(xcept::Exception, "Invalid sequence action '" + seqchoice + "'");
  }

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandEditSequence(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandEditSequence");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Edit sequence") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  // Which sequence line (or eppend)?
  string line = cgi["sequenceline"]->getValue();

  // Which action to take?
  string edit = cgi["edit"]->getValue();

  // Which action to take?
  string command = cgi["edit2"]->getValue();
  string command2 = cgi["edit3"]->getValue();

  // Which action to take?
  string xstring = cgi["x"]->getValue();
  int32_t x = -99;

  if (!String2Long(xstring, x))
    x = -99;

  const string SeqTitle = SequenceSelector.GetDefaultTitle();

  Sequence* myseq = boardLockingProxy()->GetSequence(SeqTitle);

  //----------------------------------------
  // Delete a line from a sequence.
  //----------------------------------------
  if (edit == "delete")
  {
    if (line == "< Append >")
    {
      XCEPT_RAISE(
          xcept::Exception,
          "To delete, you have to choose a line other than '< Append >'");
    }
    else
    {
      int index = myseq->GetIndex(line);
      if (index < 0)
      {
        XCEPT_RAISE(
            xcept::Exception,
            "Unable to find line '" + line + "' in sequence '" + SeqTitle + "'");
      }
      myseq->Delete(size_t(index));
    }
  }

  //----------------------------------------
  // Replace (modify) or create a new line in a sequence.
  //----------------------------------------
  else if (edit == "modify" || edit == "new")
  {
    if (command == "Choose" && command2 == "Choose")
    {
      XCEPT_RAISE(
          xcept::Exception,
          "To '" + edit + "' you have to select a command other than '" + command + "'");
    }

    string newline = command;
    if (command == "Choose")
    {
      newline = command2;
    }
    else
    {
      // Commands which have a parameter.
      if (command == "Sleep" ||
          command == "mSleep" ||
          command == "uSleep" ||
          command == "BGO" ||
          command == "Periodic" ||
          command == "Monitoring" ||
          command == "EnableCyclicBgo" ||
          command == "DisableCyclicBgo" ||
          command == "EnableCyclicTrigger" ||
          command == "DisableCyclicTrigger")
      {
        bool is_good = true;

        if (x == -99)
          is_good = false;

        if (command == "Sleep" && x < 0)
          is_good = false;

        if (command == "BGO" && (x < 0 || x > int(boardLockingProxy()->NChannels())))
          is_good = false;

        if (command == "EnableCyclicBgo" || command == "DisableCyclicBgo")
        {
          if (x < 0)
            is_good = false;

          if (size_t(x) >= boardLockingProxy()->NCyclicBGO())
            is_good = false;
        }

        if (command == "EnableCyclicTrigger" || command == "DisableCyclicTrigger")
        {
          if (x < 0)
            is_good = false;

          if (size_t(x) >= boardLockingProxy()->NCyclicTrigger())
            is_good = false;
        }

        if (!is_good)
        {
          XCEPT_RAISE(
              xcept::Exception,
              "Invalid value '" + xstring + "' for command " + command + "'");
        }

        // Append parameter to the command.
        newline = newline + " " + xstring;
      }
    }

    if (line == "< Append >")
    {
      myseq->PushBack(newline);
    }
    else
    {
      int index = myseq->GetIndex(line);
      if (index < 0)
      {
        XCEPT_RAISE(
            xcept::Exception,
            "Unable to find line '" + line + "' in sequence '" + SeqTitle + "'");
      }

      if (edit == "new")
      {
        myseq->Insert((size_t) index, newline);
      }
      else if (edit == "modify")
      {
        myseq->Set((size_t) index, newline);
      }
      else
      {
        XCEPT_RAISE(
            xcept::Exception,
            "Unknown action '" + edit + "'");
      }
    }
  } // Modify a line or add a new line.

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandConfigureCyclicGenerator(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandConfigureCyclicGenerator");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Cyclic Generator Configuration") << endl;

  // Create a new Cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);

  // Loop over all cyclic generators.
  for (size_t ik = 0; ik < (boardLockingProxy()->NCyclicBGO() + boardLockingProxy()->NCyclicTrigger()); ++ik)
  {
    char genc[23];
    sprintf(genc, "%zu__", ik);
    const string sgen = string(genc);
    bool trigger = (ik >= boardLockingProxy()->NCyclicBGO());
    const size_t i = (!trigger ? ik : ik - boardLockingProxy()->NCyclicBGO());
    CyclicTriggerOrBGO* cycl = boardLockingProxy()->GetCyclic(trigger, i);

    // start-BX
    uint32_t val = cgi[(sgen + "startbx").c_str()]->getIntegerValue(0);
    cycl->SetStartBX(val);
    // prescale
    val = cgi[(sgen + "prescale").c_str()]->getIntegerValue(0);
    cycl->SetPrescale(val);
    // initprescale
    val = cgi[(sgen + "initprescale").c_str()]->getIntegerValue(0);
    cycl->SetInitialPrescale(val);
    // Postscale
    val = cgi[(sgen + "postscale").c_str()]->getIntegerValue(0);
    cycl->SetPostscale(val);
    // Pause
    val = cgi[(sgen + "pause").c_str()]->getIntegerValue(0);
    cycl->SetPause(val);
    // Select the B-Channel:
    if (!trigger)
    {
      string channel = cgi[(sgen + "channelno").c_str()]->getValue();
      val = cgi[(sgen + "channelno").c_str()]->getIntegerValue();
      cycl->SetBChannel(val);
    }
    // repetitive
    bool boolval = cgi.queryCheckbox((sgen + "repetitive").c_str());
    cycl->SetRepetitive(boolval);
    // repetitive
    boolval = cgi.queryCheckbox((sgen + "permanent").c_str());
    cycl->SetPermanent(boolval);
    // enable
    cgicc::const_form_iterator EnableRadio = cgi.getElement((sgen + "enable").c_str());
    cycl->SetEnable((EnableRadio->getValue() == "enable"));

    boardLockingProxy()->WriteCyclicGeneratorToLTC(trigger, i);
  }

  try {
    boardLockingProxy()->CheckBXConflicts();
  }
  catch(ttc::exception::BXConflictsDetected& e)
  {
    XCEPT_RETHROW(xcept::Exception, "BX conflicts detected", e);
  }

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandReadCyclicGeneratorConfig(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandReadCyclicGeneratorConfig");

  XgiOutputHandler xoh(*out);

  boardLockingProxy()->ReadAllCyclicGeneratorsFromLTC();

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Read Cyclic Generator Configuration from Hardware") << endl;

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageCommandRegisterAccess(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageCommandRegisterAccess");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
//  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

//  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Read/Write VME Register - Results:") << endl;

  // Create a new Cgicc object containing all the CGI data
  cgicc::Cgicc cgi(in);

  const string parName = cgi["Addresses"]->getValue();

  if (parName != "none")
  {
    bool read = cgi.queryCheckbox("readchecked");
    bool write = cgi.queryCheckbox("writechecked");

    const uint32_t index = cgi["index"]->getIntegerValue(0);

    uint32_t myValue;
    stringstream g(cgi["Value"]->getValue());
    g >> hex >> myValue;

    if (read || write)
      xoh << "<ul>" << endl;

    if (read)
    {
      xoh << "<li>" << endl;
      xoh << "Reading from VME address with name '" << MONO<<parName << NOMONO<< "', index=" << dec << index
          << ": <br>" << endl;
      uint32_t output = boardLockingProxy()->Read(parName, index);
      xoh << MONO<<BLUE<< "0x" <<hex<<output<<dec<< " = " <<output
      << NOCOL <<NOMONO<< endl;
      if (write)
      {
        xoh << BLUE << " (before)." << NOCOL << endl;
      }
      xoh << "</li>" << endl;
    }

    if (write)
    {
      xoh << "<li>" << endl;
      xoh << "Writing 0x" << hex << myValue << dec << " = " << myValue << " to VME address with name '"
          << MONO<<parName << NOMONO
          << "', index=" << dec << index << endl;
      boardLockingProxy()->Write(parName, index, myValue);
      xoh << "</li>" << endl;
      if (read)
      {
        xoh << "<li>" << endl;
        xoh << "Reading (again) from VME address with name '" << MONO<<parName << NOMONO
        << "', index=" << dec << index << ": <br> " << endl;
        uint32_t output = boardLockingProxy()->Read(parName, index);
        xoh << MONO<<BLUE<< "0x" <<hex<<output<<dec<< " = " <<output
        <<NOMONO<< NOCOL << endl
        << BLUE << " (after)." << NOCOL << endl;
        xoh << "</li>" << endl;
      }
    }

    if (read || write)
      xoh << "</ul>" << endl;
  }

  string oldurl = "/" + getApplicationDescriptor()->getURN() + "/";
  xoh << BOLD<<BIGGER;
  xoh << "<a href=\"" << oldurl << "RegisterAccess\"> Back to previous page </a>";
  xoh << NOBIGGER<<NOBOLD<< "<br>" << endl;

  xoh << "</body></html>\n";
}


void ttc::LTCControl::HTMLPageSetControlRegisterBits(xgi::Input* in, xgi::Output* out)
{
  LOG4CPLUS_INFO(logger_, "LTCControl::HTMLPageSetControlRegisterBits");

  XgiOutputHandler xoh(*out);

  xoh << "<html>\n";
  xoh << "<head>\n";
  xoh << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl;
  xoh << "</head>\n";
  xoh << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  RedirectMessage(&xoh, lastPage_);
  xoh << cgicc::h1("Set Control Register Bits") << endl;

  cgicc::Cgicc cgi(in);

  uint32_t dum = 0;
  for (size_t i = 0; i < RegList.size(); ++i)
  {
    stringstream g;
    g << RegList.GetName() << "__" << RegList.GetValue(i);
    if (cgi.queryCheckbox(g.str().c_str()))
    {
      dum |= (1 << (31 - i));
    }
  }

  boardLockingProxy()->Write(LTCAdd::CONTROL, dum, "(LTCControl Register)");

  xoh << "</body>" << "</html>" << endl;
}


void ttc::LTCControl::initHTMLFields()
{
  if (areHTMLFieldsInited())
    return;

  {
    // Trigger selection:
    vector<string> values, titles;
    char dum[100];

    for (size_t i = 0; i < boardLockingProxy()->Nextern(); ++i)
    {
      sprintf(dum, "%zu", i);
      values.push_back(dum);
      sprintf(dum, "External trigger %zu (%s): %s", i, (i < 2 ? "nim" : "lvds"),
          boardLockingProxy()->GetTriggerName(i).c_str());
      titles.push_back(dum);
    }
    sprintf(dum, "%zu", boardLockingProxy()->Nextern());
    values.push_back(dum);
    titles.push_back("Cyclic Trigger Generators");
    sprintf(dum, "%zu", boardLockingProxy()->Nextern() + 1);
    values.push_back(dum);
    titles.push_back("RAM Trigger");
    Trigselector.Set(HTMLFieldElement::CHECKBOX, 0, "trigselect", values, titles, "vertical green bold");
  }

  { // BGO selection
    vector<string> values, titles;
    values.push_back("0");
    titles.push_back("Cyclic BGO Generators (should be \"on\")");
    BGOselector.Set(HTMLFieldElement::CHECKBOX, 0, "bgoselect", values, titles, "vertical green bold");
  }

  { // The 16 BGO Channels
    vector<string> values, titles;
    for (size_t i = 0; i < boardLockingProxy()->NChannels(); ++i)
    {
      char si[100];
      sprintf(si, "%zd", i);
      values.push_back(si);
      string name = boardLockingProxy()->GetBGOChannel(i)->GetName();
      if (name[0] != 'C' || name[1] != 'h' || name[2] != 'a')
      {
        name += (string(" (") + string(si) + string(")"));
      }
      titles.push_back(name);
    }
    CurrentBGOList.Set(HTMLFieldElement::DROPDOWNMENU, 0, "BGOselect", values, titles);
    CurrentBGOList.SetDefault(0);
  }
  { // The Cyclic Generators
    vector<string> values, titles;
    int jj = 0;
    for (size_t i = 0; i < boardLockingProxy()->NCyclicTrigger(); ++i)
    {
      titles.push_back(boardLockingProxy()->GetCyclic(true, i)->GetName());
      char tit[11];
      sprintf(tit, "%d", jj++);
      values.push_back(tit);
    }
    for (size_t i = 0; i < boardLockingProxy()->NCyclicBGO(); ++i)
    {
      titles.push_back(boardLockingProxy()->GetCyclic(false, i)->GetName());
      char tit[11];
      sprintf(tit, "%d", jj++);
      values.push_back(tit);
    }
    CurrentCyclicList.Set(HTMLFieldElement::DROPDOWNMENU, 0, "cyclicselect", values, titles);
    CurrentCyclicList.SetDefault(values[0]);
  }
  { // VME registers
    vector<string> add1, add1_tit;
    boardLockingProxy()->GetVMEAddresses(add1, add1_tit);
    add1.insert(add1.begin(), string("none"));
    add1_tit.insert(add1_tit.begin(), string("Choose parameter"));
    VMEAddrList1.Set(HTMLFieldElement::DROPDOWNMENU, 0, "Addresses", add1, add1_tit);
    VMEAddrList1.SetDefault("none");
  }

  {
    vector<string> names, titles;
    for (int i = 31; i >= 0; --i)
    {
      stringstream gg, gn;
      gg << "Bit " << (i < 10 ? " " : "") << i;
      titles.push_back(gg.str());
      gn << i;
      names.push_back(gn.str());
    }
    RegList.Set(HTMLFieldElement::CHECKBOX, 0, "singleregister", names, titles, "vertical green bold");
  }

  {
    vector<string> values, titles;
    values.push_back("Short");
    titles.push_back("Short");
    values.push_back("Long");
    titles.push_back("Long");
    values.push_back("A-Command");
    titles.push_back("A-Cmnd/L1A");
    DataTypeSelector.Set(HTMLFieldElement::DROPDOWNMENU, 0, "datatypeselect", values, titles);
    DataTypeSelector.SetDefault(values[0]);
  }

  {
    const vector<string> values = boardLockingProxy()->GetSequenceNames();
    SequenceSelector.Set(ttc::HTMLFieldElement::DROPDOWNMENU, 0, "sequenceselect", values, values);
    SequenceSelector.SetDefault(values[0]);
  }

  setHTMLFieldsInited();
}


void ttc::LTCControl::ErrorStatement(xgi::Output& out)
{
  try {
    boardLockingProxy()->CheckBXConflicts();
  }
  catch(ttc::exception::BXConflictsDetected& e)
  {
    error_messages.add("ERROR") << "BX conflicts detected: " << string(e.what());
  }

  if (boardLockingProxy()->IsQPLLExternal() && !boardLockingProxy()->IsClockLocked())
  {
    error_messages.add("ERROR") << "LTC Clock NOT locked!";
  }

  out << "<div id=\"" << ERROR_MESSAGES_DIV << "\">" << endl;

  if (!error_messages.isEmpty())
  {
    out << error_messages.asHtml() << endl;
    error_messages.clear();
  }

  out << "</div>" << endl;
}


string ttc::LTCControl::GetDefaultSequenceName()
{
  return SequenceSelector.GetDefaultTitle();
}


void ttc::LTCControl::ReadLTCCounters()
{
  TriggerCounter_ = boardLockingProxy()->ReadTriggerCounter();
  EventCounter_ = boardLockingProxy()->ReadEventCounter();
  OrbitCounter_ = boardLockingProxy()->ReadOrbitCounter();
  StrobeCounter_ = boardLockingProxy()->ReadStrobeCounter();
  BlockedL1ACntr_ = boardLockingProxy()->ReadBlockedTriggersCounter();
  BoardStatus_ = boardLockingProxy()->BoardStatus();
}


void ttc::LTCControl::GetFileList()
{
  char s[1024];
  gethostname(s, sizeof s);
  string machine = s;
  size_t pos = machine.find_first_of(".");
  if (pos < machine.size())
    machine.erase(machine.begin() + pos, machine.end());
  machine = string(" (on ") + MONO + machine + NOMONO + ")";

  string path = "", suffix = "";
  vector<string> filelist, filelistvar;
  if (ReadConfigFromFile_)
  {
    path = asciConfigurationFilePath_.toString();
    filelist.push_back(string(MONO) + path + string(NOMONO) + machine);
    filelistvar.push_back(path);
    size_t pos = path.find_last_of("/");
    if (pos < path.size())
    {
      suffix = path;
      path.erase(path.begin() + pos, path.end());
      // find the ending
      suffix.erase(suffix.begin(), suffix.begin() + pos + 1);
      pos = suffix.find_last_of(".");
      if (pos == 0)
        suffix = "";
      else if (pos < suffix.size())
        suffix.erase(suffix.begin(), suffix.begin() + pos);
      else
      {
        LOG4CPLUS_ERROR(logger_, "No file ending found in '" + suffix + "'");
        suffix = "";
      }
    }
  }
  vector<string> addlist = ttc::filelist(path, "", (suffix = ".dat"));
  for (size_t i = 0; i < addlist.size(); ++i)
  {
    if (filelistvar.size() > 0 && addlist[i] == filelistvar[0])
      continue;
    filelist.push_back(string(MONO) + addlist[i] + string(NOMONO) + machine);
    filelistvar.push_back(addlist[i]);
  }
  addlist = ttc::filelist(path, "", (suffix = ".txt"));
  for (size_t i = 0; i < addlist.size(); ++i)
  {
    if (filelistvar.size() > 0 && addlist[i] == filelistvar[0])
      continue;
    filelist.push_back(string(MONO) + addlist[i] + string(NOMONO) + machine);
    filelistvar.push_back(addlist[i]);
  }
  filelist.push_back("other" + machine + ":");
  filelistvar.push_back("other");
  InputFileList.Set(HTMLFieldElement::RADIOBUTTON, 0, "inputfiles", filelistvar, filelist, "vertical bold blue");
  InputFileList.SetDefault(filelistvar[0]);
}


void ttc::LTCControl::WriteBinaryRadioButton(
    ostream& out,
    const string& radio_group_name,
    const string& text1,
    const string& text2,
    const string& value1,
    const string& value2,
    unsigned selected_index,
    bool disabled)
{
  const string *texts[2] = {&text1, &text2};
  const string *values[2] = {&value1, &value2};

  for (unsigned i = 0; i < 2; ++i)
  {
    out << "<input type=\"radio\" name=\"" << radio_group_name << "\" value=\"" << (*values[i]) << "\"";
    if (i == selected_index)
      out << " checked=\"checked\"";

    if (disabled)
      out << " disabled";

    out << "/>";

    if (i == selected_index)
      out << "<u>";
    out << (*texts[i]);
    if (i == selected_index)
      out << "</u>";

    out << "</input>" << endl;
  }
}
