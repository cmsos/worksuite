#ifndef _ttc_monitoring_periodiccallback_h_
#define _ttc_monitoring_periodiccallback_h_


#include "ttc/monitoring/PeriodicListener.h"

#include <sstream>


namespace ttc
{

class ItemSetterSignature
{
public:
  virtual void invoke(
      const Component& component,
      std::ostringstream& value) = 0;

  virtual ~ItemSetterSignature() {}
};


template<class C>
class ItemSetter : public ItemSetterSignature
{
public:
  typedef void (C::*Func)(const Component&, std::ostringstream&);

  ItemSetter(C* obj, Func f) : obj_(obj), f_(f)
  {
    if (!obj_ || !f_)
    {
      XCEPT_RAISE(xcept::Exception, "Undefined obj_ or f_");
    }
  }

  virtual void invoke(
      const Component& component,
      std::ostringstream& value)
  {
    (obj_->*f_)(component, value);
  }

private:
  C* obj_;
  Func f_;
};


/**
 * This class is a PeriodicListener (with a user defined list of identical monitored components),
 * where the monitoring items for each component are registered with individual callback methods.
 */
class PeriodicCallback : public PeriodicListener
{
public:

  PeriodicCallback(
      xdaq::Application* app,
      const std::string& infospaceBaseName,
      bool autoInit,
      const double& refreshPeriodSecs,
      const double& refreshOffsetSecs,
      const uint16_t& forceUpdateEvery);

  virtual ~PeriodicCallback();

protected:

  /**
   * Defines an item and a corresponding refresh-callback, which gets called for all defined components.
   *
   * NOTE: For a given component, the order in which items are updated
   * corresponds to the order in which they are added by this method.
   * (This can be exploited if the update action for some item depends on a prior update of some other item.)
   */
  template<class T, class C>
  void addItemAndCallback(const std::string& name,
      C* obj,
      typename ItemSetter<C>::Func f)
  {
    PeriodicListener::addItem<T>(name);
    ItemSetter<C>* s = new ItemSetter<C>(obj, f);
    callbackItemNames_.push_back(name);
    mapCallbackItemNameToItemSetter_[name] = s;
  }

private:

  //! implements ttc::PeriodicListener::refreshItems
  virtual bool refreshItems(const Component& component);

  std::list<std::string> callbackItemNames_;
  std::map<std::string, ItemSetterSignature*> mapCallbackItemNameToItemSetter_;
};

}


#endif
