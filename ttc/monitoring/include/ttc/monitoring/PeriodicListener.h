#ifndef _ttc_monitoring_periodiclistener_h_
#define _ttc_monitoring_periodiclistener_h_


#include "ttc/monitoring/DataListener.h"

#include "toolbox/task/TimerListener.h"

#include <stdint.h>
#include <string>


namespace toolbox
{
namespace task { class Timer; }
}


namespace ttc
{

class PeriodicListener
:
  public DataListener,
  public toolbox::task::TimerListener
{

public:

  /**
   * Constructs a PeriodicListener for XDAQ application app.
   *
   * This monitors components and their items periodically.
   * See documentation of class DataListener for base class functionality and parameters.
   *
   * The constructor of this class takes in addition the following parameters:
   * -  @param refreshPeriodSecs = the period of regular refresh actions.
   * -  @param refreshOffsetSecs = the global time offset for refresh actions.
   * -  @param forceUpdateEvery = the number of refresh periods after which, periodically,
   *       updates are enforced (i.e. data are pushed to XMAS even if they did not change).
   */
  PeriodicListener(
      xdaq::Application* app,
      const std::string& infospaceBaseName,
      bool autoInit,
      const double& refreshPeriodSecs,
      const double& refreshOffsetSecs,
      const uint16_t& forceUpdateEvery);

  virtual ~PeriodicListener();

protected:

  //! Returns the period of regular refresh actions.
  double getRefreshPeriodSecs();

  //! Returns the global time offset for refresh actions.
  double getRefreshOffsetSecs();

  //! Returns the number of refresh periods after which, periodically, updates are enforced.
  uint16_t getForceUpdateEvery();

private:

  double refreshPeriodSecs_;

  double refreshOffsetSecs_;

  uint16_t forceUpdateEvery_;

  double xmasAlmostReadyTimeSecs_;

  double xmasReadyTimeSecs_;

  std::string timerName_;

  toolbox::task::Timer* timer_;


  //! Launches the monitoring.
  virtual void run();

  /**
   * IMPLEMENTS toolbox::task::TimerListener::timeExpired.
   * Locks infospace and calls refreshComponents() and scheduleNextRefresh(), catching all exceptions.
   */
  virtual void timeExpired(toolbox::task::TimerEvent& e);
  
  //! Schedules nearest future event in global time grid [1970 + n*refreshPeriodSecs_].
  void scheduleNextRefresh();
};

}


#endif
