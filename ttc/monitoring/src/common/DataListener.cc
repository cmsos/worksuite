#include "ttc/monitoring/DataListener.h"

#include "toolbox/net/URN.h"
#include "xdaq/Application.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/exception/Exception.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/TimeVal.h"

#include <exception>


using namespace std;


ttc::DataListener::DataListener(
    xdaq::Application* app,
    const string& infoSpaceBaseName,
    bool autoInit)
:
    app_(app),
    infoSpaceBaseName_(infoSpaceBaseName),
    initAlreadyCalled_(false)
{
  // create/get named logger
  string loggerName = app_->getApplicationLogger().getName() + ".DataListener." + infoSpaceBaseName_;
  logger_ = log4cplus::Logger::getInstance(loggerName);

  if (autoInit)
  {
    // This binds to the hook called after all values have been loaded
    // from the configuration file.
    app_->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
  }
}


ttc::DataListener::~DataListener()
{
  // delete InfoSpace items for all InfoSpaces (components)
  const vector<string>& vecComponentKeys = componentColl_.getComponentKeys();
  
  for (vector<string>::const_iterator
      it = vecComponentKeys.begin();
      it!= vecComponentKeys.end(); ++it)
  {
    const string& componentKey = *it;
    xdata::InfoSpace* infoSpace = getInfoSpace(componentKey);
  
    InfoSpaceLock lock(infoSpace);
    InfoSpaceLockHandler h(lock);
  
    for (map<string, xdata::Serializable*>::const_iterator
        it = infoSpace->begin();
        it != infoSpace->end(); ++it)
    {
      delete it->second;
    }
  }
}


void ttc::DataListener::init()
{

  // only call init once: even if it fails we want to make sure its execution is not attempted several times.
  if (initAlreadyCalled_)
  {
    LOG4CPLUS_WARN(getLogger(), "Repeated attempt to init()! Skipping.");
    return;
  }
  initAlreadyCalled_ = true;

  // add user-defined components
  addComponents();
  
  const vector<string>& vecComponentKeys = componentColl_.getComponentKeys();

  // create one InfoSpace per component
  for (vector<string>::const_iterator
      it = vecComponentKeys.begin();
      it!= vecComponentKeys.end(); ++it)
  {
    const string& componentKey = *it;

    // create and access InfoSpace
    string infoSpaceURN = app_->createQualifiedInfoSpace(infoSpaceBaseName_).toString();
    xdata::InfoSpace* infoSpace = xdata::InfoSpaceFactory::getInstance()->get(infoSpaceURN);
    
    // index InfoSpace in map
    mapComponentKeyToInfoSpace_[componentKey] = infoSpace;
  }

  // add items for all components
  addBaseItems();
  addItems();
  
  run();
}


string ttc::DataListener::getItemString(const string& componentKey, const string& name)
{
  InfoSpaceLockHandler h(infoSpaceLock(componentKey));
  return getItem(componentKey, name)->toString();
}


log4cplus::Logger& ttc::DataListener::getLogger()
{
  return logger_;
}


xdaq::Application& ttc::DataListener::getApp()
{
  if (app_)
  {
    return *app_;
  }
  else
  {
    XCEPT_RAISE(xcept::Exception, "Application pointer is NULL");
  }
}


string ttc::DataListener::getInfoSpaceBaseName()
{
  return infoSpaceBaseName_;
}


string ttc::DataListener::addComponent(
    const string& group,
    const string& system,
    const string& localID)
{
  return componentColl_.add(Component(group, system, localID));
}


ttc::InfoSpaceLock ttc::DataListener::infoSpaceLock(const string& componentKey)
{
  xdata::InfoSpace* infoSpace = getInfoSpace(componentKey);
  return InfoSpaceLock(infoSpace);
}


bool ttc::DataListener::setItem(
    const string& componentKey, 
    const string& name, 
    const xdata::Serializable& value)
{
  // lock component's InfoSpace during this operation
  InfoSpaceLockHandler h(infoSpaceLock(componentKey));

  xdata::Serializable* item = getItem(componentKey, name);

  string oldXdataValueStr = item->toString();
  item->setValue(value);
  string newXdataValueStr = item->toString();

  return newXdataValueStr != oldXdataValueStr;
}


bool ttc::DataListener::setItemFromString(
    const string& componentKey,
    const string& name,
    const string& value)
{
  // lock component's InfoSpace during this operation
  InfoSpaceLockHandler h(infoSpaceLock(componentKey));

  xdata::Serializable* item = getItem(componentKey, name);

  string oldXdataValueStr = item->toString();
  item->fromString(value);
  string newXdataValueStr = item->toString();

  return newXdataValueStr != oldXdataValueStr;
}


void ttc::DataListener::pushComponent(const string& componentKey)
{
  // lock component's InfoSpace during this operation
  InfoSpaceLockHandler h(infoSpaceLock(componentKey));

  getInfoSpace(componentKey)->fireItemGroupChanged(itemNames_, this);
}


void ttc::DataListener::refreshComponents(bool force)
{
  const vector<string>& vecComponentKeys = componentColl_.getComponentKeys();

  size_t nbChanged = 0;

  for (vector<string>::const_iterator
      it = vecComponentKeys.begin();
      it!= vecComponentKeys.end(); ++it)
  {
    const string& componentKey = *it;
    Component component = componentColl_.getComponentByKey(componentKey);

    TRY_CATCH_ALL(
        bool componentChanged = refreshItems(component);
        refreshBaseItems(component);

        if (componentChanged || force)
          pushComponent(componentKey);

        if (componentChanged)
          nbChanged++;
    );
  }

  if (nbChanged>0 || force)
  {
    LOG4CPLUS_INFO(
        getLogger(),
        "Number of changed components: " << nbChanged
        << (force ? " (update forced)" : ""));
  }
}


void ttc::DataListener::actionPerformed(xdata::Event& e)
{
  /*
   * After the xdaq::Application CTOR call,
   * when the default values have been set in the user application
   * (from the properties of the application descriptor),
   * we are ready to initialize the monitoring.
   */
  if (e.type() == "urn:xdaq-event:setDefaultValues")
  {
    init();
  }
}


xdata::InfoSpace* ttc::DataListener::getInfoSpace(const string& componentKey)
{
  map<string, xdata::InfoSpace*>::const_iterator it = mapComponentKeyToInfoSpace_.find(componentKey);
  
  if (it == mapComponentKeyToInfoSpace_.end())
  {
    XCEPT_RAISE(xcept::Exception, "No InfoSpace corresponding to component"+componentKey);
  }
  
  return it->second;
}


void ttc::DataListener::addBaseItems()
{
  addItem<xdata::TimeVal>("Timestamp");
  addItem<xdata::String>("ComponentKey");
  addItem<xdata::String>("URI");
}


void ttc::DataListener::refreshBaseItems(const Component& component)
{
  string componentKey = component.getKey();
  
  // default values indicating non-availability
  xdata::TimeVal itemTimestamp;
  xdata::String itemComponentKey;
  xdata::String itemURI;

  // get values from system
  itemTimestamp = toolbox::TimeVal::gettimeofday();
  itemComponentKey = componentKey;
  string url = getApp().getApplicationContext()->getContextDescriptor()->getURL();
  string urn = getApp().getApplicationDescriptor()->getURN();
  itemURI = url+"/"+urn;

  // set values
  setItem(componentKey, "Timestamp",    itemTimestamp);
  setItem(componentKey, "ComponentKey", itemComponentKey);
  setItem(componentKey, "URI",          itemURI);
}


xdata::Serializable* ttc::DataListener::getItem(const string& componentKey, const string& name)
{
  return getInfoSpace(componentKey)->find(name);
}
