#include "oms/DowntimeClient.h"
#include "oms/Exceptions.h"

#include "xdata/String.h"
#include "xdata/TableIterator.h"
#include "xdata/TimeVal.h"


oms::DowntimeClient::DowntimeClient
(
  xdaq::Application* app,
  const std::string& credentialsFile,
  const std::string& tstoreURL,
  const size_t tstoreLid,
  const uint32_t tStoreRetries,
  const uint32_t tStoreRetrySleepSeconds
) :
  TStoreClient(app,credentialsFile,DOWNTIMESVIEW,tstoreURL,tstoreLid,tStoreRetries,tStoreRetrySleepSeconds)
{}

oms::DowntimeEntryPtr oms::DowntimeClient::fetchOpenDowntime()
{

  std::lock_guard<std::mutex> lock(openDowntimesMutex_);
  openDowntimes_ = getOpenDowntimes();

  if ( openDowntimes_.getRowCount() == 0 )
  {
    // no open downtime found
    return nullptr;
  }
  else if ( openDowntimes_.getRowCount() > 1 )
  {
    // more than one open downtime found. This should not happen.
    std::ostringstream msg;
    msg << "Found " << openDowntimes_.getRowCount() << " open downtimes";
    XCEPT_RAISE(exception::Database,msg.str());
  }

  auto downtimeEntry = std::make_unique<DowntimeEntry>();

  for (auto const& col : openDowntimes_.getColumns() )
  {
    if      ( col == "START_TIME" )       downtimeEntry->startTime = dynamic_cast<xdata::TimeVal*>(openDowntimes_.getValueAt(0,col))->value_;
    else if ( col == "START_FILL" )       downtimeEntry->startFill = std::stoul(dynamic_cast<xdata::String*>(openDowntimes_.getValueAt(0,col))->value_);
    else if ( col == "START_RUN_NUMBER" ) downtimeEntry->startRun = std::stoul(dynamic_cast<xdata::String*>(openDowntimes_.getValueAt(0,col))->value_);
    else if ( col == "START_LS" )         downtimeEntry->startLS = std::stoul(dynamic_cast<xdata::String*>(openDowntimes_.getValueAt(0,col))->value_);
    else if ( col == "START_NIBBLE" )     downtimeEntry->startNibble = std::stoul(dynamic_cast<xdata::String*>(openDowntimes_.getValueAt(0,col))->value_);
  }

  return downtimeEntry;
}


void oms::DowntimeClient::insertNewDowntime(const DowntimeEntryPtr& downtimeEntry)
{
  try
  {
    xdata::TimeVal startTime(downtimeEntry->startTime);
    xdata::String startFill( std::to_string(downtimeEntry->startFill) );
    xdata::String startRun( std::to_string(downtimeEntry->startRun) );
    xdata::String startLS( std::to_string(downtimeEntry->startLS) );
    xdata::String startNibble( std::to_string(downtimeEntry->startNibble) );

    //xdata::Table newDowntime = getTableDefinitionFromDB(DOWNTIMESDEFINITION);
    xdata::Table newDowntime = getTableDefinition();
    newDowntime.setValueAt(0,"START_TIME",startTime);
    newDowntime.setValueAt(0,"START_FILL",startFill);
    newDowntime.setValueAt(0,"START_RUN_NUMBER",startRun);
    newDowntime.setValueAt(0,"START_LS",startLS);
    newDowntime.setValueAt(0,"START_NIBBLE",startNibble);

    insertTable("insertNewDowntime",newDowntime);
  }
  catch (exception::TStoreClientHTTPTimeout &e){
	  LOG4CPLUS_WARN(app_->getApplicationLogger(),
	  	                          "Got an HTTP SOAP timeout while inserting a downtime of START_TIME="
			  << downtimeEntry->startTime << " START_FILL=" << downtimeEntry->startFill << " START_RUN_NUMBER=" << downtimeEntry->startRun
			  << " START_LS=" << downtimeEntry->startLS << " START_NIBBLE=" << downtimeEntry->startNibble);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to insert new downtime",e);
  }
  catch (const std::exception& e) {
  			std::ostringstream msg;
  			msg << "Catch std::exception while sending postSOAP";
  			LOG4CPLUS_WARN(app_->getApplicationLogger(),
  					msg.str() << ": " << "Exception caught: " << e.what());
  			XCEPT_RAISE(exception::Database,"Could not post SOAP message and got a std::exception.");
  }
}


void oms::DowntimeClient::updateDowntime(const DowntimeEntryPtr& downtimeEntry)
{
  try
  {
    xdata::TimeVal startTime(downtimeEntry->startTime);
    xdata::String startFill( std::to_string(downtimeEntry->startFill) );
    xdata::String startRun( std::to_string(downtimeEntry->startRun) );
    xdata::String startLS( std::to_string(downtimeEntry->startLS) );
    xdata::String startNibble( std::to_string(downtimeEntry->startNibble) );
    xdata::TimeVal stopTime(downtimeEntry->stopTime);
    xdata::String stopFill( std::to_string(downtimeEntry->stopFill) );
    xdata::String stopRun( std::to_string(downtimeEntry->stopRun) );
    xdata::String stopLS( std::to_string(downtimeEntry->stopLS) );
    xdata::String stopNibble( std::to_string(downtimeEntry->stopNibble) );

    xdata::Table updateDowntime = getTableDefinition();
    updateDowntime.setValueAt(0,"START_TIME",startTime);
    updateDowntime.setValueAt(0,"START_FILL",startFill);
    updateDowntime.setValueAt(0,"START_RUN_NUMBER",startRun);
    updateDowntime.setValueAt(0,"START_LS",startLS);
    updateDowntime.setValueAt(0,"START_NIBBLE",startNibble);
    updateDowntime.setValueAt(0,"STOP_TIME",stopTime);
    updateDowntime.setValueAt(0,"STOP_FILL",stopFill);
    updateDowntime.setValueAt(0,"STOP_RUN_NUMBER",stopRun);
    updateDowntime.setValueAt(0,"STOP_LS",stopLS);
    updateDowntime.setValueAt(0,"STOP_NIBBLE",stopNibble);

      try {
    	  updateTable("updateDowntime",updateDowntime);
      }  catch (exception::TStoreClientHTTPTimeout &e){
      	  LOG4CPLUS_WARN(app_->getApplicationLogger(),
      	  	                          "Got an HTTP SOAP timeout while updating the DOWNTIMES_EVENTING table start_time='"<< startTime.toString() << " stop_time=" <<  stopTime.toString() <<"'.");
      }

  }

  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to update downtime",e);
  }
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
