#include "oms/TStoreClient.h"
#include "oms/TStoreRequest.h"
#include "oms/Exceptions.h"

#include "tstore/client/AttachmentUtils.h"
#include "tstore/client/Client.h"
#include "xcept/tools.h"
#include "xdaq/ApplicationDescriptorFactory.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xoap/SOAPBody.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Action.h"
#include "xcept/tools.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "oms/DowntimeClient.h"
#include "oms/LumisectionClient.h"
#include <unistd.h>
#include "xdata/TimeVal.h"


oms::TStoreClient::TStoreClient
(
  xdaq::Application* app,
  const std::string& credentialsFile,
  const std::string& tstoreView,
  const std::string& tstoreURL,
  const size_t tstoreLid,
  const uint32_t tStoreRetries,
  const uint32_t tStoreRetrySleepSeconds
) :
  app_(app),
  credentialsFile_(credentialsFile),
  tstoreView_(tstoreView),
  viewClass_(tstoreclient::classNameForView(tstoreView)),
  connectionTimeout_(60,0), // DB connection times out after 1 minute unless renewed
  connectionID_(""),
  tstoreDescriptor_(0),
  tStoreRetries_(tStoreRetries),
  tStoreRetrySleepSeconds_(tStoreRetrySleepSeconds)
{
  if ( ! tstoreURL.empty() )
  {
    //Creating a tstore descriptor
    auto tstoreContext = new xdaq::ContextDescriptor(tstoreURL);

    std::set<std::string> zones;
    zones.insert(app_->getApplicationContext()->getDefaultZoneName());

    std::set<std::string> groups;
    xdaq::ApplicationDescriptorFactory* applicationDescriptorFactory = xdaq::ApplicationDescriptorFactory::getInstance();

    try
    {
      tstoreDescriptor_ = applicationDescriptorFactory->createApplicationDescriptor(tstoreContext,"tstore",tstoreLid,zones,groups);
    }
    catch (xdaq::exception::DuplicateApplicationDescriptor& e)
    {
      XCEPT_RETHROW(exception::Database,"Application descriptor for 'tstore' already exists",e);
    }
    catch (xdaq::exception::InvalidZone& e)
    {
      XCEPT_RETHROW(exception::Database,"Invalid zone for application descriptor for 'tstore.",e);
    }

  }

  	// connect to Database at the constructor
  	connect();

  	// submit task of the timer every 120 seconds
	try {
		std::string timerName= "TimerRenew-" + tstoreView;
		toolbox::task::Timer *timer =
				toolbox::task::getTimerFactory()->createTimer(tstoreView);
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		toolbox::TimeInterval renewInterval(25, 0);
		timer->scheduleAtFixedRate(start, this, renewInterval, 0, "renew");
		LOG4CPLUS_INFO (app_->getApplicationLogger(),"Started the Timer="+ tstoreView );

	} catch (toolbox::task::exception::InvalidListener &e) {
		XCEPT_RETHROW(exception::Database,
				"Failed to enable renew timer", e);
	} catch (toolbox::task::exception::InvalidSubmission &e) {
		XCEPT_RETHROW(exception::Database,
				"Failed to enable renew timer", e);

	} catch (toolbox::task::exception::NotActive &e) {
		XCEPT_RETHROW(exception::Database,
				"Failed to enable renew timer", e);

	} catch (toolbox::exception::Exception &e) {
		XCEPT_RETHROW(exception::Database,
				"Failed to enable renew timer", e);

	}

  	// Get table definition for this TStoreClient
  	std::string queryName ="";
  	if (tstoreView_ == oms::LUMISECTIONSVIEW ) {
  		queryName=oms::LUMISECTIONSDEFINITION;
  	} else if (tstoreView_ == oms::DOWNTIMESVIEW ) {
  		queryName=oms::DOWNTIMESDEFINITION;
  	}

  	if (!queryName.empty()) {
  		tableDefinition_ = getTableDefinitionFromDB(queryName);
  	}

	run_ = toolbox::task::bind(this, &oms::TStoreClient::run, "run");
	  try
	  	{
		  toolbox::net::URN view(tstoreView);
	  		std::stringstream wlss;
	  		wlss << "oms-TStoreClient-" << app_->getApplicationDescriptor()->getInstance() << '-' << view.getNSS();
	  		toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "waiting")->activate();
	  		toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "waiting")->submit(run_);
	  	}
	  	catch ( toolbox::task::exception::Exception& e )
	  	{
	  		std::stringstream ss;
	  		ss << "Failed to submit workloop ";

	  		std::cerr << ss.str() << std::endl;
	  	}
	  	catch ( std::exception& se )
	  	{
	  		std::stringstream ss;
	  		ss << "Failed to submit notification to worker thread, caught standard exception '";
	  		ss << se.what() << "'";
	  		std::cerr << ss.str() << std::endl;
	  	}
}


oms::TStoreClient::~TStoreClient()
{
  disconnect();
}


void oms::TStoreClient::query(const std::string& queryName, xdata::Table& results)
{

  TStoreRequest request("query",viewClass_);
  request.addTStoreParameter("connectionID",getConnectionID());
  request.addViewSpecificParameter("name",queryName);

  xoap::MessageReference message=request.toSOAP();
  xoap::MessageReference response=sendSOAPMessage(message);

  //use the TStore client library to extract the first attachment of type "table"
  //from the SOAP response
  if ( ! tstoreclient::getFirstAttachmentOfType(response,results) )
  {
    XCEPT_RAISE(exception::Database, "Server returned no data for query "+queryName);
  }
}


xdata::Table oms::TStoreClient::getTableDefinitionFromDB(const std::string& queryName)
{

  xdata::Table definition;

  TStoreRequest request("definition",viewClass_);
  request.addTStoreParameter("connectionID",getConnectionID());
  request.addViewSpecificParameter("name",queryName);

  xoap::MessageReference message=request.toSOAP();
  xoap::MessageReference response=sendSOAPMessage(message);

  if ( ! tstoreclient::getFirstAttachmentOfType(response,definition) )
  {
    XCEPT_RAISE(exception::Database, "Server returned no definition for "+queryName);
  }

  // definition.writeTo(std::cout);

  return definition;
}


void oms::TStoreClient::insertTable(const std::string& queryName, xdata::Table& dataToInsert)
{

	oms::TStoreClient::Request r;
	r.operation= INSERTOPERATION;
	r.query=queryName;
	r.data= new xdata::Table(dataToInsert);
	messages_.push(r);
}


void oms::TStoreClient::updateTable(const std::string& queryName, xdata::Table& dataToUpdate)
{

	oms::TStoreClient::Request r;
	r.operation= UPDATEOPERATION;
	r.query=queryName;
	r.data= new xdata::Table(dataToUpdate);
	messages_.push(r);

}


void oms::TStoreClient::connectOrRenew()
{
	if (getConnectionID().empty()) {
		try {
			connect();
		} catch (xcept::Exception &e) {
			LOG4CPLUS_WARN(app_->getApplicationLogger(),
					"oms::TStoreClient::connectOrRenew(): Failed to connect to tstore for view=" << tstoreView_ <<" due to the following exception:\n" << xcept::stdformat_exception_history(e));
		}
	} else {
		try {
			renew();
		} catch (xcept::Exception&) {
			clearConnectionID();
			try {
				connect();
			} catch (xcept::Exception &e) {
				LOG4CPLUS_WARN(app_->getApplicationLogger(),
						"oms::TStoreClient::connectOrRenew(): Failed to connect to tstore for view=" << tstoreView_ <<" due to the following exception:\n" << xcept::stdformat_exception_history(e));
			}
		}
	}
}


std::string oms::TStoreClient::getCredentials() const
{
  //retreive login credentials in format username/password for the given zone

  std::fstream file(credentialsFile_,std::fstream::in);

  if ( ! file.is_open() )
  {
    XCEPT_RAISE(exception::Database,"Failed to open '"+credentialsFile_+"'");
  }

  std::string credentials;
  const auto zoneName = app_->getApplicationContext()->getDefaultZoneName();

  for (std::string line; std::getline(file,line);)
  {
    const auto pos = line.find_first_of(':');
    const std::string zone = line.substr(0, pos);
    const std::string cred = line.substr(pos+1);
    if ( pos != std::string::npos && zone == zoneName )
    {
      credentials = cred;
    }
  }

  if ( credentials.empty() )
  {
    XCEPT_RAISE(exception::Database,"Failed to find credentials for zone '"+zoneName+"'");
  }

  return credentials;
}


void oms::TStoreClient::connect()
{

  if ( ! getConnectionID().empty() ) disconnect();

  std::lock_guard<std::mutex> lock(connectionIDMutex_);
  TStoreRequest request("connect");

  request.addTStoreParameter("id",tstoreView_);

  request.addTStoreParameter("authentication","basic");
  request.addTStoreParameter("credentials",getCredentials());
  request.addTStoreParameter("timeout",connectionTimeout_.toString("xs:duration"));

  xoap::MessageReference message=request.toSOAP();

  xoap::MessageReference response=sendSOAPMessage(message);

  //use the TStore client library to extract the response from the reply
  connectionID_ = tstoreclient::connectionID(response);
  LOG4CPLUS_INFO (app_->getApplicationLogger(),"TStore connected for view=" << tstoreView_ << " connectionId=" << connectionID_);
}


void oms::TStoreClient::disconnect()
{
  if ( getConnectionID().empty() ) return;

  std::lock_guard<std::mutex> lock(connectionIDMutex_);
  TStoreRequest request("disconnect");

  request.addTStoreParameter("connectionID",getConnectionID());

  xoap::MessageReference message=request.toSOAP();

  sendSOAPMessage(message);
  LOG4CPLUS_INFO (app_->getApplicationLogger(),"TStore disconnected from view=" << tstoreView_ << " connectionId=" << connectionID_);
  connectionID_.clear();
}


void oms::TStoreClient::renew()
{
  if ( getConnectionID().empty() ) return;

  TStoreRequest request("renew");
  request.addTStoreParameter("connectionID",getConnectionID());
  request.addTStoreParameter("timeout",connectionTimeout_.toString("xs:duration"));

  xoap::MessageReference message=request.toSOAP();

  sendSOAPMessage(message);
  LOG4CPLUS_DEBUG (app_->getApplicationLogger(),"TStore renew connection from view=" << tstoreView_ << " connectionId=" << getConnectionID());
}


xoap::MessageReference oms::TStoreClient::sendSOAPMessage(xoap::MessageReference& message)
{
  xoap::MessageReference reply;


  if ( ! tstoreDescriptor_ )
  {
    XCEPT_RAISE(exception::Database,"TStore descriptor is not initialized. Is 'tstoreURL' set correctly?");
  }

  try
  {
    const auto zone = app_->getApplicationContext()->getDefaultZone();
    if ( zone )
    {
      const auto tstoreClientDescriptor = app_->getApplicationDescriptor();
      reply = app_->getApplicationContext()->postSOAP(message,*tstoreClientDescriptor,*tstoreDescriptor_);
    }
    else
    {
      XCEPT_RAISE(exception::Database,"Could not find default zone");
    }
  }
  catch (xdaq::exception::Exception& e)
  {
	xcept::Exception::const_iterator it = e.begin();
	std::size_t found = it->getProperty("message").find("Timeout during");
	if (found!=std::string::npos) {
		XCEPT_RETHROW(exception::TStoreClientHTTPTimeout,"Reached HTTP SOAP request timeout.", e);
	} else {
		XCEPT_RETHROW(exception::Database,"Could not post SOAP message", e);
	}
  }
  catch (const std::exception& e) {
  			std::ostringstream msg;
  			msg << "Catch std::exception while sending postSOAP";
  			LOG4CPLUS_WARN(app_->getApplicationLogger(),
  					msg.str() << ": " << "Exception caught: " << e.what());
  			XCEPT_RAISE(exception::Database,"Could not post SOAP message and got a std::exception.");
  }
  xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();
  if ( body.hasFault() )
  {
    //connectionID_.clear();
    XCEPT_RAISE(exception::Database, body.getFault().getFaultString());
  }
  return reply;
}

bool oms::TStoreClient::run ( toolbox::task::WorkLoop* wl )
{
	LOG4CPLUS_INFO (app_->getApplicationLogger(),"Started the workloop=" <<wl->getName() );
	for (;;)
	{
		oms::TStoreClient::Request r = messages_.pop();
		bool sentMessage = false;
		uint32_t retryCounter = 0;
		std::ostringstream msg;

		if (tstoreView_ == oms::DOWNTIMESVIEW && r.operation == UPDATEOPERATION) {
			std::lock_guard<std::mutex> lock(openDowntimesMutex_);
			if (openDowntimes_.getRowCount()==1 && r.data->getRowCount()==1) {
				xdata::String* id= nullptr;
				xdata::TimeVal* startTimeOpenDowntime = nullptr;
				for (auto const& col : openDowntimes_.getColumns() )
				  {
					if ( col == "ID" )       id = dynamic_cast<xdata::String*>(openDowntimes_.getValueAt(0,col));
					else if ( col == "START_TIME" ) startTimeOpenDowntime= dynamic_cast<xdata::TimeVal*>(openDowntimes_.getValueAt(0,col));
				  }
				const auto startTime = dynamic_cast<xdata::TimeVal*>(r.data->getValueAt(0,"START_TIME"));
				if (id != nullptr && startTimeOpenDowntime != nullptr && startTime != nullptr) {
					if (static_cast<double>(startTime->value_)==static_cast<double>(startTimeOpenDowntime->value_)) {
						xdata::String idStr( id->value_);
						r.data->setValueAt(0,"ID" , idStr);
					} else {
						LOG4CPLUS_ERROR (app_->getApplicationLogger(), "Open downtime does not match the update downtime");
						break;
					}
				} else {
					LOG4CPLUS_ERROR (app_->getApplicationLogger(), "Could not update because nullptr of id or startTimeOpenDowntime or startTime");
					break;
				}
			} else {
				LOG4CPLUS_ERROR (app_->getApplicationLogger(), "Could not update because there are multiple downtime open");
				break;
			}
		}


		xdata::Table* dataTable = new xdata::Table(*r.data);
		while (!sentMessage && retryCounter <= tStoreRetries_)
		{
			try
			{
				TStoreRequest request(r.operation, viewClass_);
				request.addTStoreParameter("connectionID", getConnectionID());
				request.addViewSpecificParameter("name", r.query);
				xoap::MessageReference message = request.toSOAP();
				//add the table with the values to be inserted as an attachment to the message, with the content-Id "update"
				tstoreclient::addAttachment(message, *dataTable, r.operation);
				sendSOAPMessage(message);
				sentMessage = true;
				delete dataTable;
				if (tstoreView_ == oms::DOWNTIMESVIEW && r.operation == INSERTOPERATION) {
					std::lock_guard<std::mutex> lock(openDowntimesMutex_);
					openDowntimes_ = getOpenDowntimes();
				}
			}
			catch (xcept::Exception & e)
			{
				msg <<  "Could not perform the " << r.operation << " using this query\n" << r.query << "\n with the following data" << xcept::stdformat_exception_history(e);
				delete dataTable;
			}
			if (!sentMessage) {
				LOG4CPLUS_WARN (app_->getApplicationLogger(), "Query to database failed number of times=" << retryCounter << "\n" <<  msg.str());
				dataTable = new xdata::Table(*r.data);
				retryCounter++;
				sleep(tStoreRetrySleepSeconds_);

			}
		}
		delete r.data;
	}
	return true;
}

void oms::TStoreClient::timeExpired(toolbox::task::TimerEvent& e)
{
	if (e.getTimerTask()->name == "renew")
	{
		connectOrRenew();
	}
}


std::string oms::TStoreClient::getConnectionID()
{
    std::lock_guard<std::mutex> lock(connectionIDMutex_);
    return connectionID_;  // Read the shared string
}

void oms::TStoreClient::clearConnectionID()
{
    std::lock_guard<std::mutex> lock(connectionIDMutex_);
    connectionID_.clear();  // Clear the string
}

xdata::Table oms::TStoreClient::getTableDefinition()
{
  return tableDefinition_;
}


xdata::Table oms::TStoreClient::getOpenDowntimes()
{
  xdata::Table openDowntimes;

  try
  {
    query("getOpenDowntimes",openDowntimes);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to fetch open downtimes",e);
  }

  openDowntimes.writeTo(std::cout);

  return openDowntimes;
}

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
