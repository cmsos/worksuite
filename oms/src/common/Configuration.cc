#include "oms/Configuration.h"


oms::Configuration::Configuration(xdata::InfoSpace* infoSpace) :
  nbNibblesStart_(4),
  nbNibblesEnd_(4),
  tcdsRecordsHistorySize_(50),
  brilRecordsHistorySize_(20),
  lumisectionHistorySize_(20),
  downtimeHistorySize_(10),
  maxTries_(10),
  deadTimeFracStart_(100),
  deadTimeFracEnd_(95),
  useDeadtimeBeamActive_(true),
  eventingBus_("brildata"),
  tStoreRetries_(6),
  tStoreRetrySleepSeconds_(20),
  tcdsTopic_("tcds_brildaq_data"),
  credentialsFile_(""),
  tstoreURLLumisection_(""),
  tstoreLidLumisection_(0),
  tstoreURLDowntime_(""),
  tstoreLidDowntime_(0)
{
  infoSpace->fireItemAvailable("nbNibblesStart",&nbNibblesStart_);
  infoSpace->fireItemAvailable("nbNibblesEnd",&nbNibblesEnd_);
  infoSpace->fireItemAvailable("tcdsRecordsHistorySize",&tcdsRecordsHistorySize_);
  infoSpace->fireItemAvailable("brilRecordsHistorySize",&brilRecordsHistorySize_);
  infoSpace->fireItemAvailable("lumisectionHistorySize",&lumisectionHistorySize_);
  infoSpace->fireItemAvailable("downtimeHistorySize",&downtimeHistorySize_);
  infoSpace->fireItemAvailable("maxTries",&maxTries_);
  infoSpace->fireItemAvailable("deadTimeFracStart",&deadTimeFracStart_);
  infoSpace->fireItemAvailable("deadTimeFracEnd",&deadTimeFracEnd_);
  infoSpace->fireItemAvailable("useDeadtimeBeamActive",&useDeadtimeBeamActive_);
  infoSpace->fireItemAvailable("eventingBus",&eventingBus_);
  infoSpace->fireItemAvailable("tcdsTopic",&tcdsTopic_);
  infoSpace->fireItemAvailable("credentialsFile",&credentialsFile_);
  infoSpace->fireItemAvailable("tstoreURLLumisection",&tstoreURLLumisection_);
  infoSpace->fireItemAvailable("tstoreLidLumisection",&tstoreLidLumisection_);
  infoSpace->fireItemAvailable("tstoreURLDowntime",&tstoreURLDowntime_);
  infoSpace->fireItemAvailable("tstoreLidDowntime",&tstoreLidDowntime_);
  infoSpace->fireItemAvailable("tStoreRetries",&tStoreRetries_);
  infoSpace->fireItemAvailable("tStoreRetrySleepSeconds",&tStoreRetrySleepSeconds_);
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
