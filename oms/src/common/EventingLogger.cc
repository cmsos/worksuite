#include <cmath>
#include <ctime>
#include <string>

#include "interface/bril/shared/CommonDataFormat.h"
#include "interface/bril/shared/CompoundDataStreamer.h"
#include "interface/bril/shared/LUMITopics.hh"

#include "b2in/nub/Method.h"
#include "xcept/tools.h"
#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/Serializable.h"
#include "xdata/Table.h"
#include "xdata/TimeVal.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

#include "oms/EventingLogger.h"
#include "oms/Exceptions.h"


oms::EventingLogger::EventingLogger(xdaq::ApplicationStub* app) :
  xdaq::Application(app),
  xgi::framework::UIManager(this),
  eventing::api::Member(this),
  configuration_(app->getInfoSpace()),
  lumisectionHandler_(this),
  downtimeHandler_(this),
  doProcessing_(true)
{
  app->getDescriptor()->setAttribute("icon","/oms/images/oms-eventinglogger.png");
  app->getDescriptor()->setAttribute("icon16x16","/oms/images/oms-eventinglogger.png");

  getApplicationContext()->addActionListener(this);

  b2in::nub::bind(this, &oms::EventingLogger::onMessage );

  xgi::framework::deferredbind(this,this,&oms::EventingLogger::defaultWebPage,"Default");
  xgi::bind(this,&oms::EventingLogger::getRunAndLumiSection,"getRunAndLumiSection");

  LOG4CPLUS_DEBUG(getApplicationLogger(), "End of constructor");
}


oms::EventingLogger::~EventingLogger()
{
  doProcessing_ = false;
  tcdsRecordsHandler_.join();
  bestLumiRecordsHandler_.join();
}


void oms::EventingLogger::actionPerformed(toolbox::Event& event)
{
  if (event.type() == "urn:xdaq-event:profile-loaded")
  {
    LOG4CPLUS_DEBUG(getApplicationLogger(),"profile-loaded");

    configure();
    subscribeToTCDSeventing();
    subscribeToBRILeventing();
  }
}


void oms::EventingLogger::configure()
{
  try
  {
    tcdsRecordsHistory_.set_capacity(configuration_.tcdsRecordsHistorySize());
    bestLumiRecordsHistory_.set_capacity(configuration_.brilRecordsHistorySize());

    lumisectionHandler_.configure(configuration_.lumisectionHistorySize(),
                                  configuration_.credentialsFile(),
                                  configuration_.tstoreURLLumisection(), configuration_.tstoreLidLumisection(),
								  configuration_.tStoreRetries(), configuration_.tStoreRetrySleepSeconds() );
    downtimeHandler_.configure(configuration_.downtimeHistorySize(),
                               configuration_.nbNibblesStart(), configuration_.nbNibblesEnd(),
                               configuration_.deadTimeFracStart(), configuration_.deadTimeFracEnd(),
                               configuration_.credentialsFile(),
                               configuration_.tstoreURLDowntime(),configuration_.tstoreLidDowntime(),
							   configuration_.tStoreRetries(), configuration_.tStoreRetrySleepSeconds() );

    tcdsRecordsHandler_ = std::thread(&oms::EventingLogger::handleTCDSrecords,this);
    bestLumiRecordsHandler_ = std::thread(&oms::EventingLogger::handleBestLumiRecords,this);

  }
  catch(xcept::Exception& e)
  {
    const std::string msg = "Failed to apply configuration";
    LOG4CPLUS_FATAL(getApplicationLogger(), msg << ": " << xcept::stdformat_exception_history(e));
    XCEPT_RETHROW(exception::Configuration, msg, e);
  }
}


void oms::EventingLogger::subscribeToTCDSeventing()
{
  try
  {
    getEventingBus(configuration_.eventingBus()).subscribe(configuration_.tcdsTopic());

    std::ostringstream msg;
    msg << "Subscribed to topic '" << configuration_.tcdsTopic()
      << "' on eventing bus '" << configuration_.eventingBus() << "'";
    LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
  }
  catch (eventing::api::exception::Exception& e)
  {
    std::ostringstream msg;
    msg << "Failed to subscribe to topic '" << configuration_.tcdsTopic()
      << "' on eventing bus '" << configuration_.eventingBus() << "'";
    LOG4CPLUS_ERROR(getApplicationLogger(), msg.str() << ": " << xcept::stdformat_exception_history(e));
    XCEPT_RETHROW(exception::Eventing, msg.str(), e);
  }
}


void oms::EventingLogger::subscribeToBRILeventing()
{
  try
  {
    getEventingBus(configuration_.eventingBus()).subscribe(interface::bril::shared::bestlumiT::topicname());

    std::ostringstream msg;
    msg << "Subscribed to topic '" << interface::bril::shared::bestlumiT::topicname()
      << "' on eventing bus '" << configuration_.eventingBus() << "'";
    LOG4CPLUS_INFO(getApplicationLogger(), msg.str());
  }
  catch (eventing::api::exception::Exception& e)
  {
    std::ostringstream msg;
    msg << "Failed to subscribe to topic '" << interface::bril::shared::bestlumiT::topicname()
      << "' on eventing bus '" << configuration_.eventingBus() << "'";
    LOG4CPLUS_ERROR(getApplicationLogger(), msg.str() << ": " << xcept::stdformat_exception_history(e));
    XCEPT_RETHROW(exception::Eventing, msg.str(), e);
  }
}


void oms::EventingLogger::onMessage(toolbox::mem::Reference* reference, xdata::Properties& plist)
{
  toolbox::mem::AutoReference ref(reference);
  const std::string action = plist.getProperty("urn:b2in-eventing:action");

  if (action == "notify")
  {
    const std::string topic = plist.getProperty("urn:b2in-eventing:topic");

    if ( topic == configuration_.tcdsTopic() )
    {
      try
      {
        auto const tcdsRecord = parseTCDSrecord(ref);
        if ( tcdsRecord )
        {
          std::lock_guard<std::mutex> guard(tcdsRecordsQueueMutex_);
          tcdsRecordsQueue_.push(std::move(tcdsRecord));
        }
      }
      catch (xdata::exception::Exception& e)
      {
        LOG4CPLUS_ERROR(getApplicationLogger(),
                        "Failed to deserialize incoming TCDS table: " << xcept::stdformat_exception_history(e));
      }
    }
    else if ( topic == interface::bril::shared::bestlumiT::topicname() )
    {
      try
      {
        auto const bestLumiRecord = parseBestLumiRecord(ref);

        if ( bestLumiRecord )
        {
          std::lock_guard<std::mutex> guard(bestLumiRecordsQueueMutex_);
          bestLumiRecordsQueue_.push(std::move(bestLumiRecord));
        }
      }
      catch (xdata::exception::Exception& e)
      {
        LOG4CPLUS_ERROR(getApplicationLogger(),
                        "Failed to deserialize incoming bestLumi table: " << xcept::stdformat_exception_history(e));
      }
    }
  }
}


void oms::EventingLogger::handleTCDSrecords()
{
  //uint32_t tries = 0;

  while ( doProcessing_ )
  {
    while ( ! tcdsRecordsQueue_.empty() )
    {
      auto const tcdsRecord = tcdsRecordsQueue_.front();

      if ( tcdsRecordsHistory_.empty() || (*tcdsRecord) != (*tcdsRecordsHistory_.back()) )
      {
        std::lock_guard<std::mutex> guard(tcdsRecordsHistoryMutex_);
        tcdsRecordsHistory_.push_back(tcdsRecord);
      }

      try
      {
        lumisectionHandler_.processTCDSrecord(tcdsRecord);
        downtimeHandler_.processTCDSrecord(tcdsRecord);

        {
          std::lock_guard<std::mutex> guard(tcdsRecordsQueueMutex_);
          tcdsRecordsQueue_.pop();
        }

        /*
        if ( tries > 1 )
        {
          LOG4CPLUS_WARN(getApplicationLogger(),
                         "Required "+std::to_string(tries)+" tries to process TCDS record '" << *tcdsRecord << "'");
          tries = 0;
        }
        */
      }
      catch (xcept::Exception& e)
      {

    	/*
        if ( ++tries > configuration_.maxTries() )
        {
          LOG4CPLUS_ERROR(getApplicationLogger(),
                          "Failed to handle TCDS record '" << *tcdsRecord << "' after 10 tries, giving up: "
                          << xcept::stdformat_exception_history(e));
          {
            std::lock_guard<std::mutex> guard(tcdsRecordsQueueMutex_);
            tcdsRecordsQueue_.pop();
          }
          tries = 0;
        }
        else
        {
          ::usleep(100000);
        }

      */
    	  LOG4CPLUS_ERROR(getApplicationLogger(),
    	                            "Failed to handle TCDS record '" << *tcdsRecord << "' giving up: "
    	                            << xcept::stdformat_exception_history(e));
      }
    }

    ::usleep(1000);
  }
}


void oms::EventingLogger::handleBestLumiRecords()
{
  uint32_t tries = 0;

  while ( doProcessing_ )
  {
    while ( ! bestLumiRecordsQueue_.empty() )
    {
      auto const bestLumiRecord = bestLumiRecordsQueue_.front();

      if ( bestLumiRecordsHistory_.empty() || (*bestLumiRecord) != (*bestLumiRecordsHistory_.back()) )
      {
        std::lock_guard<std::mutex> guard(bestLumiRecordsHistoryMutex_);
        bestLumiRecordsHistory_.push_back(bestLumiRecord);
      }

      try
      {
        lumisectionHandler_.processBestLumiRecord(bestLumiRecord);

        {
          std::lock_guard<std::mutex> guard(bestLumiRecordsQueueMutex_);
          bestLumiRecordsQueue_.pop();
        }

        if ( tries > 1 )
        {
          LOG4CPLUS_WARN(getApplicationLogger(),
                         "Required "+std::to_string(tries)+" tries to process BRIL best-lumi record '" << *bestLumiRecord << "'");
          tries = 0;
        }
      }
      catch (xcept::Exception& e)
      {
        if ( ++tries > configuration_.maxTries() )
        {
          LOG4CPLUS_ERROR(getApplicationLogger(),
                          "Failed to handle BRIL best-lumi record '" << *bestLumiRecord << "' after 10 tries, giving up: "
                          << xcept::stdformat_exception_history(e));
          {
            std::lock_guard<std::mutex> guard(bestLumiRecordsQueueMutex_);
            bestLumiRecordsQueue_.pop();
          }
          tries = 0;
        }
        else
        {
          ::usleep(100000);
        }
      }
    }

    ::usleep(1000);
  }
}


oms::TCDSrecordPtr oms::EventingLogger::parseTCDSrecord(const toolbox::mem::AutoReference& ref)
{
  xdata::Table table;
  xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
  xdata::exdr::Serializer serializer;
  serializer.import(&table, &inBuffer);
  xdata::Serializable* serializable = nullptr;
  TCDSrecordPtr tcdsRecord = std::make_unique<TCDSrecord>();

  serializable = table.getValueAt(0,"FillNumber");
  if (serializable)
  {
    tcdsRecord->fill = dynamic_cast<xdata::UnsignedInteger32*>(serializable)->value_;
    serializable = nullptr;
  }
  else
  {
    return nullptr;
  }

  serializable = table.getValueAt(0,"RunNumber");
  if (serializable)
  {
    tcdsRecord->run = dynamic_cast<xdata::UnsignedInteger32*>(serializable)->value_;
    serializable = nullptr;
  }
  else
  {
    return nullptr;
  }

  serializable = table.getValueAt(0,"SectionNumber");
  if (serializable)
  {
    tcdsRecord->ls = dynamic_cast<xdata::UnsignedInteger32*>(serializable)->value_ ;
    serializable = nullptr;
  }
  else
  {
    return nullptr;
  }

  serializable = table.getValueAt(0,"NibbleNumber");
  if (serializable)
  {
    tcdsRecord->nibble = dynamic_cast<xdata::UnsignedInteger32*>(serializable)->value_ ;
    serializable = nullptr;
  }
  else
  {
    return nullptr;
  }

  serializable = table.getValueAt(0,"NumNibblesPerSection");
  if (serializable)
  {
    tcdsRecord->nibblesPerSection = dynamic_cast<xdata::UnsignedInteger32*>(serializable)->value_ ;
    serializable = nullptr;
  }
  else
  {
    return nullptr;
  }

  serializable = table.getValueAt(0,"BSTTimestampBegin");
  if (serializable)
  {
    tcdsRecord->nibbleStartTime = static_cast<double>(dynamic_cast<xdata::TimeVal*>(serializable)->value_);
    serializable = nullptr;
  }
  else
  {
    return nullptr;
  }

  serializable = table.getValueAt(0,"BSTTimestampEnd");
  if (serializable)
  {
    tcdsRecord->nibbleEndTime = static_cast<double>(dynamic_cast<xdata::TimeVal*>(serializable)->value_);
    serializable = nullptr;
  }
  else
  {
    return nullptr;
  }

  serializable = table.getValueAt(0,"CMSRunActive");
  if (serializable)
  {
    tcdsRecord->runActive = dynamic_cast<xdata::Boolean*>(serializable)->value_ ;
    serializable = nullptr;
  }
  else
  {
    return nullptr;
  }

  if ( configuration_.useDeadtimeBeamActive() )
  {
    serializable = table.getValueAt(0,"DeadtimeBeamActive");
    if (serializable)
    {
      tcdsRecord->deadtime = dynamic_cast<xdata::Float*>(serializable)->value_;
      serializable = nullptr;
    }
    else
    {
      return nullptr;
    }
  }
  else
  {
    serializable = table.getValueAt(0,"Deadtime");
    if (serializable)
    {
      tcdsRecord->deadtime = dynamic_cast<xdata::Float*>(serializable)->value_;
      serializable = nullptr;
    }
    else
    {
      return nullptr;
    }
  }

  return tcdsRecord;
}


oms::BestLumiRecordPtr oms::EventingLogger::parseBestLumiRecord(const toolbox::mem::AutoReference& ref)
{
  BestLumiRecordPtr bestLumiRecord = std::make_unique<BestLumiRecord>();

  interface::bril::shared::DatumHead* thead = (interface::bril::shared::DatumHead*)(ref->getDataLocation());
  interface::bril::shared::CompoundDataStreamer tc(interface::bril::shared::bestlumiT::payloaddict());

  bestLumiRecord->fill = thead->fillnum;
  bestLumiRecord->run = thead->runnum;
  bestLumiRecord->ls = thead->lsnum;
  bestLumiRecord->nibble = thead->nbnum;

  tc.extract_field(&bestLumiRecord->delivered,"delivered",thead->payloadanchor);
  if ( std::isnan(bestLumiRecord->delivered) || std::isinf(bestLumiRecord->delivered) )
  {
    bestLumiRecord->delivered = 0;
    XCEPT_RAISE(exception::Eventing,"received delivered NaN or Inf from bestlumi");
  }

  tc.extract_field(&bestLumiRecord->avgpu,"avgpu",thead->payloadanchor);
  if ( std::isnan(bestLumiRecord->avgpu) || std::isinf(bestLumiRecord->avgpu) )
  {
    bestLumiRecord->avgpu = 0;
    XCEPT_RAISE(exception::Eventing,"received avgpu NaN or Inf from bestlumi");
  }

  return bestLumiRecord;
}


void oms::EventingLogger::defaultWebPage(xgi::Input* in, xgi::Output* out)
{
  cgicc::div tabs;
  tabs.set("class","xdaq-tab-wrapper");
  tabs.add(lastTCDSrecords());
  tabs.add(lastBestLumiRecords());
  tabs.add(lumisectionHandler_.lastLumisections());
  tabs.add(downtimeHandler_.lastDowntimes());

  *out << tabs;
}


cgicc::div oms::EventingLogger::lastTCDSrecords() const
{
  using namespace cgicc;

  cgicc::div tcdsRecords;
  tcdsRecords.set("class","xdaq-tab");
  tcdsRecords.set("title","TCDS Records");

  table recordsTable;
  recordsTable.set("class","xdaq-table");
  recordsTable.set("style","width: 100%;");
  thead head;
  head.add(tr()
           .add(th("Start time (UTC)"))
           .add(th("End time (UTC)"))
           .add(th("Fill"))
           .add(th("Run"))
           .add(th("LS"))
           .add(th("Nibble"))
           .add(th("Deadtime (%)"))
           .add(th("Run Active"))
           .add(th("Nibbles/LS")));

  recordsTable.add(head);

  {
    std::lock_guard<std::mutex> guard(tcdsRecordsHistoryMutex_);

    for ( auto rit = tcdsRecordsHistory_.rbegin(); rit != tcdsRecordsHistory_.rend(); ++rit )
    {
      const std::time_t startTime = std::llround((*rit)->nibbleStartTime);
      const std::time_t stopTime = std::llround((*rit)->nibbleEndTime);
      recordsTable.add(tr()
                       .add(td(std::asctime(std::gmtime(&startTime))))
                       .add(td(std::asctime(std::gmtime(&stopTime))))
                       .add(td(std::to_string((*rit)->fill)))
                       .add(td(std::to_string((*rit)->run)))
                       .add(td(std::to_string((*rit)->ls)))
                       .add(td(std::to_string((*rit)->nibble)))
                       .add(td(std::to_string((*rit)->deadtime)))
                       .add(td((*rit)->runActive?"true":"false"))
                       .add(td(std::to_string((*rit)->nibblesPerSection))));
    }
  }

  tcdsRecords.add(recordsTable);

  return tcdsRecords;
}


cgicc::div oms::EventingLogger::lastBestLumiRecords() const
{
  using namespace cgicc;

  cgicc::div bestLumiRecords;
  bestLumiRecords.set("class","xdaq-tab");
  bestLumiRecords.set("title","BRIL Records");

  table recordsTable;
  recordsTable.set("class","xdaq-table");
  recordsTable.set("style","width: 100%;");
  thead head;
  head.add(tr()
           .add(th("Fill"))
           .add(th("Run"))
           .add(th("LS"))
           .add(th("Nibble"))
           .add(th("Inst.Lumi (Hz/ub)"))
           .add(th("Avg.Pileup")));

  recordsTable.add(head);

  {
    std::lock_guard<std::mutex> guard(bestLumiRecordsHistoryMutex_);

    for ( auto rit = bestLumiRecordsHistory_.rbegin(); rit != bestLumiRecordsHistory_.rend(); ++rit )
    {
      recordsTable.add(tr()
                       .add(td(std::to_string((*rit)->fill)))
                       .add(td(std::to_string((*rit)->run)))
                       .add(td(std::to_string((*rit)->ls)))
                       .add(td(std::to_string((*rit)->nibble)))
                       .add(td(std::to_string((*rit)->delivered)))
                       .add(td(std::to_string((*rit)->avgpu))));
    }
  }

  bestLumiRecords.add(recordsTable);

  return bestLumiRecords;
}


void oms::EventingLogger::getRunAndLumiSection(xgi::Input* in, xgi::Output* out)
{
  std::lock_guard<std::mutex> guard(tcdsRecordsHistoryMutex_);

  if ( ! tcdsRecordsHistory_.empty() )
  {
    *out << tcdsRecordsHistory_.back()->run << ",";
    *out << tcdsRecordsHistory_.back()->ls << ",";
    *out << tcdsRecordsHistory_.back()->nibble;
  }
  else
  {
    *out << "0,0,0";
  }
}


/**
 * Provides the factory method for the instantiation of EventingLogger application.
 */
XDAQ_INSTANTIATOR_IMPL(oms::EventingLogger)


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
