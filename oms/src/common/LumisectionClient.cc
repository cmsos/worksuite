#include "oms/LumisectionClient.h"
#include "oms/Exceptions.h"

#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/TimeVal.h"


oms::LumisectionClient::LumisectionClient
(
  xdaq::Application* app,
  const std::string& credentialsFile,
  const std::string& tstoreURL,
  const size_t tstoreLid,
  const uint32_t tStoreRetries,
  const uint32_t tStoreRetrySleepSeconds
) :
  TStoreClient(app,credentialsFile,LUMISECTIONSVIEW,tstoreURL,tstoreLid, tStoreRetries, tStoreRetrySleepSeconds)
{}


void oms::LumisectionClient::insertNewLumisection(const LumisectionEntryPtr& lumisectionEntry)
{
  // run number needs to map to a xdata::String, as it has more than 9 digits,
  // e.g. in test setups where run numbers are >1e9
  // c.f. https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreColumnTypes

  try
  {
    xdata::TimeVal startTime(lumisectionEntry->startTime);
    xdata::TimeVal stopTime(lumisectionEntry->stopTime);
    xdata::String fill( std::to_string(lumisectionEntry->fill) );
    xdata::String run( std::to_string(lumisectionEntry->run) );
    xdata::String ls( std::to_string(lumisectionEntry->ls) );
    xdata::Integer cmsActive( lumisectionEntry->cmsActive );
    //xdata::String instantaneousLumi( std::to_string(lumisectionEntry->instantaneousLumi()) );
    //xdata::String deliveredLumi( std::to_string(lumisectionEntry->deliveredLumi()) );
    //xdata::String recordedLumi( std::to_string(lumisectionEntry->recordedLumi()) );

    std::ostringstream avgPileupStream;
    avgPileupStream.precision(10); //10 is the precision
    avgPileupStream << std::fixed << lumisectionEntry->avgPileup();
    xdata::String avgPileupString = avgPileupStream.str();

    std::ostringstream deliveredLumiStream;
    deliveredLumiStream.precision(10);
    deliveredLumiStream << std::fixed <<lumisectionEntry->deliveredLumi();
    xdata::String deliveredLumiString = deliveredLumiStream.str();

    std::ostringstream recordedLumiStream;
    recordedLumiStream.precision(10);
    recordedLumiStream << std::fixed <<lumisectionEntry->recordedLumi();
    xdata::String recordedLumiString = recordedLumiStream.str();


    std::ostringstream instantaneousLumiStream;
    instantaneousLumiStream.precision(10);
    instantaneousLumiStream << std::fixed <<lumisectionEntry->instantaneousLumi();
    xdata::String instantaneousLumiString = instantaneousLumiStream.str();


    //xdata::Table newLumisection = getTableDefinitionFromDB(LUMISECTIONSDEFINITION);
    xdata::Table newLumisection = getTableDefinition();
    newLumisection.setValueAt(0,"START_TIME",startTime);
    newLumisection.setValueAt(0,"STOP_TIME",stopTime);
    newLumisection.setValueAt(0,"FILL_NUMBER",fill);
    newLumisection.setValueAt(0,"RUN_NUMBER",run);
    newLumisection.setValueAt(0,"LUMISECTION_NUMBER",ls);
    newLumisection.setValueAt(0,"CMS_ACTIVE",cmsActive);
    if (lumisectionEntry->brilUpdates > 0 ) {
		newLumisection.setValueAt(0,"INSTANTANEOUS_LUMI",instantaneousLumiString);
		newLumisection.setValueAt(0,"DELIVERED_LUMI",deliveredLumiString);
		newLumisection.setValueAt(0,"RECORDED_LUMI",recordedLumiString);
		newLumisection.setValueAt(0,"AVG_PILEUP",avgPileupString);
    }
    if(lumisectionEntry->run != 0)
    {
      insertTable("insertNewLumisection",newLumisection);
    }
  }
  catch (exception::TStoreClientHTTPTimeout &e){
	  XCEPT_RETHROW(exception::TStoreClientHTTPTimeout,"Failed to insert new lumisection",e);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to insert new lumisection",e);
  }
  catch (const std::exception& e) {
  			std::ostringstream msg;
  			msg << "Catch std::exception while sending postSOAP";
  			LOG4CPLUS_WARN(app_->getApplicationLogger(),
  					msg.str() << ": " << "Exception caught: " << e.what());
  			XCEPT_RAISE(exception::Database,"Could not post SOAP message and got a std::exception.");
  }
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
