#include <iostream>

#include "oms/EventingLogger.h"
#include "oms/Exceptions.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "xdata/Boolean.h"
#include "xdata/Float.h"
#include "xdata/InfoSpace.h"
#include "xdata/Properties.h"
#include "xdata/Table.h"
#include "xdata/TimeVal.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "MockApplication.h"
#include "Utils.h"


namespace omstest {

  toolbox::mem::Pool* pool = toolbox::mem::getMemoryPoolFactory()->
    createPool("toolbox-mem-pool-test", new toolbox::mem::HeapAllocator());


  xdata::Properties getFakeTCDSeventingProperty()
  {
    xdata::Properties properties;
    properties.setProperty("urn:b2in-eventing:action","notify");
    properties.setProperty("urn:b2in-eventing:topic","tcds_brildaq_data");

    return properties;
  }


  toolbox::mem::Reference* getFakeBrilDaqRecord
  (
    xdata::UnsignedInteger32 run,
    xdata::UnsignedInteger32 ls,
    xdata::UnsignedInteger32 nibble,
    xdata::Float deadtime,
    xdata::Boolean cmsRunActive = true
  )
  {
    xdata::Table brildaqTable;

    brildaqTable.addColumn("FillNumber", "unsigned int 32");
    brildaqTable.addColumn("RunNumber", "unsigned int 32");
    brildaqTable.addColumn("SectionNumber", "unsigned int 32");
    brildaqTable.addColumn("NibbleNumber", "unsigned int 32");
    brildaqTable.addColumn("BSTTimestampBegin", "time");
    brildaqTable.addColumn("BSTTimestampEnd", "time");
    brildaqTable.addColumn("CMSRunActive", "bool");
    brildaqTable.addColumn("NumNibblesPerSection", "unsigned int 32");
    brildaqTable.addColumn("DeadtimeBeamActive", "float");

    xdata::UnsignedInteger32 xfill = fill;
    xdata::UnsignedInteger32 numNibblesPerSection = 64;
    xdata::TimeVal bstTimestampBegin = xdata::TimeVal(getFakeNibbleStartTime(run,ls,nibble));
    xdata::TimeVal bstTimestampEnd = xdata::TimeVal(getFakeNibbleStopTime(run,ls,nibble));
    brildaqTable.setValueAt(0, "FillNumber", xfill);
    brildaqTable.setValueAt(0, "RunNumber", run);
    brildaqTable.setValueAt(0, "SectionNumber", ls);
    brildaqTable.setValueAt(0, "NibbleNumber", nibble);
    brildaqTable.setValueAt(0, "NumNibblesPerSection", numNibblesPerSection);
    brildaqTable.setValueAt(0, "DeadtimeBeamActive", deadtime);
    brildaqTable.setValueAt(0, "CMSRunActive", cmsRunActive);
    brildaqTable.setValueAt(0, "BSTTimestampBegin", bstTimestampBegin);
    brildaqTable.setValueAt(0, "BSTTimestampEnd", bstTimestampEnd);


    const size_t maxSize = 4096;
    toolbox::mem::Reference* bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(pool, maxSize);
    xdata::exdr::FixedSizeOutputStreamBuffer outputBuffer(static_cast<char*>(bufRef->getDataLocation()), maxSize);

    xdata::exdr::Serializer serializer;
    serializer.exportAll(&brildaqTable, &outputBuffer);
    bufRef->setDataSize(outputBuffer.tellp());

    return bufRef;
  }


  struct EventingLoggerTestClass : public oms::EventingLogger
  {
    EventingLoggerTestClass() : oms::EventingLogger( unittests::getMockXdaqApplicationStub() )
    {
      try
      {
        toolbox::Event event("urn:xdaq-event:profile-loaded");
        actionPerformed(event);
      }
      catch (oms::exception::Configuration& e)
      {
        // do nothing
      }
      catch (oms::exception::Eventing& e)
      {
        // do nothing
      }
    }
  };


  BOOST_FIXTURE_TEST_SUITE(EventingLogger,EventingLoggerTestClass)


  BOOST_AUTO_TEST_CASE(recordHistory)
  {
    xdata::Properties properties = getFakeTCDSeventingProperty();

    const uint16_t run = 52313;
    const uint16_t ls = 34;

    BOOST_CHECK( tcdsRecordsHistory_.empty() );
    BOOST_CHECK_EQUAL( tcdsRecordsHistory_.size(),0U );
    BOOST_CHECK_EQUAL( tcdsRecordsHistory_.capacity(),50U );

    for ( uint16_t n = 1; n < 50; ++n )
    {
      onMessage(
        getFakeBrilDaqRecord(run,ls,n+12,100),
        properties
      );

      while ( ! empty() ) ::usleep(1000);

      BOOST_CHECK_EQUAL( tcdsRecordsHistory_.size(),n );

      auto lastRecord = tcdsRecordsHistory_.back();
      BOOST_CHECK_EQUAL( lastRecord->fill,fill );
      BOOST_CHECK_EQUAL( lastRecord->run,run );
      BOOST_CHECK_EQUAL( lastRecord->ls,ls );
      BOOST_CHECK_EQUAL( lastRecord->nibble,n+12U );
      BOOST_CHECK_CLOSE( lastRecord->deadtime,100,0.001 );
      BOOST_CHECK_EQUAL( lastRecord->runActive,true );
      BOOST_CHECK_EQUAL( lastRecord->nibbleStartTime,getFakeNibbleStartTime(run,ls,n+12) );
    }

    onMessage(
      getFakeBrilDaqRecord(run,ls,32,10,false),
      properties
    );

    while ( ! empty() ) ::usleep(1000);

    BOOST_CHECK_EQUAL( tcdsRecordsHistory_.size(),50U );

    for ( uint16_t nibble = 33; nibble < 60; ++nibble )
    {
      onMessage(
        getFakeBrilDaqRecord(run,ls,nibble,10),
        properties
      );

      while ( ! empty() ) ::usleep(1000);

      BOOST_CHECK_EQUAL( tcdsRecordsHistory_.size(),50U );

      auto lastRecord = tcdsRecordsHistory_.back();
      BOOST_CHECK_EQUAL( lastRecord->fill,fill );
      BOOST_CHECK_EQUAL( lastRecord->run,run );
      BOOST_CHECK_EQUAL( lastRecord->ls,ls );
      BOOST_CHECK_EQUAL( lastRecord->nibble,nibble );
      BOOST_CHECK_CLOSE( lastRecord->deadtime,10,0.00001 );
      BOOST_CHECK_EQUAL( lastRecord->runActive,true );
      BOOST_CHECK_EQUAL( lastRecord->nibbleStartTime,getFakeNibbleStartTime(run,ls,nibble) );
    }
  }


  BOOST_AUTO_TEST_SUITE_END()

} //namespace omstest
