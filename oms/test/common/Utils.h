#ifndef _omstest_utils_
#define _omstest_utils_

namespace omstest
{
  const uint16_t fill = 1234;
  const uint16_t run = 56789;
  const uint16_t nibblesPerSection = 64;
  const double nibbleDuration = 4096/11245;


  inline double getFakeNibbleStartTime(const uint16_t run, const uint16_t ls, const uint16_t nibble)
  {
    return 1584000000. + 10*run + (nibble*4096 + ls*262144)/11245; // 11,245 orbits/s, nibble is 2**12 orbits, LS 2**18 orbits)
  }

  inline double getFakeNibbleStopTime(const uint16_t run, const uint16_t ls, const uint16_t nibble)
  {
    return (getFakeNibbleStartTime(run,ls,nibble) + nibbleDuration);
  }
}

#endif // _omstest_utils_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
