#ifndef _oms_eventinglogger_h
#define _oms_eventinglogger_h

#include <atomic>
#include <queue>
#include <mutex>
#include <thread>

#include <boost/circular_buffer.hpp>

#include "oms/BestLumiRecord.h"
#include "oms/Configuration.h"
#include "oms/DowntimeHandler.h"
#include "oms/LumisectionHandler.h"
#include "oms/TCDSrecord.h"

#include "cgicc/HTMLClasses.h"
#include "eventing/api/Member.h"
#include "toolbox/ActionListener.h"
#include "toolbox/mem/AutoReference.h"
#include "toolbox/mem/Reference.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationStub.h"
#include "xdata/Properties.h"
#include "xgi/framework/Method.h"
#include "xgi/framework/UIManager.h"
#include "xgi/Input.h"
#include "xgi/Output.h"


namespace oms {

  class EventingLogger :
    public xdaq::Application, public toolbox::ActionListener, public xgi::framework::UIManager, public eventing::api::Member
  {
  public:

    EventingLogger(xdaq::ApplicationStub*);

    ~EventingLogger();

    XDAQ_INSTANTIATOR();

    bool empty() const { return ( tcdsRecordsQueue_.empty() && bestLumiRecordsQueue_.empty() ); }


  protected:

    void actionPerformed(toolbox::Event&) override;
    void onMessage(toolbox::mem::Reference*, xdata::Properties&);

    boost::circular_buffer<TCDSrecordPtr> tcdsRecordsHistory_;
    mutable std::mutex tcdsRecordsHistoryMutex_;

    boost::circular_buffer<BestLumiRecordPtr> bestLumiRecordsHistory_;
    mutable std::mutex bestLumiRecordsHistoryMutex_;


  private:

    void configure();
    void subscribeToTCDSeventing();
    void subscribeToBRILeventing();
    void handleTCDSrecords();
    void handleBestLumiRecords();
    TCDSrecordPtr parseTCDSrecord(const toolbox::mem::AutoReference&);
    BestLumiRecordPtr parseBestLumiRecord(const toolbox::mem::AutoReference&);
    void defaultWebPage(xgi::Input*, xgi::Output*);
    void getRunAndLumiSection(xgi::Input*, xgi::Output*);
    cgicc::div lastTCDSrecords() const;
    cgicc::div lastBestLumiRecords() const;

    Configuration configuration_;
    LumisectionHandler lumisectionHandler_;
    DowntimeHandler downtimeHandler_;

    std::queue<TCDSrecordPtr> tcdsRecordsQueue_;
    std::mutex tcdsRecordsQueueMutex_;

    std::queue<BestLumiRecordPtr> bestLumiRecordsQueue_;
    std::mutex bestLumiRecordsQueueMutex_;

    volatile std::atomic<bool> doProcessing_;
    std::thread tcdsRecordsHandler_;
    std::thread bestLumiRecordsHandler_;

  };

} // namespace oms


#endif // _oms_eventinglogger_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
