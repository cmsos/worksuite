#ifndef _oms_downtimeclient_h
#define _oms_downtimeclient_h

#include "oms/DowntimeEntry.h"
#include "oms/TStoreClient.h"

#include "xdaq/Application.h"
#include "xdata/Table.h"


namespace oms {

static const std::string DOWNTIMESVIEW ="urn:tstore-view-SQL:downtimes";
static const std::string DOWNTIMESDEFINITION = "insertNewDowntime";
  class DowntimeClient : public TStoreClient
  {

  public:

    DowntimeClient
    (
      xdaq::Application*,
      const std::string& credentialsFile,
      const std::string& tstoreURL,
      const size_t tstoreLid,
	  const uint32_t tStoreRetries,
	  const uint32_t tStoreRetrySleepSeconds
    );

    DowntimeEntryPtr fetchOpenDowntime();
    void insertNewDowntime(const DowntimeEntryPtr&);
    void updateDowntime(const DowntimeEntryPtr&);

  };

} // namespace oms


#endif // _oms_downtimeclient_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
