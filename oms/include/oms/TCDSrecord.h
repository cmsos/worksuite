#ifndef _oms_tcdsrecord_h
#define _oms_tcdsrecord_h

#include <cstdint>
#include <memory>
#include <ostream>


namespace oms {

  struct TCDSrecord
  {
    uint32_t fill;
    uint32_t run;
    uint32_t ls;
    uint32_t nibble;
    uint32_t nibblesPerSection;
    double nibbleStartTime; // represented as the floating-point number of seconds since 1970-01-01
    double nibbleEndTime;   // represented as the floating-point number of seconds since 1970-01-01
    float deadtime;
    bool runActive;

    TCDSrecord() : fill(0),run(0),ls(0),nibble(0),nibblesPerSection(0),nibbleStartTime(-1),nibbleEndTime(-1),deadtime(0),runActive(false) {}

    bool operator==(const TCDSrecord& other) const
    {
      return ( fill == other.fill && run == other.run && ls == other.ls && nibble == other.nibble );
    }

    bool operator!=(const TCDSrecord& other) const
    {
      return ( fill != other.fill || run != other.run || ls != other.ls || nibble != other.nibble );
    }
  };

  using TCDSrecordPtr = std::shared_ptr<TCDSrecord>;

  inline std::ostream& operator<<(std::ostream& s, const TCDSrecord& r)
  {
    s << "fill=" << r.fill;
    s << ",run=" << r.run;
    s << ",ls=" << r.ls;
    s << ",nibble=" << r.nibble;
    s << ",deadtime=" << r.deadtime << "%";
    s << ",runActive=" << r.runActive;

    return s;
  }

} // namespace oms


#endif // _oms_tcdsrecord_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
