#ifndef _oms_tstoreclient_h
#define _oms_tstoreclient_h

#include <string>
#include <mutex>
#include <mutex>

#include "toolbox/TimeInterval.h"
#include "xdaq/Application.h"
#include "xdata/Table.h"
#include "xoap/MessageReference.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/squeue.h"
#include "toolbox/lang/Class.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"


namespace oms {

static const std::string UPDATEOPERATION = "update";
static const std::string INSERTOPERATION = "insert";
  class TStoreClient: public toolbox::lang::Class, public toolbox::task::TimerListener
  {

  public:

    TStoreClient
    (
      xdaq::Application*,
      const std::string& credentialsFile,
      const std::string& tstoreView,
      const std::string& tstoreURL,
      const size_t tstoreLid,
	  const uint32_t tStoreRetries,
	  const uint32_t tStoreRetrySleepSeconds
    );


    ~TStoreClient();

    void timeExpired(toolbox::task::TimerEvent& e);

  protected:

    void query(const std::string& queryName, xdata::Table& results);
    xdata::Table getTableDefinitionFromDB(const std::string& queryName);
    xdata::Table getTableDefinition();
    void insertTable(const std::string& queryName, xdata::Table& dataToInsert);
    void updateTable(const std::string& queryName, xdata::Table& dataToUpdate);
    void run();
    xdata::Table getOpenDowntimes();
    xdaq::Application* app_;
    xdata::Table openDowntimes_;
    std::mutex openDowntimesMutex_;

  private:

    void connectOrRenew();
    std::string getCredentials() const;
    void connect();
    void disconnect();
    void renew();
    xoap::MessageReference sendSOAPMessage(xoap::MessageReference&);

    void clearConnectionID();
    std::string getConnectionID();

    const std::string credentialsFile_;
    const std::string tstoreView_;
    const std::string viewClass_;
    const toolbox::TimeInterval connectionTimeout_;
    std::mutex connectionIDMutex_;
    std::string connectionID_;
    xdaq::ApplicationDescriptor* tstoreDescriptor_;
    const uint32_t tStoreRetries_;
    const uint32_t tStoreRetrySleepSeconds_;

	toolbox::task::ActionSignature* run_;
	xdata::Table tableDefinition_;
	bool run(toolbox::task::WorkLoop* wl);
	struct Request{
		std::string operation;
		std::string query;
		xdata::Table  *data;
	};
	toolbox::squeue<Request> messages_;




  };

} // namespace oms


#endif // _oms_tstoreclient_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
