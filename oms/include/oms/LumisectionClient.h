#ifndef _oms_lumisectionclient_h
#define _oms_lumisectionclient_h

#include "oms/LumisectionEntry.h"
#include "oms/TStoreClient.h"
#include "xdaq/Application.h"


namespace oms {

static const std::string LUMISECTIONSVIEW = "urn:tstore-view-SQL:lumisections";
static const std::string LUMISECTIONSDEFINITION = "insertNewLumisection";

  class LumisectionClient : public TStoreClient
  {

  public:

    LumisectionClient
    (
      xdaq::Application*,
      const std::string& credentialsFile,
      const std::string& tstoreURL,
      const size_t tstoreLid,
	  const uint32_t tStoreRetries,
	  const uint32_t tStoreRetrySleepSeconds
    );

    LumisectionEntryPtr fetchOpenLumisection();
    void insertNewLumisection(const LumisectionEntryPtr&);

  };

} // namespace oms


#endif // _oms_lumisectionclient_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
