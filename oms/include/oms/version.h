#ifndef _oms_version_h_
#define _oms_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_OMS_VERSION_MAJOR 1
#define WORKSUITE_OMS_VERSION_MINOR 5
#define WORKSUITE_OMS_VERSION_PATCH 5
#define WORKSUITE_OMS_PREVIOUS_VERSIONS "1.0.0,1.0.1,1.0.2,1.0.3,1.1.0,1.1.1,1.1.2,1.2.0,1.3.0,1.4.0,1.4.1,1.4.2,1.5.0,1.5.1,1.5.2,1.5.3,1.5.4"

#define WORKSUITE_OMS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_OMS_VERSION_MAJOR,WORKSUITE_OMS_VERSION_MINOR,WORKSUITE_OMS_VERSION_PATCH)
#ifndef WORKSUITE_OMS_PREVIOUS_VERSIONS
#define WORKSUITE_OMS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_OMS_VERSION_MAJOR,WORKSUITE_OMS_VERSION_MINOR,WORKSUITE_OMS_VERSION_PATCH)
#else
#define WORKSUITE_OMS_FULL_VERSION_LIST  WORKSUITE_OMS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_OMS_VERSION_MAJOR,WORKSUITE_OMS_VERSION_MINOR,WORKSUITE_OMS_VERSION_PATCH)
#endif

namespace oms
{
  const std::string project = "worksuite";
  const std::string package = "oms";
  const std::string versions = WORKSUITE_OMS_FULL_VERSION_LIST;
  const std::string version = PACKAGE_VERSION_STRING(WORKSUITE_OMS_VERSION_MAJOR,WORKSUITE_OMS_VERSION_MINOR,WORKSUITE_OMS_VERSION_PATCH);
  const std::string description = "XDAQ application used for the CMS online monitoring system (OMS)";
  const std::string summary = "CMS OMS library";
  const std::string authors = "Remi Mommsen, Andrea Petrucci";
  const std::string link = "https://gitlab.cern.ch/oms";

  config::PackageInfo getPackageInfo();

  void checkPackageDependencies();

  std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif
