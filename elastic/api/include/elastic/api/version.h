// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius                        *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for elastic::api
//
#ifndef _elastic_api_Version_h_
#define _elastic_api_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_ELASTICAPI_VERSION_MAJOR 1
#define WORKSUITE_ELASTICAPI_VERSION_MINOR 3
#define WORKSUITE_ELASTICAPI_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_ELASTICAPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_ELASTICAPI_PREVIOUS_VERSIONS

//
// Template macros
//
#define WORKSUITE_ELASTICAPI_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_ELASTICAPI_VERSION_MAJOR,WORKSUITE_ELASTICAPI_VERSION_MINOR,WORKSUITE_ELASTICAPI_VERSION_PATCH)
#ifndef WORKSUITE_ELASTICAPI_PREVIOUS_VERSIONS
#define WORKSUITE_ELASTICAPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_ELASTICAPI_VERSION_MAJOR,WORKSUITE_ELASTICAPI_VERSION_MINOR,WORKSUITE_ELASTICAPI_VERSION_PATCH)
#else 
#define WORKSUITE_ELASTICAPI_FULL_VERSION_LIST  WORKSUITE_ELASTICAPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_ELASTICAPI_VERSION_MAJOR,WORKSUITE_ELASTICAPI_VERSION_MINOR,WORKSUITE_ELASTICAPI_VERSION_PATCH)
#endif 

namespace elasticapi
{
	const std::string project = "worksuite";
	const std::string package  =  "elasticapi";
	const std::string versions = WORKSUITE_ELASTICAPI_FULL_VERSION_LIST;
	const std::string summary = "Elasticsearch XDAQ interface";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, P.Roberts, D. Simelevicius";
	const std::string link = "http://www.elasticsearch.org/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
