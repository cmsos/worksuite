// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _restate_api_h_
#define _restate_api_h_

#include "httpserver.hpp"
#include <iostream>

#include <variant>
#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"

namespace restate
{
	//typedef httpserver::http_resource  resource;
	//typedef httpserver::http_request  request;
	//typedef httpserver::http_response  response;
	//typedef httpserver::string_response  string;

	using resource = httpserver::http_resource;
	using request = httpserver::http_request;
	using response = httpserver::http_response;
	using string = httpserver::string_response;

	void register_resource(const std::string & path, const std::function<std::shared_ptr<restate::response>(const restate::request&)> & r);

    //using connection = RestClient::Connection;
    using headers = RestClient::HeaderFields;
    //using result = RestClient::Response;

    class result: public RestClient::Response
    {
    public:

	result()
	{
	}
	result(const RestClient::Response &obj): RestClient::Response({.code = obj.code,.body =obj.body,.headers =obj.headers})
	{
	}
	
        const restate::headers &  getHeaders()
	{
		return this->headers;
	}

        int getCode()
	{
		return this->code;
	}
        const std::string & getContent()
        {
            	return this->body;
        }
    };

    class connection: public RestClient::Connection {
    public:
        
        connection(const std::string & baseurl): RestClient::Connection(baseurl)
        {
        }
        
        std::shared_ptr<restate::result> get(const std::string & path)
        {
		auto r = std::make_shared<restate::result>();
            	*r = RestClient::Connection::get(path);
            	return r;
            //RestClient::Response r = RestClient::Connection::get(path);
            //return std::shared_ptr<restate::result>( new restate::result(r));
        }
    };

}
#endif
