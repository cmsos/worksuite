<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
    <xp:Endpoint protocol="http" service="restful" hostname="xdaqdev1.cms" port="1919" network="restate"/>

    <xp:Application class="restate::Application" id="11" network="local" group="worksuite" logpolicy="inherit">
      <properties xmlns="urn:xdaq-application::restate::Application" xsi:type="soapenc:Struct">
      </properties>
    </xp:Application>

    <xp:Application class="restate::Service" id="114" network="local" group="test" service="service" logpolicy="inherit">
    </xp:Application>

    <xp:Module>/nfshome0/lorsini/devel/localbus/worksuite/restate/lib/linux/x86_64_centos8/librestate.so</xp:Module>

</xp:Profile>
