// // $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "restate/Service.h"
#include <nlohmann/json.hpp>
#include <sstream>      // std::stringstream


XDAQ_INSTANTIATOR_IMPL (restate::Service)

restate::Service::Service(xdaq::ApplicationStub* s) : xdaq::Application(s)
{
	auto aFunc = std::bind(&restate::Service::render, this, std::placeholders::_1);
	restate::register_resource("/user/service/{name}/{value|[0-9]+}",aFunc);
    
    restate::register_resource("/call",std::bind(&restate::Service::call, this, std::placeholders::_1));
    
}

restate::Service::~Service()
{
}

const std::shared_ptr<restate::response> restate::Service::render(const restate::request& r)
{
	std::cout<< "call restate::Service::render " << std::endl;

	std::string name = r.get_arg("name");
	unsigned int value = std::stoi(r.get_arg("value"));

	std::stringstream page;
	page << "<!DOCTYPE html>" << "<html><head><title>User service example</title></head>";
	page << "<link rel=\"shortcut icon\" href=\"data:image/x-icon;,\" type=\"image/x-icon\">";
	page << "<body>" << "<p>Received path " <<  r.get_path() <<  " extracted  " <<  name << " with value " <<  std::to_string(value) << "<p></body></html>";


    return std::shared_ptr<restate::response>(new restate::string(page.str(),httpserver::http::http_utils::http_ok, "text/html;charset=UTF-8"));

}


const std::shared_ptr<restate::response> restate::Service::call(const restate::request& r)
{ 
	
    std::cout<< "call restate::Service::call " << std::endl;
    
    // get a connection object to myself
    auto conn = new restate::connection(this->getApplicationContext()->getContextDescriptor()->getURL());

    std::shared_ptr<restate::result> result = conn->get("/user/service/call/1");

    std::cout<< "result code  " << result->getCode()  << std::endl;

    return std::shared_ptr<restate::response>(new restate::string(result->getContent(),httpserver::http::http_utils::http_ok, "text/plain;charset=UTF-8"));
}
