// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "xcept/tools.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "restate/Application.h"
#include "httpserver.hpp"

#include <nlohmann/json.hpp>
#include "xdata/json/Serializer.h"

#include "xdaq/ApplicationRegistry.h"

#include "restate/Address.h"

#include "pt/PeerTransportAgent.h"

class root_resource : public httpserver::http_resource {
public:
	root_resource(restate::Application & a): application(a){}

	const std::shared_ptr<httpserver::http_response> render(const httpserver::http_request& r)
    {

		if ( r.get_path() == "/" )
		{
			std::stringstream page;
			page << "<!DOCTYPE html>" << "<html><head><title>Restate application</title></head>";
			page << "<link rel=\"shortcut icon\" href=\"data:image/x-icon;,\" type=\"image/x-icon\">";
			page << "<body>/api to show all paths available</body></html>";
			return std::shared_ptr<restate::response>(new restate::string(page.str(),httpserver::http::http_utils::http_ok, "text/html;charset=UTF-8"));
		}

		return std::shared_ptr<httpserver::http_response>(new httpserver::string_response("path requestsed not available: " + r.get_path() ));
    }

	restate::Application & application;
};

class applications_resource : public httpserver::http_resource {
public:
	applications_resource(restate::Application & a): self(a){}

	const std::shared_ptr<httpserver::http_response> render(const httpserver::http_request& req)
    		{
		std::set<const xdaq::ApplicationDescriptor*> merged;

		std::set<std::string> zones = self.getApplicationContext()->getZoneNames();
		for (std::set<std::string>::iterator i = zones.begin(); i != zones.end(); i++)
		{
			std::set<const xdaq::ApplicationGroup *> groups = self.getApplicationContext()->getZone(*i)->getGroups();

			for (std::set<const xdaq::ApplicationGroup *>::iterator j = groups.begin(); j != groups.end(); j++)
			{
				std::set<const xdaq::ApplicationDescriptor*> descriptors = (*j)->getApplicationDescriptors();
				merged.insert(descriptors.begin(), descriptors.end());
			}
		}

		nlohmann::json json;

		for (std::set<const xdaq::ApplicationDescriptor*>::iterator i = merged.begin(); i != merged.end(); i++)
		{
			nlohmann::json apie;

			const xdaq::ApplicationDescriptor* d = *i;

			apie["class"] =  d->getClassName();
			if (d->hasInstanceNumber())
			{
				apie["instance"] = d->getInstance();
			}

			apie["lid"] = d->getLocalId();

			// List groups the application belongs
			std::string groupList = "";
			//--
			std::set<xdaq::ApplicationDescriptor*> merged;
			std::set < std::string > zones = self.getApplicationContext()->getZoneNames();
			for (std::set<std::string>::iterator j = zones.begin(); j != zones.end(); j++)
			{
				std::set<const xdaq::ApplicationGroup *> groups = self.getApplicationContext()->getZone(*j)->getGroups();
				for (std::set<const xdaq::ApplicationGroup *>::iterator i = groups.begin(); i != groups.end(); i++)
				{
					if ((*i)->hasApplicationDescriptor(d))
					{
						if (groupList != "") groupList += ",";
						groupList += (*j) + ".";
						groupList += (*i)->getName();
					}
				}
			}

			apie["group"] =  groupList;

			std::string url = d->getContextDescriptor()->getURL();
			apie["context"] = url;

			json.push_back(apie);

		}
		return std::shared_ptr<httpserver::http_response>(new httpserver::string_response(json.dump(4), httpserver::http::http_utils::http_ok, "application/json"));
    		}
	restate::Application & self;
};


class application_resource : public httpserver::http_resource {
public:
	application_resource(restate::Application & a): self(a){}

	const std::shared_ptr<httpserver::http_response> render(const httpserver::http_request& req)
    {
		std::string name = req.get_arg("name");
		unsigned int lid = std::stoi(req.get_arg("lid"));

		xdaq::Application * a = 0;
		try
		{
			a = self.getApplicationContext()->getApplicationRegistry()->getApplication(lid);
		}
		catch (xdaq::exception::ApplicationNotFound& e)
		{
			return std::shared_ptr<httpserver::string_response>(new httpserver::string_response("Not Found", 404, "text/plain"));
		}

		xdata::json::Serializer serializer;

		try
		{
			auto s =  a->getApplicationInfoSpace()->find(name);
			xdata::Event e("urn:xdaq-event:ParameterGet", this);
			a->getApplicationInfoSpace()->fireEvent(e);
			nlohmann::json json;
			serializer.exportAll (s, json);
			//std::cout << "dump " << json.dump(4)  << std::endl;

			return std::shared_ptr<httpserver::http_response>(new httpserver::string_response(json.dump(4), httpserver::http::http_utils::http_ok, "application/json"));
		}
		catch (xdata::exception::Exception & xde)
		{
			return std::shared_ptr<httpserver::string_response>(new httpserver::string_response("Not Found", 404, "text/plain"));
		}
    }
	restate::Application & self;
};

class service_resource : public httpserver::http_resource {
public:
	service_resource(restate::Application & a): self(a){}

	const std::shared_ptr<httpserver::http_response> render(const httpserver::http_request& req)
    {
		std::string name = req.get_arg("name");
		std::string parameter = req.get_arg("parameter");

		std::vector<xdaq::Application*> services;
		services = self.getApplicationContext()->getApplicationRegistry()->getApplications("service",name);
		if (services.size() == 0 )
		{
			return  std::shared_ptr<httpserver::string_response>(new httpserver::string_response("Not Found", 404, "text/plain"));
		}

		xdata::json::Serializer serializer;

		try
		{
			auto s =  services[0]->getApplicationInfoSpace()->find(parameter);
			xdata::Event e("urn:xdaq-event:ParameterGet", this);
			services[0]->getApplicationInfoSpace()->fireEvent(e);
			nlohmann::json json;
			serializer.exportAll (s, json);
			//std::cout << "dump " << json.dump(4)  << std::endl;

			return std::shared_ptr<httpserver::http_response>(new httpserver::string_response(json.dump(4), httpserver::http::http_utils::http_ok, "application/json"));
		}
		catch (xdata::exception::Exception & xde)
		{
			return std::shared_ptr<httpserver::string_response>(new httpserver::string_response("Not Found", 404, "text/plain"));
		}
    }
	restate::Application & self;
};

// restate example

/*
class method : public httpserver::http_resource {
public:
    method(restate::Application & a ): self(a){}

    const std::shared_ptr<httpserver::http_response> render(const httpserver::http_request& req)
    {
	    return self.render(req);
    }

    restate::Application & self;
};
 */

class cmethod : public httpserver::http_resource {
public:
	cmethod(const std::function<std::shared_ptr<restate::response>(const restate::request&)>  & cbf ): m_CbFunc(cbf){}

	const std::shared_ptr<httpserver::http_response> render(const httpserver::http_request& req)
    {
		return m_CbFunc(req);
    }
	const std::function<std::shared_ptr<restate::response>(const restate::request&)> m_CbFunc;
};

static std::map<std::string,httpserver::http_resource*> resources;


void restate::register_resource(const std::string & path, const std::function<std::shared_ptr<restate::response>(const restate::request&)> & r)
{
	if (resources.find(path) != resources.end())
	{
		XCEPT_RAISE(xdaq::exception::Exception, "Duplicate resource, " + path + " alredy existing");
	}

	resources[path] = new cmethod(r);
}

class api_resource : public httpserver::http_resource {
public:
	api_resource(restate::Application & a): application(a){}

	const std::shared_ptr<httpserver::http_response> render(const httpserver::http_request&)
    {
		nlohmann::json json;
		for (const auto& [path, value] : resources)
		{
			nlohmann::json apie;

			apie["path"] = path;
			apie["version"] = "v1";
			apie["description"] = "";
			json.push_back(apie);
		}
		return std::shared_ptr<httpserver::http_response>(new httpserver::string_response(json.dump(4), httpserver::http::http_utils::http_ok, "application/json"));
    }

	restate::Application & application;
};

XDAQ_INSTANTIATOR_IMPL (restate::Application)

restate::Application::Application(xdaq::ApplicationStub* s) : xdaq::Application(s)
{
	s->getDescriptor()->setAttribute("icon16x16", "/restate/images/restate.png");
	s->getDescriptor()->setAttribute("icon","/restate/images/restate.png");

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	this->getApplicationContext()->addActionListener(this);

	resources["/api"] = new api_resource(*this);
	resources["/api/application/{lid|[0-9]+}/{name}"] = new application_resource(*this);
	resources["/api/service/{name}/{parameter}"] = new service_resource(*this);
	resources["/api/applications"] = new applications_resource(*this);


	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	pta->addPeerTransport(this);

}

restate::Application::~Application()
{
}

void restate::Application::actionPerformed(toolbox::Event& e)
{
	// Either configuration loaded or initialization completed (without a configuration) resources are initialized
	if( e.type() == "urn:xdaq-event:configuration-loaded" || e.type() == "urn:xdaq-event:initialization-completed")
	{
		LOG4CPLUS_DEBUG(getApplicationLogger(), "activate restful resource for servers" <<  e.type());

		for(const auto& s : servers_)
		{

			std::thread threadObj([](restate::Application * a, httpserver::webserver * s){

				s->register_resource("/", new root_resource(*a), true);

				for (const auto& [path, value] : resources)
				{
					LOG4CPLUS_DEBUG(a->getApplicationLogger(), "register user resource " << path << " on " << s);
					s->register_resource(path,value);
				}

				s->start(true);

			},this, s);

			threadObj.detach();
		}
	}
}

void restate::Application::actionPerformed(xdata::Event& e)
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());

	if( e.type() == "urn:xdaq-event:setDefaultValues" )
	{


	}
}

// PT
//
std::string restate::Application::getProtocol()
{
	return "http";
}

std::vector<std::string> restate::Application::getSupportedServices()
{
	std::vector<std::string> s;
	s.push_back("restful");
	s.push_back("soap");
	// in the future also have a http "message" service for GET and POST operations
	return s;
}

bool restate::Application::isServiceSupported(const std::string& service )
{
	if (service == "restful") return true;
	if (service == "soap") return true;
	return false;
}

pt::TransportType restate::Application::getType()
{
	return pt::Receiver;
}

pt::Address::Reference restate::Application::createAddress( const std::string& url, const std::string & service )
{
	return pt::Address::Reference(new restate::Address(url,service));
}

pt::Address::Reference restate::Application::createAddress( std::map<std::string, std::string, std::less<std::string> >& address )

{
	std::string protocol = address["protocol"];

	if (protocol == "http")
	{
		std::string url = protocol;

		XCEPT_ASSERT (address["hostname"] != "",pt::exception::InvalidAddress , "Cannot create address, hostname not specified");
		XCEPT_ASSERT (address["port"] != "",pt::exception::InvalidAddress , "Cannot create address, port number not specified");

		url += "://";
		url += address["hostname"];
		url += ":";
		url += address["port"];

		std::string service = address["service"];
		if (service != "")
		{
			if (!this->isServiceSupported(service))
			{
				std::string msg = "Cannot create address, specified service for protocol ";
				msg += protocol;
				msg += " not supported: ";
				msg += service;
				XCEPT_RAISE(pt::exception::InvalidAddress, msg);
			}

			//url += "/";
			//url += service;
		}
		else
		{
			std::string msg = "Cannot create address, service for protocol ";
			msg += protocol;
			msg += " not specified";
			XCEPT_RAISE(pt::exception::InvalidAddress, msg);
		}

		// throws already pt::exception::InvalidAddress, will be chained up
		return this->createAddress(url,service);
	}
	else
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}
}

void restate::Application::addServiceListener (pt::Listener* listener)
{
	pt::PeerTransportReceiver::addServiceListener(listener);
}

void restate::Application::removeServiceListener (pt::Listener* listener )
{
	pt::PeerTransportReceiver::removeServiceListener(listener);
}

void restate::Application::removeAllServiceListeners()
{
	pt::PeerTransportReceiver::removeAllServiceListeners();
}

void restate::Application::config( pt::Address::Reference address)
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "configuring address for restate" << address->toString() );
	restate::Address & a = dynamic_cast<restate::Address &>(*address);
	uint16_t port = std::stoi(a.getPort());
	auto ws = new httpserver::webserver(httpserver::create_webserver(port).start_method(httpserver::http::http_utils::THREAD_PER_CONNECTION));
	servers_.push_back( ws );
}

