/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest, D.Simelevicius              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/QueuePair.h"

#include "ibvla/Utils.h"

#include <errno.h>
#include <string.h>
#include <sstream>

ibvla::QueuePair::QueuePair (ibvla::ProtectionDomain &pd, ibv_qp * qp)
	: pd_(pd), qp_(qp)
{
}

ibvla::QueuePair::~QueuePair ()
{
}

void ibvla::QueuePair::modify (ibv_qp_attr &attr, int attr_mask) 
{
	int ret = ibv_modify_qp(qp_, &attr, attr_mask);
	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to modify queue pair: ";
		if (ret == EINVAL)
		{
			ss << "Invalid value provided in attr or in attr_mask, " << strerror(ret) << "(" << ret << ")";
		}
		else if (ret == ENOMEM)
		{
			ss << "Not enough resources to complete this operation, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

/*
 * @param in attr_mask
 * @param out attr
 * @param out init_attr
 */
void ibvla::QueuePair::query (int attr_mask, ibv_qp_attr &attr, ibv_qp_init_attr &init_attr) 
{
	memset(&attr, 0, sizeof(ibv_qp_attr));
	memset(&init_attr, 0, sizeof(ibv_qp_init_attr));

	int ret = ibv_query_qp(qp_, &attr, attr_mask, &init_attr);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to query queue pair: ";
		if (ret == ENOMEM)
		{
			ss << "Not enough resources to complete this operation, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

void ibvla::QueuePair::postSend (ibv_send_wr &wr) 
{
	ibv_send_wr *bad_wr;

	int ret = ibv_post_send(qp_, &wr, &bad_wr);
	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to post send queue pair: ";
		if (ret == EINVAL)
		{
			ss << "Invalid value provided in wr, " << strerror(ret) << "(" << ret << ")";
		}
		else if (ret == ENOMEM)
		{
			ss << "Send Queue is full or not enough resources to complete this operation, " << strerror(ret) << "(" << ret << ")";
			XCEPT_RAISE(ibvla::exception::QueueFull, ss.str());
		}
		else if (ret == EFAULT)
		{
			ss << "Invalid value provided in qp, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::InternalError, ss.str());
	}
}

void ibvla::QueuePair::postRecv (ibv_recv_wr &wr) 
{
	ibv_recv_wr *bad_wr;

	int ret = ibv_post_recv(qp_, &wr, &bad_wr);
	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to post recv on queue pair, ";
		if (ret == EINVAL)
		{
			ss << "Invalid value provided in wr, " << strerror(ret) << "(" << ret << ")";
		}
		else if (ret == ENOMEM)
		{
			ss << "Receive Queue is full or not enough resources to complete this operation, " << strerror(ret) << "(" << ret << ")";
			XCEPT_RAISE(ibvla::exception::QueueFull, ss.str());
		}
		else if (ret == EFAULT)
		{
			ss << "Invalid value provided in qp, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

uint32_t ibvla::QueuePair::getNum ()
{
	return qp_->qp_num;
}

enum ibv_qp_state ibvla::QueuePair::getState ()
{
	return qp_->state;
}

void* ibvla::QueuePair::getContext()
{
	return qp_->qp_context;
}

// USE FOR DEBUG ONLY
namespace ibvla
{
	std::ostream& operator<< (std::ostream &ss, ibvla::QueuePair & qp)
	{
		ss << "QPN = " << qp.qp_->qp_num;
		ss << " context[" << qp.qp_->context << "]";
		ss << ", pd[" << qp.qp_->pd << "]";
		ss << ", send_cq[" << qp.qp_->send_cq << "]";
		ss << ", recv_cq[" << qp.qp_->recv_cq << "]";
		ss << ", srq[" << qp.qp_->srq << "]";
		ss << ", STATE = '" << ibvla::stateToString(qp) << "'";
		//ss << ", eventsCompleted = '" << qp.qp_->events_completed << "'";

		//ss << ", remote QPN # -1";

		return ss;
	}
}
