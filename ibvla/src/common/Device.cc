// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/Device.h"

#include <errno.h>
#include <string.h>
#include <sstream>

ibvla::Device::Device (ibv_device*  d)
	: device_(d)
{
}

std::string ibvla::Device::getName ()
{
	return ibv_get_device_name(device_);
}

uint64_t ibvla::Device::getGUID ()
{
	return ibv_get_device_guid(device_);
}
