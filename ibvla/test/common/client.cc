// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                 			             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, D. Simelevicius	 				     *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   		                     *
 *************************************************************************/

#include <stdio.h>
#include <infiniband/verbs.h>
#include <thread>
#include <sys/types.h>
#include <netinet/in.h>
#include <inttypes.h>
#include <byteswap.h>
#include <errno.h>
#include <string.h>
#include <sstream>


#include "xcept/tools.h"
#include "ibvla/ProtectionDomain.h"
#include "ibvla/CompletionQueue.h"
#include "ibvla/QueuePair.h"
#include "ibvla/Device.h"
#include "ibvla/Connector.h"
#include "ibvla/ConnectionRequest.h"
#include "ibvla/Utils.h"
#include "ibvla/Buffer.h"
#include "ibvla/Allocator.h"

 
#define PORT      1
#define SGIINDEX  3 
#define ISGLOBAL  true
#define IBPATH    0
#define MTU       2048
#define MSGSIZE   16384 
#define QPSIZE    8
#define HOSTIP    "10.177.129.9"
#define HOSTPORT  "30000"
#define LEN      8192 

class Client
{
   public:

    std::list<ibvla::Device> devices;
    ibvla::Context context;
    ibvla::ProtectionDomain pd;
    ibvla::CompletionQueue cq;
    ibvla::Acceptor * acceptor;
    ibvla::QueuePair qp;

    Client()
    {
        try
        {
            devices = ibvla::getDeviceList();

            if (devices.size() == 0)
            {
                    std::cerr << "-- CLIENT : no IB devices found" << std::endl;
                    return;
            }

            std::cout << "-- CLIENT Found " << devices.size() << " devices" << std::endl;

            for (std::list<ibvla::Device>::iterator i = devices.begin(); i != devices.end(); i++)
            {
                std::cout << "-- CLIENT : Device name = '" << (*i).getName() << "', guid = ' 0x" << std::hex << bswap_64((*i).getGUID()) << std::dec << "'" << std::endl;
            }

            std::cout << "-- CLIENT : Create context" << std::endl;
            context = ibvla::createContext(devices.front());

            std::cout << "-- CLIENT : Query device" << std::endl;
            ibv_device_attr d_att = context.queryDevice();

            uint8_t num_ports = d_att.phys_port_cnt;

            std::cout << "-- CLIENT : Device has " << (unsigned int) num_ports << " ports" << std::endl;
           
            std::cout << "-- CLIENT : allocate protection domain" << std::endl;
            pd = context.allocateProtectionDomain();

            std::cout << "-- CLIENT : create completion queue" << std::endl;
            cq = context.createCompletionQueue(256, 0, 0);

        }
        catch (ibvla::exception::Exception & e)
        {
            std::cerr << "-- CLIENT Caught exception : " << e.what() << std::endl; // << xcept::stdformat_exception_history(e) << std::endl;
            return;
        }

    	}

	void connect ()
  	{
        	std::cout << "-- CLIENT : create queue pair" << std::endl;
        	qp = pd.createQueuePair(cq, cq, QPSIZE, QPSIZE, 0);
           	// init QP
        	std::cout << "-- CLIENT : initialize queue pair" << std::endl;
		ibv_qp_attr qpattr;
		memset(&qpattr, 0, sizeof(qpattr));

		qpattr.qp_state = IBV_QPS_INIT;
		qpattr.pkey_index = 0;
		qpattr.port_num = PORT;
		qpattr.qp_access_flags = 0;

		try
		{
			qp.modify(qpattr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
        	}
        	catch (ibvla::exception::Exception & e)
        	{
           	 	std::cerr << "-- CLIENT Caught exception : " << e.what() <<  std::endl; 
            		return;
        	}
		if (qp.getState() == IBV_QPS_ERR || qp.getState() == IBV_QPS_RESET || qp.getState() != IBV_QPS_INIT)
        	{
		    	std::cout << "-- CLIENT Attempting to connect with QueuePair in " << ibvla::stateToString(qp) << " state" << std::endl;
            		return;	
        	}

        	std::cout << "-- CLIENT : connect queue pair" << std::endl;
		try
		{
			ibvla::Connector conn(MTU, true, SGIINDEX, ISGLOBAL);
			conn.connect(qp, HOSTIP, HOSTPORT, 0);
		}
		catch (ibvla::exception::Exception & e)
		{
        		std::cerr << "-- CLIENT : error connecting " << e.what() << std::endl;
			return;
		}
    }

    void postbuffer(ibvla::Buffer * buffer ) 
    {
        memset(&(buffer->send_wr), 0, sizeof(buffer->send_wr));

	buffer->op_sge_list = buffer->buffer_sge_list;
	buffer->op_sge_list.addr = (uint64_t) buffer->getAddress();
	buffer->op_sge_list.length = LEN;

	buffer->send_wr.wr_id = (uint64_t) buffer; // cookie
	buffer->send_wr.sg_list = &(buffer->op_sge_list);
	buffer->send_wr.num_sge = 1;
	buffer->send_wr.opcode = IBV_WR_SEND;
	buffer->send_wr.send_flags = IBV_SEND_SIGNALED;
	buffer->send_wr.next = 0;
	buffer->opcode = ibvla::SEND_OP;

	try
	{
		std::cout << "-- CLIENT Sending message size == " << buffer->op_sge_list.length  << std::endl;
		qp.postSend(buffer->send_wr);
	}
	catch (ibvla::exception::QueueFull & e)
	{
		std::cerr << "-- CLIENT : error sending queue full " << e.what() << std::endl;
		return;
	}
	catch (ibvla::exception::InternalError & e)
	{
		std::cerr << "-- CLIENT : error sending " << e.what() << std::endl;
		return;
	}
    }


    void run()
    {
        this->connect();

        toolbox::mem::Allocator* allocator = new ibvla::Allocator(pd, "server", 0x100000);

        ibvla::Buffer * buffer = dynamic_cast<ibvla::Buffer*>(allocator->alloc (MSGSIZE, 0));
        this->postbuffer(buffer);

	struct ibv_wc wc;
        int ret;

        while(1)
    	{
            try
            {
                ret = cq.poll(1, &wc);
                if (ret == 0)
                {
		            continue;
                }

   	        if (wc.status != IBV_WC_SUCCESS)
                {
                    std::cerr << "-- CLIENT : error sending " << std::endl;
                    return;
                }

                if (wc.opcode == IBV_WC_SEND)
                {
                    ibvla::Buffer * buffer = reinterpret_cast<ibvla::Buffer*>(wc.wr_id);
		    sleep(2);
                    this->postbuffer(buffer);
                
            	}
	        else
	        {
               	   std::cerr << "-- Client : Unknown work request has arrived with success flag set " << std::endl;
                   return;
	        }
            }
            catch (ibvla::exception::Exception & e)
            {
                std::cerr <<  "-- Client Failed sending buffer" << e.what() << std::endl;
                return;
            }
        }
    }
};


int main (void)
{

	Client client;
    	client.run();
	return 0;

}
