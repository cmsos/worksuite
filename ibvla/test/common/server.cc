// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                 			             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, D. Simelevicius	 				     *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   		                     *
 *************************************************************************/

#include <stdio.h>
#include <infiniband/verbs.h>
#include <thread>
#include <sys/types.h>
#include <netinet/in.h>
#include <inttypes.h>
#include <byteswap.h>
#include <errno.h>
#include <string.h>
#include <sstream>


#include "xcept/tools.h"
#include "ibvla/ProtectionDomain.h"
#include "ibvla/CompletionQueue.h"
#include "ibvla/QueuePair.h"
#include "ibvla/Device.h"
#include "ibvla/Acceptor.h"
#include "ibvla/AcceptorListener.h"
#include "ibvla/ConnectionRequest.h"
#include "ibvla/Utils.h"
#include "ibvla/Buffer.h"
#include "ibvla/Allocator.h"
#include "ibvla/ConnectionRequest.h"

 
#define PORT      1
#define SGIINDEX  5 
#define ISGLOBAL  true
#define IBPATH    0
#define MTU       2048
#define MSGSIZE   16384
#define QPSIZE    8
#define HOSTIP    "10.177.129.9"
#define HOSTPORT  "30000"

class Server: public ibvla::AcceptorListener
{
    public:

    std::list<ibvla::Device> devices;
    ibvla::Context context;
    ibvla::ProtectionDomain pd;
    ibvla::CompletionQueue cq;
    ibvla::Acceptor * acceptor;
    ibvla::QueuePair qp;

    Server()
    {
        try
        {
            devices = ibvla::getDeviceList();

            if (devices.size() == 0)
            {
                    std::cerr << "-- SERVER : no IB devices found" << std::endl;
                    return;
            }

            std::cout << "-- SERVER Found " << devices.size() << " devices" << std::endl;

            for (std::list<ibvla::Device>::iterator i = devices.begin(); i != devices.end(); i++)
            {
                std::cout << "-- SERVER : Device name = '" << (*i).getName() << "', guid = ' 0x" << std::hex << bswap_64((*i).getGUID()) << std::dec << "'" << std::endl;
            }

            std::cout << "-- SERVER : Create context" << std::endl;
            context = ibvla::createContext(devices.front());

            std::cout << "-- SERVER : Query device" << std::endl;
            ibv_device_attr d_att = context.queryDevice();

            uint8_t num_ports = d_att.phys_port_cnt;

            std::cout << "-- SERVER : Device has " << (unsigned int) num_ports << " ports" << std::endl;

           
            std::cout << "-- SERVER : allocate protection domain" << std::endl;
            pd = context.allocateProtectionDomain();

            std::cout << "-- SERVER : create completion queue" << std::endl;
            cq = context.createCompletionQueue(256, 0, 0);

            std::cout << "-- SERVER : create queue pair" << std::endl;
            qp = pd.createQueuePair(cq, cq, QPSIZE, QPSIZE, 0);
           

        }
        catch (ibvla::exception::Exception & e)
        {
            std::cerr << "-- SERVER Caught exception : " << e.what() << std::endl; // << xcept::stdformat_exception_history(e) << std::endl;
            return;
        }
    }

    void connectionRequest (ibvla::ConnectionRequest id)
    {
        std::cerr <<  "-- SERVER received connection" << std::endl;
	try
	{
	    qp = pd.createQueuePair(cq, cq, 0, QPSIZE, 0);

	    // init qp
	    ibv_qp_attr qpattr;
	    memset(&qpattr, 0, sizeof(qpattr));

            qpattr.qp_state = IBV_QPS_INIT;
            qpattr.pkey_index = 0;
            qpattr.port_num = id.acceptor_->ibPort_; // physical port number
            qpattr.qp_access_flags = 0;

            qp.modify(qpattr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
            
            toolbox::mem::Allocator* allocator = new ibvla::Allocator(pd, "server", 0x100000);

	    for (size_t i = 0; i < QPSIZE; i++)
	    {
                ibvla::Buffer * buffer = dynamic_cast<ibvla::Buffer *>(allocator->alloc (MSGSIZE, 0));
                this->postbuffer(buffer);
	    }
	    id.acceptor_->accept(id, qp);
	}
        catch (ibvla::exception::Exception & e)
        {
            std::cerr <<  "-- SERVER Rejecting connection due to failure to create QP" << std::endl;
            id.acceptor_->reject(id);
            return;
        }

         // start receiving thread here
        std::cout << "-- SERVER start receiver loop" << std::endl;

	std::thread th(&Server::receiveloop, this, "sender");
	th.detach();
    }

    void postbuffer(ibvla::Buffer * buffer ) 
    {
        memset(&(buffer->recv_wr), 0, sizeof(buffer->recv_wr));
        buffer->op_sge_list = buffer->buffer_sge_list;
        buffer->recv_wr.wr_id = (uint64_t) buffer;
        buffer->recv_wr.sg_list = &(buffer->op_sge_list);
        buffer->recv_wr.num_sge = 1;
        buffer->recv_wr.next = 0;
        buffer->opcode = ibvla::RECV_OP;
        try
        {
            qp.postRecv(buffer->recv_wr);
        }
        catch (ibvla::exception::Exception & e)
        {
            std::cerr <<  "-- SERVER failed in posting receive buffer" << std::endl;
            return;
        }
    }

    void receiveloop(std::string name)
    {
        struct ibv_wc wc;
        int ret;

	    while(1)
	    {
            try
            {
                ret = cq.poll(1, &wc);
            }
            catch (ibvla::exception::Exception & e)
            {
                std::cerr <<  "-- SERVER Failed process completion queue" << std::endl;
                return;
            }

            if (ret == 0)
            {
                continue;
            }

            if (wc.status != IBV_WC_SUCCESS)
            {
                std::cerr <<  "-- SERVER Failed  on completion queue" << wc.status <<  std::endl;
                return;
            }

            if (wc.opcode == IBV_WC_RECV)
            {
        	std::cout <<  "-- SERVER got buffer " << std::endl;
                ibvla::Buffer * buffer = reinterpret_cast<ibvla::Buffer*>(wc.wr_id);
                this->postbuffer(buffer);
            }
            else
            {
                std::cerr <<  "-- SERVER Unknown work request has arrived with success flag set" << wc.status <<  std::endl;
                return;
            }
        }	       
	}

    void run()
    {
        std::cout <<  "-- SERVER create acceptor on " << HOSTIP << " and port " << HOSTPORT << std::endl;
        ibvla::Acceptor * acceptor = new ibvla::Acceptor("server", this, MTU, cq, 0, PORT, IBPATH, SGIINDEX, ISGLOBAL, 0, 0);
        acceptor->listen(HOSTIP, HOSTPORT);
	pause();
    }
};


int main (void)
{

	Server server;
    server.run();
	return 0;

}
