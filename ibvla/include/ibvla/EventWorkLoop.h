// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_EventWorkLoop_h
#define _ibvla_EventWorkLoop_h

#include <iostream>
#include <string>

#include "ibvla/exception/Exception.h"

#include "ibvla/EventHandler.h"
#include "ibvla/CompletionQueue.h"

#include "toolbox/lang/Class.h"
#include "toolbox/task/PollingWorkLoop.h"

namespace ibvla
{
	class EventWorkLoop : public virtual toolbox::lang::Class
	{
		public:

			EventWorkLoop (const std::string & name, Context context, EventHandler * handler) ;

			virtual ~EventWorkLoop ();

			bool process (toolbox::task::WorkLoop* wl);

		protected:

			Context context_;
			toolbox::task::WorkLoop* workLoop_;
			toolbox::task::ActionSignature* process_;
			ibvla::EventHandler * handler_;
			std::string name_;

		public:

			size_t receivedEventCounter_;

	};

}

#endif
