// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_exception_Exception_h_
#define _ibvla_exception_Exception_h_

#include "pt/exception/Exception.h"

#define IBVLA_XCEPT_ALLOC( EXCEPTION, MSG ) \
new EXCEPTION ( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__)

#define IBVLA_XCEPT_ALLOC_NESTED( EXCEPTION, MSG, PREVIOUS ) \
new EXCEPTION ( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__, PREVIOUS)

namespace ibvla
{
	namespace exception
	{

		class Exception : public pt::exception::Exception
		{
			public:
				Exception (std::string name, std::string message, std::string module, int line, std::string function)
					: pt::exception::Exception(name, message, module, line, function)
				{
				}

				Exception (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
					: pt::exception::Exception(name, message, module, line, function, e)
				{
				}
		};
	}
}

#endif
