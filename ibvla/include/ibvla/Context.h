// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_Context_h
#define _ibvla_Context_h

#include <infiniband/verbs.h>

#include "ibvla/exception/Exception.h"

#include <stdint.h>
#include <string>

namespace ibvla
{

	class ProtectionDomain;
	class CompletionQueue;

	class Context
	{
		public:

			ibv_context *context_;

			Context ()
			{
				context_ = 0;
			}

			Context (ibv_context *context_in);
			~Context ();

			ibv_device_attr queryDevice () ;
			ibv_port_attr queryPort (uint8_t port_num) ;
			ibv_gid queryGID (uint8_t port_num, int index) ;
			uint16_t queryPKey (uint8_t port_num, int index) ;

			std::string getDeviceName ();

			ProtectionDomain allocateProtectionDomain () ;
			void deallocateProtectionDomain (ProtectionDomain & pd) ;

			CompletionQueue createCompletionQueue (int size, void* userContext, int comp_vector) ;
			void destroyCompletionQueue (CompletionQueue& cq) ;

			ibv_async_event waitAsyncEvent () ;
	};
}

#endif
