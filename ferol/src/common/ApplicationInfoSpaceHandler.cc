#include "ferol/ApplicationInfoSpaceHandler.hh"
#include "ferol/loggerMacros.h"
#include "ferol/ferolConstants.h"

ferol::ApplicationInfoSpaceHandler::ApplicationInfoSpaceHandler( xdaq::Application *xdaq )
    : utils::InfoSpaceHandler( xdaq, xdaq->getApplicationInfoSpace(), "Appl_IS" )
    // All values below are configuration parameters and should be set before "Configure"
{
    createDocumentationSeparator("Parameters which control the behaviour of the Application. They are not directly related to the hardware.");
    // Temporary hack to make RCMS and scripts happy
    createstring( "stateName", "uninitialized", "", PROCESS, "A temporary (or not so temporary...) hack to make tools like RCMS happy. These want to query the stateName variable in the ApplicationInfospace. They have no means to easily access other Infospaces." );
    // The following parameters influence the behaviour of the entire application. 
    createbool( "noFirmwareVersionCheck", false, "", NOUPDATE, "This parameter is intended for debugging only. If set to true, the version checking in the FPGA will be switched off. Do not use this option in production or serious test setups!");
    createbool(   "lightStop", false, "", NOUPDATE, "If this parameter is set to false, a \"Stop\" command will execute a \"Halt\" and a \"Configure\". If set to true, a faster transition is executed: On the FRL the emulator is stopped and a pause of 0.1sec is introduced if the FRL_EMULATOR_MODE was active. Then a softwareReset is executed, the DAQ_mode is reset adn the FifoMonCounters and the event size histogramming are disabled. Finally the expected source FED IDs are set in the hardware. In the Ferol the event generators are stopped if the FerolEmulatorMode was active. Then some monitoring counters are reset. TCP/IP connections are not touched." );
    createuint32( "slotNumber", 0, "", NOUPDATE, "The slot numbers are counted from the right side of the crate and start with '1'. (Slot 0 is occupied by the crate controller module.)" ); 
    createuint32( "deltaTMonMs", 2000, "", NOUPDATE, "The time interval in ms between successive updates of the monitoring information." ); // 
    createstring( "OperationMode", FRL_MODE, "", NOUPDATE, "Defines the Mode of operation: <br>\"FRL_MODE\" for receiving data via the FRL (SLINK or event generator in the FRL), <br>\"FEROL_MODE\" for receiving data via the the Ferol (optical inputs or from the internal event generator in the FEROL), <br>\"FEDKIT_MODE\" for receiving data via the the Ferol in the FEDKIT<br>See also the documentation of the DataSource and the FrlTriggerMode parameters!" ); // 
    createstring( "DataSource", SLINK_SOURCE, "", NOUPDATE, "Defines the Source of the data. The possible values depend on the OperationMode: <br>In FRL_MODE: \"SLINK_SOURCE\" for receiving data via the electrical SLINKs, \"GENERATOR_SOURCE\" for receiving data from the internal event generator in the FRL, <br> FEROL_MODE: \"L6G_SOURCE\" for receiving data via the 6Gbps optical inputs, \"L6G_CORE_GENERATOR_SOURCE\" for receiving data via the 6Gbps optical inputs but this time data is generated in the data-generator included in the sender FPGA core (i.e. the FEROL will send the necessary commands to the sender core via the optical fiber, to set up the event generator in the core), \"10G_SOURCE\" for receiving data via the 10Gbps optical input, and \"10G_CORE_GENERATOR_SOURCE\" for receiveing data via the optical 10Gbps input but this time data is generated in the data-generator of the sender core and finally \"GENERATOR_SOURCE\" for receiving data from the internal event generator in the FEROL, <br> FEDKIT_MODE: \"L6G_SOURCE\" for receiving data via the 6Gbps optical inputs, \"L6G_CORE_GENERATOR_SOURCE\" for receiving data via the 6Gbps optical inputs but this time data is generated in the data-generator included in the sender FPGA core (i.e. the FEROL will send the necessary commands to the sender core via the optical fiber, to set up the event generator in the core), \"10G_SOURCE\" for receiving data via the 10Gbps optical input, and \"10G_CORE_GENERATOR\" for receiveing data via the optical 10Gbps input but this time data is generated in the data-generator of the sender core." );
    createuint32( "testCounter", 0, "", NOUPDATE, "This is a dummy parameter which is counting up to see that the AJAX monitoring is working. It is useless to set this parameter to some value." );


    createDocumentationSeparator("Parameters related to the data streams. The Ferol can be operated with up to 2 streams. At the input they either correspond to the SLINK ports, to the SLINKexpress ports or to the  event generators, In the output they are handled as different TCP/IP streams. They always go to the same IP destination address but to different ports.");
    createbool(   "enableStream0", false, "", NOUPDATE, "Enable Stream 0. This corresponds to the upper SLINK connector or the upper SFP input connector if reading the SLINKexpress links of the Ferol." );
    createbool(   "enableStream1", false, "", NOUPDATE, "Enable Stream 1. This corresponds to the upper SLINK connector or the lower SFP input connector if reading the SLINKexpress links of the Ferol. In FEDKIT_MODE there is always a optical slink sender core connected to this port. If we want to use it in a loopback configuration, we need to enable both ports, so that the serializers of both 6Gbps links are set up and powered up. " );
    createuint32( "Window_trg_stop", 0, "", NOUPDATE, "To throttle the throughput of the Ferol the streams can be configured to accept at maximum a maximal number of triggers in a given time window. This parameter defines the width of the time-window in units of 6.4ns." );
    createuint32( "nb_frag_before_BP_L0", 0, "", NOUPDATE, "The maximal number of fragments in the time window defined by 'Window_trg_stop' before backpressure is generated in Stream 0. A value of 0 means that the 'artificial' backpressure is never activated." );
    createuint32( "nb_frag_before_BP_L1", 0, "", NOUPDATE, "The maximal number of fragments in the time window defined by 'Window_trg_stop' before backpressure is generated in Stream 1. A value of 0 means that the 'artificial' backpressure is never activated." );

    createDocumentationSeparator( "Parameters which are relevant for optical or copper SLINK operation" );
    createuint32( "Maximal_fragment_size", 16382, "", NOUPDATE, "Incoming fragments greater than this (value+2), measured in 64bit words, are truncated. The traler of these fragments is marked with 0xAA. No CRC will be generated for these fragments. The default corresponds to 128kB");

    createDocumentationSeparator("Parameters to control the event generators. These parameters are used to configure the generators of the FEROL and those of the FRL. Attention: these generators have slightly different limitations. If the Stdev values for the event size of the delay between events is set different from 0, the corresponding values are generated according to a log-normal distribution.");
    createuint32( "Event_Length_bytes_FED0", 0x1000, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration." ); 
    createuint32( "Event_Length_bytes_FED1", 0x1000, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration. " );
    createuint32( "Event_Length_Stdev_bytes_FED0", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated" );
    createuint32( "Event_Length_Stdev_bytes_FED1", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated" );
    createuint32( "Event_Length_Max_bytes_FED0", 0x10000, "", NOUPDATE, "Only in use if Event_Length_Stdev_bytes_FED0 is larger than 0! The event generators truncate at the given maximal event size. No events larger than this size will be generated." );
    createuint32( "Event_Length_Max_bytes_FED1", 0x10000, "", NOUPDATE, "Only in use if Event_Length_Stdev_bytes_FED1 is larger than 0! The event generators truncate at the given maximal event size. No events larger than this size will be generated." );
    createuint32( "Event_Delay_ns_FED0", 20, "", NOUPDATE, "This delay is given in nanoseconds. The FRL hardware can only generate delays in steps of 10ns. 20 bits are available for the delay: therefore the maximal possible delay int the Frl is 10485750ns (10.48ms).  The Ferol hardware can generate delays in steps of 20ns. 16 bits are available for the delay: therefore the maximal possible delay in the Ferol is 1310700ns (1.31ms)" );
    createuint32( "Event_Delay_ns_FED1", 20, "", NOUPDATE, "This delay is given in nanoseconds. The FRL hardware can only generate delays in steps of 10ns. 20 bits are available for the delay: therefore the maximal possible delay int the Frl is 10485750ns (10.48ms).  The Ferol hardware can generate delays in steps of 20ns. 16 bits are available for the delay: therefore the maximal possible delay in the Ferol is 1310700ns (1.31ms)" );
    createuint32( "Event_Delay_Stdev_ns_FED0", 0, "", NOUPDATE, "If set to 0 a fixed delay between events is generated" );
    createuint32( "Event_Delay_Stdev_ns_FED1", 0, "", NOUPDATE, "If set to 0 a fixed delay between events is generated" );
    createuint32( "Seed_FED0", 12345, "", NOUPDATE, "Used by the random generator." );
    createuint32( "Seed_FED1", 23453, "", NOUPDATE, "Used by the random generator." );

    createuint32( "N_Descriptors_FED0", 1024, "", NOUPDATE, "This parameter is only used in the Ferol event generator. The maximum number of descriptors is 1024." );
    createuint32( "N_Descriptors_FED1", 1024, "", NOUPDATE, "This parameter is only used in the Ferol event generator. The maximum number of descriptors is 1024." );
    createuint32( "N_Descriptors_FRL", 2048, "", NOUPDATE, "This parameter is only used in the FRL event generator. Only powers of 2 are valid configuration parameters (the software rounds the value down to the nearest next power of two). The maximal value is 0x8000. " );
    createuint32( "gen_Thre_busy", 500, "", NOUPDATE, "Relevant for the FRLEmulatorMode. It sets the threshold to activate the FMM busy line on the backplane to a specific number of pending triggers. " );
    createuint32( "gen_Thre_ready", 450, "", NOUPDATE, "Relevant for the FRLEmulatorMode. It sets the threshold to activate the sTTS ready on the backplane to a specific number of pending triggers. With the parameter gen_Thre_busy it is possible to create a sTTS hysteresis for the toggling of the sTTS signals BUSY - READY. Keep in mind that the sTTS signals of the FRLs in one crate are ORed on the backplane by the hardware." );

    createDocumentationSeparator("The following parameters are specific to the FRL module.");
    createbool(   "enableDeskew", false, "", NOUPDATE, "Perform a deskew calibration of the SLINK cable(s)." );
    createbool(   "enableDCBalance", false, "", NOUPDATE, "Use a DC-balance encoding on the SLINK." );
    createbool(   "enableSpy", false, "", NOUPDATE, " ...not sure if still used..." );
    createbool(   "enableWCHistogram", true, "", NOUPDATE, "If set to true, wordcount histograms are generated in the FRL." );
    createuint32( "testDurationMs", 500, "", NOUPDATE, "Defines the length of the SLINK test in ms." );
    createuint32( "WCHistogramResolution", 4, "", NOUPDATE, "" );
    createuint32( "expectedFedId_0", -1, "", NOUPDATE, "The FEDId expected to be be in the header of the incoming fragments. The FRL checks the incoming fragments against this value. If a discrepancy is found the Bit 14 of the SLINK trailer is set. The event generators use this number to define the FED-srcId field in the SLINK headers." );
    createuint32( "expectedFedId_1", -1, "", NOUPDATE, "The FEDId expected to be be in the header of the incoming fragments. The FRL checks the incoming fragments against this value. If a discrepancy is found the Bit 14 of the SLINK trailer is set. The event generators use this number to define the FED-srcId field in the SLINK headers." );
    createbool( "gotoFailedOnWrongFedId", true, "", NOUPDATE, "If set to true (default) the application goes into the failed state whenever a fragement is encountered at the input, of which the fedId does not match the expected fedId.");
    createstring( "FrlTriggerMode", "FRL_AUTO_TRIGGER_MODE", "", NOUPDATE, "If events are generated in the FRL there are different ways to trigger these events. \"FRL_AUTO_TRIGGER_MODE\" generates events in an endless loop. \"FRL_GTPE_TRIGGER_MODE\" generates an event every time a trigger signal from the backplane is received. Usually these signals are provided by the GTPe -> FanOut -> TriggerDistributor chain. \"FRL_SOAP_TRIGGER_MODE\" trigger an event every time the \"softTrigger\" SOAP command is received." );
    createstring( "FerolTriggerMode", "FEROL_AUTO_TRIGGER_MODE", "", NOUPDATE, "If events are generated in the FEROL there are different ways to trigger these events. \"FEROL_AUTO_TRIGGER_MODE\" generates events in an endless loop. \"FEROL_GTPE_TRIGGER_MODE\" generates an event every time a trigger signal from the backplane is received. Usually these signals are provided by the GTPe. ");

    createDocumentationSeparator("Ferol Configuration parameters related to the basic setup of the DAQ link.");
    createuint32( "MAC_HI_FEROL", 0x080030, "", NOUPDATE, "The MAC addresses of the Ferols are programmed into a on-board PROM. They all start with the bytes 08:00:30. Therefore this parameter should not be changed. The software checks that the MAC address of the PROM really starts with the byte combination given in this parameter. A mismatch is an indication that the PROM was not correctly programmed and the application goes to \"Failed\". The module needs to be exchanged." );
    createuint32( "MAX_ARP_Tries", 20, "", NOUPDATE, "The maximum number of ARP retries during the TCP/IP connection setup. Do not decrease this number since some switches need a substantial number of re-tries or time before they reply correctly." );
    createuint32( "ARP_Timeout_Ms", 500, "", NOUPDATE, "The number of ms to wait for an ARP reply before the next retry.");
    createuint32( "Connection_Timeout_Ms", 5000, "", NOUPDATE, "The time to wait for a reply to the \"syn\" packets the FEROL sends during TCP/IP connection setup. SYN packets are regularly sent during this time." ); 
    createstring( "IP_NETMASK", "0.0.0.0", "", NOUPDATE, "If the Ferol should send packets to another network via a router/geteway, the netmask and the gateway need to be set beforehand. If the netmask is left at 0.0.0.0 the Ferol will work, but no routing of the packets is possible.");
    createstring( "IP_GATEWAY", "0.0.0.0", "", NOUPDATE, "If the Ferol should send packets to another network via a router/geteway, the netmask and the gateway need to be set beforehand.");
    createstring( "SourceIP", "0.0.0.0", "", NOUPDATE, "The IP address of the DAQ link in the Ferol. This can be either in dotted notation or a qulified hostname which will be looked up via the DNS." );
    createuint32( "TCP_SOURCE_PORT_FED0", 10, "", NOUPDATE, "The source port for the Stream 0 of the Ferol." );
    createuint32( "TCP_SOURCE_PORT_FED1", 11, "", NOUPDATE, "The source port for the Stream 1 of the Ferol." );
    createstring( "DestinationIP", "0.0.0.0", "", NOUPDATE, "The destination IP address of the DAQ link. This can be either in dotted notation or a qulified hostname which will be looked up via the DNS." );// 
    createuint32( "TCP_DESTINATION_PORT_FED0", 2000, "", NOUPDATE, "The destination port for the Stream 0 of the Ferol." );
    createuint32( "TCP_DESTINATION_PORT_FED1", 2001, "", NOUPDATE, "The destination port for the Stream 1 of the Ferol." );
    createbool( "ENA_PAUSE_FRAME", true, "", NOUPDATE, "Specifies if the FEROL should consider TCP/IP pause-frames received via the DAQ link or wether to ignore them. " ); // 
    createuint32( "Vitesse_Timeout_Ms", 30000, "", NOUPDATE, "This timeout defines how long to wait for the low level 10 Gbps link interface to come up.");

    createbool( "TCP_SOCKET_BUFFER_DDR", false, "", NOUPDATE, "If set to true the 512MB DDR RAM on the Ferol will be used for the socket buffers of the TCP/IP streams. If set to false the 16MB of the QDR RAM will be used. (See also the next two parameters.)");

    //    createuint32( "DDR_memory_mask", 0x10000000, "hex", NOUPDATE, "If the DDR RAM is used as a socket buffer this value defines how much RAM is reserved for each stream. The maximum is 256 MB/stream since the the memory chip contains 512MB. If this value is not a power of 2 it will be rounded down the the closest power of 2." );
    //    createuint32( "QDR_memory_mask", 0x800000, "hex", NOUPDATE, "If the QDR RAM is used as a socket buffer, this value defines how much RAM is reserved for each stream. The maximal value since the memory chip contains 16MB." );

    createDocumentationSeparator("Parameters to tune the 10 Gbps TCP/IP connection to the DAQ");
    createuint32( "TCP_CONFIGURATION_FED0", 0x00004000, "hex", NOUPDATE, "Bits 15..0: sets the size of the TCP window for this stream.<br> Bit 16: The TCP push flag will be set in each outgoing packet.<br> Bit 18: no_delay will be used in the TCP statemachine.<br> Bit 19: The fast re-transmit will be disabled. <br> Bit 20: TIMER_STOP is disabled. " );
    createuint32( "TCP_CONFIGURATION_FED1", 0x00004000, "hex", NOUPDATE, "" );
    createuint32( "TCP_OPTIONS_MSS_SCALE_FED0", 0x00012300, "hex", NOUPDATE, "" );
    createuint32( "TCP_OPTIONS_MSS_SCALE_FED1", 0x00012300, "hex", NOUPDATE, "" );
    createuint32( "TCP_CWND_FED0", 800000, "", NOUPDATE, "" );
    createuint32( "TCP_CWND_FED1", 800000, "", NOUPDATE, "" );
    createuint32( "TCP_TIMER_RTT_FED0", 312500, "", NOUPDATE, "" );
    createuint32( "TCP_TIMER_RTT_FED1", 312500, "", NOUPDATE, "" );
    createuint32( "TCP_TIMER_RTT_SYN_FED0", 312500000, "", NOUPDATE, "" );
    createuint32( "TCP_TIMER_RTT_SYN_FED1", 312500000, "", NOUPDATE, "" );
    createuint32( "TCP_TIMER_PERSIST_FED0", 625000, "", NOUPDATE, "" );
    createuint32( "TCP_TIMER_PERSIST_FED1", 625000, "", NOUPDATE, "" );
    createuint32( "TCP_REXMTTHRESH_FED0", 3, "", NOUPDATE, "" );
    createuint32( "TCP_REXMTTHRESH_FED1", 3, "", NOUPDATE, "" );
    createuint32( "TCP_REXMTCWND_SHIFT_FED0", 6, "", NOUPDATE, "" );
    createuint32( "TCP_REXMTCWND_SHIFT_FED1", 6, "", NOUPDATE, "" ); 
              
    createDocumentationSeparator("The following registers exist in the hardware but currently do not have any function.");
    createuint32( "TCP_OPTION_TIMESTAMP_FED0", 0, "", NOUPDATE, "" );
    createuint32( "TCP_OPTION_TIMESTAMP_FED1", 0, "", NOUPDATE, "" );
    createuint32( "TCP_OPTION_TIMESTAMP_REPLY_FED0", 0, "", NOUPDATE, "" );
    createuint32( "TCP_OPTION_TIMESTAMP_REPLY_FED1", 0, "", NOUPDATE, "" );

    //std::cout << getDocumentation() << std::endl;
    
    // Bind setting of default parameters
    xdaq->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}


// callback for Parameter Setting
void ferol::ApplicationInfoSpaceHandler::actionPerformed( xdata::Event& e )
{
    DEBUG( "actionPerformed called with type " << e.type() );
    if ( e.type() == "urn:xdaq-event:setDefaultValues" )
        {

            DEBUG("actionPerformed for setDefaultValues" );            
            // read all values from the infospace so that they are displayed in
            // the local monitoring.
            readInfoSpace();
            // this pushes the changes into the monitoring system by firing the monitoring events:
            pushInfospace();
        }
}

// special method to set the parameters needed by the RCMSStateListener. This is a structure and a bool to monitor.
void ferol::ApplicationInfoSpaceHandler::setRCMSStateListenerParameters(  xdata::Bag<xdaq2rc::ClassnameAndInstance> *RcmsStateListenerParameters,
                                                                          xdata::Boolean *FoundRcmsStateListener )
{
    is_P->lock();
    is_P->fireItemAvailable("rcmsStateListener", RcmsStateListenerParameters );
    is_P->fireItemAvailable("foundRcmsStateListener", FoundRcmsStateListener );
    is_P->unlock();
    cpis_P->lock();
    cpis_P->fireItemAvailable("rcmsStateListener", RcmsStateListenerParameters );
    cpis_P->fireItemAvailable("foundRcmsStateListener", FoundRcmsStateListener );
    cpis_P->unlock();
}
