#include <sstream> 
#include "ferol/FerolEventGenerator.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/loggerMacros.h"

ferol::FerolEventGenerator::FerolEventGenerator( HAL::HardwareDeviceInterface *ferol,
                                                 Logger logger )
    : utils::EventGenerator( logger ),
      ferol_P( ferol )
{
}

void
ferol::FerolEventGenerator::checkParams( uint32_t &nevt, uint32_t streamNo )
{
    if ( (streamNo != 0) && (streamNo != 1) )
        {
            std::stringstream msg;
            msg << "Illegal stream number " << streamNo
                << " encountered in the FerolEventGenerator. Cannot generate event descriptors. " ;
            ERROR( msg.str() );
            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
        }

    if ( nevt > MAX_EVENT_DESCRIPTORS )
        {
            std::stringstream msg;
            msg << "You requested " << nevt << " events to be generated but the Ferol core has only space for " << MAX_EVENT_DESCRIPTORS << " descriptors. Therefore the generation of descriptors will be limited to this maximum. "; 
            WARN( msg.str() );
            nevt = MAX_EVENT_DESCRIPTORS;
        }

    return;
}

void
ferol::FerolEventGenerator::writeDescriptors( uint32_t streamNo )
{
    std::vector<descriptor>::const_iterator it;
    uint32_t fedId = (*(descriptors_.begin())).fedId;
    uint32_t bx = 1234;

    ferol_P->setBit("FEROL_EMULATOR_MODE");
    std::stringstream trgCrtlItem, fedsrcBxItem, descrMemItem, evtNumberItem;
    trgCrtlItem  << "GEN_TRIGGER_CONTROL_FED" << streamNo;
    fedsrcBxItem << "GEN_FED_SOURCE_BX_FED" << streamNo;
    descrMemItem << "GEN_RND_MEMORY_FED" << streamNo;
    evtNumberItem << "GEN_EVENT_NUMBER_FED" << streamNo;
    ferol_P->write( trgCrtlItem.str(), 0x24); // resets the random memory and stops the trigger

    uint32_t bxid = (bx << 16) + fedId;
    ferol_P->write(fedsrcBxItem.str(), bxid);

    for ( it = descriptors_.begin(); it != descriptors_.end(); it++ )
        {
            uint32_t size  =  (*it).size >> 3;       // in 64 bit words
            uint32_t delay =  (*it).delay / 20;      // in units of 20ns (50MHz clock!)
            if ( delay >= 0x10000 )
                {
                    WARN("The requested delay in the descriptor is too large: Maximal delay for the Ferol Eventgenerator is 1310700ns (1.31 ms). Setting the delay to this value.");
                    delay = 0xffff;
                }
            uint32_t desc = (delay << 16) + size;
            ferol_P->write( descrMemItem.str(), desc );
            //std::cout << std::hex << "desc " << desc << std::endl;
        }

    ferol_P->write( evtNumberItem.str(), 0x1 );
}
