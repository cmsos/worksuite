#include "ferol/TCPStreamInfoSpaceHandler.hh"

ferol::TCPStreamInfoSpaceHandler::TCPStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater, utils::InfoSpaceHandler *appIS  )
    : StreamInfoSpaceHandler( xdaq, "TCPStream", updater,  appIS )
{
    // name hwitem  update format doc

    createuint32( "BIFI_USED_FED", 0, "", HW32, "The number of 64bit words currently used in the TCP/IP stream buffer (called BIFI). This buffer is 524287 words deep.");
    createuint32( "BIFI_USED_MAX_FED", 0, "", HW32, "The maximum number of 64bit words used in the TCP/IP stream buffer (called BIFI), since the TCP/IP connection has been established. This buffer is 524287 words deep.");
    createuint32( "TCP_CONNECTION_ESTABLISHED_FED", 0, "", HW32, "A '1' indicates that a TCP/IP connection is established for this stream." );
    createuint32( "TCP_STAT_CONNATTEMPT_FED", 0, "", HW32, "Counts the number of connection attempts since the last reset." );
    createuint32( "TCP_STAT_CONNREFUSED_FED", 0, "", HW32, "Counts the number of \"connection refused\" packets received." );
    createuint32( "TCP_STAT_CONNRST_FED", 0, "", HW32, "The status of the TCP connection: Bit 0: Connection reset by Peer; Bit 1 : Connection closed by Peer; Bit 2 : Connection refused; Bit 3 : Connection terminated because of URG flag received; Bit 4 : Connection terminated because a higher sequence number than expected received (somebody tries to send data to the Ferol? This is not supported!); Bit 5 : Connection terminated because a SYN packet was received on an established connection." );
    createuint32( "TCP_STAT_SNDPROBE_FED", 0, "", HW32, "Counts the number of probe packets sent, while the TCP engine is in the PERSIST state." );
    createuint32( "TCP_STAT_SNDPACK_FED", 0, "", HW32, "The number of TCP packets sent out of the 10Gb TCP/IP link but excluding the retransmitted packets." );
    createuint32( "TCP_STAT_ACK_DELAY_MAX_FED", 0, "", HW32, "Max number of clocks (156.25MHz) between two received ACK");
    createuint32( "TCP_STAT_RCVDUPACK_FED", 0, "", HW32, "The number of duplicate acknowledge packets received by this stream.");
    createuint32( "TCP_STAT_FASTRETRANSMIT_FED", 0, "" );
    createuint32( "TCP_STAT_RCVACKTOOMUCH_FED", 0, "" );
    createuint32( "TCP_STAT_PERSIST_EXITED_FED", 0, "", HW32, "Counts the number of times this stream leaves the PERSIST state. If this counter is smaller by one than the corresponding TCP_STAT_PERSIST counter, this stream is currently in the PERSIST state." );
    createuint32( "TCP_STAT_PERSIST_FED", 0, "", HW32, "Counts the number of times this stream enters the PERSIST state. The Persist state is entered when the receiver cannot receive data anymore. The sender is sending probe packets to find out if it can send data again (see counter TCP_STAT_SNDPROBE). The replies to this probe packet are counted in TCP_STAT_RCVDUPACK." );
    createuint32( "TCP_STAT_PERSIST_CLOSEDWND_FED", 0, "" );
    createuint32( "TCP_STAT_PERSIST_ZEROWND_FED", 0, "" );
    createuint32( "TCP_STAT_DUPACK_MAX_FED", 0, "" );
    createuint64( "TCP_STAT_SNDBYTE_FED", 0, "", HW64, "The number of bytes sent out of the 10Gb TCP/IP link but excluding the retransmitted data." );
    createuint32( "TCP_STAT_SNDREXMITPACK_FED", 0, "", HW32, "The number of retransmitted packets since the TCP/IP connection has been established.");
    createuint64( "TCP_STAT_SNDREXMITBYTE_FED", 0, "", HW64, "Number of retransmitted bytes since the TCP/IP connection has been established." );
    createuint32( "TCP_STAT_CURRENT_WND_FED", 0, "", HW32, "Snapshot of the TCP window size received by the FEROL");
    createuint32( "TCP_STAT_WND_MAX_FED", 0, "", HW32, "The minimal TCP window size received by the FEROL since the TCP connection was created." );
    createuint32( "TCP_STAT_WND_MIN_FED", 0, "", HW32, "The minimal TCP window size received by the FEROL since the TCP connection was created." );
 
    createuint32( "TCP_STAT_SEG_DROP_AFTER_ACK_FED", 0, "", HW32, "Number of segments dropped AFTER acknkowldge" );
    createuint32( "TCP_STAT_SEG_DROP_FED", 0, "", HW32, "Number of segments dropped" );
    createuint32( "TCP_STAT_SEG_DROP_WITH_RST_FED", 0, "", HW32, "Number of segments dropped with RESET" );

    createuint32( "TCP_STAT_RTT_COUNTER_FED", 0, "",   HW32,    "Counts the number of RTT timeouts in this stream." );
    createdouble( "RTT_FED",                  0, "us", PROCESS, "The TCP round-trip-time for a packet measured in this moment [us]." );
    createuint32( "TCP_STAT_MEASURE_RTT_FED", 0, "",   HW32,    "The TCP round-trip-time for a packet measured in this moment [clock cycles: 156.25MHz]" );
    createuint32( "TCP_STAT_MEASURE_RTT_MAX_FED", 0, "", HW32, "Maximal TCP round-trip-time measured for this stream [clock cycles: 156.25MHz]." );
    createuint32( "TCP_STAT_MEASURE_RTT_MIN_FED", 0, "" , HW32, "Minimal TCP round-trip-time measured for this stream [clock cycles: 156.25MHz]." );
    createdouble( "RTT_MAX_FED", 0, "us", PROCESS, "Maximal TCP round-trip-time measured for this stream [us]." );
    createdouble( "RTT_MIN_FED", 0, "us", PROCESS , "Minimal TCP round-trip-time measured for this stream [us]." );
    createdouble( "AVG_RTT_FED", 0, "us", PROCESS, "The average TCP round-trip-time for packets since the TCP connection has been established." );
    createuint64( "TCP_STAT_MEASURE_RTT_SUM_FED", 0, "" , HW64, "Sum of all TCP round-trip-times measured for this stream [clock cycles: 156.25MHz]. This value is used to calculate the averate RTT." );
    createuint32( "TCP_STAT_MEASURE_RTT_COUNT_FED", 0, "", HW32, "Number of RTT measurements for this stream. (Used to calculate the average RTT)" );
    createuint32( "TCP_STAT_RTT_SHIFTS_MAX_FED", 0, "", HW32, "Maximal value for the RTT shifts in this stream." );

    createuint32( "TCP_STAT_SND_NXT_UNALIGNED_FED", 0, "", HW32, "Number of sent packets where SND_NXT was found not being aligned to 64-bit (is not divisible by 8) and alignment was forced. This register should stay at zero. If non-zero, then somewhere is a big problem. E.g. data corruption inside FEROL.");
    createuint64( "BACK_PRESSURE_FED", 0 );
    createuint64( "BACK_PRESSURE_BIFI_FED", 0 );
    createuint64( "BACK_PRESSURE_PERSIST_FED", 0 );
    createuint64( "BACK_PRESSURE_ENTRIES_FED", 0, "", HW64, "Counts the number of times the BIFI backpressure threshold is passed.");
    //createuint64( "PACKETS_RECEIVED", 0, "", HW64, "Total number of packets received from the 10Gb DAQ-link. Here all packets are counted (ARP, PING, DHCP, TCP/IP related packets, ...)" );
    //createuint64( "PACKETS_RECEIVED_BAD", 0, "", HW64, "Total number of errorneous packets received. These are packets with a CRC error." );
    //createuint64( "PACKETS_SENT", 0, "", HW64, "Total number of packets which have been sent out of the 10Gb DAQ-link. Here all packets are counted (ARP, PING, DHCP, sent and retransmitted TCP/IP packets, ...)");

    // calculated values
    createdouble( "PACK_REXMIT_FED", 0, "percent", PROCESS );
    createdouble( "BIFI_FED", 0, "percent", PROCESS, "The BIFI is the buffer for data queuing to leave the Ferol via the 10Gb TCP/IP link to the DAQ. It's size is 1 Megaword in firmware versions larger nn87. (Before it was half.) This value is a snapshot of its filling status in percent.");
    createdouble( "BIFI_MAX_FED", 0, "percent", PROCESS, "This value shows the maximal fill stand of the BIFI buffer since the TCP connection was established." );
    createdouble( "ACK_DELAY_MAX_FED", 0, "us", PROCESS );
    createdouble( "ACK_DELAY_FED", 0, "us", PROCESS );
    createdouble( "AVG_ACK_DELAY_FED", 0, "us", PROCESS );
    createdouble( "LatchedTimeBackendSeconds", 0, "", PROCESS, "A generic timestamp which is latched at the same time as some monitoring counters. This timestamp allows to calculate fractions of time or rates.");
    createdouble( "AccBIFIBackpressureSeconds", 0, "", PROCESS, "The integrated time, the BIFI fifo was almost full [seconds]." );

    // calculated rates
    createdouble( "TcpThroughput_FED", 0, "Gbitbw", TRACKER, "The current effective data rate to the DAQ link (excluding retransmitted data)." );
    createdouble( "TcpRetransmit_FED", 0, "Gbitbw", TRACKER, "Rate of retransmitted data in Gbits/sec." );
}


void
ferol::TCPStreamInfoSpaceHandler::registerTrackerItems( utils::DataTracker &tracker )
{
    tracker.registerRateTracker( "TcpThroughput_FED0", "TCP_STAT_SNDBYTE_FED0_LO", utils::DataTracker::HW64, 8.0);
    tracker.registerRateTracker( "TcpThroughput_FED1", "TCP_STAT_SNDBYTE_FED1_LO", utils::DataTracker::HW64, 8.0);
    tracker.registerRateTracker( "TcpRetransmit_FED0", "TCP_STAT_SNDREXMITBYTE_FED0_LO", utils::DataTracker::HW64, 8.0);
    tracker.registerRateTracker( "TcpRetransmit_FED1", "TCP_STAT_SNDREXMITBYTE_FED1_LO", utils::DataTracker::HW64, 8.0);
}

