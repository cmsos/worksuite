#ifndef __FrlEventGenerator
#define __FrlEventGenerator

#include "d2s/utils/EventGenerator.hh"

/************************************************************************
 *
 *
 *     @short Event Generator for the FRL
 *            
 *            This Event  Generator creates descriptors  for events and 
 *            downloadd them to the descriptor memory in the FRL.
 *
 *       @see 
 *    @author $Author$
 *   @version $Revision$
 *      @date $Date$
 *
 *      Modif 
 *
 **//////////////////////////////////////////////////////////////////////
namespace ferol
{
    class FrlEventGenerator : public utils::EventGenerator {
    public: 
        FrlEventGenerator( HAL::HardwareDeviceInterface *bridge,
                           HAL::HardwareDeviceInterface *frl,
                           Logger logger );
    protected:
        virtual void checkParams( uint32_t &nevt, uint32_t streamNo );
        virtual void writeDescriptors( uint32_t streamNo );
    private:
        HAL::HardwareDeviceInterface *bridge_P;
        HAL::HardwareDeviceInterface *frl_P;
    };
    
};

#endif /* __FrlEventGenerator */
