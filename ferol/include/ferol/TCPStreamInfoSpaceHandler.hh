#ifndef __TCPStreamInfoSpaceHandler
#define __TCPStreamInfoSpaceHandler

#include "ferol/StreamInfoSpaceHandler.hh"
#include "d2s/utils/DataTracker.hh"

namespace ferol
{
    class TCPStreamInfoSpaceHandler:public StreamInfoSpaceHandler {
    public:
        TCPStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater, utils::InfoSpaceHandler *appIS );

        void registerTrackerItems( utils::DataTracker &tracker );
  
    private:
    };
}

#endif /* __TCPStreamInfoSpaceHandler */
