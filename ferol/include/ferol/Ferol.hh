#ifndef __Ferol
#define __Ferol

#include "ferol/FrlFirmwareChecker.hh"
#include "ferol/ApplicationInfoSpaceHandler.hh"
#include "ferol/StatusInfoSpaceHandler.hh"
#include "ferol/FerolInfoSpaceHandler.hh"
#include "ferol/TCPStreamInfoSpaceHandler.hh"
#include "ferol/FerolMonitor.hh"
#include "d2s/utils/Exception.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"
#include "ferol/HardwareLocker.hh"
#include "ferol/MdioInterface.hh"
#include "xdaq/Application.h"
#include "hal/PCIDevice.hh"
#include "hal/PCILinuxBusAdapter.hh"
#include "tts/ipcutils/SemaphoreArray.hh"
#include "log4cplus/logger.h"
#include "toolbox/BSem.h"
#include "d2s/utils/DataTracker.hh"
#include "ferol/SlinkExpressCore.hh"
#include "ferol/ConfigSpaceSaver.hh"

namespace ferol
{
    class Ferol : public utils::InfoSpaceUpdater, public ConfigSpaceSaver
    {
    public:

        Ferol( xdaq::Application *xdaq,
               ApplicationInfoSpaceHandler &appIS,
               StatusInfoSpaceHandler &statusIS,
               FerolMonitor &monitor,
               HardwareLocker &hwLocker );
        ~Ferol();

      void instantiateHardware();
      void configure();
      void enable();
      void stop();
      void halt();
      void suspendEvents();
      void resumeEvents();


      bool updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo = 0 );

      void controlSerdes( bool on );

        // used by the firmwarechecker to change FRL firmware type on the fly
        void hwlock();
        void hwunlock();


        // read the SFP interface
      void readSFP( uint32_t vitesseNo );
        // Check if the i2C Bus operation terminated correctly or if the bus is idle
      std::string checkI2CState( uint32_t vitesseNo );
        
        // Start an I2C command. The other command registers must have been set beforehand (8004, 8002 for block read)
      void i2cCmd( uint32_t vitesseNo, uint32_t value );
        
        // Reset the Vitesse chip. This reset line is pulled to the value in the argument. Reset active means '0'. 
      void resetVitesse( uint32_t reset = 0 );

        // toggle the DAQ on DAQ off of the optical slink
        int32_t daqOff( uint32_t link, bool generatorOn = false );
        int32_t daqOn( uint32_t link );
        int32_t resyncSlinkExpress( uint32_t link );

        // The hardware debugger needs access to the slink express core to read out the registres of the SLINK express.
        SlinkExpressCore *getSlinkExpressCore();

        HAL::HardwareDeviceInterface * getFerolDevice();

        uint32_t getSlot();

    private:
      void createFerolDevice();
      void shutdownHwDevice();
        void openConnection( uint32_t stream );

      std::string makeIPString( uint32_t ip );

        double getTemp( u_char* data );
        double getVoltage( u_char* data );
        double getCurrent( u_char* data );
        double getPower( u_char* data );
        bool catchTcpError( uint32_t stream, std::stringstream &msg, uint32_t &tcpStatus );

        
        /**
         *
         *     @short Do an ARP-probe.
         *            
         *            This  function checks that  the MAC  addresse and  the IP 
         *            addresse of  this Ferol are  unique on the  network. This 
         *            is done by sending an  ARP request for IP address of this
         *            Ferol  on  the network.  (The  firmware  tries this  four 
         *            times). An answer to  this request indicates a problem on
         *            the  network, since  some other  device has  the  same IP 
         *            addresse.  In this  case the  Ferol goes  into  the Error 
         *            state.
         *
         *     @param doProbe: if false no ARP package is sent, but the registers
         *            indicating a conflict on the network are analysed. Note 
         *            that the statemachine analysing replies to ARP requests 
         *            is running continuously in the FEROL and not only at the
         *            time, an arp request is done! Setting this parameter to 
         *            true allows this function to be used for updating infospaces
         *            in the monitoring thread.
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void doArpProbe( bool doProbe = true );

        // makes an 4 byte value which can be written into the register which holds the
        // destination IP addresse. If the hoststr is a ip4 dot-notation of an ip address
        // it is converted directly. In all other cases, the hoststr is interpreted as a 
        // fully qualified hostname with network extension, and gethostbyname is used to
        // retrieve the ip address. 


        /**
         *
         *     @short Make a 4 byte IP number from a string.
         *            
         *            Makes  a 4  byte value  which  can be  written into  the
         *            register which holds the destination IP addresse. If the
         *            hoststr is in ip4 dot-notation it is converted directly.
         *            In  all other  cases, the  hoststr is  interpreted  as a
         *            fully  qualified hostname  with  network extension,  and
         *            gethostbyname is used retrieve the ip.
         *             
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
      uint32_t makeIPNum( std::string hoststr );

        std::string uint64ToMac( uint64_t mac );

    private:
        Logger logger_;
        ApplicationInfoSpaceHandler &appIS_;
        StatusInfoSpaceHandler &statusIS_;
        FerolMonitor &monitor_;
        HardwareLocker &hwLocker_;
        FerolInfoSpaceHandler ferolIS_;
        TCPStreamInfoSpaceHandler tcpIS_;
        HAL::PCILinuxBusAdapter busAdapter_;
        toolbox::BSem deviceLock_;
        utils::DataTracker dataTracker_;
        utils::DataTracker dataTracker2_;
        SlinkExpressCore *slCore_P;
        MdioInterface *mdioInterface_P;
        bool hwCreated_;
        uint32_t bifiSize_;

        HAL::PCIDevice *ferolDevice_P;
        HAL::PCIAddressTable *ferolTable_P;
        HAL::PCIAddressTable *slinkExpressTable_P;

        uint32_t slot_;
        bool stream0_, stream1_;
        std::string operationMode_;
        std::string dataSource_;

        FrlFirmwareChecker *fwChecker_P;

        uint32_t ferolConfigSpace_[16];
        uint32_t streamBpCounterHigh[2];
        uint32_t streamBpCounterOld[2];
    };
}

#endif /* __Ferol */

            
