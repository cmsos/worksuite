#ifndef __FrlFirmwareChecker
#define __FrlFirmwareChecker

#include "hal/PCIDevice.hh"
#include "ferol/ferolConstants.h"
#include "ferol/ConfigSpaceSaver.hh"

namespace ferol {

    class FrlFirmwareChecker {

    public:

        /**
         * Only in the constructor the access to the hardware is done.
         * This makes the life cycle of this object less critical since
         * the pointers to the PCIDevices may go out of scope before 
         * this object disapears.
         */
        FrlFirmwareChecker( HAL::PCIDevice * frlDevice,
                            HAL::PCIDevice * bridgeDevice,
                            ConfigSpaceSaver *cfgFrl,
                            ConfigSpaceSaver *cfgFerol,
                            bool dontFail = false );

        FrlFirmwareChecker( HAL::PCIDevice * ferolDevice, bool dontFail = false );

        bool checkFirmware( std::string mode, std::string dataSource, std::string &errorstr, bool *changedFw );
        uint32_t getFerolFirmwareVersion() const;
        uint32_t getFerolFirmwareType() const;
        uint32_t getFerolHardwareRevision() const;
        uint32_t getFrlFirmwareVersion() const;
        uint32_t getFrlFirmwareType() const;
        uint32_t getFrlHardwareRevision() const;
        uint32_t getBridgeFirmwareVersion() const;
        uint32_t getBridgeFirmwareType() const;
        uint32_t getBridgeHardwareRevision() const;


    private:
        bool checkFrlFirmware( std::string mode, std::string dataSource, std::string &errorstr, bool *changedFw );
        bool checkFerolFirmware( std::string mode, std::string dataSource, std::string &errorstr );
        //        void dumpConfigSpace( uint32_t * configSp_p ) const;
        bool changeFrlFirmware( uint32_t requiredFirmwareType, std::stringstream &err );

        enum FPGAType { FRL, FEROL };
        FPGAType fpgaType_;

        uint32_t frlFwVersion_;
        uint32_t frlFwType_;
        uint32_t frlHwRevision_;
        uint32_t bridgeFwVersion_;
        uint32_t bridgeFwType_;
        uint32_t bridgeHwRevision_;
        uint32_t ferolFwVersion_;
        uint32_t ferolFwType_;
        uint32_t ferolHwRevision_;
        uint32_t bridgeConfigSpace_[16];
        uint32_t frlConfigSpace_[16];
        uint32_t ferolConfigSpace_[16];

        bool dontFail_;
        
        ConfigSpaceSaver *cfgFrl_P;
        ConfigSpaceSaver *cfgFerol_P;

        HAL::PCIDevice *bridgeDevice_P;
        HAL::PCIDevice *frlDevice_P;
    };
}

#endif /* __FrlFirmwareChecker */
