#ifndef __FerolMonitor
#define __FerolMonitor

#include "xdaq/Application.h"
#include "d2s/utils/Monitor.hh"
#include "ferol/ApplicationInfoSpaceHandler.hh"
#include "ferol/FrlInfoSpaceHandler.hh"
#include "ferol/FerolInfoSpaceHandler.hh"
#include "ferol/StatusInfoSpaceHandler.hh"
#include "ferol/InputStreamInfoSpaceHandler.hh"
#include "ferol/TCPStreamInfoSpaceHandler.hh"


/************************************************************************
 *
 *
 *     @short 
 *
 *       @see 
 *    @author $Author$
 *   @version $Revision$
 *      @date $Date$
 *
 *      Modif 
 *
 **//////////////////////////////////////////////////////////////////////

namespace ferol {
    class FerolMonitor : public utils::Monitor 
    {
    public:
        FerolMonitor( Logger &logger, ApplicationInfoSpaceHandler &appIS, xdaq::Application *xdaq, utils::ApplicationStateMachineIF &fsm );

        void addInfoSpace( ferol::FrlInfoSpaceHandler *is );
        void addInfoSpace( ferol::FerolInfoSpaceHandler *is );
        void addInfoSpace( ferol::StatusInfoSpaceHandler *is );
        void addInfoSpace( ferol::InputStreamInfoSpaceHandler *is );
        void addInfoSpace( ferol::TCPStreamInfoSpaceHandler *is );

    private:
        void addApplInfoSpaceItemSets( ferol::ApplicationInfoSpaceHandler *is );
    };
}

#endif /* __FerolMonitor */
