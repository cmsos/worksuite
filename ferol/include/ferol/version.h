#ifndef _ferol_version_h_
#define _ferol_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!
#define WORKSUITE_FEROL_VERSION_MAJOR 2
#define WORKSUITE_FEROL_VERSION_MINOR 3
#define WORKSUITE_FEROL_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_FEROL_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_FEROL_PREVIOUS_VERSIONS
#define WORKSUITE_FEROL_PREVIOUS_VERSIONS "1.12.8"
//
// Template macros
//
#define WORKSUITE_FEROL_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_FEROL_VERSION_MAJOR,WORKSUITE_FEROL_VERSION_MINOR,WORKSUITE_FEROL_VERSION_PATCH)

#ifndef WORKSUITE_FEROL_PREVIOUS_VERSIONS
#define WORKSUITE_FEROL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_FEROL_VERSION_MAJOR,WORKSUITE_FEROL_VERSION_MINOR,WORKSUITE_FEROL_VERSION_PATCH)
#else 
#define WORKSUITE_FEROL_FULL_VERSION_LIST  WORKSUITE_FEROL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_FEROL_VERSION_MAJOR,WORKSUITE_FEROL_VERSION_MINOR,WORKSUITE_FEROL_VERSION_PATCH)
#endif 

namespace ferol
{
	const std::string project = "worksuite";
	const std::string package  =  "ferol";
	const std::string versions = WORKSUITE_FEROL_FULL_VERSION_LIST;
	const std::string description = "Contains the ferol library.";
	const std::string authors = "Christoph Schwick";
	const std::string summary = "CMS Ferol software.";
	const std::string link = "http://makerpmhappy";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
