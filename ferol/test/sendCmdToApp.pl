#!/usr/bin/perl

die "Usage sendCmdToApp.pl host port class instance cmd\n" if @ARGV != 5;

$host     = $ARGV[0];
$port     = $ARGV[1];
$class    = $ARGV[2];
$instance = $ARGV[3];
$cmd      = $ARGV[4];

#die "The file \"$cmdFile\" does not exist\n" unless -e $cmdFile;

$curlCmd  = "curl --stderr /dev/null -H \"Content-Type: text/xml\" -H \"Content-Description: SOAP Message\" -H \"Content-Location: urn:xdaq-application:class=$class,instance=$instance\" http://$host:$port -d \"$cmd\"";

print "$class $instance $cmd to $host:$port";
open CURL, "$curlCmd|";

while(<CURL>) {
  chomp;
  $soapReply .= $_;
}

if($soapReply =~ m/Response.*>(.*)<\/xdaq:.*Response>/i) {
  print "OK\n$1\n";
  exit 0;
} elsif($soapReply =~ m/Fault.*faultstring>(.*)<.+faultstring>/i) {
  print "FAULT\n";
  print "$1\n";
  exit 1;
} elsif($soapReply eq "") {
  print "NONE\n";
  exit 1;
} else {
  print "UNKNOWN response\n";
  print "$soapReply\n";
  exit 1;
}
