/**
 *      @file version.h
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.1 $
 *     $Date: 2007/03/27 08:46:37 $
 *
 *
 **/


#ifndef _ttsipcutils_version_h_
#define _ttsipcutils_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_TTSIPCUTILS_VERSION_MAJOR 1
#define WORKSUITE_TTSIPCUTILS_VERSION_MINOR 11
#define WORKSUITE_TTSIPCUTILS_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_TTSIPCUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_TTSIPCUTILS_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_TTSIPCUTILS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TTSIPCUTILS_VERSION_MAJOR,WORKSUITE_TTSIPCUTILS_VERSION_MINOR,WORKSUITE_TTSIPCUTILS_VERSION_PATCH)
#ifndef WORKSUITE_TTSIPCUTILS_PREVIOUS_VERSIONS
#define WORKSUITE_TTSIPCUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TTSIPCUTILS_VERSION_MAJOR,WORKSUITE_TTSIPCUTILS_VERSION_MINOR,WORKSUITE_TTSIPCUTILS_VERSION_PATCH)
#else 
#define WORKSUITE_TTSIPCUTILS_FULL_VERSION_LIST  WORKSUITE_TTSIPCUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TTSIPCUTILS_VERSION_MAJOR,WORKSUITE_TTSIPCUTILS_VERSION_MINOR,WORKSUITE_TTSIPCUTILS_VERSION_PATCH)
#endif 
namespace ttsipcutils
{
	const std::string project = "worksuite";
	const std::string package  =  "ttsipcutils";
	const std::string versions = WORKSUITE_TTSIPCUTILS_FULL_VERSION_LIST;
	const std::string description = "Provides a semaphore array for inter-process communication";
	const std::string authors = "Hannes Sakulin";
	const std::string summary = "ttsipcutils";
	const std::string link = "http://xdaqwiki.cern.ch/index.php";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
