#include "tts/ipcutils/SemaphoreArray.hh"

#include "xcept/tools.h"

#include <iostream>
#include <iomanip>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

static const uint32_t n_geoslots = 21;
static const char* key_file = "/dev/xpci";
static const char project_id = 't';


int main (int argc, char* argv[]) {

  if (argc != 2) {
    std::cout << "Usage: " << argv[0] << " c | r | d | l " << std::endl;
    std::cout << " c ... create and reset locks" << std::endl;
    std::cout << " r ... reset locks" << std::endl;
    std::cout << " d ... destroy locks" << std::endl;
    std::cout << " l ... list locks" << std::endl;
    exit (-1);
  }

  try {

    switch (argv[1][0]) {
    case 'c' :
      ipcutils::SemaphoreArray::createLocks(key_file, project_id, n_geoslots);
      std::cout << "locks created and reset" << std::endl;
      break;
    case 'r' :
      ipcutils::SemaphoreArray::resetLocks(key_file, project_id, n_geoslots);
      std::cout << "locks reset" << std::endl;
      break;
    case 'd' :
      ipcutils::SemaphoreArray::destroyLocks(key_file, project_id);
      std::cout << "locks destroyed" << std::endl;
      break;
    case 'l' :
      {
      ipcutils::SemaphoreArray crlock(key_file, project_id, false, n_geoslots);
      
      for (uint32_t i=0; i< n_geoslots; i++) {
	pid_t pid = crlock.getPID(i);
	pid_t mypid = getpid();

	std::cout << "GeoSlot " << std::setw(2) << std::dec << i << "  locked:" << crlock.isLocked(i) 
		  << " last modified by PID=" << pid << ";   current process PID=" << mypid << std::endl;  
      }
      }
      break;
    default:
      std::cout << "unknown command" << std::endl;
      break;
    }
  } catch (xcept::Exception &e) {
    std::cout << "Exception caught " <<
      xcept::stdformat_exception_history(e) << std::endl;
  }

}
