/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, D. Simelevicius and H. Sakulin       *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ttsfmmtester_version_h_
#define _ttsfmmtester_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_TTSFMMTESTER_VERSION_MAJOR 2
#define WORKSUITE_TTSFMMTESTER_VERSION_MINOR 3
#define WORKSUITE_TTSFMMTESTER_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS
#define WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS "2.2.1"

//
// Template macros
//
#define WORKSUITE_TTSFMMTESTER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TTSFMMTESTER_VERSION_MAJOR,WORKSUITE_TTSFMMTESTER_VERSION_MINOR,WORKSUITE_TTSFMMTESTER_VERSION_PATCH)
#ifndef WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS
#define WORKSUITE_TTSFMMTESTER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TTSFMMTESTER_VERSION_MAJOR,WORKSUITE_TTSFMMTESTER_VERSION_MINOR,WORKSUITE_TTSFMMTESTER_VERSION_PATCH)
#else 
#define WORKSUITE_TTSFMMTESTER_FULL_VERSION_LIST  WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TTSFMMTESTER_VERSION_MAJOR,WORKSUITE_TTSFMMTESTER_VERSION_MINOR,WORKSUITE_TTSFMMTESTER_VERSION_PATCH)
#endif 
namespace ttsfmmtester
{
	const std::string project = "worksuite";
	const std::string package  =  "ttsfmmtester";
	const std::string versions = WORKSUITE_TTSFMMTESTER_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Hannes Sakulin";
	const std::string summary = "ttsfmmtester";
	const std::string link = "http://xdaqwiki.cern.ch/index.php";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
