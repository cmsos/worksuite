/**
*      @file FMMTesterHardcodedAddressTableReader.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.3 $
*     $Date: 2007/03/27 07:53:34 $
*
*
**/
#include "tts/fmmtester/FMMTesterHardcodedAddressTableReader.hh"

tts::FMMTesterHardcodedAddressTableReader::FMMTesterHardcodedAddressTableReader() 
  : HAL::PCIAddressTableDynamicReader() {

  // ******************************************************************************************************************
  // * FMM Tester card register definitions
  // * Vendor ID : 0xECD6
  // * Device ID : 0xFEED
  // *****************************************************************************************************************
  // *  key				Access		BAR	Offset		mask		read	write	description
  // *****************************************************************************************************************
  // * 
  createItem("bar0", 				HAL::CONFIGURATION, 0, 0x00000010, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("bar1", 				HAL::CONFIGURATION, 0, 0x00000014, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("GeoSlot",                          HAL::CONFIGURATION, 0, 0x00000050, 0x000000ff, 1, 0, "The geographic slot of the FMM (0..20)");
  createItem("sn_a", 				HAL::CONFIGURATION, 0, 0x0000005c, 0xFFFFFFFF, 1, 0, "serial number a (8 digit)");
  createItem("sn_b", 				HAL::CONFIGURATION, 0, 0x00000060, 0xFFFFFFFF, 1, 0, "serial number b (8 digit)");
  createItem("sn_c", 				HAL::CONFIGURATION, 0, 0x00000064, 0x00000FFF, 1, 0, "serial number c (3 digit)");
  // *
  // *
  // *
  createItem("REPROG_XILINX", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00000004, 0, 1, "reprogram bit for Xilinx reconfiguration (toggle to 1, wait, toggle to 0)");
  createItem("REPROG_ALTERA", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00000002, 0, 1, "reprogram bit for Xilinx reconfiguration (toggle to 1, wait, toggle to 0)");
  // 
  createItem("JTAG_ENABLE", 		        HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 0, 1, "enable bit for JTAG");
  // 
  createItem("JTAG_TDI_TMS_CLK", 		HAL::CONFIGURATION, 0, 0x00000044, 0x0000007f, 0, 1, "[6:0] 6:TDI 1:TMS 0:CLK bits sent to JTAG chain");
  createItem("JTAG_TDO", 		        HAL::CONFIGURATION, 0, 0x00000044, 0x00000080, 1, 0, "TDO bit read from JTAG chain");
  createItem("FWID_ALTERA", 			HAL::CONFIGURATION, 0, 0x00000048, 0xFFFFFFFF, 1, 0, "firmware ID ALTERA");
  // 
  // 
  // *
  // * Register definition
  // *
  createItem("CNTL", 				HAL::MEMORY, 0, 0x00000100, 0x0001FFFF, 1, 1, "Control register, 17 bits are used");
  createItem("STAT", 				HAL::MEMORY, 0, 0x00000200, 0xFFFFFFFF, 1, 0, "Status register");
  createItem("TFIFO", 				HAL::MEMORY, 0, 0x00000300, 0x0000FFFF, 1, 0, "Storage FIFO (16 bits) of 4 FMM outputs");
  createItem("Test_out_ready", 			HAL::MEMORY, 0, 0x00000400, 0x000FFFFF, 1, 1, "Test register for ready(19:0), 20 bits");
  createItem("Test_out_busy", 			HAL::MEMORY, 0, 0x00000404, 0x000FFFFF, 1, 1, "Test register for busy(19:0), 20 bits");
  createItem("Test_out_synch", 			HAL::MEMORY, 0, 0x00000408, 0x000FFFFF, 1, 1, "Test register for out_of_synch(19:0), 20 bits");
  createItem("Test_out_overflow", 		HAL::MEMORY, 0, 0x0000040c, 0x000FFFFF, 1, 1, "Test register for overflow(19:0), 20 bits");
  createItem("Update_test_reg", 			HAL::MEMORY, 0, 0x00000410, 0x00000001, 0, 1, "a write updates the permanent output of the Test reg (80 bits)");
  createItem("ID", 				HAL::MEMORY, 0, 0x00000500, 0xFFFFFFFF, 1, 0, "Identification register, holds version number, etc...");
  createItem("RAMS", 				HAL::MEMORY, 0, 0x00004000, 0x000FFFFF, 1, 1, "Sequence memory (1k x 80 bits) for stimuli");
  createItem("RAMS_max", 			HAL::MEMORY, 0, 0x00007FFC, 0x000FFFFF, 1, 1, "Dummy register for accomodating software");
  createItem("ZBT", 				HAL::MEMORY, 1, 0x00000000, 0xFFFFFFFF, 1, 1, "2 MB of contiguous memory (max. is base + 0x1F_FFFC)");
  createItem("ZBT_max", 				HAL::MEMORY, 1, 0x001ffffc, 0xFFFFFFFF, 1, 1, "Last address of ZBT memory");
  // *
  // *
  // *
  // *
  // *
  // 
  // * Field definition for Control reg.
  // *
  createItem("reset_tfifo", 			HAL::MEMORY, 0, 0x00000100, 0x00000001, 1, 1, "resets TFIFO");
  createItem("reset_ram_add_gen", 		HAL::MEMORY, 0, 0x00000100, 0x00000004, 1, 1, "resets address generator for the sequence + seq_sm");
  createItem("sequence_clk_sel",                 HAL::MEMORY, 0, 0x00000100, 0x000000f8, 1, 1, "select clock for sequence: 0=20MHz,1=10MHz,2=5MHz");
  createItem("leds", 				HAL::MEMORY, 0, 0x00000100, 0x00000f00, 1, 1, "control of the 4 leds on front panel");
  createItem("led1", 				HAL::MEMORY, 0, 0x00000100, 0x00000100, 1, 1, "control of LED L1 on front panel");
  createItem("led2", 				HAL::MEMORY, 0, 0x00000100, 0x00000200, 1, 1, "control of LED L2 on front panel");
  createItem("led3", 				HAL::MEMORY, 0, 0x00000100, 0x00000400, 1, 1, "control of LED L3 on front panel");
  createItem("led4", 				HAL::MEMORY, 0, 0x00000100, 0x00000800, 1, 1, "control of LED L4 on front panel");
  createItem("fifo_delay",                       HAL::MEMORY, 0, 0x00000100, 0x0000F000, 1, 1, "delay of clock and launch to input FIFO");
  createItem("stimuli_select", 			HAL::MEMORY, 0, 0x00000100, 0x00010000, 1, 1, "switch between RAMS and TEST REG");
  createItem("launch_sequence", 			HAL::MEMORY, 0, 0x00000100, 0x00020000, 1, 1, "launch the sequence output");
  createItem("number_of_sequences",              HAL::MEMORY, 0, 0x00000100, 0x7f000000, 1, 1, "the number of sequences to output");
  createItem("indefinite_sequences",             HAL::MEMORY, 0, 0x00000100, 0x80000000, 1, 1, "indefinite number of sequences. must be stopped with reset_ram_add_gen");
  // 
  // *
  // *
  // *
  // * Field definition for Status reg.
  // *
  createItem("PLL_int", 				HAL::MEMORY, 0, 0x00000200, 0x00000001, 1, 0, "Status of PLL for internal clock, must be at 1");
  createItem("PLL_ext", 				HAL::MEMORY, 0, 0x00000200, 0x00000002, 1, 0, "Status of PLL for external clock, must be at 1");
  createItem("TFIFO_empty", 			HAL::MEMORY, 0, 0x00000200, 0x00000004, 1, 0, "When equal to 1, tfifo empty");
  createItem("TFIFO_full", 			HAL::MEMORY, 0, 0x00000200, 0x00000008, 1, 0, "When equal to 1, tfifo full");
  createItem("sequence_running", 		HAL::MEMORY, 0, 0x00000200, 0x00000010, 1, 0, "When equal to 1, a sequence is in progress");
  createItem("fmm_all", 				HAL::MEMORY, 0, 0x00000200, 0xFFFF0000, 1, 0, "Current value on the 4 FMM outputs");
  // *
  // *
  // *
  // * Field definition for ID reg.
  // *
  createItem("num_inputs", 			HAL::MEMORY, 0, 0x00000500, 0xFC000000, 1, 0, "Number of inputs");
  createItem("num_outputs", 			HAL::MEMORY, 0, 0x00000500, 0x03F00000, 1, 0, "Number of outputs");
  createItem("num_version", 			HAL::MEMORY, 0, 0x00000500, 0x000F8000, 1, 0, "Version number");
  createItem("num_revision", 			HAL::MEMORY, 0, 0x00000500, 0x00007C00, 1, 0, "Revision number");
  createItem("num_fmm", 				HAL::MEMORY, 0, 0x00000500, 0x00000300, 1, 0, "Number of logical FMMs (can be one or two)");
  createItem("id_misc", 				HAL::MEMORY, 0, 0x00000500, 0x000000ff, 1, 0, "Misc not yet defined, currently 0xaa");
  // *****************************************************************************************************************
  // 
  // *
  // *
  // *
  // * Field definition for ID reg.
  // *
  createItem("ID_year", 				HAL::MEMORY, 0, 0x00000500, 0xFF000000, 1, 0, "year since 2000 BCD");
  createItem("ID_month", 			HAL::MEMORY, 0, 0x00000500, 0x00FF0000, 1, 0, "month (1..12) BCD");
  createItem("ID_day", 				HAL::MEMORY, 0, 0x00000500, 0x0000FF00, 1, 0, "day (1..31) BCD ");
  createItem("ID_rev_in_day", 			HAL::MEMORY, 0, 0x00000500, 0x000000FF, 1, 0, "revision in this day (0..99) BCD");
  // *****************************************************************************************************************
}

