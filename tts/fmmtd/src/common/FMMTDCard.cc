/**
*      @file FMMTDCard.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:32 $
*
*
**/
#include "tts/fmmtd/FMMTDCard.hh"

#include <iostream>
#include <sstream>



tts::FMMTDCard::FMMTDCard( HAL::PCIDevice* device, ipcutils::SemaphoreArray& cratelock ) 
  : CPCICard(device, cratelock) {};

tts::FMMTDCard::~FMMTDCard() {
};

void tts::FMMTDCard::togglePCIMode(bool pcimode) {

  check_write_lock();

  _dev()->write("PCI_MODE", pcimode?0x1:0x0);
}; 


void tts::FMMTDCard::generateResetPulse() {

  check_write_lock();

  _dev()->write("reset_pulse", 0x1);
}
