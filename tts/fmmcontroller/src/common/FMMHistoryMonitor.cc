/**
*      @file FMMHistoryMonitor.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.10 $
*     $Date: 2007/03/28 12:06:23 $
*
*
**/
#include "tts/fmmcontroller/FMMHistoryMonitor.hh"
#include "tts/fmm/FMMTimer.hh"
#include "tts/fmmcontroller/FMMHistoryConsumer.hh"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/PollingWorkLoop.h"

#include "xcept/tools.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <algorithm>

#include "stdlib.h"

#define CONST_64(hi,lo) ( ( ((uint64_t) hi) << 32 ) | ( (uint64_t) lo ) )

tts::FMMHistoryMonitor::FMMHistoryMonitor( tts::FMMCard& card,
				      double cacheDepthSeconds,
				      bool selfUpdate,
				      log4cplus::Logger* logger) 
  : _card(card),
    _cacheDepthSeconds(cacheDepthSeconds),
    _selfUpdate(selfUpdate),
    _logger(logger), 
    _t_start(0), 
    _t_lastUpdate(0), 
    _lastAddress(0),
    _lastWrapCount(0),
    _lastTimeCounter(0),
    _deque_semaphore(toolbox::BSem::FULL) {

  if (_selfUpdate) {
    _wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("FMMHistoryPollingWorkLoop", "polling");
    _signature = toolbox::task::bind (this, &tts::FMMHistoryMonitor::autoUpdate, "fmm_hist_update");
  }

};

tts::FMMHistoryMonitor::~FMMHistoryMonitor() {

  _cache.clear();

};


void tts::FMMHistoryMonitor::start( bool resetHW ) {

  if (_selfUpdate && _wl->isActive())
    XCEPT_RAISE(xcept::Exception, "tts::FMMHistoryMonitor::startMonitor: monitor is already running");

  _deque_semaphore.take();

  try {
    _cache.clear();
    _t_start = _t_lastUpdate = tts::FMMTimer::getMicroTime();

    _lastAddress = _lastWrapCount = 0;
    _lastTimeCounter = 0;

    // notify consumers of monitor start
    std::list<tts::FMMHistoryConsumer*>::iterator it = _consumers.begin();
    for (; it != _consumers.end(); ++it) 
      (*it)->beforeMonitorStart();

    if ( resetHW )
      _card.resetAll();

  }
  catch (...) {
    _deque_semaphore.give();
    throw;
  }

  _deque_semaphore.give();

  if (_selfUpdate) {
    _wl->submit(_signature);
    _wl->activate();
  }
  
};

void tts::FMMHistoryMonitor::stop() {

  if (_selfUpdate)
    _wl->cancel();
  
  _deque_semaphore.take();
  
  try {
    doUpdate();

    // notify consumers of monitor stop
    std::list<tts::FMMHistoryConsumer*>::iterator it = _consumers.begin();
    for (; it != _consumers.end(); ++it) 
      (*it)->afterMonitorStop( _lastTimeCounter );

  }
  catch (...) {
    _deque_semaphore.give();
    throw;
  }

  _deque_semaphore.give();

};


long tts::FMMHistoryMonitor::update() {

  long n_read=0l;
  _deque_semaphore.take();

  try {
    n_read = doUpdate();
  }
  catch (...) {
    _deque_semaphore.give();
    throw;
  }

  _deque_semaphore.give();

  return n_read;
}

long tts::FMMHistoryMonitor::doUpdate() {

  uint64_t t_before = tts::FMMTimer::getMicroTime();
  std::pair<uint32_t, uint32_t> current_wradd = _card.readHistoryAddressAndWrapCount();
  
  // have we exceeded the time limit?
  checkElapsedTime( tts::FMMTimer::getMicroTime(), _t_lastUpdate);
  _t_lastUpdate = t_before;

  // has the history address wrapped too far since last update?
  checkOverwrite(current_wradd.first, current_wradd.second, _lastAddress, _lastWrapCount, false);
  
  // read the history
  uint64_t ticks_since_start = ( t_before - _t_start ) * 40; 
  const uint64_t tt_max = (uint64_t) 1<<40;

  long n_read = 0l;
  for (uint32_t addr = _lastAddress; addr != current_wradd.first; addr = (addr+1) % FMMHistMemSize) {
    tts::FMMHistoryItem hi;
    _card.readHistoryItem( addr, hi );    

    // extend the time tag
    uint64_t tt = hi.getTimestamp();
    uint64_t n_wraps = (ticks_since_start + tt_max/2 - tt ) / tt_max;
    hi.setWrapCount( (uint32_t) n_wraps );

    // notify consumers of new history item
    std::list<tts::FMMHistoryConsumer*>::iterator it = _consumers.begin();
    for (; it != _consumers.end(); ++it) 
      (*it)->processItem( hi, addr );

    _cache.push_back( hi );    
    ++n_read;
  } 
    
  // has the history address wrapped too far during reading ?
  std::pair<uint32_t, uint32_t> wradd_after = _card.readHistoryAddressAndWrapCount();
  checkOverwrite(wradd_after.first, wradd_after.second, _lastAddress, _lastWrapCount, true);

  _lastAddress = current_wradd.first; _lastWrapCount = current_wradd.second;

  updateTimeCounter();
  
  // drop old elements from cache
  // always leave at least one element which is older than the time window 
  while ( _cache.size() > 1 && 
	  (_lastTimeCounter - _cache[1].getExtendedTimestamp()) > 40000000. * _cacheDepthSeconds)  
    _cache.pop_front();

  return n_read;
}; 


void tts::FMMHistoryMonitor::checkElapsedTime(uint64_t t_now, uint64_t t_last) {

  double seconds_since_last_update = ( t_now - t_last ) / 1.e6;
  if ( seconds_since_last_update > 107) { 

    std::stringstream msg;    
    msg << "Warning: FMM history memory has to be polled at least every 107 seconds. Time elapsed since last poll is: " 
	<< seconds_since_last_update << " s. History memory overwrites may be undetected. Continuing.";

    if (_logger) {
      LOG4CPLUS_WARN ( (*_logger), msg.str() );
    } 
    else
      std::cout << "Warning in tts::FMMHistoryMonitor::doUpdate(): " << msg.str() << std::endl;
   
    // notify consumers of new history item
    std::list<tts::FMMHistoryConsumer*>::iterator it = _consumers.begin();
    for (; it != _consumers.end(); ++it) 
      (*it)->processWarning( msg.str() );
  }
}


void tts::FMMHistoryMonitor::checkOverwrite(uint32_t newAddress, 
				       uint32_t newWrapCount,
				       uint32_t oldAddress, 
				       uint32_t oldWrapCount, 
				       bool duringReadout) {

  uint32_t delta_wrapcount = ( newWrapCount - oldWrapCount + FMMWrapCounterLength ) % FMMWrapCounterLength;
  
  if ( ( delta_wrapcount == 1 && newAddress > oldAddress ) ||
       delta_wrapcount > 1) {

    std::stringstream msg;    
    msg << "Error: FMM history memory has been overwritten" 
	<< (duringReadout?" during history memory readout." : " since last call to update(). FMM software transition analysis cannot keep up with rate of incoming transitions. This is not a major problem since all types of dead time are now monitored in firmware.\n ")
	<< " currentAddress=" << newAddress
	<< " currentWrapCount=" << newWrapCount
	<< " lastAddress = " << oldAddress
	<< " lastWrapCount=" << oldWrapCount
	<< " Continuing. ";

    if (_logger) {
      LOG4CPLUS_ERROR ( (*_logger), msg.str() );
    } 
    else
      std::cout << "Error in tts::FMMHistoryMonitor::doUpdate(): " << msg.str() << std::endl;

    // notify consumers of new history item
    std::list<tts::FMMHistoryConsumer*>::iterator it = _consumers.begin();
    for (; it != _consumers.end(); ++it) 
      (*it)->processWarning( msg.str() );
  }

} 

void tts::FMMHistoryMonitor::updateTimeCounter() {

  //
  // read and extend current time tag 
  //
  uint64_t tt_high =    (_lastTimeCounter & CONST_64(0xffffff00, 0x00000000) ) >> 40;
  uint64_t timetag_old = _lastTimeCounter & CONST_64(0x000000ff, 0xffffffff);
  _lastTimeCounter = _card.readTimeTag();
  if (_lastTimeCounter < timetag_old) tt_high++;
  _lastTimeCounter |= tt_high << 40;
}


bool tts::FMMHistoryMonitor::autoUpdate(toolbox::task::WorkLoop* wl) {

  try {
    update();
  }
  catch (xcept::Exception &e) {
    std::cout << "Caught exception in tts::FMMHistoryMonitor::autoUpdate: " 
	      << xcept::stdformat_exception_history(e) << std::endl;
  }

  return true; // go on
}; 



void tts::FMMHistoryMonitor::registerConsumer( tts::FMMHistoryConsumer* consumer ) {

  _consumers.push_back( consumer );
}

void tts::FMMHistoryMonitor::unRegisterConsumer( tts::FMMHistoryConsumer* consumer ) {

  _consumers.remove( consumer );
}


void tts::FMMHistoryMonitor::retrieveCachedHistory( std::vector<tts::FMMHistoryItem> & items) {
  
  items.clear();
  _deque_semaphore.take();
  items.resize( _cache.size() );

  try {
    copy( _cache.begin(), _cache.end(), items.begin() );
  }
  catch (...) {
    _deque_semaphore.give();
    throw;
  }

  _deque_semaphore.give();

}



