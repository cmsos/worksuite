/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:06:23 $
 *
 *
 **/
#include "tts/fmmcontroller/version.h"

#include "tts/fmm/version.h"
#include "tts/fmmtd/version.h"
#include "tts/cpcibase/version.h"
#include "tts/ttsbase/version.h"
#include "tts/ipcutils/version.h"

#include "config/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xgi/version.h"
#include "xoap/version.h"

GETPACKAGEINFO(ttsfmmcontroller)

void ttsfmmcontroller::checkPackageDependencies()
{
	CHECKDEPENDENCY(ttsfmm);  
	CHECKDEPENDENCY(ttsfmmtd);  
	CHECKDEPENDENCY(ttscpcibase);  
	CHECKDEPENDENCY(ttsttsbase);  
	CHECKDEPENDENCY(ttsipcutils);  

	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xdata);  
	CHECKDEPENDENCY(xdaq);  
	CHECKDEPENDENCY(xgi);  
	CHECKDEPENDENCY(xoap);  
}

std::set<std::string, std::less<std::string> > ttsfmmcontroller::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,ttsfmm); 
	ADDDEPENDENCY(dependencies,ttsfmmtd); 
	ADDDEPENDENCY(dependencies,ttscpcibase); 
	ADDDEPENDENCY(dependencies,ttsttsbase); 
	ADDDEPENDENCY(dependencies,ttsipcutils); 

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,xgi);
	ADDDEPENDENCY(dependencies,xoap);
	 
	return dependencies;
}	
	
