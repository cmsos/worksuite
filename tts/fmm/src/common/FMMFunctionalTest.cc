/**
*      @file FMMFunctionalTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.9 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMFunctionalTest.hh"

#include <iostream>


std::string tts::FMMFunctionalTest::getVersion() const {
  return "$Revision: 1.9 $";
};

bool tts::FMMFunctionalTest::_run(uint32_t nloops) {

  if ( doLog(V_INFO) ) {
    _os << "===FMMFunctionalTest===" << std::endl;
    _os << "testing dead-time monitors" << std::endl;
  }

  _card.toggleSimuMode(true);
  _card.toggleDualMode(true);
  _card.setMask(0x0);
  _card.setThreshold10A(1);
  _card.setThreshold10B(1);

  uint32_t nb_error = 0;

  tts::TTSState countedStates [5] = { 
				      tts::TTSState::BUSY,
				      tts::TTSState::WARNING,
				      tts::TTSState::READY,
				      tts::TTSState::OUTOFSYNC,
				      tts::TTSState::ERROR };

  //
  // Basic test just checks that the right counters are counting anything
  //

  for (uint32_t iloop=0; iloop < nloops; iloop++) {
    std::vector <TTSState> states(20, tts::TTSState::DISCONNECT1);
    _card.setSimulatedInputStates(states);
    _card.resetAll();

    for (int itype = 0; itype<5; itype++) {
      std::vector <TTSState> states(20, tts::TTSState::DISCONNECT1);
      _card.setSimulatedInputStates(states);

      for (int i=0;i<21;i++) {
	// check that all dead time counters are zero.
	std::vector<uint32_t> dc;
	uint64_t tt;

	_card.readDeadTimeCountersAndTimeTag(dc, tt);

	for(int j=0;j<i;j++) { // check counter that should be non-zero

	  int icounter = j+22*itype;

	  if (dc[icounter]==0) {
	    if ( doLog(V_ERR) )
	      _os << errPos() 
		  << "error: dead-time counter " << icounter << " should be non-zero but was " << dc[icounter] << std::endl;
	    nb_error++;
	  }
	}

	for(int j=i;j<20;j++) { // check counters that should be zero

	  int icounter = j+22*itype;

	  if (dc[icounter]!=0) {
	    if ( doLog(V_ERR) )
	      _os << errPos()
		  << "error: dead-time counter " << icounter << " should be zero but was " << dc[icounter] << std::endl;
	    nb_error++;
	  }
	}

	if (i<10) { // check output dead times A to be zero

	  int icounter = 20+22*itype;

	  if (dc[icounter]!=0) {
	    if ( doLog(V_ERR) )
	      _os << errPos() 
		  << "error: output dead-time counter " << icounter << " should be zero but was " << dc[icounter] << std::endl;
	    nb_error++;
	  }	  
	} 
	else { // check output dead times A to be non-zero

	  int icounter = 20+22*itype;

	  if (dc[icounter]==0) {
	    if ( doLog(V_ERR) )
	      _os << errPos() 
		  << "error: output dead-time counter " << icounter << " should be non-zero but was " << dc[icounter] 
		  << "; check that the firmware revision is > 060515_00" << std::endl;
	    nb_error++;
	  }	  

	}
	
	if (i<20) { // check output dead times B to be zero

	  int icounter = 21+22*itype;

	  if (dc[icounter]!=0) {
	    if ( doLog(V_ERR) )
	      _os << errPos() 
		  << "error: output dead-time counter " << icounter << " should be zero but was " << dc[icounter] << std::endl;
	    nb_error++;
	  }	  
	} 
	else { // check output dead times B to be non-zero

	  int icounter = 21+22*itype;

	  if (dc[icounter]==0) {
	    if ( doLog(V_ERR) )
	      _os << errPos() 
		  << "error: output dead-time counter " << icounter << " should be non-zero but was " << dc[icounter] 
		  << "; check that the firmware revision is > 060515_00" << std::endl;
	    nb_error++;
	  }	  

	}
	
	if (i<20) {
	  // create some dead-time
	  tts::TTSState state = countedStates[itype];

	  _card.setSimulatedInputState(i, state);
	}
      }
    }


    // Advanced test checks that sum of counters for a certain input matches the time tag excatly 
    // (if only inputs that are counted are applied)
    
    std::vector <TTSState> states1(20, tts::TTSState::READY);
    _card.setSimulatedInputStates(states1);
    _card.resetAll();

    for (int itype = 0; itype<5; itype++) {
      
      // simulate one of the counted states

      tts::TTSState state = countedStates[itype];

      for (int i=0;i<20;++i)
	_card.setSimulatedInputState(i, state);
      

      //
      // read all counters and time tag
      //

      std::vector<uint32_t> dc;
      uint64_t tt;

      _card.readDeadTimeCountersAndTimeTag(dc, tt);

      for (int i=0;i<22;++i) {

	uint64_t sumdeadtimes = 0l;
	for (int jtype=0;jtype<5;++jtype)
	  sumdeadtimes += dc[i+22*jtype];
	if (tt != sumdeadtimes) {
	  if ( doLog(V_ERR) )
	    _os << errPos() 
		<< "error: sum of dead times for " << (i<20?"input ":"output ") << i << " (" << sumdeadtimes 
		<< ") not equal to time tag (" << tt << ")" << std::endl;
	  
	  for (int jtype=0;jtype<5;++jtype) {
	    _os << "    dead time in state "  << countedStates[jtype].getName() << " : " << dc[i+22*jtype] << std::endl; 
	  }

	  nb_error++;
	}

      }

    }

  }

  _nloops += nloops;
  _nerrors += nb_error;

  if ( doLog(V_INFO) ) 
    _os << " Done ! " << nb_error << " errors." << std::endl;

  return nb_error == 0;
}

