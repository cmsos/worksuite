/**
*      @file FMMRandomTransitionTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.9 $
*     $Date: 2007/03/27 07:53:28 $
*
*
**/
#include "tts/fmm/FMMRandomTransitionTest.hh"
#include "tts/fmm/FMMHistoryItem.hh"

#include <iostream>
#include <stdlib.h>

std::string tts::FMMRandomTransitionTest::getVersion() const {
  return "$Revision: 1.9 $";
};

bool tts::FMMRandomTransitionTest::_run(uint32_t nloops) {

  if ( doLog(V_INFO) )
    _os << "===FMMRandomTransitionTest===" << std::endl;

  _card.setMask(0xfffff);

  uint32_t last_ready = 0xfffff;
  uint32_t last_busy = 0x0;
  uint32_t last_sync = 0x0;
  uint32_t last_warn = 0x0;

  _card.device().write("Test_in_ready", last_ready);
  _card.device().write("Test_in_busy", last_busy);
  _card.device().write("Test_in_synch", last_sync);
  _card.device().write("Test_in_overflow", last_warn);
  _card.device().write("Update_test_reg", 0x1);

  _card.resetAll();

  // do this in order to avoid reading the transition generated right after the resetAll() in firmware starting from 060928_00
  _card.resetHistoryAddress(); 


  _card.toggleSimuMode(true);
  _card.toggleDMAMode(false);
  _card.setThreshold20(0);
  _card.setThreshold10A(0);
  _card.setThreshold10B(0);

  const uint32_t nb_word = 131072;
  //const uint32_t nb_word = 5;

  uint32_t ready[nb_word];
  uint32_t busy[nb_word];
  uint32_t sync[nb_word];
  uint32_t warn[nb_word];

  uint64_t nb_error = 0;
  nloops=1;
  for (uint32_t j=0; j<nloops; j++) {

    if ( doLog(V_INFO) ) {
      _os << "Starting loop " << std::dec << (j+1) << " / " << nloops << " ..." << std::endl;
      _os << "Simulating " << std::dec << nb_word << " random transitions...";
    }

    for (uint32_t i = 0; i < nb_word; i++) {

      do {
	ready[i] = ( (uint32_t) rand() ) & 0xfffff;
	busy[i]  = ( (uint32_t) rand() ) & 0xfffff;
	sync[i]  = ( (uint32_t) rand() ) & 0xfffff;
	warn[i]  = ( (uint32_t) rand() ) & 0xfffff;
      }
      while (ready[i]== last_ready &&
	     busy[i] == last_busy &&
	     sync[i] == last_sync &&
	     warn[i] == last_warn );
      _card.device().write("Test_in_ready", ready[i]);
      _card.device().write("Test_in_busy", busy[i]);
      _card.device().write("Test_in_synch", sync[i]);
      _card.device().write("Test_in_overflow", warn[i]);
      _card.device().write("Update_test_reg", 0x0);

      last_ready = ready[i];
      last_busy = busy[i];
      last_sync = sync[i];
      last_warn = warn[i];
    }

    if ( doLog(V_INFO) ) {
      _os << " Done!" << std::endl;
      _os << "Checking " << std::dec << nb_word << " history entries...";
    }
    
    for (uint32_t i = 0; i < nb_word; i++) {
      uint32_t returned_ready=0;
      _card.device().read("ZBT", &returned_ready, i*16);
      uint32_t returned_busy=0;
      _card.device().read("ZBT", &returned_busy,  i*16+4);
      uint32_t returned_sync=0;
      _card.device().read("ZBT", &returned_sync,  i*16+8);
      uint32_t returned_warn=0;
      _card.device().read("ZBT", &returned_warn, i*16+12);

      if ( (ready[i] != (returned_ready & 0xfffff) ) ||
	   (busy[i]  != (returned_busy  & 0xfffff) ) ||
	   (sync[i]  != (returned_sync  & 0xfffff) ) ||
	   (warn[i]  != (returned_warn  & 0xfffff) ) ) {
	if ( doLog(V_ERR) ) {
	  _os << errPos()
	      << "compare error at transition " << std::dec << i << " :"
	      << "simulated (ready, busy, sync, warn) = " 
	      << "(" << std::hex << ready[i] 
	      << "," << std::hex << busy[i] 
	      << "," << std::hex << sync[i] 
	      << "," << std::hex << warn[i] 
	      << "), history item: " 
	      << "(" << std::hex << returned_ready 
	      << "," << std::hex << returned_busy
	      << "," << std::hex << returned_sync
	      << "," << std::hex << returned_warn
	      << ")" << std::endl;
	  uint32_t histdata[4];
	  histdata[0]=ready[i];
	  histdata[1]=busy[i];
	  histdata[2]=sync[i];
	  histdata[3]=warn[i];
	  uint32_t histdataret[4];
	  histdataret[0]=returned_ready;
	  histdataret[1]=returned_busy;
	  histdataret[2]=returned_sync;
	  histdataret[3]=returned_warn;
	  _os << "simu:" << (new FMMHistoryItem(histdata,0))->toString() << std::endl;
	  _os << "read:" << (new FMMHistoryItem(histdataret,0))->toString() << std::endl;

	    }
	nb_error++;
      }

    }

    // check wrap count
    std::pair<uint32_t, uint32_t> wradd_wrap = _card.readHistoryAddressAndWrapCount();
    
    uint32_t wrapcount =  (j+1) % tts::FMMCard::FMMWrapCounterLength;
    if (wradd_wrap.second != wrapcount  ) {
      if ( doLog(V_ERR) ) {
	_os << errPos()
	    << "wrap counter error after loop " << std::dec << (j+1) << " : wrap counter should be " << std::dec << wrapcount 
	    << " but is " << std::dec << wradd_wrap.second << std::endl;
      }
      nb_error++;
    }
    
    if ( doLog(V_INFO) )
      _os << " Done! " << nb_error << " errors." << std::endl;
  }

  _nloops += nloops;
  _nerrors += nb_error;

  return (nb_error == 0);
}

