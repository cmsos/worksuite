/**
*      @file FMMLogicEmulator.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.6 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMLogicEmulator.hh"

#include <iostream>


tts::TTSState tts::FMMLogicEmulator::getResultA(std::vector<tts::TTSState> states) const {
  
  TTSState tmp(tts::TTSState::READY);
  uint32_t func;

  if (!_cfg.isDualMode())
    merge(states, _cfg.getMask(), _cfg.getThreshold20(), 0 , 19, tmp, func);
  else
    merge(states, _cfg.getMask(), _cfg.getThreshold10A(), 0, 9, tmp, func);
    
  return tmp;

}

tts::TTSState tts::FMMLogicEmulator::getResultB(std::vector<tts::TTSState> states) const {
  
  TTSState tmp(tts::TTSState::READY);
  uint32_t func;

  if (!_cfg.isDualMode())
    merge(states, _cfg.getMask(), _cfg.getThreshold20(), 0, 19, tmp, func);
  else
    merge(states, _cfg.getMask(), _cfg.getThreshold10B(), 10, 19, tmp, func);
    
  return tmp;

}


uint32_t tts::FMMLogicEmulator::getFuncA(std::vector<tts::TTSState> states) const {
  
  TTSState tmp(tts::TTSState::READY);
  uint32_t func;

  if (!_cfg.isDualMode())
    merge(states, _cfg.getMask(), _cfg.getThreshold20(), 0, 19, tmp, func);
  else
    merge(states, _cfg.getMask(), _cfg.getThreshold10A(), 0, 9, tmp, func);
    
  return func;

}

uint32_t tts::FMMLogicEmulator::getFuncB(std::vector<tts::TTSState> states) const {
  
  TTSState tmp(tts::TTSState::READY);
  uint32_t func;

  if (!_cfg.isDualMode())
    merge(states, _cfg.getMask(), _cfg.getThreshold20(), 0, 19, tmp, func);
  else
    merge(states, _cfg.getMask(), _cfg.getThreshold10B(), 10, 19, tmp, func);
    
  return func;

}

void tts::FMMLogicEmulator::merge(std::vector<tts::TTSState> states, uint32_t mask, uint32_t threshold, 
			     uint32_t from, uint32_t to,
			     tts::TTSState& outstate, uint32_t& func) const {

  uint32_t n_dis   = 0;
  uint32_t n_err   = 0;
  uint32_t n_oos   = 0;
  uint32_t n_busy  = 0;
  uint32_t n_warn  = 0;
  uint32_t n_inv   = 0;
  uint32_t n_ready = 0;

  for (uint32_t i=from; i<=to; i++) {

    if ( (mask & (1<<i)) != 0) {
      n_ready++;
      continue;
    }
    switch( states[i] ) {
    case tts::TTSState::DISCONNECT1:
    case tts::TTSState::DISCONNECT2: n_dis++; break;
    case tts::TTSState::ERROR:       n_err++; break;
    case tts::TTSState::OUTOFSYNC:   n_oos++; break;
    case tts::TTSState::BUSY:        n_busy++; break;
    case tts::TTSState::WARNING:     n_warn++; break;
    case tts::TTSState::READY:       n_ready++; break;
    default: n_inv++; break;
    }
  }
  
  outstate = tts::TTSState::READY;

  // FIXME: temporarily set INVALID as top priority (as currently in hardware)

  // priority encoder
  if (n_dis>0) outstate = tts::TTSState::DISCONNECT1;
  else if (n_err>0) outstate = tts::TTSState::ERROR;
  else if (n_oos>threshold) outstate = tts::TTSState::OUTOFSYNC;
  else if (n_busy>0) outstate = tts::TTSState::BUSY;
  else if (n_warn>0) outstate = tts::TTSState::WARNING;
  else if (n_inv>0) outstate = tts::TTSState::INVALID;
  else if (n_ready==0) std::cout << "problem in priority encoder ... no state";
	
  //  std::cout << " out: " << outstate.getShortName();

  // store the inputs of the priority encoder in func
  func=0;
  if (n_dis>0)            func |= 0x04;
  if (n_err>0)            func |= 0x08;
  if (n_oos>threshold)    func |= 0x10;
  if (n_busy>0)           func |= 0x02;
  if (n_warn>0)           func |= 0x20;
  if (n_inv>0)            func |= 0x40;
  if (n_ready>0)          func |= 0x01;

}
