/**
 *      @file FMMCard.cc
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.13 $
 *     $Date: 2007/03/28 12:06:23 $
 *
 *
 **/
#include "tts/fmm/FMMCard.hh"

#include <iostream>
#include <sstream>
#include <iomanip>


tts::FMMCard::FMMCard( HAL::PCIDevice* device, ipcutils::SemaphoreArray& cratelock ) 
  : CPCICard(device, cratelock), _ctrl_reg_semaphore(toolbox::BSem::FULL), _timers_semaphore(toolbox::BSem::FULL), _fwRevXilinx("") {};

tts::FMMCard::~FMMCard() {
};


void tts::FMMCard::resetDeadTimeMonitors() {

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("reset_hw_mons",0x1); 
    _dev()->write("reset_hw_mons",0x0); 
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();


};
  
void tts::FMMCard::resetTransitionMissCounter() {

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("reset_trt_miss",0x1); 
    _dev()->write("reset_trt_miss",0x0); 
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
};
  
void tts::FMMCard::resetTimeTag() {

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("reset_time_tag",0x1); 
    _dev()->write("reset_time_tag",0x0); 
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
};

void tts::FMMCard::resetHistoryAddress() {

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("reset_zbt_add",0x1); 
    _dev()->write("reset_zbt_add",0x0); 
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
};
  
void tts::FMMCard::resetAll() { 

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("reset_all",0x7f); 
    _dev()->write("reset_all",0x0); 
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
};

uint32_t tts::FMMCard::readHistoryAddress() {

  uint32_t add;
  _dev()->read("ZBT_WR_ADD", &add);
  return add;
}

std::pair<uint32_t, uint32_t> tts::FMMCard::readHistoryAddressAndWrapCount() {

  uint32_t add;
  _dev()->read("ZBT_WR_ADD_ALL", &add);

  std::pair<uint32_t, uint32_t> add_and_wrap;
  add_and_wrap.first = add & 0x1ffff;
  add_and_wrap.second = ( add & 0xfffe0000 ) >> 17;

  return add_and_wrap;
}


uint32_t tts::FMMCard::readTransitionMissCounter() {

  uint32_t cnt;
  _dev()->read("TRT_MISS", &cnt);
  return cnt;
}

std::string tts::FMMCard::readFirmwareRev() {

  if (_fwRevXilinx == "") {
    uint32_t year;
    uint32_t month;
    uint32_t day;
    uint32_t rev_in_day;
    
    _dev()->read("ID_year", &year);
    _dev()->read("ID_month", &month);
    _dev()->read("ID_day", &day);
    _dev()->read("ID_rev_in_day", &rev_in_day);

    _fwRevXilinx =  
      BCDtoString( year ) +
      BCDtoString( month ) +
      BCDtoString( day) +
      "_" +
      BCDtoString( rev_in_day );
  }

  return _fwRevXilinx;
}

std::string tts::FMMCard::BCDtoString(uint32_t num, uint32_t ndigit) {

  std::stringstream txt;
  for (int i = (int) ndigit-1; i>=0; i--)
    txt << std::setw(1) << std::hex << ( ( num & ( 0xf << (4*i) ) ) >> (4*i) );

  return txt.str();
}

void tts::FMMCard::setMask(uint32_t mask) {

  check_write_lock();

  _dev()->write( "MASK", mask);
};
  
uint32_t tts::FMMCard::getMask() {

  uint32_t mask;  
  _dev()->read( "MASK", &mask);
  return mask;
};
  
void tts::FMMCard::setThreshold20(uint32_t threshold) {

  check_write_lock();

  _dev()->write( "Synch_thres_20", threshold);
};
  
uint32_t tts::FMMCard::getThreshold20() {

  uint32_t threshold;  
  _dev()->read( "Synch_thres_20", &threshold);
  return threshold;
};
  
void tts::FMMCard::setThreshold10A(uint32_t threshold) {

  check_write_lock();

  _dev()->write( "Synch_thres_10A", threshold);
};
  
uint32_t tts::FMMCard::getThreshold10A() {

  uint32_t threshold;  
  _dev()->read( "Synch_thres_10A", &threshold);
  return threshold;
};
  
void tts::FMMCard::setThreshold10B(uint32_t threshold) {

  check_write_lock();

  _dev()->write( "Synch_thres_10B", threshold);
};
  
uint32_t tts::FMMCard::getThreshold10B() {

  uint32_t threshold;  
  _dev()->read( "Synch_thres_10B", &threshold);
  return threshold;
};
  

void tts::FMMCard::setConfig(tts::FMMConfiguration const& cfg) {

  check_write_lock();

  toggleDualMode( cfg.isDualMode() );
  setMask( cfg.getMask() );
  setThreshold20( cfg.getThreshold20() );
  setThreshold10A( cfg.getThreshold10A() );
  setThreshold10B( cfg.getThreshold10B() );
};

tts::FMMConfiguration tts::FMMCard::readConfig() {

  tts::FMMConfiguration cfg( isDualMode(),
			     getMask(),
			     getThreshold20(),
			     getThreshold10A(),
			     getThreshold10B() );
  
  return cfg;
};



bool tts::FMMCard::isSimuMode() {
  
  uint32_t simu_mode;
  _dev()->read("Test_select", &simu_mode);

  return (simu_mode & 0x1) == 0x1;
}

void tts::FMMCard::toggleSimuMode(bool simu_mode) {

  check_write_lock();


  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("Test_select", simu_mode?0x1:0x0);
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
}

bool tts::FMMCard::isDMAMode() {

  uint32_t dma_mode;
  _dev()->read("History_mode", &dma_mode);

  return (dma_mode & 0x1) == 0x1;
}; 

void tts::FMMCard::toggleDMAMode(bool dma_mode) {

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("History_mode", dma_mode?0x1:0x0);
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();

};

void tts::FMMCard::toggleTimeTagExtResetEnable(bool enabled) {

  check_write_lock();
  
  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("enable_timetag_reset", enabled?0x1:0x0);
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
};

bool tts::FMMCard::timeTagExtResetEnabled() {
  
  uint32_t enabled;
  _dev()->read("enable_timetag_reset", &enabled);

  return (enabled & 0x1) == 0x1;
};

bool tts::FMMCard::isDualMode() {

  uint32_t dual_mode;
  _dev()->read("Dual_mode", &dual_mode);

  return (dual_mode & 0x1) == 0x1;
}; 

void tts::FMMCard::toggleDualMode(bool dual) {

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("Dual_mode", dual?0x1:0x0);
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();

}


void tts::FMMCard::setLEDs(uint32_t led_setting) {

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("leds", led_setting);
  }
  catch(...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
};


tts::TTSState tts::FMMCard::readResultA() {

  uint32_t state;
  _dev()->read( "ResultA", &state);

  return tts::TTSState( (int) state );
};

tts::TTSState tts::FMMCard::readResultB() {

  uint32_t state;
  _dev()->read( "ResultB", &state);

  return tts::TTSState( (int) state );
};


uint32_t tts::FMMCard::readFuncA() {

  uint32_t func;
  _dev()->read( "FUNC_A", &func);

  return (uint32_t) func;
};

uint32_t tts::FMMCard::readFuncB() {

  uint32_t func;
  _dev()->read( "FUNC_B", &func);

  return (uint32_t) func;
};

std::vector<tts::TTSState> tts::FMMCard::getSimulatedInputStates() {

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  _dev()->read("Test_in_ready", &ready);
  _dev()->read("Test_in_busy", &busy);
  _dev()->read("Test_in_synch", &sync);
  _dev()->read("Test_in_overflow", &warn);


  std::vector<tts::TTSState> states(20, 0);

  for (int i=0;i<20;i++) {

    uint32_t state = 0;
    state |= ( (ready & (1<<i)) >> i ) << 3;
    state |= ( (busy  & (1<<i)) >> i ) << 2;
    state |= ( (sync  & (1<<i)) >> i ) << 1;
    state |= ( (warn  & (1<<i)) >> i ) ;
    
    states[i] = state;

  }

  return states;
};

std::vector<tts::TTSState> tts::FMMCard::readInputs() {

  check_write_lock();

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  // 2012-03-20: Found that HAL is not thread-safe. 
  // Need to protect against other thread using another bit in the same register.
  // => ctrl_reg_semaphore

  _ctrl_reg_semaphore.take();
  try {

    _dev()->write("Freeze_input_spy",0x0);
    _dev()->write("Freeze_input_spy",0x1);

    _dev()->read("Input_spy_ready", &ready);
    _dev()->read("Input_spy_busy", &busy);
    _dev()->read("Input_spy_synch", &sync);
    _dev()->read("Input_spy_overflow", &warn);

    _dev()->write("Freeze_input_spy",0x0);


  } catch (...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();

  std::vector<tts::TTSState> states(20, 0);

  for (int i=0;i<20;i++) {

    uint32_t state = 0;
    state |= ( (ready & (1<<i)) >> i ) << 3;
    state |= ( (busy  & (1<<i)) >> i ) << 2;
    state |= ( (sync  & (1<<i)) >> i ) << 1;
    state |= ( (warn  & (1<<i)) >> i ) ;
    
    states[i] = state;
  }

  return states;
};

void tts::FMMCard::setSimulatedInputStates(std::vector <tts::TTSState> const& states) {

  check_write_lock();

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  std::vector<tts::TTSState>::const_iterator it = states.begin();
  for (int i=0; it!= states.end() && i < 20; it++, i++) {
    ready |= ( ((*it) & 0x8) >> 3 ) << i;
    busy  |= ( ((*it) & 0x4) >> 2 ) << i;
    sync  |= ( ((*it) & 0x2) >> 1 ) << i;
    warn  |= ( ((*it) & 0x1)      ) << i;
  }

  _dev()->write("Test_in_ready", ready);
  _dev()->write("Test_in_busy", busy);
  _dev()->write("Test_in_synch", sync);
  _dev()->write("Test_in_overflow", warn);

  _dev()->write("Update_test_reg", 0x1);
};


void tts::FMMCard::setSimulatedInputState(uint32_t channel, tts::TTSState const& state) {

  if (channel >= 20) XCEPT_RAISE(xcept::Exception, "channel out of range");

  check_write_lock();

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  _dev()->read("Test_in_ready", &ready);
  _dev()->read("Test_in_busy", &busy);
  _dev()->read("Test_in_synch", &sync);
  _dev()->read("Test_in_overflow", &warn);

  ready &= ~( ((uint32_t)1) << channel);
  busy  &= ~( ((uint32_t)1) << channel);
  sync  &= ~( ((uint32_t)1) << channel);
  warn  &= ~( ((uint32_t)1) << channel);

  ready |= ( (state & 0x8) >> 3 ) << channel;
  busy  |= ( (state & 0x4) >> 2 ) << channel;
  sync  |= ( (state & 0x2) >> 1 ) << channel;
  warn  |= ( (state & 0x1)      ) << channel;

  _dev()->write("Test_in_ready", ready);
  _dev()->write("Test_in_busy", busy);
  _dev()->write("Test_in_synch", sync);
  _dev()->write("Test_in_overflow", warn);

  _dev()->write("Update_test_reg", 0x1);
};


void tts::FMMCard::enableTestOutputs(uint32_t test_output_enable) {

  check_write_lock();
  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("TestOutput_enable", test_output_enable);
  } catch (...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
}

uint32_t tts::FMMCard::readTestOutputEnables() {
  uint32_t val;
  _dev()->read("TestOutput_enable", &val);

  return val;
}


void tts::FMMCard::setTestOutputValue(uint32_t output_idx, tts::TTSState const& state ) {

  check_write_lock();

  if (output_idx > 3) XCEPT_RAISE(xcept::Exception, "output_idx out of range");

  std::stringstream item;
  item << "OutputTest_value_IO" << 20+output_idx;

  _dev()->write(item.str(), (uint32_t) state);
}

tts::TTSState tts::FMMCard::readTestOutputValue(uint32_t output_idx) {

  if (output_idx > 3) XCEPT_RAISE(xcept::Exception, "output_idx out of range");

  std::stringstream item;
  item << "OutputTest_value_IO" << 20+output_idx;

  uint32_t state;
  _dev()->read(item.str(), &state);

  return tts::TTSState(state);
}

void tts::FMMCard::readHistoryItem(uint32_t addr, tts::FMMHistoryItem& hi) {

  uint32_t hist[4];
  uint32_t val;
  for (uint32_t i=0; i<4; i++) {
    _dev()->read("ZBT", &val, addr * 16 + i * 4);
    hist[i] = (uint32_t) val;
  }

  hi.set(hist);
};

void tts::FMMCard::clearHistoryMemory(uint32_t pattern) {

  check_write_lock();

  HAL::HalVerifyOption verify_flag = HAL::HAL_NO_VERIFY;
  
  for (uint32_t i=0; i < FMMHistMemSize*4; i++)
    _dev()->write("ZBT", pattern, verify_flag, i*4);

}


void tts::FMMCard::readDeadTimeCounters(std::vector <uint32_t>& counters) {

  _timers_semaphore.take();
  try {

    unFreezeTimes();
    freezeTimes();

    doReadDeadTimeCounters(counters);

    unFreezeTimes();
  }
  catch (...) {
    _timers_semaphore.give();
    throw;
  }

  _timers_semaphore.give();

}

uint64_t tts::FMMCard::readTimeTag() {

  uint64_t tt;

  // FIXME: temporary hack for forward compatibility
  if ( readFirmwareRev() < "051221_00" ) 
    return (uint64_t) 0;

  _timers_semaphore.take();

  try {
    unFreezeTimes();
    freezeTimes();

    tt = doReadTimeTag();

    unFreezeTimes();
  }
  catch (...) {
    _timers_semaphore.give();
    throw;
  }

  _timers_semaphore.give();

  return tt;
}

void tts::FMMCard::readDeadTimeCountersAndTimeTag(std::vector <uint32_t>& counters, uint64_t& tt) {



  _timers_semaphore.take();
  try {

    unFreezeTimes();
    freezeTimes();

    doReadDeadTimeCounters(counters);
    tt = doReadTimeTag();

    unFreezeTimes();
  }
  catch (...) {
    _timers_semaphore.give();
    throw;
  }

  _timers_semaphore.give();

}


//--------------------------------------------------------------------------------
// private and protected methods
//--------------------------------------------------------------------------------


void tts::FMMCard::doReadDeadTimeCounters(std::vector <uint32_t>& counters) {

  counters.resize( NumCounters );

  if (readFirmwareRev() < "060515_00") { 

    // old register layout: 
    // registers: 20x busy, 20 x ready
    // std::vector: 22x busy, 22 x ready

    for (uint32_t i=0;i<20;i++) {
      uint32_t retval = 0;
      _dev()->read( "HW_MON_old", &retval, i*4);
      counters[i] = (uint32_t) retval;
      _dev()->read( "HW_MON_old", &retval, (i+20)*4);
      counters[i+22] = (uint32_t) retval;
    }

    for (uint32_t i=0;i<2;i++) {
      counters[i+20] = (uint32_t) 0;
      counters[i+42] = (uint32_t) 0;
    }
    
  }
  else if (readFirmwareRev() < "120119_00"){ 

    // new register layout: 
    // registers: 22x busy, 22 x ready
    // std::vector: 22x busy, 22x ready

    for (uint32_t i=0;i<44;i++) {
      uint32_t retval = 0;
      _dev()->read( "HW_MON_old", &retval, i*4);
      counters[i] = (uint32_t) retval;
    }
  }
  else {
    // 2012 layout
    // 22xbusy, 22xwarning, 22xready, 22xOOS, 22xError 

    //    jal::Timer timer;
    //    char * names[] = { "B", "W", "R", "O", "E" };

    for (uint32_t i=0;i<NumCounters;i++) {
      uint32_t retval = 0;
      //      t[i] = timer.getTicks(); 
      _dev()->read( "HW_MON", &retval, i*4);
      counters[i] = (uint32_t) retval;
    }

//    t[NumCounters] = timer.getTicks(); 

//     for (uint32_t i=0;i<NumCounters;i++) {
//       if ( (i%22) == 0 ) {
// 	std::cout << std::endl << names[i/22] << ":";
//       }
//       std::cout << " " << counters[i] ;
//       std::cout << "(" <<  (((double)(t[i+1]-t[i])) / timer.getClocksPerSec() * 1.e6) << " us), ";      
//     }

//     if (counters[44+18] != counters[44+19])
//       std::cout << "latch error";

//     std::cout << "(" << t[0] << "," << t[NumCounters] << ")" << std::endl << std::flush;

  }

}

uint64_t tts::FMMCard::doReadTimeTag() {

  uint64_t tt;
  uint32_t val;


  // FIXME: temporary hack for forward compatibility
  if ( readFirmwareRev() < "051221_00" ) 
    return (uint64_t) 0;

  _dev()->read("TimeTag_low", &val);
  tt = (uint64_t) val;
  
  _dev()->read("TimeTag_high", &val);
  tt |= ( (uint64_t) val ) << 20;

  return tt;
}

void tts::FMMCard::freezeTimes() {

  check_write_lock();

  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("Freeze_times", 0x1);
  } catch (...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
}  

void tts::FMMCard::unFreezeTimes() {

  check_write_lock();
  _ctrl_reg_semaphore.take();
  try {
    _dev()->write("Freeze_times", 0x0);
  } catch (...) {
    _ctrl_reg_semaphore.give();
    throw;
  }
  _ctrl_reg_semaphore.give();
}  

