// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_ibv_exception_ConnectionRequestFailure_h_
#define _pt_ibv_exception_ConnectionRequestFailure_h_

#include "pt/ibv/exception/Exception.h"

namespace pt
{
	namespace ibv
	{
		namespace exception
		{
			class ConnectionRequestFailure : public pt::ibv::exception::Exception
			{
				public:
					ConnectionRequestFailure (std::string name, std::string message, std::string module, int line, std::string function)
						: pt::ibv::exception::Exception(name, message, module, line, function)
					{
					}

					ConnectionRequestFailure (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
						: pt::ibv::exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
