// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include <stdlib.h>
#include <string.h>

#include "pt/udapl/dat/MemoryRegion.h"
#include "pt/udapl/dat/Utils.h"
#include "pt/udapl/dat/InterfaceAdapter.h"

pt::udapl::dat::MemoryRegion::MemoryRegion (pt::udapl::dat::InterfaceAdapter *ia, unsigned char* buffer, DAT_UINT64 size) 
	: ia_(ia), buffer_(buffer), size_(size)
{
// This to be done in the allocator
//           DAT_UINT64 fullsize = DT_RoundSize(usesize, DAT_OPTIMAL_ALIGNMENT) + DAT_OPTIMAL_ALIGNMENT;
//           reg = new TMemorySpace(fullsize);
//           unsigned char* usebuf = DT_AlignPtr(reg->GetFullBuffer(), DAT_OPTIMAL_ALIGNMENT);
//           reg->SetUsedPart(usebuf, usesize);

	reg_addr_ = 0;
	reg_size_ = 0;

	lmr_handle_ = DAT_HANDLE_NULL;
	lmr_context_ = 0;
	rmr_context_ = 0;

	DAT_REGION_DESCRIPTION rd;
	DAT_RETURN ret;

	::memset(&rd, 0, sizeof(rd));
	rd.for_va = buffer;

	ret = dat_lmr_create(ia->ia_handle_, DAT_MEM_TYPE_VIRTUAL, rd, size, ia_->pz_handle_, DAT_MEM_PRIV_ALL_FLAG, DAT_VA_TYPE_VA, &lmr_handle_, &lmr_context_, &rmr_context_, &reg_size_, &reg_addr_);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to open interface " << ia->name_ << " with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}

}

pt::udapl::dat::MemoryRegion::~MemoryRegion ()
{
	DAT_RETURN ret;

	ret = dat_lmr_free(lmr_handle_);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to free memory region for IA with code:" << pt::udapl::dat::errorToString(ret);
	}
}
