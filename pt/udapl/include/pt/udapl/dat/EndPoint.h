// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_EndPoint_h_
#define _pt_udapl_dat_EndPoint_h_

#include <iostream>
#include <string>

#include "pt/udapl/dat/Utils.h"

#include "pt/udapl/exception/Exception.h"
#include "pt/udapl/exception/InsufficientResources.h"
#include "pt/udapl/dat/InterfaceAdapter.h"
#include "toolbox/BSem.h"
#include "toolbox/rlist.h"

namespace pt
{
	namespace udapl
	{
		namespace dat
		{
			typedef struct
			{
					pt::udapl::dat::MemoryRegion * mr;
					DAT_DTO_COOKIE cookie;
					DAT_LMR_TRIPLET * local_iov;
					DAT_UINT64 size;
			} Descriptor;

			class EndPoint
			{

				public:

					EndPoint (pt::udapl::dat::InterfaceAdapter * ia) ;

					virtual ~EndPoint ();

					void postSendBuffer (pt::udapl::dat::MemoryRegion * mr, DAT_DTO_COOKIE & cookie, DAT_LMR_TRIPLET * local_iov, DAT_UINT64 size) ;

					void postRecvBuffer (pt::udapl::dat::MemoryRegion * mr, DAT_DTO_COOKIE & cookie, DAT_LMR_TRIPLET * local_iov, DAT_UINT64 size) ;

					void connect (const std::string & destination, size_t port) ;

					void disconnect () ;

					void incrementCredits () ;

					void getStatus (DAT_EP_STATE * ep_state, DAT_BOOLEAN * recv_idle, DAT_BOOLEAN * request_idle) ;

					/*
					 friend std::ostream &operator<<(std::ostream &cout, EndPoint  ep);
					 */
				public:

					toolbox::BSem mutex_;
					DAT_EP_HANDLE ep_handle_;
					InterfaceAdapter * ia_;
					size_t credits_; // credits for sender loop
					//DAT_EP_ATTR     ep_attr_;
					toolbox::rlist<Descriptor> * urgentQueue_;

			};
		}
	}
}

#endif
