// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_ReceiverLoop_h_
#define _pt_udapl_dat_ReceiverLoop_h_

#include <iostream>
#include <string>

#include "pt/udapl/exception/Exception.h"
#include "toolbox/lang/Class.h"
#include "toolbox/task/PollingWorkLoop.h"
#include "i2o/Listener.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"

namespace pt
{
	namespace udapl
	{
		class ReceiverLoop : public toolbox::lang::Class
		{
			public:

				ReceiverLoop (const std::string & name, i2o::Listener* listener) ;

				virtual ~ReceiverLoop ();

				bool process (toolbox::task::WorkLoop* wl);

			protected:

				toolbox::task::WorkLoop* workLoop_;
				toolbox::task::ActionSignature* process_;
				std::string name_;

			public:
				i2o::Listener* listener_;
				toolbox::squeue<toolbox::mem::Reference*> dispatchQueue_;
		};
	}
}

#endif
