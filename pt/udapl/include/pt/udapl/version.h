// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _ptudapl_version_h_
#define _ptudapl_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_PTUDAPL_VERSION_MAJOR 3
#define WORKSUITE_PTUDAPL_VERSION_MINOR 1
#define WORKSUITE_PTUDAPL_VERSION_PATCH 2
// If any previous versions available E.g. #define WORKSUITE_PTUDAPL_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_PTUDAPL_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_PTUDAPL_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_PTUDAPL_VERSION_MAJOR,WORKSUITE_PTUDAPL_VERSION_MINOR,WORKSUITE_PTUDAPL_VERSION_PATCH)
#ifndef WORKSUITE_PTUDAPL_PREVIOUS_VERSIONS
#define WORKSUITE_PTUDAPL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_PTUDAPL_VERSION_MAJOR,WORKSUITE_PTUDAPL_VERSION_MINOR,WORKSUITE_PTUDAPL_VERSION_PATCH)
#else 
#define WORKSUITE_PTUDAPL_FULL_VERSION_LIST  WORKSUITE_PTUDAPL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_PTUDAPL_VERSION_MAJOR,WORKSUITE_PTUDAPL_VERSION_MINOR,WORKSUITE_PTUDAPL_VERSION_PATCH)
#endif 
namespace ptudapl
{
	const std::string project = "worksuite";
    const std::string package  =  "ptudapl";
    const std::string versions = WORKSUITE_PTUDAPL_FULL_VERSION_LIST;
    const std::string summary = "uDAPL peer transport with I2O service implementation";
    const std::string description = "XDAQ I2O peer transport based on User Direct Access Programming Library (uDAPL), this is a re-factorized version of the originally created peer transport implementation on 3/2005  by J.Adamczewski, S.Linev, EE, GSI, Darmstadt";
    const std::string authors = "Luciano Orsini, Andrea Petrucci";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

