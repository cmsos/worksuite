// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "pt/vpi/EstablishedConnection.h"
#include "pt/vpi/PeerTransport.h"
#include "toolbox/rlist.h"

pt::vpi::EstablishedConnection::EstablishedConnection(pt::PeerTransport * pt, std::shared_ptr<void> & ch, std::shared_ptr<void> &  context, const std::string & network): pt_(pt), ch_(ch), context_(context), network_(network)
{	
}

pt::vpi::EstablishedConnection::~EstablishedConnection()
{
}

std::shared_ptr<void> pt::vpi::EstablishedConnection::getConnectionHandle()
{
    return ch_;
}

void *  pt::vpi::EstablishedConnection::getContext()
{
    return context_.get();
}

pt::PeerTransport * pt::vpi::EstablishedConnection::getPeerTransport()
{
    return pt_;
}

std::string pt::vpi::EstablishedConnection::getNetwork()
{
    return network_;
}


