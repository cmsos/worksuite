// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "pt/vpi/Allocator.h"

#include "pt/vpi/HeaderInfo.h"
#include "pt/vpi/exception/Exception.h"

#include "toolbox/PolicyFactory.h"
#include "toolbox/AllocPolicy.h"

#include <sstream>
#include <string>
#include <limits>

#include "toolbox/string.h"

pt::vpi::Allocator::Allocator (const std::string & name, size_t committedSize): toolbox::mem::CommittedHeapAllocator (name, committedSize)
{
}

pt::vpi::Allocator::~Allocator ()
{
}

size_t pt::vpi::Allocator::getOffset ()
{
        return pt::vpi::HeaderInfo::getHeaderSize();
}
std::string pt::vpi::Allocator::type ()
{
	return "pipe";
}

