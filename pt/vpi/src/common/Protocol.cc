// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "pt/vpi/Protocol.h"
#include "pt/vpi/Address.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>

#include <errno.h>
#include <string.h>
#include <sstream>

#include "pt/exception/Exception.h"
#include "toolbox/string.h"
#include "toolbox/hexdump.h"

pt::vpi::Channel::Channel (int connfd, nlohmann::json & cookie,  const std::string  & originatorhost, const std::string & destinationhost, unsigned int destinationport, const std::string & ip)
    :sockfd_(connfd), cookie_(cookie), originatorhost_(originatorhost), destinationhost_(destinationhost), destinationport_(destinationport),ip_(ip)
{
}

pt::vpi::Channel::~Channel()
{
    if (sockfd_ != -1)
    {
        ::close(sockfd_);
    }
}

std::string pt::vpi::Channel::getDestinationHost()
{
    return destinationhost_;
}

std::string pt::vpi::Channel::getOriginatorHost()
{
    return originatorhost_;
}

unsigned int pt::vpi::Channel::getDestinationPort()
{
    return destinationport_;
}

std::string pt::vpi::Channel::getDestinationIP()
{
    return ip_;
}

nlohmann::json pt::vpi::Channel::getCookie()
{
    return cookie_;
}

nlohmann::json pt::vpi::Channel::receive()
{
    // receive prefix length of message in bytes
    uint32_t length;
    uint32_t network_byte_order;
    
    size_t rlen = nreceive((char*)&network_byte_order,sizeof(length));
    if (rlen != sizeof(length))
    {
        std::stringstream ss;
        ss << "Receive error on " <<  originatorhost_ << " during nreceive from " << destinationhost_ << ", expected size " << length << " received " << rlen << " bytes";
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
    length = ntohl(network_byte_order);
    
    // receive payload
    std::vector<uint8_t> payload(length);
    rlen = nreceive((char*)(payload.data()),length);
    if (rlen != length)
    {
        std::stringstream ss;
        ss << "Receive error on " <<  originatorhost_ << " during nreceive from " << destinationhost_ << ", expected size " << length << " received " << rlen << " bytes";
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
    nlohmann::json document = nlohmann::json::from_bson(payload);
    return document;
}

void pt::vpi::Channel::send(nlohmann::json & document)
{
    std::vector<std::uint8_t> payload = nlohmann::json::to_bson(document);
    
    uint32_t length = payload.size() * sizeof (std::uint8_t);
    uint32_t network_byte_order = htonl(length);
    // send prefix length of message in bytes
    if (write(sockfd_, (char*)&network_byte_order, sizeof(length)) != sizeof(length))
    {
        std::stringstream ss;
        ss << "Failed to send from " << originatorhost_ << " to send to server:port '" << destinationhost_ << ":" << destinationport_ << ", with error " << strerror(errno);
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
    // send payload
    if (write(sockfd_, (char*)(payload.data()), length) != length )
    {
        std::stringstream ss;
        ss << "Failed to send from " << originatorhost_ << " to server:port '" << destinationhost_ << ":" << destinationport_ << ", with error " << strerror(errno);
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
}


size_t pt::vpi::Channel::nreceive (char* buf, size_t len)
{
    size_t totalBytes = 0;
    size_t nBytes;
    size_t size = len;
    
    do
    {
        try
        {
            nBytes = size - totalBytes; // missing bytes to read
            totalBytes += this->receive(&buf[totalBytes], nBytes);
        }
        catch (pt::exception::Exception& e)
        {
            
            //std::cout << "Receive error for a total of " << len << ", already received: " << totalBytes << std::endl;
            //toolbox::hexdump ((void*) buf, totalBytes);
            //std::cout << std::endl;
            
            std::stringstream msg;
            msg << "Receive error on " <<  originatorhost_ << " during read from " << destinationhost_ << ", for a total of " << len << " bytes";
            XCEPT_RETHROW (pt::exception::Exception, msg.str(), e);
        }
    }
    while (totalBytes < size );
    return totalBytes;
}

size_t pt::vpi::Channel::receive(char * buf, size_t len )
{
    int length;
    errno = 0;
    length = ::recv(sockfd_, buf, len, 0);
    
    if (length == -1)
    {
        if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
        {
            // TIMEOUT
            std::stringstream msg;
            msg << "Timeout when receiving " << len << " bytes";
            XCEPT_RAISE(pt::exception::Exception, msg.str() );
        }
        else if (errno == ECONNRESET)
        {
            // Connection reset by peer
            std::stringstream msg;
            msg << "Failed to receive, with error " << strerror(errno);
            XCEPT_RAISE(pt::exception::Exception, msg.str() );
        }
        else
        {
            std::stringstream msg;
            msg << "Failed to receive, with error " << strerror(errno);
            XCEPT_RAISE(pt::exception::Exception, msg.str() );
        }
    }
    if ( length == 0 )
    {
        // Connection close by peer
        std::stringstream msg;
        msg << "Connection reset by peer ";
        XCEPT_RAISE(pt::exception::Exception, msg.str());
    }
    
    
    return length;
}



void pt::vpi::Channel::close ()
{
    ::close(sockfd_);
    sockfd_ = -1;
}


//void exchange(qp);





pt::vpi::Connector::Connector (nlohmann::json & cookie):cookie_(cookie)
{}

pt::vpi::Connector::~Connector()
{
}


std::shared_ptr<pt::vpi::Channel> pt::vpi::Connector::connect ( const std::string & hostname, unsigned int port)
{
    struct addrinfo *res, *t;
    struct addrinfo hints;
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    
    int sockfd = -1;
    
    int n = getaddrinfo(hostname.c_str(), toolbox::toString("%d",port).c_str(), &hints, &res);
    
    if (n < 0)
    {
        std::stringstream ss;
        ss << "Failed to get address info for server:port '" << hostname << ":" << port << ", " << gai_strerror(n);
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
    for (t = res; t; t = t->ai_next)
    {
        errno = 0;
        sockfd = socket(t->ai_family, t->ai_socktype, t->ai_protocol);
        if (sockfd < 0)
        {
            std::stringstream ss;
            ss << "Failed to create socket to server:port '" << hostname << ":" << port << ", " << strerror(errno);
            XCEPT_RAISE(pt::exception::Exception, ss.str());
        }
        
        errno = 0;
        int ret = ::connect(sockfd, t->ai_addr, t->ai_addrlen);
        if (ret < 0)
        {
            ::close(sockfd);
            std::stringstream ss;
            ss << "Failed to connect socket to server:port '" << hostname << ":" << port << ", " << strerror(errno);
            XCEPT_RAISE(pt::exception::Exception, ss.str());
        }
        break;
    }
    
    freeaddrinfo(res);
    
    return std::make_shared<pt::vpi::Channel>(sockfd, cookie_, "unknown", hostname, port, "unknown");
    
}



pt::vpi::Acceptor::Acceptor ( ActionListener * listener, nlohmann::json & cookie): listenfd_(-1), listener_(listener),  originatorhost_(""), destinationport_(0),cookie_(cookie)
{
}

pt::vpi::Acceptor::~Acceptor()
{
    if ( listenfd_ != -1)
        ::close(listenfd_);
}

void pt::vpi::Acceptor::listen (const std::string & hostname, unsigned int port, int num)
{
    originatorhost_ = hostname;
    destinationport_ = port;
    
    struct addrinfo *res, *t;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    
    int ret;
    
    ret = getaddrinfo(hostname.c_str(), toolbox::toString("%d",port).c_str(), &hints, &res);
    
    if (ret < 0)
    {
        std::stringstream ss;
        ss << "Failed to get address info for host:port '" << hostname << ":" << port << ", " << gai_strerror(ret);
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
    for (t = res; t; t = t->ai_next)
    {
        errno = 0;
        listenfd_ = socket(t->ai_family, t->ai_socktype, t->ai_protocol);
        if (listenfd_ < 0)
        {
            std::stringstream ss;
            ss << "Failed to create socket on host:port '" << hostname << ":" << port << ", " << strerror(errno);
            XCEPT_RAISE(pt::exception::Exception, ss.str());
        }
        
        errno = 0;
        int n = 1;
        int ret = setsockopt(listenfd_, SOL_SOCKET, SO_REUSEADDR, &n, sizeof n);
        if (ret < 0)
        {
            ::close(listenfd_);
            std::stringstream ss;
            ss << "Failed to set socket options on host:port '" << hostname << ":" << port << ", " << strerror(errno);
            XCEPT_RAISE(pt::exception::Exception, ss.str());
        }
        
        errno = 0;
        ret = bind(listenfd_, t->ai_addr, t->ai_addrlen);
        if (ret != 0)
        {
            ::close(listenfd_);
            std::stringstream ss;
            ss << "Failed to bind socket on host:port '" << hostname << ":" << port << ", " << strerror(errno);
            XCEPT_RAISE(pt::exception::Exception, ss.str());
        }
        break;
    }
    
    freeaddrinfo(res);
    
    ret = ::listen(listenfd_, num);
    if (ret != 0)
    {
        ::close(listenfd_);
        std::stringstream ss;
        ss << "Failed to listen on host:port '" << hostname << ":" << port << ", " << strerror(errno);
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
}

void pt::vpi::Acceptor::accept ()
{
    errno = 0;
    
    struct sockaddr_in readAddr;
    socklen_t readAddrLen = sizeof(readAddr);
    
    int connfd = ::accept(listenfd_, (struct sockaddr *) &readAddr, &readAddrLen);
    
    if (connfd < 0)
    {
        std::stringstream ss;
        ss << "Failed to accept on " << "TBD name" << ", " << strerror(errno) << "";
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
    std::string ip;
    ip = inet_ntoa(readAddr.sin_addr);
    struct hostent * h = gethostbyaddr((char*) &readAddr.sin_addr.s_addr, sizeof(readAddr.sin_addr.s_addr), AF_INET);
    std::string remoteHostName;
    if (h != 0)
    {
        remoteHostName = h->h_name;
    }
    else
    {
        remoteHostName = "";
    }
    
    listener_->actionPerformed(std::make_shared<pt::vpi::Channel>(connfd, cookie_,originatorhost_, remoteHostName, destinationport_,ip));
}



void pt::vpi::Channel::exchange_with_client(ibvla::QueuePair &qp, enum ibv_mtu mtu, int sgid_index, size_t ibPath, int is_global, int sl, int traffic_class, const std::string & network)
{
    
    
    auto request = this->receive();
    std::cout << "on server request " << request.dump(4) << std::endl;

    int rlid;
    int rpsn;
    int rqpn;
    union ibv_gid rgid;

    rlid = request["lid"];
    rqpn = request["qpn"];
    rpsn = request["psn"];
    rgid.global.subnet_prefix = request["gid.global.subnet_prefix"];
    rgid.global.interface_id = request["gid.global.interface_id"];
    
    // Query qp to check that it is in the correct state //, and that the port has been set
    struct ibv_qp_attr qp_attr;
    struct ibv_qp_init_attr qp_init_attr;
    int querymask = IBV_QP_PORT | IBV_QP_STATE;
    try
    {
        qp.query(querymask, qp_attr, qp_init_attr);
    }
    catch (ibvla::exception::Exception & e)
    {
        XCEPT_RETHROW(pt::exception::Exception, "Failed to query queue pair, cannot connect queue pair to remote", e);
    }
    
    if (qp_attr.qp_state != IBV_QPS_INIT)
    {
        std::stringstream ss;
        ss << "Queue pair not in initialized state, cannot connect to remote QP '"; // << id << "';
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    

    // Move the QP into RTR state
    ibv_qp_attr attr;
    memset(&attr, 0, sizeof(attr));
    
    attr.ah_attr.dlid = rlid;
    attr.dest_qp_num = rqpn;
    attr.rq_psn = rpsn;
    attr.qp_state = IBV_QPS_RTR;
    attr.path_mtu = mtu;
    attr.max_dest_rd_atomic = 1;
    attr.min_rnr_timer = 0;
    
    attr.ah_attr.sl = sl;
    
    if ( is_global)
    {
        // RoCE uses sgid_index_
        // For RoCE:
        attr.ah_attr.is_global  = 1;
        attr.ah_attr.grh.dgid = rgid;
        attr.ah_attr.grh.sgid_index = sgid_index;
        attr.ah_attr.grh.hop_limit = 2;
        attr.ah_attr.grh.traffic_class = traffic_class;
    }
    else
    {
        attr.ah_attr.is_global = 0;
    }
    
    attr.ah_attr.src_path_bits = ibPath;
    // should have been set during transition from RESET to INIT
    attr.ah_attr.port_num = qp_attr.port_num;
    
    try
    {
        qp.modify(attr, ibv_qp_attr_mask(IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC | IBV_QP_MIN_RNR_TIMER));
    }
    catch (ibvla::exception::Exception & e)
    {
        std::stringstream ss;
        ss << "Failed to move local QP into RTR state";
        XCEPT_RETHROW(pt::exception::Exception, ss.str(), e);
    }
    
    // Communicate information back to remote to complete connection
    // get port attributes
    struct ibv_port_attr p_att = qp.pd_.context_.queryPort(qp_attr.port_num);
    
    
    nlohmann::json response;
    if ( is_global)
    {
        union ibv_gid gid = qp.pd_.context_.queryGID(qp_attr.port_num, sgid_index);
        response = {
                     {"lid",(p_att.lid + (unsigned int)ibPath)},
                     {"qpn",qp.getNum()},
                     {"psn",attr.sq_psn},
                     {"gid.global.subnet_prefix",gid.global.subnet_prefix},
                     {"gid.global.interface_id",gid.global.interface_id}
                   };
        
        std::cout << "on server response " << response.dump(4) << std::endl;
    }
    else
    {
        response = {
                     {"lid",(p_att.lid + (unsigned int)ibPath)},
                     {"qpn",qp.getNum()},
                     {"psn",attr.sq_psn},
                     {"gid.global.subnet_prefix",0},
                     {"gid.global.interface_id",0}};
        
        std::cout << "on server response " << response.dump(4) << std::endl;
    }
    
    this->send(response);
    nlohmann::json done = this->receive();
    std::cout << "on server done " << done.dump(4) << std::endl;
    this->close();
}


void pt::vpi::Channel::exchange_with_server(ibvla::QueuePair &qp, enum ibv_mtu mtu, int sgid_index, size_t ibPath, int is_global, int sl, int traffic_class, const std::string & network, bool sendWithTimeout)
{
    
    int psn = lrand48() & 0xffffff; // Packet Sequence Number
    
    // get port attributes
    struct ibv_qp_attr qp_attr;
    struct ibv_qp_init_attr qp_init_attr;
    int querymask = IBV_QP_PORT | IBV_QP_STATE;
    try
    {
        qp.query(querymask, qp_attr, qp_init_attr);
    }
    catch (ibvla::exception::Exception & e)
    {
        XCEPT_RETHROW(pt::exception::Exception, "Failed to query queue pair, cannot connect queue pair to remote", e);
    }
    
    if (qp_attr.qp_state != IBV_QPS_INIT)
    {
        std::stringstream ss;
        ss << "Queue pair not in initialized state, cannot connect to remote QP '"; // << id << "';
        XCEPT_RAISE(pt::exception::Exception, ss.str());
    }
    
    struct ibv_port_attr p_att = qp.pd_.context_.queryPort(qp_attr.port_num);
    
    nlohmann::json request;
    
    if ( is_global )
    {
        // RoCE uses sgid_index_
        union ibv_gid gid = qp.pd_.context_.queryGID(qp_attr.port_num, sgid_index);
        
        request = {
                    {"lid",(p_att.lid + (unsigned int)ibPath)},
                    {"qpn",qp.getNum()},
                    {"psn",psn},
                    {"gid.global.subnet_prefix",gid.global.subnet_prefix},
                    {"gid.global.interface_id",gid.global.interface_id}
                  };
        
        std::cout << "on client request " << request.dump(4) << std::endl;
        
    }
    else
    {
        //sprintf(msg, "%04x:%06x:%06x:%016lx:%016lx", (p_att.lid + (unsigned int)ibPath), qp.getNum(), psn, (long unsigned int)0, (long unsigned int)0);
        request = {
                    {"lid",(p_att.lid + (unsigned int)ibPath)},
                    {"qpn",qp.getNum()},
                    {"psn",psn},
                    {"gid.global.subnet_prefix",0},
                    {"gid.global.interface_id",0 }
                  };
        
        std::cout << "on client request " << request.dump(4) << std::endl;
    }
    
    this->send(request);
    nlohmann::json response = this->receive();
    
    std::cout << "on client response " << response.dump(4) << std::endl;

    ibv_qp_attr attr;
    memset(&attr, 0, sizeof(attr));
    
    int r_lid, r_qpn, r_psn;
    union ibv_gid r_gid;
    
    r_lid = response["lid"];
    r_qpn = response["qpn"];
    r_psn = response["psn"];
    r_gid.global.subnet_prefix = response["gid.global.subnet_prefix"];
    r_gid.global.interface_id = response["gid.global.interface_id"];
    
    //std::cout << "Received connection info from remote address, lid='" << r_lid << "', qpn='" << r_qpn << "', psn='" << r_psn << std::endl;
    
    attr.ah_attr.dlid = r_lid;
    attr.dest_qp_num = r_qpn;
    attr.rq_psn = r_psn;
    attr.qp_state = IBV_QPS_RTR;
    attr.path_mtu = mtu;
    attr.max_dest_rd_atomic = 1;
    // min_rnr_timer == receiver side, time to wait for receive buffer before sending RNR NAK
    attr.min_rnr_timer = 0; // 655.36 ms (0.6 seconds)
    attr.ah_attr.sl = sl;
    // For RoCE:
    if ( is_global )
    {
        attr.ah_attr.is_global  = 1;
        attr.ah_attr.grh.dgid = r_gid;
        attr.ah_attr.grh.sgid_index = sgid_index;
        attr.ah_attr.grh.hop_limit = 2;
        attr.ah_attr.grh.traffic_class = 0;
    }
    else
    {
        attr.ah_attr.is_global = 0;
    }
    
    attr.ah_attr.src_path_bits =  ibPath;
    //std::cout << "setting (send) src_path to " << ibPath << std::endl;
    //should have been set during transition from RESET to INIT
    attr.ah_attr.port_num = qp_attr.port_num;
    
    try
    {
        qp.modify(attr, ibv_qp_attr_mask(IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC | IBV_QP_MIN_RNR_TIMER));
    }
    catch (ibvla::exception::Exception & e)
    {
        std::stringstream ss;
        ss << "Failed to move local QP into RTR state";
        XCEPT_RETHROW(pt::exception::Exception, ss.str(), e);
    }
    
    attr.qp_state = IBV_QPS_RTS;
    // timeout(18) and retry_cnt(7) is a timeout of roughly 15 seconds
    // sender side, timeout == timeout for waiting for an ACK/NACK
    attr.timeout = 18;
    // sender side, retry for timeout, 3 bit
    attr.retry_cnt = 7;
    
    // sender side, rnr_retry, 7 == infinite, 3 bit
    if (sendWithTimeout)
    {
        attr.rnr_retry = 6;
    }
    else
    {
        attr.rnr_retry = 7;
    }
    
    attr.sq_psn = psn;
    attr.max_rd_atomic = 1;
    
    try
    {
        qp.modify(attr, ibv_qp_attr_mask(IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT | IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC));
    }
    catch (ibvla::exception::Exception & e)
    {
        std::stringstream ss;
        ss << "Failed to move local QP into RTS state";
        XCEPT_RETHROW(ibvla::exception::Exception, ss.str(), e);
    }
    
    nlohmann::json done = {{"done",true}};
    this->send(done);
    this->close();
}



