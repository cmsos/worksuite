// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */
#include "pt/vpi/qpInfo.h"
#include "pt/vpi/Output.h"
#include "pt/vpi/PeerTransport.h"
#include "pt/vpi/exception/InternalError.h"
#include "pt/vpi/exception/InsufficientResources.h"
#include "pt/pipe/EstablishedConnection.h"

#include "toolbox/hexdump.h"
#include "toolbox/net/UUID.h"

#include "ibvla/Context.h"
#include "ibvla/QueuePair.h"
#include "ibvla/CompletionQueue.h"
#include "ibvla/Utils.h"
#include "ibvla/Buffer.h"
#include "pt/vpi/Support.h"


pt::vpi::Output::Output(std::shared_ptr<pt::pipe::EstablishedConnection> & pec, pt::pipe::OutputListener * listener, pt::vpi::PeerTransport * pt):  pec_(pec), listener_(listener), pt_(pt)
{
}

pt::pipe::OutputListener *  pt::vpi::Output::getOutputListener()
{
    return listener_;
}

// Remote pipe

pt::vpi::ROutput::ROutput(std::shared_ptr<pt::pipe::EstablishedConnection> & pec, pt::pipe::OutputListener * listener, pt::vpi::PeerTransport * pt,size_t n):  pt::vpi::Output::Output(pec,listener,pt)
{
    pt::vpi::RSupport * ch = (pt::vpi::RSupport *)pec_->getConnectionHandle().get();
    
    nlohmann::json cookie = ch->channel->getCookie();

    enum ibv_mtu mtu = ibvla::mtu_to_enum(cookie["mtu"]);
    int sgid_index  = cookie["sgid_index"];
    int traffic_class  = cookie["traffic_class"];
    size_t ibPath  = cookie["path"];
    int is_global  = cookie["is_global"];
    int sl  = cookie["sl"];
    int ibPort = cookie["ibport"];
    std::string network  = cookie["network"];
    bool sendwithtimeout = cookie["sendwithtimeout"];
    
    try
    {
    	//std::lock_guard<std::mutex> guard(pt->lock_);
   
     	cqs_ = ch->context.createCompletionQueue(n, 0, 0);
    }
    catch (ibvla::exception::Exception & e)
    {
        XCEPT_RETHROW(pt::exception::Exception, "failure in creating send cq for output pipe", e);
    }
    try
    {
     	cqr_ = ch->context.createCompletionQueue(1, 0, 0);
    }
    catch (ibvla::exception::Exception & e)
    {
        XCEPT_RETHROW(pt::exception::Exception, "failure in creating recv cq for output", e);
    }
    
    try
    {
     	qp_ = ch->pd.createQueuePair(cqs_, cqr_, n, 0, new struct qpInfo(cookie["remoteip"], ch->channel->getDestinationHost(), toolbox::toString("%d",ch->channel->getDestinationPort()), network, n, n, 1));
    }
    catch (ibvla::exception::Exception & e)
    {
        XCEPT_RETHROW(pt::exception::Exception, "failure in creating qp for output", e);
    }
    
    try
    {
    	// init QP
    	ibv_qp_attr qpattr;
    	memset(&qpattr, 0, sizeof(qpattr));
    	
    	qpattr.qp_state = IBV_QPS_INIT;
    	qpattr.pkey_index = 0;
    	qpattr.port_num = ibPort; // physical port number
    	qpattr.qp_access_flags = 0;
    
        qp_.modify(qpattr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
    }
    catch (ibvla::exception::Exception & e)
    {
        XCEPT_RETHROW(pt::exception::Exception, "failure initializing", e);
    }
    
    
    if (qp_.getState() != IBV_QPS_INIT)
    {
        XCEPT_RAISE(pt::vpi::exception::Exception, "Invalid QP state");
    }
    
    struct pt::vpi::qpInfo * info = ((struct pt::vpi::qpInfo *) qp_.getContext());
    info->startConnectionTime = toolbox::TimeVal::gettimeofday() ;

    
   /* try
    {
        this->remoteconnect(qp, daddress.getHost(), daddress.getPortNum(), laddress.getIBPath(), laddress.getSGIDIndex(),  laddress.getIsGlobal(),  laddress.getSL(), laddress.getTrafficClass(), network);
    }
    catch (pt::exception::Exception & e)
    {
        std::stringstream ss;
        ss << "fail to connect queue pair with the following configuration for " <<  " local sgidindex " << laddress.getSGIDIndex() << " local isglobal " <<  laddress.getIsGlobal() << " and remote sgidindex " << daddress.getSGIDIndex() << " remote isglobal " << daddress.getIsGlobal() << std::endl;
        XCEPT_DECLARE_NESTED(pt::vpi::exception::Exception, ex, ss.str(), e);
        std::shared_ptr<pt::pipe::ConnectionError> pce = std::make_shared<pt::pipe::ConnectionError>(user_context, ex);
        std::thread([listener,pce](){ listener->actionPerformed(pce); }).detach();
        return;
    }
    */
    	ch->channel->exchange_with_server(qp_, mtu, sgid_index, ibPath, is_global,  sl,  traffic_class, network, sendwithtimeout);
    
    	ch->channel.reset();
    

    try
    {
        struct ibv_qp_attr attr;
        struct ibv_qp_init_attr init_attr;
        
        qp_.query(0, attr, init_attr);
        
        ((struct qpInfo *) qp_.getContext())->remote_qpn = attr.dest_qp_num;
    }
    catch (ibvla::exception::Exception & e)
    {
        pt->moveQueuePairIntoError(qp_);
        XCEPT_RETHROW(pt::exception::Exception, "Failed to query QP", e);
    }
    
    info->endConnectionTime = toolbox::TimeVal::gettimeofday() ;
    
    
    toolbox::net::UUID identifier;
    wclist_ = toolbox::rlist<toolbox::mem::Reference*>::create("pt-vpi-output-wclist-" + identifier.toString() , cqs_.size());
    
}

pt::vpi::ROutput::~ROutput()
{
    // empty remaining frames in rlist
    while( ! wclist_->empty() )
    {
        toolbox::mem::Reference * ref = wclist_->front();
        wclist_->pop_front();
        ref->release();
    }
    toolbox::rlist<toolbox::mem::Reference*>::destroy(wclist_);
    
    pt_->destroyQueuePair(qp_);
    pt_->destroyCompletionQueue(cqs_);
    pt_->destroyCompletionQueue(cqr_);
}

void pt::vpi::ROutput::postFrame(toolbox::mem::Reference * ref )
{
    if (qp_.getState() == IBV_QPS_ERR || qp_.getState() == IBV_QPS_RESET)
    {
        
        std::stringstream ss;
        ss << "Attempting to send with QueuePair in " << ibvla::stateToString(qp_) << " state";
        
        XCEPT_RAISE(pt::vpi::exception::Exception, ss.str());
    }
    
    if (ref == 0)
    {
        XCEPT_RAISE(pt::vpi::exception::InternalError, "Failed to post send, null reference");
    }
    
    if (ref->getNextReference() != 0)
    {
        XCEPT_RAISE(pt::vpi::exception::InternalError, "Failed to post send, reference chains not supported");
    }
    
    
    size_t size = ref->getDataSize();
    
    /*
     if ( app->maxMessageSizeCheck_ )
     {
     if (size > app->maxMessageSize_)
     {
     std::stringstream ss;
     ss << "Failed to post send, message size '" << size << "' greater than max allowed (" << app->maxMessageSize_ << ")";
     XCEPT_RAISE(pt::vpi::exception::InternalError, ss.str());
     }
     }
     */
    
    ibvla::Buffer * buffer = 0;
    try
    {
        buffer = dynamic_cast<ibvla::Buffer*>(ref->getBuffer());
        if (buffer == 0)
        {
            XCEPT_RAISE(pt::vpi::exception::InternalError, "Failed to cast, invalid memory allocator/pool, expected ibvla memory pool");
        }
    }
    catch (std::bad_cast & e)
    {
        XCEPT_RAISE(pt::vpi::exception::InternalError, e.what());
    }
    
    memset(&(buffer->send_wr), 0, sizeof(buffer->send_wr));
    
    buffer->op_sge_list = buffer->buffer_sge_list;
    buffer->op_sge_list.addr = (uint64_t) ref->getDataLocation();
    buffer->op_sge_list.length = size;
    
    buffer->send_wr.wr_id = (uint64_t) ref; // cookie
    buffer->send_wr.sg_list = &(buffer->op_sge_list);
    buffer->send_wr.num_sge = 1;
    buffer->send_wr.opcode = IBV_WR_SEND;
    buffer->send_wr.send_flags = IBV_SEND_SIGNALED;
    buffer->send_wr.next = 0;
    buffer->opcode = ibvla::SEND_OP;
    buffer->qp_ = qp_;
    
    try
    {
        //std::cout << "Sending message size == " << ref->getDataSize() << ", buffer->sge_list.length == " << buffer->op_sge_list.length << std::endl;
        qp_.postSend(buffer->send_wr);
    }
    catch (ibvla::exception::QueueFull & e)
    {
        std::stringstream ss;
        ss << "Failed to post message for sending";
        XCEPT_RETHROW(pt::vpi::exception::InsufficientResources, ss.str(), e);
    }
    catch (ibvla::exception::InternalError & e)
    {
        std::stringstream ss;
        ss << "Failed to post message for sending";
        XCEPT_RETHROW(pt::vpi::exception::InternalError, ss.str(), e);
    }
    
    outstandingRequests_++;
    
}

bool pt::vpi::ROutput::empty()
{
    struct ibv_wc wc;
    int ret;
    
    try
    {
        ret = cqs_.poll(1, &wc);
    }
    catch (ibvla::exception::Exception & e)
    {
        std::stringstream ss;
        ss << "Failed process event queue '" << e.what() << "'";
        std::cout << ss.str() << std::endl;
        return true;
    }
    if (ret == 0)
    {
        return true;
    }
    
    if (wc.status != IBV_WC_SUCCESS)
    {
        pt_->handleWorkRequestError(&wc);
        return true;
    }
    
    if (wc.opcode == IBV_WC_SEND)
    {
        outstandingRequests_--;
        //totalSent_++;
        
        toolbox::mem::Reference * ref = reinterpret_cast<toolbox::mem::Reference *>(wc.wr_id);
        ibvla::Buffer * inbuffer = dynamic_cast<ibvla::Buffer*>(ref->getBuffer());
        ibvla::QueuePair qp = inbuffer->qp_;
        
        ((struct qpInfo *) qp.getContext())->send_count++;
        
        wclist_->push_back(ref);
    }
    else
    {
        //Unknown work request has arrived with success flag set
        return true;
    }
    
    return wclist_->empty();
}

toolbox::mem::Reference * pt::vpi::ROutput::completed()
{
    toolbox::mem::Reference * ref = wclist_->front();
    wclist_->pop_front();
    return ref;
}

// Local Output Support

pt::vpi::LOutput::LOutput(std::shared_ptr<pt::pipe::EstablishedConnection> & pec, pt::pipe::OutputListener * listener, pt::vpi::PeerTransport * pt):  pt::vpi::Output::Output(pec,listener,pt)
{
    pt::vpi::LSupport * ch = (pt::vpi::LSupport *)pec_->getConnectionHandle().get();
    io_ = ch->io;
    cq_ = ch->cq;
}

pt::vpi::LOutput::~LOutput()
{
    
}

void pt::vpi::LOutput::postFrame(toolbox::mem::Reference * ref )
{
    io_->push_back(ref);
}

bool pt::vpi::LOutput::empty()
{
    return cq_->empty();
}

toolbox::mem::Reference * pt::vpi::LOutput::completed()
{
    toolbox::mem::Reference * ref = cq_->front();
    cq_->pop_front();
    return ref;
}


