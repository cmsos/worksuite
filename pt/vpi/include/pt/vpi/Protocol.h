// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, D. Simelevicius					     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_Protocol_h
#define _pt_vpi_Protocol_h

#include <memory>
#include <string>
#include <nlohmann/json.hpp>
#include "ibvla/QueuePair.h"


namespace pt
{
namespace vpi
{


class Channel
{
public:
    Channel (int connfd, nlohmann::json & cookie, const std::string  & originatorhost, const std::string & destinationhost, unsigned int destinationport, const std::string & ip);
    
    ~Channel();
    
    nlohmann::json receive();
    void send(nlohmann::json & document);
    
    std::string getDestinationIP();
    std::string getDestinationHost();
    std::string getOriginatorHost();
    unsigned int getDestinationPort();

    void exchange_with_server(ibvla::QueuePair &qp, enum ibv_mtu mtu, int sgid_index, size_t ibPath, int is_global, int sl, int traffic_class, const  std::string & network, bool sendWithTimeout);
    void exchange_with_client(ibvla::QueuePair &qp, enum ibv_mtu mtu, int sgid_index, size_t ibPath, int is_global, int sl, int traffic_class, const  std::string & network);
    
    nlohmann::json getCookie();
    

protected:
    
    size_t nreceive (char* buf, size_t len);
    size_t receive(char * buf, size_t len );

    void close ();
    
    int sockfd_;
    nlohmann::json cookie_;

    std::string originatorhost_;
    std::string destinationhost_;
    unsigned int destinationport_;
    std::string ip_;
    
};

class ActionListener
{
public:
    virtual void actionPerformed (std::shared_ptr<Channel>  channel) = 0;
};


class Connector
{
public:
    Connector (nlohmann::json & cookie);
    ~Connector();
    
    std::shared_ptr<Channel> connect ( const std::string & hostname, unsigned int port);

protected:
    
    nlohmann::json  cookie_;

};

class Acceptor
{
public:
    
    Acceptor ( ActionListener * listener, nlohmann::json & cookie);
    ~Acceptor();
    
    void listen (const std::string & hostname, unsigned int port, int num);
    void accept ();
    
protected:
    
    int listenfd_;
    ActionListener * listener_;
    std::string originatorhost_;
    unsigned int destinationport_;
    nlohmann::json  cookie_;
    
};

}
}

#endif
