// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _ptvpi_version_h_
#define _ptvpi_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PTVPI_VERSION_MAJOR 1
#define CORE_PTVPI_VERSION_MINOR 6
#define CORE_PTVPI_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_PTVPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_PTVPI_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_PTVPI_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PTVPI_VERSION_MAJOR,CORE_PTVPI_VERSION_MINOR,CORE_PTVPI_VERSION_PATCH)
#ifndef CORE_PTVPI_PREVIOUS_VERSIONS
#define CORE_PTVPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_PTVPI_VERSION_MAJOR,CORE_PTVPI_VERSION_MINOR,CORE_PTVPI_VERSION_PATCH)
#else 
#define CORE_PTVPI_FULL_VERSION_LIST  CORE_PTVPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PTVPI_VERSION_MAJOR,CORE_PTVPI_VERSION_MINOR,CORE_PTVPI_VERSION_PATCH)
#endif 
namespace ptvpi
{
	const std::string project = "core";
    const std::string package  =  "ptvpi";
    const std::string versions = CORE_PTVPI_FULL_VERSION_LIST;
    const std::string summary = "ptuTCP";
    const std::string description = "Verbs based Pipe Interface peer transport";
    const std::string authors = "Luciano Orsini,  Dainius Simelevicius";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

