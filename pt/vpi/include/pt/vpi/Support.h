// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, D. Simelevicius                        *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_Local_h_
#define _pt_vpi_Local_h_

#include "toolbox/mem/Reference.h"
#include "toolbox/rlist.h"
#include "pt/vpi/PeerTransport.h"
#include "pt/vpi/ConnectionRequest.h"
#include "pt/vpi/Protocol.h"
#include "ibvla/ConnectionRequest.h"
#include "ibvla/CompletionQueue.h"

namespace pt 
{
namespace  vpi 
{
struct Support
{
	virtual const std::type_info& getType() const = 0;
};

struct LSupport: public Support
{
    LSupport(size_t qps, size_t cqs);
    ~LSupport();
    const std::type_info& getType() const
    {
	    return typeid(LSupport);
    }

    toolbox::rlist<toolbox::mem::Reference*> * io;
    toolbox::rlist<toolbox::mem::Reference*> * cq;
};

struct RSupport: public Support
{
    RSupport(pt::PeerTransport * pt, std::shared_ptr<pt::vpi::Channel> & channel, ibvla::Context & context, ibvla::ProtectionDomain & pd);

    ~RSupport();
    const std::type_info& getType() const
    {
            return typeid(RSupport);
    }

    pt::PeerTransport * pt;
    std::shared_ptr<pt::vpi::Channel> channel;
    ibvla::Context context;
    ibvla::ProtectionDomain pd;
};


}
}

#endif
