#include <sstream>

#include "sfb/SfBid.h"
#include "sfb/SfBidFactory.h"
#include "sfb/Exception.h"
#include "xcept/tools.h"


sfb::SfBidFactory::SfBidFactory() :
  runNumber_(0),
  previousEventNumber_(0),
  resyncCount_(0),
  resyncAtEventNumber_(1<<25),
  fakeLumiSectionDuration_(0),
  fakeLumiSection_(0),
  doFakeLumiSections_(false)
{}


sfb::SfBidFactory::~SfBidFactory()
{
  stopFakeLumiThread();
}


void sfb::SfBidFactory::setLumiSectionFunction(LumiSectionFunction& lumiSectionFunction)
{
  lumiSectionFunction_ = lumiSectionFunction;
}


void sfb::SfBidFactory::setFakeLumiSectionDuration(const uint32_t duration)
{
  fakeLumiSectionDuration_ = std::chrono::seconds(duration);
}


void sfb::SfBidFactory::reset(const uint32_t runNumber)
{
  runNumber_ = runNumber;
  previousEventNumber_ = 0;
  resyncCount_ = 0;

  stopFakeLumiThread();
  if ( fakeLumiSectionDuration_.count() > 0 )
  {
    fakeLumiSection_ = 1;
    doFakeLumiSections_ = true;
    fakeLumiThread_ = std::thread(&sfb::SfBidFactory::fakeLumiActivity, this);
  }
  else
  {
    fakeLumiSection_ = 0;
  }
}


void sfb::SfBidFactory::stopFakeLumiThread()
{
  if (doFakeLumiSections_)
  {
    doFakeLumiSections_ = false;
    fakeLumiThread_.join();
  }
}


void sfb::SfBidFactory::fakeLumiActivity()
{
  auto nextLumiSectionStartTime = std::chrono::system_clock::now();
  while(doFakeLumiSections_)
  {
    nextLumiSectionStartTime += fakeLumiSectionDuration_;
    std::this_thread::sleep_until(nextLumiSectionStartTime);
    ++fakeLumiSection_;
  }
}


void sfb::SfBidFactory::resyncAtEvent(const uint32_t eventNumber)
{
  resyncAtEventNumber_ = eventNumber;
}


sfb::SfBid sfb::SfBidFactory::getSfBid()
{
  if ( previousEventNumber_ >= resyncAtEventNumber_ )
  {
    resyncAtEventNumber_ = 1 << 25;
    return getSfBid(1,1);
  }
  else
  {
    const uint32_t fakeEventNumber = (previousEventNumber_+1) % (1 << 24);
    return getSfBid(fakeEventNumber,fakeEventNumber%0xfff);
  }
}


sfb::SfBid sfb::SfBidFactory::getSfBid(const uint32_t eventNumber, const uint16_t bxId)
{
  return getSfBid(eventNumber,bxId,fakeLumiSection_);
}


sfb::SfBid sfb::SfBidFactory::getSfBid(const uint32_t eventNumber, const uint16_t bxId, const uint32_t lumiSection)
{
  bool resynced = false;

  if ( eventNumber != previousEventNumber_ + 1 )
  {
    if (eventNumber == 1 && previousEventNumber_ > 0)
    {
      // A proper TTS resync
      ++resyncCount_;
      resynced = true;
    }
    else if (eventNumber == 0 && previousEventNumber_ == 16777215 ) // (2^24)-1
    {
      // Trigger counter rolled over. Increase our resyncCount nevertheless to assure a unique SfBid.
      ++resyncCount_;
    }
    else
    {
      std::ostringstream msg;
      if ( eventNumber == previousEventNumber_ )
      {
        msg << "Received a duplicate event number " << eventNumber;
      }
      else if ( eventNumber > 1 && previousEventNumber_ == 0 && resyncCount_ == 0 )
      {
        msg << "Received " << eventNumber << " as first event number (should be 1.) Have the buffers not be drained?";
      }
      else
      {
        msg << "Skipped from event number " << previousEventNumber_;
        msg << " to " << eventNumber;
      }

      previousEventNumber_ = eventNumber;
      XCEPT_RAISE(exception::EventOutOfSequence,msg.str());
    }
  }

  previousEventNumber_ = eventNumber;

  return SfBid(resynced,resyncCount_,eventNumber,bxId,lumiSection,runNumber_);
}


sfb::SfBid sfb::SfBidFactory::getSfBid(const uint32_t eventNumber, const uint16_t bxId, const DataLocations& dataLocations)
{
  if ( lumiSectionFunction_ )
  {
    const uint32_t lsNumber = lumiSectionFunction_(dataLocations);
    return getSfBid(eventNumber,bxId,lsNumber);
  }
  else
  {
    return getSfBid(eventNumber,bxId);
  }
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
