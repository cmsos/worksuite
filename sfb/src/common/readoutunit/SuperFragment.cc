#include <sstream>
#include <utility>

#include "sfb/Exception.h"
#include "sfb/readoutunit/SuperFragment.h"


sfb::readoutunit::SuperFragment::SuperFragment(const SfBid& sfbId, const std::string& subSystem)
  : sfbId_(sfbId),subSystem_(subSystem),size_(0)
{}


void sfb::readoutunit::SuperFragment::discardFedId(const uint16_t fedId)
{
  missingFedIds_.push_back(fedId);
}


void sfb::readoutunit::SuperFragment::append(FedFragmentPtr&& fedFragment)
{
  size_ += fedFragment->getFedSize();
  fedFragments_.emplace_back(std::move(fedFragment));
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
