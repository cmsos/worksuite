#include <iomanip>
#include <sstream>
#include <bitset>

#include "sfb/readoutunit/MetaDataRetrieverDIPBridge.h"
#include "log4cplus/loggingmacros.h"

#include "b2in/nub/Method.h"
#include "xcept/tools.h"
#include "xcept/Exception.h"
#include "toolbox/mem/AutoReference.h"

#include "toolbox/task/Timer.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"

#include "xdata/Table.h"
#include "xdata/TableIterator.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/Float.h"



sfb::readoutunit::MetaDataRetrieverDIPBridge::MetaDataRetrieverDIPBridge(
  xdaq::Application* owner,
  toolbox::task::TimerListener* timerListener,
  const std::string& eventBusName,
  xdata::UnsignedInteger32& dipBridgeTimeout,
  log4cplus::Logger& logger
):
  eventing::api::Member(owner),
  logger_(logger)
{
  timerListener_= timerListener;
  eventingBusName_=eventBusName.c_str();
  dipBridgeRegisterTimeout_ = dipBridgeTimeout;
  // Attention: DCS HV values have to come first and in the order of the HV bits
  // defined in DataFormats::OnlineMetaData::DCSRecord::Partition in CMSSW
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_ECAL/CMS_ECAL_BP/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_ECAL/CMS_ECAL_BP/state"]= 0;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_ECAL/CMS_ECAL_BM/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_ECAL/CMS_ECAL_BM/state"]= 1;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_ECAL/CMS_ECAL_EP/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_ECAL/CMS_ECAL_EP/state"]= 2;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_ECAL/CMS_ECAL_EM/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_ECAL/CMS_ECAL_EM/state"]= 3;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HEHBa/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HEHBa/state"]= 4;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HEHBb/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HEHBb/state"]= 5;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HEHBc/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HEHBc/state"]= 6;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HF/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HF/state"]= 7;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HO/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_HCAL/CMS_HCAL_HO/state"]= 8;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_RPC/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_RPC/state"]= 9;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_DT/CMS_DT_DT0/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_DT/CMS_DT_DT0/state"]= 10;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_DT/CMS_DT_DTP/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_DT/CMS_DT_DTP/state"]= 11;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_DT/CMS_DT_DTM/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_DT/CMS_DT_DTM/state"]= 12;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_CSC/CMS_CSCP/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_CSC/CMS_CSCP/state"]= 13;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_CSC/CMS_CSCM/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_CSC/CMS_CSCM/state"]= 14;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_HCAL/CMS_HCAL_CASTOR/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_HCAL/CMS_HCAL_CASTOR/state"]= 15;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_HCAL/CMS_HCAL_ZDC/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_HCAL/CMS_HCAL_ZDC/state"]= 16;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_TRACKER/CMS_TRACKER_TIB_TID/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_TRACKER/CMS_TRACKER_TIB_TID/state"]= 17;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_TRACKER/CMS_TRACKER_TOB/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_TRACKER/CMS_TRACKER_TOB/state"]= 18;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_TRACKER/CMS_TRACKER_TECP/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_TRACKER/CMS_TRACKER_TECP/state"]= 19;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_TRACKER/CMS_TRACKER_TECM/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_TRACKER/CMS_TRACKER_TECM/state"]= 20;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_PIXEL/CMS_PIXEL_BPIX/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_PIXEL/CMS_PIXEL_BPIX/state"]= 21;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_PIXEL/CMS_PIXEL_FPIX/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_PIXEL/CMS_PIXEL_FPIX/state"]= 22;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_ECAL/CMS_ECAL_ESP/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_ECAL/CMS_ECAL_ESP/state"]= 23;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_ECAL/CMS_ECAL_ESM/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_ECAL/CMS_ECAL_ESM/state"]= 24;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_GEM/CMS_GEM_EP/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_GEM/CMS_GEM_EP/state"]= 25;
  dipTopics_.push_back( DipTopic("dip/CMS/DCS/CMS_GEM/CMS_GEM_EM/state",unavailable) );
  dipTopicsMap_["dip/CMS/DCS/CMS_GEM/CMS_GEM_EM/state"]= 26;
  dipTopics_.push_back( DipTopic("dip/CMS/MCS/Current",unavailable) );
  dipTopicsMap_["dip/CMS/MCS/Current"]= 27;
  dipTopics_.push_back( DipTopic("dip/CMS/BRIL/Luminosity",unavailable) );
  dipTopicsMap_["dip/CMS/BRIL/Luminosity"]= 28;
  dipTopics_.push_back( DipTopic("dip/CMS/Tracker/BeamSpot",unavailable) );
  dipTopicsMap_["dip/CMS/Tracker/BeamSpot"]= 29;
  dipTopics_.push_back( DipTopic("dip/CMS/CTPPS/detectorFSM",unavailable) );
  dipTopicsMap_["dip/CMS/CTPPS/detectorFSM"]= 30;

 b2in::nub::deferredbind(this->getOwnerApplication(),this, &sfb::readoutunit::MetaDataRetrieverDIPBridge::handleMessage);
 dipBridgeSubscriptions_ = false;

}


sfb::readoutunit::MetaDataRetrieverDIPBridge::~MetaDataRetrieverDIPBridge()
{

	for (auto& topic : dipTopics_)
  {

	try
	{
		if ( maskedDipTopics_.find(topic.first) != std::string::npos )
		  topic.second = masked;
		else if ( topic.second != okay ) {
		    xdata::Properties plist;
		    plist.setProperty("urn:dip:action", "unregister");
		    plist.setProperty("urn:dip:topicname", topic.first.c_str());
		    this->getEventingBus(eventingBusName_).publish("urn:dip:metacontrol", 0, plist);
		    LOG4CPLUS_INFO(logger_,toolbox::toString("urn:dip:action=unregister, urn:dip:topicname=%s",topic.first.c_str()));
		}
	 } catch(eventing::api::exception::Exception& e)
		{
			std::string msg("Failed to unsubscribe to eventing bus "+eventingBusName_);
			LOG4CPLUS_ERROR(logger_,msg+stdformat_exception_history(e));
		}
  }

}

void sfb::readoutunit::MetaDataRetrieverDIPBridge::timerToRegisterToDipBridge() {

     std::string appUUID= this->getOwnerApplication()->getApplicationDescriptor()->getUUID().toString();
     std::string timerName_= "timer_" + appUUID + toolbox::toString(".%d", rand());
     try {
		 toolbox::task::Timer * timer_ = toolbox::task::getTimerFactory()->createTimer(timerName_);

		 // current time, scheduled start time, and xmas-ready time
		 double now = toolbox::TimeVal::gettimeofday();
		 double startTimeSecs_ = now + dipBridgeRegisterTimeout_;

		 // schedule start of monitoring
		 toolbox::TimeVal startTime(startTimeSecs_);
		 timer_->schedule(timerListener_, startTime, 0, "");
     } catch (toolbox::task::exception::Exception &e) {
    	 std::string msg("Failed to create the timer with name=" + timerName_ + " ");
    	 LOG4CPLUS_ERROR(logger_, msg + stdformat_exception_history(e));
     }

}


void sfb::readoutunit::MetaDataRetrieverDIPBridge::registerToDipBridge(
		const std::string &maskedDipTopics) {
	for (auto &topic : dipTopics_) {
			try {
				if (maskedDipTopics.find(topic.first) != std::string::npos)
					topic.second = masked;
				else if (topic.second != okay) {
					xdata::Properties plist;
					plist.setProperty("urn:dip:action", "register");
					plist.setProperty("urn:dip:topicname", topic.first.c_str());
					this->getEventingBus(eventingBusName_).publish(
							"urn:dip:metacontrol", 0, plist);
					LOG4CPLUS_INFO(logger_,
							toolbox::toString(
									"urn:dip:action=register, urn:dip:topicname=%s",
									topic.first.c_str()));
				}
			} catch (eventing::api::exception::Exception &e) {
				std::string msg(
						"Failed to subscribe to eventing bus " + eventingBusName_);
				LOG4CPLUS_ERROR(logger_, msg + stdformat_exception_history(e));
			}
		}
}

void sfb::readoutunit::MetaDataRetrieverDIPBridge::subscribeToDip(
		const std::string &maskedDipTopics) {
	maskedDipTopics_ = maskedDipTopics;
	if (!dipBridgeSubscriptions_) {
		for (auto &topic : dipTopics_) {

			try {
				if (maskedDipTopics.find(topic.first) != std::string::npos)
					topic.second = masked;
				else if (topic.second != okay) {
					this->getEventingBus(eventingBusName_).subscribe(
							topic.first.c_str());
					LOG4CPLUS_INFO(logger_,
							toolbox::toString("subsribe, topicname=%s",
									topic.first.c_str()));
				}
			} catch (eventing::api::exception::Exception &e) {
				std::string msg(
						"Failed to subscribe to eventing bus "
								+ eventingBusName_);
				LOG4CPLUS_ERROR(logger_, msg + stdformat_exception_history(e));
			}
		}
		dipBridgeSubscriptions_ = true;
	}
	// Call the timer to register to DIPBridge
	this->timerToRegisterToDipBridge();
}

void sfb::readoutunit::MetaDataRetrieverDIPBridge::handleMessage(toolbox::mem::Reference * ref, xdata::Properties & plist)
{
	toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
	std::string action = plist.getProperty("urn:b2in-eventing:action");
	if ( action == "notify" && ref!=0 )
	{
		const std::string topicName = plist.getProperty("urn:b2in-eventing:topic");
		//parse table message
		xdata::Table table;
		xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
		xdata::exdr::Serializer serializer;
		try
		{
			serializer.import(&table, &inBuffer);
		}
		catch (xdata::exception::Exception& e)
		{
			LOG4CPLUS_ERROR(logger_,"Failed to deserialize incoming table "+stdformat_exception_history(e));
			ref->release();
		}

		// check if the notification contains any data
		if (table.begin() != table.end())
		{
			try {
			xdata::Serializable* p;
			p = table.getValueAt(0, "DipTimestamp");
			xdata::Integer64 timeStampNanos = dynamic_cast<xdata::Integer64*>(p)->value_;
			uint64_t dipTime = static_cast<uint64_t>(timeStampNanos);

			std::map<std::string,int> ::iterator it;
			it = dipTopicsMap_.find(topicName);

			if(it != dipTopicsMap_.end()) {
				dipTopics_[it->second].second = okay;
			}
			std::stringstream msg;
			if ( topicName == "dip/CMS/BRIL/Luminosity" )
			  {
				  std::lock_guard<std::mutex> guard(luminosityMutex_);

				  lastLuminosity_.timeStamp = dipTime;

				  //lastLuminosity_.lumiSection = dipData.extractInt("LumiSection");
				  p = table.getValueAt(0, "LumiSection");
				  lastLuminosity_.lumiSection = dynamic_cast<xdata::Integer32*>(p)->value_;

				  //lastLuminosity_.lumiNibble = dipData.extractInt("LumiNibble");
				  p = table.getValueAt(0, "LumiNibble");
				  lastLuminosity_.lumiNibble = dynamic_cast<xdata::Integer32*>(p)->value_;

				 //lastLuminosity_.instLumi = dipData.extractFloat("InstLumi");
				  p = table.getValueAt(0, "InstLumi");
				  lastLuminosity_.instLumi = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastLuminosity_.avgPileUp = dipData.extractFloat("AvgPileUp");
				  p = table.getValueAt(0, "AvgPileUp");
				  lastLuminosity_.avgPileUp = dynamic_cast<xdata::Float*>(p)->value_;

				  /*msg << "Got a 'dip/CMS/BRIL/Luminosity' notification: diptime=" << dipTime <<
						  " lumiSection=" << lastLuminosity_.lumiSection << " LumiNibble=" <<
						  lastLuminosity_.lumiNibble << " InstLumi=" << lastLuminosity_.instLumi <<
						  " AvgPileUp=" << lastLuminosity_.avgPileUp;
				  LOG4CPLUS_INFO(logger_,msg.str());*/
			  }
			  else if ( topicName == "dip/CMS/Tracker/BeamSpot" )
			  {
				  std::lock_guard<std::mutex> guard(beamSpotMutex_);

				  lastBeamSpot_.timeStamp = dipTime;

				  //msg << "Got a 'dip/CMS/Tracker/BeamSpot': diptime=" <<  dipTime;

				  //lastBeamSpot_.x = dipData.extractFloat("x");
				  p = table.getValueAt(0, "x");
				  lastBeamSpot_.x = dynamic_cast<xdata::Float*>(p)->value_;

				 // lastBeamSpot_.y = dipData.extractFloat("y");
				  p = table.getValueAt(0, "y");
				  lastBeamSpot_.y = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.z = dipData.extractFloat("z");
				  p = table.getValueAt(0, "z");
				  lastBeamSpot_.z = dynamic_cast<xdata::Float*>(p)->value_;

				  //msg << ", x=" << lastBeamSpot_.x << ", y=" << lastBeamSpot_.y <<", z=" << lastBeamSpot_.z;

				  //lastBeamSpot_.widthX = dipData.extractFloat("width_x");
				  p = table.getValueAt(0, "width_x");
				  lastBeamSpot_.widthX = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.widthY = dipData.extractFloat("width_y");
				  p = table.getValueAt(0, "width_y");
				  lastBeamSpot_.widthY = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.sigmaZ = dipData.extractFloat("sigma_z");
				  p = table.getValueAt(0, "sigma_z");
				  lastBeamSpot_.sigmaZ = dynamic_cast<xdata::Float*>(p)->value_;

				  //msg << ", width_x=" << lastBeamSpot_.widthX << ", width_y=" << lastBeamSpot_.widthY << ", sigma_z=" << lastBeamSpot_.sigmaZ;

				  //lastBeamSpot_.errX = dipData.extractFloat("err_x");
				  p = table.getValueAt(0, "err_x");
				  lastBeamSpot_.errX = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.errY = dipData.extractFloat("err_y");
				  p = table.getValueAt(0, "err_y");
				  lastBeamSpot_.errY = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.errZ = dipData.extractFloat("err_z");
				  p = table.getValueAt(0, "err_z");
				  lastBeamSpot_.errZ = dynamic_cast<xdata::Float*>(p)->value_;

				  //msg << ", err_x=" << lastBeamSpot_.errX << ", err_y=" << lastBeamSpot_.errY << ", err_z=" << lastBeamSpot_.errZ;

				  //lastBeamSpot_.errWidthX = dipData.extractFloat("err_width_x");
				  p = table.getValueAt(0, "err_width_x");
				  lastBeamSpot_.errWidthX = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.errWidthY = dipData.extractFloat("err_width_y");
				  p = table.getValueAt(0, "err_width_y");
				  lastBeamSpot_.errWidthY = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.errSigmaZ = dipData.extractFloat("err_sigma_z");
				  p = table.getValueAt(0, "err_sigma_z");
				  lastBeamSpot_.errSigmaZ = dynamic_cast<xdata::Float*>(p)->value_;

				  //msg << ", err_width_x="<< lastBeamSpot_.errWidthX << ", err_width_y="<< lastBeamSpot_.errWidthY << ", err_sigma_z="<< lastBeamSpot_.errSigmaZ;

				  //lastBeamSpot_.dxdz = dipData.extractFloat("dxdz");
				  p = table.getValueAt(0, "dxdz");
				  lastBeamSpot_.dxdz = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.dydz = dipData.extractFloat("dydz");
				  p = table.getValueAt(0, "dydz");
				  lastBeamSpot_.dydz = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.errDxdz = dipData.extractFloat("err_dxdz");
				  p = table.getValueAt(0, "err_dxdz");
				  lastBeamSpot_.errDxdz = dynamic_cast<xdata::Float*>(p)->value_;

				  //lastBeamSpot_.errDydz = dipData.extractFloat("err_dydz");
				  p = table.getValueAt(0, "err_dydz");
				  lastBeamSpot_.errDydz = dynamic_cast<xdata::Float*>(p)->value_;

				  //msg << ", dxdz=" << lastBeamSpot_.dxdz << ", dydz=" << lastBeamSpot_.dydz << ", err_dxdz="<< lastBeamSpot_.errDxdz << ", err_dydz="<< lastBeamSpot_.errDydz;
				  //LOG4CPLUS_INFO(logger_,msg.str());
			  }
			  else if ( topicName == "dip/CMS/CTPPS/detectorFSM" )
			  {
				  std::lock_guard<std::mutex> guard(ctppsMutex_);
				  lastCTPPS_.timeStamp = dipTime;
				  lastCTPPS_.status = 0;

				  for (uint8_t i = 0; i < MetaData::CTPPS::rpCount; ++i)
				  {
					// TODO check if it works
					p = table.getValueAt(0, MetaData::CTPPS::ctppsRP[i]);
					uint64_t status = static_cast<uint64_t>(dynamic_cast<xdata::Integer32*>(p)->value_);
					lastCTPPS_.status |= (status & 0x3) << (i*2);
				  }
				  /* msg << "Got a 'dip/CMS/CTPPS/detectorFSM' notification: diptime=" << dipTime <<
						  " status=" << lastCTPPS_.status;
				  LOG4CPLUS_INFO(logger_, msg.str());*/

			  }
			  else if ( topicName == "dip/CMS/MCS/Current" )
			  {
				  std::lock_guard<std::mutex> guard(dcsMutex_);
				  lastDCS_.timeStamp = std::max(lastDCS_.timeStamp,dipTime);

				  p = table.getValueAt(0, "__DIP_DEFAULT__");
				  lastDCS_.magnetCurrent = dynamic_cast<xdata::Float*>(p)->value_;
				  /* msg << "Got a 'dip/CMS/MCS/Current' notification: diptime=" << dipTime <<
						  " magnetCurrent=" << lastDCS_.magnetCurrent;
				  LOG4CPLUS_INFO(logger_, msg.str()); */
			  }
			  else
			  {
				const uint16_t pos = std::find_if(dipTopics_.begin(), dipTopics_.end(), isTopic(topicName)) - dipTopics_.begin();
				  std::lock_guard<std::mutex> guard(dcsMutex_);

				  p = table.getValueAt(0, "__DIP_DEFAULT__");
				  const std::string state = dynamic_cast<xdata::String*>(p)->value_;

				  lastDCS_.highVoltageValid |= (1 << pos);

				  if ( state == "ON" || state == "READY" )
					lastDCS_.highVoltageReady |= (1 << pos);
				  else
					lastDCS_.highVoltageReady &= ~(1 << pos);

				  std::bitset<32> bitValue(lastDCS_.highVoltageReady);
				  lastDCS_.timeStamp = std::max(lastDCS_.timeStamp,dipTime);
				  msg << "Got a '" << topicName << "' notification: diptime=" << dipTime << " state="  <<
						  state << " status=" << bitValue;
				  LOG4CPLUS_INFO(logger_, msg.str());

			  }
			} catch (std::bad_cast& e)
			{
				LOG4CPLUS_ERROR(logger_,toolbox::toString("Failed to dynamic cast dip value and got this error: %s", e.what()));
			}
		} else {
			LOG4CPLUS_WARN(logger_,toolbox::toString("DIP notification table is empty for this topic %s.", topicName));
			std::cout << "table is empty" << std::endl;
		}
	}
}


bool sfb::readoutunit::MetaDataRetrieverDIPBridge::fillLuminosity(MetaData::Luminosity& luminosity)
{
  std::lock_guard<std::mutex> guard(luminosityMutex_);

  if ( lastLuminosity_.timeStamp > 0 && lastLuminosity_ != luminosity )
  {
    luminosity = lastLuminosity_;
    return true;
  }

  return false;
}


bool sfb::readoutunit::MetaDataRetrieverDIPBridge::fillBeamSpot(MetaData::BeamSpot& beamSpot)
{
  std::lock_guard<std::mutex> guard(beamSpotMutex_);

  if ( lastBeamSpot_.timeStamp > 0 && lastBeamSpot_ != beamSpot )
  {
    beamSpot = lastBeamSpot_;
    return true;
  }

  return false;
}


bool sfb::readoutunit::MetaDataRetrieverDIPBridge::fillCTPPS(MetaData::CTPPS& CTPPS)
{
  std::lock_guard<std::mutex> guard(ctppsMutex_);

  if ( lastCTPPS_.timeStamp > 0 && lastCTPPS_ != CTPPS )
  {
    CTPPS = lastCTPPS_;
    return true;
  }

  return false;
}


bool sfb::readoutunit::MetaDataRetrieverDIPBridge::fillDCS(MetaData::DCS& dcs)
{
  std::lock_guard<std::mutex> guard(dcsMutex_);

  if ( lastDCS_.timeStamp > 0 && lastDCS_ != dcs )
  {
    dcs = lastDCS_;
    return true;
  }

  return false;
}


bool sfb::readoutunit::MetaDataRetrieverDIPBridge::fillData(unsigned char* payload)
{
  MetaData::Data* data = (MetaData::Data*)payload;
  data->version = MetaData::version;

  return (
    fillLuminosity(data->luminosity) ||
    fillBeamSpot(data->beamSpot) ||
    fillCTPPS(data->ctpps) ||
    fillDCS(data->dcs)
  );
}


bool sfb::readoutunit::MetaDataRetrieverDIPBridge::missingSubscriptions() const
{
  for (auto const& topic : dipTopics_)
  {
    if (topic.second == unavailable) return true;
  }
  return false;
}


void sfb::readoutunit::MetaDataRetrieverDIPBridge::addListOfSubscriptions(std::ostringstream& msg, const bool missingOnly)
{
  for (auto const& topic : dipTopics_)
  {
    if ( missingOnly )
    {
      if ( topic.second == unavailable )
        msg << " " << topic.first;
    }
    else
    {
      msg << " " << topic.first;
    }
  }
}


cgicc::td sfb::readoutunit::MetaDataRetrieverDIPBridge::getDipStatus(const std::string& urn) const
{
  using namespace cgicc;

  td status;
  status.set("colspan","6").set("style","text-align:center");
  status.add(a(std::string("soft FED ") + (missingSubscriptions()?"missing DIP subscriptions":"subscribed to DIP"))
             .set("href","/"+urn+"/dipStatus").set("target","_blank"));

  return status;
}


cgicc::table sfb::readoutunit::MetaDataRetrieverDIPBridge::dipStatusTable() const
{
  using namespace cgicc;

  table table;
  table.set("class","xdaq-table-vertical");

  table.add(tr()
            .add(th("DIP topic"))
            .add(th("value")));

  for (uint16_t pos = 0; pos < dipTopics_.size(); ++pos)
  {
    const std::string topic = dipTopics_[pos].first;

    switch ( dipTopics_[pos].second )
    {
      case okay :
      {
        tr row;
        row.add(td(topic));

        if ( topic == "dip/CMS/BRIL/Luminosity" )
        {
          std::lock_guard<std::mutex> guard(luminosityMutex_);
          std::ostringstream valueStr;
          valueStr << lastLuminosity_;
          row.add(td(pre(valueStr.str())));
        }
        else if ( topic == "dip/CMS/Tracker/BeamSpot" )
        {
          std::lock_guard<std::mutex> guard(dcsMutex_);
          std::ostringstream valueStr;
          valueStr << lastBeamSpot_;
          row.add(td(pre(valueStr.str())));
        }
        else if ( topic == "dip/CMS/CTPPS/detectorFSM" )
        {
          std::lock_guard<std::mutex> guard(ctppsMutex_);
          std::ostringstream valueStr;
          valueStr << lastCTPPS_;
          row.add(td(pre(valueStr.str())));
        }
        else if ( topic == "dip/CMS/MCS/Current" )
        {
          std::lock_guard<std::mutex> guard(dcsMutex_);
          std::ostringstream valueStr;
          valueStr << std::fixed << std::setprecision(3) << lastDCS_.magnetCurrent << " A";
          row.add(td(valueStr.str()));
        }
        else
        {
          std::lock_guard<std::mutex> guard(dcsMutex_);
          if ( lastDCS_.highVoltageReady & (1 << pos) )
            row.add(td("READY"));
          else
            row.add(td("OFF"));
        }

        table.add(row);
        break;
      }
      case unavailable:
      {
        table.add(tr().set("style","background-color: #ff9380")
                  .add(td(topic))
                  .add(td("unavailable")));
        break;
      }
      case masked:
      {
        table.add(tr().set("style","color: #c1c1c1")
                  .add(td(topic))
                  .add(td("masked")));
        break;
      }
    }
  }

  return table;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
