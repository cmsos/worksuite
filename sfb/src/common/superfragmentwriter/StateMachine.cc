#include "sfb/SFW.h"
#include "sfb/superfragmentwriter/DiskWriter.h"
#include "sfb/superfragmentwriter/RUproxy.h"
#include "sfb/superfragmentwriter/SuperFragmentHandler.h"
#include "sfb/superfragmentwriter/ResourceManager.h"
#include "sfb/superfragmentwriter/StateMachine.h"
#include "sfb/superfragmentwriter/States.h"
#include "sfb/Constants.h"
#include "sfb/Exception.h"


sfb::superfragmentwriter::StateMachine::StateMachine
(
  SFW* sfw,
  std::shared_ptr<RUproxy> ruProxy,
  std::shared_ptr<DiskWriter> diskWriter,
  SuperFragmentHandlerPtr superFragmentHandler,
  std::shared_ptr<ResourceManager> resourceManager
):
  SfBStateMachine(sfw,EvBtype::builder),
  sfw_(sfw),
  ruProxy_(ruProxy),
  diskWriter_(diskWriter),
  superFragmentHandler_(superFragmentHandler),
  resourceManager_(resourceManager)
{}

void sfb::superfragmentwriter::Halted::entryAction()
{
	auto thread = std::thread(&sfb::superfragmentwriter::Halted::activity, this);
	  thread.detach(); // otherwise, the thread tries to destruct itself on exiting this state
}

void sfb::superfragmentwriter::Halted::activity()
{
	  outermost_context_type& stateMachine = outermost_context();

	  std::string msg = "Failed to reset counter";
	  try
	  {
	    outermost_context_type& stateMachine = outermost_context();

	    stateMachine.resourceManager()->resetMonitoringCounters();
	    stateMachine.ruProxy()->resetMonitoringCounters();
	    stateMachine.superFragmentHandler()->resetMonitoringCounters();
	    stateMachine.diskWriter()->resetMonitoringCounters();
	  }
	  catch(xcept::Exception& e)
	  {
	    XCEPT_DECLARE_NESTED(exception::FSM,
	                         sentinelException, msg, e);
	    stateMachine.processFSMEvent( Fail(sentinelException) );
	  }
	  catch(std::exception& e)
	  {
	    msg += ": ";
	    msg += e.what();
	    XCEPT_DECLARE(exception::FSM,
	                  sentinelException, msg );
	    stateMachine.processFSMEvent( Fail(sentinelException) );
	  }
	  catch(...)
	  {
	    msg += ": unknown exception";
	    XCEPT_DECLARE(exception::FSM,
	                  sentinelException, msg );
	    stateMachine.processFSMEvent( Fail(sentinelException) );
	  }
}

void sfb::superfragmentwriter::Configuring::entryAction()
{
  doConfiguring_ = true;
  auto thread = std::thread(&sfb::superfragmentwriter::Configuring::activity, this);
  thread.detach(); // otherwise, the thread tries to destruct itself on exiting this state
}


void sfb::superfragmentwriter::Configuring::activity()
{
  outermost_context_type& stateMachine = outermost_context();

  std::string msg = "Failed to configure the components";
  try
  {
    outermost_context_type& stateMachine = outermost_context();

    if ( stateMachine.sfw()->getConfiguration()->useLock )
      stateMachine.acquireLock();

    if (doConfiguring_) stateMachine.resourceManager()->configure();
    if (doConfiguring_) stateMachine.ruProxy()->configure();
    if (doConfiguring_) stateMachine.superFragmentHandler()->configure();
    if (doConfiguring_) stateMachine.diskWriter()->configure();

    if (doConfiguring_) stateMachine.processFSMEvent( ConfigureDone() );
  }
  catch(xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(exception::FSM,
                         sentinelException, msg, e);
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
  catch(std::exception& e)
  {
    msg += ": ";
    msg += e.what();
    XCEPT_DECLARE(exception::FSM,
                  sentinelException, msg );
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    msg += ": unknown exception";
    XCEPT_DECLARE(exception::FSM,
                  sentinelException, msg );
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
}


void sfb::superfragmentwriter::Configuring::exitAction()
{
  doConfiguring_ = false;
}


void sfb::superfragmentwriter::Ready::entryAction()
{
  outermost_context_type& stateMachine = outermost_context();
  stateMachine.notifyRCMS("Ready");
}


void sfb::superfragmentwriter::Active::entryAction()
{
  outermost_context_type& stateMachine = outermost_context();

  if ( stateMachine.sfw()->getConfiguration()->useLock )
    stateMachine.acquireLock();

  stateMachine.resourceManager()->requestEvents(true);

  this->post_event( StartConfigure() );
}


void sfb::superfragmentwriter::Active::exitAction()
{
  outermost_context_type& stateMachine = outermost_context();
  stateMachine.resourceManager()->requestEvents(false);

  stateMachine.releaseLock();
}


void sfb::superfragmentwriter::Running::entryAction()
{
  outermost_context_type& stateMachine = outermost_context();
  const uint32_t runNumber = stateMachine.getRunNumber();

  stateMachine.diskWriter()->startProcessing(runNumber);
  stateMachine.resourceManager()->startProcessing();
  stateMachine.superFragmentHandler()->startProcessing(runNumber);
  stateMachine.ruProxy()->startProcessing();
}


void sfb::superfragmentwriter::Running::exitAction()
{
  outermost_context_type& stateMachine = outermost_context();

  stateMachine.ruProxy()->stopProcessing();
  stateMachine.resourceManager()->stopProcessing();
  stateMachine.superFragmentHandler()->stopProcessing();
  stateMachine.diskWriter()->stopProcessing();
}


void sfb::superfragmentwriter::Draining::entryAction()
{
  doDraining_ = true;
  auto thread = std::thread(&sfb::superfragmentwriter::Draining::activity, this);
  thread.detach(); // otherwise, the thread tries to destruct itself on exiting this state
}


void sfb::superfragmentwriter::Draining::activity()
{
  outermost_context_type& stateMachine = outermost_context();

  std::string msg = "Failed to drain the components";
  try
  {
    if (doDraining_) stateMachine.ruProxy()->drain();
    if (doDraining_) stateMachine.superFragmentHandler()->drain();
    if (doDraining_) stateMachine.resourceManager()->drain();
    if (doDraining_) stateMachine.diskWriter()->drain();

    if (doDraining_) stateMachine.processFSMEvent( DrainingDone() );
  }
  catch(xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(exception::FSM,
                         sentinelException, msg, e);
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
  catch(std::exception& e)
  {
    msg += ": ";
    msg += e.what();
    XCEPT_DECLARE(exception::FSM,
                  sentinelException, msg );
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    msg += ": unknown exception";
    XCEPT_DECLARE(exception::FSM,
                  sentinelException, msg );
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
}


void sfb::superfragmentwriter::Draining::exitAction()
{
  doDraining_ = false;
}


void sfb::superfragmentwriter::Paused::entryAction()
{
  outermost_context_type& stateMachine = outermost_context();
  stateMachine.resourceManager()->requestEvents(false);
}


void sfb::superfragmentwriter::Paused::exitAction()
{
  outermost_context_type& stateMachine = outermost_context();
  stateMachine.resourceManager()->requestEvents(true);
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
