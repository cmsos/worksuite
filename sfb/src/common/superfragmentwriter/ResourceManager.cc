#include "sfb/SFW.h"
#include "sfb/superfragmentwriter/ResourceManager.h"
#include "sfb/superfragmentwriter/StateMachine.h"
#include "sfb/Constants.h"
#include "sfb/SfBid.h"
#include "sfb/Exception.h"
#include "sfb/I2OMessages.h"
#include "xcept/tools.h"

#include <algorithm>
#include <fstream>
#include <math.h>
#include <memory>
#include <string>
#include <utility>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>


sfb::superfragmentwriter::ResourceManager::ResourceManager
(
  SFW* sfw
) :
  sfw_(sfw),
  configuration_(sfw->getConfiguration()),
  resourceFIFO_(sfw,"resourceFIFO"),
  runNumber_(0),
  eventsToDiscard_(0),
  nbResources_(1),
  blockedResources_(1),
  builderId_(0),
  activeFUcount_(0),
  blacklistedFUcount_(0),
  fusHLT_(0),
  fusCloud_(0),
  fusQuarantined_(0),
  fusStale_(0),
  allFUsStaleDetected_(0),
  initiallyQueuedLS_(0),
  queuedLS_(0),
  queuedLSonFUs_(-1),
  fuOutBwMB_(0),
  requestEvents_(false),
  pauseRequested_(false),
  resourceSummaryFailureAlreadyNotified_(false),
  resourceLimitiationAlreadyNotified_(false),
  oldestIncompleteLumiSection_(0),
  doProcessing_(false),
  nextLumiSectionAccountBool_(false)
{
  startResourceMonitorWorkLoop();
  resetMonitoringCounters();
  eventMonitoring_.outstandingRequests = 0;
}


sfb::superfragmentwriter::ResourceManager::~ResourceManager()
{
  if ( resourceMonitorWL_ && resourceMonitorWL_->isActive() )
    resourceMonitorWL_->cancel();
}


void sfb::superfragmentwriter::ResourceManager::startResourceMonitorWorkLoop()
{
  try
  {
    resourceMonitorWL_ = toolbox::task::getWorkLoopFactory()->
      getWorkLoop( sfw_->getIdentifier("resourceMonitor"), "waiting" );

    if ( ! resourceMonitorWL_->isActive() )
    {
      resourceMonitorWL_->activate();

      toolbox::task::ActionSignature* resourceMonitorAction =
        toolbox::task::bind(this, &sfb::superfragmentwriter::ResourceManager::resourceMonitor,
                            sfw_->getIdentifier("resourceMonitor") );

      resourceMonitorWL_->submit(resourceMonitorAction);
    }
  }
  catch(xcept::Exception& e)
  {
    std::string msg = "Failed to start workloop 'resourceMonitor'";
    XCEPT_RETHROW(exception::WorkLoop, msg, e);
  }
}


uint16_t sfb::superfragmentwriter::ResourceManager::underConstruction(const msg::DataBlockMsg* dataBlockMsg)
{
  BuilderResources::iterator pos = builderResources_.find(dataBlockMsg->buResourceId);
  const I2O_TID ruTid = ((I2O_MESSAGE_FRAME*)dataBlockMsg)->InitiatorAddress;

  if ( pos == builderResources_.end() )
  {
    std::ostringstream msg;
    msg << "The buResourceId " << dataBlockMsg->buResourceId;
    msg << " received from RU tid " << ruTid;
    msg << " is not in the builder resources";
    XCEPT_RAISE(exception::EventOrder, msg.str());
  }

  if ( pos->second->builderId == -1 )
  {
    std::ostringstream msg;
    msg << "The buResourceId " << dataBlockMsg->buResourceId;
    msg << " received from RU tid " << ruTid;
    msg << " corresponds to an unassigned builder ID";
    XCEPT_RAISE(exception::EventOrder, msg.str());
  }

  if ( dataBlockMsg->blockNb == 1 ) //only the first block contains the SfBid
  {
    if ( dataBlockMsg->nbSuperFragments == 0 ) // this resource is returned w/o any data
    {
      pos->second->builderId = -1;
      {
        std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
        --eventMonitoring_.outstandingRequests;
      }
      {
        std::lock_guard<std::mutex> guard(resourceFIFOmutex_);
        resourceFIFO_.enqWait(std::move(pos));
      }
      return -1;
    }

    unsigned char* evbIdPos = (unsigned char*)&dataBlockMsg->sfbIds;

    if ( pos->second->evbIdList.empty() )
    {
      // first answer defines the SfBids handled by this resource
      for (uint32_t i=0; i < dataBlockMsg->nbSuperFragments; ++i)
      {
        const SfBid& evbId = *(SfBid*)(evbIdPos + i*sizeof(SfBid));
        pos->second->evbIdList.push_back(evbId);
      }
      {
        std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
        eventMonitoring_.nbEventsInBU += dataBlockMsg->nbSuperFragments;
        --eventMonitoring_.outstandingRequests;
      }
    }
    else
    {
      // check consistency
      if ( pos->second->evbIdList.size() != dataBlockMsg->nbSuperFragments )
      {
        std::ostringstream msg;
        msg << "Received a DataBlockMsg for buResourceId " << dataBlockMsg->buResourceId;
        msg << " from RU tid " << ruTid;
        msg << " with an inconsistent number of super fragments: expected " << pos->second->evbIdList.size();
        msg << ", but got " << dataBlockMsg->nbSuperFragments;
        XCEPT_RAISE(exception::SuperFragment, msg.str());
      }
      uint32_t index = 0;
      for (auto &id : pos->second->evbIdList)
      {
        const SfBid& evbId = *(SfBid*)(evbIdPos + index*sizeof(SfBid));
        if ( evbId != id )
        {
          std::ostringstream msg;
          msg << "Received a DataBlockMsg for buResourceId " << dataBlockMsg->buResourceId;
          msg << " from RU tid " << ruTid;
          msg << " with an inconsistent SfBid for super fragment " << index;
          msg << ": expected " << id;
          msg << ", but got " << evbId;
          XCEPT_RAISE(exception::SuperFragment, msg.str());
        }
        if ( evbId.lumiSection() != id.lumiSection() )
        {
          std::ostringstream msg;
          msg << "Received a DataBlockMsg for buResourceId " << dataBlockMsg->buResourceId;
          msg << " from RU tid " << ruTid;
          msg << " with an inconsistent lumi section for super fragment " << index;
          msg << ": expected " << id.lumiSection();
          msg << ", but got " << evbId.lumiSection();
          XCEPT_RAISE(exception::SuperFragment, msg.str());
        }
        ++index;
      }
    }
    incrementEventsInLumiSection(pos->second->evbIdList);
  }

  return pos->second->builderId;
}


uint32_t sfb::superfragmentwriter::ResourceManager::getOldestIncompleteLumiSection() const
{
  std::lock_guard<std::mutex> guard(lsLatencyMutex_);
  return oldestIncompleteLumiSection_;
}


void sfb::superfragmentwriter::ResourceManager::incrementEventsInLumiSection(const SfBidList& evbIdList)
{
  std::lock_guard<std::mutex> guard(lumiSectionAccountsMutex_);

  const uint32_t lumiSection = evbIdList.front().lumiSection();

  if ( lumiSection == evbIdList.back().lumiSection() )
  {
    LumiSectionAccounts::const_iterator pos = getLumiSectionAccount(lumiSection,evbIdList.front().eventNumber());
    const uint32_t eventCount = evbIdList.size();
    pos->second->nbEvents += eventCount;
    pos->second->nbIncompleteEvents += eventCount;
  }
  else
  {
    for ( auto const& evbId : evbIdList )
    {
      LumiSectionAccounts::const_iterator pos = getLumiSectionAccount(evbId.lumiSection(), evbId.eventNumber());
      pos->second->nbEvents++;
      pos->second->nbIncompleteEvents++;
    }
  }
}


sfb::superfragmentwriter::ResourceManager::LumiSectionAccounts::const_iterator
sfb::superfragmentwriter::ResourceManager::getLumiSectionAccount(const uint32_t lumiSection, const uint32_t eventNumber)
{
  LumiSectionAccounts::const_iterator pos = lumiSectionAccounts_.find(lumiSection);

  if ( pos == lumiSectionAccounts_.end() )
  {
    // backfill any skipped lumi sections
    // if there are pervious LS, fill anything in between
    // if the current lumiSection is 0, this means that we do not create any LS numbers,
    // otherwise start with the first legal LS 1
    const uint32_t newestLumiSection = !lumiSectionAccounts_.empty() ? lumiSectionAccounts_.rbegin()->first + 1 :
      (lumiSection == 0 ? 0 : 1);
    for ( uint32_t ls = newestLumiSection; ls <= lumiSection; ++ls )
    {
      pos = lumiSectionAccounts_.emplace(ls,std::make_unique<LumiSectionAccount>(ls)).first;
    }
  }

  if ( pos == lumiSectionAccounts_.end() )
  {
    std::ostringstream msg;
    msg << "Received an event from an earlier lumi section " << lumiSection;
    msg << " that has already been closed for event number=" << eventNumber << ".";
    XCEPT_RAISE(exception::EventOrder, msg.str());
  }

  return pos;
}


void sfb::superfragmentwriter::ResourceManager::eventCompletedForLumiSection(const uint32_t lumiSection)
{
  std::lock_guard<std::mutex> guard(lumiSectionAccountsMutex_);

  LumiSectionAccounts::iterator pos = lumiSectionAccounts_.find(lumiSection);

  if ( pos == lumiSectionAccounts_.end() )
  {
    std::ostringstream msg;
    msg << "Completed an event from an unknown lumi section " << lumiSection;
    XCEPT_RAISE(exception::EventOrder, msg.str());
  }

  --(pos->second->nbIncompleteEvents);
}


bool sfb::superfragmentwriter::ResourceManager::getNextLumiSectionAccount
(
  LumiSectionAccountPtr& lumiSectionAccount,
  const bool completeLumiSectionsOnly
)
{
  std::lock_guard<std::mutex> guard(lumiSectionAccountsMutex_);

  const LumiSectionAccounts::iterator oldestLumiSection = lumiSectionAccounts_.begin();
  bool foundCompleteLS = false;

  if ( oldestLumiSection == lumiSectionAccounts_.end() ) return false;

  if ( !completeLumiSectionsOnly )
  {
    lumiSectionAccount = std::move(oldestLumiSection->second);
    lumiSectionAccounts_.erase(oldestLumiSection);
    return true;
  }

  uint32_t outstandingRequests;
  {
    std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
    outstandingRequests = std::max(0,eventMonitoring_.outstandingRequests);
  }

  if ( outstandingRequests > 0 )
  {
    // do not expire any lumi section if there are outstanding requests
    const time_t now = time(0);
    for (auto& lsAccount : lumiSectionAccounts_)
    {
      if ( now > lsAccount.second->startTime )
        lsAccount.second->startTime = now;
    }
  }

  if ( oldestLumiSection->second->nbIncompleteEvents == 0 )
  {

	  if ( lumiSectionAccounts_.size() > 2 ) // there are newer lumi sections
	  {
		  uint32_t lumiSection = oldestLumiSection->second->lumiSection;
	      lumiSectionAccount = std::move(oldestLumiSection->second);
	      lumiSectionAccounts_.erase(oldestLumiSection);
	      foundCompleteLS = true;
	      std::ostringstream msg;
	      msg << "Erased old Lumi section " << lumiSection <<  " marked completed.";
	      LOG4CPLUS_DEBUG(sfw_->getApplicationLogger(), msg.str());
	  }
	  else if ( lumiSectionAccounts_.size() > 1)
	  {
		const time_t current_time = time(0);
		if (nextLumiSectionAccountBool_ && nextLumiSectionTimeout_ > 0 && (current_time - nextLumiSectionAccountTime_ > nextLumiSectionTimeout_) ) {
			uint32_t lumiSection = oldestLumiSection->second->lumiSection;
            lumiSectionAccount = std::move(oldestLumiSection->second);
            lumiSectionAccounts_.erase(oldestLumiSection);
            foundCompleteLS = true;
            std::ostringstream msg;
            msg << "Size > 1 and timeout 5 seconds: Erased old Lumi section " << lumiSection << " marked completed.";
            LOG4CPLUS_DEBUG(sfw_->getApplicationLogger(), msg.str());
            nextLumiSectionAccountBool_ = false;
		} else if (!nextLumiSectionAccountBool_) {
			nextLumiSectionAccountBool_ = true;
			nextLumiSectionAccountTime_ = current_time;
		}
	}
	else // check if the current lumi section timed out
    {
      uint32_t lumiSection = oldestLumiSection->second->lumiSection;
      const time_t now = time(0);
      const uint32_t lumiDuration = now>oldestLumiSection->second->startTime ? now-oldestLumiSection->second->startTime : 0;
      if ( lumiSection > 0 && lumiSectionTimeout_ > 0 && lumiDuration > lumiSectionTimeout_ )
      {
        std::ostringstream msg;
        msg.setf(std::ios::fixed);
        msg.precision(1);
        msg << "Lumi section " << lumiSection << " timed out after " << lumiDuration << "s";
        LOG4CPLUS_INFO(sfw_->getApplicationLogger(), msg.str());
        lumiSectionAccount = std::move(oldestLumiSection->second);
        lumiSectionAccounts_.erase(oldestLumiSection);
        foundCompleteLS = true;

        // Insert the next lumi section
        ++lumiSection;
        lumiSectionAccounts_.emplace(lumiSection,std::make_unique<LumiSectionAccount>(lumiSection));
      }
    }
  }

  {
    std::lock_guard<std::mutex> guard(lsLatencyMutex_);
    oldestIncompleteLumiSection_ = lumiSectionAccounts_.begin()->first;
  }
  return foundCompleteLS;
}


void sfb::superfragmentwriter::ResourceManager::eventCompleted(const EventPtr& event)
{
  eventCompletedForLumiSection(event->getEventInfo()->lumiSection());

  std::lock_guard<std::mutex> guard(eventMonitoringMutex_);

  const uint32_t eventSize = event->getEventInfo()->eventSize();
  eventMonitoring_.perf.sumOfSizes += eventSize;
  eventMonitoring_.perf.sumOfSquares += eventSize*eventSize;
  ++eventMonitoring_.perf.logicalCount;
  ++eventMonitoring_.nbEventsBuilt;
}


void sfb::superfragmentwriter::ResourceManager::discardEvent(const EventPtr& event)
{
  BuilderResources::iterator pos = builderResources_.find(event->buResourceId());

  if ( pos == builderResources_.end() )
  {
    std::ostringstream msg;
    msg << "The buResourceId " << event->buResourceId();
    msg << " is not in the builder resources";
    XCEPT_RAISE(exception::EventOrder, msg.str());
  }

  {
    std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
    --eventMonitoring_.nbEventsInBU;
    ++eventsToDiscard_;
  }

  {
    std::lock_guard<std::mutex> guard(pos->second->evbIdListMutex);

    pos->second->evbIdList.remove(event->getSfBid());

    if ( pos->second->evbIdList.empty() )
    {
      pos->second->builderId = -1;
      std::lock_guard<std::mutex> guard(resourceFIFOmutex_);
      resourceFIFO_.enqWait(std::move(pos));
    }
  }
}


void sfb::superfragmentwriter::ResourceManager::getAllAvailableResources(BUresources& resources)
{
  resources.clear();

  if ( blockedResources_ == nbResources_ || !requestEvents_ )
  {
    if ( eventsToDiscard_ > 0 )
    {
      std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
      resources.push_back( BUresource(0,eventsToDiscard_) );
      eventsToDiscard_ = 0;
    }
    else
    {
      ::usleep(1000);
    }
  }
  else
  {
    BuilderResources::iterator pos;
    while ( doProcessing_ && resourceFIFO_.deq(pos) )
    {
      if ( pos->second->blocked )
      {
        ::usleep(configuration_->sleepTimeBlocked);

        std::lock_guard<std::mutex> guard(resourceFIFOmutex_);
        resourceFIFO_.enqWait(std::move(pos));
        return;
      }
      else
      {
        std::lock_guard<std::mutex> guard(eventMonitoringMutex_);

        pos->second->builderId = (++builderId_) % configuration_->numberOfBuilders;
        resources.push_back( BUresource(pos->first,eventsToDiscard_) );
        eventsToDiscard_ = 0;
        ++eventMonitoring_.outstandingRequests;
      }
    }
  }
}


void sfb::superfragmentwriter::ResourceManager::startProcessing()
{
  resetMonitoringCounters();
  doProcessing_ = true;
  runNumber_ = sfw_->getStateMachine()->getRunNumber();
}


void sfb::superfragmentwriter::ResourceManager::drain()
{
  doProcessing_ = false;
}


void sfb::superfragmentwriter::ResourceManager::stopProcessing()
{
  doProcessing_ = false;
}


float sfb::superfragmentwriter::ResourceManager::getAvailableResources(std::string& statusMsg, std::string& statusKeys)
{
  std::lock_guard<std::mutex> guard(resourceSummaryMutex_);

  if ( resourceSummary_.empty() ) return nbResources_;

  const std::time_t lastWriteTime = boost::filesystem::last_write_time(resourceSummary_);
  if ( configuration_->staleResourceTime > 0U &&
       (std::time(0) - lastWriteTime) > configuration_->staleResourceTime )
  {
    std::ostringstream msg;
    msg << resourceSummary_.string() << " has not been updated in the last ";
    msg << configuration_->staleResourceTime << "s";
    handleResourceSummaryFailure(msg.str());
    statusMsg = "Blocking requests because " + resourceSummary_.string() + " is stale";
    statusKeys = "RESOURCE_SUMMARY_STALE";
    return 0;
  }

  uint32_t lsLatency = 0;
  try
  {
    std::lock_guard<std::mutex> guard(lsLatencyMutex_);

    boost::property_tree::ptree pt;
    boost::property_tree::read_json(resourceSummary_.string(), pt);
    fusHLT_ = pt.get<int>("active_resources");
    fusCloud_ = pt.get<int>("cloud");
    fusQuarantined_ = pt.get<int>("quarantined");
    fusStale_ = pt.get<int>("stale_resources");
    fuOutBwMB_ = pt.get<double>("activeRunLSBWMB");
    queuedLSonFUs_ = pt.get<int>("activeRunNumQueuedLS");
    pauseRequested_ = pt.get<bool>("bu_stop_requests_flag");

    const uint32_t activeFURun = pt.get<int>("activeFURun");
    const uint32_t activeRunCMSSWMaxLS = std::max(0,pt.get<int>("activeRunCMSSWMaxLS"));
    if ( activeFURun == runNumber_ && activeRunCMSSWMaxLS > 0 )
    {
      if ( oldestIncompleteLumiSection_ >= activeRunCMSSWMaxLS )
        queuedLS_ = oldestIncompleteLumiSection_ - activeRunCMSSWMaxLS;
      if ( initiallyQueuedLS_ == 0 )
        initiallyQueuedLS_ = queuedLS_;
      else if ( queuedLS_ > initiallyQueuedLS_ )
        lsLatency = queuedLS_ - initiallyQueuedLS_;
      else
        lsLatency = 0;
    }
  }
  catch(boost::property_tree::ptree_error& e)
  {
    std::ostringstream msg;
    msg << "Failed to parse " << resourceSummary_.string() << ": ";
    msg << e.what();
    handleResourceSummaryFailure(msg.str());
    statusMsg = "Blocking requests because " + resourceSummary_.string() + " cannot be parsed";
    statusKeys = "RESOURCE_SUMMARY_FAILURE";
    return 0;
  }
  resourceSummaryFailureAlreadyNotified_ = false;

  if ( fusHLT_ == 0 )
  {
    if ( fusQuarantined_ > 0 )
    {
      statusMsg = "Blocking requests because all FU slots are quarantined";
      statusKeys = "ALL_FUS_QUARANTINED";
    }
    else if ( fusStale_ > 0 )
    {
      statusMsg = "Blocking requests because all FU slots are stale";
      statusKeys = "ALL_FUS_STALE";
    }
    else
    {
      statusMsg = "Blocking requests because there are no FU slots available";
      statusKeys = "NO_FUS_AVAILABLE";
    }
    return 0;
  }

  if ( lsLatency > configuration_->lumiSectionLatencyHigh.value_ )
  {
    std::ostringstream msg;
    msg << "Blocking requests because there are " << lsLatency << " LS queued for the FUs which is more than the allowed latency of "
      << configuration_->lumiSectionLatencyHigh.value_ << " LS";
    statusMsg = msg.str();
    statusKeys = "TOO_MANY_LS_QUEUED_FOR_FUS";
    return 0;
  }

  if ( fuOutBwMB_ > configuration_->fuOutputBandwidthHigh.value_ )
  {
    std::ostringstream msg;
    msg << "Blocking requests because the output throughput from the FUs is " << fuOutBwMB_ << " MB/s, which is higher than the high water mark of "
      << configuration_->fuOutputBandwidthHigh.value_ << " MB/s";
    statusMsg = msg.str();
    statusKeys = "HLT_OUTPUT_TOO_HIGH";
    return 0;
  }

  if ( queuedLSonFUs_ > static_cast<int32_t>(configuration_->maxFuLumiSectionLatency.value_) )
  {
    std::ostringstream msg;
    msg << "Blocking requests because there are " << queuedLSonFUs_ << " LS queued on FUs which is more than the allowed latency of "
      << configuration_->maxFuLumiSectionLatency.value_ << " LS";
    statusMsg = msg.str();
    statusKeys = "TOO_MANY_LS_QUEUED_ON_FUS";
    return 0;
  }

  float resourcesFromFUs = std::max(1.0, fusHLT_ * configuration_->resourcesPerCore);

  if ( resourcesFromFUs < static_cast<float>(nbResources_) )
  {
    std::ostringstream msg;
    msg << "Throttling requests because there are only " << fusHLT_ << " FU slots available";
    if ( fusQuarantined_ > 0 || fusStale_ > 0 )
    {
      msg << ". In addition, there are ";
      if ( fusQuarantined_ > 0 )
        msg << fusQuarantined_ << " quarantined";
      if ( fusQuarantined_ > 0 && fusStale_ > 0 )
        msg << ", and ";
      if ( fusStale_ > 0 )
        msg << fusStale_ << " stale";
      msg << " FU slots";
    }
    statusMsg = msg.str();
    statusKeys = "NOT_ENOUGH_FUS";
  }

  if ( fuOutBwMB_ > configuration_->fuOutputBandwidthLow.value_ )
  {
    std::ostringstream msg;
    if ( statusMsg.empty() )
    {
      msg << "Throttling requests because";
    }
    else
    {
      msg << ", and";
      statusKeys += ",";
    }
    msg << " the output throughput from the FUs is " << fuOutBwMB_ << " MB/s, which is above the low water mark of "
      << configuration_->fuOutputBandwidthLow.value_ << " MB/s";
    statusMsg += msg.str();
    statusKeys += "HLT_OUTPUT_HIGH";

    const float throttle = 1 - ( (fuOutBwMB_ - configuration_->fuOutputBandwidthLow) /
                                 (configuration_->fuOutputBandwidthHigh - configuration_->fuOutputBandwidthLow) );
    resourcesFromFUs = std::min(resourcesFromFUs,static_cast<float>(nbResources_)) * throttle;
  }

  if ( lsLatency > configuration_->lumiSectionLatencyLow.value_ )
  {
    std::ostringstream msg;
    if ( statusMsg.empty() )
    {
      msg << "Throttling requests because";
    }
    else
    {
      msg << ", and";
      statusKeys += ",";
    }
    msg << " there are " << lsLatency << " LS queued for FUs which is above the low water mark of "
      << configuration_->lumiSectionLatencyLow.value_ << " LS";
    statusMsg += msg.str();
    statusKeys += "MANY_LS_QUEUED_FOR_FUS";

    const float throttle = 1 - ( static_cast<float>(lsLatency - configuration_->lumiSectionLatencyLow) /
                                 (configuration_->lumiSectionLatencyHigh - configuration_->lumiSectionLatencyLow) );
    resourcesFromFUs = std::min(resourcesFromFUs,static_cast<float>(nbResources_)) * throttle;
  }

  if ( resourcesFromFUs >= static_cast<float>(nbResources_) )
    return nbResources_;
  else
    return resourcesFromFUs;
}


void sfb::superfragmentwriter::ResourceManager::handleResourceSummaryFailure(const std::string& msg)
{
  if ( resourceSummaryFailureAlreadyNotified_ ) return;

  LOG4CPLUS_ERROR(sfw_->getApplicationLogger(),msg);

  XCEPT_DECLARE(exception::DiskWriting,sentinelError,msg);
  sfw_->notifyQualified("error",sentinelError);

  {
    std::lock_guard<std::mutex> guard(lsLatencyMutex_);

    fusHLT_ = 0;
    fusCloud_ = 0;
    fusStale_ = 0;
    fusQuarantined_ = 0;
    initiallyQueuedLS_ = 0;
    queuedLS_ = 0;
    queuedLSonFUs_ = -1;
    fuOutBwMB_ = 0;
    resourceSummaryFailureAlreadyNotified_ = true;
  }
}


void sfb::superfragmentwriter::ResourceManager::updateDiskUsages()
{
  std::lock_guard<std::mutex> guard(diskUsageMonitorsMutex_);

  for (auto& diskUsageMonitor : diskUsageMonitors_)
  {
    diskUsageMonitor->update();

    const boost::filesystem::path path = diskUsageMonitor->path();
    boost::filesystem::path::const_iterator pathIter = path.begin();
    while ( pathIter != path.end() )
    {
      if ( boost::algorithm::icontains(pathIter->c_str(),"ramdisk") )
      {
        ramDiskSizeInGB_ = diskUsageMonitor->diskSizeGB();
        ramDiskUsed_ = diskUsageMonitor->relDiskUsage();
        break;
      }
      ++pathIter;
    }
  }
}


float sfb::superfragmentwriter::ResourceManager::getOverThreshold()
{
  std::lock_guard<std::mutex> guard(diskUsageMonitorsMutex_);

  if ( diskUsageMonitors_.empty() ) return 0;

  float overThreshold = 0;

  for (auto const& diskUsageMonitor : diskUsageMonitors_)
  {
    overThreshold = std::max(overThreshold,diskUsageMonitor->overThreshold());
  }

  return overThreshold;
}


bool sfb::superfragmentwriter::ResourceManager::resourceMonitor(toolbox::task::WorkLoop*)
{
  try
  {
    std::string statusMsg;
    std::string statusKeys;

    const float availableResources = getAvailableResources(statusMsg,statusKeys);

    if ( doProcessing_ )
    {
      updateResources(availableResources,statusMsg,statusKeys);
      changeStatesBasedOnResources();
    }
    {
      std::lock_guard<std::mutex> guard(statusMessageMutex_);

      statusMessage_ = statusMsg;
      statusKeys_ = statusKeys;

      if ( statusMsg.empty() )
      {
        resourceLimitiationAlreadyNotified_ = false;
      }
      else if ( ! resourceLimitiationAlreadyNotified_ )
      {
        LOG4CPLUS_WARN(sfw_->getApplicationLogger(), statusMsg);
        resourceLimitiationAlreadyNotified_ = true;
      }
    }
  }
  catch(xcept::Exception &e)
  {
    {
      std::lock_guard<std::mutex> guard(statusMessageMutex_);
      statusMessage_ = e.message();
    }
    sfw_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    {
      std::lock_guard<std::mutex> guard(statusMessageMutex_);
      statusMessage_ = e.what();
    }
    XCEPT_DECLARE(exception::FFF,
                  sentinelException, e.what());
    sfw_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    {
      std::lock_guard<std::mutex> guard(statusMessageMutex_);
      statusMessage_ = "Failed for unknown reason";
    }
    XCEPT_DECLARE(exception::FFF,
                  sentinelException, "unkown exception");
    sfw_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }

  ::sleep(1);

  return true;
}


void sfb::superfragmentwriter::ResourceManager::updateResources(const float availableResources, std::string& statusMsg, std::string& statusKeys)
{
  uint32_t resourcesToBlock;
  const float overThreshold = getOverThreshold();

  if ( overThreshold >= 1 )
  {
    resourcesToBlock = nbResources_;
    statusMsg = "Blocking requests because RAMdisk is full";
    statusKeys = "RAMDISK_USAGE_TOO_HIGH";
  }
  else
  {
    const uint32_t usableResources = round( (1-overThreshold) * availableResources );
    resourcesToBlock = nbResources_ < usableResources ? 0 : nbResources_ - usableResources;

    if ( usableResources < round(availableResources) )
    {
      if ( statusMsg.empty() )
      {
        statusMsg = "Throttling because";
      }
      else
      {
        statusMsg += ", and";
        statusKeys += ",";
      }
      std::ostringstream msg;
      msg.setf(std::ios::fixed);
      msg.precision(0);
      msg << " RAMdisk is " << overThreshold*100 << "% over the safe disk-usage threshold of ";
      msg << configuration_->rawDataLowWaterMark*100 << "%";
      statusMsg += msg.str();
      statusKeys += "RAMDISK_USAGE_HIGH";
    }
  }

  {
    std::lock_guard<std::mutex> guard(builderResourcesMutex_);

    BuilderResources::reverse_iterator rit = builderResources_.rbegin();
    const BuilderResources::reverse_iterator ritEnd = builderResources_.rend();
    while ( blockedResources_ < resourcesToBlock && rit != ritEnd )
    {
      if ( !rit->second->blocked )
      {
        rit->second->blocked = true;
        ++blockedResources_;
      }
      ++rit;
    }

    BuilderResources::iterator it = builderResources_.begin();
    const BuilderResources::iterator itEnd = builderResources_.end();
    while ( blockedResources_ > resourcesToBlock && it != itEnd )
    {
      if ( it->second->blocked )
      {
        it->second->blocked = false;
        --blockedResources_;
      }
      ++it;
    }
  }
}



void sfb::superfragmentwriter::ResourceManager::changeStatesBasedOnResources()
{
  bool allFUsQuarantined;
  bool allFUsStale;
  bool fusInCloud;
  {
    std::lock_guard<std::mutex> guard(lsLatencyMutex_);
    allFUsQuarantined = (fusHLT_ == 0U) && (fusQuarantined_ > 0U);
    allFUsStale = (fusHLT_ == 0U) && (fusStale_ > 0U);
    fusInCloud = (fusCloud_ > 0U) && (fusStale_ == 0U);
  }


  if ( allFUsStale )
    ++allFUsStaleDetected_;
  else
    allFUsStaleDetected_ = 0;

  uint32_t outstandingRequests;
  {
    std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
    outstandingRequests = std::max(0,eventMonitoring_.outstandingRequests);
  }

  if ( pauseRequested_ )
  {
    sfw_->getStateMachine()->processFSMEvent( Pause() );
  }
  else if ( allFUsQuarantined )
  {
    XCEPT_RAISE(exception::FFF, "All FU cores in the appliance are quarantined, i.e. HLT is failing on all of them.");
  }
  else if ( allFUsStaleDetected_ >= configuration_->maxTriesFUsStale )
  {
    std::ostringstream msg;
    msg << "All FUs in the appliance are reporting a stale file handle since more than ";
    msg << allFUsStaleDetected_ << " seconds";
    XCEPT_RAISE(exception::FFF, msg.str());
  }
  else if ( blockedResources_ == 0U || outstandingRequests > configuration_->numberOfBuilders )
  {
    sfw_->getStateMachine()->processFSMEvent( Release() );
  }
  else if ( blockedResources_ == nbResources_ )
  {
    if ( fusInCloud )
      sfw_->getStateMachine()->processFSMEvent( Clouded() );
    else
      sfw_->getStateMachine()->processFSMEvent( Block() );
  }
  else if ( blockedResources_ > 0U && outstandingRequests == 0 )
  {
    if ( fusInCloud )
      sfw_->getStateMachine()->processFSMEvent( Misted() );
    else
      sfw_->getStateMachine()->processFSMEvent( Throttle() );
  }
}


void sfb::superfragmentwriter::ResourceManager::appendMonitoringItems(InfoSpaceItems& items)
{
  nbEventsInBU_ = 0;
  nbEventsBuilt_ = 0;
  eventRate_ = 0;
  throughput_ = 0;
  eventSize_ = 0;
  eventSizeStdDev_ = 0;
  nbTotalResources_ = 1;
  nbFreeResources_ = 0;
  nbSentResources_ = 0;
  nbUsedResources_ = 0;
  nbBlockedResources_ = 1;
  fuSlotsHLT_ = 0;
  fuSlotsCloud_ = 0;
  fuSlotsQuarantined_ = 0;
  fuSlotsStale_ = 0;
  fuOutputBandwidthInMB_ = 0;
  queuedLumiSections_ = 0;
  queuedLumiSectionsOnFUs_ = -1;
  ramDiskSizeInGB_ = 0;
  ramDiskUsed_ = 0;
  statusMsg_ = "";
  statusKeywords_ = "";

  items.add("nbEventsInBU", &nbEventsInBU_);
  items.add("nbEventsBuilt", &nbEventsBuilt_);
  items.add("eventRate", &eventRate_);
  items.add("throughput", &throughput_);
  items.add("eventSize", &eventSize_);
  items.add("eventSizeStdDev", &eventSizeStdDev_);
  items.add("nbTotalResources", &nbTotalResources_);
  items.add("nbFreeResources", &nbFreeResources_);
  items.add("nbSentResources", &nbSentResources_);
  items.add("nbUsedResources", &nbUsedResources_);
  items.add("nbBlockedResources", &nbBlockedResources_);
  items.add("fuSlotsHLT", &fuSlotsHLT_);
  items.add("fuSlotsCloud", &fuSlotsCloud_);
  items.add("fuSlotsQuarantined", &fuSlotsQuarantined_);
  items.add("fuSlotsStale", &fuSlotsStale_);
  items.add("fuOutputBandwidthInMB", &fuOutputBandwidthInMB_);
  items.add("queuedLumiSections", &queuedLumiSections_);
  items.add("queuedLumiSectionsOnFUs", &queuedLumiSectionsOnFUs_);
  items.add("ramDiskSizeInGB", &ramDiskSizeInGB_);
  items.add("ramDiskUsed", &ramDiskUsed_);
  items.add("statusMsg", &statusMsg_);
  items.add("statusKeywords", &statusKeywords_);
}


void sfb::superfragmentwriter::ResourceManager::updateMonitoringItems()
{
  updateDiskUsages();

  {
    std::lock_guard<std::mutex> guard(lsLatencyMutex_);

    fuSlotsHLT_ = fusHLT_;
    fuSlotsCloud_ = fusCloud_;
    fuSlotsQuarantined_ = fusQuarantined_;
    fuSlotsStale_ = fusStale_;
    fuOutputBandwidthInMB_ = fuOutBwMB_;
    queuedLumiSections_ = queuedLS_;
    queuedLumiSectionsOnFUs_ = queuedLSonFUs_;
  }
  {
    std::lock_guard<std::mutex> guard(eventMonitoringMutex_);

    const double deltaT = eventMonitoring_.perf.deltaT();
    nbEventsInBU_ = eventMonitoring_.nbEventsInBU;
    nbEventsBuilt_ = eventMonitoring_.nbEventsBuilt;
    eventRate_ = eventMonitoring_.perf.logicalRate(deltaT);
    throughput_ = eventMonitoring_.perf.throughput(deltaT);
    if ( eventRate_ > 0U )
    {
      eventMonitoring_.eventSize = eventMonitoring_.perf.size();
      eventMonitoring_.eventSizeStdDev = eventMonitoring_.perf.sizeStdDev();
    }
    eventSize_ = eventMonitoring_.eventSize;
    eventSizeStdDev_ = eventMonitoring_.eventSizeStdDev;

    eventMonitoring_.perf.reset();
  }
  {
    std::lock_guard<std::mutex> guard(builderResourcesMutex_);

    nbTotalResources_ = nbResources_;
    nbBlockedResources_ = 0;
    nbFreeResources_ = 0;
    nbSentResources_ = 0;
    nbUsedResources_ = 0;

    for (auto const& resource : builderResources_)
    {
      if ( resource.second->blocked )
        ++nbBlockedResources_;
      else if ( resource.second->builderId == -1 )
        ++nbFreeResources_;
      else if ( resource.second->evbIdList.empty() )
        ++nbSentResources_;
      else
        ++nbUsedResources_;
    }
  }
  {
    std::lock_guard<std::mutex> guard(statusMessageMutex_);
    statusMsg_ = statusMessage_;
    statusKeywords_ = statusKeys_;
  }
}


void sfb::superfragmentwriter::ResourceManager::resetMonitoringCounters()
{
  {
    std::lock_guard<std::mutex> guard(lsLatencyMutex_);

    oldestIncompleteLumiSection_ = 0;
    queuedLS_ = 0;
    initiallyQueuedLS_ = 0;
  }
  {
    std::lock_guard<std::mutex> guard(eventMonitoringMutex_);

    eventMonitoring_.nbEventsInBU = 0;
    eventMonitoring_.nbEventsBuilt = 0;
    eventMonitoring_.eventSize = 0;
    eventMonitoring_.eventSizeStdDev = 0;
    eventMonitoring_.perf.reset();
  }
}


uint32_t sfb::superfragmentwriter::ResourceManager::getEventSize() const
{
  std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
  return eventMonitoring_.perf.size();
}


uint32_t sfb::superfragmentwriter::ResourceManager::getEventRate() const
{
  std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
  const double deltaT = eventMonitoring_.perf.deltaT();
  return eventMonitoring_.perf.logicalRate(deltaT);
}


uint32_t sfb::superfragmentwriter::ResourceManager::getThroughput() const
{
  std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
  const double deltaT = eventMonitoring_.perf.deltaT();
  return eventMonitoring_.perf.throughput(deltaT);
}


uint32_t sfb::superfragmentwriter::ResourceManager::getNbEventsInBU() const
{
  std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
  return eventMonitoring_.nbEventsInBU;
}


uint64_t sfb::superfragmentwriter::ResourceManager::getNbEventsBuilt() const
{
  std::lock_guard<std::mutex> guard(eventMonitoringMutex_);
  return eventMonitoring_.nbEventsBuilt;
}


void sfb::superfragmentwriter::ResourceManager::configure()
{
  resourceFIFO_.clear();
  lumiSectionAccounts_.clear();

  eventsToDiscard_ = 0;
  eventMonitoring_.outstandingRequests = 0;

  lumiSectionTimeout_ = configuration_->lumiSectionTimeout;
  nextLumiSectionTimeout_ = configuration_->nextLumiSectionTimeout;

  nbResources_ = std::max(1U,
                          configuration_->maxEvtsUnderConstruction.value_ /
                          configuration_->eventsPerRequest.value_);
  resourceFIFO_.resize(nbResources_);

  configureResources();
  configureResourceSummary();
  configureDiskUsageMonitors();
}


void sfb::superfragmentwriter::ResourceManager::configureResources()
{
  std::lock_guard<std::mutex> guard(builderResourcesMutex_);
  if (configuration_->dropEventData)
    blockedResources_ = 0;
  else
    blockedResources_ = nbResources_;

  builderResources_.clear();
  for (uint32_t resourceId = 1; resourceId <= nbResources_; ++resourceId)
  {
    auto result = builderResources_.emplace(resourceId,
                                            std::make_unique<ResourceInfo>
                                            (-1,!configuration_->dropEventData));
    if ( ! result.second )
    {
      std::ostringstream msg;
      msg << "Failed to insert resource " << resourceId << " into builder resource map";
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }
    assert( resourceFIFO_.enq(std::move(result.first)) );
  }
}


void sfb::superfragmentwriter::ResourceManager::configureResourceSummary()
{
  std::lock_guard<std::mutex> guard(resourceSummaryMutex_);

  resourceSummary_.clear();
  resourceSummaryFailureAlreadyNotified_ = false;
  resourceLimitiationAlreadyNotified_ = false;

  if ( configuration_->dropEventData ) return;

  if ( !configuration_->ignoreResourceSummary && !configuration_->deleteRawDataFiles )
  {
    resourceSummary_ = boost::filesystem::path(configuration_->rawDataDir.value_) / configuration_->resourceSummaryFileName.value_;
    if ( !boost::filesystem::exists(resourceSummary_) )
    {
      std::ostringstream msg;
      msg << "Resource summary file " << resourceSummary_ << " does not exist";
      resourceSummary_.clear();
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }
  }
}


void sfb::superfragmentwriter::ResourceManager::configureDiskUsageMonitors()
{
  std::lock_guard<std::mutex> guard(diskUsageMonitorsMutex_);

  diskUsageMonitors_.clear();

  if ( configuration_->dropEventData ) return;

  diskUsageMonitors_.emplace_back(
    std::make_unique<DiskUsage>(configuration_->rawDataDir.value_,
                                configuration_->rawDataLowWaterMark,
                                configuration_->rawDataHighWaterMark)
  );

  if ( configuration_->metaDataDir != configuration_->rawDataDir )
  {
    diskUsageMonitors_.emplace_back(std::make_unique<DiskUsage>(configuration_->metaDataDir.value_,
                                                                configuration_->metaDataLowWaterMark,
                                                                configuration_->metaDataHighWaterMark)
    );
  }
}


cgicc::div sfb::superfragmentwriter::ResourceManager::getHtmlSnipped() const
{
  using namespace cgicc;

  cgicc::div div;
  div.add(p("ResourceManager"));

  {
    table table;
    table.set("title","Statistics of built events. If the number of outstanding requests is larger than 0, the BU waits for data.");

    std::lock_guard<std::mutex> guard(eventMonitoringMutex_);

    table.add(tr()
              .add(td("# events built"))
              .add(td(std::to_string(eventMonitoring_.nbEventsBuilt))));
    table.add(tr()
              .add(td("# events in BU"))
              .add(td(std::to_string(eventMonitoring_.nbEventsInBU))));
    table.add(tr()
              .add(td("# outstanding requests"))
              .add(td(std::to_string(eventMonitoring_.outstandingRequests))));
    table.add(tr()
              .add(td("throughput (MB/s)"))
              .add(td(doubleToString(throughput_.value_ / 1e6,2))));
    table.add(tr()
              .add(td("rate (events/s)"))
              .add(td(std::to_string(eventRate_))));
    {
      std::ostringstream str;
      str.setf(std::ios::fixed);
      str.precision(1);
      str << eventMonitoring_.eventSize / 1e3 << " +/- " << eventMonitoring_.eventSizeStdDev / 1e3;
      table.add(tr()
                .add(td("event size (kB)"))
                .add(td(str.str())));
    }

    div.add(table);
  }

  {
    table table;
    table.set("title","If no FU slots are available or the output disk is full, no events are requested unless 'dropEventData' is set to true in the configuration.");

    table.add(tr()
              .add(td("# available/blacklisted FUs"))
              .add(td(std::to_string(activeFUcount_)+"/"+std::to_string(blacklistedFUcount_))));

    {
      std::lock_guard<std::mutex> guard(lsLatencyMutex_);

      table.add(tr()
                .add(td("# FU slots available"))
                .add(td(std::to_string(fusHLT_))));
      table.add(tr()
                .add(td("# FU slots used for cloud"))
                .add(td(std::to_string(fusCloud_))));
      table.add(tr()
                .add(td("# FU slots quarantined"))
                .add(td(std::to_string(fusQuarantined_))));
      table.add(tr()
                .add(td("# stale FU slots"))
                .add(td(std::to_string(fusStale_))));
      table.add(tr()
                .add(td("# queued lumi sections for FUs"))
                .add(td(std::to_string(queuedLS_))));
      table.add(tr()
                .add(td("# queued lumi sections on FUs"))
                .add(td(std::to_string(queuedLSonFUs_))));
      table.add(tr()
                .add(td("output throughput of FUs (MB/s)"))
                .add(td(doubleToString(fuOutBwMB_,2))));
      table.add(tr()
                .add(td("# blocked resources"))
                .add(td(std::to_string(blockedResources_)+"/"+std::to_string(nbResources_))));
    }
    {
      std::lock_guard<std::mutex> guard(diskUsageMonitorsMutex_);

      if ( ! diskUsageMonitors_.empty() )
      {
        table.add(tr()
                  .add(th("Output disk usage").set("colspan","2")));

        for (auto const& diskUsageMonitor : diskUsageMonitors_)
        {
          std::ostringstream str;
          str.setf(std::ios::fixed);
          str.precision(1);
          str << diskUsageMonitor->relDiskUsage()*100 << "% of " << diskUsageMonitor->diskSizeGB() << " GB";
          table.add(tr()
                    .add(td(diskUsageMonitor->path().string()))
                    .add(td(str.str())));
        }
      }
    }

    div.add(table);
  }

  {
    cgicc::div resources;
    resources.set("title","A resource is used to request a number of events ('eventsPerRequest'). Resources are blocked if not enough FU cores are available or if the output disk becomes full. The number of resources per FU slot is governed by the configuration parameter 'resourcesPerCore'.");

    resources.add(resourceFIFO_.getHtmlSnipped());

    div.add(resources);
  }

  {
    std::lock_guard<std::mutex> guard(statusMessageMutex_);
    div.add(p(statusMessage_));
  }

  {
    std::lock_guard<std::mutex> guard(lumiSectionAccountsMutex_);

    if ( ! lumiSectionAccounts_.empty() )
    {
      cgicc::table table;
      table.set("title","List of lumi sections for which events are currently being built.");

      table.add(tr()
                .add(th("Active lumi sections").set("colspan","4")));
      table.add(tr()
                .add(td("LS"))
                .add(td("#events"))
                .add(td("#incomplete"))
                .add(td("age (s)")));

      const time_t now = time(0);
      for (auto const& lsAccount : lumiSectionAccounts_)
      {
        table.add(tr()
                  .add(td(std::to_string(lsAccount.first)))
                  .add(td(std::to_string(lsAccount.second->nbEvents)))
                  .add(td(std::to_string(lsAccount.second->nbIncompleteEvents)))
                  .add(td(doubleToString((now>lsAccount.second->startTime ? now-lsAccount.second->startTime : 0),0))));
      }

      div.add(table);
    }
  }

  div.add(br());
  div.add(a("display resource table")
          .set("href","/"+sfw_->getURN().toString()+"/resourceTable").set("target","_blank"));

  return div;
}


cgicc::div sfb::superfragmentwriter::ResourceManager::getHtmlSnippedForResourceTable() const
{
  using namespace cgicc;

  table table;
  table.set("class","xdaq-table-vertical");

  tr row;
  row.add(th("id")).add(th("builder"));
  for (uint32_t request = 0; request < configuration_->eventsPerRequest; ++request)
    row.add(th(std::to_string(request)));
  table.add(row);

  const std::string colspan = std::to_string(configuration_->eventsPerRequest+1);

  {
    std::lock_guard<std::mutex> guard(builderResourcesMutex_);

    for (auto const& resource : builderResources_)
    {
      tr row;
      row.add(td((std::to_string(resource.first))));

      if ( resource.second->blocked )
      {
        row.add(td("BLOCKED").set("colspan",colspan));
      }
      else if ( resource.second->builderId == -1 )
      {
        row.add(td("free").set("colspan",colspan));
      }
      else if ( resource.second->evbIdList.empty() )
      {
        row.add(td("outstanding").set("colspan",colspan));
      }
      else
      {
        row.add(td(std::to_string(resource.second->builderId)));

        uint32_t colCount = 0;
        for (auto const& id : resource.second->evbIdList)
        {
          std::ostringstream evbid;
          evbid << id;
          row.add(td(evbid.str()));
          ++colCount;
        }
        for (uint32_t i = colCount; i < configuration_->eventsPerRequest; ++i)
        {
          row.add(td(" "));
        }
      }

      table.add(row);
    }
  }

  cgicc::div div;
  div.add(table);
  return div;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
