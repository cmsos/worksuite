#include "sfb/superfragmentwriter/FragmentChain.h"
#include "sfb/I2OMessages.h"


sfb::superfragmentwriter::FragmentChain::FragmentChain(uint32_t blockCount) :
  missingBlocks_(blockCount),
  head_(0),tail_(0),
  size_(0)
{}


sfb::superfragmentwriter::FragmentChain::~FragmentChain()
{
  if ( head_ ) head_->release();
}


bool sfb::superfragmentwriter::FragmentChain::append
(
  toolbox::mem::Reference* bufRef
)
{
  chainFragment(bufRef);
  return ( --missingBlocks_ == 0 );
}


void sfb::superfragmentwriter::FragmentChain::chainFragment
(
  toolbox::mem::Reference* bufRef
)
{
  if ( ! head_ )
    head_ = bufRef;
  else
    tail_->setNextReference(bufRef);

  do
  {
    size_ += bufRef->getDataSize() - sizeof(msg::DataBlockMsg);
    tail_ = bufRef;
    bufRef = bufRef->getNextReference();
  }
  while (bufRef);
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
