#include <memory>
#include <sstream>
#include <string>
#include <utility>

#include <boost/lexical_cast.hpp>

#include "sfb/SFW.h"
#include "sfb/Constants.h"
#include "sfb/superfragmentwriter/DiskWriter.h"
#include "sfb/superfragmentwriter/SuperFragmentHandler.h"
#include "sfb/superfragmentwriter/ResourceManager.h"
#include "sfb/superfragmentwriter/StateMachine.h"
#include "sfb/Exception.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xcept/tools.h"


sfb::superfragmentwriter::SuperFragmentHandler::SuperFragmentHandler
(
  SFW* sfw,
  std::shared_ptr<DiskWriter> diskWriter,
  std::shared_ptr<ResourceManager> resourceManager
) :
  sfw_(sfw),
  diskWriter_(diskWriter),
  resourceManager_(resourceManager),
  configuration_(sfw->getConfiguration()),
  corruptedEvents_(0),
  eventsWithCRCerrors_(0),
  eventsMissingData_(0),
  doProcessing_(false),
  writeNextEventsToFile_(0)
{
  builderAction_ =
    toolbox::task::bind(this, &sfb::superfragmentwriter::SuperFragmentHandler::process,
                        sfw_->getIdentifier("eventBuilder") );
}


sfb::superfragmentwriter::SuperFragmentHandler::~SuperFragmentHandler()
{
  for (auto const& builderWL : builderWorkLoops_)
  {
    if ( builderWL->isActive() )
      builderWL->cancel();
  }
}


void sfb::superfragmentwriter::SuperFragmentHandler::addSuperFragment
(
  const uint16_t builderId,
  FragmentChainPtr&& superFragments
)
{
  auto const pos = superFragmentFIFOs_.find(builderId);
  // During halting from running state, superfragments might
  // arrive after the FIFOs have been destroyed.
  // In this case the super-fragment is dropped.
  if ( pos != superFragmentFIFOs_.end() )
    pos->second->enqWait(std::move(superFragments));
}


void sfb::superfragmentwriter::SuperFragmentHandler::configure()
{
  superFragmentFIFOs_.clear();
  eventMapMonitors_.clear();
  writeNextEventsToFile_ = 0;

  {
    std::lock_guard<std::mutex> guard(processesActiveMutex_);

    processesActive_.clear();
    processesActive_.resize(configuration_->numberOfBuilders.value_);
  }

  for (uint16_t i=0; i < configuration_->numberOfBuilders; ++i)
  {
    std::ostringstream fifoName;
    fifoName << "superFragmentFIFO_" << i;
    SuperFragmentFIFOPtr superFragmentFIFO = std::make_shared<SuperFragmentFIFO>(sfw_,fifoName.str());
    superFragmentFIFO->resize(configuration_->superFragmentFIFOCapacity);
    superFragmentFIFOs_.emplace(i,std::move(superFragmentFIFO));

    eventMapMonitors_.emplace(i,EventMapMonitor());
  }

  createProcessingWorkLoops();
}


void sfb::superfragmentwriter::SuperFragmentHandler::createProcessingWorkLoops()
{
  const std::string identifier = sfw_->getIdentifier();

  try
  {
    // Leave any previous created workloops alone. Only add new ones if needed.
    for (uint16_t i=builderWorkLoops_.size(); i < configuration_->numberOfBuilders; ++i)
    {
      std::ostringstream workLoopName;
      workLoopName << identifier << "/Builder_" << i;
      toolbox::task::WorkLoop* wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( workLoopName.str(), "waiting" );

      if ( ! wl->isActive() ) wl->activate();
      builderWorkLoops_.push_back(wl);
    }
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::WorkLoop, "Failed to start workloops", e);
  }
}

void sfb::superfragmentwriter::SuperFragmentHandler::resetMonitoringCounters()
{
	for (auto& monitor : eventMapMonitors_)
	  {
	    monitor.second.reset();
	  }

	{
	    std::lock_guard<std::mutex> guard(errorCountMutex_);
	    corruptedEvents_ = 0;
	    eventsWithCRCerrors_ = 0;
	    eventsMissingData_ = 0;
	 }
}


void sfb::superfragmentwriter::SuperFragmentHandler::startProcessing(const uint32_t runNumber)
{
  for (auto& monitor : eventMapMonitors_)
  {
    monitor.second.reset();
  }

  runNumber_ = runNumber;
  {
    std::lock_guard<std::mutex> guard(errorCountMutex_);
    corruptedEvents_ = 0;
    eventsWithCRCerrors_ = 0;
    eventsMissingData_ = 0;
  }
  doProcessing_ = true;
  for (uint32_t i=0; i < configuration_->numberOfBuilders; ++i)
  {
    builderWorkLoops_.at(i)->submit(builderAction_);
  }
}


bool sfb::superfragmentwriter::SuperFragmentHandler::isEmpty() const
{
  for (auto const& fifo : superFragmentFIFOs_)
  {
    if ( ! fifo.second->empty() ) return false;
  }

  return processesActive_.none();
}


void sfb::superfragmentwriter::SuperFragmentHandler::drain() const
{
  while ( !isEmpty() ) ::usleep(10000);
}


void sfb::superfragmentwriter::SuperFragmentHandler::stopProcessing()
{
  doProcessing_ = false;

  while ( processesActive_.any() ) ::usleep(1000);

  for (auto const& fifo : superFragmentFIFOs_)
  {
    fifo.second->clear();
  }
}


bool sfb::superfragmentwriter::SuperFragmentHandler::process(toolbox::task::WorkLoop* wl)
{
  if ( ! doProcessing_ ) return false;

  const std::string wlName =  wl->getName();
  const size_t startPos = wlName.find_last_of("_") + 1;
  const size_t endPos = wlName.find("/",startPos);
  const uint16_t builderId = boost::lexical_cast<uint16_t>( wlName.substr(startPos,endPos-startPos) );

  {
    std::lock_guard<std::mutex> guard(processesActiveMutex_);
    processesActive_.set(builderId);
  }

  FragmentChainPtr superFragments;
  SuperFragmentFIFOPtr superFragmentFIFO = superFragmentFIFOs_[builderId];

  PartialEvents partialEvents;
  EventMapMonitor& eventMapMonitor = eventMapMonitors_[builderId];

  try
  {
    while ( doProcessing_ )
    {
      bool workDone = false;

      if ( superFragmentFIFO->deq(superFragments) )
      {
        workDone = true;
        try
        {
          buildEvent(superFragments,partialEvents,eventMapMonitor);
        }
        catch(exception::DataCorruption& e)
        {
          {
            std::lock_guard<std::mutex> guard(errorCountMutex_);
            ++corruptedEvents_;
          }

          LOG4CPLUS_ERROR(sfw_->getApplicationLogger(),
                          xcept::stdformat_exception_history(e));
          sfw_->notifyQualified("error",e);
        }
      }

      eventMapMonitor.partialEvents = partialEvents.size();

      if ( ! workDone )
      {
        std::unique_lock<std::mutex> lock(processesActiveMutex_);
        processesActive_.reset(builderId);
        lock.unlock();
        ::usleep(1000);
        lock.lock();
        processesActive_.set(builderId);
      }
    }
  }
  catch(xcept::Exception& e)
  {
    {
      std::lock_guard<std::mutex> guard(processesActiveMutex_);
      processesActive_.reset(builderId);
    }
    sfw_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    {
      std::lock_guard<std::mutex> guard(processesActiveMutex_);
      processesActive_.reset(builderId);
    }
    XCEPT_DECLARE(exception::SuperFragment,
                  sentinelException, e.what());
    sfw_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    {
      std::lock_guard<std::mutex> guard(processesActiveMutex_);
      processesActive_.reset(builderId);
    }
    XCEPT_DECLARE(exception::SuperFragment,
                  sentinelException, "unkown exception");
    sfw_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }

  {
    std::lock_guard<std::mutex> guard(processesActiveMutex_);
    processesActive_.reset(builderId);
  }

  return false;
}


void sfb::superfragmentwriter::SuperFragmentHandler::buildEvent
(
  FragmentChainPtr& superFragments,
  PartialEvents& partialEvents,
  EventMapMonitor& eventMapMonitor
)
{
  toolbox::mem::Reference* bufRef = superFragments->head()->duplicate();
  const I2O_MESSAGE_FRAME* stdMsg =
    (I2O_MESSAGE_FRAME*)bufRef->getDataLocation();
  const msg::DataBlockMsg* firstDataBlockMsg =
    (msg::DataBlockMsg*)stdMsg;
  const I2O_TID ruTid = stdMsg->InitiatorAddress;
  const uint16_t nbSuperFragments = firstDataBlockMsg->nbSuperFragments;
  uint16_t superFragmentCount = 0;
  msg::SfBids evbIds;
  firstDataBlockMsg->getSfBids(evbIds);
  msg::RUtids ruTids;
  firstDataBlockMsg->getRUtids(ruTids);

  do
  {
    toolbox::mem::Reference* nextRef = bufRef->getNextReference();
    bufRef->setNextReference(0);

    const msg::DataBlockMsg* dataBlockMsg =
      (msg::DataBlockMsg*)bufRef->getDataLocation();

    unsigned char* payload = (unsigned char*)bufRef->getDataLocation() + dataBlockMsg->headerSize;
    uint32_t remainingBufferSize = bufRef->getDataSize() - dataBlockMsg->headerSize;

    while ( remainingBufferSize > 0 && superFragmentCount < nbSuperFragments )
    {
      const msg::SuperFragment* superFragmentMsg = (msg::SuperFragment*)payload;
      const SfBid& evbId = evbIds[superFragmentCount];
      const bool checkCRC = ( configuration_->checkCRC > 0U && evbId.eventNumber() % configuration_->checkCRC == 0 );
      auto const pos = partialEvents.emplace(evbId,
                                             std::make_unique<Event>(evbId,
                                                                     ruTids,
                                                                     dataBlockMsg->buResourceId,
                                                                     checkCRC,
                                                                     configuration_->calculateCRC32c)
      ).first;

      eventMapMonitor.currentLumiSection = evbId.lumiSection();

      if ( pos->second->appendSuperFragment(ruTid,
                                            bufRef->duplicate(),
                                            payload) )
      {
        // the super fragment is complete
        ++superFragmentCount;

        if ( pos->second->isComplete() )
        {
          // the event is complete, too
          ++eventMapMonitor.totalEvents;
          checkEvent(pos->second);
          resourceManager_->eventCompleted(pos->second);
          diskWriter_->handleEvent(std::move(pos->second));
          partialEvents.erase(pos);
        }
      }

      const uint32_t size = superFragmentMsg->headerSize + superFragmentMsg->partSize;
      payload += size;
      remainingBufferSize -= size;
      if ( remainingBufferSize < sizeof(msg::SuperFragment) )
        remainingBufferSize = 0;
    }

    bufRef->release();
    bufRef = nextRef;

  } while ( bufRef );

  if (superFragmentCount != nbSuperFragments)
  {
    std::ostringstream msg;
    msg << "Incomplete DataBlockMsg from RU TID " << ruTid;
    msg << ": expected " << nbSuperFragments << " super fragments, but found " << superFragmentCount;
    XCEPT_RAISE(exception::SuperFragment, msg.str());
  }
}


void sfb::superfragmentwriter::SuperFragmentHandler::checkEvent(const EventPtr& event)
{
  try
  {
    event->checkEvent();
  }
  catch(exception::DataCorruption& e)
  {
    std::lock_guard<std::mutex> guard(errorCountMutex_);
    ++corruptedEvents_;

    LOG4CPLUS_ERROR(sfw_->getApplicationLogger(),
                    xcept::stdformat_exception_history(e));
    sfw_->notifyQualified("error",e);
  }

  if ( event->isMissingData() )
  {
    std::lock_guard<std::mutex> guard(errorCountMutex_);
    ++eventsMissingData_;
  }

  if ( event->hasCRCerrors() )
  {
    std::lock_guard<std::mutex> guard(errorCountMutex_);

    if ( sfb::isFibonacci(++eventsWithCRCerrors_) )
    {
      std::ostringstream msg;
      msg << "received " << eventsWithCRCerrors_ << " events with CRC error: ";
      msg << event->getCRCerrorMessage();
      XCEPT_DECLARE(exception::CRCerror,sentinelError,msg.str());
      sfw_->notifyQualified("error",sentinelError);
      LOG4CPLUS_ERROR(sfw_->getApplicationLogger(),msg.str());
    }
  }

  if ( writeNextEventsToFile_ > 0 )
  {
    std::lock_guard<std::mutex> guard(writeNextEventsToFileMutex_);
    if ( writeNextEventsToFile_ > 0 ) // recheck once we have the lock
    {
      event->dumpEventToFile("Requested by user");
      --writeNextEventsToFile_;
    }
  }
}


void sfb::superfragmentwriter::SuperFragmentHandler::appendMonitoringItems(InfoSpaceItems& items)
{
  nbCorruptedEvents_ = 0;
  nbEventsWithCRCerrors_ = 0;
  nbEventsMissingData_ = 0;

  items.add("nbCorruptedEvents", &nbCorruptedEvents_);
  items.add("nbEventsWithCRCerrors", &nbEventsWithCRCerrors_);
  items.add("nbEventsMissingData", &nbEventsMissingData_);
}


void sfb::superfragmentwriter::SuperFragmentHandler::updateMonitoringItems()
{
  std::lock_guard<std::mutex> guard(errorCountMutex_);

  nbCorruptedEvents_ = corruptedEvents_;
  nbEventsWithCRCerrors_ = eventsWithCRCerrors_;
  nbEventsMissingData_ = eventsMissingData_;
}


uint64_t sfb::superfragmentwriter::SuperFragmentHandler::getNbCorruptedEvents() const
{
  std::lock_guard<std::mutex> guard(errorCountMutex_);
  return corruptedEvents_;
}


uint64_t sfb::superfragmentwriter::SuperFragmentHandler::getNbEventsWithCRCerrors() const
{
  std::lock_guard<std::mutex> guard(errorCountMutex_);
  return eventsWithCRCerrors_;
}


uint64_t sfb::superfragmentwriter::SuperFragmentHandler::getNbEventsMissingData() const
{
  std::lock_guard<std::mutex> guard(errorCountMutex_);
  return eventsMissingData_;
}


void sfb::superfragmentwriter::SuperFragmentHandler::writeNextEventsToFile(const uint16_t count)
{
  std::lock_guard<std::mutex> guard(writeNextEventsToFileMutex_);
  writeNextEventsToFile_ = count;
}


cgicc::div sfb::superfragmentwriter::SuperFragmentHandler::getHtmlSnipped() const
{
  using namespace cgicc;

  cgicc::div div;
  div.add(p("SuperFragmentHandler"));

  {
    std::lock_guard<std::mutex> guard(errorCountMutex_);

    cgicc::table table;
    table.set("title","Number of bad events since the beginning of the run.");

    table.add(tr()
              .add(td("# corrupted events"))
              .add(td(std::to_string(corruptedEvents_))));
    table.add(tr()
              .add(td("# events w/ CRC errors"))
              .add(td(std::to_string(eventsWithCRCerrors_))));
    table.add(tr()
              .add(td("# events w/ missing FED data"))
              .add(td(std::to_string(eventsMissingData_))));

    div.add(table);
  }

  {
    cgicc::table table;
    table.set("title","List of builder threads. Each thread assembles events independently. Any complete event is held back until all threads have reached the next lumi section.");

    std::lock_guard<std::mutex> guard(processesActiveMutex_);

    table.add(tr()
              .add(th("Event builders").set("colspan","5")));
    table.add(tr()
              .add(td("builder"))
              .add(td("active"))
              .add(td("ls"))
              .add(td("#partial"))
              .add(td("#total")));

    for (auto const& monitor : eventMapMonitors_)
    {
      table.add(tr()
                .add(td(std::to_string(monitor.first)))
                .add(td(processesActive_[monitor.first]?"yes":"no"))
                .add(td(std::to_string(monitor.second.currentLumiSection)))
                .add(td(std::to_string(monitor.second.partialEvents)))
                .add(td(std::to_string(monitor.second.totalEvents))));
    }

    div.add(table);
  }

  if ( ! superFragmentFIFOs_.empty() )
  {
    cgicc::div fifos;
    fifos.set("title","FIFOs holding super-fragments to be fed to the corresponding builder threads.");

    SuperFragmentFIFOs::const_iterator it = superFragmentFIFOs_.begin();
    while ( it != superFragmentFIFOs_.end() )
    {
      try
      {
        fifos.add(it->second->getHtmlSnipped());
        ++it;
      }
      catch(...) {}
    }

    div.add(fifos);
  }

  div.add(br());
  const std::string javaScript = "function dumpEvents(count) { var options = { url:'/" +
    sfw_->getURN().toString() + "/writeNextEventsToFile?count='+count }; xdaqAJAX(options,null); }";
  div.add(script(javaScript));
  div.add(button("dump next event").set("type","button").set("title","dump the next event to /tmp")
          .set("onclick","dumpEvents(1);"));

  return div;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
