#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"
#include "interface/shared/i2ogevb2g.h"
#include "sfb/EVM.h"
#include "sfb/evm/RUproxy.h"
#include "sfb/Constants.h"
#include "sfb/SfBid.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xcept/tools.h"
#include "xdaq/ApplicationDescriptor.h"

#include <algorithm>
#include <string>
#include <utility>


sfb::evm::RUproxy::RUproxy
(
  EVM* evm,
  std::shared_ptr< readoutunit::StateMachine<EVM> > stateMachine
) :
  evm_(evm),
  stateMachine_(stateMachine),
  msgPool_(evm->getMsgPool()),
  readoutMsgFIFO_(evm,"readoutMsgFIFO"),
  doProcessing_(false),
  draining_(false),
  processingActive_(false),
  tid_(0),
  ruCount_(0)
{
  resetMonitoringCounters();
  startRequestWorkLoop();
}


sfb::evm::RUproxy::~RUproxy()
{
  if ( processRequestsWL_->isActive() )
    processRequestsWL_->cancel();
}


void sfb::evm::RUproxy::startRequestWorkLoop()
{
  try
  {
    processRequestsWL_ = toolbox::task::getWorkLoopFactory()->
      getWorkLoop( evm_->getIdentifier("processRequests"), "waiting" );

    if ( ! processRequestsWL_->isActive() )
    {
      processRequestsWL_->activate();

      processRequestsAction_ =
        toolbox::task::bind(this, &sfb::evm::RUproxy::processRequests,
                            evm_->getIdentifier("processRequests") );
    }
  }
  catch(xcept::Exception& e)
  {
    std::string msg = "Failed to start workloop 'processRequests'";
    XCEPT_RETHROW(exception::WorkLoop, msg, e);
  }
}


void sfb::evm::RUproxy::startProcessing()
{
  resetMonitoringCounters();
  if ( participatingRUs_.empty() ) return;

  doProcessing_ = true;
  draining_ = false;

  processRequestsWL_->submit(processRequestsAction_);
}


void sfb::evm::RUproxy::drain()
{
  draining_ = true;
  while ( !readoutMsgFIFO_.empty() || processingActive_ ) ::usleep(1000);
}


void sfb::evm::RUproxy::stopProcessing()
{
  doProcessing_ = false;
  draining_ = false;
  while ( processingActive_ ) ::usleep(1000);

  readoutMsgFIFO_.clear();
}


toolbox::mem::Reference* sfb::evm::RUproxy::getRequestMsgBuffer(const uint32_t bufSize)
{
  toolbox::mem::Reference* rqstBufRef = 0;
  do
  {
    try
    {
      rqstBufRef = toolbox::mem::getMemoryPoolFactory()->
        getFrame(msgPool_, bufSize);
      rqstBufRef->setDataSize(bufSize);
    }
    catch(toolbox::mem::exception::Exception&)
    {
      rqstBufRef = 0;
      if ( ! doProcessing_ )
        throw exception::HaltRequested();
      ::usleep(100);
    }
  } while ( !rqstBufRef );

  return rqstBufRef;
}


void sfb::evm::RUproxy::sendRequest(readoutunit::FragmentRequestPtr fragmentRequest)
{
  if ( ! participatingRUs_.empty() )
  {
    readoutMsgFIFO_.enqWait(std::move(fragmentRequest));
  }
}


bool sfb::evm::RUproxy::processRequests(toolbox::task::WorkLoop* wl)
{
  if ( ! doProcessing_ ) return false;

  processingActive_ = true;

  const uint32_t blockSize = evm_->getConfiguration()->allocateBlockSize;
  toolbox::mem::Reference* rqstBufRef = 0;
  //uint32_t initRuToSendTID = 0U;
  I2O_TID ruToSendTIDLast = 0U;
  uint32_t msgSize = 0;
  unsigned char* payload = 0;
  uint32_t requestCount = 0;
  const uint64_t maxAllocateTime = evm_->getConfiguration()->maxAllocateTime * 1000 * ruCount_;
  uint64_t timeLimit = getTimeStamp() + maxAllocateTime;

  try
  {
    do
    {
      readoutunit::FragmentRequestPtr fragmentRequest;
      while ( readoutMsgFIFO_.deq(fragmentRequest) )
      {
        const uint16_t nbRequests = fragmentRequest->nbRequests;
        const uint16_t ruCount = fragmentRequest->ruTids.size();
        const size_t eventRequestSize = sizeof(msg::EventRequest) +
          nbRequests * sizeof(SfBid) +
          ((ruCount+1)&~1) * sizeof(I2O_TID); // even number of I2O_TIDs to align header to 64-bits
        const I2O_TID ruToSendTID = fragmentRequest->ruTids[0];
        std::ostringstream msg;
        msg << "ProcessRequests fragementRequest from TID " << fragmentRequest->sfwTid << " ruTids=";
        if ( !fragmentRequest->ruTids.empty() )
        {
            msg << '[';
            for (auto i = fragmentRequest->ruTids.begin(); i != fragmentRequest->ruTids.end(); ++i)
            	  msg  << *i << ' ';
      	  msg << "]";
            }
        msg << " ruToSendTID=" << ruToSendTID;
        LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg.str());

        if ( (msgSize+eventRequestSize > blockSize && rqstBufRef && ruToSendTIDLast != 0U && ruToSendTID == ruToSendTIDLast)
        		||  ( rqstBufRef && ruToSendTIDLast != 0U && ruToSendTID != ruToSendTIDLast) )
        {
           std::ostringstream msg;
           msg << "BeforeCalling 1 ruToSendTID="<< ruToSendTID << " ruToSendTIDLast=" << ruToSendTIDLast ;
           LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg.str());
          sendMsgToRUs(rqstBufRef,msgSize,requestCount,ruToSendTIDLast);
          std::ostringstream  msg2;
          msg2 << "AfterCalling 1 ruToSendTID="<< ruToSendTID << " ruToSendTIDLast=" << ruToSendTIDLast ;
          LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg2.str());
        }

        if ( ! rqstBufRef )
        {
          rqstBufRef = getRequestMsgBuffer(blockSize);
          msgSize = sizeof(msg::ReadoutMsg);
          payload = ((unsigned char*)rqstBufRef->getDataLocation()) + msgSize;
          timeLimit = getTimeStamp() + maxAllocateTime;
        }

        ruToSendTIDLast = ruToSendTID;
        msg::EventRequest* eventRequest = (msg::EventRequest*)payload;
        eventRequest->msgSize      = eventRequestSize;
        eventRequest->sfwTid        = fragmentRequest->sfwTid;
        eventRequest->buResourceId = fragmentRequest->buResourceId;
        eventRequest->timeStampNS  = fragmentRequest->timeStampNS;
        eventRequest->nbRequests   = nbRequests;
        eventRequest->nbDiscards   = fragmentRequest->nbDiscards;
        eventRequest->nbRUtids     = ruCount;
        payload += sizeof(msg::EventRequest);

        memcpy(payload,&fragmentRequest->sfbIds[0],nbRequests*sizeof(SfBid));
        payload += nbRequests*sizeof(SfBid);

        memcpy(payload,&fragmentRequest->ruTids[0],ruCount*sizeof(I2O_TID));
        payload += ((ruCount+1)&~1)*sizeof(I2O_TID);
        msgSize += eventRequestSize;
        ++requestCount;

        {
          std::lock_guard<std::mutex> guard(allocateMonitoringMutex_);

          allocateMonitoring_.lastEventNumberToRUs = fragmentRequest->sfbIds[nbRequests-1].eventNumber();
          allocateMonitoring_.perf.logicalCount += nbRequests*ruCount_;
        }
      }
      ::usleep(10);
    } while ( getTimeStamp() < timeLimit && !draining_ );

    if ( rqstBufRef )
    {
    	std::ostringstream msg;
    	msg << "BeforeCalling 2 ruToSendTIDLast="<< ruToSendTIDLast;
    	LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg.str());
      // send what we have
      sendMsgToRUs(rqstBufRef,msgSize,requestCount,ruToSendTIDLast);
      std::ostringstream  msg2;
      msg2 << "BeforeCalling 2 ruToSendTIDLast="<< ruToSendTIDLast;
       LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg2.str());
    }
  }
  catch(exception::HaltRequested& e)
  {
    if (rqstBufRef) rqstBufRef->release();
    processingActive_ = false;
    return false;
  }
  catch(xcept::Exception& e)
  {
    if (rqstBufRef) rqstBufRef->release();
    processingActive_ = false;
    stateMachine_->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    if (rqstBufRef) rqstBufRef->release();
    processingActive_ = false;
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, e.what());
    stateMachine_->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    if (rqstBufRef) rqstBufRef->release();
    processingActive_ = false;
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, "unkown exception");
    stateMachine_->processFSMEvent( Fail(sentinelException) );
  }

  processingActive_ = false;

  return doProcessing_;
}


void sfb::evm::RUproxy::sendMsgToRUs(toolbox::mem::Reference*& rqstBufRef, const uint32_t msgSize, uint32_t& requestCount,const I2O_TID ruToSentTID)
{
  I2O_MESSAGE_FRAME* stdMsg =
    (I2O_MESSAGE_FRAME*)rqstBufRef->getDataLocation();
  I2O_PRIVATE_MESSAGE_FRAME* pvtMsg = (I2O_PRIVATE_MESSAGE_FRAME*)stdMsg;
  msg::ReadoutMsg* readoutMsg = (msg::ReadoutMsg*)stdMsg;

  stdMsg->VersionOffset    = 0;
  stdMsg->MsgFlags         = 0;
  stdMsg->MessageSize      = msgSize >> 2;
  stdMsg->InitiatorAddress = tid_;
  stdMsg->Function         = I2O_PRIVATE_MESSAGE;
  pvtMsg->OrganizationID   = XDAQ_ORGANIZATION_ID;
  pvtMsg->XFunctionCode    = I2O_SHIP_FRAGMENTS;
  readoutMsg->nbRequests   = requestCount;

  rqstBufRef->setDataSize(msgSize);
  requestCount = 0;

  uint32_t retries = 0;
  boost::upgrade_lock<boost::shared_mutex> sl(ruConnectionsMutex_);
  auto ru = participatingRUs_.find(ruToSentTID);

  if ( ru == participatingRUs_.end() )
    {
	  std::ostringstream msg;
	  	      msg << "Failed to send message RU. Cannot fins application descriptor RU tid=";
	  	      msg << ru->first;
	  	    XCEPT_RAISE(exception::I2O, msg.str());

    } else {


    	toolbox::mem::Reference* bufRef = getRequestMsgBuffer(msgSize);
	    void* payload = bufRef->getDataLocation();
	    memcpy(payload,rqstBufRef->getDataLocation(),msgSize);
	    I2O_MESSAGE_FRAME* stdMsg = (I2O_MESSAGE_FRAME*)payload;
	    stdMsg->TargetAddress = ru->first;
        std::ostringstream msg;
        msg << "Send message to RU class=" << ru->second.descriptor->getClassName() << " instance=" <<  ru->second.descriptor->getInstance() << " destination URL=" << ru->second.descriptor->getContextDescriptor()->getURL() << " tid=" << ru->first << " lid=" << ru->second.descriptor->getLocalId() << " requested tid=" << ruToSentTID ;
        LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg.str());
	    try
	    {
	      retries += evm_->postMessage(bufRef,ru->second.descriptor);
	    }
	    catch(exception::I2O& e)
	    {
	      std::ostringstream msg;
	      msg << "Failed to send message to RU ";
	      msg << ru->first;
	      XCEPT_RETHROW(exception::I2O, msg.str(), e);
	    }
	    msg.clear();
	    msg << "AFTER message to RU class=" << ru->second.descriptor->getClassName() << " instance=" <<  ru->second.descriptor->getInstance() << " destination URL=" << ru->second.descriptor->getContextDescriptor()->getURL() << " tid=" << ru->first << " lid=" << ru->second.descriptor->getLocalId() << " requested tid=" << ruToSentTID ;
	    LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg.str());
  }
  rqstBufRef->release();
  rqstBufRef = 0;

  {
    std::lock_guard<std::mutex> guard(allocateMonitoringMutex_);

    const uint32_t totalSize = msgSize*ruCount_;
    allocateMonitoring_.perf.sumOfSizes += totalSize;
    allocateMonitoring_.perf.sumOfSquares += totalSize*totalSize;
    allocateMonitoring_.perf.i2oCount += ruCount_;
    allocateMonitoring_.perf.retryCount += retries;
  }
}


void sfb::evm::RUproxy::appendMonitoringItems(InfoSpaceItems& items)
{
  allocateRate_ = 0;
  allocateRetryRate_ = 0;
  allocateRetryCount_= 0;

  items.add("allocateRate", &allocateRate_);
  items.add("allocateRetryRate", &allocateRetryRate_);
  items.add("allocateRetryCount", &allocateRetryCount_);
}


void sfb::evm::RUproxy::updateMonitoringItems()
{
  std::lock_guard<std::mutex> guard(allocateMonitoringMutex_);

  const double deltaT = allocateMonitoring_.perf.deltaT();
  allocateMonitoring_.throughput = allocateMonitoring_.perf.throughput(deltaT);
  allocateMonitoring_.assignmentRate = allocateMonitoring_.perf.logicalRate(deltaT);
  allocateMonitoring_.i2oRate = allocateMonitoring_.perf.i2oRate(deltaT);
  allocateMonitoring_.retryRate = allocateMonitoring_.perf.retryRate(deltaT);
  allocateMonitoring_.retryCount = allocateMonitoring_.perf.retryCount;
  allocateMonitoring_.packingFactor = allocateMonitoring_.perf.packingFactor();
  allocateMonitoring_.perf.reset();

  allocateRate_ = allocateMonitoring_.i2oRate;
  allocateRetryRate_ = allocateMonitoring_.retryRate;
  allocateRetryCount_ = allocateMonitoring_.retryCount;
}


void sfb::evm::RUproxy::resetMonitoringCounters()
{
  std::lock_guard<std::mutex> guard(allocateMonitoringMutex_);
  allocateMonitoring_.lastEventNumberToRUs = 0;
  allocateMonitoring_.retryCount = 0;
  allocateMonitoring_.perf.reset();
}


void sfb::evm::RUproxy::configure()
{
  getApplicationDescriptors();
  ruCount_ = participatingRUs_.size();
  sfwCount_ =  participatingSFWs_.size();

  readoutMsgFIFO_.clear();
  readoutMsgFIFO_.resize(evm_->getConfiguration()->allocateFIFOCapacity);
}


void sfb::evm::RUproxy::getApplicationDescriptors()
{
  try
  {
    tid_ = i2o::utils::getAddressMap()->
      getTid(evm_->getApplicationDescriptor());
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::I2O,
                  "Failed to get I2O TID for this application", e);
  }

  // Clear list of participating RUs
  participatingRUs_.clear();
  mappingRUInstanceToTid_.clear();
  mappingSFWToRUTid_.clear();
  ruCount_ = 0;
  ruTids_.clear();
  ruTids_.push_back(tid_);

  for (auto const& ru : evm_->getConfiguration()->ruInstances)
  {
    fillRUInstance(ru);
  }

  // Clear list of participating SFWs
  participatingSFWs_.clear();
  sfwCount_ = 0;
  sfwTids_.clear();
  std::ostringstream msg;
  msg << "sfb::evm::RUproxy::getApplicationDescriptors sfwInstances=";
  if ( !evm_->getConfiguration()->sfwInstances.empty() )
  {
      msg << '[';
      for (auto i = evm_->getConfiguration()->sfwInstances.begin(); i != evm_->getConfiguration()->sfwInstances.end(); ++i) {
    	  msg  << *i << ' ';
      }
	  msg << "]";
  }
  LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg.str());

  for (auto const& sfw : evm_->getConfiguration()->sfwInstances)
  {
	  fillSFWInstance(sfw);
  }

}


void sfb::evm::RUproxy::fillRUInstance(xdata::UnsignedInteger32 instance)
{
  ApplicationDescriptorAndTid ru;

  try
  {
    ru.descriptor =
      evm_->getApplicationContext()->
      getDefaultZone()->
      getApplicationDescriptor("sfb::RU", instance);
  }
  catch(xcept::Exception &e)
  {
    std::ostringstream msg;
    msg << "Failed to get application descriptor for RU ";
    msg << instance.toString();
    XCEPT_RETHROW(exception::Configuration, msg.str(), e);
  }

  try
  {
    ru.tid = i2o::utils::getAddressMap()->getTid(ru.descriptor);
  }
  catch(xcept::Exception &e)
  {
    std::ostringstream msg;
    msg << "Failed to get I2O TID for RU ";
    msg << instance.toString();
    XCEPT_RETHROW(exception::I2O, msg.str(), e);
  }

  if (auto search = participatingRUs_.find(ru.tid); search != participatingRUs_.end())
  {
    std::ostringstream msg;
    msg << "Participating RU instance " << instance.toString();
    msg << " is a duplicate.";
    XCEPT_RAISE(exception::Configuration, msg.str());
  }

  // duplicate instance on mappingRUInstanceToTid_
  if(auto search = mappingRUInstanceToTid_.find(instance); search != mappingRUInstanceToTid_.end()){
	    std::ostringstream msg;
	    msg << "Mapping RU to instance " << instance.toString();
	    msg << " is a duplicate.";
	    XCEPT_RAISE(exception::Configuration, msg.str());
  }
  mappingRUInstanceToTid_.insert({instance,ru.tid});
  participatingRUs_.insert({ru.tid,std::move(ru)});
  ruTids_.push_back(ru.tid);
}


void sfb::evm::RUproxy::fillSFWInstance(xdata::UnsignedInteger32 instance)
{
  ApplicationDescriptorAndTid sfw;

  try
  {
    sfw.descriptor =
      evm_->getApplicationContext()->
      getDefaultZone()->
      getApplicationDescriptor("sfb::SFW", instance);
  }
  catch(xcept::Exception &e)
  {
    std::ostringstream msg;
    msg << "Failed to get application descriptor for SFW ";
    msg << instance.toString();
    XCEPT_RETHROW(exception::Configuration, msg.str(), e);
  }

  try
  {
    sfw.tid = i2o::utils::getAddressMap()->getTid(sfw.descriptor);
  }
  catch(xcept::Exception &e)
  {
    std::ostringstream msg;
    msg << "Failed to get I2O TID for SFW ";
    msg << instance.toString();
    XCEPT_RETHROW(exception::I2O, msg.str(), e);
  }

  if ( std::find(participatingSFWs_.begin(),participatingSFWs_.end(),sfw) != participatingSFWs_.end() )
  {
    std::ostringstream msg;
    msg << "Participating SFW instance " << instance.toString();
    msg << " is a duplicate.";
    XCEPT_RAISE(exception::Configuration, msg.str());
  }
  std::vector<I2O_TID> ruVector;
  if (instance==0U) {
	  evmSFW_=sfw;
	  evmSFWTid_=sfw.tid;
	  I2O_TID evmTid;
	  try
	   {
		  evmTid = i2o::utils::getAddressMap()->getTid(evm_->getApplicationDescriptor());
	   }
	   catch(xcept::Exception &e)
	   {
	     std::ostringstream msg;
	     msg << "Failed to get I2O TID for EVM ";
	     msg << instance.toString();
	     XCEPT_RETHROW(exception::I2O, msg.str(), e);
	   }
	  ruVector.push_back(evmTid);
	  std::ostringstream msg;
	        msg << "Got the EVM tid=" << evmTid ;
	  LOG4CPLUS_DEBUG(evm_->getApplicationLogger(), msg.str());
	  mappingSFWToRUTid_[sfw.tid]=ruVector;
  } else {
	  ruVector.push_back(mappingRUInstanceToTid_[instance]);
	  mappingSFWToRUTid_[sfw.tid]=ruVector;
  }
  participatingSFWs_.push_back(sfw);
  sfwTids_.push_back(sfw.tid);

}


cgicc::div sfb::evm::RUproxy::getHtmlSnipped() const
{
  using namespace cgicc;

  cgicc::div div;
  div.add(p("RUproxy"));

  {
    table table;
    table.set("title","Statistics of readout messages sent to the RUs. Normally, the allocate FIFO should be empty.");

    std::lock_guard<std::mutex> guard(allocateMonitoringMutex_);

    table.add(tr()
              .add(td("last event number to RUs"))
              .add(td(std::to_string(allocateMonitoring_.lastEventNumberToRUs))));
    table.add(tr()
              .add(td("throughput (kB/s)"))
              .add(td(doubleToString(allocateMonitoring_.throughput / 1e3,2))));
    table.add(tr()
              .add(td("assignment rate (Hz)"))
              .add(td(std::to_string(allocateMonitoring_.assignmentRate))));
    table.add(tr()
              .add(td("I2O rate (Hz)"))
              .add(td(std::to_string(allocateMonitoring_.i2oRate))));
    table.add(tr()
              .add(td("I2O retry rate (Hz)"))
              .add(td(doubleToString(allocateMonitoring_.retryRate,2))));
    table.add(tr()
              .add(td("I2O retry count"))
              .add(td(std::to_string(allocateMonitoring_.retryCount))));
    table.add(tr()
              .add(td("Events assigned/I2O"))
              .add(td(doubleToString(allocateMonitoring_.packingFactor,1))));

    const uint32_t activeRUs = ruTids_.empty() ? 0 : ruTids_.size()-1; //exclude the EVM TID
    table.add(tr()
              .add(td("# active RUs"))
              .add(td(std::to_string(activeRUs))));
    const uint32_t activeSFWs = sfwTids_.empty() ? 0 : sfwTids_.size();
    table.add(tr()
              .add(td("# active SFWs"))
              .add(td(std::to_string(activeSFWs))));

    div.add(table);
  }

  div.add(readoutMsgFIFO_.getHtmlSnipped());

  return div;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
