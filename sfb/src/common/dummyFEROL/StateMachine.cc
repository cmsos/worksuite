#include "sfb/Exception.h"
#include "sfb/DummyFEROL.h"
#include "sfb/dummyFEROL/StateMachine.h"
#include "sfb/dummyFEROL/States.h"


sfb::test::dummyFEROL::StateMachine::StateMachine
(
  DummyFEROL* dummyFEROL
):
  SfBStateMachine(dummyFEROL,EvBtype::dummy),
  dummyFEROL_(dummyFEROL)
{}


void sfb::test::dummyFEROL::Configuring::entryAction()
{
  doConfiguring_ = true;
  auto thread = std::thread(&sfb::test::dummyFEROL::Configuring::activity, this);
  thread.detach(); // otherwise, the thread tries to destruct itself on exiting this state
}


void sfb::test::dummyFEROL::Configuring::activity()
{
  outermost_context_type& stateMachine = outermost_context();

  std::string msg = "Failed to configure the components";
  try
  {
    if (doConfiguring_) stateMachine.dummyFEROL()->configure();
    if (doConfiguring_) stateMachine.processFSMEvent( ConfigureDone() );
  }
  catch(xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(exception::FSM,
                         sentinelException, msg, e);
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
  catch(std::exception& e)
  {
    msg += ": ";
    msg += e.what();
    XCEPT_DECLARE(exception::FSM,
                  sentinelException, msg );
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    msg += ": unknown exception";
    XCEPT_DECLARE(exception::FSM,
                  sentinelException, msg );
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
}


void sfb::test::dummyFEROL::Configuring::exitAction()
{
  doConfiguring_ = false;
}


void sfb::test::dummyFEROL::Running::entryAction()
{
  outermost_context_type& stateMachine = outermost_context();
  stateMachine.dummyFEROL()->startProcessing();
}


void sfb::test::dummyFEROL::Running::exitAction()
{
  outermost_context_type& stateMachine = outermost_context();
  stateMachine.dummyFEROL()->stopProcessing();
}


void sfb::test::dummyFEROL::Draining::entryAction()
{
  doDraining_ = true;
  auto thread = std::thread(&sfb::test::dummyFEROL::Draining::activity, this);
  thread.detach(); // otherwise, the thread tries to destruct itself on exiting this state
}


void sfb::test::dummyFEROL::Draining::activity()
{
  outermost_context_type& stateMachine = outermost_context();

  std::string msg = "Failed to drain the components";
  try
  {
    if (doDraining_) stateMachine.dummyFEROL()->drain();
    if (doDraining_) stateMachine.processFSMEvent( DrainingDone() );
  }
  catch(xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(exception::FSM,
                         sentinelException, msg, e);
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
  catch(std::exception& e)
  {
    msg += ": ";
    msg += e.what();
    XCEPT_DECLARE(exception::FSM,
                  sentinelException, msg );
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    msg += ": unknown exception";
    XCEPT_DECLARE(exception::FSM,
                  sentinelException, msg );
    stateMachine.processFSMEvent( Fail(sentinelException) );
  }
}


void sfb::test::dummyFEROL::Draining::exitAction()
{
  doDraining_ = false;
}


void sfb::test::dummyFEROL::Active::exitAction()
{
  outermost_context_type& stateMachine = outermost_context();
  stateMachine.dummyFEROL()->closeConnection();
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
