#include <memory>
#include <sched.h>
#include <utility>

#include "sfb/EVM.h"
#include "sfb/evm/Configuration.h"
#include "sfb/readoutunit/SuperFragmentBuilder.h"
#include "sfb/readoutunit/FedFragment.h"
#include "sfb/readoutunit/FerolConnectionManager.h"
#include "sfb/readoutunit/MetaDataRetrieverDIPBridge.h"

#include "sfb/readoutunit/Input.h"
#include "sfb/readoutunit/States.h"
#include "interface/shared/GlobalEventNumber.h"
#include "xgi/Method.h"


sfb::EVM::EVM(xdaq::ApplicationStub* app) :
  readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>(app,"/sfb/images/evm.png")
{
  this->stateMachine_ = std::make_shared<readoutunit::StateMachine<EVM>>(this);
  this->input_ = std::make_shared<readoutunit::Input<EVM,evm::Configuration>>(this);
  this->ferolConnectionManager_ = std::make_shared<readoutunit::FerolConnectionManager<EVM,evm::Configuration>>(this);
  this->ruProxy_ = std::make_shared<evm::RUproxy>(this,this->stateMachine_);
  this->superFragmentBuilder_ = std::make_shared<readoutunit::SuperFragmentBuilder<EVM>>(this);

  this->initialize();
  this->do_EVMbindI2oCallbacks();

  LOG4CPLUS_INFO(this->getApplicationLogger(), "End of constructor");
}

void sfb::EVM::timeExpired (toolbox::task::TimerEvent& e) {

	xdata::String maskedDipTopics =  this->configuration_->maskedDipTopics;
	sfb::readoutunit::MetaDataRetrieverDIPBridgePtr dipRetriver = this->input_.get()->getMetaDataRetrieverDIPBridgePtr();
	dipRetriver->registerToDipBridge(maskedDipTopics);

}

void sfb::EVM::do_appendMonitoringInfoSpaceItems(InfoSpaceItems& items)
{
  readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>::do_appendMonitoringInfoSpaceItems(items);
  ruProxy_->appendMonitoringItems(items);
}


void sfb::EVM::do_updateMonitoringInfo()
{
  readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>::do_updateMonitoringInfo();
  ruProxy_->updateMonitoringItems();
}


void sfb::EVM::do_handleItemChangedEvent(const std::string& item)
{
  if (item == "maxTriggerRate")
  {
    const uint32_t triggerRate = this->configuration_->maxTriggerRate;
    std::ostringstream msg;
    msg << "Setting maxTriggerRate to " << triggerRate << " Hz";
    LOG4CPLUS_INFO(this->getApplicationLogger(),msg.str());
    superFragmentBuilder_->setMaxTriggerRate(triggerRate);
  }
  else
  {
    readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>::do_handleItemChangedEvent(item);
  }
}

void sfb::EVM::do_EVMbindI2oCallbacks() {
	i2o::bind(this,
			&sfb::EVM::I2O_REQUEST_LUMISECTION_INFO_Callback,
			I2O_REQUEST_LUMISECTION_INFO, XDAQ_ORGANIZATION_ID);
}

void sfb::EVM::I2O_REQUEST_LUMISECTION_INFO_Callback(
		toolbox::mem::Reference *bufRef) {
	try {

		 const I2O_MESSAGE_FRAME* stdMsg =
		 (I2O_MESSAGE_FRAME*)bufRef->getDataLocation();
		 const msg::RequestLumiSectionInfoMsg* requestLumiSectionInfoMsg =
		 (msg::RequestLumiSectionInfoMsg*)stdMsg;

		 superFragmentBuilder_->sendLumiInfoToSFW(stdMsg->InitiatorAddress,requestLumiSectionInfoMsg->lumiSection,
		 input_->getEventCountForLumiSection(requestLumiSectionInfoMsg->lumiSection));

		bufRef->release();
		bufRef = 0;
	} catch (xcept::Exception &e) {
		if (bufRef)
			bufRef->release();
		this->stateMachine_->processFSMEvent(Fail(e));
	} catch (std::exception &e) {
		if (bufRef)
			bufRef->release();
		XCEPT_DECLARE(exception::I2O, sentinelException, e.what());
		this->stateMachine_->processFSMEvent(Fail(sentinelException));
	} catch (...) {
		if (bufRef)
			bufRef->release();
		XCEPT_DECLARE(exception::I2O, sentinelException, "unkown exception");
		this->stateMachine_->processFSMEvent(Fail(sentinelException));
	}
}

namespace sfb {
  namespace readoutunit {

    template<>
    uint32_t sfb::readoutunit::Input<EVM,evm::Configuration>::getLumiSectionFromTCDS(const DataLocations& dataLocations) const
    {
      using namespace evtn;

      uint32_t offset = sizeof(fedh_t) + 7 * SLINK_WORD_SIZE + SLINK_HALFWORD_SIZE;
      DataLocations::const_iterator it = dataLocations.begin();
      const DataLocations::const_iterator itEnd = dataLocations.end();

      while ( it != itEnd && offset > it->iov_len )
      {
        offset -= it->iov_len;
        ++it;
      }

      if ( it == itEnd )
      {
        std::ostringstream msg;
        msg << "Premature end of TCDS data block from FED " << TCDS_FED_ID;
        XCEPT_RAISE(exception::TCDS, msg.str());
      }

      return *(uint32_t*)((unsigned char*)it->iov_base + offset) & 0xffffffff;
    }


    template<>
    uint32_t sfb::readoutunit::Input<EVM,evm::Configuration>::getLumiSectionFromGTPe(const DataLocations& dataLocations) const
    {
      using namespace evtn;

      uint32_t offset = GTPE_ORBTNR_OFFSET * SLINK_HALFWORD_SIZE; // includes FED header

      DataLocations::const_iterator it = dataLocations.begin();
      const DataLocations::const_iterator itEnd = dataLocations.end();

      while ( it != itEnd && offset > it->iov_len )
      {
        offset -= it->iov_len;
        ++it;
      }

      if ( it == itEnd )
      {
        std::ostringstream msg;
        msg << "Premature end of GTPe data block from FED " << GTPe_FED_ID;
        XCEPT_RAISE(exception::TCDS, msg.str());
      }

      const uint32_t orbitNumber = *(uint32_t*)((unsigned char*)it->iov_base + offset);

      return (orbitNumber / ORBITS_PER_LS) + 1;
    }


    template<>
    void sfb::readoutunit::Input<EVM,evm::Configuration>::setMasterStream()
    {
      auto const& configuration = readoutUnit_->getConfiguration();

      masterStream_ = ferolStreams_.end();

      if ( ferolStreams_.empty() || configuration->dropInputData ) return;

      SfBidFactory::LumiSectionFunction lumiSectionFunction = 0;

      if ( configuration->getLumiSectionFromTrigger )
      {
        masterStream_ = ferolStreams_.find(TCDS_FED_ID);
        if ( masterStream_ != ferolStreams_.end() )
        {
          lumiSectionFunction = boost::bind(&sfb::readoutunit::Input<EVM,evm::Configuration>::getLumiSectionFromTCDS, this, _1);
          LOG4CPLUS_INFO(readoutUnit_->getApplicationLogger(), "Using TCDS as lumi section source");
        }
        else
        {
          masterStream_ = ferolStreams_.find(GTPe_FED_ID);
          if ( masterStream_ != ferolStreams_.end() )
          {
            lumiSectionFunction = boost::bind(&sfb::readoutunit::Input<EVM,evm::Configuration>::getLumiSectionFromGTPe, this, _1);
            LOG4CPLUS_INFO(readoutUnit_->getApplicationLogger(), "Using GTPe as lumi section source");
          }
        }
      }

      // If no magic FED id has been found, pick the first available FED in fedSourceIds
      xdata::Vector<xdata::UnsignedInteger32>::const_iterator it = configuration->fedSourceIds.begin();
      while ( masterStream_ == ferolStreams_.end() &&
              it != configuration->fedSourceIds.end() )
      {
        masterStream_ = ferolStreams_.find(*it);
        ++it;
      }

      // At this point the master stream must be defined
      assert( masterStream_ != ferolStreams_.end() );

      masterStream_->second->useAsMaster();

      const SfBidFactoryPtr& sfbIdFactory = masterStream_->second->getSfBidFactory();
      if ( lumiSectionFunction )
      {
        sfbIdFactory->setLumiSectionFunction(lumiSectionFunction);
      }
      else
      {
        const uint32_t lsDuration = configuration->fakeLumiSectionDuration;
        sfbIdFactory->setFakeLumiSectionDuration(lsDuration);

        std::ostringstream msg;
        msg << "Emulating a lumi section duration of " << lsDuration << "s";
        LOG4CPLUS_INFO(readoutUnit_->getApplicationLogger(),msg.str());
      }
    }



    template<>
    void SuperFragmentBuilder<EVM>::handleRequest(const msg::EventRequest* eventRequest, FragmentRequestPtr& fragmentRequest)
    {
      fragmentRequest->nbDiscards = eventRequest->nbDiscards;
      fragmentRequest->ruTids = readoutUnit_->getRUproxy()->getRUtis(eventRequest->sfwTid); // RU vector should include only 1 RU belong to the SFW Tid
      std::ostringstream msg;
      msg << "Got EventRequest from TID " << eventRequest->sfwTid << " ruTids=";
      if ( !fragmentRequest->ruTids.empty() )
      {
          msg << '[';
          for (auto i = fragmentRequest->ruTids.begin(); i != fragmentRequest->ruTids.end(); ++i)
          	  msg  << *i << ' ';
    	  msg << "]";
          }
      LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), msg.str());



      boost::upgrade_lock<boost::shared_mutex> sl(fragmentRequestFIFOsMutex_);
      std::ostringstream msg2;
      msg2 << "Comparison of eventRequest->sfwTid=" << eventRequest->sfwTid << " readoutUnit_->getRUproxy()->getEvmSFWTid())=" << readoutUnit_->getRUproxy()->getEvmSFWTid();
      LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), msg2.str());
      if (eventRequest->sfwTid == readoutUnit_->getRUproxy()->getEvmSFWTid()) {
    	  evmSFWFragmentRequestFIFO_->enqWait(std::move(fragmentRequest));
    	  LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), "Add fragmentRequest on evmSFWFragmentRequestFIFO_");
      } else {

		  // TODO implement the requests from RUs instead from BUss
		  auto pos = fragmentRequestFIFOs_.find(eventRequest->sfwTid);
		  if ( pos == fragmentRequestFIFOs_.end() )
		  {
			// new TID
			boost::upgrade_to_unique_lock< boost::shared_mutex > ul(sl);
			std::ostringstream name;
							name << "fragmentRequestFIFO_SFW" << eventRequest->sfwTid;
			FragmentRequestFIFOPtr requestFIFO = std::make_unique<FragmentRequestFIFO>(readoutUnit_,name.str());
			requestFIFO->resize(readoutUnit_->getConfiguration()->fragmentRequestFIFOCapacity);
			pos = fragmentRequestFIFOs_.emplace_hint(pos,
													 eventRequest->sfwTid,
													 std::move(requestFIFO));
			std::ostringstream msg;
			msg << "Created fragmentRequestFIFO_SWF for SWF_TID=" << eventRequest->sfwTid;
			LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(),msg.str() );
		  }

		  pos->second->enqWait(std::move(fragmentRequest));
		  std::ostringstream msg;
		  msg << "Add fragmentRequest on fragmentRequestFIFOs_ for SWF_TID=" << eventRequest->sfwTid;
		  LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), msg.str());
      }
    }

    template<>
    void SuperFragmentBuilder<EVM>::waitForNextTrigger(const uint16_t nbRequests)
    {
      if ( availableTriggers_ >= nbRequests )
      {
        availableTriggers_ -= nbRequests;
        return;
      }

      uint64_t now = 0;
      while ( availableTriggers_ < nbRequests )
      {
        now = getTimeStamp();
        if ( lastTime_ == 0 )
          availableTriggers_ = nbRequests;
        else
          availableTriggers_ = static_cast<uint32_t>(now>lastTime_ ? (now-lastTime_)/1e9*maxTriggerRate_ : 0);
      }
      lastTime_ = now;
      availableTriggers_ -= nbRequests;
    }


    template<>
    bool SuperFragmentBuilder<EVM>::processRequest(FragmentRequestPtr& fragmentRequest, SuperFragments& superFragments)
    {
      std::lock_guard<std::mutex> guard(processingRequestMutex_);

      SuperFragmentPtr superFragment;
      std::vector<FragmentRequestPtr> fragmentRequests;
      uint16_t nbRequests;

      try
      {
        {
          boost::shared_lock<boost::shared_mutex> frm(fragmentRequestFIFOsMutex_);
          if ( ! doProcessing_ ) return false;

          if (evmSFWFragmentRequestFIFO_== nullptr) return false;
          LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), "SuperFragmentBuilder<EVM>::processRequest after (evmSFWFragmentRequestFIFO_== nullptr)");

          if (evmSFWFragmentRequestFIFO_->empty()) return false;

          LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), "SuperFragmentBuilder<EVM>::processRequest before check fragmentRequestFIFOs_.empty()");

          std::ostringstream msg;
                msg << "------AAA readoutUnit_->getRUproxy()->getRUtids() ruTids=";
                if ( !readoutUnit_->getRUproxy()->getRUtids().empty() )
                {
                    msg << '[';
                    for (auto i = readoutUnit_->getRUproxy()->getRUtids().begin(); i != readoutUnit_->getRUproxy()->getRUtids().end(); ++i)
                    	  msg  << *i << ' ';
              	  msg << "]";
                    }
                msg << " FIFOSize=" << fragmentRequestFIFOs_.size() << " RUSize=" <<readoutUnit_->getRUproxy()->getRUtids().size();
                LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), msg.str());

          if ( (fragmentRequestFIFOs_.empty() &&  readoutUnit_->getRUproxy()->getRUtids().size()> 1) ||
        		  (readoutUnit_->getRUproxy()->getRUtids().size()> 1 && fragmentRequestFIFOs_.size() < readoutUnit_->getRUproxy()->getRUtids().size()-1)) return false;

          LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), "SuperFragmentBuilder<EVM>::processRequest before fragmentRequestFIFOs_. check");

          for (auto it = fragmentRequestFIFOs_.begin(); it != fragmentRequestFIFOs_.end(); ++it) {
        	  if (it->second->empty()){
        		  // the SFB needs to have at least one fragment request from all the active SFW
        		  // to start the super-fragment building
        		  return false;
        	  }
          }

          LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), "SuperFragmentBuilder<EVM>::processRequest before getNextAvailableSuperFragment(superFragment");
          if ( !readoutUnit_->getInput()->getNextAvailableSuperFragment(superFragment) ) return false;
          LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), "SuperFragmentBuilder<EVM>::processRequest After getNextAvailableSuperFragment(superFragment");
          assert( evmSFWFragmentRequestFIFO_->deq(fragmentRequest));
          LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), "SuperFragmentBuilder<EVM>::processRequest After assert evmSFWFragmentRequestFIFO_->deq(fragmentRequest)");
          for (auto it = fragmentRequestFIFOs_.begin(); it != fragmentRequestFIFOs_.end(); ++it) {
        	  // this must succeed as we checked that the queue is non-empty
        	  FragmentRequestPtr fragmentRequestTmp;
        	  assert( it->second->deq(fragmentRequestTmp) );
        	  fragmentRequests.push_back(std::move(fragmentRequestTmp));
          }
        }
		LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), "processRequest started");

        if ( maxTriggerRate_ > 0 ) waitForNextTrigger(fragmentRequest->nbRequests);

        fragmentRequest->sfbIds.clear();
        fragmentRequest->sfbIds.reserve(fragmentRequest->nbRequests);

        fragmentRequest->sfbIds.push_back( superFragment->getSfBid() );
        superFragments.push_back(std::move(superFragment));

        uint32_t remainingRequests = fragmentRequest->nbRequests  - 1;
        const uint64_t maxTries = readoutUnit_->getConfiguration()->maxTriggerAgeMSec*10;
        uint64_t tries = 0;
        while ( remainingRequests > 0 && tries < maxTries )
        {
          if ( readoutUnit_->getInput()->getNextAvailableSuperFragment(superFragment) )
          {
            fragmentRequest->sfbIds.push_back( superFragment->getSfBid() );
            superFragments.push_back(std::move(superFragment));
            --remainingRequests;
          }
          else
          {
            ::usleep(100);
            ++tries;
          }
        }
        --requestMonitoring_.activeRequests;
      }
      catch(exception::HaltRequested&)
      {
        return false;
      }
      nbRequests = fragmentRequest->sfbIds.size();
      fragmentRequest->nbRequests = nbRequests;

      for (auto &frp : fragmentRequests) {
    	frp->sfbIds = fragmentRequest->sfbIds;
    	frp->nbRequests = nbRequests;
    	frp->nbDiscards = nbRequests;
		readoutUnit_->getRUproxy()->sendRequest(frp);
      }

      return true;
    }


    template<>
    bool SuperFragmentBuilder<EVM>::isEmpty()
    {
      {
        std::lock_guard<std::mutex> guard(dataMonitoringMutex_);
        if ( dataMonitoring_.outstandingEvents != 0 ) return false;
      }
      {
        std::lock_guard<std::mutex> guard(processesActiveMutex_);
        if ( processesActive_.any() ) return false;
      }
      return true;
    }


    template<>
    std::string SuperFragmentBuilder<EVM>::getHelpTextForBuRequests() const
    {
      return "Event requests from the RUs. If no events are requested, the RUs can not write in the RAM disk or stuck with an issue in the ReadOut.";
    }

    template<>
    void Configuring<EVM>::doConfigure(const EVM* evm)
    {
      evm->getRUproxy()->configure();
      evm->getSuperFragmentBuilder()->setMaxTriggerRate(evm->getConfiguration()->maxTriggerRate);
      evm->getSuperFragmentBuilder()->cleanUPevmSFWFragmentRequestFIFO(evm->getRUproxy()->getEvmSFWTid());
    }


    template<>
    void Running<EVM>::doStartProcessing(const EVM* evm, const uint32_t runNumber)
    {
      evm->getRUproxy()->startProcessing();
    }


    template<>
    void Running<EVM>::doStopProcessing(const EVM* evm)
    {
      evm->getRUproxy()->stopProcessing();
    }


    template<>
    void Draining<EVM>::doDraining(const EVM* evm)
    {
      evm->getRUproxy()->drain();
    }


    template<>
    void readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>::addComponentsToWebPage
    (
      cgicc::table& table
    ) const
    {
      using namespace cgicc;

      table.add(tr()
                .add(td(this->input_->getHtmlSnipped()).set("class","xdaq-sfb-component").set("rowspan","2"))
                .add(td(img().set("src","/sfb/images/arrow_e.gif").set("alt","")))
                .add(td(dynamic_cast<const EVM*>(this)->getRUproxy()->getHtmlSnipped()).set("class","xdaq-sfb-component")));
      table.add(tr()
                .add(td(img().set("src","/sfb/images/arrow_e.gif").set("alt","")))
                .add(td(superFragmentBuilder_->getHtmlSnipped()).set("class","xdaq-sfb-component")));

    }
  }
}


/**
 * Provides the factory method for the instantiation of EVM applications.
 */
XDAQ_INSTANTIATOR_IMPL(sfb::EVM)



/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
