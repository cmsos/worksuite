#include <memory>
#include <utility>

#include "sfb/RU.h"

#include "sfb/readoutunit/SuperFragmentBuilder.h"
#include "sfb/readoutunit/Configuration.h"
#include "sfb/readoutunit/FerolConnectionManager.h"
#include "sfb/readoutunit/Input.h"
#include "sfb/readoutunit/States.h"


sfb::RU::RU(xdaq::ApplicationStub* app) :
  readoutunit::ReadoutUnit<RU,readoutunit::Configuration,readoutunit::StateMachine<RU>>(app,"/sfb/images/ru.png")
{
  this->stateMachine_ = std::make_shared<readoutunit::StateMachine<RU>>(this);
  this->input_ = std::make_shared<readoutunit::Input<RU,readoutunit::Configuration>>(this);
  this->ferolConnectionManager_ = std::make_shared<readoutunit::FerolConnectionManager<RU,readoutunit::Configuration>>(this);

  this->superFragmentBuilder_ = std::make_shared<readoutunit::SuperFragmentBuilder<RU>>(this);

  this->initialize();

  LOG4CPLUS_INFO(this->getApplicationLogger(), "End of constructor");
}

void sfb::RU::timeExpired (toolbox::task::TimerEvent& e) {

	xdata::String maskedDipTopics =  this->configuration_->maskedDipTopics;
	sfb::readoutunit::MetaDataRetrieverDIPBridgePtr dipRetriver = this->input_.get()->getMetaDataRetrieverDIPBridgePtr();
	dipRetriver->registerToDipBridge(maskedDipTopics);

}


namespace sfb {
  namespace readoutunit {


    template<>
    void SuperFragmentBuilder<RU>::handleRequest(const msg::EventRequest* eventRequest, FragmentRequestPtr& fragmentRequest)
    {
      eventRequest->getSfBids(fragmentRequest->sfbIds);
      eventRequest->getRUtids(fragmentRequest->ruTids);
      std::ostringstream msg;
      msg << "Got EventRequest from TID " << eventRequest->sfwTid << " ruTids=";
      if ( !fragmentRequest->ruTids.empty() )
      {
          msg << '[';
          for (auto i = fragmentRequest->ruTids.begin(); i != fragmentRequest->ruTids.end(); ++i)
          	  msg  << *i << ' ';

      	  }
		  msg << "]";
		  msg << " bxIds=[";
		  for (uint32_t i=0; i < fragmentRequest->nbRequests; ++i)
		  {
					  const SfBid& sfbId = fragmentRequest->sfbIds.at(i);
					  if (i> 0)
						  msg << ",";
					  msg << sfbId.bxId();
		  }
		  msg << "]";
      LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(), msg.str());

      fragmentRequestFIFO_.enqWait(std::move(fragmentRequest));
    }


    template<>
    bool SuperFragmentBuilder<RU>::processRequest(FragmentRequestPtr& fragmentRequest, SuperFragments& superFragments)
    {
      std::lock_guard<std::mutex> guard(processingRequestMutex_);

      if ( doProcessing_ && fragmentRequestFIFO_.deq(fragmentRequest) )
      {
        try
        {
          for (uint32_t i=0; i < fragmentRequest->nbRequests; ++i)
          {
            const SfBid& sfbId = fragmentRequest->sfbIds.at(i);
            SuperFragmentPtr superFragment;
            readoutUnit_->getInput()->getSuperFragmentWithSfBid(sfbId, superFragment);
            superFragments.push_back(std::move(superFragment));
          }
          --requestMonitoring_.activeRequests;

          return true;
        }
        catch(exception::HaltRequested&)
        {
          return false;
        }
      }

      return false;
    }


    template<>
    bool SuperFragmentBuilder<RU>::isEmpty()
    {
      std::lock_guard<std::mutex> guard(processesActiveMutex_);
      return ( fragmentRequestFIFO_.empty() && processesActive_.none() );
    }


    template<>
    std::string SuperFragmentBuilder<RU>::getHelpTextForBuRequests() const
    {
      return "RU event requests forwarded by the EVM. If no events are requested, the EVM has not got any requests or has no data.";
    }


    template<>
    void readoutunit::ReadoutUnit<RU,readoutunit::Configuration,readoutunit::StateMachine<RU>>::addComponentsToWebPage
    (
      cgicc::table& table
    ) const
    {
      using namespace cgicc;

      table.add(tr()
                .add(td(input_->getHtmlSnipped()).set("class","xdaq-sfb-component"))
                .add(td(img().set("src","/sfb/images/arrow_e.gif").set("alt","")))
				.add(td(superFragmentBuilder_->getHtmlSnipped()).set("class","xdaq-sfb-component")));
    }
  }
}


/**
 * Provides the factory method for the instantiation of RU applications.
 */
XDAQ_INSTANTIATOR_IMPL(sfb::RU)



/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
