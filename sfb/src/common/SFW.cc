#include "i2o/Method.h"
#include "interface/shared/i2ogevb2g.h"
#include "sfb/SFW.h"
#include "sfb/superfragmentwriter/DiskWriter.h"
#include "sfb/superfragmentwriter/ResourceManager.h"
#include "sfb/superfragmentwriter/RUproxy.h"
#include "sfb/superfragmentwriter/StateMachine.h"
#include "sfb/InfoSpaceItems.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"

#include <math.h>
#include <sfb/superfragmentwriter/SuperFragmentHandler.h>
#include <memory>


sfb::SFW::SFW(xdaq::ApplicationStub* app) :
  SfBApplication<superfragmentwriter::Configuration,superfragmentwriter::StateMachine>(app,"/sfb/images/sfw.png")
{
  resourceManager_ = std::make_shared<superfragmentwriter::ResourceManager>(this);
  diskWriter_ = std::make_shared<superfragmentwriter::DiskWriter>(this, resourceManager_);
  superFragmentHandler_ = std::make_shared<superfragmentwriter::SuperFragmentHandler>(this, diskWriter_, resourceManager_);
  ruProxy_ = std::make_shared<superfragmentwriter::RUproxy>(this, superFragmentHandler_, resourceManager_);
  stateMachine_ = std::make_shared<superfragmentwriter::StateMachine>(this,
                                                     ruProxy_, diskWriter_,
                                                     superFragmentHandler_, resourceManager_);

  initialize();

  LOG4CPLUS_INFO(getApplicationLogger(), "End of constructor");
}


uint32_t sfb::SFW::getTotalEventsInLumiSection(const uint32_t lumiSection) const
{
  return ruProxy_->getTotalEventsInLumiSection(lumiSection);
}


void sfb::SFW::do_appendApplicationInfoSpaceItems
(
  InfoSpaceItems& appInfoSpaceParams
)
{
  eventSize_ = 0;
  eventRate_ = 0;
  throughput_ = 0;
  nbEventsInBU_ = 0;
  nbEventsBuilt_ = 0;
  nbLumiSections_ = 0;
  nbCorruptedEvents_ = 0;
  nbEventsWithCRCerrors_ = 0;
  nbEventsMissingData_ = 0;

  appInfoSpaceParams.add("eventSize", &eventSize_, InfoSpaceItems::retrieve);
  appInfoSpaceParams.add("eventRate", &eventRate_, InfoSpaceItems::retrieve);
  appInfoSpaceParams.add("throughput", &throughput_, InfoSpaceItems::retrieve);
  appInfoSpaceParams.add("nbEventsInBU", &nbEventsInBU_, InfoSpaceItems::retrieve);
  appInfoSpaceParams.add("nbEventsBuilt", &nbEventsBuilt_, InfoSpaceItems::retrieve);
  appInfoSpaceParams.add("nbLumiSections", &nbLumiSections_, InfoSpaceItems::retrieve);
  appInfoSpaceParams.add("nbCorruptedEvents", &nbCorruptedEvents_, InfoSpaceItems::retrieve);
  appInfoSpaceParams.add("nbEventsWithCRCerrors", &nbEventsWithCRCerrors_, InfoSpaceItems::retrieve);
  appInfoSpaceParams.add("nbEventsMissingData", &nbEventsMissingData_, InfoSpaceItems::retrieve);
}


void sfb::SFW::timeExpired (toolbox::task::TimerEvent& e) {
	 LOG4CPLUS_ERROR(getApplicationLogger(), "The timeExperied should be called only for the EVM application.");
}


void sfb::SFW::do_appendMonitoringInfoSpaceItems
(
  InfoSpaceItems& monitoringParams
)
{
  diskWriter_->appendMonitoringItems(monitoringParams);
  resourceManager_->appendMonitoringItems(monitoringParams);
  superFragmentHandler_->appendMonitoringItems(monitoringParams);
  ruProxy_->appendMonitoringItems(monitoringParams);
  stateMachine_->appendMonitoringItems(monitoringParams);
}


void sfb::SFW::do_updateMonitoringInfo()
{
  diskWriter_->updateMonitoringItems();
  resourceManager_->updateMonitoringItems();
  superFragmentHandler_->updateMonitoringItems();
  ruProxy_->updateMonitoringItems();
  stateMachine_->updateMonitoringItems();
}


void sfb::SFW::do_handleItemRetrieveEvent(const std::string& item)
{
  if (item == "eventSize")
    eventSize_ = resourceManager_->getEventSize();
  else if (item == "eventRate")
    eventRate_ = resourceManager_->getEventRate();
  else if (item == "throughput")
    throughput_ = resourceManager_->getThroughput();
  else if (item == "nbEventsInBU")
    nbEventsInBU_ = resourceManager_->getNbEventsInBU();
  else if (item == "nbEventsBuilt")
    nbEventsBuilt_ = resourceManager_->getNbEventsBuilt();
  else if (item == "nbLumiSections")
    nbLumiSections_ = diskWriter_->getNbLumiSections();
  else if (item == "nbCorruptedEvents")
    nbCorruptedEvents_ = superFragmentHandler_->getNbCorruptedEvents();
  else if (item == "nbEventsWithCRCerrors")
    nbEventsWithCRCerrors_ = superFragmentHandler_->getNbEventsWithCRCerrors();
  else if (item == "nbEventsMissingData")
    nbEventsMissingData_ = superFragmentHandler_->getNbEventsMissingData();
}


void sfb::SFW::do_bindI2oCallbacks()
{
  i2o::bind
    (
      this,
      &sfb::SFW::I2O_BU_CACHE_Callback,
      I2O_BU_CACHE,
      XDAQ_ORGANIZATION_ID
    );
  i2o::bind
    (
      this,
      &sfb::SFW::I2O_LUMISECTION_INFO_Callback,
      I2O_LUMISECTION_INFO,
      XDAQ_ORGANIZATION_ID
    );
}


void sfb::SFW::I2O_BU_CACHE_Callback
(
  toolbox::mem::Reference* bufRef
)
{
  try
  {
    ruProxy_->superFragmentCallback(bufRef);
  }
  catch(xcept::Exception& e)
  {
    stateMachine_->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, e.what());
    this->stateMachine_->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, "unkown exception");
    this->stateMachine_->processFSMEvent( Fail(sentinelException) );
  }
}


void sfb::SFW::I2O_LUMISECTION_INFO_Callback
(
  toolbox::mem::Reference* bufRef
)
{
  try
  {
    ruProxy_->lumisectionInfoCallback(bufRef);
  }
  catch(xcept::Exception& e)
  {
    stateMachine_->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, e.what());
    this->stateMachine_->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    XCEPT_DECLARE(exception::I2O,
                  sentinelException, "unkown exception");
    this->stateMachine_->processFSMEvent( Fail(sentinelException) );
  }
}


void sfb::SFW::bindNonDefaultXgiCallbacks()
{
  xgi::framework::deferredbind(this, this,
                               &sfb::SFW::displayResourceTable,
                               "resourceTable");

  xgi::bind(
    this,
    &sfb::SFW::writeNextEventsToFile,
    "writeNextEventsToFile"
  );
}


cgicc::table sfb::SFW::getMainWebPage() const
{
  using namespace cgicc;

  table layoutTable;
  layoutTable.set("class","xdaq-sfb-layout");
  layoutTable.add(colgroup()
                  .add(col())
                  .add(col().set("class","xdaq-sfb-arrow"))
                  .add(col())
                  .add(col().set("class","xdaq-sfb-arrow"))
                  .add(col()));
  layoutTable.add(tr()
                  .add(td(this->getWebPageBanner()).set("colspan","5")));

  layoutTable.add(tr()
                  .add(td(ruProxy_->getHtmlSnipped()).set("class","xdaq-sfb-component").set("rowspan","3"))
                  .add(td(img().set("src","/sfb/images/arrow_e.gif").set("alt","")))
                  .add(td(resourceManager_->getHtmlSnipped()).set("class","xdaq-sfb-component"))
                  .add(td(img().set("src","/sfb/images/arrow_w.gif").set("alt","")))
                  .add(td(diskWriter_->getHtmlSnipped()).set("class","xdaq-sfb-component").set("rowspan","3")));

  layoutTable.add(tr()
                  .add(td(" "))
                  .add(td(img().set("src","/sfb/images/arrow_ns.gif").set("alt","")))
                  .add(td(" ")));

  layoutTable.add(tr()
                  .add(td(img().set("src","/sfb/images/arrow_e.gif").set("alt","")))
                  .add(td(superFragmentHandler_->getHtmlSnipped()).set("class","xdaq-sfb-component"))
                  .add(td(img().set("src","/sfb/images/arrow_e.gif").set("alt",""))));

  return layoutTable;
}


void sfb::SFW::displayResourceTable(xgi::Input* in,xgi::Output* out)
{
  using namespace cgicc;
  table layoutTable;

  layoutTable.set("class","xdaq-sfb-layout");
  layoutTable.add(tr()
                  .add(td(getWebPageBanner())));
  layoutTable.add(tr()
                  .add(td(resourceManager_->getHtmlSnippedForResourceTable())));

  *out << getWebPageHeader();
  *out << layoutTable;
}


void sfb::SFW::writeNextEventsToFile(xgi::Input* in,xgi::Output* out)
{
  cgicc::Cgicc cgi(in);
  uint16_t count = 1;

  if ( xgi::Utils::hasFormElement(cgi,"count") )
    count = xgi::Utils::getFormElement(cgi, "count")->getIntegerValue();

  superFragmentHandler_->writeNextEventsToFile(count);
}

/**
 * Provides the factory method for the instantiation of RU applications.
 */
XDAQ_INSTANTIATOR_IMPL(sfb::SFW)


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
