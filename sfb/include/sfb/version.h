#ifndef _sfb_version_h_
#define _sfb_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_SFB_VERSION_MAJOR 1
#define WORKSUITE_SFB_VERSION_MINOR 1
#define WORKSUITE_SFB_VERSION_PATCH 2 
#undef WORKSUITE_SFB_PREVIOUS_VERSIONS

#define WORKSUITE_SFB_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SFB_VERSION_MAJOR,WORKSUITE_SFB_VERSION_MINOR,WORKSUITE_SFB_VERSION_PATCH)
#ifndef WORKSUITE_SFB_PREVIOUS_VERSIONS
#define WORKSUITE_SFB_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SFB_VERSION_MAJOR,WORKSUITE_SFB_VERSION_MINOR,WORKSUITE_SFB_VERSION_PATCH)
#else
#define WORKSUITE_SFB_FULL_VERSION_LIST  WORKSUITE_SFB_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SFB_VERSION_MAJOR,WORKSUITE_SFB_VERSION_MINOR,WORKSUITE_SFB_VERSION_PATCH)
#endif


namespace sfb
{
  const std::string project = "worksuite";
  const std::string package = "sfb";
  const std::string versions = WORKSUITE_SFB_FULL_VERSION_LIST;
  const std::string version = PACKAGE_VERSION_STRING(WORKSUITE_SFB_VERSION_MAJOR,WORKSUITE_SFB_VERSION_MINOR,WORKSUITE_SFB_VERSION_PATCH);
  const std::string description = "The CMS Super-fragment builder";
  const std::string summary = " Super-fragment builder library";
  const std::string authors = "Andrea Petrucci, Remi Mommsen";
  const std::string link = "https://svnweb.cern.ch/trac/cmsos";

  config::PackageInfo getPackageInfo();

  void checkPackageDependencies();

  std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif
