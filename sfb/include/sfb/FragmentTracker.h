#ifndef _sfb_FragmentTracker_h_
#define _sfb_FragmentTracker_h_

#include <memory>
#include <ostream>
#include <cstdint>

#include "sfb/CRCCalculator.h"
#include "sfb/SfBid.h"
#include "sfb/FragmentSize.h"


namespace sfb {

  class FragmentTracker
  {
  public:

    /**
     * Constructor.
     */
    FragmentTracker
    (
      const uint32_t fedId,
      const uint32_t fedSize,
      const uint32_t fedSizeStdDev,
      const uint32_t minFedSize,
      const uint32_t maxFedSize,
      const bool computeCRC
    );

    /**
     * Start the clock for determining the available triggers
     */
    void startRun();

    /**
    */
    void setMaxTriggerRate(const uint32_t rate)
    { maxTriggerRate_ = rate; }

    /**
     * Starts a new FED fragment with the specified event number.
     * Return the size of the FED data.
     */
    uint32_t startFragment(const SfBid&);

    /**
     * Fill the FED data into the payload using at most
     * nbBytesAvailable Bytes. It returns the number of
     * Bytes actually filled.
     */
    size_t fillData
    (
      unsigned char* payload,
      const size_t nbBytesAvailable
    );


  private:

    void waitForNextTrigger();

    CRCCalculator crcCalculator_;
    std::unique_ptr<FragmentSize> fragmentSize_;

    enum FedComponent
    {
      FED_HEADER,
      FED_PAYLOAD,
      FED_TRAILER
    };

    const uint32_t fedId_;
    const uint32_t fedSize_;
    const uint32_t minFedSize_;
    const uint32_t maxFedSize_;
    uint32_t maxTriggerRate_;
    const bool computeCRC_;
    uint16_t fedCRC_;
    FedComponent typeOfNextComponent_;
    uint32_t currentFedSize_;
    uint32_t remainingFedSize_;
    SfBid sfbId_;
    uint64_t lastTime_;
    uint32_t availableTriggers_;
  };

  using FragmentTrackerPtr = std::shared_ptr<FragmentTracker>;

} // namespace sfb

#endif // _sfb_FragmentTracker_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
