#ifndef _sfb_evm_configuration_h_
#define _sfb_evm_configuration_h_

#include <memory>
#include <cstdint>

#include "sfb/Exception.h"
#include "sfb/InfoSpaceItems.h"
#include "sfb/UnsignedInteger32Less.h"
#include "sfb/readoutunit/Configuration.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"


namespace sfb {

  namespace evm {

    /**
     * \ingroup xdaqApps
     * \brief Configuration for the readout units (EVM/RU)
     */
    struct Configuration : public readoutunit::Configuration
    {
      xdata::UnsignedInteger32 maxTriggerRate;               // Maximum trigger rate in Hz. 0 means no limitation.
      xdata::Vector<xdata::UnsignedInteger32> ruInstances;   // Vector of RU instances served from the EVM
      xdata::Vector<xdata::UnsignedInteger32> sfwInstances;   // Vector of SWF instances served from the EVM
      xdata::UnsignedInteger32 maxTriggerAgeMSec;            // Maximum time in milliseconds before sending a response to event requests
      xdata::Boolean getLumiSectionFromTrigger;              // If set to true, try to get the lumi section number from the trigger. Otherwise, use fake LS
      xdata::UnsignedInteger32 fakeLumiSectionDuration;      // Duration in seconds of a fake luminosity section. If 0, don't generate lumi sections
      xdata::UnsignedInteger32 allocateFIFOCapacity;         // Capacity of the FIFO to store allocation messages
      xdata::UnsignedInteger32 allocateBlockSize;            // I2O block size used for packing readout msg from EVM to RUs
      xdata::UnsignedInteger32 maxAllocateTime;              // Maximum time in microseconds waited while packing readout msg from EVM to RUs

      Configuration()
        : maxTriggerRate(0),
          maxTriggerAgeMSec(1000),
          getLumiSectionFromTrigger(true),
          fakeLumiSectionDuration(0),
          allocateFIFOCapacity(1440),
          allocateBlockSize(8192),
          maxAllocateTime(250)
      {};

      void addToInfoSpace
      (
        InfoSpaceItems& params,
        xdaq::ApplicationContext* context
      )
      {
        readoutunit::Configuration::addToInfoSpace(params,context);
        fillDefaultRUinstances(context);
        fillDefaultSFWinstances(context);

        params.add("maxTriggerRate", &maxTriggerRate, InfoSpaceItems::change);
        params.add("ruInstances", &ruInstances);
        params.add("sfwInstances", &sfwInstances);
        params.add("maxTriggerAgeMSec", &maxTriggerAgeMSec);
        params.add("getLumiSectionFromTrigger", &getLumiSectionFromTrigger);
        params.add("fakeLumiSectionDuration", &fakeLumiSectionDuration);
        params.add("allocateFIFOCapacity", &allocateFIFOCapacity);
        params.add("allocateBlockSize", &allocateBlockSize);
        params.add("maxAllocateTime", &maxAllocateTime);
      }

      void fillDefaultRUinstances(xdaq::ApplicationContext* context)
      {
        ruInstances.clear();

        std::set<const xdaq::ApplicationDescriptor*> ruDescriptors;

        try
        {
          ruDescriptors =
            context->getDefaultZone()->
            getApplicationDescriptors("sfb::RU");
        }
        catch(xcept::Exception& e)
        {
          XCEPT_RETHROW(exception::I2O,
                        "Failed to get RU application descriptor", e);
        }

        for (auto const& ru : ruDescriptors)
        {
          ruInstances.push_back(ru->getInstance());
        }

        std::sort(ruInstances.begin(), ruInstances.end(),
                  UnsignedInteger32Less());
      }

    void fillDefaultSFWinstances(xdaq::ApplicationContext* context)
    {
      sfwInstances.clear();

      std::set<const xdaq::ApplicationDescriptor*> sfwDescriptors;

      try
      {
        sfwDescriptors =
          context->getDefaultZone()->
          getApplicationDescriptors("sfb::SFW");
      }
      catch(xcept::Exception& e)
      {
        XCEPT_RETHROW(exception::I2O,
                      "Failed to get SFW application descriptor", e);
      }

      for (auto const& sfw : sfwDescriptors)
      {
        sfwInstances.push_back(sfw->getInstance());
      }

      std::sort(sfwInstances.begin(), sfwInstances.end(),
                UnsignedInteger32Less());
    }
  };

  } } // namespace sfb::evm

#endif // _sfb_evm_configuration_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
