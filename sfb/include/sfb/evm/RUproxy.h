#ifndef _sfb_evm_RUproxy_h_
#define _sfb_evm_RUproxy_h_

#include <atomic>
#include <cstdint>
#include <memory>
#include <map>

#include "cgicc/HTMLClasses.h"
#include "sfb/ApplicationDescriptorAndTid.h"
#include "sfb/SfBid.h"
#include "sfb/I2OMessages.h"
#include "sfb/InfoSpaceItems.h"
#include "sfb/OneToOneQueue.h"
#include "sfb/PerformanceMonitor.h"
#include "sfb/readoutunit/FragmentRequest.h"
#include "sfb/readoutunit/StateMachine.h"
#include "toolbox/lang/Class.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "xdaq/Application.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xgi/Output.h"


namespace sfb {

  class EVM;

  namespace evm { // namespace sfb::evm

    /**
     * \ingroup xdaqApps
     * \brief Proxy for EVM-RU communication
     */

    class RUproxy : public toolbox::lang::Class
    {

    public:

      RUproxy
      (
        EVM*,
        std::shared_ptr<readoutunit::StateMachine<EVM>>
      );

      ~RUproxy();

      /**
       * Send the request for event fragments to all RUs
       */
      void sendRequest(readoutunit::FragmentRequestPtr);

      /**
       * Append the info space items to be published in the
       * monitoring info space to the InfoSpaceItems
       */
      void appendMonitoringItems(InfoSpaceItems&);

      /**
       * Update all values of the items put into the monitoring
       * info space. The caller has to make sure that the info
       * space where the items reside is locked and properly unlocked
       * after the call.
       */
      void updateMonitoringItems();

      /**
       * Configure
       */
      void configure();

      /**
       * Start processing events
       */
      void startProcessing();

      /**
       * Drain events
       */
      void drain();

      /**
       * Stop processing events
       */
      void stopProcessing();

      /**
       * Return the tids of RUs participating in the event building
       */
      std::vector<I2O_TID>& getRUtids()
      { return ruTids_; }

      std::vector<I2O_TID>& getRUtis(I2O_TID sfwTid){
    	  return mappingSFWToRUTid_[sfwTid];
      }

      /**
       * Return the tids of SWFs participating in the event building
       */
      std::vector<I2O_TID>& getSWTtids()
      { return sfwTids_; }

      /**
       * Return the tid of SWF belonging to the EVM
       */
      I2O_TID& getEvmSFWTid()
      { return evmSFWTid_; }

      /**
       * Return monitoring information as cgicc snipped
       */
      cgicc::div getHtmlSnipped() const;

    private:

      void resetMonitoringCounters();
      void startRequestWorkLoop();
      toolbox::mem::Reference* getRequestMsgBuffer(const uint32_t bufSize);
      void sendMsgToRUs(toolbox::mem::Reference*&, const uint32_t msgSize, uint32_t& requestCount,const I2O_TID ruToSentTID );
      bool processRequests(toolbox::task::WorkLoop*);
      void getApplicationDescriptors();
      void fillRUInstance(xdata::UnsignedInteger32 instance);
      void fillSFWInstance(xdata::UnsignedInteger32 instance);
      EVM* evm_;
      std::shared_ptr<readoutunit::StateMachine<EVM>> stateMachine_;

      toolbox::mem::Pool* msgPool_;

      using ReadoutMsgFIFO = OneToOneQueue<readoutunit::FragmentRequestPtr>;
      ReadoutMsgFIFO readoutMsgFIFO_;

      toolbox::task::WorkLoop* processRequestsWL_;
      toolbox::task::ActionSignature* processRequestsAction_;
      volatile std::atomic<bool> doProcessing_;
      volatile std::atomic<bool> draining_;
      volatile std::atomic<bool> processingActive_;

      I2O_TID tid_;
      uint32_t ruCount_;
      uint32_t sfwCount_;


      using ApplicationDescriptorsAndTids = std::vector<ApplicationDescriptorAndTid>;
      using RUInstanceToTidMap = std::unordered_map<uint32_t,I2O_TID>;
      using SFWToRUTidMap = std::unordered_map<I2O_TID,std::vector<I2O_TID>>;
      std::unordered_map<I2O_TID,ApplicationDescriptorAndTid> participatingRUs_;
      RUInstanceToTidMap mappingRUInstanceToTid_;
      SFWToRUTidMap mappingSFWToRUTid_;
      std::vector<I2O_TID> ruTids_;
      ApplicationDescriptorAndTid evmSFW_;
      I2O_TID evmSFWTid_;
      ApplicationDescriptorsAndTids participatingSFWs_;
      std::vector<I2O_TID> sfwTids_;

      using FrameFIFO = OneToOneQueue<toolbox::mem::Reference*>;
      using FrameFIFOPtr = std::unique_ptr<FrameFIFO>;
      mutable boost::shared_mutex ruConnectionsMutex_;

      struct AllocateMonitoring
      {
        uint32_t lastEventNumberToRUs;
        uint64_t throughput;
        uint32_t assignmentRate;
        uint32_t i2oRate;
        double retryRate;
        uint32_t retryCount;
        double packingFactor;
        PerformanceMonitor perf;
     } allocateMonitoring_;
      mutable std::mutex allocateMonitoringMutex_;

      xdata::UnsignedInteger32 allocateRate_;
      xdata::Double allocateRetryRate_;
      xdata::UnsignedInteger32 allocateRetryCount_;

    };


  } //namespace sfb::evm


} //namespace sfb

#endif // _sfb_evm_RUproxy_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
