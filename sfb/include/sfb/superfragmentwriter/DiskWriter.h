#ifndef _sfb_superfragmentwriter_DiskWriter_h_
#define _sfb_superfragmentwriter_DiskWriter_h_

#include <boost/filesystem/convenience.hpp>

#include <atomic>
#include <cstdint>
#include <curl/curl.h>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include "cgicc/HTMLClasses.h"
#include "sfb/superfragmentwriter/Configuration.h"
#include "sfb/superfragmentwriter/Event.h"
#include "sfb/superfragmentwriter/FileInfo.h"
#include "sfb/superfragmentwriter/StreamHandler.h"
#include "sfb/InfoSpaceItems.h"
#include "sfb/OneToOneQueue.h"
#include "toolbox/lang/Class.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoop.h"
#include "xdata/UnsignedInteger32.h"


namespace sfb {

  class SFW;

  namespace superfragmentwriter { // namespace sfb::superfragmentwriter

    class ResourceManager;
    class StateMachine;

    /**
     * \ingroup xdaqApps
     * \brief Write events to disk
     */

    class DiskWriter : public toolbox::lang::Class
    {
    public:

      DiskWriter
      (
        SFW*,
        std::shared_ptr<ResourceManager>
      );

      ~DiskWriter();

      /**
       * Process the event
       */
      void handleEvent(EventPtr&&);

      /**
       * Get the next file info.
       * Return false if no event is available
       */
      bool getNextFileInfo(FileInfoPtr&);

      /**
       * Append the info space items to be published in the
       * monitoring info space to the InfoSpaceItems
       */
      void appendMonitoringItems(InfoSpaceItems&);

      /**
       * Update all values of the items put into the monitoring
       * info space. The caller has to make sure that the info
       * space where the items reside is locked and properly unlocked
       * after the call.
       */
      void updateMonitoringItems();

      /**
       * Configure
       */
      void configure();

      /**
       * Start processing messages
       */
      void startProcessing(const uint32_t runNumber);

      /**
       * Drain messages
       */
      void drain();

      /**
       * Stop processing messages
       */
      void stopProcessing();

      /**
       * Return monitoring information as cgicc snipped
       */
      cgicc::div getHtmlSnipped() const;

      /**
       * Return the number of lumi sections since the start of the run
       */
      uint32_t getNbLumiSections() const;

      void resetMonitoringCounters();

    private:

      struct LumiInfo
      {
        const uint32_t lumiSection;
        uint32_t totalEvents;
        uint32_t nbEvents;
        uint32_t nbEventsWritten;
        uint64_t nbBytesWritten;
        uint32_t nbIncompleteEvents;
        uint32_t fileCount;
        uint32_t index;
        bool isEmpty;

        LumiInfo(const uint32_t ls)
          : lumiSection(ls),totalEvents(0),nbEvents(0),nbEventsWritten(0),nbBytesWritten(0),
            nbIncompleteEvents(0),fileCount(0),index(0),isEmpty(false) {};

        bool isComplete() const
        { return isEmpty || (nbEvents > 0 && nbEvents == nbEventsWritten+nbIncompleteEvents); }
      };
      using LumiInfoPtr = std::unique_ptr<LumiInfo>;
      using LumiStatistics = std::map<uint32_t,LumiInfoPtr>;
      LumiStatistics lumiStatistics_;
      std::mutex lumiStatisticsMutex_;

      bool getReadyFileInfo(FileInfoPtr&);
      bool populateFileInfo(FileInfoPtr&);
      void startFileAccounting();
      bool idle() const;
      bool fileAccounting(toolbox::task::WorkLoop*);
      bool processLumiSections(const bool completeLumiSectionsOnly);
      bool closeStaleFiles();
      bool closeLumiSections();
      bool moveFiles();
      void handleRawDataFile(const FileInfoPtr&);
      LumiStatistics::iterator getLumiStatistics(const uint32_t lumiSection);
      void createDir(const boost::filesystem::path&) const;
      void removeDir(const boost::filesystem::path&) const;
      void closeAnyOldRuns() const;
      void populateHltdDirectory(const boost::filesystem::path& runDir) const;
      void getHLTmenu(const boost::filesystem::path& tmpDir) const;
      void writeHLTinfo(const boost::filesystem::path& tmpDir) const;
      void writeBlacklist(const boost::filesystem::path& tmpDir) const;
      void writeWhitelist(const boost::filesystem::path& tmpDir) const;
      uint16_t writeHostList(const boost::filesystem::path&, const xdata::String& hosts) const;
      void retrieveFromURL(CURL*, const std::string& url, const boost::filesystem::path& output) const;
      void createLockFile(const boost::filesystem::path&) const;
      void writeEoLS(const LumiInfoPtr&) const;
      void writeEoR() const;
      void defineEoLS(const boost::filesystem::path& jsdDir);
      void defineEoR(const boost::filesystem::path& jsdDir);

      SFW* sfw_;
      std::shared_ptr<ResourceManager> resourceManager_;
      const ConfigurationPtr& configuration_;

      const uint32_t sfwInstance_;
      uint32_t runNumber_;
      uint64_t tmpFileIndex_;

      boost::filesystem::path runRawDataDir_;
      boost::filesystem::path runMetaDataDir_;
      boost::filesystem::path rawDataDefFile_;
      boost::filesystem::path eolsDefFile_;
      boost::filesystem::path eorDefFile_;

      using EventFIFO = OneToOneQueue<EventPtr>;
      EventFIFO eventFIFO_;
      mutable std::mutex eventFIFOmutex_;

      using FileInfoMap = std::unordered_map<uint32_t,FileInfoPtr>;
      FileInfoMap fileInfoMap_;
      mutable std::mutex fileInfoMapMutex_;

      using FileInfoFIFO = OneToOneQueue<FileInfoPtr>;
      FileInfoFIFO fileInfoFIFO_;
      mutable std::mutex fileInfoFIFOmutex_;

      using StreamHandlers = std::vector<StreamHandlerPtr>;
      StreamHandlers streamHandlers_;

      toolbox::task::WorkLoop* fileAccountingWorkLoop_;
      toolbox::task::ActionSignature* fileAccountingAction_;
      volatile std::atomic<bool> doProcessing_;
      volatile std::atomic<bool> active_;

      struct DiskWriterMonitoring
      {
        uint32_t nbFiles;
        uint32_t nbEventsWritten;
        uint32_t nbLumiSections;
        uint32_t lastLumiSection;
        uint32_t currentLumiSection;
      } diskWriterMonitoring_;
      mutable std::mutex diskWriterMonitoringMutex_;

      xdata::UnsignedInteger32 nbFilesWritten_;
      xdata::UnsignedInteger32 nbLumiSections_;
      xdata::UnsignedInteger32 currentLumiSection_;
      xdata::String fuGroup_;
    };

  } } // namespace sfb::superfragmentwriter


#endif // _sfb_superfragmentwriter_DiskWriter_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
