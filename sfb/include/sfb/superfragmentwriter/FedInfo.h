#ifndef _sfb_superfragmentwriter_FedInfo_h_
#define _sfb_superfragmentwriter_FedInfo_h_

#include <memory>
#include <cstdint>
#include <sys/uio.h>
#include <vector>

#include "sfb/CRCCalculator.h"
#include "sfb/DataLocations.h"
#include "interface/shared/fed_header.h"
#include "interface/shared/fed_trailer.h"


namespace sfb {
  namespace superfragmentwriter {

    class FedInfo
    {
    public:
      FedInfo(const unsigned char* pos, uint32_t& remainingLength);
      void addDataChunk(const unsigned char* pos, uint32_t& remainingLength);
      void checkData(const uint32_t eventNumber, const bool computeCRC) const;

      bool complete() const { return (remainingFedSize_ == 0); }

      uint32_t eventId()  const { return (header_?FED_LVL1_EXTRACT(header_->eventid):0); }
      uint16_t fedId()    const { return (header_?FED_SOID_EXTRACT(header_->sourceid):0); }
      uint32_t fedSize()  const { return FED_EVSZ_EXTRACT(trailer_->eventsize)<<3; }
      uint16_t crc()      const { return FED_CRCS_EXTRACT(trailer_->conscheck); }

    private:
      fedh_t* header_;
      fedt_t* trailer_;

      DataLocations fedData_;
      uint32_t remainingFedSize_;

      static CRCCalculator crcCalculator_;
    };

    using FedInfoPtr = std::unique_ptr<FedInfo>;

  } } // namespace sfb::superfragmentwriter

#endif // _sfb_superfragmentwriter_FedInfo_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
