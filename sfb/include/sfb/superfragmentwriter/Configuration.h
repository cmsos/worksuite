#ifndef _sfb_superfragmentwriter_configuration_h_
#define _sfb_superfragmentwriter_configuration_h_

#include <memory>
#include <cstdint>

#include "sfb/InfoSpaceItems.h"
#include "xdaq/ApplicationContext.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Integer32.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"


namespace sfb {

  namespace superfragmentwriter {

    /**
     * \ingroup xdaqApps
     * \brief Configuration for the Disk Writer (DW)
     */
    struct Configuration
    {
      xdata::String sendPoolName;                          // The pool name used for sfb messages
      xdata::Integer32 evmInstance;                        // Instance of the EVM. If not set, discover the EVM over I2O.
      xdata::UnsignedInteger32 maxEvtsUnderConstruction;   // Maximum number of events in BU
      xdata::UnsignedInteger32 eventsPerRequest;           // Number of events requested at a time
      xdata::Double resourcesPerCore;                      // Number of resource IDs per active FU core
      xdata::UnsignedInteger32 sleepTimeBlocked;           // Time to sleep in ns for each blocked resource
      xdata::UnsignedInteger32 maxRequestRate;             // Maximum rate in Hz to send request to EVM
      xdata::Double fuOutputBandwidthLow;                  // Low water mark on bandwidth used for output of FUs
      xdata::Double fuOutputBandwidthHigh;                 // High water mark on bandwidth used for output of FUs
      xdata::UnsignedInteger32 lumiSectionLatencyLow;      // Low water mark on how many LS may be queued for the FUs
      xdata::UnsignedInteger32 lumiSectionLatencyHigh;     // High water mark on how many LS may be queued for the FUs
      xdata::UnsignedInteger32 maxFuLumiSectionLatency;    // Maximum number of lumi sections the FUs may lag behind
      xdata::UnsignedInteger32 maxTriesFUsStale;           // Maximum number of consecutive tests for FU staleness before failing
      xdata::UnsignedInteger32 staleResourceTime;          // Number of seconds after which a FU resource is no longer considered
      xdata::UnsignedInteger32 eventFragmentFIFOCapacity;  // Capacity of the FIFO used to buffer incoming event fragments
      xdata::UnsignedInteger32 superFragmentFIFOCapacity;  // Capacity of the FIFO for super-fragment
      xdata::Boolean dropEventData;                        // If true, drop the data as soon as the event is complete
      xdata::UnsignedInteger32 numberOfBuilders;           // Number of threads used to build events
      xdata::UnsignedInteger32 numberOfDiskWriters;        // Number of threads used to write events
      xdata::String rawDataDir;                            // Path to the top directory used to write the event data
      xdata::String metaDataDir;                           // Path to the top directory used to write the meta data (JSON)
      xdata::String jsdDirName;                            // Directory name under the run directory used for JSON definition files
      xdata::String hltDirName;                            // Directory name under the run directory used for HLT configuration files
      xdata::String fuLockName;                            // Filename of the lock file used to arbritrate file-access btw FUs
      xdata::String resourceSummaryFileName;               // Relative path to resource summary file
      xdata::Double rawDataHighWaterMark;                  // Relative high-water mark for the event data directory
      xdata::Double rawDataLowWaterMark;
      xdata::Double metaDataHighWaterMark;
      xdata::Double metaDataLowWaterMark;
      xdata::UnsignedInteger32 checkCRC;                   // Check the CRC of the FED fragments for every Nth event
      xdata::Boolean calculateCRC32c;                      // If set to true, a CRC32c checksum of data blob of each event is calculated
      xdata::Boolean useLock;                              // Prevent more than one BU in Configured or Enabled state
      xdata::Boolean deleteRawDataFiles;                   // If true, delete raw data files when the high-water mark is reached
      xdata::Boolean ignoreResourceSummary;                // If true, ignore the resource_summary file from hltd
      xdata::Boolean closeOldRuns;                         // If true, create empty EoR files in any old run directories without EoR files
      xdata::Boolean usePriorities;                        // If true, prioritize the event requests to the EVM
      xdata::UnsignedInteger32 minPriority;                // Minimum priority for requesting events
      xdata::UnsignedInteger32 maxEventsPerFile;           // Maximum number of events written into one file
      xdata::Double maxFileAgeSeconds;                     // Maximum time in seconds before events are written to disk
      xdata::UnsignedInteger32 fileInfoFIFOCapacity;       // Capacity of the FIFO used for files ready to be written
      xdata::UnsignedInteger32 closedFileInfoFIFOCapacity; // Capacity of the FIFO used for file accounting
      xdata::UnsignedInteger32 lumiSectionFIFOCapacity;    // Capacity of the FIFO used for lumi-section accounting
      xdata::UnsignedInteger32 lumiSectionTimeout;         // Time in seconds after which a lumi-section is considered complete
      xdata::UnsignedInteger32 nextLumiSectionTimeout;     // Time in seconds after which a lumi-section is considered complete after a new lumi-section is detected
      xdata::String hltParameterSetURL;                    // URL of the HLT menu
      xdata::Vector<xdata::String> hltFiles;               // List of file names to retrieve from hltParameterSetURL
      xdata::String blacklistName;                         // Name of the blacklist file
      xdata::String fuBlacklist;                           // The FUs to blacklist as string
      xdata::String whitelistName;                         // Name of the whitelist file
      xdata::String fuWhitelist;                           // The FUs to whitelist as string
      xdata::String hltinfoName;                           // Name of the file containing the HLT info
      xdata::Boolean isGlobalRun;                          // Set to true if the configuration is for a global run
      xdata::String daqSystem;                             // Idenfier of the DAQ system, e.g. 'cDAQ','904', etc
      xdata::String daqInstance;                           // Idenfier of the DAQ instance, e.g. 'global','ecal','hcal', etc
      xdata::String fuGroup;                               // Name of the group of FUs assigned to this BU
      xdata::UnsignedInteger32 roundTripTimeSamples;       // Rolling average of round trip times for the last N I2O mesage (0 disables it)
      xdata::UnsignedInteger32 maxPostRetries;             // Max. attempts to post an I2O message


      Configuration()
        : sendPoolName("sudapl"),
          evmInstance(-1), // Explicitly indicate parameter not set
          maxEvtsUnderConstruction(256),
          eventsPerRequest(8),
          resourcesPerCore(0.4),
          sleepTimeBlocked(200),
          maxRequestRate(1000),
          fuOutputBandwidthLow(100),
          fuOutputBandwidthHigh(120),
          lumiSectionLatencyLow(1),
          lumiSectionLatencyHigh(4),
          maxFuLumiSectionLatency(3),
          maxTriesFUsStale(60),
          staleResourceTime(10),
          eventFragmentFIFOCapacity(256),
          superFragmentFIFOCapacity(3072),
          dropEventData(false),
          numberOfBuilders(5),
          numberOfDiskWriters(5),
          rawDataDir("/tmp/fff"),
          metaDataDir("/tmp/fff"),
          jsdDirName("jsd"),
          hltDirName("hlt"),
          fuLockName("fu.lock"),
          resourceSummaryFileName("appliance/resource_summary"),
          rawDataHighWaterMark(0.95),
          rawDataLowWaterMark(0.75),
          metaDataHighWaterMark(0.95),
          metaDataLowWaterMark(0.75),
          checkCRC(1),
          calculateCRC32c(true),
          useLock(true),
          deleteRawDataFiles(false),
          ignoreResourceSummary(false),
          closeOldRuns(true),
          usePriorities(true),
          minPriority(0),
          maxEventsPerFile(100),
          maxFileAgeSeconds(2),
          fileInfoFIFOCapacity(64),
          closedFileInfoFIFOCapacity(64),
          lumiSectionFIFOCapacity(128),
          lumiSectionTimeout(30),
          nextLumiSectionTimeout(3),
          hltParameterSetURL(""),
          blacklistName("blacklist"),
          fuBlacklist("[]"),
          whitelistName("whitelist"),
          fuWhitelist("[]"),
          hltinfoName("hltinfo"),
          isGlobalRun(false),
          daqSystem(""),
          daqInstance(""),
          fuGroup(""),
          roundTripTimeSamples(1000),
          maxPostRetries(10)
      {
        hltFiles.push_back("HltConfig.py");
        hltFiles.push_back("fffParameters.jsn");
        hltFiles.push_back("daqParameters.jsn");
      };

      void addToInfoSpace
      (
        InfoSpaceItems& params,
        xdaq::ApplicationContext* context
      )
      {
        params.add("sendPoolName", &sendPoolName);
        params.add("evmInstance", &evmInstance);
        params.add("maxEvtsUnderConstruction", &maxEvtsUnderConstruction);
        params.add("eventsPerRequest", &eventsPerRequest);
        params.add("resourcesPerCore", &resourcesPerCore);
        params.add("sleepTimeBlocked", &sleepTimeBlocked);
        params.add("maxRequestRate", &maxRequestRate);
        params.add("fuOutputBandwidthLow", &fuOutputBandwidthLow);
        params.add("fuOutputBandwidthHigh", &fuOutputBandwidthHigh);
        params.add("lumiSectionLatencyLow", &lumiSectionLatencyLow);
        params.add("lumiSectionLatencyHigh", &lumiSectionLatencyHigh);
        params.add("maxFuLumiSectionLatency", &maxFuLumiSectionLatency);
        params.add("maxTriesFUsStale", &maxTriesFUsStale);
        params.add("staleResourceTime", &staleResourceTime);
        params.add("eventFragmentFIFOCapacity", &eventFragmentFIFOCapacity);
        params.add("superFragmentFIFOCapacity", &superFragmentFIFOCapacity);
        params.add("dropEventData", &dropEventData);
        params.add("numberOfBuilders", &numberOfBuilders);
        params.add("numberOfDiskWriters", &numberOfDiskWriters);
        params.add("rawDataDir", &rawDataDir);
        params.add("metaDataDir", &metaDataDir);
        params.add("jsdDirName", &jsdDirName);
        params.add("hltDirName", &hltDirName);
        params.add("fuLockName", &fuLockName);
        params.add("resourceSummaryFileName", &resourceSummaryFileName);
        params.add("rawDataHighWaterMark", &rawDataHighWaterMark);
        params.add("rawDataLowWaterMark", &rawDataLowWaterMark);
        params.add("metaDataHighWaterMark", &metaDataHighWaterMark);
        params.add("metaDataLowWaterMark", &metaDataLowWaterMark);
        params.add("checkCRC", &checkCRC);
        params.add("calculateCRC32c", &calculateCRC32c);
        params.add("useLock", &useLock);
        params.add("deleteRawDataFiles", &deleteRawDataFiles);
        params.add("ignoreResourceSummary", &ignoreResourceSummary);
        params.add("closeOldRuns", &closeOldRuns);
        params.add("usePriorities", &usePriorities);
        params.add("minPriority", &minPriority);
        params.add("maxEventsPerFile", &maxEventsPerFile);
        params.add("maxFileAgeSeconds", &maxFileAgeSeconds);
        params.add("fileInfoFIFOCapacity", &fileInfoFIFOCapacity);
        params.add("closedFileInfoFIFOCapacity", &closedFileInfoFIFOCapacity);
        params.add("lumiSectionFIFOCapacity", &lumiSectionFIFOCapacity);
        params.add("lumiSectionTimeout", &lumiSectionTimeout);
        params.add("nextLumiSectionTimeout", &nextLumiSectionTimeout);
        params.add("hltParameterSetURL", &hltParameterSetURL);
        params.add("hltFiles", &hltFiles);
        params.add("blacklistName", &blacklistName);
        params.add("fuBlacklist", &fuBlacklist);
        params.add("whitelistName", &whitelistName);
        params.add("fuWhitelist", &fuWhitelist);
        params.add("hltinfoName", &hltinfoName);
        params.add("isGlobalRun", &isGlobalRun);
        params.add("daqSystem", &daqSystem);
        params.add("daqInstance", &daqInstance);
        params.add("fuGroup", &fuGroup);
        params.add("roundTripTimeSamples", &roundTripTimeSamples);
        params.add("maxPostRetries", &maxPostRetries);
      }
    };

    using ConfigurationPtr = std::unique_ptr<Configuration>;

  } } // namespace sfb::bu

#endif // _sfb_superfragmentwriter_configuration_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
