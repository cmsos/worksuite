#ifndef _sfb_superfragmentwriter_StateMachine_h_
#define _sfb_superfragmentwriter_StateMachine_h_

#include <memory>
#include <boost/statechart/event_base.hpp>

#include "sfb/Exception.h"
#include "sfb/SfBStateMachine.h"
#include "xdaq/Application.h"
#include "xdata/UnsignedInteger32.h"


namespace sfb {

  class SFW;

  namespace superfragmentwriter {

    class FUproxy;
    class RUproxy;
    class DiskWriter;
    class SuperFragmentHandler;
    class ResourceManager;
    class StateMachine;
    class Outermost;

    ////////////////////////////////
    // Internal transition events //
    ////////////////////////////////

    class Release: public boost::statechart::event<Release> {};
    class Throttle: public boost::statechart::event<Throttle> {};
    class Block: public boost::statechart::event<Block> {};
    class Misted: public boost::statechart::event<Misted> {};
    class Clouded: public boost::statechart::event<Clouded> {};
    class Pause: public boost::statechart::event<Pause> {};


    ///////////////////////
    // The state machine //
    ///////////////////////

    using SfBStateMachine = SfBStateMachine<StateMachine,Outermost>;
    class StateMachine: public SfBStateMachine
    {

    public:

      StateMachine
      (
        SFW*,
        std::shared_ptr<RUproxy>,
        std::shared_ptr<DiskWriter>,
        std::shared_ptr<SuperFragmentHandler>,
        std::shared_ptr<ResourceManager>
      );

      SFW* sfw() const { return sfw_; }
      std::shared_ptr<RUproxy> ruProxy() const { return ruProxy_; }
      std::shared_ptr<DiskWriter> diskWriter() const { return diskWriter_; }
      std::shared_ptr<SuperFragmentHandler> superFragmentHandler() const { return superFragmentHandler_; }
      std::shared_ptr<ResourceManager> resourceManager() const { return resourceManager_; }

    private:

      SFW* sfw_;
      std::shared_ptr<RUproxy> ruProxy_;
      std::shared_ptr<DiskWriter> diskWriter_;
      std::shared_ptr<SuperFragmentHandler> superFragmentHandler_;
      std::shared_ptr<ResourceManager> resourceManager_;

    };

  } } //namespace sfb::superfragmentwriter

#endif //_sfb_superfragmentwriter_StateMachine_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
