#ifndef _sfb_superfragmentwriter_SuperFragmentHandler_h_
#define _sfb_superfragmentwriter_SuperFragmentHandler_h_

#include <boost/dynamic_bitset.hpp>

#include <atomic>
#include <cstdint>
#include <map>
#include <memory>
#include <mutex>
#include <unordered_map>
#include <vector>

#include "sfb/I2OMessages.h"
#include "sfb/InfoSpaceItems.h"
#include "sfb/OneToOneQueue.h"
#include "sfb/PerformanceMonitor.h"
#include "sfb/superfragmentwriter/Configuration.h"
#include "sfb/superfragmentwriter/Event.h"
#include "sfb/superfragmentwriter/FragmentChain.h"
#include "sfb/superfragmentwriter/RUproxy.h"
#include "sfb/superfragmentwriter/StreamHandler.h"
#include "toolbox/lang/Class.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoop.h"
#include "xdaq/Application.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/UnsignedInteger32.h"


namespace sfb {

  class SFW;

  namespace superfragmentwriter {

    class DiskWriter;
    class ResourceManager;
    class StateMachine;

    /**
     * \ingroup xdaqApps
     * \brief Keep track of events
     */

    class SuperFragmentHandler : public toolbox::lang::Class
    {
    public:

      SuperFragmentHandler
      (
        SFW*,
        std::shared_ptr<DiskWriter>,
        std::shared_ptr<ResourceManager>
      );

      ~SuperFragmentHandler();

      /**
       * Add the super fragment received for the SFW resource id
       */
      void addSuperFragment(const uint16_t buResourceId, FragmentChainPtr&&);

      /**
       * Configure
       */
      void configure();

      /**
       * Start processing events
       */
      void startProcessing(const uint32_t runNumber);

      /**
       * Drain events
       */
      void drain() const;

      /**
       * Stop processing events
       */
      void stopProcessing();

      /**
       * Append the info space items to be published in the
       * monitoring info space to the InfoSpaceItems
       */
      void appendMonitoringItems(InfoSpaceItems&);

      /**
       * Update all values of the items put into the monitoring
       * info space. The caller has to make sure that the info
       * space where the items reside is locked and properly unlocked
       * after the call.
       */
      void updateMonitoringItems();

      /**
       * Return monitoring information as cgicc snipped
       */
      cgicc::div getHtmlSnipped() const;

      /**
       * Write the next count events to a text file
       */
      void writeNextEventsToFile(const uint16_t count);

      /**
       * Return the number of corrupted events since the start of the run
       */
      uint64_t getNbCorruptedEvents() const;

      /**
       * Return the number of events with CRC errors since the start of the run
       */
      uint64_t getNbEventsWithCRCerrors() const;

      /**
       * Return the number of events with missing FED data since the start of the run
       */
      uint64_t getNbEventsMissingData() const;

      void resetMonitoringCounters();

    private:

      using PartialEvents = std::unordered_map<SfBid,EventPtr,hash_sfbid>; //indexed by SfBid

      struct EventMapMonitor
      {
        uint32_t currentLumiSection;
        uint32_t partialEvents;
        uint64_t totalEvents;

        EventMapMonitor() :
          currentLumiSection(0),partialEvents(0),totalEvents(0) {};

        void reset()
        { currentLumiSection = 0; partialEvents = 0; totalEvents = 0; }
      };

      void createProcessingWorkLoops();
      bool process(toolbox::task::WorkLoop*);
      void buildEvent(FragmentChainPtr&, PartialEvents&, EventMapMonitor&);
      void checkEvent(const EventPtr&);
      bool isEmpty() const;

      SFW* sfw_;
      std::shared_ptr<DiskWriter> diskWriter_;
      std::shared_ptr<ResourceManager> resourceManager_;

      const ConfigurationPtr& configuration_;
      uint32_t runNumber_;

      using SuperFragmentFIFO = OneToOneQueue<FragmentChainPtr>;
      using SuperFragmentFIFOPtr = std::shared_ptr<SuperFragmentFIFO>;
      using SuperFragmentFIFOs = std::map<uint16_t,SuperFragmentFIFOPtr>;
      SuperFragmentFIFOs superFragmentFIFOs_;

      using WorkLoops = std::vector<toolbox::task::WorkLoop*>;
      WorkLoops builderWorkLoops_;
      toolbox::task::ActionSignature* builderAction_;

      using EventMapMonitors = std::map<uint16_t,EventMapMonitor>;
      EventMapMonitors eventMapMonitors_;

      uint64_t corruptedEvents_;
      uint64_t eventsWithCRCerrors_;
      uint64_t eventsMissingData_;
      mutable std::mutex errorCountMutex_;

      volatile std::atomic<bool> doProcessing_;
      boost::dynamic_bitset<> processesActive_;
      mutable std::mutex processesActiveMutex_;

      mutable uint16_t writeNextEventsToFile_;
      mutable std::mutex writeNextEventsToFileMutex_;

      xdata::UnsignedInteger64 nbCorruptedEvents_;
      xdata::UnsignedInteger64 nbEventsWithCRCerrors_;
      xdata::UnsignedInteger64 nbEventsMissingData_;

    }; // SuperFragmentHandler

    using SuperFragmentHandlerPtr = std::shared_ptr<SuperFragmentHandler>;

  } } // namespace sfb::superfragmentwriter

#endif // _sfb_superfragmentwriter_SuperFragmentHandler_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
