#ifndef _sfb_superfragmentwriter_StreamHandler_h_
#define _sfb_superfragmentwriter_StreamHandler_h_

#include <atomic>
#include <cstdint>
#include <memory>
#include <string>

#include "cgicc/HTMLClasses.h"
#include "sfb/OneToOneQueue.h"
#include "sfb/superfragmentwriter/FileInfo.h"
#include "sfb/superfragmentwriter/ResourceManager.h"
#include "toolbox/lang/Class.h"
#include "toolbox/task/WorkLoop.h"


namespace sfb {

  class SFW;
  class DiskWriter;

  namespace superfragmentwriter { // namespace sfb::superfragmentwriter

    /**
     * \ingroup xdaqApps
     * \brief Handle a stream of events to be written to disk
     */

    class StreamHandler : public toolbox::lang::Class
    {
    public:

      StreamHandler
      (
        SFW*,
        DiskWriter*,
        std::shared_ptr<ResourceManager>,
        const std::string& id
      );

      ~StreamHandler();

      /**
       * Return true if nothing is ongoing
       */
      bool idle() const;

      /**
       * Stop writing events
       */
      void stopProcessing();

      /**
       * Get the next available statistics of a closed file.
       * Return false if no statistics is available
       */
      bool getNextClosedFileInfo(FileInfoPtr&);

      /**
       * Return the number of events processed so far for the given lumi section
       */
      uint32_t getEventsForLS(const uint32_t lumiSection) const;

      /**
       * Return a CGI table row with statistics for this stream writer
       */
      cgicc::tr getWriterTableRow() const;


    private:

      void startWritingWorkLoop();
      bool eventWriter(toolbox::task::WorkLoop*);
      void writeEvents();

      SFW* sfw_;
      DiskWriter* diskWriter_;
      std::shared_ptr<ResourceManager> resourceManager_;
      const std::string id_;

      toolbox::task::WorkLoop* writingWL_;
      volatile std::atomic<bool> doProcessing_;
      volatile std::atomic<bool> eventWriterActive_;

      using ClosedFileInfoFIFO = OneToOneQueue<FileInfoPtr>;
      ClosedFileInfoFIFO closedFileInfoFIFO_;

      std::atomic<uint64_t> eventCount_;
      std::atomic<uint64_t> bytesWritten_;

    };

    using StreamHandlerPtr = std::unique_ptr<StreamHandler>;

  } } // namespace sfb::superfragmentwriter

#endif // _sfb_superfragmentwriter_StreamHandler_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
