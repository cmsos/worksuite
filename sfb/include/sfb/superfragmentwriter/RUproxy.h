#ifndef _sfb_superfragmentwriter_RUproxy_h_
#define _sfb_superfragmentwriter_RUproxy_h_

#include <atomic>
#include <future>
#include <map>
#include <memory>
#include <mutex>
#include <cstdint>
#include <unordered_map>

#include "cgicc/HTMLClasses.h"
#include "sfb/ApplicationDescriptorAndTid.h"
#include "sfb/SfBid.h"
#include "sfb/I2OMessages.h"
#include "sfb/InfoSpaceItems.h"
#include "sfb/OneToOneQueue.h"
#include "sfb/superfragmentwriter/Configuration.h"
#include "sfb/superfragmentwriter/FragmentChain.h"
#include "toolbox/lang/Class.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "xdaq/Application.h"
#include "xdata/Double.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Vector.h"
#include "xgi/Output.h"


namespace sfb {

  class SFW;

  namespace superfragmentwriter { // namespace sfb::superfragmentwriter

    class SuperFragmentHandler;
    class ResourceManager;
    class StateMachine;

    /**
     * \ingroup xdaqApps
     * \brief Proxy for EVM-SFW communication
     */

    class RUproxy : public toolbox::lang::Class
    {

    public:

      RUproxy
      (
        SFW*,
        std::shared_ptr<SuperFragmentHandler>,
        std::shared_ptr<ResourceManager>
      );

      ~RUproxy();

      /**
       * Callback for I2O message containing a super fragment
       */
      void superFragmentCallback(toolbox::mem::Reference*);

      /**
       * Send request for N trigger data fragments to the RUs
       */
      void requestFragments(const uint16_t buResourceId, const uint16_t count);

      /**
       * Get the total number of events in the given lumi section from the EVM
       */
      uint32_t getTotalEventsInLumiSection(const uint32_t lumiSection);

      /**
       * Append the info space items to be published in the
       * monitoring info space to the InfoSpaceItems
       */
      void appendMonitoringItems(InfoSpaceItems&);

      /**
       * Update all values of the items put into the monitoring
       * info space. The caller has to make sure that the info
       * space where the items reside is locked and properly unlocked
       * after the call.
       */
      void updateMonitoringItems();

      /**
       * Configure
       */
      void configure();

      /**
       * Start processing events
       */
      void startProcessing();

      /**
       * Drain events
       */
      void drain();

      /**
       * Stop processing events
       */
      void stopProcessing();

      /**
       * Return monitoring information as cgicc snipped
       */
      cgicc::div getHtmlSnipped() const;

      /**
       * Callback for I2O_LUMISECTION_INFO
       */
      void lumisectionInfoCallback(toolbox::mem::Reference*);

      void resetMonitoringCounters();

    private:

      void getApplicationDescriptors();
      void startEventFragmentWorkLoop();
      void startRequestFragmentsWorkLoop();
      bool eventFragment(toolbox::task::WorkLoop*);
      bool requestFragments(toolbox::task::WorkLoop*);
      void handleEventFragment(toolbox::mem::Reference*);
      void sendRequests();
      uint64_t getTimeStamp() const;
      void getApplicationDescriptorForEVM();
      cgicc::table getStatisticsPerRU() const;

      SFW* sfw_;
      std::shared_ptr<SuperFragmentHandler> superFragmentHandler_;
      std::shared_ptr<ResourceManager> resourceManager_;

      toolbox::mem::Pool* msgPool_;
      const ConfigurationPtr& configuration_;

      using EventFragmentFIFO = OneToOneQueue<toolbox::mem::Reference*>;
      EventFragmentFIFO eventFragmentFIFO_;
      std::mutex eventFragmentFIFOmutex_;

      std::atomic<bool> doProcessing_;
      std::atomic<bool> eventFragmentActive_;
      std::atomic<bool> requestFragmentsActive_;

      toolbox::task::WorkLoop* eventFragmentWL_;
      toolbox::task::ActionSignature* eventFragmentAction_;

      toolbox::task::WorkLoop* requestFragmentsWL_;
      toolbox::task::ActionSignature* requestFragmentsAction_;

      I2O_TID tid_;
      ApplicationDescriptorAndTid evm_;
      float roundTripTimeSampling_;

      // Lookup table of data blocks, indexed by RU tid and SFW resource id
      using TIDandResourceId = std::pair<I2O_TID,uint16_t>;
      struct hash_tidAndResourceId
      {
        size_t operator()(const TIDandResourceId& tidAndResourceId) const
        { return (tidAndResourceId.first << 16) | tidAndResourceId.second; }
      };
      using DataBlockMap = std::unordered_map<TIDandResourceId,FragmentChainPtr,hash_tidAndResourceId>;
      DataBlockMap dataBlockMap_;
      std::mutex dataBlockMapMutex_;

      typedef std::unordered_multimap<uint32_t,std::promise<uint32_t>> LumiSectionInfo;
      LumiSectionInfo lumiSectionInfo_;
      std::mutex lumiSectionInfoMutex_;

      struct StatsPerRU
      {
        uint64_t logicalCount;
        uint64_t payload;
        uint32_t roundTripTime;
        uint32_t deltaTns;

        StatsPerRU() : logicalCount(0),payload(0),roundTripTime(0) {};
      };
      using CountsPerRU = std::map<uint32_t,StatsPerRU>;

      struct FragmentMonitoring
      {
        uint32_t lastEventNumber;
        uint32_t incompleteSuperFragments;
        uint64_t throughput;
        uint32_t fragmentRate;
        uint32_t i2oRate;
        double packingFactor;
        PerformanceMonitor perf;
        CountsPerRU countsPerRU;
      } fragmentMonitoring_;
      mutable std::mutex fragmentMonitoringMutex_;

      struct RequestMonitoring
      {
        uint64_t throughput;
        uint32_t requestRate;
        double requestRetryRate;
        uint32_t i2oRate;
        double retryRate;
        uint32_t retryCount;
        double packingFactor;
        PerformanceMonitor perf;
      } requestMonitoring_;
      mutable std::mutex requestMonitoringMutex_;

      xdata::UnsignedInteger32 requestRate_;
      xdata::Double requestRetryRate_;
      xdata::UnsignedInteger32 requestRetryCount_;
      xdata::UnsignedInteger32 fragmentRate_;
      xdata::Vector<xdata::UnsignedInteger64> fragmentCountPerRU_;
      xdata::Vector<xdata::UnsignedInteger64> payloadPerRU_;
      xdata::UnsignedInteger32 slowestRUtid_;
    };

  } //namespace sfb::superfragmentwriter

  namespace detail
  {
    template <>
    inline void formatter
    (
      const superfragmentwriter::FragmentChain& fragmentChain,
      std::ostringstream* out
    )
    {
      toolbox::mem::Reference* bufRef = fragmentChain.head();
      if ( bufRef )
      {
        msg::DataBlockMsg* msg =
          (msg::DataBlockMsg*)bufRef->getDataLocation();
        *out << *msg;
      }
      else
        *out << "n/a";
    }
  } // namespace detail

} //namespace sfb

#endif // _sfb_superfragmentwriter_RUproxy_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
