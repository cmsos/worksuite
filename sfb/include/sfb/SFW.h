#ifndef _sfb_superfragmentwriter_h_
#define _sfb_superfragmentwriter_h_

#include <cstdint>
#include <memory>
#include <string>

#include "cgicc/HTMLClasses.h"
#include "sfb/SfBApplication.h"
#include "sfb/PerformanceMonitor.h"
#include "sfb/superfragmentwriter/Configuration.h"
#include "sfb/superfragmentwriter/StateMachine.h"
#include "sfb/superfragmentwriter/States.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/WorkLoop.h"
#include "xdaq/ApplicationStub.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xgi/Input.h"
#include "xgi/Output.h"


namespace sfb {

  class InfoSpaceItems;

  namespace superfragmentwriter {
    class SuperFragmentHandler;
    class ResourceManager;
    class RUproxy;
  }

  /**
   * \ingroup xdaqApps
   * \brief Builder Unit (BU)
   */

  class SFW : public SfBApplication<superfragmentwriter::Configuration,superfragmentwriter::StateMachine>
  {

  public:

    SFW(xdaq::ApplicationStub*);

    virtual ~SFW() {};

    uint32_t getTotalEventsInLumiSection(const uint32_t lumiSection) const;

    XDAQ_INSTANTIATOR();


  private:

    virtual void do_bindI2oCallbacks();
    inline void I2O_BU_CACHE_Callback(toolbox::mem::Reference*);
    inline void I2O_LUMISECTION_INFO_Callback(toolbox::mem::Reference*);

    virtual void timeExpired (toolbox::task::TimerEvent&);
    virtual void do_appendApplicationInfoSpaceItems(InfoSpaceItems&);
    virtual void do_appendMonitoringInfoSpaceItems(InfoSpaceItems&);
    virtual void do_updateMonitoringInfo();

    virtual void do_handleItemRetrieveEvent(const std::string& item);

    virtual void bindNonDefaultXgiCallbacks();
    virtual cgicc::table getMainWebPage() const;
    void displayResourceTable(xgi::Input*,xgi::Output*);
    void writeNextEventsToFile(xgi::Input*,xgi::Output*);

    std::shared_ptr<superfragmentwriter::DiskWriter> diskWriter_;
    std::shared_ptr<superfragmentwriter::ResourceManager> resourceManager_;
    std::shared_ptr<superfragmentwriter::SuperFragmentHandler> superFragmentHandler_;
    std::shared_ptr<superfragmentwriter::RUproxy> ruProxy_;

    xdata::UnsignedInteger64 throughput_;
    xdata::UnsignedInteger32 eventRate_;
    xdata::UnsignedInteger32 eventSize_;
    xdata::UnsignedInteger32 nbEventsInBU_;
    xdata::UnsignedInteger64 nbEventsBuilt_;
    xdata::UnsignedInteger64 nbCorruptedEvents_;
    xdata::UnsignedInteger64 nbEventsWithCRCerrors_;
    xdata::UnsignedInteger64 nbEventsMissingData_;
    xdata::UnsignedInteger32 nbLumiSections_;
  };


} //namespace sfb

#endif // _sfb_superfragmentwriter_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
