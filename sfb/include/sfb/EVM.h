#ifndef _sfb_evm_h_
#define _sfb_evm_h_
#include <memory>
#include <vector>

#include "sfb/evm/Configuration.h"
#include "sfb/evm/RUproxy.h"
#include "sfb/readoutunit/ReadoutUnit.h"
#include "sfb/readoutunit/StateMachine.h"
#include "i2o/i2oDdmLib.h"
#include "xdaq/ApplicationStub.h"
#include "toolbox/mem/Reference.h"



namespace sfb {

  /**
   * \ingroup xdaqApps
   * \brief Event Manager (EVM)
   */
  class EVM : public readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>
  {
  public:

    EVM(xdaq::ApplicationStub*);

    XDAQ_INSTANTIATOR();

    using RUproxyPtr = std::shared_ptr<evm::RUproxy>;
    const RUproxyPtr& getRUproxy() const
    { return ruProxy_; }

    std::vector<I2O_TID>& getRUtids() const
    { return ruProxy_->getRUtids(); }

  private:
    virtual void timeExpired (toolbox::task::TimerEvent&);
    virtual void do_appendMonitoringInfoSpaceItems(InfoSpaceItems&);
    virtual void do_updateMonitoringInfo();
    virtual void do_handleItemChangedEvent(const std::string& item);

    virtual void do_EVMbindI2oCallbacks();
    virtual void I2O_REQUEST_LUMISECTION_INFO_Callback(toolbox::mem::Reference*);

    RUproxyPtr ruProxy_;

    ////////////////////////////////////////////////////////////////////////////////
    // Implementation follows                                                     //
    ////////////////////////////////////////////////////////////////////////////////



  }; // class EVM

} // namespace sfb

#endif // _sfb_evm_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
