#ifndef _sfb_I2OMessages_h_
#define _sfb_I2OMessages_h_

#include <iostream>
#include <cstdint>
#include <vector>

#include "i2o/i2o.h"
#include "i2o/i2oDdmLib.h"
#include "sfb/SfBid.h"


namespace sfb {
  namespace msg {

    using SfBids = std::vector<SfBid>;
    using RUtids = std::vector<I2O_TID>;
    using FedIds = std::vector<uint16_t>;

    /**
     * Event request for one or more events identified by the event-builder ids to be sent
     * to the BU TID specified. The EVM ignores the sfbIds and sends the next nbRequests
     * fragements available. The RU TIDs are filled by the EVM.
     */
    struct EventRequest
    {
      I2O_TID sfwTid;                             // Request SFW TID
      uint32_t msgSize;                          // Size of the message
      uint16_t padding;
      uint64_t timeStampNS;                      // time stamp in ns set by the BU when sending request
      uint16_t buResourceId;                     // Index of BU resource used to built the event
      uint16_t nbRequests;                       // Number of requested SfBids
      uint16_t nbDiscards;                       // Number of previously sent SfBids to discard
      uint16_t nbRUtids;                         // Number of RU TIDs
      SfBid sfbIds[0];                           // SfBids
      I2O_TID ruTids[0];                         // List of RU TIDs participating in the event building

      void getSfBids(SfBids&) const;
      void getRUtids(RUtids&) const;
    };


    /**
     * BU to EVM to RU message that contains one or more event requests
     */
    struct ReadoutMsg
    {
      I2O_PRIVATE_MESSAGE_FRAME PvtMessageFrame; // I2O information
      uint32_t padding;
      uint32_t nbRequests;                       // Number of requests
      EventRequest requests[];                   // List of event requests
    };


    /**
     * A super-fragment containing data from all FEDs connected to the RU
     */
    struct SuperFragment
    {
      uint16_t headerSize;                       // Size of the message header
      uint16_t superFragmentNb;                  // Index of the super fragment
      uint32_t totalSize;                        // Total size of the super fragment
      uint32_t partSize;                         // Partial size of the super-fragment contained in this message
      uint16_t nbDroppedFeds;                    // Number of FEDs dropped from the super fragment
      uint16_t fedIds[];                         // List of dropped FED ids

      void appendFedIds(FedIds&) const;

    };


    /**
     * EVM/RU to BU message that contains one or more super-fragments
     */
    struct DataBlockMsg
    {
      I2O_PRIVATE_MESSAGE_FRAME PvtMessageFrame; // I2O information
      uint16_t headerSize;                       // Size of the message header
      uint16_t buResourceId;                     // Index of BU resource used to built the event
      uint32_t nbBlocks;                         // Total number of I2O blocks
      uint32_t blockNb;                          // Index of the this block
      uint64_t timeStampNS;                      // time stamp in ns set by the BU when sending request
      uint16_t nbSuperFragments;                 // Total number of super fragments
      uint16_t nbRUtids;                         // Number of RU TIDs
      SfBid sfbIds[0];                           // The SfBids of the super fragments
      I2O_TID ruTids[0];                         // List of RU TIDs participating in the event building

      void getSfBids(SfBids&) const;
      void getRUtids(RUtids&) const;

    };


    /**
     * BU to EVM message to request information about a lumisection
     */
    struct RequestLumiSectionInfoMsg
    {
      I2O_PRIVATE_MESSAGE_FRAME PvtMessageFrame; // I2O information
      uint32_t lumiSection;                      // The lumi section number
      uint32_t padding;
    };


    /**
     * EVM to BU message to containing the information for a lumisection
     */
    struct LumiSectionInfoMsg
    {
      I2O_PRIVATE_MESSAGE_FRAME PvtMessageFrame; // I2O information
      uint32_t lumiSection;                      // The lumi section number
      uint32_t numberOfEventsBuilt;              // Total number of events built for this lumisection
    };


    std::ostream& operator<<
    (
      std::ostream& s,
      const I2O_PRIVATE_MESSAGE_FRAME
    );


    std::ostream& operator<<
    (
      std::ostream&,
      const sfb::msg::ReadoutMsg*
    );


    std::ostream& operator<<
    (
      std::ostream&,
      const sfb::msg::SuperFragment
    );


    std::ostream& operator<<
    (
      std::ostream&,
      const sfb::msg::DataBlockMsg&
    );


    std::ostream& operator<<
    (
      std::ostream&,
      const sfb::msg::RequestLumiSectionInfoMsg&
    );


    std::ostream& operator<<
    (
      std::ostream&,
      const sfb::msg::LumiSectionInfoMsg&
    );

  } } // namespace sfb::msg


#endif // _sfb_I2OMessages_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
