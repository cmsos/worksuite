#ifndef _sfb_test_dummyFEROL_StateMachine_h_
#define _sfb_test_dummyFEROL_StateMachine_h_

#include <boost/statechart/event_base.hpp>

#include "sfb/Exception.h"
#include "sfb/SfBStateMachine.h"
#include "toolbox/mem/Reference.h"
#include "xdaq/Application.h"


namespace sfb {
  namespace test {

    class DummyFEROL;

    namespace dummyFEROL {

      class StateMachine;
      class Outermost;

      ///////////////////////
      // The state machine //
      ///////////////////////

      using SfBStateMachine = SfBStateMachine<StateMachine,Outermost>;
      class StateMachine: public SfBStateMachine
      {

      public:

        StateMachine(DummyFEROL*);

        DummyFEROL* dummyFEROL() const { return dummyFEROL_; }

      private:

        DummyFEROL* dummyFEROL_;

      };

    } } } //namespace sfb::test::dummyFEROL

#endif //_sfb_test_dummyFEROL_StateMachine_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
