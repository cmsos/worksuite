#ifndef _sfb_ru_h_
#define _sfb_ru_h_

#include "sfb/readoutunit/Configuration.h"
#include "sfb/readoutunit/ReadoutUnit.h"
#include "sfb/readoutunit/StateMachine.h"
#include "xdaq/ApplicationStub.h"


namespace sfb {

  /**
   * \ingroup xdaqApps
   * \brief Readout unit (RU)
   */
  class RU : public readoutunit::ReadoutUnit<RU,readoutunit::Configuration,readoutunit::StateMachine<RU>>
  {
  public:

    RU(xdaq::ApplicationStub*);

    XDAQ_INSTANTIATOR();


  private:
    virtual void timeExpired (toolbox::task::TimerEvent&);

  }; // class RU

} // namespace sfb

#endif // _sfb_ru_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
