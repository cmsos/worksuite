#ifndef _sfb_SfBidFactory_h_
#define _sfb_SfBidFactory_h_

#include <atomic>
#include <chrono>
#include <functional>
#include <memory>
#include <cstdint>
#include <thread>

#include "sfb/DataLocations.h"


namespace sfb {

  class SfBid;

  class SfBidFactory
  {
  public:

    SfBidFactory();
    ~SfBidFactory();

    /**
     * Pass the function to extract the lumi section information from the payload
     */
    using LumiSectionFunction = std::function< uint32_t(const DataLocations&) >;
    void setLumiSectionFunction(LumiSectionFunction&);

    /**
     * Set the duration of the fake lumi section in seconds.
     * Setting it to 0 disables the generation of fake lumi sections.
     */
    void setFakeLumiSectionDuration(const uint32_t duration);

    /**
     * Create a resync at the given event number
     */
    void resyncAtEvent(const uint32_t eventNumber);

    /**
     * Return SfBid with a fake eventNumber, bunch crossing id, and lumi section
     */
    SfBid getSfBid();

    /**
     * Return SfBid for given eventNumber, bunch crossing id, and a fake lumi section
     */
    SfBid getSfBid(const uint32_t eventNumber, const uint16_t bxId);

    /**
     * Return SfBid for given eventNumber, bunch crossing id, and lumi section
     */
    SfBid getSfBid(const uint32_t eventNumber, const uint16_t bxId, const uint32_t lumiSection);

    /**
     * Return SfBid for given eventNumber, bunch crossing id, and extract lumi section from payload
     */
    SfBid getSfBid(const uint32_t eventNumber, const uint16_t bxId, const DataLocations&);

    /**
     * Reset the counters for a new run
     */
    void reset(const uint32_t runNumber);

  private:

    void stopFakeLumiThread();
    void fakeLumiActivity();

    uint32_t runNumber_;
    uint32_t previousEventNumber_;
    uint32_t resyncCount_;
    uint32_t resyncAtEventNumber_;
    LumiSectionFunction lumiSectionFunction_;
    std::chrono::seconds fakeLumiSectionDuration_;
    uint32_t fakeLumiSection_;
    std::thread fakeLumiThread_;
    volatile std::atomic<bool> doFakeLumiSections_;

  };

  using SfBidFactoryPtr = std::unique_ptr<SfBidFactory>;

} // namespace sfb

#endif // _sfb_SfBidFactory_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
