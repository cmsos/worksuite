#ifndef _sfb_DumpUtility_h_
#define _sfb_DumpUtility_h_

#include <cstdint>
#include <iostream>


namespace sfb {

  class DumpUtility
  {
  public:

    static void dumpBlockData
    (
      std::ostream&,
      const unsigned char* data,
      uint32_t len
    );

  }; // class DumpUtility

} // namespace sfb

#endif


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
