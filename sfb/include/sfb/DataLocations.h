#ifndef _sfb_DataLocations_h_
#define _sfb_DataLocations_h_

#include <sys/uio.h>
#include <vector>

namespace sfb {

  using DataLocations = std::vector<iovec>;

} // namespace sfb

#endif // _sfb_DataLocations_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
