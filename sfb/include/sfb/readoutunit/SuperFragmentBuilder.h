#ifndef _sfb_readoutunit_SuperFragmentBuilder_h_
#define _sfb_readoutunit_SuperFragmentBuilder_h_

#include <boost/dynamic_bitset.hpp>
#include <boost/lexical_cast.hpp>

#include <atomic>
#include <cstdint>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

#include "cgicc/HTMLClasses.h"
#include "sfb/Constants.h"
#include "sfb/DataLocations.h"
#include "sfb/SfBid.h"
#include "sfb/Exception.h"
#include "sfb/I2OMessages.h"
#include "sfb/InfoSpaceItems.h"
#include "sfb/OneToOneQueue.h"
#include "sfb/PerformanceMonitor.h"
#include "sfb/readoutunit/SFWposter.h"
#include "sfb/readoutunit/Configuration.h"
#include "sfb/readoutunit/FragmentRequest.h"
#include "sfb/readoutunit/StateMachine.h"
#include "sfb/readoutunit/SuperFragment.h"
#include "sfb/ApplicationDescriptorAndTid.h"
#include "i2o/utils/AddressMap.h"
#include "i2o/i2oDdmLib.h"
#include "i2o/Method.h"
#include "interface/shared/fed_header.h"
#include "interface/shared/fed_trailer.h"
#include "interface/shared/ferol_header.h"
#include "interface/shared/i2ogevb2g.h"
#include "toolbox/lang/Class.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "xcept/tools.h"
#include "xdaq/Application.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Vector.h"
#include "xgi/Output.h"


namespace sfb {

  namespace readoutunit {

    /**
     * \ingroup xdaqApps
     * \brief Proxy for RU-BU communication
     */

    template<class ReadoutUnit>
    class SuperFragmentBuilder : public toolbox::lang::Class
    {

    public:

      SuperFragmentBuilder(ReadoutUnit*);

      ~SuperFragmentBuilder();

      /**
       * Callback for fragment request I2O message from BU/EVM
       */
      void readoutMsgCallback(toolbox::mem::Reference*);

      /**
       * Send the event count for the given lumi section to the SFW TID
       */
      void sendLumiInfoToSFW(const I2O_TID buTid, const uint32_t lumiSection, const uint32_t eventCount);

      /**
       * Configure the BU proxy
       */
      void configure();

      /**
       * Append the info space items to be published in the
       * monitoring info space to the InfoSpaceItems
       */
      void appendMonitoringItems(InfoSpaceItems&);

      /**
       * Update all values of the items put into the monitoring
       * info space. The caller has to make sure that the info
       * space where the items reside is locked and properly unlocked
       * after the call.
       */
      void updateMonitoringItems();

      /**
       * Start processing events
       */
      void startProcessing();

      /**
       * Drain events
       */
      void drain();

      /**
       * Stop processing events
       */
      void stopProcessing();

      /**
       * Set the maximum super-fragment rate accepted
       */
      void setMaxTriggerRate(const uint32_t maxTriggerRate)
      { maxTriggerRate_ = maxTriggerRate; }

      /**
       * Return the latest lumi section sent to the BUs
       */
      uint32_t getLatestLumiSection() const;

      /**
       * Return the number of events built so far
       */
      uint64_t getNbEventsBuilt() const;

      /**
       * Return the number of events built so far
       */
      uint64_t getFragmentCount() const;

      /**
       * Return monitoring information as cgicc snipped
       */
      cgicc::div getHtmlSnipped() const;

      /**
       * Create and clenup the FragmentRequestFIFO for SFW connected to EVM
       */
      void cleanUPevmSFWFragmentRequestFIFO (const I2O_TID sfwEvm);
      void resetMonitoringCounters();

    private:

      using SuperFragments = std::vector<SuperFragmentPtr>;


      void startProcessingWorkLoop();
      void createProcessingWorkLoops();
      bool process(toolbox::task::WorkLoop*);
      bool processRequest(FragmentRequestPtr&, SuperFragments&);
      void handleRequest(const msg::EventRequest*, FragmentRequestPtr&);
      void sendData(const FragmentRequestPtr&, const SuperFragments&);
      toolbox::mem::Reference* getNextBlock(const uint32_t blockNb) const;
      void fillSuperFragmentHeader
      (
        unsigned char*& payload,
        uint32_t& remainingPayloadSize,
        const uint32_t superFragmentNb,
        const SuperFragmentPtr& superFragment,
        const uint32_t currentFragmentSize
      ) const;
      void waitForNextTrigger(const uint16_t nbRequests);
      bool isEmpty();
      std::string getHelpTextForBuRequests() const;

      ReadoutUnit* readoutUnit_;
      SFWposter<ReadoutUnit> sfwPoster_;
      I2O_TID tid_;
      toolbox::mem::Pool* msgPool_;

      ApplicationDescriptorAndTid sfw_;

      using WorkLoops = std::vector<toolbox::task::WorkLoop*>;
      WorkLoops workLoops_;
      toolbox::task::ActionSignature* action_;
      volatile std::atomic<bool> doProcessing_;
      boost::dynamic_bitset<> processesActive_;
      std::mutex processesActiveMutex_;
      uint32_t nbActiveProcesses_;
      mutable std::mutex processingRequestMutex_;

      //used on the RU
      using FragmentRequestFIFO = OneToOneQueue<FragmentRequestPtr>;
      FragmentRequestFIFO fragmentRequestFIFO_;

      //used on the EVM
      using FragmentRequestFIFOPtr = std::unique_ptr<FragmentRequestFIFO>;
      FragmentRequestFIFOPtr evmSFWFragmentRequestFIFO_ = nullptr;
      using FragmentRequestFIFOs = std::unordered_map<I2O_TID,FragmentRequestFIFOPtr>;
      FragmentRequestFIFOs fragmentRequestFIFOs_;
      mutable boost::shared_mutex fragmentRequestFIFOsMutex_;

      uint64_t lastLumiTransition_;
      uint64_t lastTime_;
      uint32_t maxTriggerRate_;
      uint32_t availableTriggers_;

      using BUtimestamps = std::map<I2O_TID,std::atomic<uint64_t>>;
      struct RequestMonitoring
      {
        uint64_t throughput;
        uint32_t requestRate;
        uint32_t i2oRate;
        double packingFactor;
        std::atomic<int32_t> activeRequests;
        PerformanceMonitor perf;
        BUtimestamps buTimestamps;
      } requestMonitoring_;
      mutable std::mutex requestMonitoringMutex_;

      struct DataMonitoring
      {
        uint32_t lastEventNumberToBUs;
        uint32_t lastLumiSectionToBUs;
        int32_t outstandingEvents;
        uint64_t fragmentCount;
        uint64_t nbEventsBuilt;
        uint64_t throughput;
        uint32_t fragmentRate;
        uint32_t i2oRate;
        double packingFactor;
        PerformanceMonitor perf;
      } dataMonitoring_;
      mutable std::mutex dataMonitoringMutex_;

      xdata::UnsignedInteger32 activeRequests_;
      xdata::UnsignedInteger32 requestRate_;
      xdata::UnsignedInteger32 fragmentRate_;
      xdata::UnsignedInteger64 nbEventsBuilt_;

    };

  } } //namespace sfb::readoutunit


////////////////////////////////////////////////////////////////////////////////
// Implementation follows                                                     //
////////////////////////////////////////////////////////////////////////////////

template<class ReadoutUnit>
sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::SuperFragmentBuilder(ReadoutUnit* readoutUnit) :
readoutUnit_(readoutUnit),
// TODO Remove BUposter
sfwPoster_(readoutUnit),
tid_(0),
msgPool_(readoutUnit->getMsgPool()),
doProcessing_(false),
nbActiveProcesses_(0),
fragmentRequestFIFO_(readoutUnit,"fragmentRequestFIFO"),
lastLumiTransition_(0),
lastTime_(0),
maxTriggerRate_(0),
availableTriggers_(0)
{
  resetMonitoringCounters();
  startProcessingWorkLoop();
}


template<class ReadoutUnit>
sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::~SuperFragmentBuilder()
{
  for (auto const& wl : workLoops_)
  {
    if ( wl->isActive() )
      wl->cancel();
  }
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::startProcessingWorkLoop()
{
  action_ = toolbox::task::bind(this, &sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::process,
                                readoutUnit_->getIdentifier("process") );
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::cleanUPevmSFWFragmentRequestFIFO(const I2O_TID sfwEvm)
{
	if (evmSFWFragmentRequestFIFO_ == nullptr) {
	    boost::unique_lock<boost::shared_mutex> ul(fragmentRequestFIFOsMutex_);
		std::ostringstream name;
		name << "fragmentRequestFIFO_SFW" << sfwEvm;
		evmSFWFragmentRequestFIFO_ = std::make_unique<FragmentRequestFIFO>(readoutUnit_, name.str());
		evmSFWFragmentRequestFIFO_->resize(readoutUnit_->getConfiguration()->fragmentRequestFIFOCapacity);
		LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(),"::cleanUPevmSFWFragmentRequestFIFO Created evmSFWFragmentRequestFIFO_");
	} else {
	    boost::unique_lock<boost::shared_mutex> ul(fragmentRequestFIFOsMutex_);
	    evmSFWFragmentRequestFIFO_->clear();
	    evmSFWFragmentRequestFIFO_->resize(readoutUnit_->getConfiguration()->fragmentRequestFIFOCapacity);
	    LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(),"::cleanUPevmSFWFragmentRequestFIFO Clear and Resize evmSFWFragmentRequestFIFO_");
	}
}

template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::startProcessing()
{
  resetMonitoringCounters();
  sfwPoster_.startProcessing();

  doProcessing_ = true;

  for (uint32_t i=0; i < readoutUnit_->getConfiguration()->numberOfResponders; ++i)
  {
    workLoops_.at(i)->submit(action_);
  }
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::drain()
{
  while ( ! isEmpty() ) ::usleep(1000);
  sfwPoster_.drain();
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::stopProcessing()
{
  doProcessing_ = false;
  while ( processesActive_.any() ) ::usleep(1000);
  sfwPoster_.stopProcessing();

}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::readoutMsgCallback(toolbox::mem::Reference* bufRef)
{
  I2O_MESSAGE_FRAME* stdMsg = (I2O_MESSAGE_FRAME*)bufRef->getDataLocation();
  msg::ReadoutMsg* readoutMsg = (msg::ReadoutMsg*)stdMsg;

  uint32_t nbDiscards = 0;
  uint32_t nbRequests = 0;
  uint32_t nbRequestMsg = 0;
  unsigned char* payload = (unsigned char*)&readoutMsg->requests[0];

  for (uint32_t i = 0; i < readoutMsg->nbRequests; ++i)
  {
    msg::EventRequest* eventRequest = (msg::EventRequest*)payload;

    if (eventRequest->sfwTid == sfw_.tid ) {
    	nbDiscards += eventRequest->nbDiscards;
    	nbRequests += eventRequest->nbRequests;
    	
    }
    if ( eventRequest->nbRequests > 0 )
    {
      ++nbRequestMsg;
      // The request is shared by the RUproxy and SuperFragmentBuilder
      FragmentRequestPtr fragmentRequest = std::make_shared<FragmentRequest>();
      fragmentRequest->sfwTid = eventRequest->sfwTid;
      fragmentRequest->buResourceId = eventRequest->buResourceId;
      fragmentRequest->timeStampNS = eventRequest->timeStampNS;
      fragmentRequest->nbRequests = eventRequest->nbRequests;
      handleRequest(eventRequest, fragmentRequest);
      requestMonitoring_.buTimestamps[eventRequest->sfwTid] = eventRequest->timeStampNS;
    }

    payload += eventRequest->msgSize;
  }

  {
    std::lock_guard<std::mutex> guard(dataMonitoringMutex_);
    dataMonitoring_.outstandingEvents -= nbDiscards;
  }

  {
    std::lock_guard<std::mutex> guard(requestMonitoringMutex_);

    const uint32_t msgSize = stdMsg->MessageSize << 2;
    requestMonitoring_.perf.sumOfSizes += msgSize;
    requestMonitoring_.perf.sumOfSquares += msgSize*msgSize;
    requestMonitoring_.perf.logicalCount += nbRequests;
    ++requestMonitoring_.perf.i2oCount;
    requestMonitoring_.activeRequests += nbRequestMsg;
  }

  bufRef->release();
  bufRef = 0;
}


template<class ReadoutUnit>
bool sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::process(toolbox::task::WorkLoop* wl)
{
  if ( ! doProcessing_ ) return false;

  const std::string wlName =  wl->getName();
  const size_t startPos = wlName.find_last_of("_") + 1;
  const size_t endPos = wlName.find("/",startPos);
  const uint16_t responderId = boost::lexical_cast<uint16_t>( wlName.substr(startPos,endPos-startPos) );

  {
    std::lock_guard<std::mutex> guard(processesActiveMutex_);
    processesActive_.set(responderId);
  }

  try
  {
    FragmentRequestPtr fragmentRequest;
    SuperFragments superFragments;

    while ( processRequest(fragmentRequest,superFragments) )
    {
      sendData(fragmentRequest, superFragments);
      superFragments.clear();
    }
  }
  catch(xcept::Exception& e)
  {
    {
      std::lock_guard<std::mutex> guard(processesActiveMutex_);
      processesActive_.reset(responderId);
    }
    readoutUnit_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    {
      std::lock_guard<std::mutex> guard(processesActiveMutex_);
      processesActive_.reset(responderId);
    }
    XCEPT_DECLARE(exception::SuperFragment,
                  sentinelException, e.what());
    readoutUnit_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    {
      std::lock_guard<std::mutex> guard(processesActiveMutex_);
      processesActive_.reset(responderId);
    }
    XCEPT_DECLARE(exception::SuperFragment,
                  sentinelException, "unkown exception");
    readoutUnit_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }

  {
    std::lock_guard<std::mutex> guard(processesActiveMutex_);
    processesActive_.reset(responderId);
  }

  ::usleep(10);

  return doProcessing_;
}



template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::sendData
(
  const FragmentRequestPtr& fragmentRequest,
  const SuperFragments& superFragments
)
{
  uint32_t blockNb = 1;
  uint32_t nbSF = 0;
  const uint16_t nbSuperFragments = superFragments.size();
  if (fragmentRequest->sfwTid == sfw_.tid ) {
	  nbSF =  superFragments.size();
  } else {
	  nbSF=0;
  	std::ostringstream msg;
  	msg << "sendData request from SWF_TID=" << fragmentRequest->sfwTid << " superFragments.size()="<<superFragments.size();
  	LOG4CPLUS_INFO(readoutUnit_->getApplicationLogger(),msg.str());
  }

  assert( nbSuperFragments == fragmentRequest->sfbIds.size() );
  const uint16_t nbRUtids = (nbSuperFragments>0)?fragmentRequest->ruTids.size():0;

  toolbox::mem::Reference* head = getNextBlock(blockNb);
  toolbox::mem::Reference* tail = head;

  const uint32_t blockHeaderSize = sizeof(msg::DataBlockMsg)
    + nbSuperFragments * sizeof(SfBid)
    + ((nbRUtids+1)&~1) * sizeof(I2O_TID); // always have an even number of 32-bit I2O_TIDs to keep 64-bit alignment

  assert( blockHeaderSize % 8 == 0 );
  assert( blockHeaderSize < readoutUnit_->getConfiguration()->blockSize );
  unsigned char* payload = (unsigned char*)head->getDataLocation() + blockHeaderSize;
  uint32_t remainingPayloadSize = readoutUnit_->getConfiguration()->blockSize - blockHeaderSize;

  for (uint32_t i=0; i < nbSuperFragments; ++i)
  {
    const SuperFragmentPtr& superFragment = superFragments[i];
    uint32_t remainingSuperFragmentSize = superFragment->getSize();

    fillSuperFragmentHeader(payload,remainingPayloadSize,i+1,superFragment,remainingSuperFragmentSize);

    const SuperFragment::FedFragments& fedFragments = superFragment->getFedFragments();
    for (auto& fragment : fedFragments)
    {
      uint32_t copiedSize = 0;
      while ( ! fragment->fillData(payload,remainingPayloadSize,copiedSize) )
      {
        // not all data fit into the remainingPayloadSize
        // get a new block
        remainingSuperFragmentSize -= copiedSize;
        toolbox::mem::Reference* nextBlock = getNextBlock(++blockNb);
        payload = (unsigned char*)nextBlock->getDataLocation() + sizeof(msg::DataBlockMsg);
        remainingPayloadSize = readoutUnit_->getConfiguration()->blockSize - sizeof(msg::DataBlockMsg);
        fillSuperFragmentHeader(payload,remainingPayloadSize,i+1,superFragment,remainingSuperFragmentSize);
        tail->setNextReference(nextBlock);
        tail = nextBlock;
      }
      payload += copiedSize;
      remainingPayloadSize -= copiedSize;
      remainingSuperFragmentSize -= copiedSize;

      const fedt_t* trailer = (fedt_t*)(payload - sizeof(fedt_t));
      assert ( FED_TCTRLID_EXTRACT(trailer->eventsize) == FED_SLINK_END_MARKER );
    }
  }
  tail->setDataSize( readoutUnit_->getConfiguration()->blockSize - remainingPayloadSize );

  toolbox::mem::Reference* bufRef = head;
  uint32_t payloadSize = 0;
  uint32_t i2oCount = 0;
  uint32_t lastEventNumberToBUs = 0;
  uint32_t lastLumiSectionToBUs = 0;


  // Prepare each event data block for the BU
  while (bufRef)
  {
    toolbox::mem::Reference* nextRef = bufRef->getNextReference();
    bufRef->setNextReference(0);

    I2O_MESSAGE_FRAME* stdMsg = (I2O_MESSAGE_FRAME*)bufRef->getDataLocation();
    I2O_PRIVATE_MESSAGE_FRAME* pvtMsg = (I2O_PRIVATE_MESSAGE_FRAME*)stdMsg;
    msg::DataBlockMsg* dataBlockMsg = (msg::DataBlockMsg*)stdMsg;

    stdMsg->VersionOffset          = 0;
    stdMsg->MsgFlags               = 0;
    stdMsg->MessageSize            = bufRef->getDataSize() >> 2;
    stdMsg->InitiatorAddress       = tid_;
    stdMsg->TargetAddress          = fragmentRequest->sfwTid;
    stdMsg->Function               = I2O_PRIVATE_MESSAGE;
    pvtMsg->OrganizationID         = XDAQ_ORGANIZATION_ID;
    pvtMsg->XFunctionCode          = I2O_BU_CACHE;
    dataBlockMsg->buResourceId     = fragmentRequest->buResourceId;
    dataBlockMsg->timeStampNS      = fragmentRequest->timeStampNS;
    dataBlockMsg->nbBlocks         = blockNb;
    dataBlockMsg->nbSuperFragments = nbSuperFragments;
    dataBlockMsg->nbRUtids         = nbRUtids;

    if ( dataBlockMsg->blockNb == 1 )
    {
      dataBlockMsg->headerSize = blockHeaderSize;

      unsigned char* payload = (unsigned char*)&dataBlockMsg->sfbIds;
      size_t size = nbSuperFragments*sizeof(SfBid);
      memcpy(payload,&fragmentRequest->sfbIds[0],size);
      payload += size;
      if ( nbSuperFragments > 0 )
      {
        lastEventNumberToBUs = fragmentRequest->sfbIds[nbSuperFragments-1].eventNumber();
        lastLumiSectionToBUs = fragmentRequest->sfbIds[nbSuperFragments-1].lumiSection();
      }

      size = nbRUtids*sizeof(I2O_TID);
      memcpy(payload,&fragmentRequest->ruTids[0],size);
    }
    else
    {
      dataBlockMsg->headerSize = sizeof(msg::DataBlockMsg);
    }

    payloadSize += bufRef->getDataSize();
    ++i2oCount;

    sfwPoster_.sendFrame(fragmentRequest->sfwTid,bufRef);
    bufRef = nextRef;
  }

  {
    std::lock_guard<std::mutex> guard(dataMonitoringMutex_);

    if ( lastLumiSectionToBUs > dataMonitoring_.lastLumiSectionToBUs )
    {
      dataMonitoring_.lastLumiSectionToBUs = lastLumiSectionToBUs;
    }

    dataMonitoring_.lastEventNumberToBUs = lastEventNumberToBUs;
    dataMonitoring_.outstandingEvents += nbSF;
    dataMonitoring_.perf.i2oCount += i2oCount;
    dataMonitoring_.perf.sumOfSizes += payloadSize;
    dataMonitoring_.perf.sumOfSquares += payloadSize*payloadSize;
    dataMonitoring_.perf.logicalCount += nbSuperFragments;
  }

}


template<class ReadoutUnit>
toolbox::mem::Reference* sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::getNextBlock
(
  const uint32_t blockNb
) const
{
  toolbox::mem::Reference* bufRef = 0;

  do
  {
    try
    {
      const uint32_t blockSize = readoutUnit_->getConfiguration()->blockSize;
      bufRef = toolbox::mem::getMemoryPoolFactory()->
        getFrame(msgPool_,blockSize);
      bufRef->setDataSize(blockSize);
    }
    catch(toolbox::mem::exception::Exception&)
    {
      bufRef = 0;
      ::usleep(100);
    }
  } while ( !bufRef );

  msg::DataBlockMsg* dataBlockMsg = (msg::DataBlockMsg*)bufRef->getDataLocation();
  dataBlockMsg->blockNb = blockNb;

  return bufRef;
}

template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::fillSuperFragmentHeader
(
  unsigned char*& payload,
  uint32_t& remainingPayloadSize,
  const uint32_t superFragmentNb,
  const SuperFragmentPtr& superFragment,
  const uint32_t currentFragmentSize
) const
{
  const SuperFragment::MissingFedIds& missingFedIds = superFragment->getMissingFedIds();
  const uint16_t nbDroppedFeds = missingFedIds.size();
  const uint32_t headerSize = sizeof(msg::SuperFragment)
    + ((nbDroppedFeds-1 + 3) / 4) * sizeof(uint64_t);
  // Keep 64-bit alignment if nbDroppedFeds > 1.
  // ceil(x/y) can be expressed as (x+y-1)/y for positive integers
  assert( headerSize % 8 == 0 );

  if ( remainingPayloadSize < headerSize )
  {
    remainingPayloadSize = 0;
    return;
  }

  msg::SuperFragment* superFragmentMsg = (msg::SuperFragment*)payload;

  payload += headerSize;
  remainingPayloadSize -= headerSize;

  superFragmentMsg->headerSize = headerSize;
  superFragmentMsg->superFragmentNb = superFragmentNb;
  superFragmentMsg->totalSize = superFragment->getSize();
  superFragmentMsg->partSize = currentFragmentSize > remainingPayloadSize ? remainingPayloadSize : currentFragmentSize;
  superFragmentMsg->nbDroppedFeds = nbDroppedFeds;

  memcpy(&superFragmentMsg->fedIds[0],&missingFedIds[0],nbDroppedFeds*sizeof(uint16_t));
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::sendLumiInfoToSFW(const I2O_TID buTid, const uint32_t lumiSection, const uint32_t eventCount)
{
  toolbox::mem::Reference* lumiInfoBufRef = 0;
  const uint32_t msgSize = sizeof(msg::LumiSectionInfoMsg);

  do
  {
    try
    {
      lumiInfoBufRef = toolbox::mem::getMemoryPoolFactory()->
        getFrame(msgPool_, msgSize);
      lumiInfoBufRef->setDataSize(msgSize);
    }
    catch(toolbox::mem::exception::Exception&)
    {
      lumiInfoBufRef = 0;
      ::usleep(100);
    }
  } while ( !lumiInfoBufRef );

  I2O_MESSAGE_FRAME* stdMsg =
    (I2O_MESSAGE_FRAME*)lumiInfoBufRef->getDataLocation();
  I2O_PRIVATE_MESSAGE_FRAME* pvtMsg = (I2O_PRIVATE_MESSAGE_FRAME*)stdMsg;
  msg::LumiSectionInfoMsg* lumiInfo = (msg::LumiSectionInfoMsg*)stdMsg;
  stdMsg->VersionOffset         = 0;
  stdMsg->MsgFlags              = 0;
  stdMsg->MessageSize           = msgSize >> 2;
  stdMsg->InitiatorAddress      = tid_;
  stdMsg->TargetAddress         = buTid;
  stdMsg->Function              = I2O_PRIVATE_MESSAGE;
  pvtMsg->OrganizationID        = XDAQ_ORGANIZATION_ID;
  pvtMsg->XFunctionCode         = I2O_LUMISECTION_INFO;
  lumiInfo->lumiSection         = lumiSection;
  lumiInfo->numberOfEventsBuilt = eventCount;

  sfwPoster_.sendFrame(buTid,lumiInfoBufRef);
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::configure()
{
  {
     boost::unique_lock<boost::shared_mutex> ul(fragmentRequestFIFOsMutex_);

     fragmentRequestFIFO_.clear();
     fragmentRequestFIFO_.resize(readoutUnit_->getConfiguration()->fragmentRequestFIFOCapacity);
     fragmentRequestFIFOs_.clear();
     LOG4CPLUS_DEBUG(readoutUnit_->getApplicationLogger(),"::configure Clear and Resize fragmentRequestFIFO_");
  }
  {
    std::lock_guard<std::mutex> guard(requestMonitoringMutex_);
    requestMonitoring_.activeRequests = 0;
  }


  if ( readoutUnit_->getConfiguration()->numberOfPreallocatedBlocks.value_ > 0 )
  {
    toolbox::mem::Reference* head =
      toolbox::mem::getMemoryPoolFactory()->getFrame(msgPool_,readoutUnit_->getConfiguration()->blockSize);
    toolbox::mem::Reference* tail = head;
    for (uint32_t i = 1; i < readoutUnit_->getConfiguration()->numberOfPreallocatedBlocks; ++i)
    {
      toolbox::mem::Reference* bufRef =
        toolbox::mem::getMemoryPoolFactory()->getFrame(msgPool_,readoutUnit_->getConfiguration()->blockSize);
      tail->setNextReference(bufRef);
      tail = bufRef;
    }
    head->release();
  }

  try
  {
    tid_ = i2o::utils::getAddressMap()->
      getTid(readoutUnit_->getApplicationDescriptor());
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::I2O,
                  "Failed to get I2O TID for this application", e);
  }

  try
  {
	  sfw_.descriptor =
			  readoutUnit_->getApplicationContext()->
      getDefaultZone()->
      getApplicationDescriptor("sfb::SFW",readoutUnit_->getApplicationDescriptor()->getInstance());
  }
  catch(xcept::Exception &e)
  {
    std::ostringstream msg;
    msg << "Failed to get application descriptor for SFW for this RU ";
    msg << readoutUnit_->getApplicationDescriptor()->getInstance();
    XCEPT_RETHROW(exception::Configuration, msg.str(), e);
  }

  try
  {
	  sfw_.tid = i2o::utils::getAddressMap()->getTid(sfw_.descriptor);
  }
  catch(xcept::Exception &e)
  {
    std::ostringstream msg;
    msg << "Failed to get I2O TID for SFW of this RU ";
    msg << readoutUnit_->getApplicationDescriptor()->getInstance();
    XCEPT_RETHROW(exception::I2O, msg.str(), e);
  }

  createProcessingWorkLoops();
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::createProcessingWorkLoops()
{
  processesActive_.clear();
  processesActive_.resize(readoutUnit_->getConfiguration()->numberOfResponders);

  const std::string identifier = readoutUnit_->getIdentifier();

  try
  {
    // Leave any previous created workloops alone. Only add new ones if needed.
    for (uint16_t i=workLoops_.size(); i < readoutUnit_->getConfiguration()->numberOfResponders; ++i)
    {
      std::ostringstream workLoopName;
      workLoopName << identifier << "/Responder_" << i;
      toolbox::task::WorkLoop* wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( workLoopName.str(), "waiting" );

      if ( ! wl->isActive() ) wl->activate();
      workLoops_.push_back(wl);
    }
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::WorkLoop, "Failed to start workloops", e);
  }
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::appendMonitoringItems(InfoSpaceItems& items)
{
  activeRequests_ = 0;
  requestRate_ = 0;
  fragmentRate_ = 0;
  nbEventsBuilt_ = 0;

  items.add("activeRequests", &activeRequests_);
  items.add("requestRate", &requestRate_);
  items.add("fragmentRate", &fragmentRate_);
  items.add("nbEventsBuilt", &nbEventsBuilt_);
  sfwPoster_.appendMonitoringItems(items);
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::updateMonitoringItems()
{
  {
    std::lock_guard<std::mutex> guard(requestMonitoringMutex_);

    const double deltaT = requestMonitoring_.perf.deltaT();
    requestMonitoring_.throughput = requestMonitoring_.perf.throughput(deltaT);
    requestMonitoring_.requestRate = requestMonitoring_.perf.logicalRate(deltaT);
    requestMonitoring_.i2oRate = requestMonitoring_.perf.i2oRate(deltaT);
    requestMonitoring_.packingFactor = requestMonitoring_.perf.packingFactor();
    requestRate_ = requestMonitoring_.i2oRate;
    activeRequests_ = std::max(0,requestMonitoring_.activeRequests.load());
    requestMonitoring_.perf.reset();
  }
  {
    std::lock_guard<std::mutex> guard(dataMonitoringMutex_);

    const double deltaT = dataMonitoring_.perf.deltaT();
    dataMonitoring_.fragmentCount += dataMonitoring_.perf.logicalCount;
    dataMonitoring_.nbEventsBuilt += dataMonitoring_.fragmentCount - dataMonitoring_.outstandingEvents;
    dataMonitoring_.throughput = dataMonitoring_.perf.throughput(deltaT);
    dataMonitoring_.fragmentRate = dataMonitoring_.perf.logicalRate(deltaT);
    dataMonitoring_.i2oRate = dataMonitoring_.perf.i2oRate(deltaT);
    dataMonitoring_.packingFactor = dataMonitoring_.perf.packingFactor();
    fragmentRate_ = dataMonitoring_.i2oRate;
    nbEventsBuilt_ = dataMonitoring_.nbEventsBuilt;
    dataMonitoring_.perf.reset();
  }
  {
    std::lock_guard<std::mutex> guard(processesActiveMutex_);
    nbActiveProcesses_ = processesActive_.count();
  }
  sfwPoster_.updateMonitoringItems();
}


template<class ReadoutUnit>
void sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::resetMonitoringCounters()
{
  {
    std::lock_guard<std::mutex> guard(requestMonitoringMutex_);
    requestMonitoring_.perf.reset();
    requestMonitoring_.buTimestamps.clear();
  }
  {
    std::lock_guard<std::mutex> guard(dataMonitoringMutex_);
    dataMonitoring_.lastEventNumberToBUs = 0;
    dataMonitoring_.lastLumiSectionToBUs = 0;
    dataMonitoring_.outstandingEvents = 0;
    dataMonitoring_.fragmentCount = 0;
    dataMonitoring_.nbEventsBuilt = 0;
    dataMonitoring_.perf.reset();
  }
}


template<class ReadoutUnit>
uint64_t sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::getNbEventsBuilt() const
{
  std::lock_guard<std::mutex> guard(dataMonitoringMutex_);
  return dataMonitoring_.fragmentCount + dataMonitoring_.perf.logicalCount - dataMonitoring_.outstandingEvents;
}


template<class ReadoutUnit>
uint64_t sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::getFragmentCount() const
{
  std::lock_guard<std::mutex> guard(dataMonitoringMutex_);
  return dataMonitoring_.fragmentCount + dataMonitoring_.perf.logicalCount;
}


template<class ReadoutUnit>
uint32_t sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::getLatestLumiSection() const
{
  std::lock_guard<std::mutex> guard(dataMonitoringMutex_);
  return dataMonitoring_.lastLumiSectionToBUs;
}


template<class ReadoutUnit>
cgicc::div sfb::readoutunit::SuperFragmentBuilder<ReadoutUnit>::getHtmlSnipped() const
{
  using namespace cgicc;

  cgicc::div div;
  div.add(p("SuperFragmentBuilder"));

  {
    std::lock_guard<std::mutex> guard(dataMonitoringMutex_);

    table table;
    table.set("title","Super fragments are sent to the BUs requesting events.");

    table.add(tr()
              .add(td("last evt number to BUs"))
              .add(td(std::to_string(dataMonitoring_.lastEventNumberToBUs))));
    table.add(tr()
              .add(td("last lumi section to BUs"))
              .add(td(std::to_string(dataMonitoring_.lastLumiSectionToBUs))));
    table.add(tr()
              .add(td("# of events built"))
              .add(td(std::to_string(dataMonitoring_.fragmentCount + dataMonitoring_.perf.logicalCount - dataMonitoring_.outstandingEvents))));
    table.add(tr()
              .add(td("# of active responders"))
              .add(td(std::to_string(nbActiveProcesses_))));
    table.add(tr()
              .add(td("# of active requests"))
              .add(td(std::to_string(activeRequests_.value_))));

    // outstanding events is negative for the RUs, but positive for the EVM
    table.add(tr()
              .add(td("# of outstanding events"))
              .add(td(std::to_string(abs(dataMonitoring_.outstandingEvents)))));

    table.add(tr()
              .add(th("Event data").set("colspan","2")));
    table.add(tr()
              .add(td("throughput (MB/s)"))
              .add(td(doubleToString(dataMonitoring_.throughput / 1e6,2))));
    table.add(tr()
              .add(td("fragment rate (Hz)"))
              .add(td(std::to_string(dataMonitoring_.fragmentRate))));
    table.add(tr()
              .add(td("I2O rate (Hz)"))
              .add(td(std::to_string(dataMonitoring_.i2oRate))));
    table.add(tr()
              .add(td("Fragments/I2O"))
              .add(td(doubleToString(dataMonitoring_.packingFactor,1))));
    div.add(table);
  }

  {
    std::lock_guard<std::mutex> guard(requestMonitoringMutex_);

    table table;
    table.set("title",getHelpTextForBuRequests());

    table.add(tr()
              .add(th("Event requests").set("colspan","2")));
    table.add(tr()
              .add(td("throughput (kB/s)"))
              .add(td(doubleToString(requestMonitoring_.throughput / 1e3,2))));
    table.add(tr()
              .add(td("request rate (Hz)"))
              .add(td(std::to_string(requestMonitoring_.requestRate))));
    table.add(tr()
              .add(td("I2O rate (Hz)"))
              .add(td(std::to_string(requestMonitoring_.i2oRate))));
    table.add(tr()
              .add(td("Events requested/I2O"))
              .add(td(doubleToString(requestMonitoring_.packingFactor,1))));
    div.add(table);
  }

  {
    boost::shared_lock<boost::shared_mutex> sl(fragmentRequestFIFOsMutex_);

    if (evmSFWFragmentRequestFIFO_ != nullptr) {
    	div.add(evmSFWFragmentRequestFIFO_->getHtmlSnipped());
    }

    if ( fragmentRequestFIFOs_.empty() )
    {
      div.add(fragmentRequestFIFO_.getHtmlSnipped());
    }
    else
    {
      for (auto const& requestFIFO : fragmentRequestFIFOs_)
      {
          div.add(requestFIFO.second->getHtmlSnipped());
      }
    }
  }


  div.add(sfwPoster_.getPosterFIFOs());
  div.add(sfwPoster_.getStatisticsPerBU());
  return div;
}

#endif // _sfb_readoutunit_SuperFragmentBuilder_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
