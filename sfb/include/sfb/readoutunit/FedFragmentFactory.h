#ifndef _sfb_readoutunit_FedFragmentFactory_h_
#define _sfb_readoutunit_FedFragmentFactory_h_

#include <cstdint>
#include <memory>

#include "sfb/CRCCalculator.h"
#include "sfb/SfBid.h"
#include "sfb/SfBidFactory.h"
#include "sfb/readoutunit/DummyFragment.h"
#include "sfb/readoutunit/SocketBuffer.h"
#include "sfb/readoutunit/FedFragment.h"
#include "sfb/readoutunit/StateMachine.h"
#include "toolbox/mem/Reference.h"


namespace sfb {

  namespace readoutunit { // namespace sfb::readoutunit

    /**
     * \ingroup xdaqApps
     * \brief Factory of FEDFragments
     */

    template<class ReadoutUnit>
    class FedFragmentFactory
    {

    public:

      FedFragmentFactory(ReadoutUnit*, const SfBidFactoryPtr&);

      FedFragmentPtr getFedFragment(const uint16_t fedId, const bool isMasterFed);
      FedFragmentPtr getDummyFragment(const uint16_t fedId, const bool isMasterFed, const uint32_t fedSize, const bool computeCRC);

      bool append(FedFragmentPtr&, SocketBufferPtr&, uint32_t& usedSize);

      void reset(const uint32_t runNumber);
      void writeFragmentToFile(const FedFragmentPtr&,const std::string& reasonFordump) const;

      uint32_t getCorruptedEvents() const { return fedErrors_.corruptedEvents; }
      uint32_t getEventsOutOfSequence() const { return fedErrors_.eventsOutOfSequence; }
      uint32_t getCRCerrors() const { return fedErrors_.crcErrors; }


    private:

      FedFragmentPtr makeFedFragment(const uint16_t fedId, const bool isMasterFed);
      bool errorHandler(const FedFragmentPtr&);

      ReadoutUnit* readoutUnit_;
      const SfBidFactoryPtr& sfbIdFactory_;
      uint32_t runNumber_;

      static CRCCalculator crcCalculator_;

      struct FedErrors
      {
        uint32_t corruptedEvents;
        uint32_t eventsOutOfSequence;
        uint32_t crcErrors;
        uint32_t fedErrors;
        uint32_t nbDumps;

        FedErrors() { reset(); }
        void reset()
        { corruptedEvents=0;eventsOutOfSequence=0;crcErrors=0;fedErrors=0;nbDumps=0; }
      } fedErrors_;

    };

  } } //namespace sfb::readoutunit


////////////////////////////////////////////////////////////////////////////////
// Implementation follows                                                     //
////////////////////////////////////////////////////////////////////////////////

template<class ReadoutUnit>
sfb::CRCCalculator sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::crcCalculator_;


template<class ReadoutUnit>
sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::FedFragmentFactory(ReadoutUnit* readoutUnit, const SfBidFactoryPtr& sfbIdFactory) :
  readoutUnit_(readoutUnit),
  sfbIdFactory_(sfbIdFactory),
  runNumber_(0)
{}


template<class ReadoutUnit>
void sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::reset(const uint32_t runNumber)
{
  runNumber_ = runNumber;
  fedErrors_.reset();
}


template<class ReadoutUnit>
sfb::readoutunit::FedFragmentPtr
sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::getFedFragment
(
  const uint16_t fedId,
  const bool isMasterFed
)
{
  return makeFedFragment(fedId,isMasterFed);
}


template<class ReadoutUnit>
bool sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::append
(
  FedFragmentPtr& fedFragment,
  SocketBufferPtr& socketBuffer,
  uint32_t& usedSize
)
{
  try
  {
    return fedFragment->append(socketBuffer,usedSize);
  }
  catch(...)
  {
    return errorHandler(fedFragment);
  }
  return false;
}


template<class ReadoutUnit>
sfb::readoutunit::FedFragmentPtr sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::makeFedFragment
(
  const uint16_t fedId,
  const bool isMasterFed
)
{
  return std::make_unique<FedFragment>(
    fedId,
    isMasterFed,
    readoutUnit_->getSubSystem(),
    sfbIdFactory_,
    readoutUnit_->getConfiguration()->checkCRC,
    &(fedErrors_.fedErrors),
    &(fedErrors_.crcErrors)
  );
}


template<class ReadoutUnit>
sfb::readoutunit::FedFragmentPtr
sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::getDummyFragment
(
  const uint16_t fedId,
  const bool isMasterFed,
  const uint32_t fedSize,
  const bool computeCRC
)
{
  return std::make_unique<DummyFragment>(
    fedId,
    isMasterFed,
    fedSize,
    computeCRC,
    readoutUnit_->getSubSystem(),
    sfbIdFactory_,
    readoutUnit_->getConfiguration()->checkCRC,
    &(fedErrors_.fedErrors),
    &(fedErrors_.crcErrors)
  );
}


template<class ReadoutUnit>
bool sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::errorHandler(const FedFragmentPtr& fedFragment)
{
  try
  {
    throw;
  }
  catch(exception::FEDerror& e)
  {
    LOG4CPLUS_WARN(readoutUnit_->getApplicationLogger(),
                    xcept::stdformat_exception_history(e));
    readoutUnit_->notifyQualified("warn",e);

    if ( ++fedErrors_.nbDumps <= readoutUnit_->getConfiguration()->maxDumpsPerFED )
      writeFragmentToFile(fedFragment,e.message());
  }
  catch(exception::CRCerror& e)
  {
    LOG4CPLUS_ERROR(readoutUnit_->getApplicationLogger(),
                    xcept::stdformat_exception_history(e));
    readoutUnit_->notifyQualified("error",e);

    if ( ++fedErrors_.nbDumps <= readoutUnit_->getConfiguration()->maxDumpsPerFED )
      writeFragmentToFile(fedFragment,e.message());
  }
  catch(exception::DataCorruption& e)
  {
    ++fedErrors_.corruptedEvents;

    if ( readoutUnit_->getConfiguration()->tolerateCorruptedEvents
         && ! fedFragment->isMasterFed() )
    {
      if ( ++fedErrors_.nbDumps <= readoutUnit_->getConfiguration()->maxDumpsPerFED )
        writeFragmentToFile(fedFragment,e.message());

      readoutUnit_->getStateMachine()->processFSMEvent( DataLoss(e,fedFragment->getFedId()) );
    }
    else
    {
      writeFragmentToFile(fedFragment,e.message());
      throw e;
    }
  }
  catch(exception::EventOutOfSequence& e)
  {
    ++fedErrors_.eventsOutOfSequence;

    if ( readoutUnit_->getConfiguration()->tolerateOutOfSequenceEvents
         && ! fedFragment->isMasterFed() )
    {
      if ( ++fedErrors_.nbDumps <= readoutUnit_->getConfiguration()->maxDumpsPerFED )
        writeFragmentToFile(fedFragment,e.message());

      readoutUnit_->getStateMachine()->processFSMEvent( DataLoss(e,fedFragment->getFedId()) );
    }
    else
    {
      writeFragmentToFile(fedFragment,e.message());
      readoutUnit_->getStateMachine()->processFSMEvent( EventOutOfSequence(e) );
    }
  }
  catch(exception::TCDS& e)
  {
    std::ostringstream msg;
    msg << "Failed to extract lumi section from FED " << fedFragment->getFedId();

    XCEPT_DECLARE_NESTED(exception::TCDS, sentinelException,
                         msg.str(),e);
    msg << ": " << e.message();
    writeFragmentToFile(fedFragment,msg.str());

    throw sentinelException;
  }

  return true;
}


template<class ReadoutUnit>
void sfb::readoutunit::FedFragmentFactory<ReadoutUnit>::writeFragmentToFile
(
  const FedFragmentPtr& fragment,
  const std::string& reasonForDump
) const
{
  std::ostringstream fileName;
  fileName << "/tmp/dump_run" << std::setfill('0') << std::setw(6) << runNumber_
    << "_event" << std::setw(8) << fragment->getEventNumber()
    << "_fed" << std::setw(4) << fragment->getFedId()
    << ".txt";
  std::ofstream dumpFile;
  dumpFile.open(fileName.str().c_str());
  fragment->dump(dumpFile,reasonForDump);
  dumpFile.close();
}


#endif // _sfb_readoutunit_FedFragmentFactory_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
