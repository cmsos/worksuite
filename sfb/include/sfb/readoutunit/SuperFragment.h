#ifndef _sfb_readoutunit_SuperFragment_h_
#define _sfb_readoutunit_SuperFragment_h_

#include <memory>
#include <cstdint>
#include <vector>

#include "sfb/SfBid.h"
#include "sfb/readoutunit/FedFragment.h"


namespace sfb {
  namespace readoutunit {

    /**
     * \ingroup xdaqApps
     * \brief Represent a super-fragment
     */

    class SuperFragment
    {
    public:

      SuperFragment(const SfBid&, const std::string& subSystem);

      void discardFedId(const uint16_t fedId);

      void append(FedFragmentPtr&&);

      bool hasMissingFEDs() const
      { return !missingFedIds_.empty(); }

      using MissingFedIds = std::vector<uint16_t>;
      const MissingFedIds& getMissingFedIds() const
      { return missingFedIds_; }

      using FedFragments = std::vector<FedFragmentPtr>;
      const FedFragments& getFedFragments() const
      { return fedFragments_; }

      const SfBid& getSfBid() const { return sfbId_; }
      uint32_t getSize() const { return size_; }

    private:

      const SfBid sfbId_;
      const std::string& subSystem_;
      uint32_t size_;

      MissingFedIds missingFedIds_;
      FedFragments fedFragments_;

    };

    using SuperFragmentPtr = std::unique_ptr<SuperFragment>;

  } //namespace readoutunit
} //namespace sfb

#endif // _sfb_readoutunit_SuperFragment_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
