#ifndef _sfb_readoutunit_InputMonitor_h_
#define _sfb_readoutunit_InputMonitor_h_

#include <cstdint>

#include "sfb/PerformanceMonitor.h"


namespace sfb {

  namespace readoutunit {

    struct InputMonitor
    {
      uint32_t lastEventNumber;
      uint64_t eventCount;
      PerformanceMonitor perf;
      uint32_t rate;
      double eventSize;
      double eventSizeStdDev;
      double throughput;

      InputMonitor() { reset(); }

      void reset()
      { lastEventNumber=0;eventCount=0;rate=0;eventSize=0;eventSizeStdDev=0;throughput=0; }
    };

  } } // namespace sfb::readoutunit


#endif // _sfb_readoutunit_InputMonitor_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
