#ifndef _sfb_readoutunit_FragmentRequest_h_
#define _sfb_readoutunit_FragmentRequest_h_

#include <memory>
#include <cstdint>
#include <vector>

#include "sfb/SfBid.h"
#include "i2o/i2oDdmLib.h"


namespace sfb {

  namespace readoutunit {

    struct FragmentRequest
    {
      I2O_TID  sfwTid;
      uint16_t buResourceId;
      uint16_t nbRequests;
      uint16_t nbDiscards;
      uint64_t timeStampNS;
      std::vector<SfBid> sfbIds;
      std::vector<I2O_TID> ruTids;
    };

    using FragmentRequestPtr = std::shared_ptr<FragmentRequest>;

    inline std::ostream& operator<<
    (
      std::ostream& str,
      const sfb::readoutunit::FragmentRequest& request
    )
    {
      str << "Fragment request:" << std::endl;

      str << "sfwTid=" << request.sfwTid << std::endl;
      str << "buResourceId=" << request.buResourceId << std::endl;
      str << "nbRequests=" << request.nbRequests << std::endl;
      str << "nbDiscards=" << request.nbDiscards << std::endl;
      str << "timeStampNS=" << request.timeStampNS << std::endl;
      if ( !request.sfbIds.empty() )
      {
        str << "sfbIds:" << std::endl;
        for (uint32_t i=0; i < request.nbRequests; ++i)
          str << "   [" << i << "]: " << request.sfbIds[i] << std::endl;
      }
      if ( !request.ruTids.empty() )
      {
        str << "ruTids:" << std::endl;
        for (uint32_t i=0; i < request.ruTids.size(); ++i)
          str << "   [" << i << "]: " << request.ruTids[i] << std::endl;
      }

      return str;
    }

  } } // namespace sfb::readoutunit


#endif // _sfb_readoutunit_FragmentRequest_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
