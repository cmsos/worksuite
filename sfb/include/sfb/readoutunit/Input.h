#ifndef _sfb_readoutunit_Input_h_
#define _sfb_readoutunit_Input_h_

#include <boost/thread/shared_mutex.hpp>

#include <atomic>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <math.h>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include <utility>

#include "cgicc/HTMLClasses.h"
#include "sfb/Constants.h"
#include "sfb/DataLocations.h"
#include "sfb/DumpUtility.h"
#include "sfb/SfBid.h"
#include "sfb/SfBidFactory.h"
#include "sfb/Exception.h"
#include "sfb/InfoSpaceItems.h"
#include "sfb/OneToOneQueue.h"
#include "sfb/PerformanceMonitor.h"
#include "sfb/readoutunit/FerolStream.h"
#include "sfb/readoutunit/LocalStream.h"
#include "sfb/readoutunit/MetaDataStream.h"
#include "sfb/readoutunit/MetaDataRetrieverDIPBridge.h"
#include "sfb/readoutunit/InputMonitor.h"
#include "sfb/readoutunit/StateMachine.h"
#include "interface/shared/ferol_header.h"
#include "interface/shared/i2ogevb2g.h"
#include "log4cplus/loggingmacros.h"
#include "toolbox/lang/Class.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "xcept/tools.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Vector.h"
#include "eventing/api/Bus.h"


namespace sfb {

  namespace readoutunit {

    /**
     * \ingroup xdaqApps
     * \brief Generic input for the readout units
     */
    template<class ReadoutUnit, class Configuration>
    class Input : public toolbox::lang::Class
    {
    public:

      Input(ReadoutUnit*);

      ~Input();

      /**
       * Get the next complete super fragment.
       * If none is available, the method returns false.
       * Otherwise, the SuperFragmentPtr holds a vector of FED fragements.
       */
      bool getNextAvailableSuperFragment(SuperFragmentPtr&);

      /**
       * Get the complete super fragment with SfBid.
       * If it is not available or complete, the method returns false.
       * Otherwise, the SuperFragmentPtr holds a vector of FED fragements.
       */
      void getSuperFragmentWithSfBid(const SfBid&, SuperFragmentPtr&);

      /**
       * Get the number of events contained in the given lumi section
       */
      uint32_t getEventCountForLumiSection(const uint32_t lumiSection);

      /**
       * Append the info space items to be published in the
       * monitoring info space to the InfoSpaceItems
       */
      void appendMonitoringItems(InfoSpaceItems&);

      /**
       * Update all values of the items put into the monitoring
       * info space. The caller has to make sure that the info
       * space where the items reside is locked and properly unlocked
       * after the call.
       */
      void updateMonitoringItems();

      /**
       * Configure
       */
      void configure();

      /**
       * Start processing events
       */
      void startProcessing(const uint32_t runNumber);

      /**
       * Drain the remainig events
       */
      void drain() const;

      /**
       * Block the input queues
       */
      void blockInput();

      /**
       * Stop processing events
       */
      void stopProcessing();

      /**
       * Return monitoring information as cgicc snipped
       */
      cgicc::div getHtmlSnipped() const;

      /**
       * Write the next count FED fragment to a text file
       */
      void writeNextFragmentsToFile(const uint16_t count, const uint16_t fedId=FED_COUNT+1);

      /**
       * Stop generating local events at the given event number
       */
      void stopLocalInputAtEvent(const uint32_t eventNumberToStop);

      /**
       * Adjust the trigger rate when generating local data
       */
      void setMaxTriggerRate(const uint32_t maxTriggerRate);

      /**
       * Return the event rate
       */
      uint32_t getEventRate() const;

      /**
       * Return the number of events received since the start of the run
       */
      uint64_t getEventCount() const;

      /**
       * Return the last event number received
       */
      uint32_t getLastEventNumber() const;

      /**
       * Return the average size of the super fragment
       */
      uint32_t getSuperFragmentSize() const;

      /**
       * Return a HTML status page on the DIP status
       */
      cgicc::div getHtmlSnippedForDipStatus() const;

      /**
       * Return the readout unit associated with this input
       */
      ReadoutUnit* getReadoutUnit() const
      { return readoutUnit_; }

      MetaDataRetrieverDIPBridgePtr getMetaDataRetrieverDIPBridgePtr() const
      {return metaDataRetrieverDIPBridge_;}

      /**
       * Return the metaData Retriever DIPBridge
       */
      //MetaDataRetrieverDIPBridge& getMetaDataRetrieverDIPBridge()
      //{ return metaDataRetrieverDIPBridge_; }
      void resetMonitoringCounters();

      using FerolStreamPtr = std::shared_ptr<FerolStream<ReadoutUnit,Configuration>>; //SocketStreams is shared with PipeHandler
      using FerolStreams = std::map<uint16_t,FerolStreamPtr>;

    private:

      void setMasterStream();

      void updateSuperFragmentCounters(const SuperFragmentPtr&);
      void startDummySuperFragmentWorkLoop();
      bool buildDummySuperFragments(toolbox::task::WorkLoop*);
      cgicc::table getFedTable() const;

      // these methods are only implemented for EVM
      uint32_t getLumiSectionFromTCDS(const DataLocations&) const;
      uint32_t getLumiSectionFromGTPe(const DataLocations&) const;

      ReadoutUnit* readoutUnit_;


      FerolStreams ferolStreams_;
      mutable boost::shared_mutex ferolStreamsMutex_;
      typename FerolStreams::iterator masterStream_;

      using LumiCounterMap = std::unordered_map<uint32_t,uint32_t>;
      LumiCounterMap lumiCounterMap_;
      LumiCounterMap::iterator currentLumiCounter_;
      std::mutex lumiCounterMutex_;

      uint32_t runNumber_;

      toolbox::task::WorkLoop* dummySuperFragmentWL_;
      toolbox::task::ActionSignature* dummySuperFragmentAction_;
      volatile std::atomic<bool> buildDummySuperFragmentActive_;

      MetaDataRetrieverDIPBridgePtr metaDataRetrieverDIPBridge_;
      InputMonitor superFragmentMonitor_;
      mutable std::mutex superFragmentMonitorMutex_;
      uint32_t incompleteEvents_;

      xdata::UnsignedInteger32 lastEventNumber_;
      xdata::UnsignedInteger32 eventRate_;
      xdata::UnsignedInteger32 superFragmentSize_;
      xdata::UnsignedInteger32 superFragmentSizeStdDev_;
      xdata::UnsignedInteger32 incompleteSuperFragmentCount_;
      xdata::UnsignedInteger64 eventCount_;
      xdata::Vector<xdata::UnsignedInteger32> fedIds_;
      xdata::Vector<xdata::UnsignedInteger32> fedFragmentSizes_;
      xdata::Vector<xdata::UnsignedInteger32> fedFragmentSizeStdDevs_;
      xdata::Vector<xdata::UnsignedInteger32> fedIdsWithoutFragments_;
      xdata::Vector<xdata::UnsignedInteger32> fedIdsWithErrors_;
      xdata::Vector<xdata::UnsignedInteger32> fedDataCorruption_;
      xdata::Vector<xdata::UnsignedInteger32> fedOutOfSync_;
      xdata::Vector<xdata::UnsignedInteger32> fedCRCerrors_;
      xdata::Vector<xdata::UnsignedInteger32> fedBXerrors_;

    };

  } } //namespace sfb::readoutunit


////////////////////////////////////////////////////////////////////////////////
// Implementation follows                                                     //
////////////////////////////////////////////////////////////////////////////////

template<class ReadoutUnit,class Configuration>
sfb::readoutunit::Input<ReadoutUnit,Configuration>::Input
(
  ReadoutUnit* readoutUnit
) :
readoutUnit_(readoutUnit),
runNumber_(0),
buildDummySuperFragmentActive_(false),
incompleteEvents_(0)
{}


template<class ReadoutUnit,class Configuration>
sfb::readoutunit::Input<ReadoutUnit,Configuration>::~Input()
{
  if ( dummySuperFragmentWL_ && dummySuperFragmentWL_->isActive() )
    dummySuperFragmentWL_->cancel();
}


template<class ReadoutUnit,class Configuration>
bool sfb::readoutunit::Input<ReadoutUnit,Configuration>::getNextAvailableSuperFragment(SuperFragmentPtr& superFragment)
{
  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    // This method only returns true if there is a master stream defined,
    // which is typically only true on the EVM
    if ( masterStream_ == ferolStreams_.end() ) return false;

    // Check that we can build a complete superfragment before dequeuing fragments.
    // Otherwise the EVM gets blocked if a FED has stopped sending data
    for (auto const& stream : ferolStreams_)
    {
      if ( stream.second->hasNoFragments() ) return false;
    }

    FedFragmentPtr fedFragment;
    if ( ! masterStream_->second->getNextFedFragment(fedFragment) ) return false;

    superFragment = std::make_unique<SuperFragment>(fedFragment->getSfBid(),readoutUnit_->getSubSystem());
    superFragment->append(std::move(fedFragment));

    for (auto& stream : ferolStreams_)
    {
      if ( stream.first != masterStream_->first )
      {
        stream.second->appendFedFragment(superFragment);
      }
    }
  }

  updateSuperFragmentCounters(superFragment);

  return true;
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::getSuperFragmentWithSfBid(const SfBid& sfbId, SuperFragmentPtr& superFragment)
{
  superFragment = std::make_unique<SuperFragment>(sfbId,readoutUnit_->getSubSystem());

  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);
    for (auto& stream : ferolStreams_)
    {
      stream.second->appendFedFragment(superFragment);
    }
  }

  updateSuperFragmentCounters(superFragment);
}


template<class ReadoutUnit,class Configuration>
bool sfb::readoutunit::Input<ReadoutUnit,Configuration>::buildDummySuperFragments(toolbox::task::WorkLoop* wl)
{
  if ( ferolStreams_.empty() ) return false;

  FedFragmentPtr fedFragment;

  try
  {
    while (1)
    {
      typename FerolStreams::iterator it = ferolStreams_.begin();
      if ( it->second->getNextFedFragment(fedFragment) )
      {
        buildDummySuperFragmentActive_ = true;
        uint32_t size = fedFragment->getFedSize();
        const uint32_t eventNumber = fedFragment->getEventNumber();

        while ( ++it != ferolStreams_.end() )
        {
          while ( ! it->second->getNextFedFragment(fedFragment) ) { sched_yield(); }
          size += fedFragment->getFedSize();
        }

        {
          std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);
          superFragmentMonitor_.lastEventNumber = eventNumber;
          superFragmentMonitor_.perf.sumOfSizes += size;
          superFragmentMonitor_.perf.sumOfSquares += size*size;
          ++superFragmentMonitor_.perf.logicalCount;
          ++superFragmentMonitor_.eventCount;
        }
      }
      else
      {
        buildDummySuperFragmentActive_ = false;
        ::usleep(10);
      }
    }
  }
  catch(exception::HaltRequested&)
  {
    buildDummySuperFragmentActive_ = false;
    return false;
  }

  buildDummySuperFragmentActive_ = false;
  return true;
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::updateSuperFragmentCounters(const SuperFragmentPtr& superFragment)
{
  {
    std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);

    const uint32_t size = superFragment->getSize();
    superFragmentMonitor_.lastEventNumber = superFragment->getSfBid().eventNumber();
    superFragmentMonitor_.perf.sumOfSizes += size;
    superFragmentMonitor_.perf.sumOfSquares += size*size;
    ++superFragmentMonitor_.perf.logicalCount;
    ++superFragmentMonitor_.eventCount;
    if ( superFragment->hasMissingFEDs() )
      ++incompleteEvents_;
  }

  {
    std::lock_guard<std::mutex> guard(lumiCounterMutex_);

    const uint32_t lumiSection = superFragment->getSfBid().lumiSection();
    for(uint32_t ls = currentLumiCounter_->first+1; ls <= lumiSection; ++ls)
    {
      auto const result = lumiCounterMap_.emplace(ls,0);
      if ( ! result.second )
      {
        std::ostringstream msg;
        msg << "Received an event from lumi section " << ls;
        msg << " for which an entry in lumiCounterMap already exists.";
        XCEPT_RAISE(exception::EventOrder,msg.str());
      }
      currentLumiCounter_ = result.first;
    }
  }
  ++(currentLumiCounter_->second);
}


template<class ReadoutUnit,class Configuration>
uint32_t sfb::readoutunit::Input<ReadoutUnit,Configuration>::getEventCountForLumiSection(const uint32_t lumiSection)
{
  std::lock_guard<std::mutex> guard(lumiCounterMutex_);

  const LumiCounterMap::const_iterator pos = lumiCounterMap_.find(lumiSection);

  if ( pos == lumiCounterMap_.end() )
    return 0;
  else
    return pos->second;
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::startProcessing(const uint32_t runNumber)
{
  runNumber_ = runNumber;

  {
    std::lock_guard<std::mutex> guard(lumiCounterMutex_);

    lumiCounterMap_.clear();
    currentLumiCounter_ = lumiCounterMap_.emplace(0,0).first;
  }

  resetMonitoringCounters();

  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    for (auto& stream : ferolStreams_)
    {
      stream.second->startProcessing(runNumber);
    }
  }

  if ( readoutUnit_->getConfiguration()->dropInputData )
  {
    dummySuperFragmentWL_->submit(dummySuperFragmentAction_);
  }
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::drain() const
{
  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    for (auto& stream : ferolStreams_)
    {
      stream.second->drain();
    }
  }
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::blockInput()
{
  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    for (auto& stream : ferolStreams_)
    {
      stream.second->blockInput();
    }
  }
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::stopProcessing()
{
  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    for (auto& stream : ferolStreams_)
    {
      stream.second->stopProcessing();
    }
  }
  while ( buildDummySuperFragmentActive_ ) ::usleep(1000);
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::stopLocalInputAtEvent(const uint32_t eventNumberToStop)
{
  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    for (auto& stream : ferolStreams_)
    {
      stream.second->setEventNumberToStop(eventNumberToStop);
    }
  }
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::setMaxTriggerRate(const uint32_t rate)
{
  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    for (auto& stream : ferolStreams_)
    {
      stream.second->setMaxTriggerRate(rate);
    }
  }
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::appendMonitoringItems(InfoSpaceItems& items)
{
  lastEventNumber_ = 0;
  eventRate_ = 0;
  superFragmentSize_ = 0;
  superFragmentSizeStdDev_ = 0;
  incompleteSuperFragmentCount_ = 0;
  eventCount_ = 0;
  fedIds_.clear();
  fedFragmentSizes_.clear();
  fedFragmentSizeStdDevs_.clear();
  fedIdsWithoutFragments_.clear();
  fedIdsWithErrors_.clear();
  fedDataCorruption_.clear();
  fedOutOfSync_.clear();
  fedCRCerrors_.clear();
  fedBXerrors_.clear();

  items.add("lastEventNumber", &lastEventNumber_);
  items.add("eventRate", &eventRate_);
  items.add("superFragmentSize", &superFragmentSize_);
  items.add("superFragmentSizeStdDev", &superFragmentSizeStdDev_);
  items.add("incompleteSuperFragmentCount", &incompleteSuperFragmentCount_);
  items.add("eventCount", &eventCount_);
  items.add("fedIds", &fedIds_);
  items.add("fedFragmentSizes", &fedFragmentSizes_);
  items.add("fedFragmentSizeStdDevs", &fedFragmentSizeStdDevs_);
  items.add("fedIdsWithoutFragments", &fedIdsWithoutFragments_);
  items.add("fedIdsWithErrors", &fedIdsWithErrors_);
  items.add("fedDataCorruption", &fedDataCorruption_);
  items.add("fedOutOfSync", &fedOutOfSync_);
  items.add("fedCRCerrors", &fedCRCerrors_);
  items.add("fedBXerrors", &fedBXerrors_);
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::updateMonitoringItems()
{
  {
    std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);

    const double deltaT = superFragmentMonitor_.perf.deltaT();
    superFragmentMonitor_.rate = superFragmentMonitor_.perf.logicalRate(deltaT);
    superFragmentMonitor_.throughput = superFragmentMonitor_.perf.throughput(deltaT);
    if ( superFragmentMonitor_.rate > 0 )
    {
      superFragmentMonitor_.eventSize = superFragmentMonitor_.perf.size();
      superFragmentMonitor_.eventSizeStdDev = superFragmentMonitor_.perf.sizeStdDev();
    }
    superFragmentMonitor_.perf.reset();

    lastEventNumber_ = superFragmentMonitor_.lastEventNumber;
    eventRate_ = superFragmentMonitor_.rate;
    superFragmentSize_ = superFragmentMonitor_.eventSize;
    superFragmentSizeStdDev_ = superFragmentMonitor_.eventSizeStdDev;
    eventCount_ = superFragmentMonitor_.eventCount;
  }

  {
    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    fedIds_.clear();
    fedFragmentSizes_.clear();
    fedFragmentSizeStdDevs_.clear();
    fedIdsWithoutFragments_.clear();
    fedIdsWithErrors_.clear();
    fedDataCorruption_.clear();
    fedOutOfSync_.clear();
    fedCRCerrors_.clear();
    fedBXerrors_.clear();

    uint32_t maxElements = 0;

    for (auto const& stream : ferolStreams_)
    {
      uint32_t fragmentSize = 0;
      uint32_t fragmentSizeStdDev = 0;
      uint32_t queueElements = 0;
      uint32_t corruptedEvents = 0;
      uint32_t eventsOutOfSequence = 0;
      uint32_t crcErrors = 0;
      uint32_t bxErrors = 0;

      stream.second->retrieveMonitoringQuantities(fragmentSize,fragmentSizeStdDev,queueElements,corruptedEvents,eventsOutOfSequence,crcErrors,bxErrors);

      fedIds_.push_back(stream.first);
      fedFragmentSizes_.push_back(fragmentSize);
      fedFragmentSizeStdDevs_.push_back(fragmentSizeStdDev);

      if ( queueElements > maxElements )
        maxElements = queueElements;

      if ( queueElements == 0 )
        fedIdsWithoutFragments_.push_back(stream.first);

      if ( corruptedEvents > 0 || eventsOutOfSequence > 0 || crcErrors > 0 || bxErrors > 0 )
      {
        fedIdsWithErrors_.push_back(stream.first);
        fedDataCorruption_.push_back(corruptedEvents);
        fedOutOfSync_.push_back(eventsOutOfSequence);
        fedCRCerrors_.push_back(crcErrors);
        fedBXerrors_.push_back(bxErrors);
      }
    }
    incompleteSuperFragmentCount_ = maxElements;

    if ( eventRate_ > 0U || incompleteSuperFragmentCount_ == 0U )
      fedIdsWithoutFragments_.clear();
  }
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::resetMonitoringCounters()
{
  std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);
  superFragmentMonitor_.reset();

  incompleteEvents_ = 0;
}


template<class ReadoutUnit,class Configuration>
uint32_t sfb::readoutunit::Input<ReadoutUnit,Configuration>::getEventRate() const
{
  std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);
  return superFragmentMonitor_.rate;
}


template<class ReadoutUnit,class Configuration>
uint32_t sfb::readoutunit::Input<ReadoutUnit,Configuration>::getLastEventNumber() const
{
  std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);
  return superFragmentMonitor_.lastEventNumber;
}


template<class ReadoutUnit,class Configuration>
uint64_t sfb::readoutunit::Input<ReadoutUnit,Configuration>::getEventCount() const
{
    std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);
    return superFragmentMonitor_.eventCount;
}


template<class ReadoutUnit,class Configuration>
uint32_t sfb::readoutunit::Input<ReadoutUnit,Configuration>::getSuperFragmentSize() const
{
    std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);
    return superFragmentMonitor_.eventSize;
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::configure()
{
  const auto& configuration = readoutUnit_->getConfiguration();

  // Get the DIP eventing BUS to be use for topic subscriptions
 // eventing::api::Bus& bus = readoutUnit_->getEventingBus(configuration->dipEventingBus);

  if ( configuration->blockSize % 8 != 0 )
  {
    XCEPT_RAISE(exception::Configuration, "The block size must be a multiple of 64-bits");
  }

  // This may take a while. Thus, do not call subscribeToDip while holding ferolStreamsMutex_
  if ( std::find(configuration->fedSourceIds.begin(),configuration->fedSourceIds.end(),xdata::UnsignedInteger32(SOFT_FED_ID)) != configuration->fedSourceIds.end()
       || configuration->createSoftFed1022 )
  {
    if ( ! metaDataRetrieverDIPBridge_ )
    	metaDataRetrieverDIPBridge_ = std::make_shared<MetaDataRetrieverDIPBridge>(readoutUnit_,readoutUnit_,configuration->dipEventingBus, configuration->dipBridgeRegisterTimeout,readoutUnit_->getApplicationLogger());

    metaDataRetrieverDIPBridge_->subscribeToDip( configuration->maskedDipTopics );
  }
  else
  {
    metaDataRetrieverDIPBridge_.reset();
  }

  {
    boost::unique_lock<boost::shared_mutex> ul(ferolStreamsMutex_);

    ferolStreams_.clear();

    if ( configuration->inputSource == "Socket" )
    {
      readoutUnit_->getFerolConnectionManager()->getActiveFerolStreams(ferolStreams_);
    }
    else if ( configuration->inputSource == "Local" )
    {
      for (auto const& source : configuration->ferolSources)
      {
        const uint16_t fedId = source.bag.fedId.value_;
        if (fedId > FED_COUNT)
        {
          std::ostringstream msg;
          msg << "The FED " << fedId;
          msg << " is larger than maximal value FED_COUNT=" << FED_COUNT;
          XCEPT_RAISE(exception::Configuration, msg.str());
        }

        if ( fedId != SOFT_FED_ID )
        {
          ferolStreams_.emplace(fedId,std::make_shared<LocalStream<ReadoutUnit,Configuration>>(readoutUnit_,source.bag) );
        }
      }
    }
    else
    {
      XCEPT_RAISE(exception::Configuration, "Unknown inputSource '"+configuration->inputSource.toString()+"'");
    }

    setMasterStream();

    if ( metaDataRetrieverDIPBridge_ )
    {
      ferolStreams_.emplace(SOFT_FED_ID,std::make_shared<MetaDataStream<ReadoutUnit,Configuration>>(readoutUnit_,metaDataRetrieverDIPBridge_));
    }

    if ( configuration->dropInputData )
    {
      startDummySuperFragmentWorkLoop();
    }
  }
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::setMasterStream()
{
  masterStream_ = ferolStreams_.end();
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::startDummySuperFragmentWorkLoop()
{
  try
  {
    dummySuperFragmentWL_ = toolbox::task::getWorkLoopFactory()->
      getWorkLoop( readoutUnit_->getIdentifier("dummySuperFragment"), "waiting" );

    dummySuperFragmentAction_ =
      toolbox::task::bind(this, &sfb::readoutunit::Input<ReadoutUnit,Configuration>::buildDummySuperFragments,
                          readoutUnit_->getIdentifier("buildDummySuperFragments") );

    if ( ! dummySuperFragmentWL_->isActive() )
      dummySuperFragmentWL_->activate();
  }
  catch(xcept::Exception& e)
  {
    std::string msg = "Failed to start workloop 'dummySuperFragment'";
    XCEPT_RETHROW(exception::WorkLoop, msg, e);
  }
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::Input<ReadoutUnit,Configuration>::writeNextFragmentsToFile
(
  const uint16_t count,
  const uint16_t fedId
)
{
  boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);
  typename FerolStreams::iterator pos;

  if ( fedId == FED_COUNT+1 )
    pos = ferolStreams_.begin();
  else
    pos = ferolStreams_.find(fedId);

  if ( pos != ferolStreams_.end() )
    pos->second->writeNextFragmentsToFile(count);
}


template<class ReadoutUnit,class Configuration>
cgicc::div sfb::readoutunit::Input<ReadoutUnit,Configuration>::getHtmlSnipped() const
{
  using namespace cgicc;

  cgicc::div div;
  const std::string javaScript = "function dumpFragments(fedid,count) { var options = { url:'/" +
    readoutUnit_->getURN().toString() + "/writeNextFragmentsToFile?fedid='+fedid+'&count='+count }; xdaqAJAX(options,null); }";
  div.add(script(javaScript).set("type","text/javascript"));
  div.add(p("Input - "+readoutUnit_->getConfiguration()->inputSource.toString()));


  {
    table table;
    table.set("title","Super-fragment statistics are only filled when super-fragments have been built. When there are no requests from the BUs, these counters remain 0.");

    std::lock_guard<std::mutex> guard(superFragmentMonitorMutex_);

    table.add(tr()
              .add(td("evt number of last super fragment"))
              .add(td(std::to_string(superFragmentMonitor_.lastEventNumber))));
    table.add(tr().set("title","Number of successfully built super fragments")
              .add(td("# super fragments"))
              .add(td(std::to_string(superFragmentMonitor_.eventCount))));
    table.add(tr()
              .add(td("# incomplete super fragments"))
              .add(td(std::to_string(incompleteSuperFragmentCount_.value_))));
    table.add(tr()
              .add(td("# events with missing FEDs"))
              .add(td(std::to_string(incompleteEvents_))));
    table.add(tr()
              .add(td("throughput (MB/s)"))
              .add(td(doubleToString(superFragmentMonitor_.throughput / 1e6,2))));
    table.add(tr()
              .add(td("rate (events/s)"))
              .add(td(std::to_string(superFragmentMonitor_.rate))));
    {
      std::ostringstream str;
      str.setf(std::ios::fixed);
      str.precision(1);
      str << superFragmentMonitor_.eventSize / 1e3 << " +/- " << superFragmentMonitor_.eventSizeStdDev / 1e3;
      table.add(tr()
                .add(td("super fragment size (kB)"))
                .add(td(str.str())));
    }
    div.add(table);
  }

  {
    cgicc::div ferolStreams;
    ferolStreams.set("title","Any fragments received from the FEROL are accounted here. If any fragment FIFO is empty, the EvB waits for data from the given FED.");

    boost::shared_lock<boost::shared_mutex> sl(ferolStreamsMutex_);

    ferolStreams.add(getFedTable());

    for (auto const& stream : ferolStreams_)
    {
      ferolStreams.add(stream.second->getHtmlSnippedForFragmentFIFO());
    }
    div.add(ferolStreams);
  }

  return div;
}


template<class ReadoutUnit,class Configuration>
cgicc::table sfb::readoutunit::Input<ReadoutUnit,Configuration>::getFedTable() const
{
  using namespace cgicc;

  table fedTable;

  fedTable.add(colgroup().add(col().set("span","8")));
  fedTable.add(tr()
               .add(th("Statistics per FED").set("colspan","11")));
  fedTable.add(tr()
               .add(td("FED id").set("colspan","2"))
               .add(td("Last event"))
               .add(td("Size (Bytes)"))
               .add(td("B/w (MB/s)"))
               .add(td("#CRC"))
               .add(td("#bad"))
               .add(td("#OOS"))
               .add(td("#BX"))
               .add(td("Buf. (Bytes)"))
               .add(td("Rate (Hz)")));

  for (auto const& stream : ferolStreams_)
  {
    fedTable.add( stream.second->getFedTableRow() );
  }

  return fedTable;
}


template<class ReadoutUnit,class Configuration>
cgicc::div sfb::readoutunit::Input<ReadoutUnit,Configuration>::getHtmlSnippedForDipStatus() const
{
  using namespace cgicc;

  cgicc::div div;

  if ( metaDataRetrieverDIPBridge_)
  {
    div.add( metaDataRetrieverDIPBridge_->dipStatusTable() );
  }
  else
  {
    div.add(p("No meta-data is retrieved from DIP"));
  }

  return div;
}


namespace sfb
{
  namespace detail
  {
    template <>
    inline void formatter
    (
      const readoutunit::FedFragmentPtr& fragment,
      std::ostringstream* out
    )
    {
      if ( fragment.get() )
      {
        *out << "FED fragment:" << std::endl;
        *out << "  FED id: " << fragment->getFedId() << std::endl;
        *out << "  trigger no: " << fragment->getEventNumber() << std::endl;
        *out << "  FED size: " << fragment->getFedSize() << std::endl;
      }
      else
      {
        *out << "n/a";
      }
    }
  } // namespace detail
}


#endif // _sfb_readoutunit_Input_h_

/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
