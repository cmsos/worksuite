#ifndef _sfb_readoutunit_DummyFragment_h_
#define _sfb_readoutunit_DummyFragment_h_


#include <cstdint>
#include <string>

#include "sfb/SfBidFactory.h"
#include "sfb/readoutunit/FedFragment.h"


namespace sfb {
  namespace readoutunit {

    /**
     * \ingroup xdaqApps
     * \brief Represent a dummy FED fragment
     */

    class DummyFragment : public FedFragment
    {
    public:

      DummyFragment
      (
        const uint16_t fedId,
        const bool isMasterFed,
        const uint32_t fedSize,
        const bool computeCRC,
        const std::string& subSystem,
        const SfBidFactoryPtr&,
        const uint32_t checkCRC,
        uint32_t* fedErrorCount,
        uint32_t* crcErrors
      );
      ~DummyFragment();

      virtual bool fillData(unsigned char* payload, const uint32_t remainingPayloadSize, uint32_t& copiedSize);

    private:

      void fillFedHeader(fedh_t*);
      void fillFedTrailer(fedt_t*);

      uint32_t remainingFedSize_;
      const bool computeCRC_;
      uint16_t fedCRC_;

    };

  } //namespace readoutunit
} //namespace sfb

#endif // _sfb_readoutunit_DummyFragment_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
