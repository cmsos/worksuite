#ifndef _sfb_readoutunit_LocalStream_h_
#define _sfb_readoutunit_LocalStream_h_

#include <atomic>
#include <cstdint>
#include <memory>
#include <string>

#include "sfb/Constants.h"
#include "sfb/FragmentSize.h"
#include "sfb/readoutunit/Configuration.h"
#include "sfb/readoutunit/FedFragment.h"
#include "sfb/readoutunit/FerolStream.h"
#include "sfb/readoutunit/ReadoutUnit.h"
#include "sfb/readoutunit/StateMachine.h"
#include "toolbox/lang/Class.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xcept/tools.h"


namespace sfb {

  namespace readoutunit {

   /**
    * \ingroup xdaqApps
    * \brief Represent a stream of locally generated FEROL data
    */

    template<class ReadoutUnit, class Configuration>
    class LocalStream : public FerolStream<ReadoutUnit,Configuration>, public toolbox::lang::Class
    {
    public:

      LocalStream(ReadoutUnit*, const typename Configuration::FerolSource&);

      ~LocalStream();

      /**
       * Start processing events
       */
      virtual void startProcessing(const uint32_t runNumber);

      /**
       * Drain the remainig events
       */
      virtual void drain();

      /**
       * Stop processing events
       */
      virtual void stopProcessing();


    private:

      void startGeneratorWorkLoop();
      bool generating(toolbox::task::WorkLoop*);

      toolbox::task::WorkLoop* generatingWorkLoop_;
      toolbox::task::ActionSignature* generatingAction_;
      std::unique_ptr<FragmentSize> fragmentSize_;

      volatile std::atomic<bool> generatingActive_;

    };

  } } // namespace sfb::readoutunit


////////////////////////////////////////////////////////////////////////////////
// Implementation follows                                                     //
////////////////////////////////////////////////////////////////////////////////

template<class ReadoutUnit,class Configuration>
sfb::readoutunit::LocalStream<ReadoutUnit,Configuration>::LocalStream
(
  ReadoutUnit* readoutUnit,
  const typename Configuration::FerolSource& ferolSource
) :
  FerolStream<ReadoutUnit,Configuration>(readoutUnit,ferolSource.fedId),
  generatingActive_(false)
{
  if ( readoutUnit->getConfiguration()->dummyFedSizeMin.value_ < sizeof(fedh_t) + sizeof(fedt_t) )
  {
    std::ostringstream msg;
    msg << "The minimal FED size in the configuration (dummyFedSizeMin) must be at least ";
    msg << sizeof(fedh_t) + sizeof(fedt_t) << " Bytes instead of ";
    msg << readoutUnit->getConfiguration()->dummyFedSizeMin << " Bytes";
    XCEPT_RAISE(exception::Configuration, msg.str());
  }
  fragmentSize_ = std::make_unique<FragmentSize>(ferolSource.dummyFedSize,
                                                 ferolSource.dummyFedSizeStdDev,
                                                 readoutUnit->getConfiguration()->dummyFedSizeMin,
                                                 readoutUnit->getConfiguration()->dummyFedSizeMax);
  startGeneratorWorkLoop();
}


template<class ReadoutUnit,class Configuration>
sfb::readoutunit::LocalStream<ReadoutUnit,Configuration>::~LocalStream()
{
  if ( generatingWorkLoop_ && generatingWorkLoop_->isActive() )
    generatingWorkLoop_->cancel();
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::LocalStream<ReadoutUnit,Configuration>::startGeneratorWorkLoop()
{
  const std::string fedIdStr = std::to_string(this->fedId_);
  try
  {
    generatingWorkLoop_ =
      toolbox::task::getWorkLoopFactory()->getWorkLoop(this->readoutUnit_->getIdentifier("generating_"+fedIdStr), "waiting");

    if ( !generatingWorkLoop_->isActive() )
      generatingWorkLoop_->activate();

    generatingAction_ =
      toolbox::task::bind(this,
                          &sfb::readoutunit::LocalStream<ReadoutUnit,Configuration>::generating,
                          this->readoutUnit_->getIdentifier("generatingAction_"+fedIdStr));
  }
  catch(xcept::Exception& e)
  {
    std::string msg = "Failed to start event generation workloop for FED " + fedIdStr;
    XCEPT_RETHROW(exception::WorkLoop, msg, e);
  }
}


template<class ReadoutUnit,class Configuration>
bool sfb::readoutunit::LocalStream<ReadoutUnit,Configuration>::generating(toolbox::task::WorkLoop*)
{
  generatingActive_ = true;
  FedFragmentPtr fedFragment;
  bool doGenerate = true;

  try
  {
    while (doGenerate)
    {
      const uint32_t fedSize = fragmentSize_->get();
      fedFragment = this->fedFragmentFactory_.getDummyFragment(this->fedId_,
                                                               this->isMasterStream_,
                                                               fedSize,
                                                               this->readoutUnit_->getConfiguration()->computeCRC);
      doGenerate = (fedFragment->getEventNumber() < this->eventNumberToStop_);
      this->addFedFragment(fedFragment);
    }
  }
  catch(xcept::Exception &e)
  {
    generatingActive_ = false;
    this->readoutUnit_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    generatingActive_ = false;
    XCEPT_DECLARE(exception::DummyData,
                  sentinelException, e.what());
    this->readoutUnit_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    generatingActive_ = false;
    XCEPT_DECLARE(exception::DummyData,
                  sentinelException, "unkown exception");
    this->readoutUnit_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }

  generatingActive_ = false;

  return false;
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::LocalStream<ReadoutUnit,Configuration>::startProcessing(const uint32_t runNumber)
{
  FerolStream<ReadoutUnit,Configuration>::startProcessing(runNumber);

  this->eventNumberToStop_ = (1 << 25); //larger than maximum event number

  generatingWorkLoop_->submit(generatingAction_);
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::LocalStream<ReadoutUnit,Configuration>::drain()
{
  while ( generatingActive_ ) ::usleep(1000);
}


template<class ReadoutUnit,class Configuration>
void sfb::readoutunit::LocalStream<ReadoutUnit,Configuration>::stopProcessing()
{
  this->eventNumberToStop_ = 0;

  FerolStream<ReadoutUnit,Configuration>::stopProcessing();
}


#endif // _sfb_readoutunit_LocalStream_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
