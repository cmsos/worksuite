#ifndef _sfb_Exception_h_
#define _sfb_Exception_h_

#include <exception>

#include "xcept/Exception.h"

/**
 * Exception raised in case of a configuration problem
 */
XCEPT_DEFINE_EXCEPTION(sfb, Configuration)

/**
 * Exception raised when receiving a corrupt event fragment
 */
XCEPT_DEFINE_EXCEPTION(sfb, DataCorruption)

/**
 * Exception raised when detecting a wrong CRC checksum
 */
XCEPT_DEFINE_EXCEPTION(sfb, CRCerror)

/**
 * Exception raised when detecting an issue in the FED trailer
 */
XCEPT_DEFINE_EXCEPTION(sfb, FEDerror)

/**
 * Exception raised when failing to write data do disk
 */
XCEPT_DEFINE_EXCEPTION(sfb, DiskWriting)

/**
 * Exception raised when failing to provide dummy data
 */
XCEPT_DEFINE_EXCEPTION(sfb, DummyData)

/**
 * Exception raised when encountering an out-of-order event
 */
XCEPT_DEFINE_EXCEPTION(sfb, EventOrder)

/**
 * Exception raised by queue templates
 */
XCEPT_DEFINE_EXCEPTION(sfb, FIFO)

/**
 * Exception raised when a state machine problem arises
 */
XCEPT_DEFINE_EXCEPTION(sfb, FSM)

/**
 * Exception raised when an I2O problem occured
 */
XCEPT_DEFINE_EXCEPTION(sfb, I2O)

/**
 * Exception raised when a TCP problem occured
 */
XCEPT_DEFINE_EXCEPTION(sfb, TCP)

/**
 * Exception raised if the meta data cannot be retrieved
 */
XCEPT_DEFINE_EXCEPTION(sfb, METADATA)

/**
 * Exception raised by the EVM if the TCDS information cannot be extracted
 */
XCEPT_DEFINE_EXCEPTION(sfb, TCDS)

/**
 * Exception raised by the BU if there's an error on the file-based filter farm
 */
XCEPT_DEFINE_EXCEPTION(sfb, FFF)

/**
 * Exception raised when an unexpeced event number is received
 */
XCEPT_DEFINE_EXCEPTION(sfb, EventOutOfSequence)

/**
 * Exception raised when a super-fragment mismatch occured
 */
XCEPT_DEFINE_EXCEPTION(sfb, MismatchDetected)

/**
 * Exception raised when encountering a problem providing monitoring information
 */
XCEPT_DEFINE_EXCEPTION(sfb, Monitoring)

/**
 * Exception raised when running out of memory
 */
XCEPT_DEFINE_EXCEPTION(sfb, OutOfMemory)

/**
 * Exception raised when on response to a SOAP message
 */
XCEPT_DEFINE_EXCEPTION(sfb, SOAP)

/**
 * Exception raised when a super-fragment problem occured
 */
XCEPT_DEFINE_EXCEPTION(sfb, SuperFragment)

/**
 * Exception raised by issues with xdaq::WorkLoop
 */
XCEPT_DEFINE_EXCEPTION(sfb, WorkLoop)

/**
 * Local exception signalling a Halt command
 */
namespace sfb {
  namespace exception {
    class HaltRequested : public std::exception {};
  }
}

#endif


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
