import copy
import gzip
import os
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import QName as QN

import Application
import Context
import SymbolMap
import XMLtools

class Configuration():

    def __init__(self,symbolMap,useNuma,evbType = 'EVB'):
        self.symbolMap = symbolMap
        self.useNuma = useNuma
        self.contexts = {}
        self.ptUtcp = []
        self.ptIBV = []
        self.applications = {}
        self.evbType = evbType
        self.xcns = 'http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30'


    def add(self,context):
        appInfo = dict((key,context.hostinfo[key]) for key in ('soapHostname','soapPort','launcherPort'))
        contextKey = (appInfo['soapHostname'],appInfo['soapPort'])
        self.contexts[contextKey] = context
        for app in context.applications:
            appInfo['app'] = app.params['class']
            appInfo['instance'] = app.params['instance']
            if app.params['class'].startswith(('sfb::test::DummyFEROL','ferol::','ferol40::')):
                self.addAppInfoToApplications('FEROL',appInfo)
            elif app.params['class'] == 'sfb::EVM':
                self.addAppInfoToApplications('EVM',appInfo)
                self.evmFedId = self.getEvmFedId(app)
            elif app.params['class'] == 'sfb::RU':
                self.addAppInfoToApplications('RU',appInfo)
            elif app.params['class'] == 'sfb::SFW':
                self.addAppInfoToApplications('BU',appInfo)
            elif app.params['class'] == 'gevb2g::EVM':
                self.addAppInfoToApplications('EVM',appInfo)
            elif app.params['class'] == 'gevb2g::InputEmulator':
                self.addAppInfoToApplications('IE',appInfo)
            elif app.params['class'] == 'gevb2g::RU':
                self.addAppInfoToApplications('RU',appInfo)
            elif app.params['class'] == 'gevb2g::BU':
                self.addAppInfoToApplications('BU',appInfo)
            elif app.params['class'] == 'pt::utcp::Application':
                self.ptUtcp.append( copy.deepcopy(appInfo) )
            elif app.params['class'] == 'pt::ibv::Application':
                self.ptIBV.append( copy.deepcopy(appInfo) )


    def getEvmFedId(self,app):
        for prop in app.properties:
            if prop[0] == 'fedSourceIds':
                return prop[2][0]


    def addAppInfoToApplications(self,role,appInfo):
        if role not in self.applications:
            self.applications[role] = []
        self.applications[role].append( copy.deepcopy(appInfo) )


    def isI2Otarget(self,myKey,otherKey):
        if myKey == otherKey:
            return True
        if self.contexts[myKey].role in ('EVM','EVMBU') and self.contexts[otherKey].role in ('RU','BU','RUBU'):
            return True
        if self.contexts[myKey].role == 'RU':
            if self.evbType == 'EVB' and self.contexts[otherKey].role in ('BU','RUBU'):
                return True
            elif self.evbType == 'GEVB' and self.contexts[otherKey].role in ('BU','RUBU','EVM','EVMBU'):
                return True
        if self.contexts[myKey].role == 'BU':
             if self.evbType == 'EVB' and self.contexts[otherKey].role in ('EVM','EVMBU'):
                 return True
             elif self.evbType == 'GEVB' and self.contexts[otherKey].role in ('EVM','EVMBU','RU'):
                 return True
        if self.contexts[myKey].role == 'RUBU' and self.contexts[otherKey].role in ('EVM','EVMBU','BU','RUBU'):
            return True
        return False


    def getTargets(self,key):
        i2ons = "http://xdaq.web.cern.ch/xdaq/xsd/2004/I2OConfiguration-30"
        protocol = ET.Element(QN(i2ons,'protocol'))
        for k,c in self.contexts.items():
            if self.isI2Otarget(key,k):
                for app in c.applications:
                    app.addTargetElement(protocol,i2ons)
        return protocol


    def getPartition(self,key):

        partition = ET.Element(QN(self.xcns,'Partition'))

        protocol = self.getTargets(key)
        if len(protocol) > 0:
            partition.append(protocol)

        for k,c in self.contexts.items():
            if k == key:
                partition.append( c.getContext(self.xcns,self.useNuma) )
            else:
                if self.isI2Otarget(key,k):
                    partition.append( c.getContext(self.xcns,self.useNuma,False) )
        return partition


    def getConfigCmd(self,key):
        configns = 'urn:xdaq-soap:3.0'
        configCmd = ET.Element(QN(configns,'Configure'))
        configCmd.append( self.getPartition(key) )
        XMLtools.indent(configCmd)
        configString = ET.tostring(configCmd)
        configString = self.symbolMap.parse(configString)

        print("******** "+key[0]+":"+key[1]+" ********")
        print(configString)
        print("******** "+key[0]+":"+key[1]+" ********")

        return configString

    def getRUBUs(self):
        # finds RUBUs, i.e. RUs which share the same host and soapport
        # with a BU. The return value is a list of objects in the same
        # format as in self.applications

        # maps from (soap host, soap port) to the dict describing a RU
        hostPortToRU = {}
        result = []

        try:
            for application in self.applications['RU']:
                key = (application['soapHostname'], application['soapPort'])
                hostPortToRU[key] = application
        except KeyError:
            return result

        # now find BUs which share the same context (soap host and port)
        for application in self.applications['BU']:
            key = (application['soapHostname'], application['soapPort'])
            if key in hostPortToRU:
                result.append(application)

        return result

class ConfigFromFile(Configuration):

    def __init__(self,symbolMap,configFile,fixPorts,useNuma,generateAtRU,dropAtRU,dropAtSocket,dropAtStream,dropAtBU,writeDelete,hlt,ferolMode,ferolsOnly,maxTriggerRate):
        self.frlPorts = []
        self.fedId2Port = {}
        Configuration.__init__(self,symbolMap,useNuma)

        if os.path.exists(configFile):
            ETroot = ET.parse(configFile).getroot()
        elif os.path.exists(configFile+".gz"):
            f = gzip.open(configFile+".gz") #gzip.open() doesn't support the context manager protocol needed for it to be used in a 'with' statement.
            ETroot = ET.parse(f).getroot()
            f.close()
        else:
            raise IOError(configFile+"(.gz) does not exist")

        self.xcns = re.match(r'\{(.*?)\}Partition',ETroot.tag).group(1) ## Extract xdaq namespace
        tid = 1

        for c in ETroot.getiterator(str(QN(self.xcns,'Context'))):
            try:
                url = c.attrib['url']
                role,count = self.urlToHostAndNumber(url)
                hostinfo = self.symbolMap.getHostInfo(role+str(count))
            except TypeError:
                #print("Found unknown role for "+url+", which will be ignored")
                continue

            if generateAtRU and role == "FEROLCONTROLLER":
                continue

            #if hostinfo['soapHostname'] in ('d3vrubu-c2e33-10-01.cms','d3vrubu-c2e34-20-01.cms','d3vrubu-c2e34-27-01.cms'):
            #    isAMD = True;
            #else:
            #    isAMD = False;

            context = Context.Context(role,self.evbType,hostinfo)
            if (role != "FEROLCONTROLLER"):
                context.extractPolicy(c)

            for endpoint in c.findall(QN(self.xcns,'Endpoint').text): ## all 'Endpoint's of this context
                if endpoint.attrib['protocol'] == "btcp":
                    if fixPorts:
                        self.frlPorts = ['60500','60600','60700','60800']
                    else:
                        if endpoint.attrib['port'] not in self.frlPorts:
                            self.frlPorts.append(endpoint.attrib['port'])

            #if 'rubu' in hostinfo['soapHostname']:
            #    if isAMD:
            #        context.policyElements = self.getPolicyForRUBUonAMD(hostinfo)
            #    else:
            #        context.policyElements = self.getPolicyForRUBUonSkylake(hostinfo)

            for application in c.findall(QN(self.xcns,'Application').text): ## all 'Application's of this context
                if ferolsOnly and not application.attrib['class'].startswith(('ferol::','ferol40::')):
                    continue
                if generateAtRU and application.attrib['class'] == "pt::blit::Application":
                    continue
                properties = self.getProperties(application)
                app = Application.Application(application.attrib['class'],application.attrib['instance'],properties)
                app.params['network'] = application.attrib['network']
                app.params['id'] = application.attrib['id']
                if app.params['class'] == 'pt::blit::Application':
                    app.params['frlHostname'] = context.hostinfo['frlHostname']
                    for portCount,portNb in enumerate(self.frlPorts):
                        app.params['frlPort'+str(portCount+1)] = portNb
                        app.params['maxbulksize'] = self.getBlockSize(app)

                elif app.params['class'] == 'pt::ibv::Application':
                    app.params['protocol'] = 'ibv'
                    app.params['i2oHostname'] = context.hostinfo['i2oHostname']
                    app.params['i2oPort'] =  context.hostinfo['i2oPort']
                    self.setIaName(app,context.hostinfo['i2oHostname'])
                elif app.params['class'] == 'pt::utcp::Application':
                    app.params['protocol'] = 'utcp'
                    try:
                        app.params['i2oHostname'] = context.hostinfo['i2oHostname']
                        app.params['i2oPort'] =  context.hostinfo['i2oPort']
                    except KeyError:
                        pass
                elif app.params['class'] == 'ferol::FerolController':
                    if fixPorts:
                        self.rewriteFerolProperties(app,hostinfo['frlHostname'])
                    self.setOperationMode(app,ferolMode)
                elif app.params['class'].startswith('gevb2g::'):
                    app.params['network'] = 'infini'
                    app.params['tid'] = str(tid)
                    tid += 1
                elif app.params['class'].startswith('sfb::'):
                    app.params['network'] = 'infini'
                    app.params['tid'] = str(tid)
                    tid += 1
                    if app.params['class'] == 'sfb::EVM':
                        context.role = 'EVM'
                        self.evmFedId = self.getEvmFedId(app)
                        #if count != '0':
                        #    raise Exception("The EVM must map to RU0, but maps to RU"+str(count))
                        if maxTriggerRate is not None:
                            self.setMaxTriggerRate(app,maxTriggerRate)
                    elif app.params['class'] == 'sfb::SFW':
                        if dropAtBU:
                            self.dropAtBU(app)
                        elif writeDelete:
                            self.writeDelete(app)
                        elif hlt:
                            self.noDelete(app,hostinfo['whitelist'])
                    if generateAtRU:
                        self.setLocalInput(app)
                    if dropAtRU:
                        self.dropInputData(app,'dropInputData')
                    if dropAtSocket:
                        self.dropInputData(app,'dropAtSocket')
                    if dropAtStream:
                        self.dropInputData(app,'dropAtStream')
                    elif fixPorts:
                        self.fixFerolPorts(app)

                context.applications.append(app)
            self.add(context)


    def getProperties(self,app):
        properties = []
        for child in app:
            if 'properties' in child.tag:
                for prop in child:
                    if 'rcmsStateListener' in prop.tag:
                        continue
                    param = self.getValues(prop)
                    if param[1] == 'Array':
                        val = []
                        for item in prop:
                            param[1] = self.getValues(item)[1]
                            if param[1] == 'Struct':
                                struct = []
                                for i in item:
                                    struct.append( tuple(self.getValues(i)) )
                                val.append(struct)
                            else:
                                val.append(item.text)
                        param[2] = val
                    properties.append( tuple(param) )
        return properties


    def getValues(self,element):
        xsins = "http://www.w3.org/2001/XMLSchema-instance"
        name = re.match(r'\{.*?\}(.*)',element.tag).group(1)
        type = re.match(r'.*?:(.*)',element.get(QN(xsins,'type'))).group(1)
        if element.text:
            value = element.text
        else:
            value = ''
        return [name,type,value]


    def rewriteFerolProperties(self,app,frlHostname):
        newProp = []
        for prop in app.properties:
            if prop[0] == 'enableStream0':
                enableStream0 = (prop[2] == 'true')
            elif prop[0] == 'enableStream1':
                enableStream1 = (prop[2] == 'true')
            elif prop[0] == 'expectedFedId_0':
                fedId0 = prop[2]
            elif prop[0] == 'expectedFedId_1':
                fedId1 = prop[2]
            elif prop[0] == 'slotNumber':
                slotNumber = int(prop[2])
            elif '_PORT_' not in prop[0] and 'lightStop' not in prop[0]:
                newProp.append(prop)
        newProp.append(('lightStop','boolean','true'))
        newProp.append(('DestinationIP','string',frlHostname))
        if enableStream0 and enableStream1 or slotNumber%2:
            newProp.append(('TCP_SOURCE_PORT_FED0','unsignedInt',self.frlPorts[0]))
            newProp.append(('TCP_SOURCE_PORT_FED1','unsignedInt',self.frlPorts[1]))
            newProp.append(('TCP_DESTINATION_PORT_FED0','unsignedInt',self.frlPorts[0]))
            newProp.append(('TCP_DESTINATION_PORT_FED1','unsignedInt',self.frlPorts[1]))
            self.fedId2Port[fedId0] = self.frlPorts[0]
            self.fedId2Port[fedId1] = self.frlPorts[1]
        else:
            newProp.append(('TCP_SOURCE_PORT_FED0','unsignedInt',self.frlPorts[1]))
            newProp.append(('TCP_SOURCE_PORT_FED1','unsignedInt',self.frlPorts[0]))
            newProp.append(('TCP_DESTINATION_PORT_FED0','unsignedInt',self.frlPorts[1]))
            newProp.append(('TCP_DESTINATION_PORT_FED1','unsignedInt',self.frlPorts[0]))
            self.fedId2Port[fedId0] = self.frlPorts[1]
            self.fedId2Port[fedId1] = self.frlPorts[0]
        app.properties = newProp


    def getBlockSize(self,app):
        for prop in app.properties:
            if prop[0] == 'maxBlockSize':
                return prop[2]
        return None


    def setBlockSize(self,app,maxBlockSize):
        newProp = []
        for prop in app.properties:
            if prop[0] == 'maxBlockSize':
                newProp.append(('maxBlockSize','unsignedInt',maxBlockSize))
            else:
                newProp.append(prop)
        app.properties = newProp


    def setOperationMode(self,app,ferolMode):
        newProp = []
        for prop in app.properties:
            if prop[0] == 'OperationMode':
                newProp.append(('OperationMode','string',ferolMode))
            else:
                newProp.append(prop)
        app.properties = newProp


    def dropInputData(self,app,dropMode='dropInputData'):
        if app.params['class'] in ('sfb::EVM','sfb::RU'):
            newProp = []
            for prop in app.properties:
                if not prop[0] == dropMode:
                    newProp.append(prop)
            newProp.append((dropMode,'boolean','true'))
            app.properties = newProp


    def dropAtBU(self,app):
        if app.params['class'] in ('sfb::SFW'):
            newProp = []
            for prop in app.properties:
                if not prop[0] == 'dropEventData':
                    newProp.append(prop)
            newProp.append(('dropEventData','boolean','true'))
            app.properties = newProp


    def writeDelete(self,app):
        if app.params['class'] in ('sfb::SFW'):
            newProp = []
            for prop in app.properties:
                if prop[0] not in ('dropEventData','deleteRawDataFiles','ignoreResourceSummary'):
                    newProp.append(prop)
            newProp.append(('dropEventData','boolean','false'))
            newProp.append(('deleteRawDataFiles','boolean','true'))
            newProp.append(('ignoreResourceSummary','boolean','true'))
            app.properties = newProp


    def noDelete(self,app,whitelist):
        if app.params['class'] in ('sfb::SFW'):
            newProp = []
            for prop in app.properties:
                if prop[0] not in ('deleteRawDataFiles','fuWhitelist'):
                    newProp.append(prop)
            newProp.append(('deleteRawDataFiles','boolean','false'))
            newProp.append(('fuWhitelist','string',whitelist))
            app.properties = newProp


    def setLocalInput(self,app):
        if app.params['class'] in ('sfb::EVM','sfb::RU'):
            newProp = [('computeCRC','boolean','true')]
            for prop in app.properties:
                if prop[0] == 'inputSource':
                    newProp.append(('inputSource','string','Local'))
                elif prop[0] not in ('ferolSources','computeCRC'):
                    newProp.append(prop)
            app.properties = newProp


    def setMaxTriggerRate(self,app,rate):
        if app.params['class'] in ('sfb::EVM'):
            newProp = [('maxTriggerRate','unsignedInt',rate)]
            for prop in app.properties:
                if not prop[0] == 'maxTriggerRate':
                    newProp.append(prop)
            app.properties = newProp


    def setIaName(self,app,hostname):
        interface = SymbolMap.getI2OInterfaceName(hostname)
        newProp = []
        for prop in app.properties:
            if prop[0] == 'iaName':
                newProp.append(('iaName','string',interface))
            else:
                newProp.append(prop)
        app.properties = newProp


    def fixFerolPorts(self,app):
        newProp = []
        for prop in app.properties:
            if prop[0] == 'ferolSources':
                sources = []
                for source in prop[2]:
                    items = []
                    for item in source:
                        if item[0] == 'fedId':
                            fedId = item[2]
                        if item[0] != 'port':
                            items.append(item)
                    items.append(('port','unsignedInt',self.fedId2Port[fedId]))
                    sources.append(items)
                newProp.append(('ferolSources','Struct',sources))
            else:
                newProp.append(prop)
        app.properties = newProp


    def urlToHostAndNumber(self,url):
        """
        Converts context url strings like
        'http://RU0_SOAP_HOST_NAME:RU0_SOAP_PORT'
        to a pair of strings of hosttype and index. I.e. 'RU' and '0' in this case.
        """
        pattern = re.compile(r'http://([A-Z0-9]*?)([0-9]+)_SOAP_HOST_NAME:.*')
        match = pattern.match(url)
        if match:
            return match.group(1), match.group(2) ## so h will be RU/BU/EVM/FEROLCONTROLLER/..., n will be 0,1,2,3,...
        return None


    def getPolicyForRUBUonSkylake(self,hostinfo):
        policy = [
            {'affinity':'5', 'memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::acceptor(.*)/waiting','type':'thread'},
            {'affinity':'11','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::eventworkloop/polling','type':'thread'},
            {'affinity':'19','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloopr(.*)/polling','type':'thread'},
            {'affinity':'33','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloops(.*)/polling','type':'thread'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:toolbox-mem-allocator-ibv-receiver(.*):ibvla','type':'alloc'},
            {'mempolicy':'onnode','node':'0','package':'numa','pattern':'urn:toolbox-mem-allocator-ibv-sender(.*):ibvla','type':'alloc'},
            {'affinity':'2,10,18,26,34,42,50,58','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/parseSocketBuffers_(.*)/waiting','type':'thread'},
            {'affinity':'2,10,18,26,34,42,50,58','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/generating_(.*)/waiting','type':'thread'},
            {'affinity':'4', 'memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_0/waiting','type':'thread'},
            {'affinity':'8', 'memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_1/waiting','type':'thread'},
            {'affinity':'12','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_2/waiting','type':'thread'},
            {'affinity':'16','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_3/waiting','type':'thread'},
            {'affinity':'28','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_4/waiting','type':'thread'},
            {'affinity':'24','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_5/waiting','type':'thread'},
            {'affinity':'30','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/processRequests/waiting','type':'thread'},
            {'affinity':'36','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:fifo/PeerTransport/waiting','type':'thread'},
            {'affinity':'18','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/buPoster/waiting','type':'thread'},
            {'affinity':'0', 'memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/dummySuperFragment/waiting','type':'thread'},
            {'affinity':'27','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/monitoring/waiting','type':'thread'},
            {'mempolicy':'onnode','node':'0','package':'numa','pattern':'urn:readoutMsgFIFO(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':'0','package':'numa','pattern':'urn:fragmentFIFO_FED(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:frameFIFO_BU(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:fragmentRequestFIFO(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':'0','package':'numa','pattern':'urn:socketBufferFIFO(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':'0','package':'numa','pattern':'urn:grantFIFO(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:pt::ibv::(.*)','type':'alloc'},
            {'affinity':'3', 'memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_0/waiting','type':'thread'},
            {'affinity':'39','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_1/waiting','type':'thread'},
            {'affinity':'23','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_2/waiting','type':'thread'},
            {'affinity':'37','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_3/waiting','type':'thread'},
            {'affinity':'43','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_4/waiting','type':'thread'},
            {'affinity':'59','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_5/waiting','type':'thread'},
            {'affinity':'5', 'memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_0/waiting','type':'thread'},
            {'affinity':'13','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_1/waiting','type':'thread'},
            {'affinity':'15','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_2/waiting','type':'thread'},
            {'affinity':'17','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_3/waiting','type':'thread'},
            {'affinity':'21','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_4/waiting','type':'thread'},
            {'affinity':'29','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_5/waiting','type':'thread'},
            {'affinity':'7', 'memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_6/waiting','type':'thread'},
            {'affinity':'35','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/eventFragment/waiting','type':'thread'},
            {'affinity':'6', 'memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/requestFragments/waiting','type':'thread'},
            {'affinity':'63','memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/fileAccounting/waiting','type':'thread'},
            {'affinity':'2', 'memnode':'1','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/resourceMonitor/waiting','type':'thread'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:superFragmentFIFO(.+)','type':'alloc'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:fileInfoFIFO:alloc','type':'alloc'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:closedFileInfoFIFO_(.+):alloc','type':'alloc'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:resourceFIFO:alloc','type':'alloc'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:eventFIFO:alloc','type':'alloc'},
            {'mempolicy':'onnode','node':'1','package':'numa','pattern':'urn:eventFragmentFIFO:alloc','type':'alloc'},
            ]
        if 'frlHostname' not in hostinfo:
            return policy
        else:
            return policy + [
            {'affinity':'14','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[0]+'/waiting','type':'thread'},
            {'affinity':'14','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[1]+'/waiting','type':'thread'},
            {'affinity':'14','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[2]+'/waiting','type':'thread'},
            {'affinity':'14','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[3]+'/waiting','type':'thread'},
            {'affinity':'6', 'memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[0]+'/polling','type':'thread'},
            {'affinity':'46','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[1]+'/polling','type':'thread'},
            {'affinity':'22','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[2]+'/polling','type':'thread'},
            {'affinity':'30','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[3]+'/polling','type':'thread'},
            {'affinity':'38','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.*)/Pipe_'+hostinfo['frlHostname']+':'+self.frlPorts[0]+'/waiting','type':'thread'},
            {'affinity':'2', 'memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.*)/Pipe_'+hostinfo['frlHostname']+':'+self.frlPorts[1]+'/waiting','type':'thread'},
            {'affinity':'54','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.*)/Pipe_'+hostinfo['frlHostname']+':'+self.frlPorts[2]+'/waiting','type':'thread'},
            {'affinity':'56','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.*)/Pipe_'+hostinfo['frlHostname']+':'+self.frlPorts[3]+'/waiting','type':'thread'},
            {'affinity':'16','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-acceptor-dispatcher/(.*)/waiting','type':'thread'},
            {'affinity':'33','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-connector-dispatcher/(.*)/waiting','type':'thread'},
            {'affinity':'10','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-sender-dispatcher/(.*)/waiting','type':'thread'},
            {'affinity':'17','memnode':'0','mempolicy':'onnode','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-workloop/(.*)/polling','type':'thread'},
            {'mempolicy':'onnode','node':'0','package':'numa','pattern':'urn:toolbox-mem-allocator-blit-socket(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':'0','package':'numa','pattern':'urn:pt-blit-inputpipe-rlist(.*)','type':'alloc'},
            {'mempolicy':'onnode','node':'0','package':'numa','pattern':'urn:tcpla-(.*)/'+hostinfo['frlHostname']+'(.*)','type':'alloc'},
            ]


    def getPolicyForRUBUonAMD(self,hostinfo):
        policy = [
            {'affinity':'15','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::acceptor(.*)/waiting','type':'thread'},
            {'affinity':'58','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloopr(.*)/polling','type':'thread'},
            {'affinity':'22','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::completionworkloops(.*)/polling','type':'thread'},
            {'affinity':'45','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:pt::ibv::eventworkloop/polling','type':'thread'},
            {'affinity':'3', 'memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_0/waiting','type':'thread'},
            {'affinity':'39','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_1/waiting','type':'thread'},
            {'affinity':'30','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_2/waiting','type':'thread'},
            {'affinity':'16','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_3/waiting','type':'thread'},
            {'affinity':'29','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_4/waiting','type':'thread'},
            {'affinity':'38','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_5/waiting','type':'thread'},
            {'affinity':'8', 'memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_6/waiting','type':'thread'},
            {'affinity':'42','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Responder_7/waiting','type':'thread'},
            {'affinity':'20','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:fifo/PeerTransport/waiting','type':'thread'},
            {'affinity':'9', 'memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/buPoster/waiting','type':'thread'},
            {'affinity':'55','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/monitoring/waiting','type':'thread'},
            {'affinity':'2', 'memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_0/waiting','type':'thread'},
            {'affinity':'18','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_1/waiting','type':'thread'},
            {'affinity':'14','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_2/waiting','type':'thread'},
            {'affinity':'26','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_3/waiting','type':'thread'},
            {'affinity':'56','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_4/waiting','type':'thread'},
            {'affinity':'40','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Builder_5/waiting','type':'thread'},
            {'affinity':'12','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_0/waiting','type':'thread'},
            {'affinity':'27','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_1/waiting','type':'thread'},
            {'affinity':'19','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_2/waiting','type':'thread'},
            {'affinity':'27','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_3/waiting','type':'thread'},
            {'affinity':'23','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_4/waiting','type':'thread'},
            {'affinity':'0', 'memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_5/waiting','type':'thread'},
            {'affinity':'35','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/Writer_6/waiting','type':'thread'},
            {'affinity':'8', 'memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/eventFragment/waiting','type':'thread'},
            {'affinity':'23','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/requestFragments/waiting','type':'thread'},
            {'affinity':'57','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/fileAccounting/waiting','type':'thread'},
            {'affinity':'15','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/resourceMonitor/waiting','type':'thread'},
            {'affinity':'2,4,5,9,10,11,15,20,21,24,28,32,36,37,43,46,49,53,54,55,57,58,60,63','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.+)/parseSocketBuffers(.+)/waiting','type':'thread'},
            ]
        if 'frlHostname' not in hostinfo:
            return policy
        else:
            return policy + [
                {'affinity':'58','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[0]+'/waiting','type':'thread'},
                {'affinity':'36','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[1]+'/waiting','type':'thread'},
                {'affinity':'7', 'memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[2]+'/waiting','type':'thread'},
                {'affinity':'45','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp-receiver-dispatcher/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[3]+'/waiting','type':'thread'},
                {'affinity':'50','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[0]+'/polling','type':'thread'},
                {'affinity':'9', 'memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[1]+'/polling','type':'thread'},
                {'affinity':'11','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[2]+'/polling','type':'thread'},
                {'affinity':'31','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-psp(.*)/'+hostinfo['frlHostname']+'([:/])'+self.frlPorts[3]+'/polling','type':'thread'},
                {'affinity':'59','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.*)/Pipe_'+hostinfo['frlHostname']+':'+self.frlPorts[0]+'/waiting','type':'thread'},
                {'affinity':'48','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.*)/Pipe_'+hostinfo['frlHostname']+':'+self.frlPorts[1]+'/waiting','type':'thread'},
                {'affinity':'54','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.*)/Pipe_'+hostinfo['frlHostname']+':'+self.frlPorts[2]+'/waiting','type':'thread'},
                {'affinity':'28','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:sfb::(.*)/Pipe_'+hostinfo['frlHostname']+':'+self.frlPorts[3]+'/waiting','type':'thread'},
                {'affinity':'39','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-acceptor-dispatcher/(.*)/waiting','type':'thread'},
                {'affinity':'30','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-connector-dispatcher/(.*)/waiting','type':'thread'},
                {'affinity':'32','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-sender-dispatcher/(.*)/waiting','type':'thread'},
                {'affinity':'51','memnodes':'all','mempolicy':'local','package':'numa','pattern':'urn:toolbox-task-workloop:tcpla-ia-workloop/(.*)/polling','type':'thread'},
                ]


if __name__ == "__main__":
    symbolMap = SymbolMap.SymbolMap(os.environ["EVB_TESTER_HOME"]+"/cdaq/20170131/canon_1str_4x4/symbolMap.txt")
    config = ConfigFromFile(symbolMap,os.environ["EVB_TESTER_HOME"]+"/cdaq/20170131/canon_1str_4x4/canon_1str_4x4.xml",
                                fixPorts=False,useNuma=True,generateAtRU=False,dropAtRU=False,dropAtSocket=False,dropAtStream=False,dropAtBU=True,
                                writeDelete=False,hlt=False,ferolMode=True,ferolsOnly=False,maxTriggerRate=None)
    #print(config.contexts)
    for key in config.contexts.keys():
        config.getConfigCmd(key)
    print(config.applications)
    print(config.ptUtcp)
