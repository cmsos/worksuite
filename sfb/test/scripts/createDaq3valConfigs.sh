#!/bin/sh

swt=/DAQ3_Official_folded/StandAlone/StandAlone_40g_infini_dropAtBU
#swt=/Test/Remi/StandAlone/StandAlone_40g_infini_dropAtBU
hwcfg=/daq3val/eq_200904
dir=../daq3val/20200915
mkdir -p $dir

(
cat <<'EOF'
RUEVM_CHECK_CRC 0
BU_CHECK_CRC 0
EVM_CREATE_FED1022 false
EOF
) > /tmp/$$.txt

(
cat <<EOF
EVM_CREATE_FED1022 false
EOF
) > /tmp/fed1022_$$.txt

(
cat <<EOF
d3vrubu-c2e33-10-01.cms
d3vrubu-c2e34-27-01.cms
EOF
) > /tmp/hostlist.txt

for nbFEDs in '4' '8' '12' '16' '20' '27' ; do
#for nbFEDs in '4' ; do
    fbSet=$hwcfg/evb/fb_1x${nbFEDs}
    ./createConfig.py $fbSet $swt 2 $dir/${nbFEDs}x1x2 -s /tmp/fed1022_$$.txt --daqval -p $HOME/configurator/CONFIGURATOR_DAQ2VAL.properties --hostList /tmp/hostlist.txt
    #./createConfig.py $fbSet $swt 2 $dir/${nbFEDs}x1x2 -s /tmp/fed1022_$$.txt --dpSet ${fbSet}/dp_2BU --daqval -p $HOME/configurator/CONFIGURATOR_DAQ2VAL.properties
    #./createConfig.py $fbSet $swt 2 $dir/${nbFEDs}x1x2_noChksums -s /tmp/$$.txt --dpSet ${fbSet}/dp_1x2 --daqval -p $HOME/configurator/CONFIGURATOR_DAQ2VAL.properties
done
