#!/usr/bin/env python3

import functools
import gzip
import os
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import QName as QN


import FitModels


def readPlainFile(configFile):
    rus = []
    with open(configFile) as file:
        for line in file:
            result = re.match(r'.*FEDBuilder:.*id=(\d+).*name=([\w+-]+).*srcIds: ([\d\(\),; ]+)',line)
            if result:
                fedIds = re.findall(r'[\(\),; ]*(\d+)',result.group(3))
                rus.append((["fb_%02d"%int(result.group(1)),result.group(2)],fedIds))
    return rus


class CheckFBthroughput:

    def __init__(self,args):
        self.args = vars(args)
        if self.args['plain']:
            self.RUs = readPlainFile(self.args['configFile'])
        else:
            config = self.readXMLFile(self.args['configFile'])
            self.RUs = self.getRUs(config)
        self.params = self.readParamFile(self.args['paramFile'])
        #self.getModelParamsSkylake()
        self.getModelParamsAMD()
        #self.maxThroughputModels = {
        #     8 : functools.partial(FitModels.linearPlateauModel,parameters=(0,0.79978,12149.5)),
        #    10 : functools.partial(FitModels.linearPlateauModel,parameters=(0,0.99971, 8703.38)),
        #    12 : functools.partial(FitModels.linearPlateauModel,parameters=(0,1.19966, 7789.65)),
        #    16 : functools.partial(FitModels.linearPlateauModel,parameters=(0,1.59955, 5635.41)),
        #    20 : functools.partial(FitModels.linearPlateauModel,parameters=(0,1.99939, 4334.02)),
        #    27 : functools.partial(FitModels.linearPlateauModel,parameters=(0,2.69930, 3024.79)),
        #    }


    def getModelParamsSkylake(self):
        #CHEP2019
        self.maxThroughputModels = {
             8 : functools.partial(FitModels.linearPlateauModel,   parameters=(0,2.24928,4316.72)),
            10 : functools.partial(FitModels.linearPlateauModel,   parameters=(29.8127,2.73777,3168.96)),
            12 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,3.44992,5752.32)),
            16 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,3.94864,4944.05)),
            20 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,4.59320,3892.66)),
            27 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,5.13860,3238.54)),
            36 : functools.partial(FitModels.quadraticPlateauModel,parameters=(37.5104,5.20777,2989.20)),
            44 : functools.partial(FitModels.quadraticPlateauModel,parameters=(177.452,4.82780,3098.82)),
            55 : functools.partial(FitModels.quadraticPlateauModel,parameters=(289.654,4.47295,3394.14)),
            }
        self.maxFragmentSizes = {
            100 : {
                 8 : 12000,
                10 :  8200,
                12 :  7500,
                16 :  5500,
                20 :  4300,
                27 :  3000,
                36 :  2000,
                44 :  1000,
                55 :  200,
                },
            50 : {
                 8 : 12000,
                10 : 12000,
                12 : 12000,
                16 : 11000,
                20 :  8000,
                27 :  5500,
                36 :  4200,
                44 :  3300,
                55 :  1500,
                }
            }


    def getModelParamsAMD(self):
        #20191120
        self.maxThroughputModels = {
             8 : functools.partial(FitModels.linearPlateauModel,   parameters=(143.961,2.82612,2125.97)),
            10 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,3.82341,3415.73)),
            12 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,4.17365,3398.74)),
            16 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,4.58240,3232.70)),
            20 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,4.88082,3211.46)),
            27 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,5.23743,3052.89)),
            36 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,5.36374,2730.50)),
            44 : functools.partial(FitModels.quadraticPlateauModel,parameters=(0,5.33543,2736.68)),
            55 : functools.partial(FitModels.quadraticPlateauModel,parameters=(13.5097,5.20221,2695.82)),
            }
        self.maxFragmentSizes = {
            100 : {
                 8 :  7500,
                10 :  6000,
                12 :  5600,
                16 :  4600,
                20 :  3700,
                27 :  2800,
                36 :  1800,
                44 :  1000,
                55 :  0,
                },
            50 : {
                 8 : 12000,
                10 : 12000,
                12 : 12000,
                16 : 10000,
                20 :  8000,
                27 :  5500,
                36 :  3200,
                44 :  3000,
                55 :  1500,
                }
            }


    def readXMLFile(self,configFile):
        if os.path.exists(configFile):
            ETroot = ET.parse(configFile).getroot()
        elif os.path.exists(configFile+".gz"):
            f = gzip.open(configFile+".gz") #gzip.open() doesn't support the context manager protocol needed for it to be used in a 'with' statement.
            ETroot = ET.parse(f).getroot()
            f.close()
        else:
            raise IOError(configFile+"(.gz) does not exist")
        return ETroot


    def readParamFile(self,paramFile):
        params = {}
        with open(paramFile) as file:
            try:
                for line in file:
                    try:
                        (fedIds,a,b,c,rms) = line.split(',',5)
                        params[fedIds] = (float(a),float(b),float(c),None)
                    except ValueError:
                        (fedIds,a,b,c,d,rms) = line.split(',',6)
                        params[fedIds] = (float(a),float(b),float(c),float(d))
            except Exception as e:
                print("Failed to parse: "+line)
                raise e
        return params


    def getRUs(self,config):
        rus = []
        xcns = re.match(r'\{(.*?)\}Partition',config.tag).group(1) ## Extract xdaq namespace
        for context in config.getiterator(str(QN(xcns,'Context'))):
            for app in context.getiterator(str(QN(xcns,'Application'))):
                if app.attrib['class'] in ('sfb::EVM','sfb::RU'):
                    fedIds = []
                    group = app.attrib['group'].split(',')
                    for child in app:
                        if 'properties' in child.tag:
                            for prop in child:
                                if 'fedSourceIds' in prop.tag:
                                    for item in prop:
                                        fedIds.append(item.text)
                    rus.append((group[1:],fedIds))
        return rus


    def calculateThroughput(self):
        eventSize = 0
        fedCount = 0
        fbCount = 0
        for ru in self.RUs:
            superFragmentSize = 0.
            for fed in ru[1]:
                try:
                    param = self.params[fed]
                    fedSize = param[0] + param[1]*self.args['relEventSize'] + param[2]*self.args['relEventSize']**2
                    if param[3]:
                        fedSize += param[3]*self.args['relEventSize']**3
                except KeyError:
                    fedSize = 2048
                    print("WARNING: FED %s not found in paramFile. Using %d Bytes" % (fed,fedSize))
                superFragmentSize += int(fedSize+4)&~0x7
            eventSize += superFragmentSize/1e6
            triggerRate = self.args['triggerRate']
            throughput = (superFragmentSize*triggerRate)/1e6
            fbSize = len(ru[1])
            fedCount += fbSize
            fbCount += 1
            averageFedSize = superFragmentSize/fbSize
            maxFragmentSizes = self.maxFragmentSizes[triggerRate] if triggerRate in self.maxFragmentSizes else self.maxFragmentSizes[min(self.maxFragmentSizes.keys(), key=lambda k: abs(k-triggerRate-1))]
            maxFedSize = maxFragmentSizes[fbSize] if fbSize in maxFragmentSizes else maxFragmentSizes[min(maxFragmentSizes.keys(), key=lambda k: abs(k-fbSize-1))]
            maxThroughputModel = self.maxThroughputModels[fbSize] if fbSize in self.maxThroughputModels else self.maxThroughputModels[min(self.maxThroughputModels.keys(), key=lambda k: abs(k-fbSize-1))]
            maxThroughput = maxThroughputModel((averageFedSize,))/1e3
            if self.args['printAll'] or throughput > maxThroughput or averageFedSize > maxFedSize:
                #print("  %1.2f GB/s (< %1.2f GB/s) from %2d FEDs with average %4d Bytes (< %d Bytes) : %s %s" % (throughput,maxThroughput,fbSize,averageFedSize,maxFedSize,ru[0],ru[1]))
                print("  %1.2f GB/s (< %1.2f GB/s) from %2d FEDs with average %4d Bytes : %s %s" % (throughput,maxThroughput,fbSize,averageFedSize,ru[0],ru[1]))
        print("============================")
        print("Total event size: %1.2f MB"%(eventSize))
        print(str(fedCount)+" FEDs in "+str(fbCount)+" FEDbuilders")


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("configFile",help="configuration file")
    parser.add_argument("paramFile",help="calculate the FED fragment sizes using the parameters in the given file")
    parser.add_argument("relEventSize",type=float,help="scale the event size by this factor")
    parser.add_argument("-a","--printAll",action='store_true',help="print the throughput of all FB irrespectively if they exceed the maximum or not")
    parser.add_argument("-p","--plain",action='store_true',help="use plain text file instead of XML configuration")
    parser.add_argument("-t","--triggerRate",default=100,type=int,help="L1 trigger rate in kHz [default: %(default)s]")
    checkFBthroughput = CheckFBthroughput( parser.parse_args() )
    checkFBthroughput.calculateThroughput()
