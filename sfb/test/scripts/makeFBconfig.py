#!/usr/bin/env python3

import gzip
import os
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import QName as QN
from checkFBthroughput import readPlainFile


class MakeFBconfig:

    def __init__(self,args):
        self.args = vars(args)

        if os.path.exists(self.args['templateFile']):
            self.template = ET.parse(self.args['templateFile']).getroot()
        elif os.path.exists(self.args['templateFile']+".gz"):
            f = gzip.open(self.args['templateFile']+".gz") #gzip.open() doesn't support the context manager protocol needed for it to be used in a 'with' statement.
            self.template = ET.parse(f).getroot()
            f.close()
        else:
            raise IOError(self.args['templateFile']+"(.gz) does not exist")

        self.xcns = re.match(r'\{(.*?)\}Partition',self.template.tag).group(1) ## Extract xdaq namespace

        self.fedIDs = self.getFedIDs()
        self.setFedIdsOnFEROLs()
        self.setFedIdsOnRUs()
        self.writeConfig()


    def writeConfig(self):
        outputFile = self.args['fb']+"_"+str(len(self.fedIDs))+"streams.xml"
        with open(outputFile,'w') as output:
            output.write(ET.tostring(self.template,encoding='utf8', method='xml').decode(encoding="utf-8"))


    def getFedIDs(self):
        for fb in readPlainFile(self.args['configFile']):
            if self.args['fb'] in fb[0]:
                return fb[1]
        raise Exception("FB "+self.args['fb']+" not found in "+self.args['configFile'])


    def getValueForFEDid(self,placeholder):
        val = int(placeholder[-2:])
        if val > 0:
            return str(self.fedIDs[(val-1)])
        else:
            return placeholder


    def setFedIdsOnFEROL40(self):
        for ferol40 in self.template.findall('.//'+QN(self.xcns,'Application').text+'[@class="ferol40::Ferol40Controller"]'):
            propNS = re.match(r'\{(.*?)\}properties',ferol40[0].tag).group(1) ## Extract namespace of properties
            for item in ferol40.findall('.//*'+QN(propNS,'item').text):
                for value in item:
                    if 'expectedFedId' in value.tag:
                        try:
                            value.text = self.getValueForFEDid(value.text)
                            enabled = True
                        except IndexError:
                            value.text = '9999'
                            enabled = False
                        except ValueError:
                            value.text = '9999'
                            enabled = False
                for value in item:
                    if 'enable' in value.tag:
                        if enabled:
                            value.text = 'true'
                        else:
                            value.text = 'false'


    def setFedIdsOnFEROL(self):
        for ferol in self.template.findall('.//'+QN(self.xcns,'Application').text+'[@class="ferol::FerolController"]'):
            #propNS = re.match(r'\{(.*?)\}properties',ferol[0].tag).group(1) ## Extract namespace of properties
            for item in ferol:
                for value in item:
                    if 'expectedFedId_0' in value.tag:
                        try:
                            value.text = self.getValueForFEDid(value.text)
                            enabled_0 = True
                        except IndexError:
                            value.text = '9999'
                            enabled_0 = False
                        except ValueError:
                            value.text = '9999'
                            enabled_0 = False
                    if 'expectedFedId_1' in value.tag:
                        try:
                            value.text = self.getValueForFEDid(value.text)
                            enabled_1 = True
                        except IndexError:
                            value.text = '9999'
                            enabled_1 = False
                        except ValueError:
                            value.text = '9999'
                            enabled_1 = False
                for value in item:
                    if 'enableStream0' in value.tag:
                        if enabled_0:
                            value.text = 'true'
                        else:
                            value.text = 'false'
                    if 'enableStream1' in value.tag:
                        if enabled_1:
                            value.text = 'true'
                        else:
                            value.text = 'false'


    def setFedIdsOnFEROLs(self):
        self.setFedIdsOnFEROL40()
        self.setFedIdsOnFEROL()


    def setFedIdsOnRUs(self):
        for ru in self.template.findall('.//'+QN(self.xcns,'Application').text+'[@class="sfb::RU"]'):
            propNS = re.match(r'\{(.*?)\}properties',ru[0].tag).group(1) ## Extract namespace of properties
            for fedSourceIds in ru.findall('.//*'+QN(propNS,'fedSourceIds').text):
                if len(self.fedIDs) > len(fedSourceIds):
                    raise Exception("Template "+self.args['templateFile']+"(.gz) contains only "+str(len(fedSourceIds))+" fedSourceIds, but FB "+self.args['fb']+" requires "+str(len(self.fedIDs))+" FEDs")
                itemsToRemove = []
                for item in fedSourceIds:
                    try:
                        item.text = self.getValueForFEDid(item.text)
                    except IndexError:
                        itemsToRemove.append(item)
                for item in itemsToRemove:
                    fedSourceIds.remove(item)
            for fedId in ru.findall('.//*'+QN(propNS,'fedId').text):
                try:
                    fedId.text = self.getValueForFEDid(fedId.text)
                except IndexError:
                    fedId.text = '9999'


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("configFile",help="configuration file")
    parser.add_argument("templateFile",help="use given XML file as template")
    parser.add_argument("fb",help="name of FEDbuilder to use")
    makeFBconfig = MakeFBconfig( parser.parse_args() )
