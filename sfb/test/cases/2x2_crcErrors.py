import operator
import time
import array

from TestCase import *
from Context import FEROL,RU,BU


class case_2x2_crcErrors(TestCase):

    def checkIt(self,crcErrors=array.array('i', [0,0])):
        self.checkEVM(8192)
        self.checkRU(8192)
        self.checkBU(8192,100,0)
        self.checkBU(8192,100,0)
        self.checkAppParam('nbCorruptedEvents','unsignedLong',0,operator.eq,"BU",0)
        self.checkAppParam('nbCorruptedEvents','unsignedLong',0,operator.eq,"BU",1)
        self.checkAppParam('nbEventsWithCRCerrors','unsignedLong',crcErrors[0],operator.eq,"BU",0)
        self.checkAppParam('nbEventsWithCRCerrors','unsignedLong',crcErrors[1],operator.eq,"BU",1)


    def runTest(self):
        testDirBU0="/tmp/evb_test/ramdisk_BU0"
        testDirBU1="/tmp/evb_test/ramdisk_BU1"
        self.prepareAppliance(testDirBU0,runNumber=1)
        self.prepareAppliance(testDirBU1,runNumber=1)
        self.setAppParam('rawDataDir','string',testDirBU0,'BU',0)
        self.setAppParam('rawDataDir','string',testDirBU1,'BU',1)
        self.setAppParam('metaDataDir','string',testDirBU0,'BU',0)
        self.setAppParam('metaDataDir','string',testDirBU1,'BU',1)
        self.configureEvB()
        self.setAppParam('hltParameterSetURL','string','file://'+testDirBU0,'BU',0)
        self.setAppParam('hltParameterSetURL','string','file://'+testDirBU1,'BU',1)
        self.enableEvB(runNumber=1)
        self.checkIt()

        print("1 CRC error on FED 1")
        self.setAppParam('nbFedCRCerrors','unsignedInt','1','FEROL',1)
        time.sleep(2)
        print("3 CRC error on FED 5")
        self.setAppParam('nbSlinkCRCerrors','unsignedInt','3','FEROL',5)
        time.sleep(2)
        print("2 CRC error on FED 7")
        self.setAppParam('nbFedCRCerrors','unsignedInt','2','FEROL',7)
        time.sleep(3)
        self.checkState("Enabled")
        self.checkIt(array.array('i', [1,5]))
        dumps = self.getFiles("dump_run000001_event[0-9]+_fed0001.txt$",app='EVM')
        if len(dumps) != 1:
            raise ValueException("Expected 1 FED dump file on EVM, but found: "+str(dumps))
        dumps = self.getFiles("dump_run000001_event[0-9]+_fed000[57].txt$",app='RU')
        if len(dumps) != 5:
            raise ValueException("Expected 5 FED dump files on RU, but found: "+str(dumps))
        dumps = self.getFiles("dump_run000001_event[0-9]+.txt$",app='BU')
        if len(dumps) != 0:
            raise ValueException("Expected no event dump files on BU, but found: "+str(dumps))

        print("100 CRC errors on FED 6")
        self.setAppParam('nbSlinkCRCerrors','unsignedInt','100','FEROL',6)
        time.sleep(5)
        self.checkState("Enabled")
        dumps = self.getFiles("dump_run000001_event[0-9]+_fed0006.txt$",app='RU')
        if len(dumps) != 10:
            raise ValueException("Expected 10 dump file from FED 6, but found: "+str(dumps))
        self.checkAppParam('nbCorruptedEvents','unsignedLong',0,operator.eq,"BU",0)
        self.checkAppParam('nbCorruptedEvents','unsignedLong',0,operator.eq,"BU",1)
        self.checkAppParam('nbEventsWithCRCerrors','unsignedLong',1,operator.eq,"BU",0)
        self.checkAppParam('nbEventsWithCRCerrors','unsignedLong',10,operator.gt,"BU",1)

        self.stopEvB()
        self.checkBuDir(testDirBU0,"000001",eventSize=8192,buInstance=0)
        self.checkBuDir(testDirBU1,"000001",eventSize=8192,buInstance=1)

        self.enableEvB(runNumber=2)
        self.checkIt()
        self.stopEvB()
        self.checkBuDir(testDirBU0,"000002",eventSize=8192,buInstance=0)
        self.checkBuDir(testDirBU1,"000002",eventSize=8192,buInstance=1)


    def fillConfiguration(self,symbolMap):
        evm = RU(symbolMap,[
             ('inputSource','string','Socket'),
             ('checkCRC','unsignedInt','1'),
             ('fakeLumiSectionDuration','unsignedInt','5')
            ])
        for id in range(0,4):
            self._config.add( FEROL(symbolMap,evm,id) )

        ru = RU(symbolMap,[
             ('inputSource','string','Socket'),
             ('checkCRC','unsignedInt','1')
            ])
        for id in range(4,8):
            self._config.add( FEROL(symbolMap,ru,id) )

        self._config.add( evm )
        self._config.add( ru )

        self._config.add( BU(symbolMap,[
             ('checkCRC','unsignedInt','1'),
             ('staleResourceTime','unsignedInt','0'),
             ('lumiSectionTimeout','unsignedInt','6')
            ]) )
        self._config.add( BU(symbolMap,[
             ('checkCRC','unsignedInt','1'),
             ('staleResourceTime','unsignedInt','0'),
             ('lumiSectionTimeout','unsignedInt','6')
            ]) )
