import sys
import time

from TestCase import TestCase
from Context import FEROL,RU,BU


class case_1x1_stuckFED(TestCase):

    def runTest(self):
        testDir="/tmp/evb_test/ramdisk"
        runNumber=time.strftime("%s",time.localtime())
        self.prepareAppliance(testDir,runNumber)
        self.setAppParam('rawDataDir','string',testDir,'BU')
        self.setAppParam('metaDataDir','string',testDir,'BU')
        self.configureEvB()
        self.setAppParam('hltParameterSetURL','string','file://'+testDir,'BU')
        self.enableEvB(runNumber=runNumber)
        self.checkEVM(4096)
        self.checkBU(4096,100,0)
        eventToStop = self.getEventInFuture()
        self.setAppParam('stopAtEvent','unsignedInt',eventToStop,'FEROL',1)
        sys.stdout.write("Stop FED 587 at event "+str(eventToStop))
        sys.stdout.flush()
        self.stop('FEROL',1)
        self.waitForAppState('Ready','FEROL',1)
        print(" done")
        time.sleep(5)
        self.haltEvB()
        self.checkEventCount(allBuilt=False)
        self.checkBuDir(testDir,runNumber,eventSize=4096)


    def fillConfiguration(self,symbolMap):
        evm = RU(symbolMap,[
             ('inputSource','string','Socket'),
             ('fakeLumiSectionDuration','unsignedInt','0')
            ])
        self._config.add( FEROL(symbolMap,evm,512) )
        self._config.add( FEROL(symbolMap,evm,587) )
        self._config.add( evm )
        self._config.add( BU(symbolMap,[
             ('lumiSectionTimeout','unsignedInt','0'),
             ('staleResourceTime','unsignedInt','0')
            ]) )
