#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 19:06:40 2021

@author: apetro
"""

import csv

#os = open("CDAQ3_SymbolMap.txt", "w")
os = open("CDAQ3_SymbolMap-patch.txt", "w")

print("LAUNCHER_BASE_PORT 17777", end="\n", file=os)
print("SOAP_BASE_PORT 25000", end="\n", file=os)
print("I2O_BASE_PORT 54320", end="\n", file=os)
print("FRL_BASE_PORT 55320", end="\n", file=os)
print("", end="\n", file=os)



with open('rubu_cdaq3_host_20210920.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    hostnames = []
    for parts in readCSV:
        hostnames.append(parts[0])
    index=0
    for hostname in hostnames:
        print("RU"+ str(index) + "_SOAP_HOST_NAME " + hostname + ".cms", end="\n", file=os)
        print("RU"+ str(index) + "_I2O_HOST_NAME " + hostname + ".ebs0.cms", end="\n", file=os)
        index += 1
    print("", end="\n", file=os)

    hostnames.pop(0)
    #hostnames.reverse()
    index=0
    for hostname in hostnames:
        print("BU"+ str(index) + "_SOAP_HOST_NAME " + hostname + ".cms", end="\n", file=os)
        print("BU"+ str(index) + "_I2O_HOST_NAME " + hostname + ".ebs0.cms", end="\n", file=os)
        index += 1