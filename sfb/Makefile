# $Id: Makefile,v 1.9 2009/02/04 15:17:18 cschwick Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: R.K. Mommsen                                                 #
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# This is the TriDAS/daq/sfb Package Makefile
#
##

BUILD_HOME:=$(shell pwd)/..

BUILD_SUPPORT=build
PROJECT_NAME=worksuite
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)

#
# Packages to be built
#
Project=$(PROJECT_NAME)
Package=sfb

PACKAGE_REQUIRED_PACKAGE_LIST = \
	boost-filesystem \
	boost-thread \
	boost-system

Sources+=\
	CRCCalculator.cc \
	DumpUtility.cc \
	SfBidFactory.cc \
	FragmentSize.cc \
	FragmentTracker.cc\
	I2OMessages.cc \
	InfoSpaceItems.cc \
	readoutunit/DummyFragment.cc \
	readoutunit/FedFragment.cc \
	readoutunit/MetaDataRetrieverDIPBridge.cc \
	readoutunit/MetaData.cc \
	readoutunit/SuperFragment.cc \
	EVM.cc \
	evm/RUproxy.cc\
	RU.cc \
	SFW.cc \
	superfragmentwriter/DiskUsage.cc \
	superfragmentwriter/DiskWriter.cc \
	superfragmentwriter/Event.cc \
	superfragmentwriter/SuperFragmentHandler.cc \
	superfragmentwriter/EventInfo.cc \
	superfragmentwriter/FedInfo.cc \
	superfragmentwriter/FragmentChain.cc \
	superfragmentwriter/ResourceManager.cc \
	superfragmentwriter/RUproxy.cc \
	superfragmentwriter/StateMachine.cc \
	superfragmentwriter/StreamHandler.cc \
	version.cc \
	DummyFEROL.cc \
	dummyFEROL/FragmentGenerator.cc \
	dummyFEROL/StateMachine.cc 

ifeq ($(XDAQ_ARCH),x86_64)
Sources+=\
	crc16_T10DIF_128x_extended.S \
	crc32c.cc 
endif


UnitTests = \
	EvBid.cxx \
	GetIPaddress.cxx \
	Fibonacci.cxx \
	LogNormal.cxx \
	OneToOneQueue.cxx \
	OneToOneQueueWait.cxx

IncludeDirs = \
	$(XGI_TOOLS_INCLUDE_PREFIX) \
	$(XDAQ2RC_INCLUDE_PREFIX) \
	$(INTERFACE_SFB_INCLUDE_PREFIX) \
	$(INTERFACE_SHARED_INCLUDE_PREFIX) \
	/usr/include/tirpc/ 


TestIncludeDirs = test/include

TestLibraries = \
	b2innub \
	boost_system \
	boost_thread-mt \
	cgicc \
	config \
	sfb \
	executive \
	interfaceshared \
	log4cplus \
	logudpappender \
	logxmlappender \
	mimetic \
	numa \
	peer \
	ptblit \
	tcpla \
	toolbox \
	asyncresolv \
	uuid \
	xcept \
	xdaq \
	xdata \
	xdaq2rc \
	xerces-c \
	xgi \
	xoap

TestLibraryDirs = \
        $(SFB_LIB_PREFIX)  \
        $(PEER_LIB_PREFIX) \
        $(UUID_LIB_PREFIX) \
        $(INTERFACE_SHARED_LIB_PREFIX) \
        $(XDAQ2RC_LIB_PREFIX)

UserCCFlags = -O3 -funroll-loops -Werror
#UserCCFlags += -DEVB_DEBUG_CORRUPT_EVENT
#UserCCFlags += -DEVB_USE_LOCS_VECTOR

# These libraries can be platform specific and
# potentially need conditional processing
DependentLibraries = interfaceshared xdaq2rc ptblit boost_filesystem boost_thread curl
DependentLibraryDirs = \
	/usr/lib64 \
	$(INTERFACE_SHARED_LIB_PREFIX) \
	$(XDAQ2RC_LIB_PREFIX)

#
# Compile the source files and create a shared library
#
DynamicLibrary=sfb

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
