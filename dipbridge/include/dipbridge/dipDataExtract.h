// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _dipbridge_dipDataExtract_h_
#define _dipbridge_dipDataExtract_h_

#include "dip/DipData.h"
#include "xdata/Table.h"
#include <string>

namespace dipbridge
{

	/**
	 *  Extract xdata::Serializable pointer and type string of dipdata
	 */
	class dipDataExtract
	{
		public:
			//constructor with dipdata and topic name
			explicit dipDataExtract(const std::string& topicname);
			//destructor
			~dipDataExtract();
			//topic name
			std::string topicname() const;
			//extract dipdata to xdata::Table
			xdata::Table::Reference getAll(const DipData& dipdata, const DipQuality& dipquality);
		private:
			std::string m_topicname;
	};
}
#endif
