// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius               *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "dipbridge/DipErrorHandlers.h"
#include <sstream>
#include <iostream>
#include "dip/DipPublication.h"
#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"


void dipbridge::PubErrorHandler::handleException(DipPublication* publication, DipException& ex)
{
	std::stringstream ss;
	ss<<"dip pub error "<<ex.what();

	if(!pm_logger)
	{
		LOG4CPLUS_ERROR(*pm_logger,ss.str());
	}
	else
	{
		std::cout<<ss.str()<<std::endl;
	}
}

void dipbridge::ServerErrorHandler::handleException(int severity, int code, char *msg)
{
	std::stringstream ss;
	ss<<"dns server error severity "<<severity<<" code "<<code<<" msg "<<msg;

	if(!pm_logger)
	{
		LOG4CPLUS_ERROR(*pm_logger,ss.str());
	}
	else
	{
		std::cout<<ss.str()<<std::endl;
	}
}


