// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius               *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "dipbridge/TypeConverter.h"
#include "dipbridge/dipDataExtract.h"
#include "dipbridge/exception/Exception.h"
#include "dip/DipData.h"
#include "dip/DipException.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Integer.h"
#include "xdata/Integer8.h"
#include "xdata/Integer16.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/Vector.h"
#include <sstream>


dipbridge::dipDataExtract::dipDataExtract( const std::string& topicname ): m_topicname(topicname)
{
}

std::string dipbridge::dipDataExtract::topicname() const
{
	return m_topicname;
}

xdata::Table::Reference dipbridge::dipDataExtract::getAll( const DipData& dipdata, const DipQuality& dipquality )
{
	std::stringstream ss;
	int nfields;
	xdata::Table* data = new xdata::Table;

	if( ! dipdata.isEmpty() )
	{
		//Adding timestamp
		const DipTimestamp dipt = dipdata.extractDipTime();
		data->addColumn("DipTimestamp", "int 64");
		xdata::Integer64 fieldvalue(dipt.getAsMillis());
		data->setValueAt(0, "DipTimestamp", fieldvalue);

		//Adding quality
		data->addColumn("DipQuality", "int");
		xdata::Integer qualityvalue(dipquality);
		data->setValueAt(0, "DipQuality", qualityvalue);

		const char** allfields = dipdata.getTags(nfields);
		for(int i=0; i<nfields; ++i)
		{
			std::string fieldname(allfields[i]);
			DipDataType t = dipdata.getValueType( fieldname.c_str() );
			std::string xdatatype = DipDataToXdataType(t);
			data->addColumn(fieldname,xdatatype);
			int arraysize = 0;
			if( xdatatype==std::string("bool") )
			{
				try
				{
					xdata::Boolean fieldvalue = (bool)dipdata.extractBool(fieldname.c_str() ) ;
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractBool "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("vector bool") )
			{
				try
				{
					const DipBool* r = dipdata.extractBoolArray( arraysize, fieldname.c_str() );
					xdata::Vector< xdata::Boolean > fieldvalue;

					for(int idx=0; idx<arraysize; ++idx)
					{
						fieldvalue.push_back( xdata::Boolean(r[idx]) );
					}
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractBoolArray "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("int 8") )
			{
				try
				{
					xdata::Integer8 fieldvalue( (char)dipdata.extractByte(fieldname.c_str() ) );
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractByte "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("vector int 8") )
			{
				try
				{
					const DipByte* r = dipdata.extractByteArray( arraysize, fieldname.c_str() );
					xdata::Vector< xdata::Integer8 > fieldvalue;
					for(int idx=0; idx<arraysize; ++idx)
					{
						fieldvalue.push_back( xdata::Integer8(r[idx]) );
					}
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractByteArray "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}else if( xdatatype==std::string("int 16") )
			{
				try
				{
					xdata::Integer16 fieldvalue( (short)dipdata.extractShort(fieldname.c_str()) );
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractShort "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("vector int 16") )
			{
				try
				{
					const DipShort* r = dipdata.extractShortArray( arraysize, fieldname.c_str() );
					xdata::Vector< xdata::Integer16 > fieldvalue;
					for(int idx=0; idx<arraysize; ++idx)
					{
						fieldvalue.push_back( xdata::Integer16(r[idx]) );
					}
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractShortArray "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}else if( xdatatype==std::string("int 32") )
			{
				try
				{
					xdata::Integer32 fieldvalue( (int)dipdata.extractInt(fieldname.c_str() ) );
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractInt "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("vector int 32") )
			{
				try
				{
					const DipInt* r = dipdata.extractIntArray( arraysize, fieldname.c_str() );
					xdata::Vector< xdata::Integer32 > fieldvalue;
					for(int idx=0; idx<arraysize; ++idx)
					{
						fieldvalue.push_back( xdata::Integer32(r[idx]) );
					}
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractIntArray "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}else if( xdatatype==std::string("int 64") )
			{
				try
				{
					xdata::Integer64 fieldvalue( dipdata.extractLong(fieldname.c_str() ) );
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractLong "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("vector int 64") ){
				try
				{
					const DipLong* r = dipdata.extractLongArray( arraysize, fieldname.c_str() );
					xdata::Vector< xdata::Integer64 > fieldvalue;
					for(int idx=0; idx<arraysize; ++idx)
					{
						fieldvalue.push_back( xdata::Integer64(r[idx]) );
					}
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractLongArray "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("float") )
			{
				try
				{
					xdata::Float fieldvalue( dipdata.extractFloat(fieldname.c_str()) );
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractFloat "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("vector float") )
			{
				try
				{
					const DipFloat* r = dipdata.extractFloatArray( arraysize, fieldname.c_str() );
					xdata::Vector< xdata::Float > fieldvalue;
					for(int idx=0; idx<arraysize; ++idx)
					{
						fieldvalue.push_back( xdata::Float(r[idx]) );
					}
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractFloatArray "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("double") )
			{
				try
				{
					xdata::Double fieldvalue( dipdata.extractDouble(fieldname.c_str()) );
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractDouble "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("vector double") )
			{
				try
				{
					const DipDouble* r = dipdata.extractDoubleArray( arraysize, fieldname.c_str() );
					xdata::Vector< xdata::Double > fieldvalue;
					for(int idx=0; idx<arraysize; ++idx)
					{
						fieldvalue.push_back( xdata::Double(r[idx]) );
					}
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractDoubleArray "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("string") )
			{
				try
				{
					xdata::String fieldvalue( std::string(dipdata.extractString(fieldname.c_str())) );
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractString "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
			else if( xdatatype==std::string("vector string") )
			{
				try
				{
					const std::string* r = dipdata.extractStringArray( arraysize, fieldname.c_str() );
					xdata::Vector< xdata::String > fieldvalue;
					for(int idx=0; idx<arraysize; ++idx)
					{
						fieldvalue.push_back( xdata::String( std::string(r[idx])) );
					}
					data->setValueAt(0,fieldname,fieldvalue);
				}
				catch(const DipException& e)
				{
					ss<<"Failed to extractStringArray "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
				catch(const xdata::exception::Exception& e)
				{
					ss<<"Failed to setValue for "<<fieldname<<" "<<e.what();
					XCEPT_RAISE( dipbridge::exception::dipExtractError, ss.str());
					delete data; data = 0;
				}
			}
		}//end of nfields
	}
	return xdata::Table::Reference(data);
}

dipbridge::dipDataExtract::~dipDataExtract()
{
}


