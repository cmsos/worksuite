/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest					 								 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			       			 *
 *************************************************************************/

		console.log("loaded dev");
$(document).on("xdaq-post-load", function() {
	console.log("init dev");
	$('#exception-maker-submit').on('click', function () {
		console.log("clicked dev");

		var params = "";

		params += "user=" + $('#exception-maker-user').val() + "&"; 
		params += "type=" + $('#exception-maker-type').val() + "&"; 
		
		var fields = ["uuid", "message", "identifier", "notifier", "severity", "class", "tag", "instance", "unamespace", "uvalue"];
		for (var i = 0; i < fields.length; i++)
		{
			var val = $('#exception-maker-' + fields[i]).val();
			if (val != "")
			{
				params += fields[i] + "=" + val + "&"; 
			}
		}

		
		console.log(params);
		
		var options = {
			url: rootURL + "fire?" + params,
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to get send rule request - status code " + xhr.status;
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			console.log("rule submitted");
		});
	});
});