// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: Karoly Banicz and Luciano Orsini					         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _sentinel_utils_Sensor_h_
#define _sentinel_utils_Sensor_h_

#define GCC_VERSION ( __GNUC__            * 10000 +             \
                      __GNUC_MINOR__      * 100   +             \
                      __GNUC_PATCHLEVEL__           )
#include <string>

#include "xdaq/Application.h"
#include "xdaq/Object.h"
#include "log4cplus/logger.h"
#include "xcept/tools.h"
#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"
#include "sentinel/utils/Alarm.h"


#include <cxxabi.h>

/// Macro to raise alarm to Sentinel.
///
/// @param TYPE alarm type; must be an class derived from xcept::Exception
/// @param NAME name of alarm
/// @param SEVERITY warning | error | fatal
/// @param MESSAGE alarm message
/// @param TAG hardware id in Emu
/// @param LOGGER_POINTER pointer to a logger; if NULL, no log message will be logged in the case of failure
///
#define SENTINEL_RAISE_ALARM( TYPE, NAME, SEVERITY, MESSAGE, TAG ) \
sentinel::utils::Sensor<TYPE>(this).raise( NAME, SEVERITY, MESSAGE, TAG, __FILE__, __LINE__, __FUNCTION__)

/// Macro to raise alarm to Sentinel, including a nested exception.
///
/// @param TYPE alarm type; must be an class derived from xcept::Exception
/// @param NAME name of alarm
/// @param SEVERITY warning | error | fatal
/// @param MESSAGE alarm message
/// @param TAG hardware id in Emu
/// @param LOGGER_POINTER pointer to a logger; if NULL, no log message will be logged in the case of failure
/// @param NESTED_EXCEPTION an exception to be embedded in this alarm; must be an class derived from xcept::Exception
///
#define SENTINEL_RAISE_ALARM_NESTED( TYPE, NAME, SEVERITY, MESSAGE, TAG, NESTED_EXCEPTION ) \
sentinel::utils::Sensor<TYPE>(this).raiseNested( NAME, SEVERITY, MESSAGE, TAG, __FILE__, __LINE__, __FUNCTION__, NESTED_EXCEPTION )

/// Macro to revoke an alarm identified by @param NAME
///
/// @param NAME name of alarm
/// @param LOGGER_POINTER pointer to a logger; if NULL, no log message will be logged in the case of failure
///
// No alarm type needed for revoking; use xcept::Exception as dummy template class:
#define SENTINEL_REVOKE_ALARM( NAME ) \
sentinel::utils::Sensor<xcept::Exception>(this).revoke( NAME, __FILE__, __LINE__, __FUNCTION__ )

namespace sentinel
{
	namespace utils
	{

		///
		/// Template for convenience classes for raising and revoking alarms of \c alarmType.
		///
		template<class alarmType>
		class Sensor : public xdaq::Object
		{
			public:

				Sensor(xdaq::Application * parent): xdaq::Object(parent)
				{

				}

				/// Raise alarm of \c alarmType.
				///
				/// @param name name of alarm
				/// @param severity warning | error | fatal
				/// @param message alarm message
				/// @param tag hardware id in Emu
				/// @param file source code file that raised this alarm
				/// @param line source code line that raised this alarm
				/// @param function source code method that raised this alarm
				/// @param owner pointer to the xdaq::Application that raised this alarm
				/// @param logger pointer to a logger; if NULL, no log message will be logged in the case of failure
				///
				void raise(const std::string & name, const std::string & severity, const std::string & message, const std::string & tag, const std::string & file, const int line, const std::string & function)
				{
					try
					{
						// Get info space for alarms:
						xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
						// Declare an exception that will be the alarm:
						alarmType ex(name, message, file, line, function);
						// Set 'tag' property:
						ex.setProperty("tag", tag);
						// Create an xdata container for it, specifying severity and owner:
						sentinel::utils::Alarm *alarm = new sentinel::utils::Alarm(severity, ex, this->getOwnerApplication());
						// Name and emit it:
						if (is && alarm) is->fireItemAvailable(name, alarm);
					}
					catch (xdata::exception::Exception& e)
					{
						// Log failure to emit alarm:
						LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Failed to emit alarm \"" << name << "\" from " << function << " at " << file << ":" << line << " : " << xcept::stdformat_exception_history(e));
					}
#if GCC_VERSION >= 40300
					catch( abi::__forced_unwind& )
					// This is needed for pthread not to abort on cancellation because of "exception not rethrown".
					{
						throw;
					}
#endif
					catch (...)
					{
						// Log failure to emit alarm:
						LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Failed to emit alarm \"" << name << "\" from " << function << " at " << file << ":" << line << " : Unknown exception.");
#if GCC_VERSION < 40300
							throw;
#endif
					}
				}

				/// Raise alarm of \c alarmType.
				///
				/// @param name name of alarm
				/// @param severity warning | error | fatal
				/// @param message alarm message
				/// @param tag hardware id in Emu
				/// @param file source code file that raised this alarm
				/// @param line source code line that raised this alarm
				/// @param function source code method that raised this alarm
				/// @param nestedException an exception of \c nestedExceptionType to be embedded in this alarm; must be an class derived from xcept::Exception
				///
				template<class nestedExceptionType>
				void raiseNested(const std::string & name, const std::string & severity, const std::string & message, const std::string & tag, const std::string & file, const int line, const std::string & function, nestedExceptionType &nestedException)
				{
					try
					{
						// Get info space for alarms:
						xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
						// Declare an exception that will be the alarm:
						alarmType ex(name, message, file, line, function, nestedException);
						// Set 'tag' property:
						ex.setProperty("tag", tag);
						// Create an xdata container for it, specifying severity and owner:
						sentinel::utils::Alarm *alarm = new sentinel::utils::Alarm(severity, ex, this->getOwnerApplication());
						// Name and emit it:
						if (is && alarm) is->fireItemAvailable(name, alarm);
					}
					catch (xdata::exception::Exception& e)
					{
						// Log failure to emit alarm:
						LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Failed to emit alarm \"" << name << "\" from " << function << " at " << file << ":" << line << " : " << xcept::stdformat_exception_history(e));
					}
#if GCC_VERSION >= 40300
					catch( abi::__forced_unwind& )
					// This is needed for pthread not to abort on cancellation because of "exception not rethrown".
					{
						throw;
					}
#endif
					catch (...)
					{
						// Log failure to emit alarm:
						LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Failed to emit alarm \"" << name << "\" from " << function << " at " << file << ":" << line << " : Unknown exception.");

#if GCC_VERSION < 40300
						throw;
#endif
					}
				}

				/// Revoke an alarm identified by @param name
				///
				/// @param name name of alarm
				/// @param file source code file that raised this alarm
				/// @param line source code line that raised this alarm
				/// @param function source code method that raised this alarm
				/// @param owner pointer to the xdaq::Application that raised this alarm
				/// @param logger pointer to a logger; if NULL, no log message will be logged in the case of failure
				///
				void revoke(const std::string & name, const std::string  &file, const int line, const std::string & function)
				{
					try
					{
						// Get info space for alarms:
						xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
						if (is)
						{
							// See if this alarm has already been raised:
							sentinel::utils::Alarm *alarm = dynamic_cast<sentinel::utils::Alarm*>(is->find(name));
							// If it has already been raised, revoke and delete it:
							if (alarm)
							{
								is->fireItemRevoked(name, this->getOwnerApplication());
								delete alarm;
							}
						}
					}
					catch (xdata::exception::Exception& e)
					{
						// Log failure to revoke alarm:
						LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Failed to revoke alarm \"" << name << "\" from " << function << " at " << file << ":" << line << " : " << xcept::stdformat_exception_history(e));
					}
#if GCC_VERSION >= 40300
					catch( abi::__forced_unwind& )
					// This is needed for pthread not to abort on cancellation because of "exception not rethrown".
					{
						throw;
					}
#endif
					catch (...)
					{
						// Log failure to revoke alarm:

						LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Failed to revoke alarm \"" << name << "\" from " << function << " at " << file << ":" << line << " : Unknown exception.");
#if GCC_VERSION < 40300
							throw;
#endif
					}
				}

		};

	}
} // end namespaces

#endif
