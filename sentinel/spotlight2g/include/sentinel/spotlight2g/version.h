/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _sentinel_spotlight2g_version_h_
#define _sentinel_spotlight2g_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MAJOR 3
#define WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MINOR 1
#define WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_PATCH 2
// If any previous versions available E.g. #define WORKSUITE_SENTINELSPOTLIGHT2G_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_SENTINELSPOTLIGHT2G_PREVIOUS_VERSIONS "3.1.0"


//
// Template macros
//
#define WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MAJOR,WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MINOR,WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELSPOTLIGHT2G_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELSPOTLIGHT2G_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MAJOR,WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MINOR,WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_PATCH)
#else 
#define WORKSUITE_SENTINELSPOTLIGHT2G_FULL_VERSION_LIST  WORKSUITE_SENTINELSPOTLIGHT2G_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MAJOR,WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MINOR,WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_PATCH)
#endif 

namespace sentinelspotlight2g
{
	const std::string project = "worksuite";
	const std::string package  =  "sentinelspotlight2g";
	const std::string versions = WORKSUITE_SENTINELSPOTLIGHT2G_FULL_VERSION_LIST;
	const std::string summary = "Server for exceptions and alarms";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
