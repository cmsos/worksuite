/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: D. Simelevicius and L. Orsini                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _sentinel_xmasforward_version_h_
#define _sentinel_xmasforward_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_SENTINELXMASFORWARD_VERSION_MAJOR 1
#define WORKSUITE_SENTINELXMASFORWARD_VERSION_MINOR 0
#define WORKSUITE_SENTINELXMASFORWARD_VERSION_PATCH 0

#undef WORKSUITE_SENTINELXMASFORWARD_PREVIOUS_VERSIONS

#define WORKSUITE_SENTINELXMASFORWARD_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELXMASFORWARD_VERSION_MAJOR, SENTINELXMASFORWARD_VERSION_MINOR, SENTINELXMASFORWARD_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELXMASFORWARD_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELXMASFORWARD_FULL_VERSION_LIST PACKAGE_VERSION_STRING(WORKSUITE_SENTINELXMASFORWARD_VERSION_MAJOR, SENTINELXMASFORWARD_VERSION_MINOR, SENTINELXMASFORWARD_VERSION_PATCH)
#else
#define WORKSUITE_SENTINELXMASFORWARD_FULL_VERSION_LIST WORKSUITE_SENTINELXMASFORWARD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELXMASFORWARD_VERSION_MAJOR, SENTINELXMASFORWARD_VERSION_MINOR, SENTINELXMASFORWARD_VERSION_PATCH)
#endif

namespace sentinelxmasforward
{
	const std::string project = "worksuite";
	const std::string package = "sentinelxmasforward";
	const std::string versions = WORKSUITE_SENTINELXMASFORWARD_FULL_VERSION_LIST;
	const std::string summary = "Sentinel xmas forward";
	const std::string description = "Application retrieves errors from spotlight application and forwards to xmas monitoring system";
	const std::string authors = "D. Simelevicius and L. Orsini";
	const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMSPublic/CMSOS";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string>> getPackageDependencies();
}

#endif
