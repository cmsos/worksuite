/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: D. Simelevicius and L. Orsini                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "sentinel/xmasforward/version.h"

GETPACKAGEINFO(sentinelxmasforward)

void sentinelxmasforward::checkPackageDependencies()
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string>> sentinelxmasforward::getPackageDependencies()
{
	std::set<std::string, std::less<std::string>> dependencies;

	ADDDEPENDENCY(dependencies, config);
	ADDDEPENDENCY(dependencies, xcept);
	ADDDEPENDENCY(dependencies, xdaq);

	return dependencies;
}
