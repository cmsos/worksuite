export ORACLE_BASE=23/client64
export ORACLE_HOME=/usr/lib/oracle/$ORACLE_BASE
export ORACLE_INCLUDE=/usr/include/oracle/$ORACLE_BASE

export XDAQ_ROOT=/opt/xdaq
export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs
export XDAQ_SETTINGS_ROOT=/opt/xdaq/share
export LD_LIBRARY_PATH=/opt/xdaq/lib:lib/linux/x86_64_slc5/:/opt/TimesTen/tt1121/lib/:/nfshome0/mbowen/trunk/daq/sentinel/spotlightocci/lib/linux/x86_64_slc5:$ORACLE_HOME/lib
