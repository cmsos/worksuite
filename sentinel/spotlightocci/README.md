# Preparation

For the usage of OCCI, the `extern/oralce` should not be used anymore. The packages of version 23.4 `Basic Package` and `SDK Package` must be downloaded as rpm for Oracle9 from https://www.oracle.com/de/database/technologies/instant-client/linux-x86-64-downloads.html and installed. Oracle9 is compatible with Alma9.

```bash 
wget https://download.oracle.com/otn_software/linux/instantclient/2340000/oracle-instantclient-basic-23.4.0.24.05-1.el9.x86_64.rpm
wget https://download.oracle.com/otn_software/linux/instantclient/2340000/oracle-instantclient-devel-23.4.0.24.05-1.el9.x86_64.rpm

sudo rpm -i oracle-instantclient-basic-23.4.0.24.05-1.el9.x86_64.rpm
sudo rpm -i oracle-instantclient-devel-23.4.0.24.05-1.el9.x86_64.rpm
```

To be able to access the Oracle installation, the following environment variables must be set.

```bash
export ORACLE_BASE=23/client64
export ORACLE_HOME=/usr/lib/oracle/$ORACLE_BASE
export ORACLE_INCLUDE=/usr/include/oracle/$ORACLE_BASE
```

# Compilation 

To install, check if all XDAQ and Oracle environment variables are correctly set. If yes, compile using the Makefile.

```bash
make
make rpm
```

Install the RPM in the directory `rpm`.

```bash
cd rpm

sudo rpm -i cmsos-worksuite-sentinelspotlightocci-4.5.0-1.r0.alma9.gcc11.x86_64.rpm
```

# Running

It is not possible anymore to run the SpotlightOCCI directly. For this the repository XAAS is needed. The simplest way to test it, is using the standalone configuration. For information about the usage go to the repository [XAAS](https://gitlab.cern.ch/cmsos/xaas/-/tree/baseline_sulfur_16/slim?ref_type=heads). A README for the standalone can be found in its directory `slim/standalone`.
