// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <sstream>
#include "toolbox/TimeVal.h"
#include "toolbox/net/URL.h"
#include "toolbox/Runtime.h"
#include "toolbox/string.h"
#include "toolbox/utils.h"

#include "sentinel/spotlightocci/Repository.h"
#include <iomanip>

#include "xcept/tools.h"

// SQLite Callback to SELECT query
// function is called once per result row
/*
	pArg is a copy of the fourth argument to sqlite_exec(). This parameter is used to pass arbitrary information through to the callback function.
	argc is the number of columns in the query result.
	argv is an array of pointers to strings where each string is a single column of the result for that record.
	columnNames is an array of pointers to the column names, possibly followed by column data types.
*/

/* DISABLED CODE 
int archiveSelectionCallback(void *pArg, int argc, char **argv, char **columnNames)
{
	// store into corresponding archive
	sentinel::spotlightocci::Repository* repository = (sentinel::spotlightocci::Repository*)pArg;
	
	sentinel::spotlightocci::DataBase * currentDataBase = repository->getCurrentDataBase();
	
	toolbox::Properties properties;
	for(int i = 0; i < argc; ++i)
	{
		properties.setProperty(columnNames[i], (argv[i] ? argv[i] : "NULL") );
	}
	toolbox::TimeVal timestamp(toolbox::toUnsignedLong(properties.getProperty("dateTime")));
	
	sentinel::spotlightocci::DataBase * archiveDataBase = 0;
	try
	{
		archiveDataBase = repository->getArchive(timestamp);
	}
	catch(sentinel::spotlightocci::exception::FailedToOpen & e)
	{
		LOG4CPLUS_FATAL (repository->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(e));
		return SQLITE_ABORT;
	}
	
	std::string blob;
	try
	{
		currentDataBase->readBlob(properties.getProperty("exception"), blob);
	}
	catch (sentinel::spotlightocci::exception::FailedToRead& e)
	{
		std::stringstream msg;
		msg << "Failed to read blob from current database for uniqueid " << properties.getProperty("exception");
		XCEPT_DECLARE_NESTED(sentinel::spotlightocci::exception::FailedToRead, f, msg.str(), e);
		LOG4CPLUS_FATAL (repository->getOwnerApplication()->getApplicationLogger(),  xcept::stdformat_exception_history(f));
		return SQLITE_ABORT;
	}
	
	try
	{
		archiveDataBase->store(properties, blob);
	}
	catch (sentinel::spotlightocci::exception::ConstraintViolated& e)
	{
		// ignore
		std::stringstream msg;
		msg << "Exception with uniqueid " << properties.getProperty("uniqueid") << " already transferred to archive, continue.";
		LOG4CPLUS_WARN (repository->getOwnerApplication()->getApplicationLogger(), msg.str());
	}
	catch (sentinel::spotlightocci::exception::FailedToStore& e)
	{
		std::stringstream msg;
		msg << "Failed to transfer row into archive for uniqueid " << properties.getProperty("uniqueid");
		XCEPT_DECLARE_NESTED(sentinel::spotlightocci::exception::FailedToStore,f,msg.str(), e);
		LOG4CPLUS_FATAL (repository->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(f));
		return SQLITE_ABORT;
	}
	
	return SQLITE_OK;
}
DISABLED CODE*/
							   
// -- End of SQLite C callback definition

sentinel::spotlightocci::Repository::Repository(xdaq::Application * owner,
const std::string& username,
const std::string& password,
const std::string& tnsname,
unsigned int scatterReadNum)

: xdaq::Object(owner), repositoryLock_(toolbox::BSem::FULL, true)
{
		
	
	try
	{
		currentDataBase_ = new sentinel::spotlightocci::DataBase( username, password, tnsname, false, scatterReadNum);
		archiveDataBases_["current"] = currentDataBase_;
		
	}
	catch(sentinel::spotlightocci::exception::FailedToOpen & e )
	{
		XCEPT_RETHROW(sentinel::spotlightocci::exception::Exception, "Failed to create exception repository",e);	
	}
}

sentinel::spotlightocci::Repository::~Repository()
{
	for (std::map<std::string,sentinel::spotlightocci::DataBase *>::iterator i = archiveDataBases_.begin(); i != archiveDataBases_.end(); i++ )
	{
		delete (*i).second;
	}
}


void sentinel::spotlightocci::Repository::rearm (const std::string & exception, const std::string & source)

{
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	repositoryLock_.take();
	
	try
	{
		currentDataBase_->rearm(exception,source);
	}
	catch (sentinel::spotlightocci::exception::FailedToStore& fts)
	{
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), "Failed to revoke an exception");
	}
	catch (sentinel::spotlightocci::exception::ConstraintViolated& cv)
	{
		LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Revoke received at least twice");
	}
	
	repositoryLock_.give();
}



void sentinel::spotlightocci::Repository::revoke (xcept::Exception& ex)

{
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	repositoryLock_.take();
	
	try
	{
		currentDataBase_->revoke(ex);
	}
	catch (sentinel::spotlightocci::exception::FailedToStore& fts)
	{
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), "Failed to revoke an exception "  <<  xcept::stdformat_exception_history(fts));
	}
	catch (sentinel::spotlightocci::exception::ConstraintViolated& cv)
	{
		LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Revoke received at least twice "  <<  xcept::stdformat_exception_history(cv));
	}
	
	repositoryLock_.give();
}


void sentinel::spotlightocci::Repository::store (xcept::Exception& ex)

{
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	repositoryLock_.take();
	
	try
	{
		currentDataBase_->fire(ex);
	}
	catch (sentinel::spotlightocci::exception::FailedToStore& fts)
	{
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), "Failed to store an exception " << xcept::stdformat_exception_history(fts));
	}
	catch (sentinel::spotlightocci::exception::ConstraintViolated& cv)
	{
		LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Exception received at least twice " <<  xcept::stdformat_exception_history(cv));
	}
	
	repositoryLock_.give();
}


void sentinel::spotlightocci::Repository::retrieve (const std::string& uuid, const std::string& datetime, const std::string& format, xgi::Output* out) 

{
	repositoryLock_.take();
	
	// this should iterate over all databases if time is older then window

	// LO try current DB first, if not found search in archive accoring datetime 
	if ( currentDataBase_->hasException(uuid) )
	{                                            
                //LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Retrieving exception from file: " << currentDataBase_->getFileName());
		currentDataBase_->retrieve(uuid,format,out);
		repositoryLock_.give();
		return;
	}

	// Better to raise a not found TBD
	std::stringstream msg;
	msg << "Nothing found for date: " << datetime;
        XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, msg.str());
	//LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Nothing found in file for date " << roundedArchiveDate.toString(toolbox::TimeVal::gmt));

	
	repositoryLock_.give();
}

void sentinel::spotlightocci::Repository::catalog
(
 xgi::Output* out, 
 toolbox::TimeVal & start, 
 toolbox::TimeVal &  end,
 const std::string& format
)

{
	repositoryLock_.take();
	
	// this should iterate over all databases if time is older then window
	
	*out << "{\"table\":{";
	
	// definition (header)
	*out << "\"definition\":[{\"key\":\"uniqueid\", \"type\":\"string\"},";
	*out << "{\"key\":\"storeTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"dateTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"identifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"severity\", \"type\":\"string\"},";
	*out << "{\"key\":\"occurrences\", \"type\":\"string\"},";
	*out << "{\"key\":\"notifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"context\", \"type\":\"string\"},";
	*out << "{\"key\":\"message\", \"type\":\"string\"},";
	*out << "{\"key\":\"class\", \"type\":\"string\"}";
	*out << "],";
	
	// rows (data)
	*out << "\"rows\":[";
	
	DataBase::hasInsertedfirstRow(false);
	try
	{
		// Last argument 'true' indicates that the structure output does not follow already other structures delimited by {}
		// Therefore no leading comma separator is needed.
		currentDataBase_->catalog(out,start,end,format);
	}
	catch (sentinel::spotlightocci::exception::NotFound& e)
	{
		// ignore if nothing found
	}	
	
	*out << "]}}" << std::endl;
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	
	repositoryLock_.give();
}

void sentinel::spotlightocci::Repository::lastStoredEvents
(
 xgi::Output* out, 
 toolbox::TimeVal & since, 
 const std::string& format
)

{
	repositoryLock_.take();

	
	// this should iterate over all databases if time is older then window

	// very old *out << "{\"latestStoreTime\":" << std::fixed << std::setprecision(6) << (double) currentDataBase_->getLatestStoreTime();

	//*out << "{\"lastStoreTime\":\"" << currentDataBase_->getLatestStoreTime().toString(toolbox::TimeVal::gmt);
	*out << "{";
	*out << "\"table\":{";
	
	// definition (header)
	*out << "\"definition\":[{\"key\":\"uniqueid\", \"type\":\"string\"},";
	*out << "{\"key\":\"storeTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"dateTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"identifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"severity\", \"type\":\"string\"},";
	*out << "{\"key\":\"occurrences\", \"type\":\"string\"},";
	*out << "{\"key\":\"notifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"context\", \"type\":\"string\"},";
	*out << "{\"key\":\"message\", \"type\":\"string\"},";
	*out << "{\"key\":\"class\", \"type\":\"string\"},";
	*out << "{\"key\":\"args\", \"type\":\"string\"}";
	*out << "],";
	
	// rows (data)
	*out << "\"rows\":[";
	
	DataBase::hasInsertedfirstRow(false);
	// query data base only if something has been inserted

// NO longer necessary just make a query every time TT is fast!!!!
//	if ( since < currentDataBase_->getLatestStoreTime() )
//	{
	toolbox::TimeVal mostRecentStoreTime;
		try
		{
			// Last argument 'true' indicates that the structure output does not follow already other structures delimited by {}
			// Therefore no leading comma separator is needed.
			mostRecentStoreTime = currentDataBase_->lastStoredEvents(out,since,format);
		}
		catch (sentinel::spotlightocci::exception::NotFound& e)
		{
			std::cout << xcept::stdformat_exception_history(e) << std::endl;
			// ignore if nothing found
		}
//	}	
	
	*out << "]} ,";
	*out << "\"lastStoreTime\":\"" << mostRecentStoreTime.toString(toolbox::TimeVal::gmt);
	*out << "\"";

	*out << "}" << std::endl;
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	
	repositoryLock_.give();
}


void sentinel::spotlightocci::Repository::query ( xgi::Output* out, const std::string& query)

{
	std::cout << "Repository::query: " << query << std::endl;
	repositoryLock_.take();

	*out << "{\"table\":{";

	// definition (header)
        *out << "\"definition\":[{\"key\":\"uniqueid\", \"type\":\"string\"},";
        *out << "{\"key\":\"storeTime\", \"type\":\"time\"},";
        *out << "{\"key\":\"dateTime\", \"type\":\"time\"},";
        *out << "{\"key\":\"identifier\", \"type\":\"string\"},";
        *out << "{\"key\":\"severity\", \"type\":\"string\"},";
        *out << "{\"key\":\"occurrences\", \"type\":\"string\"},";
        *out << "{\"key\":\"notifier\", \"type\":\"string\"},";
        *out << "{\"key\":\"context\", \"type\":\"string\"},";
        *out << "{\"key\":\"message\", \"type\":\"string\"},";
        *out << "{\"key\":\"class\", \"type\":\"string\"}";
        *out << "],";

        // rows (data)
        *out << "\"rows\":[";

	DataBase::hasInsertedfirstRow(false);
	try
	{
		currentDataBase_->query(out, query);
	}
	catch (sentinel::spotlightocci::exception::NotFound& e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		// ignore if nothing found
	}

	*out << "]}}" << std::endl;
        out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");

	repositoryLock_.give();
}

//--
void sentinel::spotlightocci::Repository::events
(
 xgi::Output* out, 
 toolbox::TimeVal & start, 
 toolbox::TimeVal &  end,
 const std::string& format
)

{
	repositoryLock_.take();
	
	// this should iterate over all databases if time is older then window
	
	*out << "{\"table\":{";
	
	// definition (header)
	*out << "\"definition\":[{\"key\":\"uniqueid\", \"type\":\"string\"},";
	*out << "{\"key\":\"storeTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"dateTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"identifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"severity\", \"type\":\"string\"},";
	*out << "{\"key\":\"occurrences\", \"type\":\"string\"},";
	*out << "{\"key\":\"notifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"context\", \"type\":\"string\"},";
	*out << "{\"key\":\"message\", \"type\":\"string\"},";
	*out << "{\"key\":\"class\", \"type\":\"string\"}";
	*out << "],";
	
	// rows (data)
	*out << "\"rows\":[";
	
	DataBase::hasInsertedfirstRow(false);
	try
	{
		// Last argument 'true' indicates that the structure output does not follow already other structures delimited by {}
		// Therefore no leading comma separator is needed.
		currentDataBase_->events(out,start,end,format);
	}
	catch (sentinel::spotlightocci::exception::NotFound& e)
	{
		// ignore if nothing found
	}	
	
	*out << "]}}" << std::endl;
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	
	repositoryLock_.give();
}

//--


/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal& sentinel::spotlightocci::Repository::lastExceptionTime()
{
	return lastExceptionTime_;
}

/*! Average time to store a single exception */
sentinel::spotlightocci::DataBase * sentinel::spotlightocci::Repository::getCurrentDataBase()
{
	return currentDataBase_;
}

/* DISBALED CODE TimeTT uses a unique DB
*/

std::list<toolbox::Properties> sentinel::spotlightocci::Repository::getOpenArchivesInfo()
{
	std::list<toolbox::Properties> plist;
	
	repositoryLock_.take();
		
	for (std::map<std::string,sentinel::spotlightocci::DataBase *>::iterator i = archiveDataBases_.begin(); i != archiveDataBases_.end(); i++)
	{
		toolbox::Properties p;
		try
		{
			p.setProperty("latest", (*i).second->getLatestExceptionTime().toString(toolbox::TimeVal::gmt));
			p.setProperty("count",(*i).second->getNumberOfExceptions());
		}
		catch (sentinel::spotlightocci::exception::Exception& e)
		{
                	std::stringstream msg;
                	msg << "Failed to retrieve archive information from db, ";
                	msg << xcept::stdformat_exception_history(e);
                	LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), msg.str());
        	}
		p.setProperty("oldest", (*i).second->getOldestExceptionTime().toString(toolbox::TimeVal::gmt));
		std::stringstream size;
		size << (*i).second->getSize();
		p.setProperty("size",size.str());
		
		std::stringstream readexceptiontime;
		readexceptiontime << (*i).second->getAverageTimeToRetrieveException();
		p.setProperty("readexceptiontime",readexceptiontime.str());
		
		std::stringstream readcatalogtime;
		readcatalogtime << (*i).second->getAverageTimeToRetrieveCatalog();
		p.setProperty("readcatalogtime",readcatalogtime.str());
		
		std::stringstream writeexceptiontime;
		writeexceptiontime << (*i).second->getAverageTimeToStore();
		p.setProperty("writeexceptiontime",writeexceptiontime.str());
		
		if ((*i).second->getAverageTimeToRetrieveException() > 0 )
		{
			std::stringstream rate;
			rate << 1/(*i).second->getAverageTimeToRetrieveException();
			p.setProperty("readexceptionrate",rate.str());
		}
		else
		{
			p.setProperty("readexceptionrate","0");
		}
	
		if ((*i).second->getAverageTimeToRetrieveCatalog() > 0 )
		{
			std::stringstream rate;
			rate << 1/(*i).second->getAverageTimeToRetrieveCatalog();
			p.setProperty("readcatalograte",rate.str());
		}
		else
		{
			p.setProperty("readcatalograte","0");
		
		}
		
		if ((*i).second->getAverageTimeToStore() > 0 )
		{
			std::stringstream rate;
			rate << 1/(*i).second->getAverageTimeToStore();
			p.setProperty("writeexceptionrate",rate.str());
		}
		else
		{
			p.setProperty("writeexceptionrate","0");
		}
		
		plist.push_back(p);
	}
	repositoryLock_.give();
	
	return plist;
}
void sentinel::spotlightocci::Repository::reset()
{
	repositoryLock_.take();
	try
        {
                currentDataBase_->maintenance();
        }
        catch (sentinel::spotlightocci::exception::Exception& e)
        {
                // Possible archivation errors are logged, but ignored otherwise
                std::stringstream msg;
                msg << "Failed to perform maintenance operation on current db, ";
                msg << xcept::stdformat_exception_history(e);
                LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), msg.str());
        }
	repositoryLock_.give();

}
