// $Id$

/*************************************************************************
 * XDAQ Sentinel Daemon		               								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include <sstream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "sentinel/sentineld/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"

#include "xoap/DOMParserFactory.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

XDAQ_INSTANTIATOR_IMPL (sentinel::sentineld::Application);

XERCES_CPP_NAMESPACE_USE

sentinel::sentineld::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this)
{

	b2in::nub::bind(this, &sentinel::sentineld::Application::onMessage);

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/sentinel/sentineld/images/sentineld-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/sentinel/sentineld/images/sentineld-icon.png");

	inputBus_ = "";
	outputBus_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("inputBus", &inputBus_);
	this->getApplicationInfoSpace()->fireItemAvailable("outputBus", &outputBus_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &sentinel::sentineld::Application::Default, "Default");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

sentinel::sentineld::Application::~Application ()
{

}

void sentinel::sentineld::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) 
{
	//LOG4CPLUS_INFO(this->getApplicationLogger(), "Received message");
	if (plist.getProperty("urn:b2in-eventing:topic") == "urn:xdaq-exception:any")
	{
		// force severity to lowercase
		std::string severity = toolbox::tolower(plist.getProperty("severity"));
		plist.setProperty("severity", severity);


		// forward message , ne need to free it
		std::string name = plist.getProperty("urn:sentinel-exception:identifier");
		counters_[name].incrementPulseCounter();
		if (! this->getEventingBus(outputBus_.toString()).canPublish())
		{
			counters_[name].incrementCommunicationLossCounter();

			if (msg != 0) msg->release();
			return;
		}

		auto it = plist.find ("urn:b2in-protocol:lid");
		if ( it != plist.end() )
		{
			plist.erase (it);
		}

		try
		{
			this->getEventingBus(outputBus_.toString()).publish("sentinel", msg, plist);
			counters_[name].incrementFireCounter();
		}
		catch(eventing::api::exception::Exception & e)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "failed to send flashlist to sensord: " << xcept::stdformat_exception_history(e));
			counters_[name].incrementInternalLossCounter();
			if (msg != 0) msg->release();

		}

	}

}

void sentinel::sentineld::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		try
		{
			this->getEventingBus(inputBus_.toString()).subscribe("urn:xdaq-exception:any");
		}
		catch (eventing::api::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
		}

		try
		{

			if (this->getEventingBus(outputBus_.toString()).canPublish())
			{
				LOG4CPLUS_INFO(this->getApplicationLogger(), "ready to publish on " << outputBus_.toString());
			}
		}
		catch (eventing::api::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
			return;
		}

	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(sentinel::sentineld::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void sentinel::sentineld::Application::TabPanel (xgi::Output * out)
{

	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\" id=\"tabPage2\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void sentinel::sentineld::Application::StatisticsTabPage (xgi::Output * out)
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	// Per exception loss of reports
	*out << cgicc::table().set("class","xdaq-table");
	*out << cgicc::caption("Exceptions");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Exceptions").set("class","xdaq-sortable").set("style", "min-width: 100px;");
	*out << cgicc::th("Received Counter");
	*out << cgicc::th("Enqueued Counter");
	*out << cgicc::th("Enqueuing Loss");
	*out << cgicc::th("No Network Loss");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();


	for (auto w = counters_.begin(); w != counters_.end(); w++)
	{

		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*w).first) << std::endl;

		xdata::UnsignedInteger64 num;
		num = 0;
		num = counters_[(*w).first].getPulseCounter(); // used a received
		*out << cgicc::td(num.toString()) << std::endl;

		num = 0;
		num = counters_[(*w).first].getFireCounter(); // used as enqueued
		*out << cgicc::td(num.toString()) << std::endl;

		num = counters_[(*w).first].getInternalLossCounter();
		*out << cgicc::td(num.toString()) << std::endl;

		num = counters_[(*w).first].getCommunicationLossCounter(); // no network
		*out << cgicc::td(num.toString()) << std::endl;

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void sentinel::sentineld::Application::Default (xgi::Input * in, xgi::Output * out) 
{

	this->TabPanel(out);

}
