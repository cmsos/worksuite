// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "sentinel/tester/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xoap/version.h"
#include "xdata/version.h"
#include "xgi/version.h"
#include "toolbox/version.h"


GETPACKAGEINFO(sentineltester)

void sentineltester::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);   
	CHECKDEPENDENCY(xcept);   
	CHECKDEPENDENCY(xdaq);   
	CHECKDEPENDENCY(xoap);   
	CHECKDEPENDENCY(xdata);   
	CHECKDEPENDENCY(xgi);   
	CHECKDEPENDENCY(toolbox);   
}

std::set<std::string, std::less<std::string> > sentineltester::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies,config); 
    ADDDEPENDENCY(dependencies,xcept);
    ADDDEPENDENCY(dependencies,xdaq);
    ADDDEPENDENCY(dependencies,xoap);
    ADDDEPENDENCY(dependencies,xdata);
    ADDDEPENDENCY(dependencies,xgi);
    ADDDEPENDENCY(dependencies,toolbox);
  
    return dependencies;
}	
	
