// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_tester_version_h_
#define _sentinel_tester_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_SENTINELTESTER_VERSION_MAJOR 2
#define WORKSUITE_SENTINELTESTER_VERSION_MINOR 0 
#define WORKSUITE_SENTINELTESTER_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_SENTINELTESTER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_SENTINELTESTER_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_SENTINELTESTER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELTESTER_VERSION_MAJOR,WORKSUITE_SENTINELTESTER_VERSION_MINOR,WORKSUITE_SENTINELTESTER_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELTESTER_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELTESTER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SENTINELTESTER_VERSION_MAJOR,WORKSUITE_SENTINELTESTER_VERSION_MINOR,WORKSUITE_SENTINELTESTER_VERSION_PATCH)
#else 
#define WORKSUITE_SENTINELTESTER_FULL_VERSION_LIST  WORKSUITE_SENTINELTESTER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELTESTER_VERSION_MAJOR,WORKSUITE_SENTINELTESTER_VERSION_MINOR,WORKSUITE_SENTINELTESTER_VERSION_PATCH)
#endif 

namespace sentineltester
{
	const std::string project = "worksuite";
	const std::string package  =  "sentineltester";
	const std::string versions = WORKSUITE_SENTINELTESTER_FULL_VERSION_LIST;
	const std::string summary = "Distributed error handling tester";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
